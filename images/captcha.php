<?php
	session_start();
 
	// создаем случайное число и сохраняем в сессии
	$randomnr = rand(10000, 99999);

	$_SESSION['randomnr2'] = md5($randomnr);
 
	//создаем изображение
	$im = imagecreatetruecolor(100, 38);
 
	//цвета:
	$white = imagecolorallocate($im, 255, 255, 255);
	$grey = imagecolorallocate($im, 128, 128, 128);
	$black = imagecolorallocate($im, 0, 0, 0);
 
	imagefilledrectangle($im, 0, 0, 100, 38, $grey);
 
	//путь к шрифту:
 
	$font = str_replace('images','',__DIR__) . '/media/captcha/fonts/font.ttf';
	
	//рисуем текст:
	imagettftext($im, 22, 0, 5, 30, $white, $font, $randomnr);
	imagettftext($im, 21, 0, 0, 26, $grey, $font, $randomnr);
 
	// предотвращаем кэширование на стороне пользователя
	header("Expires: Wed, 1 Jan 1997 00:00:00 GMT");
	header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
	header("Cache-Control: no-store, no-cache, must-revalidate");
	header("Cache-Control: post-check=0, pre-check=0", false);
	header("Pragma: no-cache");
 
	//отсылаем изображение браузеру
	header ("Content-type: image/gif");
	imagegif($im);
	imagedestroy($im);
?>