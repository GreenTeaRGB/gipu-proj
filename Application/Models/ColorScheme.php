<?php

namespace Application\Models;
class ColorScheme extends \Application\Classes\Model implements \Application\Classes\Forms\Interfaces\FormBuilderInterface
{
  public function __construct()
  {
    parent::__construct( 'color_scheme', $this->getTypes(), $this );
  }

  public function getTypes()
  {
    return [
      'id'    => 'int',
      'name'  => 'str',
      'image' => 'str',
      'file'  => 'str',

    ];
  }

  public function getFields()
  {
    return array(
      'id' => array(
        'type'    => 'string',
        'visible' => false,
        'edited'  => false,
        'label'   => 'ID'
      ),

      'name' => array(
        'type'     => 'string',
        'visible'  => false,
        'edited'   => true,
        'label'    => 'Название',
        'required' => false,
        'multiple' => false,
      ),

      'image' => array(
        'type'      => 'file',
        'visible'   => false,
        'edited'    => true,
        'label'     => 'Превью изображение',
        'required'  => true,
        'file-type' => 'image/*',
        'multiple'  => false,
      ),

      'file' => array(
        'type'      => 'file',
        'visible'   => false,
        'edited'    => true,
        'label'     => 'css файл цветовой схемы',
        'required'  => true,
        'file-type' => '*',
        'multiple'  => false,
      ),

    );
  }

  public function rules(): array
  {
    return [
      [ [ "name" ], 'required' ],
      [ [ "name" ], 'string' ],
      [ [ "file" ], 'file', 'extensions' => [ 'css' ], 'skipOnEmpty' => isset($_POST['is_file']), 'checkExtensionByMimeType' => false ],
      [ [ "image" ], 'image', 'extensions' => [ 'png', 'jpg', 'jpeg' ], 'skipOnEmpty' => isset($_POST['is_image']) ],

    ];
  }

  public function attributeLabels(): array
  {
    return [
      'name'  => 'Название',
      'image' => 'Превью изображение',
      'file'  => 'css файл цветовой схемы',

    ];
  }

  public function formFields( array $data = [] )
  {
    $fields = [
      'name'  => [ 'type' => 'text', 'class' => [ 'form-control' ] ],
      'image' => [ 'type' => 'file', 'class' => [ 'form-control' ] ],
      'file'  => [ 'type' => 'file', 'class' => [ 'form-control' ] ],

    ];
    if ( count( $data ) > 0 ) {
      $fieldsFilter = [];
      foreach ( $data as $datum ) {
        if ( isset( $fields[$datum] ) )
          $fieldsFilter[$datum] = $fields[$datum];
      }
      return $fieldsFilter;
    }
    return $fields;
  }

  protected function beforeInsert( array $data ): array
  {
    $unixTime = $this->getUnixTime();

    return $data;
  }


}
