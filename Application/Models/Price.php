<?php

namespace Application\Models;
class Price extends \Application\Classes\Model implements \Application\Classes\Forms\Interfaces\FormBuilderInterface
{
  public function __construct()
  {
    parent::__construct( 'price', $this->getTypes(), $this );
  }

  public function getTypes()
  {
    return [
      'id'          => 'int',
      'price_agent' => 'str',
      'discount'    => 'str',
      'active'      => 'str',
      'product_id'  => 'int',
      'agent_id'    => 'int',
      'shop_id'     => 'int'
    ];
  }

  public function getFields()
  {
    return array(
      'id' => array(
        'type'    => 'string',
        'visible' => true,
        'edited'  => false,
        'label'   => 'ID'
      ),

      'price_agent' => array(
        'type'     => 'string',
        'visible'  => true,
        'edited'   => true,
        'label'    => 'Цена',
        'required' => false,
        'multiple' => false,
      ),

      'discount' => array(
        'type'     => 'string',
        'visible'  => true,
        'edited'   => true,
        'label'    => 'Скидка',
        'required' => false,
        'multiple' => false,
      ),

      'active'     => array(
        'type'     => 'string',
        'visible'  => true,
        'edited'   => true,
        'label'    => 'Активность',
        'required' => false,
        'multiple' => false,
      ),
      'product_id' => array(
        'type'     => 'string',
        'visible'  => true,
        'edited'   => true,
        'label'    => 'ID товара',
        'required' => false,
        'multiple' => false,
      ),
      'agent_id'   => array(
        'type'     => 'string',
        'visible'  => true,
        'edited'   => true,
        'label'    => 'ID агента',
        'required' => false,
        'multiple' => false,
      ),

    );
  }

  public function rules(): array
  {
    return [
      [ [ 'agent_id', 'product_id' ], 'required' ],
//      [ [ 'agent_id', 'product_id' ], 'exist', 'on' => 'update_price' ],
      [ [ "active" ], 'string' ],
      [ 'shop_id', 'integer' ],
      [ 'price_agent', 'required', 'on' => 'update_price' ],
      [ [ "price_agent", "discount" ], 'double' ],
    ];
  }

  public function attributeLabels(): array
  {
    return [
      'price'    => 'Цена',
      'discount' => 'Скидка',
      'active'   => 'Активность',

    ];
  }

  public function formFields( array $data = [] )
  {
    $fields = [
      'price'    => [ 'type' => 'text', 'class' => [ 'form-control' ] ],
      'discount' => [ 'type' => 'text', 'class' => [ 'form-control' ] ],
      'active'   => [ 'type' => 'text', 'class' => [ 'form-control' ] ],

    ];
    if ( count( $data ) > 0 ) {
      $fieldsFilter = [];
      foreach ( $data as $datum ) {
        if ( isset( $fields[$datum] ) )
          $fieldsFilter[$datum] = $fields[$datum];
      }
      return $fieldsFilter;
    }
    return $fields;
  }
}
