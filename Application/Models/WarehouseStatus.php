<?php
	
	namespace Application\Models;
	class WarehouseStatus extends \Application\Classes\Model
	{
		public function __construct()
		{
			parent::__construct('warehouse_status', $this->getTypes(), $this);
		}
		
		public function getTypes()
		{
			return array(
				'id'   => 'int',
				'type' => 'str'
			);
		}
		
		public function getFields()
		{
			return [
				'type' => [
					'type'     => 'string',
					'visible'  => true,
					'edited'   => true,
					'required' => false,
					'label'    => 'type'
				]
			];
		}

		protected function afterFetchAll($data){
		  $result = [];
      foreach( $data as $datum ) {
        $result[$datum['id']] = $datum;
		  }
		  return $result;
    }
		
		public static function getWarehouseStatuses()
		{
			$model = new WarehouseStatus();
			return $model->fetchAll();
		}
		
		public static function getStatusById($id)
		{
			$model = new WarehouseStatus();
			$status = $model->where(['id' => $id])->fetchOne();
			
			if ( $status ) {
				return $status['type'];
			}
			
			return false;
		}
	}