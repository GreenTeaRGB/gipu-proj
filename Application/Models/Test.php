<?php
	namespace Application\Models;
class Test extends \Application\Classes\Model implements \Application\Classes\Forms\Interfaces\FormBuilderInterface {

	public function __construct()
	{
		parent::__construct('test', $this->getTypes(), $this);
	}

	public function getTypes(){
		return [
			'email'=>'str',
			'phone'=>'str',
			'name'=>'str',
			'primaryImage'=>'str'
		];
	}

	public function formFields(array $data = []) : array
	{
		return [
			'email' => ['type'=>'text'],
			'password' => ['type'=>'password', 'confirm'=>true],
//			'selected' => ['type'=>'checkbox'],
//			'deleted' => ['type'=>'checkbox'],
			'verificationCode' => ['type'=>'captcha'],
//			'age' => ['type'=>'text'],
//			'salary' => ['type'=>'text'],
//			'a1' => ['type'=>'text'],
//			'a2' => ['type'=>'text'],
//			'a3' => ['type'=>'text'],
//			'username' => ['type'=>'text'],
//			'level' => ['type'=>'text'],
			'phone' => ['type'=>'text'],
			'primaryImage' => ['type'=>'file', 'multi'=>false],
//			'description' => ['type'=>'textarea'],
			'name'=>['type'=>'text']

		];
	}
	public function attributeLabels() : array
	{
		return [
					'email' => 'email_label',
					'password' => 'password_label',
					'password_repeat' => 'password_repeat_label',
					'selected' => 'selected_label',
					'deleted' => 'deleted_label',
					'verificationCode' => 'verificationCode_label',
					'age' => 'age_label',
					'salary' => 'salary_label',
					'a1' => 'a1_label',
					'name' => 'a2_label',
					'a3' => 'a3_label',
					'username' => 'username_label',
					'level' => 'level_label',
					'phone' => 'phone_label',
					'primaryImage' => 'primaryImage_label',
					'description' => 'description_label',
		];
	}
	public function rules() : array
	{
		return [
			// проверяет, что "email" - это корректный email-адрес
			[['email'], 'email', 'on'=>'lol'],
//			['email', 'exist'],
			[['email', 'password', 'verificationCode', 'password_repeat'], 'required', 'on'=>'lol'],
			[['name', 'phone'], 'required', 'on'=>'default'],
			['password', 'compare'],
			['password_repeat', 'safe'],
			// Проверяет 'selected' на равенство 0 или 1, без учета типа данных
//			['selected', 'boolean', 'when'=>function($attr, $val){
//				echo 'when';
//				return  true;
//			}],
//			// Проверяет, что "deleted" - это тип данных boolean и содержит true или false
//			['deleted', 'boolean', 'on'=>'lool', 'trueValue' => true, 'falseValue' => false, 'strict' => true],
			['verificationCode', 'captcha', 'on'=>'lol'],
//			// проверяет, является ли значение атрибута "password" таким же, как "password_repeat"
//			['password', 'compare'],
//			// то же, что и выше, но атрбут для сравнения указан явно
//			['password', 'compare', 'compareAttribute' => 'password_repeat'],
//			// проверяет, что возраст больше или равен 30
//			['age', 'compare', 'compareValue' => 30, 'operator' => '>=', 'type' => 'number'],
//
//			[['from', 'to'], 'date'],
//			[['from_datetime', 'to_datetime'], 'datetime'],
//			[['some_time'], 'time'],
//
//			// установить null для "age" в качестве значения по умолчанию
//			['age', 'default', 'value' => null],
//
//			// установить "USA" в качестве значения по умолчанию для "country"
//			['country', 'default', 'value' => 'USA'],
//
//			// установить в "from" и "to" дату 3 дня и 6 дней от сегодняшней, если они пустые
//			[['from', 'to'], 'default', 'value' => function ($model, $attribute) {
//				return date('Y-m-d', strtotime($attribute === 'to' ? '+3 days' : '+6 days'));
//			}],
//
//			['salary', 'double'],
//
//
//
//			// a1 должно существовать в столбце, который представляется атрибутом "a1"
//			['a1', 'exist'],
//
//			// a1 должно существовать, но его значение будет использовать a2 для проверки существования
//			['a1', 'exist', 'targetAttribute' => 'a2'],
//
//			// и a1, и a2 должны существовать, в противном случае оба атрибута будут возвращать ошибку
//			[['a1', 'a2'], 'exist', 'targetAttribute' => ['a1', 'a2']],
//
//			// и a1, и a2 должны существовать, но только атрибут a1 будет возвращать ошибку
//			['a1', 'exist', 'targetAttribute' => ['a1', 'a2']],
//
//			// a1 требует проверки существования a2 и a3 (используя значение a1)
//			['a1', 'exist', 'targetAttribute' => ['a2', 'a1' => 'a3']],
//
//			// a1 должен существовать. Если a1 - массив, то каждый его элемент должен существовать
//			['a1', 'exist', 'allowArray' => true],

			// проверяет, что "primaryImage" - это загруженное изображение в формате PNG, JPG или GIF
			// размер файла должен быть меньше 1MB
//			['primaryImage', 'file', 'extensions' => ['png', 'jpg', 'gif'], 'maxSize' => 1024*1024],

			// обрезает пробелы вокруг "username" и "email"
//			[['username', 'email'], 'filter', 'filter' => 'trim', 'skipOnArray' => true],
//
//			// нормализует значение "phone"
//			['phone', 'filter', 'filter' => function ($value) {
//				// нормализация значения происходит тут
//				return $value;
//			}],

			['primaryImage', 'image', 'on'=>'lol', 'extensions' => ['png', 'jpg'],
			 'minWidth' => 100, 'maxWidth' => 1000,
			 'minHeight' => 100, 'maxHeight' => 1000,
			 'maxFiles' => 3
			],

//			// проверяет, что значение "level" равно 1, 2 или 3
//			['level', 'in', 'range' => [1, 2, 3]],
//			['age', 'integer'],
//
//			// проверяет, что "username" начинается с буквы и содержит только буквенные символы,
//			// числовые символы и знак подчеркивания
//			['username', 'match', 'pattern' => '/^[a-z]\w*$/i'],
//
//			// проверяет, является ли "salary" числом
////			['salary', 'number'],
//
//			// обозначает "description" как safe атрибут
//			['description', 'safe'],
//
//			// проверяет, что "username" это строка с длиной от 4 до 24 символов
//			['username', 'string', 'length' => [4, 24]],
//			// обрезает пробелы вокруг "username" и "email"
//			[['username', 'email'], 'trim'],
//
//			// a1 должен быть уникальным в столбце, который представляет "a1" атрибут
//			['phone', 'unique'],
//
//			// a1 должен быть уникальным, но для проверки на уникальность
//			// будет использован столбец a2
//			['a1', 'unique', 'targetAttribute' => 'a2'],
//
//			// a1 и a2 вместе должны быть уникальны, и каждый из них
//			// будет получать сообщения об ошибке
				[['name', 'phone'], 'unique', 'on'=>'default'],
//
//			// a1 и a2 вместе должны быть уникальны, но только a1 будет получать сообщение об ошибке
//			['a1', 'unique', 'targetAttribute' => ['a1', 'a2']],
//
//			// a1 должен быть уникальным, что устанавливается проверкой уникальности a2 и a3
//			// (используя значение a1)
//			['a1', 'unique', 'targetAttribute' => ['a2', 'a1' => 'a3']],
		];
	}


}
?>