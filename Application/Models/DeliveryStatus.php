<?php
	
	namespace Application\Models;
	class DeliveryStatus extends \Application\Classes\Model
	{
		public function __construct()
		{
			parent::__construct('delivery_status', $this->getTypes(), $this);
		}
		
		public function getTypes()
		{
			return array(
				'id'   => 'int',
				'type' => 'str'
			);
		}
		
		public static function getStatusById($id)
		{
			$model = new DeliveryStatus();
			$status = $model->where(['id' => $id])->fetchOne();
			
			if ( $status ) {
				return $status['type'];
			}
			
			return false;
		}
		
		public static function find($filter = [], $fetchOne = false)
		{
			$model = new DeliveryStatus();
			if ( count($filter) > 0 ) $model->where($filter);
			
			if ( $fetchOne ) {
				return $model->fetchOne();
			}
			return $model->fetchAll();
		}
	}