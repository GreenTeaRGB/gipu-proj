<?php

  namespace Application\Models;
  class Slider extends \Application\Classes\Model
  {
    public function __construct()
    {
      parent::__construct( 'slider', $this->getTypes(), $this );
    }

    public function getTypes()
    {
      return [
          'id'           => 'int',
          'active'       => 'int',
          'sort'         => 'int',
          'image'        => 'str',
          'link'         => 'str',
          'text_cursive' => 'str',
          'text'         => 'str',
          'text_bold'    => 'str',

      ];
    }

    public function getFields()
    {
      return array(
          'id' => array(
              'type'    => 'string',
              'visible' => true,
              'edited'  => false,
              'label'   => 'ID'
          ),

          'active' => array(
              'type'     => 'string',
              'visible'  => false,
              'edited'   => false,
              'label'    => 'active',
              'required' => false,
              'multiple' => false,
          ),

          'sort' => array(
              'type'     => 'string',
              'visible'  => false,
              'edited'   => false,
              'label'    => 'sort',
              'required' => false,
              'multiple' => false,
          ),

          'image' => array(
              'type'      => 'file',
              'visible'   => false,
              'edited'    => true,
              'label'     => 'Изображение',
              'required'  => false,
              'file-type' => 'image/*',
              'multiple'  => false,
          ),

          'link' => array(
              'type'     => 'string',
              'visible'  => false,
              'edited'   => true,
              'label'    => 'Ссылка',
              'required' => false,
              'multiple' => false,
          ),

          'text_cursive' => array(
              'type'     => 'string',
              'visible'  => true,
              'edited'   => true,
              'label'    => 'Курсивный текст',
              'required' => false,
              'multiple' => false,
          ),

          'text' => array(
              'type'     => 'string',
              'visible'  => true,
              'edited'   => true,
              'label'    => 'Текст',
              'required' => false,
              'multiple' => false,
          ),

          'text_bold' => array(
              'type'     => 'string',
              'visible'  => true,
              'edited'   => true,
              'label'    => 'Жирный текст',
              'required' => false,
              'multiple' => false,
          ),

      );
    }
	
	  public function rules()
	  {
		  return [
			  [ [ "link", "text_cursive", "text", "text_bold" ], 'string' ],
			  [ [ "active", "sort" ], 'number' ],
			  [ [ "image" ], 'image', 'extensions' => [ 'png', 'jpg', 'jpeg' ] ],
			  [ [ "link" ], 'required' ],
		
		  ];
	  }

    public function attributeLabels()
    {
      return [
          'active'       => 'active',
          'sort'         => 'sort',
          'image'        => 'Изображение',
          'link'         => 'Ссылка',
          'text_cursive' => 'Курсивный текст',
          'text'         => 'Текст',
          'text_bold'    => 'Жирный текст',

      ];
    }

    public function formFields( array $data = [] )
    {
      $fields = [
          'active'       => [ 'type' => 'text', 'class' => [ 'form-control' ] ],
          'sort'         => [ 'type' => 'text', 'class' => [ 'form-control' ] ],
          'image'        => [ 'type' => 'file', 'class' => [ 'form-control' ] ],
          'link'         => [ 'type' => 'text', 'class' => [ 'form-control' ] ],
          'text_cursive' => [ 'type' => 'text', 'class' => [ 'form-control' ] ],
          'text'         => [ 'type' => 'text', 'class' => [ 'form-control' ] ],
          'text_bold'    => [ 'type' => 'text', 'class' => [ 'form-control' ] ],

      ];
      if( count( $data ) > 0 ) {
        $fieldsFilter = [];
        foreach( $data as $datum ) {
          if( isset( $fields[$datum] ) )
            $fieldsFilter[$datum] = $fields[$datum];
        }
        return $fieldsFilter;
      }
      return $fields;
    }

    protected function afterDelete( $model ){
      if(!parent::afterDelete($model)) return;
      $slider = $this->getDeletedItems();
      self::deleteFiles(array_column($slider, 'image'));
    }




}
