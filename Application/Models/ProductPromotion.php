<?php
	
	namespace Application\Models;
	class ProductPromotion extends \Application\Classes\Model
	{
		public function __construct()
		{
			parent::__construct('product_promotion', $this->getTypes(), $this);
		}
		
		public function getTypes()
		{
			return array(
				'id'           => 'int',
				'promotion_id' => 'int',
				'product_id'   => 'int',
			);
		}
	}