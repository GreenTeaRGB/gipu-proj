<?php

  namespace Application\Models;
  use Application\Helpers\DataHelper;

  class Remittance extends \Application\Classes\Model implements \Application\Classes\Forms\Interfaces\FormBuilderInterface
  {
    public function __construct()
    {
      parent::__construct( '', $this->getTypes(), $this );
    }

    public function getTypes()
    {
      return [
          'id'        => 'int',
          'user_from' => 'int',
          'user_to'   => 'int',
          'sum'       => 'str',

      ];
    }

    public function getFields()
    {
      return array(
          'id' => array(
              'type'    => 'string',
              'visible' => true,
              'edited'  => false,
              'label'   => 'ID'
          ),

          'user_from' => array(
              'type'     => 'modals',
              'visible'  => true,
              'edited'   => true,
              'label'    => 'Отправитель',
              'required' => false,
              'multiple' => false,
              'class' => '\\Application\\Models\\User',
              'fields' => ['name', 'last_name'],
              'info' => ['name', 'last_name', 'registration_date', 'email', 'phone', 'balance'],
              'where' => ['type'=>DataHelper::DEFAULT_SELLER_TYPE_ID]
          ),

          'user_to' => array(
              'type'     => 'modals',
              'visible'  => true,
              'edited'   => true,
              'label'    => 'Получатель',
              'required' => false,
              'multiple' => false,
              'class' => '\\Application\\Models\\User', // это сам класс
              'fields' => ['name', 'last_name'], // Колонки отображающиеся в модалке
              'info' => ['name', 'last_name', 'registration_date', 'email', 'phone', 'balance'], // поля которые отображаются после выбора
              'where' => ['type'=>DataHelper::DEFAULT_SELLER_TYPE_ID] // условаие выборки для модалки
          ),

          'sum' => array(
              'type'     => 'string',
              'visible'  => true,
              'edited'   => true,
              'label'    => 'Сумма',
              'required' => false,
              'multiple' => false,
          ),

      );
    }

    public function rules():array
    {
      return [
          [ ["user_from", "user_to", "sum"], 'required' ],
          [ [ "user_from", "user_to" ], 'number' ],
          [ [ "sum" ], 'double' ],

      ];
    }

    public function attributeLabels():array
    {
      return [
          'user_from' => 'Отправитель',
          'user_to'   => 'Получатель',
          'sum'       => 'Сумма',

      ];
    }

    public function formFields( array $data = [] )
    {
      $fields = [
          'user_from' => [ 'type' => 'text', 'class' => [ 'form-control' ] ],
          'user_to'   => [ 'type' => 'text', 'class' => [ 'form-control' ] ],
          'sum'       => [ 'type' => 'text', 'class' => [ 'form-control' ] ],

      ];
      if( count( $data ) > 0 ) {
        $fieldsFilter = [];
        foreach( $data as $datum ) {
          if( isset( $fields[$datum] ) )
            $fieldsFilter[$datum] = $fields[$datum];
        }
        return $fieldsFilter;
      }
      return $fields;
    }


  }
