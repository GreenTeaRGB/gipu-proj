<?php

namespace Application\Models;

use Application\Helpers\DataHelper;
use Application\Helpers\Image;
use Application\Helpers\UserHelper;

class User extends \Application\Classes\Model implements \Application\Classes\Forms\Interfaces\FormBuilderInterface
{

  public function __construct()
  {
    parent::__construct( 'user', $this->getTypes(), $this );
  }

  public function getTypes()
  {
    return array(
      'id'                => 'int',
      'registration_date' => 'int',
      'type'              => 'int',
      'region'            => 'int',
      'city'              => 'int',
      'phone'             => 'str',
      'email'             => 'str',
      'name'              => 'str',
      'last_name'         => 'str',
      'password'          => 'str',
      'restrict_id'       => 'int',
      'last_activity'     => 'int',

      'notify_by_email'    => 'int',
      'notify_by_phone'    => 'int',
      'passport_data'      => 'str',
      'balance'            => 'str',
      'active'             => 'int',
      'referral_id'        => 'int',
      'referral_level'     => 'int',
      'status_id'          => 'int',
      'seller_type'        => 'int',
      'juristic_documents' => 'str',
      'juristic_type_id'   => 'int',
      'juristic_data_id'   => 'int',
      'secret_question'    => 'str',
      'secret_answer'      => 'str',
      'user_group_id'      => 'int',
      'avatar'             => 'str',
      'active'             => 'int',
      'position'           => 'str',
      'access'             => 'str',
    );
  }

  public function getFields()
  {
    return [
      'id'                 => [
        'type'     => 'read',
        'visible'  => false,
        'edited'   => false,
        'label'    => 'ID',
        'required' => false,
        'sort'     => 100,
        'dsort'     => 100,
      ],
      'registration_date'  => [
        'type'     => 'date',
        'visible'  => false,
        'edited'   => false,
        'label'    => 'Дата регистрации',
        'required' => false,
        'sort'     => 200,
        'dsort'     => 200,
      ],
      'type'               => [
        'type'     => 'select',
        'visible'  => false,
        'edited'   => false,
        'values'   => \Application\Models\UserType::find(),
        'id_key'   => 'id',
        'name_key' => 'type',
        'label'    => 'Тип',
        'required' => false,
        'sort'     => 300,
        'dsort'     => 300,
      ],
      //      'user_group_id'      => [
      //        'type'     => 'select',
      //        'visible'  => false,
      //        'edited'   => true,
      //        'values'   => \Application\Models\UserGroup::find(),
      //        'id_key'   => 'id',
      //        'name_key' => 'name',
      //        'label'    => 'Права доступа',
      //        'required' => false,
      //        'sort'     => 400,
      //      ],
      'access'             => [
        'type'     => 'selectTree',
        'visible'  => false,
        'edited'   => true,
        'values'   => \Application\Models\AdminPage::find( [ 'visible' => 1 ] ),
        'id_key'   => 'id',
        'multiple' => true,
        'name_key' => 'name',
        'label'    => 'Права доступа',
        'required' => false,
        'sort'     => 400,
        'dsort'     => 10000,
      ],
      'region'             => [
        'type'     => 'string',
        'visible'  => false,
        'edited'   => false,
        'label'    => 'Регион',
        'required' => true,
        'sort'     => 500,
        'dsort'     => 500,
      ],
      'city'               => [
        'type'     => 'string',
        'visible'  => false,
        'edited'   => false,
        'label'    => 'Город',
        'required' => false,
        'sort'     => 600,
        'dsort'     => 600,
      ],
      'phone'              => [
        'type'     => 'string',
        'visible'  => true,
        'edited'   => true,
        'label'    => 'Телефон',
        'required' => false,
        'sort'     => 5,
        'dsort'     => 5,
      ],
      'email'              => [
        'type'     => 'string',
        'visible'  => true,
        'edited'   => true,
        'label'    => 'E-mail',
        'required' => false,
        'sort'     => 700,
        'dsort'     => 700,
      ],
      'name'               => [
        'type'     => 'string',
        'visible'  => true,
        'edited'   => true,
        'label'    => 'Имя',
        'required' => false,
        'sort'     => 1,
        'dsort'     => 1,
      ],
      'last_name'          => [
        'type'     => 'string',
        'visible'  => false,
        'edited'   => false,
        'label'    => 'Фамилия',
        'required' => false,
        'sort'     => 800,
        'dsort'     => 800,
      ],
      'password'           => [
        'type'     => 'string',
        'visible'  => false,
        'edited'   => true,
        'label'    => 'Пароль',
        'required' => false,
        'sort'     => 900,
        'dsort'     => 900,
      ],
      'restrict_id'        => [
        'type'     => 'select',
        'visible'  => false,
        'edited'   => false,
        'values'   => \Application\Models\Restriction::getRestrictions(),
        'id_key'   => 'id',
        'name_key' => 'type',
        'label'    => 'Ограничения',
        'required' => false,
        'sort'     => 1000,
        'dsort'     => 1000,
      ],
      'last_activity'      => [
        'type'     => 'datetime',
        'visible'  => false,
        'edited'   => false,
        'label'    => 'Последняя активность',
        'required' => false,
        'sort'     => 1100,
        'dsort'     => 1100,
      ],
      'notify_by_email'    => [
        'type'     => 'checkbox',
        'visible'  => false,
        'edited'   => false,
        'label'    => 'Подтверджение email',
        'required' => false,
        'sort'     => 1200,
        'dsort'     => 1200,
      ],
      'notify_by_phone'    => [
        'type'     => 'checkbox',
        'visible'  => false,
        'edited'   => false,
        'label'    => 'Подтверждение телефона',
        'required' => false,
        'sort'     => 1300,
        'dsort'     => 1300,
      ],
      'passport_data'      => [
        'type'     => 'string',
        'visible'  => false,
        'edited'   => false,
        'label'    => 'Данные паспорта',
        'required' => false,
        'sort'     => 1400,
        'dsort'     => 1400,
      ],
      'balance'            => [
        'type'     => 'string',
        'visible'  => false,
        'edited'   => false,
        'label'    => 'Баланс',
        'required' => false,
        'sort'     => 1500,
        'dsort'     => 1500,
      ],
      'referral_id'        => [
        'type'     => 'select',
        'visible'  => false,
        'edited'   => false,
        'values'   => User::find(),
        'id_key'   => 'id',
        'name_key' => 'name',
        'label'    => 'Реферал',
        'required' => false,
        'sort'     => 1600,
        'dsort'     => 1600,
      ],
      'referral_level'     => [
        'type'     => 'string',
        'visible'  => false,
        'edited'   => false,
        'label'    => 'Звание',
        'required' => false,
        'sort'     => 1700,
        'dsort'     => 1700,
      ],
      'status_id'          => [
        'type'     => 'select',
        'visible'  => false,
        'edited'   => false,
        'values'   => \Application\Models\UserStatus::getUserStatuses(),
        'id_key'   => 'id',
        'name_key' => 'type',
        'label'    => 'Статус',
        'required' => false,
        'sort'     => 1800,
        'dsort'     => 1800,
      ],
      'seller_type'        => [
        'type'     => 'string',
        'visible'  => false,
        'edited'   => false,
        'label'    => 'Тип продавца',
        'required' => false,
        'sort'     => 1900,
        'dsort'     => 1900,
      ],
      'juristic_documents' => [
        'type'     => 'string',
        'visible'  => false,
        'edited'   => false,
        'label'    => 'Юридические документы',
        'required' => false,
        'sort'     => 500,
        'dsort'     => 500,
      ],

      'juristic_type_id' => [
        'type'     => 'select',
        'visible'  => false,
        'edited'   => false,
        'values'   => \Application\Models\JuristicType::getJuristicTypes(),
        'id_key'   => 'id',
        'name_key' => 'type',
        'label'    => 'Форма ответственности',
        'required' => false,
        'sort'     => 2000,
        'dsort'     => 2000,
      ],

      'secret_question'  => [
        'type'     => 'string',
        'visible'  => false,
        'edited'   => false,
        'label'    => 'Секретный вопрос',
        'required' => false,
        'sort'     => 2100,
        'dsort'     => 2100,
      ],
      'secret_answer'    => [
        'type'     => 'string',
        'visible'  => false,
        'edited'   => false,
        'label'    => 'Секретные ответ',
        'required' => false,
        'sort'     => 2200,
        'dsort'     => 2200,
      ],
      'avatar'           => [
        'type'      => 'file',
        'visible'   => false,
        'edited'    => false,
        'required'  => false,
        'label'     => 'Аватар',
        'multiple'  => false,
        'file-type' => 'image/*',
        'sort'      => 2300
      ],
      'juristic_data_id' => [
        'type'     => 'select',
        'visible'  => false,
        'edited'   => false,
        'values'   => \Application\Models\JuristicData::find(),
        'id_key'   => 'id',
        'name_key' => 'organization_name',
        'label'    => 'Юр. информация',
        'required' => false,
        'sort'     => 2400,
        'dsort'     => 2400,
      ],
      'active'           => [
        'type'    => 'string',
        'visible' => false,
        'edited'  => false,
        'label'   => 'Отключить',
      ],
      'position'         => [
        'type'     => 'string',
        'visible'  => true,
        'edited'   => false,
        'required' => false,
        'label'    => 'Должность',
        'sort'     => 2500,
        'dsort'     => 2500,
      ]
    ];
  }

  public function attributeLabels(): array
  {
    return [
      'login'             => 'Логин',
      'password'          => 'Пароль',
      'password_repeat'   => 'Повторите пароль',
      'organization_name' => 'Наименование',
      'juristic_address'  => 'Юр. адрес',
      'contact_name'      => 'Контактное лицо',
      'referral_id'       => 'Реферал',
      'region'            => 'Город',
      'phone'             => 'Номер телефона',
      'email'             => 'E-mail',
      'balance'           => 'Баланс',
      'registration_date' => 'Дата регистрации',
      'name'              => 'Имя',
      'last_name'         => "Фамилия",

    ];
  }

  public function formFields( array $data = [] )
  {
    $fields = [
      'login'    => [ 'type' => 'text', 'label' => 'Логин', 'attr' => [ 'placeholder' => 'Телефон или Email' ] ],
      'password' => [ 'type' => 'password', 'label' => 'Пароль', 'attr' => [ 'placeholder' => '********' ] ],
      'submit'   => [
        'type'  => 'submit',
        'label' => 'Вход',
        'attr'  => [ 'class' => 'center-block btn btn_red btn_long' ]
      ],
    ];
    if ( count( $data ) > 0 ) {
      $fieldsFilter = [];
      foreach ( $data as $datum ) {
        if ( isset( $fields[$datum] ) )
          $fieldsFilter[$datum] = $fields[$datum];
      }
      return $fieldsFilter;
    }
    return $fields;
  }

  public function rules(): array
  {
    return [
      [
        [ 'email', 'phone' ],
        'required',
        'on'      => 'default',
        'message' => 'Обязательное поле'
      ],
      //      [ 'password', 'required', 'skipOnEmpty' => self::getModule() == 'admin' ? true : false ],
      [ 'email', 'email' ],
      [ [ 'login', 'password' ], 'required', 'on' => 'login', 'message' => 'Обязательное поле' ],
      [
        'password',
        'filter',
        'on'     => 'login',
        'filter' => function ( $value ) {
          $value = UserHelper::hashPassword( $value );
          return $value;
        }
      ],
      [ [ 'email', 'phone' ], 'unique' ],
      [ 'password', 'compare', 'except' => [ 'login', 'default' ], 'on' => 'register' ],
      [ [ 'password', 'email', 'phone', 'contact_name' ], 'required', 'on' => 'register' ],
      [
        [ 'login', 'password' ],
        'exist',
        'on'              => 'login',
        'targetAttribute' => [ 'login' => [ 'phone', 'email' ], 'password' ],
        'type'            => 'or',
        'message'         => 'Не правильный логин или пароль'
      ],
      [ 'phone', 'string', 'min' => 12 ],
      //      [
      //        'phone',
      //        'match',
      //        'pattern'                => '/[\+]\s\d{1}\s\d{3}\s\d{3}[\-]\d{2}[\-]\d{2}/',
      //        'except'                 => 'update_in_cabiten'
      //      ],
      [
        [
          'juristic_data_id',
          'juristic_type_id',
          'referral_id',
          'restrict_id',
          'referral_level',
          'city',
          'type',
          'region',
          'status_id',
          'balance'
        ],
        'number',
        'skipOnEmpty' => true
      ],
      [ [ 'registration_date', 'last_activity' ], 'date' ],
      [
        [
          'organization_name',
          'juristic_address',
          'contact_name',
          'phone',
          'referral_level',
          'last_name',
          'name',
          'passport_data',
          'seller_type',
          'juristic_documents',
          'secret_question',
          'secret_answer',
        ],
        'safe',
      ],
      [ [ 'phone' ], 'safe', 'on' => 'update_in_cabiten' ],
    ];
  }

  public function afterFetchAll( $data )
  {
    if ( self::getModule() == 'admin' ) {
      if ( isset( $data[DataHelper::SUPPORT_ID] ) )
        unset( $data[DataHelper::SUPPORT_ID] );
    }
    foreach ( $data as $index => $user ) {
      $user[$index]['password'] = '';
    }
    return $data;
  }

  public function afterFetchOne( $data )
  {
//    if ( self::getModule() == 'admin' ) return $data;
//    if ( isset( $data['password'] ) ) {
    if ( $data )
      $data['password'] = '';
//    }
    return $data;
  }

  protected function getRegistrationDate( $element )
  {
    return date( 'd.m.Y', $element );
  }

  protected function getLastActivity( $element )
  {
    return date( 'd.m.Y H:i:s', $element );
  }

  protected function setRegistrationDate( $element )
  {
    return strtotime( $element );
  }

  protected function setLastActivity( $element )
  {
    return strtotime( $element );
  }

  protected function setAccess( $element )
  {
    return self::serialize( $element );
  }

  protected function getAccess( $element )
  {
    return self::unserialize( $element );
  }

  public static function create( $data )
  {
    $model = new User();
    return $model->setFields( $data )->insert();
  }

  protected function getAvatar( $element )
  {
    if ( self::getModule() == 'admin' ) return $element;
    return $element == '' ? '/images/no_photo.png' : $element;
  }

  public function registerForm()
  {
//    $userTypeModel = new UserType();
//    $userTypes     = $userTypeModel->fetchAll();
    $regions = DataHelper::getRegions();

    return [
      'juristic_address' => [
        'type' => 'text',
        'attr' => [ 'class' => 'form-control', 'placeholder' => 'ул. Некрасова 10' ]
      ],
      'contact_name'     => [
        'type' => 'text',
        'attr' => [ 'class' => 'form-control', 'placeholder' => 'Крылов Иван' ]
      ],
      'phone'            => [
        'type' => 'tel',
        'attr' => [ 'class' => 'form-control', 'placeholder' => '+7 (999) 123 45 67' ]
      ],
      'email'            => [
        'type' => 'text',
        'attr' => [ 'class' => 'form-control', 'placeholder' => 'ivan@email.ru' ]
      ],
      'password'         => [
        'type'    => 'password',
        'attr'    => [ 'class' => 'form-control input_short', 'placeholder' => '' ],
        'confirm' => true
      ],
      'password_repeat'  => [
        'type'    => 'password',
        'attr'    => [ 'class' => 'form-control input_short', 'placeholder' => '' ],
        'confirm' => true
      ],
      'region'           => [
        'type'   => 'select',
        'attr'   => [ 'class' => 'form-control', 'placeholder' => '' ],
        'values' => $regions,
        'id'     => 'id',
        'name'   => 'name'
      ],
      'referral_id'      => [
        'type' => 'text',
        'attr' => [ 'class' => 'form-control', 'placeholder' => '123456' ]
      ],
      'submit'           => [
        'type'  => 'submit',
        'label' => 'Регистрация',
        'attr'  => [ 'class' => 'center-block btn btn_red btn_long' ]
      ],
      //'type'      => ['type' => 'select', 'values' => $userTypes, 'id' => 'id', 'name' => 'type'],
      //				'last_name'        => ['type' => 'text', 'attr' => ['class' => 'form-control']],
      //				'email'=>['type' => 'text', 'default' => 'test', 'attr'=>['class'=>'form-control']],
      //				'phone' => ['type' => 'text', 'attr'=>['class'=>'form-control']],
      //				'groups' => ['type' => 'select', 'values'=>DataHelper::USER_TYPES, 'id'=>'id', 'name'=>'type'],
      //				'active' => ['type' => 'checkbox', 'values'=>DataHelper::USER_TYPES, 'id'=>'id', 'name'=>'type'],
      //				'file' => ['type'=>'file', 'label'=>'Фаил', 'multi'=>true]
    ];
  }

  protected function beforeInsert( array $data ): array
  {

    $unixTime                  = $this->getUnixTime();
    $data['registration_date'] = $unixTime;
    $data['restrict_id']       = DataHelper::DEFAULT_RESTRICTION_ID;
    $data['password']          = md5( DataHelper::SALT . $data['password'] );

    return $data;
  }

  protected function beforeUpdate( array $data ): array
  {
    if ( isset( $data['password'] ) && $data['password'] != '' ) {
      $data['password'] = UserHelper::hashPassword( $data['password'] );
    }
    return $data;
  }

  public static function getUserByLoginAndPass( $login, $password )
  {
    $model = new User();

    $sql
      = "
				SELECT
					u.*,
					ut.`type` as user_type,
					ut.`type_code`
				FROM `user` as u
					LEFT JOIN `user_type` as ut ON u.`type` = ut.`id`
				WHERE (u.`email` = '$login' OR u.`phone` = '$login')
				AND u.`password` = '$password'
			";

    $user = $model->sql( $sql )->fetchOne();

    if ( $user ) {
      $user = self::loadAdditional( $user );
    }

    return $user;
  }

  public static function getById( $id )
  {
    $model = new User();
    if ( !self::updateById( $id, [ 'last_activity' => date( 'd.m.Y H:i:s', time() ) ] ) ) return false;
    $sql
      = "
				SELECT
					u.*,
					ut.`type` as user_type,
					ut.`type_code`
				FROM `user` as u
					LEFT JOIN `user_type` as ut ON u.`type` = ut.`id`
				WHERE u.`id` = '$id'
			";

    $user = $model->sql( $sql )->fetchOne();

    if ( $user ) {
      $user = self::loadAdditional( $user );
    }

    return $user;
  }

  public static function find( $filter = [] )
  {
    $model = new User();
    if ( count( $filter ) > 0 ) {
      $model->where( $filter );
    }

    return $model->fetchAll();
  }

  public static function loadAdditional( $user )
  {
    $user['user_status'] = UserStatus::getStatusById( (int)$user['id'] );
    return $user;
  }

  public function afterDelete( $model )
  {
    if ( !parent::afterDelete( $model ) ) return;
    $user = $this->getDeletedItems();
    $ids  = array_column( $user, 'id' );
    //Если пользователь был продавцом, то удаляем из таблицы shop
    $model_shop = new Shop();
    $model_shop->setFields( [ 'seller_id' => "0", 'status' => "5" ] )->where( [ 'seller_id' => $ids ] )->update();

    self::removeFiles( array_column( $user, 'avatar' ) );
    JuristicData::deleteFilter( [ 'id' => array_column( $user, 'juristic_data_id' ) ] );
    //Сделано через связи
    //Очищаем диалоги
//		$modelDialog = new Dialog();
//		$modelDialog->where(['sender_id'=>$ids])->orWhere(['receiver_id'=>$ids])->delete();

    //Удаляем обзоры
//		Review::deleteFilter(['buyer_id'=>$ids]);
//		ReviewLike::deleteFilter(['user_id'=>$ids]);

    //Удаляем лист покупок
//		Wishlist::deleteFilter(['user_id'=>$ids]);


  }


  private static $max_rank = 0;

  public static function regProfit( $id, $sum, $level = 1 )
  {
    $user        = self::findOne( $id );
    $referral_id = $user['referral_id'];

    if ( $referral_id != 0 ) {
      $referral = self::findOne( $referral_id );
      $rank     = $referral['referral_level'];
      if ( $referral && ( self::$max_rank <= $rank ) ) {
        self::$max_rank = $rank;
        if ( $level == 1 ) $bonus = DataHelper::DEPTH_ONE_PERCENT * $sum;
        if ( $level == 2 ) $bonus = DataHelper::DEPTH_TWO_PERCENT * $sum;
        if ( $level > 2 ) {
          $percent = DataHelper::getPercentRegByRank( $rank );
          $bonus   = $percent * $sum;
        }
        if ( $bonus != 0 ) {
          self::updateById( $referral['id'], [ 'balance' => $referral['balance'] + $bonus ] );
        }
        self::checkRank( $referral['id'], $referral['referral_level'] );
      }
      $level++;
      self::regProfit( $referral['id'], $sum, $level );
    }
    return true;
  }

  private static function checkRank( $id, $rank )
  {
    $count = self::getCount( [ 'id' => $id, 'referral_level' => $rank ] );
    if ( $count >= 3 ) {
      $rank++;
      if ( $rank <= 5 ) {
        self::updateById( $id, [ 'referral_level' => $rank ] );
      }
    }
  }

  public static function getRegProfit( $id, $level = 1, $data = [])
  {
    $user        = self::findOne( $id );
    $referral_id = $user['referral_id'];

    if ( $referral_id != 0 ) {
      $referral = self::findOne( $referral_id );
      $rank     = $referral['referral_level'];
      if ( $referral && ( self::$max_rank <= $rank ) ) {
        self::$max_rank = $rank;
        if ( $level == 1 ) $percent = DataHelper::DEPTH_ONE_PERCENT;
        if ( $level == 2 ) $percent = DataHelper::DEPTH_TWO_PERCENT;
        if ( $level > 2 ) {
          $percent = DataHelper::getPercentRegByRank( $rank );
        }
        $percentRank = DataHelper::getPercentByRank($referral['referral_level']);
      }
      $level++;
      $res = self::getRegProfit( $referral['id'], $level, $data );
      if(isset($res[0]))
      $data[] = $res[0];
      if(isset($percent) && isset($percentRank))
      $data[] = $referral_id.', '. ($percent*100).', '.($percentRank * 100)."<br>";
      return $data;
    }
    return '';
  }

}