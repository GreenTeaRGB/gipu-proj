<?php

namespace Application\Models;

use Application\Classes\Session;
use Application\Helpers\DataHelper;
use Application\Helpers\UserHelper;

class Dialog extends \Application\Classes\Model
{
  public function __construct()
  {
    parent::__construct( 'dialog', $this->getTypes(), $this );
  }

  public function getTypes()
  {
    return array(
      'id'          => 'int',
      'sender_id'   => 'int',
      'receiver_id' => 'int',
    );
  }

  public static function find( $filter = [], $fetchOne = false )
  {
    $model = new Dialog();
    if ( count( $filter ) > 0 ) $model->where( $filter );

    if ( $fetchOne ) {
      return $model->fetchOne();
    }
    return $model->fetchAll();
  }

  protected function afterFetchAll( $dialogs )
  {
    $user = UserHelper::getUser();
    if ( $user && $dialogs && count( $dialogs ) > 0 ) {

      $senderIds = array_unique( array_column( $dialogs, 'sender_id' ) );
      $receiverIds = array_unique( array_column( $dialogs, 'receiver_id' ) );

      $resultUsersIds = [];
      foreach ( $senderIds as $senderId ) {
        if ( $senderId != $user['id'] ) {
          $resultUsersIds[] = $senderId;
        } else {
          continue;
        }
      }

      foreach ( $receiverIds as $receiverId ) {
        if ( $receiverId != $user['id'] ) {
          $resultUsersIds[] = $receiverId;
        } else {
          continue;
        }
      }

      $userModel = new User();
      $companions = $userModel->where( [ 'id' => $resultUsersIds ] )->fetchAll();

      $dialogIds = array_column( $dialogs, 'id' );

      $messageModel = new Message();

//      $lastMsgId = Session::getByKey( 'last_msg_id' );
      $messageModel->order( [ 'id' => 'desc' ] )->where( [ 'dialog_id' => $dialogIds ] )->limit(DataHelper::DEFAULT_LOAD_MESSAGE_COUNT);

//      if ( $lastMsgId ) {
//        $messageModel->andWhere( [ '>id' => $lastMsgId ] );
//      }

      $messages = $messageModel->fetchAll();
    
      $messagesByDialogId = [];

      if ( $messages && count( $messages ) > 0 ) {
        $messages = array_reverse( $messages );

        $messagesByDialogId = [];

        foreach ( $messages as $message ) {
          $messagesByDialogId[$message['dialog_id']][] = $message;
        }

        Session::store( [ 'last_msg_id' => $messages[count( $messages ) - 1]['id'] ] );
      }

      $companionById = [];
      foreach ( $companions as $companion ) {
        $companionById[$companion['id']] = [
          'id'            => $companion['id'],
          'name'          => $companion['name'],
          'avatar'        => $companion['avatar'],
          'last_activity' => $companion['last_activity'],
        ];
      }

      foreach ( $dialogs as $key => $dialog ) {
        $uid = ( $dialog['sender_id'] == $user['id'] ) ? $dialog['receiver_id'] : $dialog['sender_id'];
        if ( isset( $companionById[$uid] ) && $companionById[$uid] ) {
          $dialogs[$key]['companion'] = $companionById[$uid];
        }

        if ( isset( $messagesByDialogId[$dialog['id']] ) && $messagesByDialogId[$dialog['id']] ) {
          $dialogs[$key]['messages'] = $messagesByDialogId[$dialog['id']];
        }else{
          $dialogs[$key]['messages'] = [];
        }

      }
    }
    return $dialogs;
  }

//		public function afterDelete( $model )
//		{
//      if(!parent::afterDelete($model)) return;
//			$dialog = $this->getDeletedItems();
//			$ids = array_column($dialog, 'id');
//			Message::deleteFilter(['id'=>$ids]);
//		}
}