<?php
	
	namespace Application\Models;
	use Application\Helpers\DataHelper;
	use Application\Helpers\UserHelper;
	
	class ReportDaily extends \Application\Classes\Model implements \Application\Classes\Forms\Interfaces\FormBuilderInterface
	{
		public function __construct()
		{
			parent::__construct('report_Daily', $this->getTypes(), $this);
		}
		
		public function getTypes()
		{
			return [
				'created_at' => 'str',
			];
		}
		
		public function getFields()
		{
			return array(
				'created_at' => array(
					'type' => 'date', 'visible' => false, 'edited' => true, 'label' => 'Дата создания', 'required' => true, 'multiple' => false,
				),
			);
		}
		
		public function rules(): array
		{
			return [
				[["created_at"], 'required'],
			];
		}
		
		public function attributeLabels(): array
		{
			return [
				'created_at' => 'Отчетный день',
			];
		}
		
		public function formFields(array $data = [])
		{
			$fields = [
				'created_at' => ['type' => 'date', 'class' => ['form-control']],
			];
			if ( count($data) > 0 ) {
				$fieldsFilter = [];
				foreach ($data as $datum) {
					if ( isset($fields[$datum]) ) $fieldsFilter[$datum] = $fields[$datum];
				}
				return $fieldsFilter;
			}
			return $fields;
		}
		
		
		protected function setCreatedAt($element)
		{
			return strtotime($element);
		}
		
		
		protected function getCreatedAt($element)
		{
			return date('d.m.Y', $element);
		}
		
		public static function getReportDaily()
		{
			$model = new OrderProduct();
//			$user = UserHelper::getUser();
			$sql = "SELECT store_order.total, store_order.count_products, store_order.id as `main_id`
					FROM store_order
					WHERE store_order.created_at > ".(mktime(0, 0, 0, date("m"), date("d")-5, date("Y")))."
					AND store_order.parent_id IS NULL";
			return $model->sql($sql)->fetchAll();
		}
	
		public function getProducts($id){
			$model = new Product();
//			$user = UserHelper::getUser();
			$sql = "SELECT p.name as p_name, op.count, p.price, u.name as u_name, u.last_name, u.phone, u.email
					FROM `product` as `p`JOIN `order_product` as `op` ON op.product_id = p.id
					AND IF (op.main_id = 0, op.order_id = ".$id.", op.main_id = ".$id.")
					JOIN `user` as `u` ON op.buyer_id = u.id";
			return $model->sql($sql)->fetchAll();
		}
	}
