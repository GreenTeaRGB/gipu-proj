<?php

namespace Application\Models;
class Courier extends \Application\Classes\Model implements \Application\Classes\Forms\Interfaces\FormBuilderInterface
{
  public function __construct()
  {
    parent::__construct( 'courier', $this->getTypes(), $this );
  }

  public function getTypes()
  {
    return [
      'id'                  => 'int',
      'user_id'             => 'int',
      'license_plate'       => 'str',
      'passport_photo'      => 'str',
      'drive_license_photo' => 'str',
      'count_order'         => 'int',
      'car_type'            => 'str',
      'mark'                => 'str',
      'status'              => 'str',
    ];
  }

  public function getFields()
  {
    return array(
      'id' => array(
        'type'    => 'string',
        'visible' => true,
        'edited'  => false,
        'sort'    => 1,
        'label'   => 'ID'
      ),

      'user_id' => array(
        'type'     => 'string',
        'class'    => '\Application\Models\User',
        'visible'  => false,
        'edited'   => true,
        'label'    => 'ID пользователя',
        'required' => true,
        'sort'     => 2,
        'id_key' => 'id',
        'name_key' => 'name',
        'multiple' => false,
        'fields'   => [ 'id', 'name', 'last_name' ],
      ),

      'license_plate' => array(
        'type'     => 'string',
        'visible'  => true,
        'edited'   => true,
        'label'    => 'Номер машины',
        'required' => true,
        'sort'     => 4,
        'multiple' => false,
      ),

      'passport_photo' => array(
        'type'      => 'file',
        'visible'   => false,
        'edited'    => true,
        'label'     => 'Фото паспорта',
        'required'  => false,
        'sort'      => 500,
        'file-type' => 'image/*',
        'multiple'  => false,
      ),

      'drive_license_photo' => array(
        'type'      => 'file',
        'visible'   => false,
        'edited'    => true,
        'label'     => 'Фото водительского удостоверения',
        'required'  => false,
        'sort'      => 500,
        'file-type' => 'image/*',
        'multiple'  => false,
      ),
      'count_order'         => [
        'type'     => 'string',
        'visible'  => true,
        'edited'   => true,
        'label'    => 'Всего заказов',
        'required' => false,
        'sort'     => 500,
        'multiple' => false,
      ],
      'car_type'            => [
        'type'     => 'string',
        'visible'  => true,
        'edited'   => true,
        'label'    => 'Тип машины',
        'required' => false,
        'sort'     => 5,
        'multiple' => false,
      ],
      'mark'                => [
        'type'     => 'string',
        'visible'  => true,
        'edited'   => true,
        'label'    => 'Марка машины',
        'required' => false,
        'sort'     => 10,
        'multiple' => false,
      ],
      'status'              => [
        'type'     => 'string',
        'visible'  => true,
        'edited'   => true,
        'label'    => 'Статус',
        'required' => false,
        'sort'     => 980,
        'multiple' => false,
      ]

    );
  }

  public function rules(): array
  {
    return [
      [ [ "license_plate" ], 'string' ],
      //      [ [ "user_id", 'count_order' ], 'number' ],
      [ [ "passport_photo", "drive_license_photo" ], 'image', 'extensions' => [ 'jpg', 'png', 'jpeg' ] ],
      [ [ "user_id", "license_plate" ], 'required' ],
      [ [ "car_type", 'mark', 'status' ], 'string' ]

    ];
  }

  public function attributeLabels(): array
  {
    return [
      'user_id'             => 'user_id',
      'license_plate'       => 'license_plate',
      'passport_photo'      => 'passport_photo',
      'drive_license_photo' => 'drive_license_photo',

    ];
  }

  public function formFields( array $data = [] )
  {
    $fields = [
      'user_id'             => [
        'type'   => 'select',
        'values' => \Application\Models\User::find(),
        'id'     => 'id',
        'name'   => 'name',
        'class'  => [ 'form-control' ]
      ],
      'license_plate'       => [ 'type' => 'text', 'class' => [ 'form-control' ] ],
      'passport_photo'      => [ 'type' => 'file', 'class' => [ 'form-control' ] ],
      'drive_license_photo' => [ 'type' => 'file', 'class' => [ 'form-control' ] ],

    ];
    if ( count( $data ) > 0 ) {
      $fieldsFilter = [];
      foreach ( $data as $datum ) {
        if ( isset( $fields[$datum] ) )
          $fieldsFilter[$datum] = $fields[$datum];
      }
      return $fieldsFilter;
    }
    return $fields;
  }

//    protected function beforeInsert( array $data ):array
//    {
//      $unixTime = $this->getUnixTime();
//
//      return $data;
//    }

  public function afterDelete( $model )
  {
    if ( !parent::afterDelete( $model ) ) return;
    $couriers = $this->getDeletedItems();
    self::removeFiles( array_column( $couriers, 'passport_photo' ) );
    self::removeFiles( array_column( $couriers, 'drive_license_photo' ) );

  }


}
