<?php
	
	namespace Application\Models;
	class Discount extends \Application\Classes\Model
	{
		public function __construct()
		{
			parent::__construct('discount', $this->getTypes(), $this);
		}
		
		public function getTypes()
		{
			return array(
				'id'          => 'int',
				'title'       => 'str',
				'discount'    => 'int',
				'has_expired' => 'int',
			);
		}

    public function getFields(){
      return [
          'id' => [
              'type' => 'string',
              'visible' => true,
              'edited' => false,
              'required' => false,
              'label' => 'ID'
          ],
          'title' => [
              'type' => 'string',
              'visible' => true,
              'edited' => true,
              'required' => false,
              'label' => 'Заголовок'
          ],
          'discount' => [
              'type' => 'string',
              'visible' => true,
              'edited' => true,
              'required' => false,
              'label' => 'Скидка'
          ],
          'has_expired' => [
              'type' => 'checkbox',
              'visible' => true,
              'edited' => true,
              'required' => false,
              'label' => 'закончилась'
          ],
      ];
    }
		
		public function rules()
		{
			return [
				[ [ "title" ], 'string' ],
				[ [ "discount" ], 'number' ],
				[ [ "title", "discount" ], 'required' ],
			];
		}

		public static function find($filter = [], $fetchOne = false)
		{
			$model = new Discount();
			if ( count($filter) > 0 ) $model->where($filter);
			
			if ( $fetchOne ) {
				return $model->fetchOne();
			}
			return $model->fetchAll();
		}
		
		public function afterDelete( $model )
		{
      if(!parent::afterDelete($model)) return;
			$discount = $this->getDeletedItems();
			$ids = array_column($discount, 'id');
			$model = new Product();
			$model->setFields(['sale_id'=>"0"])->where(['sale_id'=>$ids])->update();
		}
	}