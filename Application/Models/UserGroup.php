<?php
namespace Application\Models;
class UserGroup extends \Application\Classes\Model
{
	public function __construct()
	{
		parent::__construct('user_group', $this->getTypes(), $this);
	}

	public function getTypes()
	{
		return [
			'id'          => 'int',
						'name' => 'str',
		];
	}

	public function getFields()
	{
		return array(
			'id'          => array(
				'type'    => 'string',
				'visible' => true,
				'edited'  => false,
				'label'   => 'ID'
			),
					
			'name' => array(
				'type'=>'string',
				'visible'=>true,
				'edited'=>true,
				'label'=>'Название',
				'required'=>false,
				'multiple'=>false,
			),
			'pages' => array(
				'type'=>'mselect',
				'visible'=>false,
				'values' => \Application\Models\AdminPage::find(),
				'id_key' => 'id',
				'name_key' => 'name',
				'edited'=>true,
				'label'=>'Страницы',
				'required'=>false,
				'multiple'=>true,
			),
		);
	}
	
	public function rules() : array
	{
		return array(
			[['name'], 'string'],
			[['name'], 'required'],
		);
	}

	protected function afterFetchOne($data){
		if(isset($data['id']))
			$data['pages'] = \Application\Models\UserGroupRights::getListByGroup($data['id']);
		return $data;
	}

	public static function find($filter = array()){
		$model = new UserGroup();
		if(count($filter) > 0) $model->where($filter);
		return $model->fetchAll();
	}

	public static function findOne($id){
		$model = new UserGroup();
		return $model->where(['id'=>$id])->fetchOne();
	}

	public static function create($data){
		$model = new UserGroup();
		return $model->setFields($data)->insert();
	}

	public static function deleteById($id){
		$model = new UserGroup();
		return $model->where(['id'=>$id])->delete();
	}

	public static function updateById($id, $data){
		$model = new UserGroup();
		return $model->setFields($data)->where(['id'=>$id])->update();
	}
	
	public function afterDelete( $model )
	{
    if(!parent::afterDelete($model)) return;
		$userGroup = $this->getDeletedItems();
		$ids = array_column($userGroup, 'id');
		User::deleteFilter(['user_group_id'=>$ids]);
		User::removeFiles(array_column($userGroup, 'avatar'));
	}
}
