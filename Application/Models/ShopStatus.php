<?php

	namespace Application\Models;
	class ShopStatus extends \Application\Classes\Model
	{
		public function __construct()
		{
			parent::__construct( 'shop_status', $this->getTypes(), $this );
		}

		public function getTypes()
		{
			return [
				'id'     => 'int',
				'active' => 'int',
				'name'   => 'str',
				'code'   => 'str'

			];
		}

		public function getFields()
		{
			return array(
				'id'     => array(
					'type'    => 'string',
					'visible' => true,
					'edited'  => false,
					'label'   => 'ID'
				),
				'active' => array(
					'type'     => 'checkbox',
					'visible'  => false,
					'edited'   => true,
					'label'    => 'Активность',
					'required' => false,
					'multiple' => false,
				),

				'name' => array(
					'type'     => 'string',
					'visible'  => true,
					'edited'   => true,
					'label'    => 'Название',
					'required' => false,
					'multiple' => false,
				),
				'code' => array(
					'type'     => 'string',
					'visible'  => true,
					'edited'   => true,
					'label'    => 'Код',
					'required' => false,
					'multiple' => false,
				),
			);
		}
		
		public function rules()
		{
			return [
				[ [ "name" ], 'string' ],
				[ [ "active" ], 'number' ],
				[ [ "code", "name" ], 'required' ],
			];
		}

		public function attributeLabels()
		{
			return [
				'name'   => 'Название',
				'active' => 'Активность',

			];
		}

		public function formFields( array $data = [] )
		{
			$fields = [
				'name'   => [ 'type' => 'text', 'class' => [ 'form-control' ] ],
				'active' => [ 'type' => 'checkbox', 'class' => [ 'form-control' ] ],

			];
			if( count( $data ) > 0 ) {
				$fieldsFilter = [];
				foreach( $data as $datum ) {
					if( isset( $fields[ $datum ] ) )
						$fieldsFilter[ $datum ] = $fields[ $datum ];
				}
				return $fieldsFilter;
			}
			return $fields;
		}


	}
