<?php

namespace Application\Models;
class JuristicData extends \Application\Classes\Model
{
  public function __construct()
  {
    parent::__construct( 'juristic_data', $this->getTypes(), $this );
  }

  public function getTypes()
  {
    return array(
      'id'                => 'int',
      'organization_name' => 'str',
      'juristic_address'  => 'str',
      'inn'               => 'int',
      'kpp'               => 'int',
      'checking_account'  => 'str',
      'bik'               => 'int',
      'ks'                => 'str',
      'contact_name'      => 'str',
      'bank_name'         => 'str',
      'skan_registration' => 'str',
      'skan_passport'     => 'str',
      'address_bank'      => 'str',
      'swift'             => 'str',
      'rs'                => 'str',
      'ogrn'              => 'str',
    );
  }

  public function getFields()
  {
    return [
      'id'                => [
        'type'     => 'string',
        'visible'  => false,
        'edited'   => false,
        'required' => false,
        'sort'     => 500,
        'label'    => 'id',
      ],
      'organization_name' => [
        'type'     => 'string',
        'visible'  => false,
        'edited'   => true,
        'required' => false,
        'sort'     => 500,
        'label'    => 'Наименование организации',
      ],
      'juristic_address'  => [
        'type'     => 'string',
        'visible'  => true,
        'edited'   => true,
        'required' => false,
        'sort'     => 500,
        'label'    => 'адрес',
      ],
      'inn'               => [
        'type'     => 'string',
        'visible'  => false,
        'edited'   => true,
        'required' => true,
        'sort'     => 500,
        'label'    => 'ИНН',
      ],
      'kpp'               => [
        'type'     => 'string',
        'visible'  => false,
        'edited'   => true,
        'required' => false,
        'sort'     => 500,
        'label'    => 'КПП',
      ],
      'checking_account'  => [
        'type'     => 'string',
        'visible'  => false,
        'edited'   => true,
        'required' => false,
        'sort'     => 500,
        'label'    => 'Проверен',
      ],
      'bik'               => [
        'type'     => 'string',
        'visible'  => false,
        'edited'   => true,
        'required' => false,
        'sort'     => 500,
        'label'    => 'БИК',
      ],
      'ks'                => [
        'type'     => 'string',
        'visible'  => false,
        'edited'   => true,
        'required' => false,
        'sort'     => 500,
        'label'    => 'КС',
      ],
      'contact_name'      => [
        'type'     => 'string',
        'visible'  => true,
        'edited'   => true,
        'required' => false,
        'sort'     => 2,
        'label'    => 'Контактное лицо',
      ],
      'bank_name'         => [
        'type'     => 'string',
        'visible'  => false,
        'edited'   => true,
        'required' => false,
        'sort'     => 500,
        'label'    => 'Название банка'
      ],
      'skan_registration' => [
        'type'      => 'file',
        'visible'   => false,
        'edited'    => true,
        'required'  => false,
        'label'     => 'Скан свидетельства о регистрации ип',
        'multiple'  => false,
        'sort'      => 500,
        'file-type' => 'image/*',
      ],
      'skan_passport'     => [
        'type'      => 'file',
        'visible'   => false,
        'edited'    => true,
        'required'  => false,
        'file-type' => 'image/*',
        'label'     => 'Скан копия паспорта',
        'sort'      => 500,
        'multiple'  => false
      ],
      'address_bank'      => [
        'type'     => 'string',
        'visible'  => false,
        'edited'   => true,
        'required' => false,
        'sort'     => 500,
        'label'    => 'Адрес банка'
      ],
      'swift'             => [
        'type'     => 'string',
        'visible'  => false,
        'edited'   => true,
        'required' => false,
        'sort'     => 500,
        'label'    => 'Свифт банка'
      ],
      'rs'                => [
        'type'     => 'string',
        'visible'  => false,
        'edited'   => true,
        'required' => false,
        'sort'     => 500,
        'label'    => 'Номер Р/С'
      ],
      'ogrn'              => [
        'type'     => 'string',
        'visible'  => false,
        'edited'   => true,
        'required' => false,
        'label'    => 'ОГРН',
        'sort'     => 500,
      ],
    ];
  }

  public function rules()
  {
    return [
      [
        [
          'organization_name',
          'juristic_address',
          'inn',
          'kpp',
          'checking_account',
          'bik',
          'ks',
          'contact_name',
          'bank_name',
          'skan_registration',
          'skan_passport',
          'address_bank',
          'swift',
          'rs',
          'ogrn',
        ],
        'string'
      ],
      [ [ 'skan_registration', 'skan_passport' ], 'file' ]
      //      [
      //        [
      //          'organization_name',
      //          'juristic_address',
      //          'inn',
      //          'kpp',
      //          'checking_account',
      //          'bik',
      //          'ks',
      //          'contact_name',
      //        ],
      //        'required'
      //      ]
    ];
  }

  public function attributeLabels()
  {
    return [
      'JuristicData' => 'Юр. информация'
    ];
  }


  public static function find( $filter = [] )
  {
    $model = new JuristicData();
    if ( count( $filter ) > 0 )
      $model->where( $filter );
    return $model->fetchAll();
  }
}