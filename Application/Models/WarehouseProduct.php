<?php

  namespace Application\Models;

  use \Application\Models\Warehouse;
  
  class WarehouseProduct extends \Application\Classes\Model
  {
    public function __construct()
    {
      parent::__construct( 'warehouse_product', $this->getTypes(), $this );
    }

    public function getTypes()
    {
      return array(
          'id'           => 'int',
          'warehouse_id' => 'int',
          'product_id'   => 'int',
          'price'        => 'str'
      );
    }
    
    protected function afterFetchAll($data){
      $warehouses = Warehouse::find(['id' => array_column($data, 'warehouse_id')]);
      foreach($data as $key => $relation){
        $data[$key]['name'] = $warehouses[$relation['warehouse_id']]['name'];
      }
      return $data;
    }
    
  }

  ?>