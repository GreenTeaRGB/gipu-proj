<?php

namespace Application\Models;
class Block extends \Application\Classes\Model implements \Application\Classes\Forms\Interfaces\FormBuilderInterface
{
  public function __construct()
  {
    parent::__construct( 'block', $this->getTypes(), $this );
  }

  public function getTypes()
  {
    return [
      'id'       => 'int',
      'name'     => 'str',
      'image'    => 'str',
      'required' => 'int',

    ];
  }

  public function getFields()
  {
    return array(
      'id' => array(
        'type'    => 'string',
        'visible' => true,
        'edited'  => false,
        'label'   => 'ID'
      ),

      'name' => array(
        'type'     => 'string',
        'visible'  => true,
        'edited'   => true,
        'label'    => 'Название',
        'required' => true,
        'multiple' => false,
      ),

      'image' => array(
        'type'      => 'file',
        'visible'   => false,
        'edited'    => true,
        'label'     => 'Изображение',
        'required'  => true,
        'file-type' => 'image/*',
        'multiple'  => false,
      ),

      'required' => array(
        'type'     => 'checkbox',
        'visible'  => false,
        'edited'   => true,
        'label'    => 'Обязательный',
        'required' => false,
        'multiple' => false,
      ),

    );
  }

  public function rules(): array
  {
    return [
      [ [ "name" ], 'required' ],
      [ [ "name" ], 'string' ],
      [ [ "required" ], 'number' ],
      [ [ "image" ], 'image', 'extensions' => [ 'png', 'jpg', 'jpeg' ], 'skipOnEmpty' => isset($_POST['is_image']) ],

    ];
  }

  public function attributeLabels(): array
  {
    return [
      'name'     => 'Название',
      'image'    => 'Изображение',
      'required' => 'Обязательный',

    ];
  }

  public function formFields( array $data = [] )
  {
    $fields = [
      'name'     => [ 'type' => 'text', 'class' => [ 'form-control' ] ],
      'image'    => [ 'type' => 'file', 'class' => [ 'form-control' ] ],
      'required' => [ 'type' => 'checkbox', 'class' => [ 'form-control' ] ],

    ];
    if ( count( $data ) > 0 ) {
      $fieldsFilter = [];
      foreach ( $data as $datum ) {
        if ( isset( $fields[$datum] ) )
          $fieldsFilter[$datum] = $fields[$datum];
      }
      return $fieldsFilter;
    }
    return $fields;
  }

}
