<?php

namespace Application\Models;
class Brands extends \Application\Classes\Model
{
  public function __construct()
  {
    parent::__construct( 'brands', $this->getTypes(), $this );
  }

  public function getTypes()
  {
    return [
        'id'     => 'int',
        'active' => 'int',
        'sort'   => 'int',
        'image'  => 'str',
        'link'   => 'str',
        'name'   => 'str',
    ];
  }

  public function getFields()
  {
    return array(
        'id' => array(
            'type'    => 'string',
            'visible' => true,
            'edited'  => false,
            'label'   => 'ID'
        ),

        'active' => array(
            'type'     => 'string',
            'visible'  => false,
            'edited'   => false,
            'label'    => 'active',
            'required' => false,
            'multiple' => false,
        ),

        'sort' => array(
            'type'     => 'string',
            'visible'  => false,
            'edited'   => false,
            'label'    => 'sort',
            'required' => false,
            'multiple' => false,
        ),

        'image' => array(
            'type'      => 'file',
            'visible'   => false,
            'edited'    => true,
            'label'     => 'Изображение',
            'required'  => false,
            'file-type' => 'image/*',
            'multiple'  => false,
        ),

        'link' => array(
            'type'     => 'string',
            'visible'  => true,
            'edited'   => true,
            'label'    => 'Ссылка',
            'required' => false,
            'multiple' => false,
        ),

        'name' => array(
            'type'     => 'string',
            'visible'  => true,
            'edited'   => true,
            'label'    => 'Название',
            'required' => false,
            'multiple' => false,
        ),

    );
  }
	
	public function rules()
	{
		return [
			[ ["link", "name" ], 'string' ],
			[ [ "image" ], 'image', 'extensions' => [ 'png', 'jpg', 'jpeg' ] ],
			[ [ "active", "sort" ], 'number' ],
			[ [ "name" ], 'required'],
		
		];
	}

  public function attributeLabels()
  {
    return [
        'active' => 'active',
        'sort'   => 'sort',
        'image'  => 'Изображение',
        'link'   => 'Ссылка',
        'name'   => 'Название',

    ];
  }

  public function formFields( array $data = [] )
  {
    $fields = [
        'active' => [ 'type' => 'text', 'class' => [ 'form-control' ] ],
        'sort'   => [ 'type' => 'text', 'class' => [ 'form-control' ] ],
        'image'  => [ 'type' => 'text', 'class' => [ 'form-control' ] ],
        'link'   => [ 'type' => 'text', 'class' => [ 'form-control' ] ],
        'name'   => [ 'type' => 'text', 'class' => [ 'form-control' ] ],

    ];
    if( count( $data ) > 0 ) {
      $fieldsFilter = [];
      foreach( $data as $datum ) {
        if( isset( $fields[$datum] ) )
          $fieldsFilter[$datum] = $fields[$datum];
      }
      return $fieldsFilter;
    }
    return $fields;
  }

  protected function afterDelete( $model ){
    if(!parent::afterDelete($model)) return;
    $brands = $this->getDeletedItems();
    self::removeFiles(array_column($brands, 'image'));
  }


}
