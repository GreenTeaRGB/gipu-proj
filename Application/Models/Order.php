<?php

namespace Application\Models;

use Application\Classes\Session;
use Application\Helpers\DataHelper;
use Delivery;

/**
 * Class Order
 * @package Application\Models
 */
class Order extends \Application\Classes\Model
{
  /**
   * @var null
   */
  protected $deliveryAddress = null;
  /**
   * @var null
   */
  protected $productsByOwner = null;
  /**
   * @var null
   */
  protected $rawProducts = null;
  /**
   * @var int
   */
  protected $countProducts = 0;
  /**
   * @var null
   */
  protected $discount = null;
  /**
   * @var int
   */
  protected $total = 0;
  /**
   * @var int
   */
  protected $buyerId = 0;

  /**
   * Order constructor.
   */
  public function __construct()
  {
    parent::__construct( 'store_order', $this->getTypes(), $this );
  }

  /**
   * @return array|mixed
   */
  public function getTypes()
  {
    return array(
      'id'                 => 'int',
      'parent_id'          => 'int',
      'created_at'         => 'int',
      'updated_at'         => 'int',
      'buyer_id'           => 'int',
      'seller_id'          => 'int',
      'supplier_id'        => 'int',
      'pay_type_id'        => 'int',
      'status_id'          => 'int',
      'pay_date'           => 'int',
      'status_updated_at'  => 'int',
      'delivery_status_id' => 'int',
      'courier_id'         => 'int',
      'count_products'     => 'int',
      'total'              => 'int',
      'delivery_price'     => 'int',
      'discount'           => 'int',
      'name'               => 'str',
      'address'            => 'str',
      'email'              => 'str',
      'phone'              => 'str',
      'comment'            => 'str',
      'internal_account'   => 'int',
      'confirm_internal'   => 'int',
      'delivery_date'      => 'int',
      'delivery_type_id'   => 'int',
    );
  }

  /**
   * @return array
   */
  public function getFields()
  {
    return [
      'id'                 => [
        'type'     => 'string',
        'visible'  => true,
        'edited'   => false,
        'sort'     => 10,
        'required' => false,
        'label'    => 'ID'
      ],
      'parent_id'          => [
        'type'     => 'string',
        'visible'  => false,
        'edited'   => false,
        'sort'     => 1000,
        'required' => false,
        'label'    => 'Родительский заказ'
      ],
      'created_at'         => [
        'type'     => 'date',
        'visible'  => true,
        'edited'   => false,
        'sort'     => 990,
        'required' => false,
        'label'    => 'Дата создания'
      ],
      'updated_at'         => [
        'type'     => 'date',
        'visible'  => false,
        'edited'   => false,
        'sort'     => 1000,
        'required' => false,
        'label'    => 'Дата обновления'
      ],
      'buyer_id'           => [
        'type'     => 'modals',
        'visible'  => false,
        'edited'   => false,
        'sort'     => 1000,
        'required' => false,
        'class'    => '\\Application\\Models\\User',
        'fields'   => [ 'id', 'name', 'last_name' ],
        'info'     => [ 'name', 'last_name', 'registration_date', 'email', 'phone', 'type' ],
        'label'    => 'Покупатель'
      ],
      'seller_id'          => [
        'type'     => 'modals',
        'visible'  => false,
        'edited'   => false,
        'sort'     => 1000,
        'required' => false,
        'class'    => '\\Application\\Models\\User',
        'fields'   => [ 'id', 'name', 'last_name' ],
        'info'     => [ 'name', 'last_name', 'registration_date', 'email', 'phone', 'type' ],
        'label'    => 'Агент'
      ],
      'supplier_id'        => [
        'type'     => 'modals',
        'visible'  => false,
        'edited'   => true,
        'sort'     => 1000,
        'required' => false,
        'class'    => '\\Application\\Models\\User',
        'fields'   => [ 'id', 'name', 'last_name' ],
        'info'     => [ 'name', 'last_name', 'registration_date', 'email', 'phone', 'type' ],
        'label'    => 'Поставщик'
      ],
      'pay_type_id'        => [
        'type'     => 'string',
        'visible'  => false,
        'edited'   => true,
        'sort'     => 1000,
        'required' => false,
        'label'    => 'Тип оплаты'
      ],
      'status_id'          => [
        'type'     => 'select',
        'visible'  => true,
        'edited'   => false,
        'sort'     => 1000,
        'required' => false,
        'values'   => \Application\Models\OrderStatus::find( [ 'active' => DataHelper::DEFAULT_ACTIVE ] ),
        'id_key'   => 'id',
        'name_key' => 'name',
        'label'    => 'Статус',
        'color' => 'color'
      ],
      'pay_date'           => [
        'type'     => 'date',
        'visible'  => false,
        'edited'   => false,
        'sort'     => 1000,
        'required' => false,
        'label'    => 'Дата оплаты'
      ],
      'status_updated_at'  => [
        'type'     => 'date',
        'visible'  => false,
        'edited'   => false,
        'sort'     => 1000,
        'required' => false,
        'label'    => 'Дата изменения статуса'
      ],
      'delivery_date'      => [
        'type'     => 'datetime',
        'visible'  => false,
        'edited'   => true,
        'sort'     => 1000,
        'required' => false,
        'label'    => 'Дата доставки'
      ],
      'delivery_status_id' => [
        'type'     => 'select',
        'visible'  => false,
        'edited'   => false,
        'sort'     => 1000,
        'values'   => DeliveryStatus::find(),
        'id_key'   => 'id',
        'name_key' => 'type',
        'required' => false,
        'label'    => 'Статус доставки'
      ],
      'pay_type_id'        => [
        'type'     => 'select',
        'visible'  => false,
        'edited'   => true,
        'sort'     => 1000,
        'values'   => PayType::find(),
        'id_key'   => 'id',
        'name_key' => 'type',
        'required' => false,
        'label'    => 'Способ оплаты'
      ],
      'courier_id'         => [
        'type'     => 'modals',
        'visible'  => false,
        'edited'   => false,
        'sort'     => 1000,
        'required' => false,
        'class'    => '\\Application\\Models\\Courier',
        'fields'   => [ 'id', 'user_id', 'license_plate' ],
        'label'    => 'ID курьера'
      ],
      'total'              => [
        'type'     => 'string',
        'visible'  => false,
        'edited'   => false,
        'sort'     => 1000,
        'required' => false,
        'label'    => 'Общая стоимость'
      ],
      'discount'           => [
        'type'     => 'string',
        'visible'  => false,
        'edited'   => false,
        'sort'     => 1000,
        'required' => false,
        'label'    => 'Скидка по промокоду'
      ],
      'delivery_price'     => [
        'type'     => 'string',
        'visible'  => false,
        'edited'   => false,
        'sort'     => 1000,
        'required' => false,
        'label'    => 'Стоимость доставки'
      ],
      'count_products'     => [
        'type'     => 'string',
        'visible'  => false,
        'edited'   => false,
        'sort'     => 1000,
        'required' => false,
        'label'    => 'Общее количество продуктов'
      ],
      'name'               => [
        'type'     => 'read',
        'visible'  => false,
        'edited'   => true,
        'sort'     => 1000,
        'required' => false,
        'label'    => 'Представитель'
      ],
      'email'              => [
        'type'     => 'read',
        'visible'  => false,
        'edited'   => true,
        'sort'     => 1000,
        'required' => false,
        'label'    => 'Email'
      ],
      'address'            => [
        'type'     => 'read',
        'visible'  => false,
        'edited'   => true,
        'sort'     => 1000,
        'required' => false,
        'label'    => 'Адрес'
      ],
      'comment'            => [
        'type'     => 'text',
        'visible'  => false,
        'edited'   => false,
        'sort'     => 1000,
        'required' => false,
        'label'    => 'Коментарий'
      ],
      'phone'              => [
        'type'     => 'read',
        'visible'  => false,
        'edited'   => true,
        'sort'     => 1000,
        'required' => false,
        'label'    => 'Телефон'
      ],
      'product'            => [
        'type'     => 'modals',
        'visible'  => false,
        'edited'   => false,
        'sort'     => 1000,
        'required' => false,
        'class'    => '\Application\Models\Product',
        'fields'   => [ 'id', 'name' ],
      ],
      'internal_account'   => [
        'type'     => 'read',
        'visible'  => false,
        'edited'   => false,
        'sort'     => 1000,
        'label'    => "Покупка за внутренний счет",
        'required' => false,
      ],
      'confirm_internal'   => [
        'type'     => 'read',
        'visible'  => false,
        'edited'   => false,
        'sort'     => 1000,
        'required' => false,
        'label'    => "Покупка за внутренний счет разрешена"
      ],
      'link'               => [
        'type'    => 'link',
        'visible' => false,
        'edited'  => true,
        'label'   => 'Назначить курьера',
        'url'     => '/admin/appoint/'
      ],
      'delivery_type_id'   => [
        'type'     => 'select',
        'visible'  => false,
        'edited'   => false,
        'sort'     => 1000,
        'required' => false,
        'values'   => \Application\Models\DeliveryType::find(),
        'id_key'   => 'id',
        'name_key' => 'name',
        'label'    => 'Тип доставки',
      ],
    ];
  }

  /**
   * @return array
   */
  public function attributeLabels()
  {
    return [
      'name'    => 'Представтесь',
      'phone'   => 'Телефон',
      'email'   => 'Почта',
      'address' => 'Адрес доставки',
      'payment' => 'Способ оплаты'
    ];
  }

  /**
   * @return array
   */
  public function rules()
  {
    return [
      [ [ 'name', 'phone', 'email', 'address', 'pay_type_id', 'payment' ], 'required', 'on' => 'order' ],
      [ [ 'name', 'phone', 'email', 'address' ], 'string' ],
      [ 'email', 'email' ]
    ];
  }

  /**
   * @param $element
   * @return false|int
   */
  protected function setCreatedAt( $element )
  {
    return strtotime( $element );
  }

  /**
   * @param $element
   * @return false|int
   */
  protected function setUpdatedAt( $element )
  {
    return strtotime( $element );
  }

  /**
   * @param $element
   * @return false|int
   */
  protected function setPayDate( $element )
  {
    if ( $element == '' ) return $element;
    return strtotime( $element );
  }

  /**
   * @param $element
   * @return false|int
   */
  protected function setStatusUpdatedAt( $element )
  {
    if ( $element == '' ) return $element;
    return strtotime( $element );
  }

  /**
   * @param $element
   * @return false|string
   */
  protected function getStatusUpdatedAt( $element )
  {
    return DataHelper::redate( 'd m Y', $element );
  }

  /**
   * @param $element
   * @return false|string
   */
  protected function getCreatedAt( $element )
  {
    return DataHelper::redate( 'd m Y', $element );
  }

  /**
   * @param $element
   * @return false|string
   */
  protected function getUpdatedAt( $element )
  {
    return DataHelper::redate( 'd m Y', $element );
  }

  /**
   * @param $element
   * @return false|string
   */
  protected function getPayDate( $element )
  {
    return DataHelper::redate( 'd m Y', $element );
  }

  /**
   * @param $element
   * @return false|string
   */
  protected function getDeliveryDate( $element )
  {
    return self::dateFormat( $element, 'd-m-Y H:i:s' );
  }

  /**
   * @param $element
   * @return false|int
   */
  protected function setDeliveryDate( $element )
  {
    return self::strtotime( $element );
  }

  /**
   * @param array $data
   * @return array
   */
  protected function beforeInsert( array $data ): array
  {
    $unixTime                  = $this->getUnixTime();
    $data['created_at']        = $unixTime;
    $data['updated_at']        = $unixTime;
    $data['status_updated_at'] = $unixTime;

    return $data;
  }

  /**
   * @param $data
   * @return mixed
   */
  protected function afterFetchOne( $data )
  {
    /*$data['start'] = date("d.m.Y", $data['start']);
    $data['finish'] = date("d.m.Y", $data['finish']);*/
    $model = new Product();

    if ( isset( $data['id'] ) ) {
      $sql = "
					SELECT prod.*, so.`count_products` as order_count,
						op.`count` as product_count,
						op.properties as product_properties,
						op.buy_price,
						cat.`name` as category
					FROM `store_order` as so
						LEFT JOIN `order_product` as op ON so.`id` = op.`order_id`
						LEFT JOIN `product` as prod ON prod.`id` = op.`product_id`
						LEFT JOIN `category` as cat ON prod.`category_id` = cat.`id`
					WHERE so.`id` = '{$data["id"]}'
				";

      $orderAdditional = $model->sql( $sql )->fetchAll();

      $data['order_additional'] = $orderAdditional;
      $property_value           = PropertyValue::find( [
        'product_id'  => array_column( $orderAdditional, 'id' ),
        '!value_enum' => 0
      ] );
      $sortPropertyValues       = [];
      foreach ( $property_value as $item ) {
        $sortPropertyValues[$item['product_id']][$item['property_id']][$item['id']] = $item;
      }
      $data['property_values'] = $sortPropertyValues;
      $payment                 = PayType::findOne( $data['pay_type_id'] );
      $data['payment_name']    = $payment ? $payment['type'] : 'Неизвестный тип';

      $deliveryType = DeliveryType::findOne( [ 'id' => $data['delivery_type_id'] ] );
      $data['delivery_name'] = $deliveryType ? $deliveryType['name'] : 'Доставка не выбрана';
//      $orderAddressSql = "SELECT oa.* FROM `order_address` as oa WHERE oa.`order_id` = {$data['id']}";
//      $orderAddress    = $model->sql( $orderAddressSql )->fetchOne();
//
//      $data['order_address'] = $orderAddress;
    }
    return $data;
  }

  /**
   * @param $fields
   * @return bool
   */
  public function save( $fields )
  {
    $fields['delivery_price'] = DataHelper::DEFAULT_DELIVERY_PRICE;

    if ( $this->discount && isset( $this->discount['discount'] ) ) {
      $fields['discount'] = $this->discount['discount'];

      $discountModel = new Discount();
      $discountModel->setFields( [ 'has_expired' => 1 ] )->where( [ 'id' => $this->discount['id'] ] )->update();
      Session::remove( 'discount' );
    }

    if ( count( $this->productsByOwner ) > 1 ) {
      $fields['count_products'] = $this->countProducts;
      $fields['total']          = $this->total;

      $orderId = $this->setFields( $fields )->insert();
      $this->saveOrderProducts( $fields, $orderId );
    } else {
      $ownerId             = array_keys( $this->productsByOwner )[0];
      $fields['seller_id'] = $ownerId;

      $countProductsByOwner = 0;
      foreach ( $this->productsByOwner[$ownerId] as $item ) {
        $countProductsByOwner += (int)$item['count'];
      }

      $fields['count_products'] = $countProductsByOwner;
      $fields['total']          = $this->total;

      $orderId = $this->setFields( $fields )->insert();

      $this->saveOrderProducts( $fields, $orderId, true );
    }

    return $orderId;
  }

  /**
   * @param      $fields
   * @param      $orderId
   * @param bool $saveByParentOrder
   * @return bool
   */
  public function saveOrderProducts( $fields, $orderId, $saveByParentOrder = false )
  {
    $this->buyerId = $fields['buyer_id'];
    if ( !$saveByParentOrder ) {
      $subOrderModel = new Order();
      foreach ( $this->productsByOwner as $ownerId => $products ) {
        $countProductsByOwner = 0;
        $total                = 0;
        foreach ( $products as $item ) {
          $countProductsByOwner += (int)$item['count'];
          $total                += $item['product']['full_price'] * (int)$item['count'];
        }

        $fields['parent_id']      = $orderId;
        $fields['supplier_id']    = $ownerId;
        $fields['count_products'] = $countProductsByOwner;
        $fields['total']          = $total;
//        $fields['warehouse_id']   = $item['product']['warehouses'][$item['warehouse_id']]['warehouse_id'];

        unset( $fields['delivery_price'] );
        unset( $fields['discount'] );

        $subOrderModel->storeDeliveryAddress( $this->deliveryAddress );
        $subOrderId = $subOrderModel->setFields( $fields )->insert();

        if ( $subOrderId ) {
          $orderProductModel = new OrderProduct();

          $data = [];
          foreach ( $products as $item ) {
            $data[] = [
              'order_id'   => $subOrderId,
              'product_id' => $item['product']['id'],
              'properties' => $item['properties'],
              'count'      => $item['count'],
              'buyer_id'   => $this->buyerId,
              'buy_price'  => $item['product']['price']
              //              'main_id'      => $orderId,
              //              'warehouse_id' => $item['product']['warehouses'][$item['warehouse_id']]['warehouse_id'],
            ];
          }
          $orderProductModel->setAllFields( $data )->insertAll();
        } else {
          return false;
        }
      }
    } else {
      $orderProductModel = new OrderProduct();

      $data = [];
      foreach ( $this->rawProducts as $item ) {
        $data[] = [
          'order_id'   => $orderId,
          'product_id' => $item['product']['id'],
          'count'      => $item['count'],
          'buyer_id'   => $this->buyerId,
          'buy_price'  => $item['product']['price'],
          //          'main_id'      => $orderId,
          'properties' => $item['properties'],
          //          'warehouse_id' => $item['product']['warehouses'][$item['warehouse_id']]['warehouse_id']
        ];
      }
      $orderProductModel->setAllFields( $data )->insertAll();

    }
    return false;
  }

  /**
   * @param $orderId
   * @return bool
   */
  protected function afterInsert( $orderId )
  {
//      if( !is_null( $this->deliveryAddress ) ) {
//        $orderAddressModel = new OrderAddress();
//
//        $orderAddress             = $this->deliveryAddress;
//        $orderAddress['order_id'] = $orderId;
//
//        $orderAddressId = $orderAddressModel->setFields( $orderAddress )->insert();
//
//        if( $orderAddressId ) {
//          return true;
//        } else {
//          return false;
//        }
//      }
    return true;
  }

  /**
   * @param array $filter
   * @param bool  $fetchOne
   * @return array|mixed
   */
  public static function find( $filter = [], $fetchOne = false )
  {
    $model = new Order();
    if ( count( $filter ) > 0 )
      $model->where( $filter );

    if ( $fetchOne ) {
      return $model->fetchOne();
    }
    return $model->fetchAll();
  }

  /**
   * @param $products
   */
  public function storeProducts( $products )
  {
    if ( $products && count( $products ) > 0 ) {
      foreach ( $products as $key => $product ) {
        $this->countProducts += (int)$product['count'];
        $this->total         += (float)$product['product']['full_price'] * (int)$product['count'];

        $this->productsByOwner[$product['product']['owner_id'][0]][] = $product;
      }
      $this->rawProducts = $products;
    }
  }

  /**
   * @param $address
   */
  public function storeDeliveryAddress( $address )
  {
    $this->deliveryAddress = $address;
  }

  /**
   * @param $discount
   */
  public function storeDiscount( $discount )
  {
    $this->discount = $discount;
  }

  /**
   * @param $orderId
   * @param $buyerId
   * @return array
   */
  public static function getOrderWithChild( $orderId, $buyerId )
  {
    $model = new Order();

    $sql    = "SELECT so.* FROM `store_order` as so
					WHERE so.`id` = $orderId AND so.`buyer_id` = $buyerId
					OR so.`parent_id` = $orderId AND so.`buyer_id` = $buyerId";
    $orders = $model->sql( $sql )->fetchAll();

    //      $orderIds = '';
    //      foreach( $orders as $order ) {
    //        $orderIds .= $order['id'].',';
    //      }
    //      $orderIds = substr_replace( $orderIds, "", -1 );
    $orderIds          = implode( ',', array_column( $orders, 'id' ) );
    $orders['all_ids'] = $orderIds;

    $productsSql        = "SELECT op.`count` as order_count, prod.*, cat.`name` as category FROM `order_product` as op
                              LEFT JOIN `product` as prod ON op.`product_id` = prod.`id`
                              LEFT JOIN `category` as cat ON prod.`category_id` = cat.`id`
                              WHERE op.`order_id` IN ({$orderIds})";
    $productModel       = new Product( true );
    $products           = $productModel->sql( $productsSql )->fetchAll();
    $orders['products'] = $products;

    $addressSql = "SELECT oa.* FROM `order_address` as oa
					            WHERE oa.`order_id` IN ({$orderIds})";

    $address           = $model->sql( $addressSql )->fetchOne();
    $orders['address'] = $address;

    return $orders;
  }

  /**
   * @param      $ownerId
   * @param bool $supplier
   * @return array
   */
  public static function getOrdersWithProductsByOwner( $ownerId, $supplier = true )
  {
    $model = new Order();
    $field = '';
    if ( $supplier ) {
      $field = "supplier_id";
    } else {
      $field = 'seller_id';
    }

    $sql    = "SELECT so.* FROM `store_order` as so WHERE so.`$field` = $ownerId";
    $orders = $model->sql( $sql )->fetchAll();
    return $orders;
  }


  //	  public function afterDelete($model){
  //      if(!parent::afterDelete($model)) return;
  //		  $order = $this->getDeletedItems();
  //		  $ids = array_column($order, 'id');
  //		  OrderAddress::deleteFilter(['order_id'=>$ids]);
  //		  OrderProduct::deleteFilter(['order_id'=>$ids]);
  //	  }

//    public static function findProductBySellerId($seller_id, $page, $default_count){
//      $model = new self();
//      $start = ($page - 1) * $default_count;
//      $sql = "SELECT p.* FROM `store_order` as `o`
//              JOIN `order_product` as `op` ON op.order_id = o.id
//              LEFT JOIN `product` as `p` ON op.product_id = p.id
//              WHERE o.pay_date <> 0 AND p.owner_id = $seller_id
//              LIMIT $start, $default_count
//";
//
//      return $model->sql($sql)->fetchAll();
//    }

}