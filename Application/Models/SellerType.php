<?php
	
	namespace Application\Models;
	class SellerType extends \Application\Classes\Model
	{
		public function __construct()
		{
			parent::__construct('seller_type', $this->getTypes(), $this);
		}
		
		public function getTypes()
		{
			return array(
				'id'          => 'int',
				'seller_type' => 'str'
			);
		}
		
		public static function getSellerTypes()
		{
			$model = new SellerType();
			return $model->fetchAll();
		}
	}