<?php

namespace Application\Models;
class AdminPageCategory extends \Application\Classes\Model implements \Application\Classes\Forms\Interfaces\FormBuilderInterface
{
  public function __construct()
  {
    parent::__construct( 'admin_page_category', $this->getTypes(), $this );
  }

  public function getTypes()
  {
    return [
      'id'    => 'int',
      'name'  => 'str',
      'sort'  => 'int',
      'pages' => 'str',

    ];
  }

  public function getFields()
  {
    return array(
      'id' => array(
        'type'    => 'string',
        'visible' => true,
        'edited'  => false,
        'label'   => 'ID',
        'sort' => 1000,
      ),

      'name' => array(
        'type'     => 'string',
        'visible'  => true,
        'edited'   => true,
        'label'    => 'Название',
        'required' => true,
        'multiple' => false,
        'sort' => 1000,
      ),

      'sort' => array(
        'type'     => 'string',
        'visible'  => true,
        'edited'   => false,
        'label'    => '',
        'required' => false,
        'multiple' => false,
        'sort' => 1,
      ),

      'pages' => array(
        'type'     => 'aselect',
        'values'   => AdminPage::find( [ 'visible' => 1 ] ),
        'id_key'   => 'id',
        'name_key' => 'name',
        'visible'  => false,
        'edited'   => true,
        'label'    => 'Пункты меню',
        'required' => false,
        'multiple' => true,
        'sort' => 1000,
      ),

    );
  }

  public function rules(): array
  {
    return [
      [ [ "name" ], 'required' ],
      [ [ "name" ], 'string' ],
      [ [ "sort" ], 'number' ],

    ];
  }

  protected function setPages( $element )
  {
    return serialize( $element );
  }

  protected function getPages( $element )
  {
    return unserialize( $element );
  }

  public function attributeLabels(): array
  {
    return [
      'name'  => 'Название',
      'sort'  => 'sort',
      'pages' => 'Пункты меню',

    ];
  }

  public function formFields( array $data = [] )
  {
    $fields = [
      'name'  => [ 'type' => 'text', 'class' => [ 'form-control' ] ],
      'sort'  => [ 'type' => 'text', 'class' => [ 'form-control' ] ],
      'pages' => [
        'type'   => 'mselect',
        'values' => \Application\Models\AdminPage::find(),
        'id'     => 'id',
        'name'   => 'name',
        'class'  => [ 'form-control' ]
      ],

    ];
    if ( count( $data ) > 0 ) {
      $fieldsFilter = [];
      foreach ( $data as $datum ) {
        if ( isset( $fields[$datum] ) )
          $fieldsFilter[$datum] = $fields[$datum];
      }
      return $fieldsFilter;
    }
    return $fields;
  }

  protected function beforeInsert( array $data ): array
  {
    $unixTime = $this->getUnixTime();

    return $data;
  }


}
