<?php

namespace Application\Models;

use Application\Lib\Mail;
use Application\Lib\PHPMailer\PHPMailer;
use function mail;

class SupportQuestion extends \Application\Classes\Model
{
  public function __construct()
  {
    parent::__construct( 'support_question', $this->getTypes(), $this );
  }

  public function getTypes()
  {
    return [
      'id'                => 'int',
      'email'             => 'str',
      'question_category' => 'int',
      'question_type'     => 'int',
      'question'          => 'str',
      'status'            => 'int',
      "answer"            => 'str'

    ];
  }

  public function getFields()
  {
    return array(
      'id' => array(
        'type'    => 'string',
        'visible' => true,
        'sort'    => 500,
        'edited'  => false,
        'label'   => 'ID'
      ),

      'email' => array(
        'type'     => 'string',
        'visible'  => true,
        'sort'     => 500,
        'edited'   => true,
        'label'    => 'Email',
        'required' => false,
        'multiple' => false,
      ),

      'question_category' => array(
        'type'     => 'aselect',
        'visible'  => true,
        'sort'     => 500,
        'edited'   => true,
        'values'   => QuestionCategory::find(),
        'id_key'   => 'id',
        'name_key' => 'name',
        'label'    => 'Категория вопроса',
        'required' => false,
        'multiple' => false,
      ),

      'question' => array(
        'type'     => 'text',
        'visible'  => true,
        'sort'     => 500,
        'edited'   => true,
        'label'    => 'Текст ',
        'required' => false,
        'multiple' => false,
      ),

      'question_type' => array(
        'type'     => 'aselect',
        'visible'  => false,
        'sort'     => 500,
        'edited'   => true,
        'values'   => QuestionType::find(),
        'id_key'   => 'id',
        'name_key' => 'name',
        'label'    => 'Тип вопроса',
        'required' => false,
        'multiple' => false,
      ),
      'answer'        => [
        'type'        => 'text',
        'visible'     => false,
        'sort'        => 500,
        'edited'      => true,
        'description' => 'Если ответ уже получен то он больше отправляться не будет',
        'required'    => false,
        'label'       => 'Ответ'
      ],
      'status'        => [
        'type'     => 'read',
        'visible'  => true,
        'sort'     => 500,
        'edited'   => true,
        'required' => false,
        'label'    => 'Статус'
      ],

    );
  }

  protected function beforeUpdate( $data )
  {
    if ( $data['status'] == 0 ) {
      $mail = new Mail;
      $mail->to( $data['email'], $data['email'] );
      $mail->subject = 'Ответ на вопрос';
      $mail->setTemplate( 'question_answer', [ 'question' => $data['question'], 'answer' => $data['answer'] ] );
      if ( $mail->send() ) $data['status'] = 1;
    }
    return $data;
  }

  public function rules()
  {
    return [
      [ [ "email" ], 'email' ],
      [ [ "question" ], 'string' ],
      [ 'answer', 'string', 'min' => 10 ],
      [ [ "question_category", ], 'number' ],
      [ [ "email", "question_category", "question", 'question_type' ], 'required' ]

    ];
  }

  public function attributeLabels()
  {
    return [
      'email'             => 'Email пользователя',
      'question_category' => 'Категория вопроса',
      'question'          => 'Текст вопроса',
      'status'            => 'Статус вопроса',

    ];
  }

  public function formFields( array $data = [] )
  {
    $fields = [
      'email'             => [ 'type' => 'text', 'class' => [ 'form-control' ] ],
      'question_category' => [ 'type' => 'text', 'class' => [ 'form-control' ] ],
      'question'          => [ 'type' => 'textarea', 'class' => [ 'form-control' ] ],
      'status'            => [ 'type' => 'text', 'class' => [ 'form-control' ] ],

    ];
    if ( count( $data ) > 0 ) {
      $fieldsFilter = [];
      foreach ( $data as $datum ) {
        if ( isset( $fields[$datum] ) )
          $fieldsFilter[$datum] = $fields[$datum];
      }
      return $fieldsFilter;
    }
    return $fields;
  }


}
