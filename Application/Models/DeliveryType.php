<?php

namespace Application\Models;
class DeliveryType extends \Application\Classes\Model implements \Application\Classes\Forms\Interfaces\FormBuilderInterface
{
  public function __construct()
  {
    parent::__construct( 'delivery_type', $this->getTypes(), $this );
  }

  public function getTypes()
  {
    return [
      'id'   => 'int',
      'name' => 'str',

    ];
  }

  public function getFields()
  {
    return array(
      'id' => array(
        'type'    => 'string',
        'visible' => true,
        'edited'  => false,
        'label'   => 'ID'
      ),

      'name' => array(
        'type'     => 'string',
        'visible'  => true,
        'edited'   => true,
        'label'    => 'Название доставки',
        'required' => true,
        'multiple' => false,
      ),

    );
  }

  public function rules(): array
  {
    return [
      [ [ "name" ], 'required' ],
      [ [ "name" ], 'string' ],

    ];
  }

  public function attributeLabels(): array
  {
    return [
      'name' => 'Название доставки',

    ];
  }

  public function formFields( array $data = [] )
  {
    $fields = [
      'name' => [ 'type' => 'text', 'class' => [ 'form-control' ] ],

    ];
    if ( count( $data ) > 0 ) {
      $fieldsFilter = [];
      foreach ( $data as $datum ) {
        if ( isset( $fields[$datum] ) )
          $fieldsFilter[$datum] = $fields[$datum];
      }
      return $fieldsFilter;
    }
    return $fields;
  }
  
}
