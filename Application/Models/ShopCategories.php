<?php

namespace Application\Models;

class ShopCategories extends \Application\Classes\Model
{

  public function __construct()
  {
    parent::__construct( 'shop_categories', $this->getTypes(), $this );
  }

  public function getTypes()
  {
    return [
      'id'          => 'int',
      'shop_id'     => 'int',
      'category_id' => 'int',
    ];
  }

}
