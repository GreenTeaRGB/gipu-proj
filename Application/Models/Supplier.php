<?php
	
	namespace Application\Models;
	use \Application\Helpers\DataHelper;
	class Supplier extends \Application\Classes\Model implements \Application\Classes\Forms\Interfaces\FormBuilderInterface
	{
		public function __construct()
		{
			parent::__construct('supplier', $this->getTypes(), $this);
		}
		
		public function getTypes()
		{
			return array(
				'id'                 => 'int',
				'user_id'            => 'int',
				'status_id'          => 'int',
				'referral_id'        => 'int',
				'referral_level'     => 'int',
				'balance'            => 'str',
				'juristic_type'      => 'int',
				'juristic_documents' => 'str',
				'juristic_id'        => 'int',
				'secret_question'    => 'str',
				'secret_answer'      => 'str'
			);
		}
		
		public function rules() : array
		{
			return [
					['organization_name', 'required'],
					[['user_id','status_id','referral_id','balance','juristic_id', 'referral_level','juristic_type'], 'number'],
					[['juristic_documents','secret_question','secret_answer', 'organization_name','juristic_address','contact_name'], 'string'],
			];
		}

		public function attributeLabels() :array
		{
			return  [];
		}

		public function formFields(array $data = []) :array 
		{
			return [
				'referral_id' => ['type' => 'text', 'default' => (isset($_GET['referrer_id'])) ? $_GET['referrer_id'] : '', 'attr'=>['class'=>'form-control']],
				'organization_name'=>['type' => 'text', 'default' => '', 'attr'=>['class'=>'form-control']],
				'juristic_address'=>['type' => 'textarea', 'default' => '', 'attr'=>['class'=>'form-control']],
				'contact_name' => ['type' => 'text', 'label'=>'Телефон', 'attr'=>['class'=>'form-control']],
			];
		}
	}