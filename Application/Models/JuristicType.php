<?php
	
	namespace Application\Models;
	class JuristicType extends \Application\Classes\Model
	{
		public function __construct()
		{
			parent::__construct('juristic_type', $this->getTypes(), $this);
		}
		
		public function getTypes()
		{
			return array(
				'id'   => 'int',
				'type' => 'str'
			);
		}
		
		public static function getJuristicTypes()
		{
			$model = new JuristicType();
			return $model->fetchAll();
		}
	}