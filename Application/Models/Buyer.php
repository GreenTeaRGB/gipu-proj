<?php
	
	namespace Application\Models;
	class Buyer extends User
	{
		public function __construct()
		{
			parent::__construct('buyer', $this->getTypes(), $this);
		}
		
		public function getTypes()
		{
			/*$parentTypes = parent::getTypes();
			var_dump($parentTypes);*/
			return array(
				'id'              => 'int',
				'user_id'         => 'int',
				'address_id'      => 'int',
				'notify_by_email' => 'int',
				'notify_by_phone' => 'int',
				'passport_data'   => 'str',
				'balance'         => 'str'
			);
		}


		public static function find($filter = []){
			$model = new Buyer();
			if(count($filter) > 0) $model->where($filter);
			return $model->fetchAll();
		}

	}