<?php
	
	namespace Application\Models;
	class Review extends \Application\Classes\Model implements \Application\Classes\Forms\Interfaces\FormBuilderInterface
	{
		public function __construct()
		{
			parent::__construct('review', $this->getTypes(), $this);
		}
		
		public function getTypes()
		{
			return [
				'id'              => 'int',
				'created_at'      => 'str',
				'updated_at'      => 'str',
				'updater_id'      => 'int',
				'product_id'      => 'int',
				'buyer_id'        => 'int',
				'order_id'        => 'int',
				'rating'          => 'int',
				'comment'         => 'str',
				'accomplishments' => 'str',
				'limitations'     => 'str',
				'recommendations' => 'str',
				'like'            => 'int',
				'dislike'         => 'int',
				'active'          => 'int',
			];
		}
		
		public function getFields()
		{
			return array(
				'id' => array(
					'type'    => 'string',
					'visible' => true,
					'edited'  => false,
					'label'   => 'ID'
				),
				
				'created_at' => array(
					'type'     => 'date',
					'visible'  => true,
					'edited'   => true,
					'label'    => 'Дата',
					'required' => false,
					'multiple' => false,
				),
				
				'updated_at' => array(
					'type'     => 'date',
					'visible'  => false,
					'edited'   => true,
					'label'    => 'Дата изменения',
					'required' => false,
					'multiple' => false,
				),
				
				'updater_id' => array(
          'type'     => 'modals',
          'visible'  => false,
          'edited'   => true,
          'required' => false,
          'class'    => '\\Application\\Models\\User',
          'fields'   => [ 'id', 'name', 'last_name' ],
          'info'     => [ 'name', 'last_name', 'registration_date', 'email', 'phone', 'type' ],
          'label'    => 'Кто изменил'
				),
				
				'product_id' => array(
          'type'     => 'modals',
          'visible'  => true,
          'edited'   => true,
          'required' => false,
          'class'    => '\\Application\\Models\\Product',
          'fields'   => [ 'id', 'name' ],
          'label'    => 'ID товара'
				),

        'buyer_id'           => [
          'type'     => 'modals',
          'visible'  => false,
          'edited'   => true,
          'required' => false,
          'class'    => '\\Application\\Models\\User',
          'fields'   => [ 'id', 'name', 'last_name' ],
          'info'     => [ 'name', 'last_name', 'registration_date', 'email', 'phone', 'type' ],
          'label'    => 'Покупатель'
        ],
				
				'order_id' => array(
          'type'     => 'modals',
          'visible'  => false,
          'edited'   => true,
          'required' => false,
          'class'    => '\\Application\\Models\\Order',
          'fields'   => [ 'id', 'name',  ],
          'label'    => 'ID заказа'
				),
				
				'rating' => array(
					'type'     => 'string',
					'visible'  => false,
					'edited'   => true,
					'label'    => 'Оценка',
					'required' => false,
					'multiple' => false,
				),
				
				'comment' => array(
					'type'     => 'text',
					'visible'  => true,
					'edited'   => true,
					'label'    => 'Комментарий',
					'required' => false,
					'multiple' => false,
				),
				
				'accomplishments' => array(
					'type'     => 'text',
					'visible'  => false,
					'edited'   => true,
					'label'    => 'Достоинства',
					'required' => false,
					'multiple' => false,
				),
				
				'limitations' => array(
					'type'     => 'text',
					'visible'  => false,
					'edited'   => true,
					'label'    => 'Недостатки',
					'required' => false,
					'multiple' => false,
				),
				
				'recommendations' => array(
					'type'     => 'text',
					'visible'  => false,
					'edited'   => true,
					'label'    => 'Рекомендации',
					'required' => false,
					'multiple' => false,
				),
				
				'like' => array(
					'type'     => 'string',
					'visible'  => true,
					'edited'   => true,
					'label'    => 'Лайк',
					'required' => false,
					'multiple' => false,
				),
				
				'dislike' => array(
					'type'     => 'string',
					'visible'  => false,
					'edited'   => true,
					'label'    => 'Dislike',
					'required' => false,
					'multiple' => false,
				),
				
				'active' => array(
					'type'     => 'checkbox',
					'visible'  => false,
					'edited'   => true,
					'label'    => 'Активность',
					'required' => false,
					'multiple' => false,
				),
			
			);
		}
		
		public function rules(): array
		{
			return [
				[["updater_id", "product_id", "buyer_id", "order_id", "rating", "like", "dislike", "active"], 'number'],
				[["created_at", "updated_at"], 'date'],
				[["rating", "product_id", "updater_id", "buyer_id", "order_id"], 'required'],
			];
		}
		
		public function attributeLabels(): array
		{
			return [
				'created_at'      => 'Дата добавления',
				'updated_at'      => 'Дата изменения',
				'updater_id'      => 'Кто изменил',
				'product_id'      => 'Идентификатор товара',
				'buyer_id'        => 'Идентификатор покупателя',
				'order_id'        => 'Идентификатор заказа',
				'rating'          => 'Оценка',
				'comment'         => 'Комментарий',
				'accomplishments' => 'Достоинства',
				'limitations'     => 'Недостатки',
				'recommendations' => 'Рекомендации',
				'like'            => 'Like',
				'dislike'         => 'Dislike',
				'active'          => 'Активность',
			
			];
		}
		
		public function formFields(array $data = [])
		{
			$fields = [
				'created_at'      => ['type' => 'date', 'class' => ['form-control']],
				'updated_at'      => ['type' => 'date', 'class' => ['form-control']],
				'updater_id'      => ['type' => 'text', 'class' => ['form-control']],
				'product_id'      => ['type' => 'text', 'class' => ['form-control']],
				'buyer_id'        => ['type' => 'text', 'class' => ['form-control']],
				'order_id'        => ['type' => 'text', 'class' => ['form-control']],
				'rating'          => ['type' => 'text', 'class' => ['form-control']],
				'comment'         => ['type' => 'textarea', 'class' => ['form-control']],
				'accomplishments' => ['type' => 'textarea', 'class' => ['form-control']],
				'limitations'     => ['type' => 'textarea', 'class' => ['form-control']],
				'recommendations' => ['type' => 'textarea', 'class' => ['form-control']],
				'like'            => ['type' => 'text', 'class' => ['form-control']],
				'dislike'         => ['type' => 'text', 'class' => ['form-control']],
				'active'          => ['type' => 'checkbox', 'class' => ['form-control']],
			
			];
			if ( count($data) > 0 ) {
				$fieldsFilter = [];
				foreach ($data as $datum) {
					if ( isset($fields[$datum]) )
						$fieldsFilter[$datum] = $fields[$datum];
				}
				return $fieldsFilter;
			}
			return $fields;
		}
		
		
		protected function setCreatedAt($element)
		{
			return strtotime($element);
		}
		
		protected function setUpdatedAt($element)
		{
			return strtotime($element);
		}

		protected function getCreatedAt($element)
		{
			return date('d.m.Y', $element);
		}
		
		protected function getUpdatedAt($element)
		{
			return date('d.m.Y', $element);
		}
		
		protected function beforeInsert($fields)
		{
			$unixTime = $this->getUnixTime();
			$fields['created_at'] = $unixTime;
			$fields['updated_at'] = $unixTime;
			
			return $fields;
		}
		
		protected function afterFetchAll($reviews)
		{
			$userIds = array_unique(array_column($reviews, 'buyer_id'));
			
			if ( $userIds && count($userIds) > 0 ) {
				$userModel = new User();
				
				$sql = "SELECT u.id, u.name, u.city, u.avatar FROM `user` as u WHERE u.`id` IN (";
				foreach ($userIds as $userId) {
					$sql .= $userId . ',';
				}
				$sql = trim($sql, ',');
				$sql .= ")";
				$users = $userModel->sql($sql)->fetchAll();
				
				$usersByOwnId = [];
				foreach ($users as $user) {
					$usersByOwnId[$user['id']] = $user;
				}
				
				foreach ($reviews as $key => $review) {
					if ( isset($usersByOwnId[$review['buyer_id']]) ) {
						$reviews[$key]['reviewed_by'] = $usersByOwnId[$review['buyer_id']];
					}
				}
			}
			return $reviews;
		}
		
//		public function afterDelete( $model )
//		{
//			if(!parent::afterDelete($model)) return;
//			$review = $this->getDeletedItems();
//			$ids = array_column($review, 'id');
//			ReviewLike::deleteFilter(['review_id'=>$ids]);
//		}
	}