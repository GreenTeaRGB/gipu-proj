<?php
	
	namespace Application\Models;
	class Seller extends \Application\Classes\Model implements \Application\Classes\Forms\Interfaces\FormBuilderInterface
	{
		public function __construct()
		{
			parent::__construct('seller', $this->getTypes(), $this);
		}
		
		public function getTypes()
		{
			return array(
				'id'                 => 'int',
				'user_id'            => 'int',
				'referral_id'        => 'int',
				'referral_level'     => 'int',
				'balance'            => 'str',
				'living_address'     => 'str',
				'status_id'          => 'int',
				'seller_type'        => 'int',
				'juristic_documents' => 'str',
				'passport_data'      => 'str',
				'card_id'            => 'int',
				'juristic_type'      => 'int',
				'juristic_id'        => 'int',
				'secret_question'    => 'str',
				'secret_answer'      => 'str'
			);
		}

    public function getFields(){
      return [
          'user_id' => [
              'type' => 'string',
              'visible' => false,
              'edited' => true,
              'required' => false,
              'label'=> 'Пользователь'
          ],
          'referral_id' => [
              'type' => 'string',
              'visible' => false,
              'edited' => true,
              'required' => false,
              'label'=> 'Реферал'
          ],
          'referral_level' => [
              'type' => 'string',
              'visible' => false,
              'edited' => true,
              'required' => false,
              'label'=> 'Уровень реферала'
          ],
          'balance' => [
              'type' => 'string',
              'visible' => false,
              'edited' => true,
              'required' => false,
              'label'=> 'Баланс'
          ],
          'living_address' => [
              'type' => 'string',
              'visible' => false,
              'edited' => true,
              'required' => false,
              'label'=> 'Адрес проживания'
          ],
          'status_id' => [
              'type' => 'string',
              'visible' => false,
              'edited' => true,
              'required' => false,
              'label'=> 'Статус'
          ],
          'seller_type' => [
              'type' => 'string',
              'visible' => false,
              'edited' => true,
              'required' => false,
              'label'=> 'Тип продавца'
          ],
          'juristic_documents' => [
              'type' => 'string',
              'visible' => false,
              'edited' => true,
              'required' => false,
              'label'=> 'Юридические документы'
          ],
          'passport_data' => [
              'type' => 'string',
              'visible' => false,
              'edited' => true,
              'required' => false,
              'label'=> 'Данные пасспорта'
          ],
          'card_id' => [
              'type' => 'string',
              'visible' => false,
              'edited' => true,
              'required' => false,
              'label'=> 'Карта'
          ],
          'juristic_type' => [
              'type' => 'string',
              'visible' => false,
              'edited' => true,
              'required' => false,
              'label'=> 'Юр. тип'
          ],
          'juristic_id' => [
              'type' => 'string',
              'visible' => false,
              'edited' => true,
              'required' => false,
              'label'=> 'Юр. лицо'
          ],
          'secret_question' => [
              'type' => 'string',
              'visible' => false,
              'edited' => true,
              'required' => false,
              'label'=> 'Секретный вопрос'
          ],
          'secret_answer' => [
              'type' => 'string',
              'visible' => false,
              'edited' => true,
              'required' => false,
              'label'=> 'Секретный ответ'
          ],
      ];
    }

		public function formFields(array $data = []):array {
			return [];
		}
		public function rules() :array
		{
			return [
			
			];
		}

		public function attributeLabels() : array
		{
			return [];
		}

		public function registerForm(){
			return [
				'referral_id' => ['type' => 'text', 'default' => (isset($_GET['referrer_id'])) ? $_GET['referrer_id'] : '' , 'attr'=>['class'=>'form-control']]
			];
		}

	}