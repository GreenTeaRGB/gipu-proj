<?php
	
	namespace Application\Models;
	class PropertyValue extends \Application\Classes\Model
	{
		public function __construct()
		{
			parent::__construct('property_value', $this->getTypes(), $this);
		}
		
		public function getTypes()
		{
			return [
				'id'          => 'int',
				'property_id' => 'int',
				'product_id'  => 'int',
				'value'       => 'str',
				'description' => 'str',
				'type'        => 'str',
				'value_enum'  => 'int',
			];
		}
		
		public function getFields()
		{
			return array(
				'id' => array(
					'type'    => 'string',
					'visible' => true,
					'edited'  => false,
					'label'   => 'ID'
				),
				
				'property_id' => array(
					'type'     => 'string',
					'visible'  => true,
					'edited'   => true,
					'label'    => 'Идентификатор свойства',
					'required' => false,
					'multiple' => false,
				),
				
				'product_id' => array(
					'type'     => 'string',
					'visible'  => true,
					'edited'   => true,
					'label'    => 'Идентификатор продукта',
					'required' => false,
					'multiple' => false,
				),
				
				'value' => array(
					'type'     => 'string',
					'visible'  => true,
					'edited'   => true,
					'label'    => 'Значение свойства',
					'required' => false,
					'multiple' => false,
				),
				
				'description' => array(
					'type'     => 'text',
					'visible'  => true,
					'edited'   => true,
					'label'    => 'Описание',
					'required' => false,
					'multiple' => false,
				),
				
				'type' => array(
					'type'     => 'string',
					'visible'  => true,
					'edited'   => true,
					'label'    => 'Тип',
					'required' => false,
					'multiple' => false,
				),
				
				'value_enum' => array(
					'type'     => 'string',
					'visible'  => true,
					'edited'   => true,
					'label'    => 'Id списка',
					'required' => false,
					'multiple' => false,
				),
			
			);
		}
		
		public function rules()
		{
			return [
				[["value", "type"], 'string'],
				[["property_id", "product_id"], 'number'],
				[["value_enum", "property_id", "product_id"], 'required']
			
			];
		}
		
		public function attributeLabels()
		{
			return [
				'property_id' => 'Идентификатор свойства',
				'product_id'  => 'Идентификатор продукта',
				'value'       => 'Значение свойства',
				'description' => 'Описание',
				'type'        => 'Тип',
				'value_enum'  => 'Id списка',
			
			];
		}
		
		public function formFields(array $data = [])
		{
			$fields = [
				'property_id' => ['type' => 'text', 'class' => ['form-control']],
				'product_id'  => ['type' => 'text', 'class' => ['form-control']],
				'value'       => ['type' => 'text', 'class' => ['form-control']],
				'description' => ['type' => 'textarea', 'class' => ['form-control']],
				'type'        => ['type' => 'text', 'class' => ['form-control']],
			
			];
			if ( count($data) > 0 ) {
				$fieldsFilter = [];
				foreach ($data as $datum) {
					if ( isset($fields[$datum]) )
						$fieldsFilter[$datum] = $fields[$datum];
				}
				return $fieldsFilter;
			}
			return $fields;
		}
		
		public static function deleteByProperty($propertyId){
			$model = new PropertyValue();
			return $model->where(['property_id'=>$propertyId])->delete();
		}
		
		public static function deleteByProductId($productId){
			$model = new PropertyValue();
			return $model->where(['product_id'=>$productId])->delete();
		}

		protected function afterDelete($model){
		  $values = $this->getDeletedItems();
		  self::removeFiles(array_column($values, 'value'));
    }

	}
