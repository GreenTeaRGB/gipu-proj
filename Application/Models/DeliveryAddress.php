<?php
	
	namespace Application\Models;
	class DeliveryAddress extends \Application\Classes\Model
	{
		public function __construct()
		{
			parent::__construct('delivery_address', $this->getTypes(), $this);
		}
		
		public function getTypes()
		{
			return array(
				'id'                 => 'int',
				'delivery_name'      => 'str',
				'delivery_last_name' => 'str',
				'patronymic'         => 'str',
				'country'            => 'str',
				'region'             => 'str',
				'city'               => 'str',
				'street'             => 'str',
				'house_number'       => 'int',
				'apartment'          => 'int',
				'user_id'            => 'int',
				'post_index'         => 'str',
				'phone'              => 'str',
			);
		}
	}