<?php

namespace Application\Models;
class PayCard extends \Application\Classes\Model
{
  public function __construct()
  {
    parent::__construct( 'pay_card', $this->getTypes(), $this );
  }

  public function getTypes()
  {
    return array(
      'id'          => 'int',
      'user_id'     => 'int',
      'description' => 'str',
      'name'        => 'str',
      'last_name'   => 'str',
      'card_num'    => 'int',
      'month'       => 'int',
      'year'        => 'int',
    );
  }

  public function rules()
  {
    return [
      [ [ 'card_num', 'user_id' ], 'required' ],
      [ [ 'user_id', 'card_num' ], 'number' ]
    ];
  }

  public function getFields()
  {
    return [
      'id'          => [
        'type'     => 'string',
        'require'  => false,
        'visible'  => false,
        'editable' => false
      ],
      'user_id'     => [
        'type'     => 'string',
        'require'  => false,
        'visible'  => false,
        'editable' => false
      ],
      'description' => [
        'type'     => 'string',
        'require'  => false,
        'visible'  => false,
        'editable' => false
      ],
      'name'        => [
        'type'     => 'string',
        'require'  => false,
        'visible'  => false,
        'editable' => false
      ],
      'last_name'   => [
        'type'     => 'string',
        'require'  => false,
        'visible'  => false,
        'editable' => false
      ],
      'card_num'    => [
        'type'     => 'string',
        'require'  => false,
        'visible'  => false,
        'editable' => false
      ],
      'month'       => [
        'type'     => 'string',
        'require'  => false,
        'visible'  => false,
        'editable' => false
      ],
      'year'        => [
        'type'     => 'string',
        'require'  => false,
        'visible'  => false,
        'editable' => false
      ],
    ];
  }

}