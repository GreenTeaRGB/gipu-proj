<?php
	
	namespace Application\Models;
	class UserStatus extends \Application\Classes\Model
	{
		public function __construct()
		{
			parent::__construct('user_status', $this->getTypes(), $this);
		}
		
		public function getTypes()
		{
			return array(
				'id'   => 'int',
				'type' => 'str'
			);
		}

		public static function getFields(){
		  return [
        'id'                 => [
          'type'     => 'read',
          'visible'  => false,
          'edited'   => false,
          'label'    => 'ID',
          'required' => false,
          'sort'     => 500
        ],
        'type'  => [
          'type'     => 'string',
          'visible'  => false,
          'edited'   => true,
          'label'    => 'Название статуса',
          'required' => false,
          'sort'     => 1810
        ],
      ];
    }
		
		public static function getUserStatuses()
		{
			$model = new UserStatus();
			return $model->fetchAll();
		}
		
		public static function getStatusById($id)
		{
			$model = new UserStatus();
			$status = $model->where(['id' => $id])->fetchOne();
			
			if ( $status ) {
				return $status['type'];
			}
			
			return false;
		}
	}