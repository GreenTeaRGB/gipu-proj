<?php

namespace Application\Models;
class PropertyGroup extends \Application\Classes\Model
{
	public function __construct()
	{
		parent::__construct('property_group', $this->getTypes(), $this);
	}
	
	public function getTypes()
	{
		return [
			'id'     => 'int',
			'name'   => 'str',
			'active' => 'int',
			'sort'   => 'int',
		
		];
	}
	
	public function getFields()
	{
		return array(
			'id' => array(
				'type'    => 'string',
				'visible' => true,
				'edited'  => false,
				'label'   => 'ID'
			),
			
			'name' => array(
				'type'     => 'string',
				'visible'  => true,
				'edited'   => true,
				'label'    => 'Имя группы',
				'required' => false,
				'multiple' => false,
			),
			
			'active' => array(
				'type'     => 'checkbox',
				'visible'  => false,
				'edited'   => false,
				'label'    => 'Активность',
				'required' => false,
				'multiple' => false,
			),
			
			'sort' => array(
				'type'     => 'string',
				'visible'  => false,
				'edited'   => false,
				'label'    => 'Сортировка',
				'required' => false,
				'multiple' => false,
			),
		
		);
	}
	
	public function rules()
	{
		return [
			[["name"], 'string'],
			[["active", "sort"], 'number'],
			[ [ "name" ], 'required' ],
		
		];
	}
	
	public function attributeLabels()
	{
		return [
			'name'   => 'Имя группы',
			'active' => 'Активность',
			'sort'   => 'Сортировка',
		
		];
	}
	
	public function formFields(array $data = [])
	{
		$fields = [
			'name'   => ['type' => 'text', 'class' => ['form-control']],
			'active' => ['type' => 'checkbox', 'class' => ['form-control']],
			'sort'   => ['type' => 'text', 'class' => ['form-control']],
		
		];
		if ( count($data) > 0 ) {
			$fieldsFilter = [];
			foreach ($data as $datum) {
				if ( isset($fields[$datum]) )
					$fieldsFilter[$datum] = $fields[$datum];
			}
			return $fieldsFilter;
		}
		return $fields;
	}
	
	public function afterDelete( $model )
	{
    if(!parent::afterDelete($model)) return;
		$propertyGroup = $this->getDeletedItems();
		$ids = array_column($propertyGroup, 'id');
		Properties::deleteFilter(['property_group_id'=>$ids]);
	}
}
