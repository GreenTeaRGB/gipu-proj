<?php
	namespace Application\Models;
class UserGroupRights extends \Application\Classes\Model
{
    public function __construct() 
    {
        parent::__construct ('user_group_rights', $this->getTypes(), $this);
    }

    public function getTypes(){
    		return array(
						'id' => 'int',
						'admin_page_id' => 'int',
						'user_group_id' => 'int',
				);
		}

		public function getFields(){
    	return [
				'season_id' => [
					'type'=>'select',
					'values' => Season::find(),
					'id_key'=>'id',
					'name_key'=>'name',
					'visible'=>true,
					'edited'=>true,
					'label'=>'Сезон',
					'required'=>false,
					'multiple'=>true,
				],
			];
		}


    public static function getListByGroup($group_id){
        $model = new UserGroupRights();
        $item = $model->where(array('user_group_id'=>$group_id))->fetchAll();
        $items = array();
        foreach ($item as $i) $items[] = $i['admin_page_id'];
        return $items;
    }

    public static function createGroup($categories, $groupId)
    {   
        foreach ($categories as $catId)
        {
            $model = new UserGroupRights();
            $result = $model->setFields(array('admin_page_id'=>$catId,'user_group_id'=>$groupId))->insert();
        }
    }

    public static function deleteByGroup($id)
    {
        $model = new UserGroupRights();
        $result = $model->where(array('user_group_id'=>$id))->delete();
        return $result;
    }

    public static function deleteByIdGroup($catId, $groupId)
    {
        $model = new UserGroupRights();
        $result = $model->where(array('admin_page_id'=>$catId,'user_group_id'=>$groupId))->delete();
        return $result;
    }

    public static function updateByIdGroup($id, $update)
    {
        $model = new UserGroup();
        $result = $model->setFields($update)->where(array('id'=>$id))->update();
        return $result;
    }
}   
