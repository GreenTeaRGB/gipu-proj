<?php

	namespace Application\Models;
	class ProductStatus extends \Application\Classes\Model
	{
		public function __construct()
		{
			parent::__construct('product_status', $this->getTypes(), $this);
		}

		public function getTypes()
		{
			return array(
				'id'   => 'int',
				'type' => 'str'
			);
		}

		public static function getProductStatuses()
		{
			$model = new ProductStatus();
			return $model->fetchAll();
		}

		public static function getProductStatusTypeById($id)
		{
			$model = new ProductStatus();
			$status = $model->where(['id' => $id])->fetchOne();

			if ( $status ) {
				return $status['type'];
			}

			return false;
		}
	}