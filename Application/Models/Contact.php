<?php

namespace Application\Models;
class Contact extends \Application\Classes\Model implements \Application\Classes\Forms\Interfaces\FormBuilderInterface
{
  private static $contact;
  public function __construct()
  {
    parent::__construct( 'contact', $this->getTypes(), $this );
  }

  public function getTypes()
  {
    return [
      'id'            => 'int',
      'support_phone' => 'str',
      'phone'         => 'str',
      'text'          => 'str',

    ];
  }

  public function getFields()
  {
    return array(
      'id' => array(
        'type'    => 'string',
        'visible' => true,
        'edited'  => false,
        'label'   => 'ID'
      ),

      'support_phone' => array(
        'type'     => 'string',
        'visible'  => true,
        'edited'   => true,
        'label'    => 'Служба поддержки',
        'required' => false,
        'multiple' => false,
      ),

      'phone' => array(
        'type'     => 'string',
        'visible'  => true,
        'edited'   => true,
        'label'    => 'Контакнтый центр',
        'required' => false,
        'multiple' => false,
      ),

      'text' => array(
        'type'     => 'text',
        'visible'  => false,
        'edited'   => true,
        'label'    => 'Текст',
        'required' => true,
        'multiple' => false,
        'html' => true,
      ),

    );
  }

  public function rules(): array
  {
    return [
      [ [ "text" ], 'required' ],
      [ [ "support_phone", "phone" ], 'string' ],

    ];
  }

  public function attributeLabels(): array
  {
    return [
      'support_phone' => 'Служба поддержки',
      'phone'         => 'Контакнтый центр',
      'text'          => 'Текст',

    ];
  }

  public function formFields( array $data = [] )
  {
    $fields = [
      'support_phone' => [ 'type' => 'text', 'class' => [ 'form-control' ] ],
      'phone'         => [ 'type' => 'text', 'class' => [ 'form-control' ] ],
      'text'          => [ 'type' => 'textarea', 'class' => [ 'form-control' ] ],

    ];
    if ( count( $data ) > 0 ) {
      $fieldsFilter = [];
      foreach ( $data as $datum ) {
        if ( isset( $fields[$datum] ) )
          $fieldsFilter[$datum] = $fields[$datum];
      }
      return $fieldsFilter;
    }
    return $fields;
  }

  protected function beforeInsert( array $data ): array
  {
    $unixTime = $this->getUnixTime();

    return $data;
  }

  public static function get(){
    if(self::$contact == null){
      $model = new self();
      self::$contact = $model->fetchOne();
    }
    return self::$contact;
  }

}
