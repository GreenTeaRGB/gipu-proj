<?php

namespace Application\Models;
class Template extends \Application\Classes\Model implements \Application\Classes\Forms\Interfaces\FormBuilderInterface
{
  public function __construct()
  {
    parent::__construct( 'template', $this->getTypes(), $this );
  }

  public function getTypes()
  {
    return [
      'id'    => 'int',
      'name'  => 'str',
      'image' => 'str',
      'code'  => 'str',
      'block_id' => 'int'

    ];
  }

  public function getFields()
  {
    return array(
      'id' => array(
        'type'    => 'string',
        'visible' => false,
        'edited'  => false,
        'label'   => 'ID'
      ),

      'name' => array(
        'type'     => 'string',
        'visible'  => true,
        'edited'   => true,
        'label'    => 'Название',
        'required' => true,
        'multiple' => false,
      ),
      'block_id' => [
        'type'     => 'aselect',
        'visible'  => true,
        'edited'   => false,
        'values' => Block::find(),
        'id_key' => 'id',
        'name_key'  => 'name',
        'label'    => 'Родительский блок',
        'multiple' => false,
      ],
      'image' => array(
        'type'      => 'file',
        'visible'   => false,
        'edited'    => true,
        'label'     => 'Превью изображение',
        'required'  => true,
        'file-type' => 'image/*',
        'multiple'  => false,
      ),

      'code' => array(
        'type'     => 'text',
        'visible'  => false,
        'edited'   => true,
        'label'    => 'php код блока',
        'required' => true,
        'multiple' => false,
      ),

    );
  }

  public function rules(): array
  {
    return [
      [ [ "name", "code" ], 'required' ],
      [ [ "name" ], 'string' ],
      [ [ "image" ], 'image', 'extensions' => [ 'png', 'jpg', 'jpeg' ], 'skipOnEmpty' => isset($_POST['is_image']) ],

    ];
  }

  public function attributeLabels(): array
  {
    return [
      'name'  => 'Название',
      'image' => 'Превью изображение',
      'code'  => 'php код блока',

    ];
  }

  public function formFields( array $data = [] )
  {
    $fields = [
      'name'  => [ 'type' => 'text', 'class' => [ 'form-control' ] ],
      'image' => [ 'type' => 'file', 'class' => [ 'form-control' ] ],
      'code'  => [ 'type' => 'textarea', 'class' => [ 'form-control' ] ],

    ];
    if ( count( $data ) > 0 ) {
      $fieldsFilter = [];
      foreach ( $data as $datum ) {
        if ( isset( $fields[$datum] ) )
          $fieldsFilter[$datum] = $fields[$datum];
      }
      return $fieldsFilter;
    }
    return $fields;
  }

  protected function beforeInsert( array $data ): array
  {
    $unixTime = $this->getUnixTime();

    return $data;
  }


}
