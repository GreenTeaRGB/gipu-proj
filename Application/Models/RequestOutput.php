<?php

namespace Application\Models;

use Application\Helpers\DataHelper;
use Application\Helpers\UserHelper;

class RequestOutput extends \Application\Classes\Model implements \Application\Classes\Forms\Interfaces\FormBuilderInterface
{
  public function __construct()
  {
    parent::__construct( 'request_output', $this->getTypes(), $this );
  }

  public function getTypes()
  {
    return [
      'id'               => 'int',
      'created_at'       => 'str',
      'updated_at'       => 'str',
      'sum'              => 'str',
      'user_id'          => 'int',
      'status_id'        => 'int',
      'complete'         => 'int',
      'card'             => 'str',
      'juristic_type_id' => 'int',
      'success_date'     => 'int',
      'fail'             => 'int',

    ];
  }

  public function getFields()
  {
    return array(
      'id' => array(
        'type'    => 'string',
        'visible' => true,
        'edited'  => false,
        'sort'    => 1,
        'label'   => 'ID'
      ),

      'created_at' => array(
        'type'     => 'date',
        'visible'  => true,
        'edited'   => true,
        'label'    => 'Дата запроса',
        'required' => false,
        'sort'     => 5000,
        'multiple' => false,
      ),

      'updated_at' => array(
        'type'     => 'date',
        'visible'  => false,
        'edited'   => false,
        'label'    => 'Дата изменения',
        'required' => false,
        'sort'     => 5000,
        'multiple' => false,
      ),

      'sum' => array(
        'type'     => 'string',
        'visible'  => false,
        'edited'   => true,
        'label'    => 'Сумма',
        'required' => true,
        'sort'     => 5000,
        'multiple' => false,
      ),

      'user_id' => array(
        'type'     => 'modals',
        'visible'  => false,
        'edited'   => true,
        'label'    => 'Агент',
        'required' => false,
        'multiple' => false,
        'sort'     => 5000,
        'class'    => "\\Application\\Models\\User",
        'fields'   => [ 'name', 'last_name' ]
      ),

      'status_id'        => array(
        'type'     => 'select',
        'visible'  => false,
        'values'   => \Application\Helpers\DataHelper::REQUEST_OUTPUT_STATUS,
        'id_key'   => 'id',
        'name_key' => 'name',
        'edited'   => true,
        'required' => false,
        'label'    => 'Статус',
        'sort'     => 5000,
        'empty'    => true,
      ),
      //      'complete'         => [
      //        'type'     => 'read',
      //        'visible'  => true,
      //        'edited'   => true,
      //        'label'    => 'Выполнено',
      //        'required' => false,
      //        'sort' => 11000
      //      ],
      'card'             => [
        'type'     => 'read',
        'visible'  => true,
        'edited'   => true,
        'sort'     => 4500,
        'label'    => 'Р/С или карта',
        'required' => false
      ],
      'juristic_type_id' => [
        'type'     => 'select',
        'visible'  => false,
        'edited'   => false,
        'values'   => \Application\Models\JuristicType::getJuristicTypes(),
        'id_key'   => 'id',
        'name_key' => 'type',
        'label'    => 'Юр. лицо',
        'required' => false,
        'sort'     => 2000
      ],
      'bank_name'        => [
        'type'    => 'string',
        'visible' => true,
        'edited'  => false,
        'sort'    => 4400,
        'label'   => 'Название банка'
      ],
      'success_date'     => [
        'type'    => 'string',
        'visible' => true,
        'edited'  => false,
        'label'   => 'Дата исполнения',
        'sort'    => 12000
      ]
    );
  }

  public function rules(): array
  {
    $user = UserHelper::getUser();
    return [
      [ [ "user_id", "status_id" ], 'number' ],
      [ [ "sum" ], 'double' ],
      [ [ "sum" ], 'double', 'max' => $user['balance'] ],
      [ [ "created_at", "updated_at" ], 'date' ],
      [ [ "sum" ], 'required' ],

    ];
  }

  public function attributeLabels(): array
  {
    return [
      'created_at' => 'Дата создания',
      'updated_at' => 'Дата изменения',
      'sum'        => 'Сумма',
      'user_id'    => 'Агент',

    ];
  }

  public function formFields( array $data = [] )
  {
    $fields = [
      'created_at' => [ 'type' => 'date', 'class' => [ 'form-control' ] ],
      'updated_at' => [ 'type' => 'date', 'class' => [ 'form-control' ] ],
      'sum'        => [
        'type'  => 'text',
        'class' => [ 'form-control' ],
        'attr'  => [ 'placeholder' => 'Введите сумму которую хотите вывести' ]
      ],
      //          'user_id'    => [ 'type'   => 'select',
      //                            'values' => \Application\Models\::find(), 'id'=>'', 'name'=>'' ,'class'=>[ 'form-control' ]],

    ];
    if ( count( $data ) > 0 ) {
      $fieldsFilter = [];
      foreach ( $data as $datum ) {
        if ( isset( $fields[$datum] ) )
          $fieldsFilter[$datum] = $fields[$datum];
      }
      return $fieldsFilter;
    }
    return $fields;
  }


  protected function getSuccessDate( $element )
  {
    return self::dateFormat( $element, 'd m Y H:i:s' );
  }

  protected function setSuccessDate( $element )
  {
    return self::strtotime( $element );
  }

  protected function beforeInsert( $fields )
  {
    $unixTime             = $this->getUnixTime();
    $fields['created_at'] = $unixTime;
    $fields['updated_at'] = $unixTime;
    return $fields;
  }

  protected function beforeUpdate( $fields )
  {
    $unixTime             = $this->getUnixTime();
    $fields['updated_at'] = $unixTime;
    return $fields;
  }

  protected function afterFetchAll( $data )
  {
    foreach ( $data as $key => $element ) {
      $data[$key]['status_name'] = DataHelper::REQUEST_OUTPUT_STATUS[$element['status_id']]['name'];
    }
    return $data;
  }

  protected function setCreatedAt( $element )
  {
    return strtotime( $element );
  }

  protected function setUpdatedAt( $element )
  {
    return strtotime( $element );
  }


  protected function getCreatedAt( $element )
  {
    return date( 'd.m.Y', $element );
  }

  protected function getUpdatedAt( $element )
  {
    return date( 'd.m.Y', $element );
  }


}
