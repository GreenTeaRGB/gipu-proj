<?php
	
	namespace Application\Models;
	class NewsCategory extends \Application\Classes\Model
	{
		public function __construct()
		{
			parent::__construct('news_category', $this->getTypes(), $this);
		}
		
		public function getTypes()
		{
			return [
				'id'   => 'int',
				'name' => 'str',
				'sort' => 'int',
			
			];
		}
		
		public function getFields()
		{
			return array(
				'id' => array(
					'type'    => 'string',
					'visible' => true,
					'edited'  => false,
					'label'   => 'ID'
				),
				
				'name' => array(
					'type'     => 'text',
					'visible'  => true,
					'edited'   => true,
					'label'    => 'Имя категории',
					'required' => true,
					'multiple' => false,
				),
				
				'alias' => array(
					'type'     => 'text',
					'visible'  => true,
					'edited'   => true,
					'label'    => 'Привязка',
					'required' => true,
					'multiple' => false,
				),
				
				'sort' => array(
					'type'     => 'string',
					'visible'  => true,
					'edited'   => true,
					'label'    => 'Сортировка',
					'required' => false,
					'multiple' => false,
				),
			
			);
		}
		
		public function rules()
		{
			return [
				[ [ "name", "alias" ], 'string' ],
        ['alias' , 'link'],
				[ [ "sort" ], 'number' ],
				[ [ "name", "alias" ], 'required' ],
			
			];
		}
		
		public function attributeLabels()
		{
			return [
				'name' => 'Имя категории',
				'sort' => 'Сортировка',
				'alias'=> 'Привязка',
			];
		}
		
		public function formFields(array $data = [])
		{
			$fields = [
				'name'  => ['type' => 'textarea', 'class' => ['form-control']],
				'alias' => ['type' => 'text', 'class' => ['form-control']],
				'sort'  => ['type' => 'text', 'class' => ['form-control']],
			];
			if ( count($data) > 0 ) {
				$fieldsFilter = [];
				foreach ($data as $datum) {
					if ( isset($fields[$datum]) )
						$fieldsFilter[$datum] = $fields[$datum];
				}
				return $fieldsFilter;
			}
			return $fields;
		}
		
		public static function find($filter = [], $fetchOne = false)
		{
			$model = new NewsCategory();
			if ( count($filter) > 0 ) $model->where($filter);
			
			if ( $fetchOne ) {
				return $model->fetchOne();
			}
			return $model->fetchAll();
		}
		
		public static function findOne($id)
		{
			$model = new NewsCategory();
			return $model->where(['id' => $id])->fetchOne();
		}
		
		public static function create($data)
		{
			$model = new NewsCategory();
			return $model->setFields($data)->insert();
		}
		
		public static function deleteById($id)
		{
			$model = new NewsCategory();
			return $model->where(['id' => $id])->delete();
		}
		
		public static function updateById($id, $data)
		{
			$model = new NewsCategory();
			return $model->setFields($data)->where(['id' => $id])->update();
		}
		
		protected function afterDelete( $model )
		{
      if(!parent::afterDelete($model)) return;
			$newsCategory = $this->getDeletedItems();
			$ids = array_column($newsCategory, 'id');
			News::deleteFilter(['category_id'=>$ids]);
			self::removeFiles(array_column($newsCategory, 'image'));
		}
	}
