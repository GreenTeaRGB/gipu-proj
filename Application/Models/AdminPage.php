<?php

namespace Application\Models;
class AdminPage extends \Application\Classes\Model
{
  public function __construct()
  {
    parent::__construct( 'admin_pages', $this->getTypes(), $this );
  }

  public function getTypes()
  {
    return array(
      'id'          => 'int',
      'name'        => 'str',
      'icon'        => 'str',
      'uri'         => 'str',
      'sort_order'  => 'int',
      'site_access' => 'int',
      'visible'     => 'int',
      'category_id' => 'int'
    );
  }

  public static function find( $filter = [] )
  {
    $model = new AdminPage();
    if ( count( $filter ) > 0 ) $model->where( $filter );
    return $model->fetchAll();
  }

  public static function findOne( $filter )
  {
    $model = new AdminPage();
    return $model->where( $filter )->fetchOne();
  }

  public static function getListByUserGroup( $groupId, $visible = true )
  {
    if ( $visible ) $add = " AND `admin_pages`.`visible`='1'";
    else $add = '';
    $model = new AdminPage();
    $item  = $model->sql( "SELECT `user_group_rights`.*, `admin_pages`.* FROM `user_group_rights`,`admin_pages` WHERE `user_group_rights`.`admin_page_id`=`admin_pages`.`id` AND `user_group_rights`.`user_group_id`=$groupId $add  ORDER BY `admin_pages`.`sort_order` ASC" )
                   ->fetchAll();
    return $item;
  }
}   
