<?php

namespace Application\Models;

use Application\Helpers\DataHelper;
use Application\Helpers\UserHelper;
use Exception;

class Shop extends \Application\Classes\Model implements \Application\Classes\Forms\Interfaces\FormBuilderInterface
{

  public static $maxCategories = 1;

  public static $afterFetchAll = true;

  public function __construct()
  {
    parent::__construct( 'shop', $this->getTypes(), $this );
  }

  public function getTypes()
  {
    return [
      'id'                => 'int',
      'created_at'        => 'str',
      'updated_at'        => 'str',
      'date_active_start' => 'str',
      'date_active_end'   => 'str',
      'seller_id'         => 'int',
      'name'              => 'str',
      'url'               => 'str',
      'status'            => 'int',
      'rate'              => 'int',
      //				'categories' => 'str',
      'paid'              => 'int',
      'config'            => 'str'
    ];
  }

  public function getFields()
  {
    return array(
      'id' => array(
        'type'    => 'string',
        'visible' => true,
        'edited'  => false,
        'label'   => 'ID'
      ),

      'created_at' => array(
        'type'     => 'date',
        'visible'  => true,
        'edited'   => true,
        'label'    => 'Дата создания',
        'required' => false,
        'multiple' => false,
      ),

      'updated_at' => array(
        'type'     => 'date',
        'visible'  => false,
        'edited'   => true,
        'label'    => 'дата обновления',
        'required' => false,
        'multiple' => false,
      ),

      'date_active_start' => array(
        'type'     => 'date',
        'visible'  => false,
        'edited'   => true,
        'label'    => 'Дата активации',
        'required' => false,
        'multiple' => false,
      ),

      'date_active_end' => array(
        'type'     => 'date',
        'visible'  => false,
        'edited'   => true,
        'label'    => 'Дата окончания аренды',
        'required' => false,
        'multiple' => false,
      ),

      'seller_id' => array(
        'type'     => 'select',
        'values'   => User::find( [ 'type' => DataHelper::DEFAULT_SELLER_TYPE_ID ] ),
        'id_key'   => 'id',
        'name_key' => 'name',
        'visible'  => false,
        'edited'   => true,
        'label'    => 'Продавец',
        'required' => false,
        'multiple' => true,
      ),

      'name' => array(
        'type'     => 'string',
        'visible'  => true,
        'edited'   => true,
        'label'    => 'Название',
        'required' => false,
        'multiple' => false,
      ),

      'url' => array(
        'type'     => 'string',
        'visible'  => false,
        'edited'   => true,
        'label'    => 'Адресс магазина',
        'required' => false,
        'multiple' => false,
      ),

      'status' => array(
        'type'     => 'select',
        'values'   => ShopStatus::find(),
        'id_key'   => 'id',
        'name_key' => 'name',
        'visible'  => false,
        'edited'   => true,
        'label'    => 'Статус',
        'required' => false,
        'multiple' => true,
      ),

      'rate' => array(
        'type'     => 'select',
        'values'   => Rate::find(),
        'id_key'   => 'id',
        'name_key' => 'name',
        'visible'  => false,
        'edited'   => true,
        'label'    => 'Тариф',
        'required' => false,
        'multiple' => true,
      ),

      'categories' => array(
        'type'     => 'mselect',
        'values'   => Category::find(),
        'id_key'   => 'id',
        'name_key' => 'name',
        'visible'  => false,
        'edited'   => true,
        'label'    => 'Категории',
        'required' => false,
        'multiple' => true,
        'merge'    => [
          'class'       => '\Application\Models\ShopCategories',
          'field'       => 'shop_id',
          'item'        => 'id',
          'class_field' => 'category_id'
        ]
      ),
      'paid'       => array(
        'type'     => 'checkbox',
        'visible'  => false,
        'edited'   => true,
        'label'    => 'Оплачен',
        'required' => false,
        'multiple' => false,
      ),
    );
  }

  public function rules(): array
  {
    return [
      [ [ "name", "url", "categories", 'rate' ], 'required', 'on' => 'default' ],
      [ 'name', 'required', 'on' => 'update_name', 'except' => 'default' ],
      [
        'categories',
        'each',
        'rule'     => [ 'number' ],
        'max'      => self::$maxCategories,
        'tooBig'   => 'Вы не можете выбрать больше {max} категорий',
        'tooSmall' => 'Вы не можете выбрать меньше {min} категорий',
      ],
      [ [ "name", "url", 'config' ], 'string' ],
      [ 'url', 'url' ],
      [ 'rate', 'number' ],
      [ [ "seller_id", "status" ], 'number' ],
      [ [ "created_at", "updated_at", 'date_active_start', 'date_active_end' ], 'date' ],

    ];
  }

  public function attributeLabels(): array
  {
    return [
      'created_at' => 'Дата создания',
      'updated_at' => 'дата обновления',
      'seller_id'  => 'Продавец',
      'name'       => 'Название',
      'url'        => 'Адресс магазина',
      'status'     => 'Статус',
      'rate'       => 'Тариф',
      'categories' => 'Категории',

    ];
  }

  public function formFields( array $data = [] ): array
  {
    $count  = count( $data );
    $fields = [
      'created_at' => [ 'type' => 'date', 'class' => [ 'form-control' ] ],
      'updated_at' => [ 'type' => 'date', 'class' => [ 'form-control' ] ],
      'name'       => [ 'type' => 'text', 'class' => [ 'form-control' ] ],
      'url'        => [ 'type' => 'text', 'class' => [ 'form-control' ] ],
      'rate'       => [ 'type' => 'hidden', 'class' => [ 'form-control' ] ],
    ];
    if ( in_array( 'seller_id', $data ) && $count > 0 )
      $fields['seller_id'] = [
        'type'   => 'select',
        'values' => \Application\Models\Seller::find(),
        'id'     => 'id',
        'name'   => 'name',
        'class'  => [ 'form-control' ]
      ];
    if ( in_array( 'categories', $data ) && $count > 0 )
      $fields['categories'] = [
        'type'   => 'mselect',
        'values' => \Application\Models\Category::find( [ 'parent_id' => 0 ] ),
        'id'     => 'id',
        'name'   => 'name',
        'class'  => [ 'form-control' ]
      ];

    if ( in_array( 'status', $data ) && $count > 0 )
      $fields['status'] = [
        'type'   => 'select',
        'values' => \Application\Models\ShopStatus::find(),
        'id'     => 'id',
        'name'   => 'name',
        'class'  => [ 'form-control' ]
      ];

    if ( in_array( 'rate_id', $data ) && $count > 0 )
      $fields['rate'] = [
        'type'   => 'select',
        'values' => \Application\Models\Rate::find(),
        'id'     => 'id',
        'name'   => 'name',
        'class'  => [ 'form-control' ]
      ];

    if ( count( $data ) > 0 ) {
      $fieldsFilter = [];
      foreach ( $data as $datum ) {
        if ( isset( $fields[$datum] ) )
          $fieldsFilter[$datum] = $fields[$datum];
      }
      return $fieldsFilter;
    }
    return $fields;
  }

  protected function setCreatedAt( $element )
  {
    return strtotime( $element );
  }

  protected function setUpdatedAt( $element )
  {
    return strtotime( $element );
  }

  protected function setDateActiveStart( $element )
  {
    return strtotime( $element );
  }

  protected function setDateActiveEnd( $element )
  {
    return strtotime( $element );
  }

  protected function setConfig( $element )
  {
    return serialize( $element );
  }

  protected function getConfig( $element )
  {
    return unserialize( $element );
  }

  protected function getCreatedAt( $element )
  {
    return date( 'd.m.Y', $element );
  }

  protected function getUpdatedAt( $element )
  {
    return date( 'd.m.Y', $element );
  }

  protected function getDateActiveStart( $element )
  {
    if ( self::getModule() != 'admin' )
      return DataHelper::redate( 'd M Yг.', $element );
    return date( 'd.m.Y', $element );
  }

  protected function getDateActiveEnd( $element )
  {
    if ( self::getModule() != 'admin' )
      return DataHelper::redate( 'd M Yг.', $element );
    return date( 'd.m.Y', $element );
  }

  protected function beforeInsert( $data )
  {
    $status                    = ShopStatus::findOne( [ 'code' => 'mod' ] );
    $unixTime                  = $this->getUnixTime();
    $data['created_at']        = $unixTime;
    $data['updated_at']        = $unixTime;
    $data['date_active_start'] = $unixTime;
    $data['date_active_end']   = $unixTime;
    $data['seller_id']         = UserHelper::getUser()['id'];
    $data['paid']              = 0;
    $data['status']            = $status ? $status['id'] : 0;
    $data['config']            = 'a:3:{s:6:"header";i:1;s:6:"blocks";a:1:{i:0;a:3:{s:4:"type";s:6:"slider";s:9:"variation";i:1;s:7:"options";a:0:{}}}s:6:"footer";i:1;}';
    return $data;
  }

  protected function beforeUpdate( $data )
  {
    $unixTime           = $this->getUnixTime();
    $data['updated_at'] = $unixTime;
    return $data;
  }

  protected function afterFetchAll( $data )
  {
    if(!self::$afterFetchAll) return $data;
    $rates          = Rate::find();
    $status         = ShopStatus::find();
    $categoriesSort = [];
    $categories     = ShopCategories::find( [ 'shop_id' => array_column( $data, 'id' ) ] );
    foreach ( $categories as $category ) {
      $categoriesSort[$category['shop_id']][] = $category;
    }
    foreach ( $data as $id => $element ) {
      $data[$id]['status_name'] = $status[$element['status']]['name'];
      if ( $element['rate'] == 0 ) {
        $data[$id]['rate_name'] = 'Не определено';
        continue;
      }
      $data[$id]['rate_name'] = $rates[$element['rate']]['name'];
      if ( isset( $categoriesSort[$id] ) )
        $data[$id]['categories'] = $categoriesSort[$id];
    }
    return $data;
  }

  protected function afterFetchOne( $data )
  {
    $categories         = ShopCategories::find( [ 'shop_id' => $data['id'] ] );
    $data['categories'] = array_column( $categories, 'category_id' );
    return $data;
  }

  protected function afterInsert( $id )
  {
    $insert = [];
    $scope  = isset( $_POST['Shop'] ) ? null : '';
    $this->load( $_POST, $scope );
    $model = new ShopCategories();
    if ( !is_null( $this->categories ) ) {
      foreach ( $this->categories as $category ) {
        $insert[] = [ 'category_id' => $category, 'shop_id' => $id ];
      }
      $model->setAllFields( $insert )->insertAll();
    }
  }

  protected function afterUpdate( $result )
  {
    $scope       = isset( $_POST['Shop'] ) ? null : '';
    $whereParams = $this->getLastState()->getWhereParams();
    if ( $this->load( $_POST, $scope ) ) {
      $id = isset( $whereParams['id'] ) ? $whereParams['id'] : false;

      if ( $id && isset( $this->categories ) && is_array( $this->categories ) ) {
        $model = new ShopCategories();
        $model->where( [ 'shop_id' => $id ] )->delete();
        foreach ( $this->categories as $category ) {
          $insert[] = [ 'category_id' => $category, 'shop_id' => $id ];
        }
        $model->setAllFields( $insert )->insertAll();
      }
    }
  }

  protected function beforeDelete( $id ): bool
  {
    return false;
  }

  public static function findInfo( $id )
  {
    $model = new self();
    $user  = UserHelper::getUser();
    if ( !$user ) return false;
    $sql
      = 'SELECT `s`.*, `ss`.`name` as `status_name`, `r`.`name` as `rate_name` FROM `shop` as `s`
				 LEFT JOIN `shop_status` as `ss` ON `ss`.`id` = `s`.`status`
				 LEFT JOIN `rate` as `r` ON `r`.`id` = `s`.`rate`
				 WHERE `s`.`id` = ' . (int)$id . ' AND `seller_id` = ' . (int)$user['id'];
    return $model->sql( $sql )->fetchOne();
  }

  public static function findData( $id )
  {
    $model = new self();
    self::$afterFetchAll = false;
    $shop  = $model::findInfo( $id );
    if ( $shop === false ) return false;
    $sql = 'SELECT `c`.* FROM `category` as `c` 
            LEFT JOIN `shop_categories` as `sh` ON `sh`.`category_id` = `c`.`id` 
            WHERE `sh`.`shop_id` = ' . (int)$id;

    $shop['categories'] = $model->sql( $sql )->fetchAll();
    return $shop;
  }

}
