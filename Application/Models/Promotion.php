<?php
	
	namespace Application\Models;
	
	class Promotion extends \Application\Classes\Model
	{
		protected $tempRequest = null;
		
		public function __construct()
		{
			parent::__construct('promotion', $this->getTypes(), $this);
		}
		
		public function getTypes()
		{
			return array(
				'id'          => 'int',
				'name'        => 'str',
				'description' => 'str',
				'percent'     => 'int',
				'owner_id'    => 'int',
				'start'       => 'int',
				'finish'      => 'int',
				'is_active'   => 'int',
				'alias'       => 'str ',
			);
		}
		
		public function beforeInsert($data)
		{
			$data['start'] = strtotime($data['start']);
			$data['finish'] = strtotime($data['finish']);
			
			return $data;
		}
		
		protected function saveInRelated($promotionId)
		{
			if ( !is_null($this->tempRequest) ) {
				$relatedModel = new ProductPromotion();
				
				$this->removeFromRelatedById($relatedModel, $promotionId);
				$relatedProducts = $this->tempRequest['related_products'];
				
				if ( $relatedProducts && count($relatedProducts) > 0 ) {
					$sql = "
						INSERT INTO `product_promotion` (id, promotion_id, product_id)
						VALUES ";
					
					foreach ($this->tempRequest['related_products'] as $key => $productId) {
						$productId = (int)$productId;
						if ( $key < count($this->tempRequest['related_products']) - 1 ) {
							$sql .= "('', '$promotionId', '$productId'),";
						} else {
							$sql .= "('', '$promotionId', '$productId')";
						}
					}
					$relatedModel->sql($sql)->exec();
					
					$this->clearTempRequest();
					
					return true;
				} else {
					return false;
				}
			}
			
			return true;
		}
		
		protected function afterFetchOne($data)
		{
			$data['start'] = date("d.m.Y", $data['start']);
			$data['finish'] = date("d.m.Y", $data['finish']);
			$model = new Promotion();
			
			if ( $data['id'] ) {
				$sql = "
					SELECT prod.*
					FROM `promotion` as p
						LEFT JOIN `product_promotion` as pp ON p.`id` = pp.`promotion_id`
						LEFT JOIN `product` as prod ON prod.`id` = pp.`product_id`
					WHERE p.`id` = '{$data["id"]}'
				";
				
				$relatedProducts = $model->sql($sql)->fetchAll();
				
				$data['related_products'] = $relatedProducts;
			}
			
			return $data;
		}
		
		protected function afterInsert($promotionId)
		{
			$done = $this->saveInRelated($promotionId);
			
			if ( $done ) {
				return true;
			}
			
			return false;
		}
		
		protected function beforeUpdate($data)
		{
			$data['start'] = strtotime($data['start']);
			$data['finish'] = strtotime($data['finish']);
			
			return $data;
		}
		
		protected function afterUpdate()
		{
			$promotionId = (!is_null($this->tempRequest)) ? $this->tempRequest['promotion_id'] : '';
			
			if ( $promotionId ) {
				$done = $this->saveInRelated((int)$promotionId);
				
				if ( $done ) {
					return true;
				}
			}
			return false;
		}
		
		protected function removeFromRelatedById($model, $promotionId)
		{
			
			
			$sql = "
					DELETE pp.* FROM `product_promotion` as pp
					WHERE pp.`promotion_id` = '$promotionId'
				";
			
			$model->sql($sql)->exec();
		}
		
		public function setTempRequest($request)
		{
			$this->tempRequest = $request;
		}
		
		protected function clearTempRequest()
		{
			$this->tempRequest = null;
		}
		
		
		protected function beforeDelete($model): bool 
		{
			$relatedModel = new ProductPromotion();
			$promotionId = (!is_null($this->tempRequest)) ? $this->tempRequest['promotion_id'] : '';
			$this->removeFromRelatedById($relatedModel, $promotionId);
			
			$this->clearTempRequest();
			
			return true;
		}
		
		
		public static function find($filter = [], $fetchOne = false)
		{
			$model = new Promotion();
			if ( count($filter) > 0 ) $model->where($filter);
			
			if ( $fetchOne ) {
				return $model->fetchOne();
			}
			return $model->fetchAll();
		}
		
		public static function getActivePromotions()
		{
			$model = new Promotion();
			$currentTimestamp = $model->getUnixTime();
			
			$sql = "
				SELECT p.*
				FROM `promotion` as p
				WHERE p.`is_active` = 1
				AND p.`finish` >= '$currentTimestamp'
			";
			
			/*AND p.`start` <= '$currentTimestamp'*/
			
			return $model->sql($sql)->fetchAll();
		}
	}