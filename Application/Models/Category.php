<?php

namespace Application\Models;

use Application\Helpers\Image;

/**
 * Class Category
 * @package Application\Models
 */
class Category extends \Application\Classes\Model
{

  /**
   * @var null
   */
  private static $fetchAllData = null;

  /**
   * Category constructor.
   */
  public function __construct()
  {
    parent::__construct( 'category', $this->getTypes(), $this );
  }

  /**
   * @return array|mixed
   */
  public function getTypes()
  {
    return array(
      'id'               => 'int',
      'name'             => 'str',
      'created_at'       => 'int',
      'updated_at'       => 'int',
      'category_icon'    => 'str',
      'sort'             => 'int',
      'parent_id'        => 'int',
      'category_image'   => 'str',
      'alias'            => 'str',
      'meta_title'       => 'str',
      'meta_description' => 'str',
      'meta_keywords'    => 'str',
      'commission'       => 'str',
      'active'           => 'int',
    );
  }

  /**
   * @return array
   */
  public function getFields()
  {
    return [
      'name'             => [
        'type'     => 'string',
        'label'    => 'Название',
        'edited'   => true,
        'visible'  => true,
        'required' => false,
      ],
      'created_at'       => [
        'type'     => 'date',
        'label'    => 'Дата создания',
        'edited'   => false,
        'visible'  => false,
        'required' => false,
      ],
      'updated_at'       => [
        'type'     => 'date',
        'label'    => 'Дата обновления',
        'edited'   => false,
        'visible'  => false,
        'required' => false,
      ],
      'category_icon'    => [
        'type'     => 'afile',
        'label'    => 'Иконка',
        'edited'   => false,
        'visible'  => false,
        'required' => false,
        'multiple' => false
      ],
      'sort'             => [
        'type'     => 'string',
        'label'    => 'Сортировка',
        'edited'   => false,
        'visible'  => false,
        'required' => false,
      ],
      'parent_id'        => [
        'type'     => 'selectTree',
        'label'    => 'Родительская категория',
        'values'   => Category::getFullList(),
        'id_key'   => 'id',
        'name_key' => 'name',
        'edited'   => true,
        'visible'  => false,
        'required' => false,
      ],
      'category_image'   => [
        'type'         => 'afile',
        'visible'      => false,
        'edited'       => true,
        'required'     => false,
        'label'        => 'Изображение',
        'multiple'     => false,
        'file-type'    => 'image/*',
        'whereVisible' => [ 'field' => 'parent_id', 'operation' => '!', 'value' => '0' ]
      ],
      'alias'            => [
        'type'     => 'string',
        'label'    => 'Ссылка',
        'edited'   => true,
        'visible'  => false,
        'required' => false,
      ],
      'meta_title'       => [
        'type'     => 'string',
        'label'    => 'Meta заголовок',
        'edited'   => true,
        'visible'  => false,
        'required' => false,
      ],
      'meta_description' => [
        'type'     => 'string',
        'label'    => 'Meta описание',
        'edited'   => true,
        'visible'  => false,
        'required' => false,
      ],
      'meta_keywords'    => [
        'type'     => 'string',
        'label'    => 'Ключевые слова',
        'edited'   => true,
        'visible'  => false,
        'required' => false,
      ],
      'commission'       => [
        'type'     => 'string',
        'label'    => 'Комиссия',
        'edited'   => false,
        'visible'  => false,
        'required' => false,
      ],
      'balls'            => [
        'type'     => 'string',
        'label'    => 'Стоимость категории в баллах',
        'edited'   => true,
        'visible'  => false,
        'required' => false,
      ]
    ];
  }

  /**
   * @return array
   */
  public function rules()
  {
    return [
      [ [ "name" ], 'string' ],
      [ [ "sort", "commission" ], 'number' ],
      [ [ 'alias' ], 'link' ],
      [ 'alias', 'unique' ],
      [ [ "created_at", "updated_at" ], 'date' ],
      [ [ "category_image" ], 'image', 'extensions' => [ 'png', 'jpg', 'jpeg' ] ],
      [ [ "name", "alias" ], 'required' ],
    ];
  }

  /**
   * @param $element
   * @return false|string
   */
  protected function getCreatedAt( $element )
  {
    return date( 'd.m.Y', $element );
  }

  /**
   * @param $element
   * @return false|string
   */
  protected function getUpdatedAt( $element )
  {
    return date( 'd.m.Y', $element );
  }

  /**
   * @param $element
   * @return false|int
   */
  protected function setCreatedAt( $element )
  {
    return strtotime( $element );
  }

  /**
   * @param $element
   * @return false|int
   */
  protected function setUpdatedAt( $element )
  {
    return strtotime( $element );
  }

  /**
   * @param $element
   * @return Image
   */
  protected function getCategoryImage( $element )
  {
    if ( self::getModule() == 'admin' )
      return $element;
    if ( $element == '' ) {
      return new Image( '/images/no_photo.png' );
    }
    return new Image( $element );
  }

  /**
   * @param array $data
   * @return array
   */
  protected function beforeInsert( array $data ): array
  {
    $unixTime           = $this->getUnixTime();
    $data['created_at'] = $unixTime;
    $data['updated_at'] = $unixTime;
    return $data;
  }

  /**
   * @param array $filter
   * @param bool  $fetchOne
   * @return array|mixed
   */
  public static function find( $filter = [], $fetchOne = false )
  {
    $model = new Category();
    if ( count( $filter ) > 0 )
      $model->where( $filter );

    if ( $fetchOne ) {
      return $model->fetchOne();
    }
    return $model->fetchAll();
  }

  /**
   * @param $id
   * @return mixed
   */
  public static function findOne( $id )
  {
    if ( self::$fetchAllData !== null && isset( self::$fetchAllData[$id] ) )
      return self::$fetchAllData[$id];
    $model = new Category();
    return $model->where( [ 'id' => $id ] )->fetchOne();
  }

  /**
   * @return array|null
   */
  public static function getFullList()
  {
    if ( self::$fetchAllData === null ) {
      $model              = new Category();
      self::$fetchAllData = $model->where( [ 'active' => 1 ] )->order( [ 'sort' => 'ASC' ] )->fetchAll();
    }
    return self::$fetchAllData;
  }

  /**
   * @return array
   */
  public static function findTree()
  {
    $return = [];
    $data   = self::getFullList();
    foreach ( $data as $datum ) {
      $return[$datum['parent_id']][] = $datum;
    }
    return $return;
  }

  /**
   * @param       $id
   * @param array $ids
   * @return array
   */
  public static function makeParentCategories( $id, $ids = [] )
  {
    $categories = self::getFullList();
    if ( !isset( $categories[$id] ) ) return [];
    $activeCategory = $categories[$id];
    $ids[]          = $activeCategory['id'];
    if ( $activeCategory['parent_id'] == 0 ) {
      return $ids;
    }
    $ids = array_merge( $ids, self::makeParentCategories( $activeCategory['parent_id'], $ids ) );
    return $ids;
  }

  /**
   * @param $model
   * @return bool|void
   */
  public function afterDelete( $model )
  {
    if ( !parent::afterDelete( $model ) ) return;
    $categories = $this->getDeletedItems();
    $ids        = array_column( $categories, 'id' );
    self::deleteFilter( [ 'parent_id' => $ids ] );
    Product::deleteFilter( [ 'category_id' => $ids ] );
    self::removeFiles( array_column( $categories, 'images' ) );
//	      ShopCategories::deleteFilter(['category_id'=>$ids]);
  }

  /**
   * @param $ids
   * @return array
   */
  public static function getTwoLevelByIds( $ids )
  {
    $rootCategories = self::find( [ 'id' => $ids ] );
    $allId          = array_column( $rootCategories, 'id' );
    $categories     = self::find( [ 'parent_id' => $allId ] );
    foreach ( $categories as $category ) {
      $allId[]                                         = $category['id'];
      $rootCategories[$category['parent_id']]['sub'][] = $category;
    }
    return [ $rootCategories, $allId ];
  }

}