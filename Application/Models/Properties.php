<?php

  namespace Application\Models;

  use Application\Helpers\DataHelper;

  class Properties extends \Application\Classes\Model
  {
    public function __construct()
    {
      parent::__construct( 'properties', $this->getTypes(), $this );
    }

    public function getTypes()
    {
      return [
          'id'                   => 'int',
          'name'                 => 'str',
          'code'                 => 'str',
          'active'               => 'int',
          'sort'                 => 'int',
          'multi'                => 'int',
          'required'             => 'int',
          'filter'               => 'int',
          'property_description' => 'str',
          'main'                 => 'int',
          'type'                 => 'str',
          'template'             => 'str',
          'template_filter'      => 'str',
          'property_group_id'    => 'int',
          'description_field'    => 'int',
          'category_id'          => 'int',
          'redactor'             => 'int',
      ];
    }

    public function getFields()
    {
      return array(
          'id' => array(
              'type'    => 'string',
              'visible' => true,
              'edited'  => false,
              'label'   => 'ID'
          ),

          'name' => array(
              'type'     => 'string',
              'visible'  => true,
              'edited'   => true,
              'label'    => 'Название свойства',
              'required' => false,
              'multiple' => false,
          ),

          'code' => array(
              'type'     => 'string',
              'visible'  => true,
              'edited'   => true,
              'label'    => 'Код свойства',
              'required' => false,
              'multiple' => false,
          ),

          'active' => array(
              'type'     => 'checkbox',
              'visible'  => false,
              'edited'   => true,
              'label'    => 'Активность',
              'required' => false,
              'multiple' => false,
          ),

          'sort' => array(
              'type'     => 'string',
              'visible'  => false,
              'edited'   => false,
              'label'    => 'Сортировка',
              'required' => false,
              'multiple' => false,
          ),

          'multi' => array(
              'type'     => 'checkbox',
              'visible'  => false,
              'edited'   => true,
              'label'    => 'Множественное свойство',
              'required' => false,
              'multiple' => false,
          ),

          'required' => array(
              'type'     => 'checkbox',
              'visible'  => false,
              'edited'   => true,
              'label'    => 'Обязательное свойство',
              'required' => false,
              'multiple' => false,
          ),

          'filter' => array(
              'type'     => 'checkbox',
              'visible'  => false,
              'edited'   => true,
              'label'    => 'Отображение в фильтре каталога',
              'required' => false,
              'multiple' => false,
          ),

          'property_description' => array(
              'type'     => 'text',
              'visible'  => false,
              'edited'   => true,
              'label'    => 'Описание',
              'required' => false,
              'multiple' => false,
          ),

          'main' => array(
              'type'     => 'checkbox',
              'visible'  => false,
              'edited'   => true,
              'label'    => 'Общее для всех категорий свойство',
              'required' => false,
              'multiple' => false,
          ),

          'type' => array(
              'type'     => 'string',
              'visible'  => false,
              'edited'   => true,
              'label'    => 'Тип',
              'required' => false,
              'multiple' => false,
          ),

          'template' => array(
              'type'     => 'string',
              'visible'  => false,
              'edited'   => true,
              'label'    => 'Шаблон',
              'required' => false,
              'multiple' => false,
          ),

          'template_filter' => array(
              'type'     => 'string',
              'visible'  => false,
              'edited'   => true,
              'label'    => 'Шаблон в фильтре каталога',
              'required' => false,
              'multiple' => false,
          ),

          'property_group_id' => array(
              'type'     => 'select',
              'visible'  => false,
              'values'   => PropertyGroup::find( [ 'active' => 1 ] ),
              'id_key'   => 'id',
              'name_key' => 'type',
              'edited'   => true,
              'required' => false,
              'label'    => 'Идентификатор группы свойств',
          ),

          'category_id' => array(
              'type'     => 'select',
              'visible'  => false,
              'values'   => Category::find( [ 'active' => 1 ] ),
              'id_key'   => 'id',
              'name_key' => 'name',
              'edited'   => true,
              'required' => false,
              'label'    => 'Категория',
          ),

      );
    }
	
	  public function rules()
	  {
		  return [
			  [ [ "name", "code", "type", "template", "template_filter" ], 'string' ],
        [['name', 'code'], 'unique'],
			  [ [ "active", "sort", "multi", "required", "filter", "main", "property_group_id" ], 'number' ],
			  [ [ "name" ], 'required'],
		  ];
	  }

    public function attributeLabels()
    {
      return [
          'name'                 => 'Название свойства',
          'code'                 => 'Код свойства',
          'active'               => 'Активность',
          'sort'                 => 'Сортировка',
          'multi'                => 'Множественное свойство',
          'required'             => 'Обязательное свойство',
          'filter'               => 'Отображение в фильтре каталога',
          'property_description' => 'Описание',
          'main'                 => 'Общее для всех категорий свойство',
          'type'                 => 'Тип',
          'template'             => 'Шаблон',
          'template_filter'      => 'Шаблон в фильтре каталога',
          'property_group_id'    => 'Идентификатор группы свойств',
          'description_field'    => 'Отображать дополнительное поле с описанием',
          'category_id'          => 'Категория',

      ];
    }

    public function formFields( array $data = [] )
    {
      $fields = [
          'name'                 => [ 'type' => 'text', 'class' => [ 'form-control' ] ],
          'code'                 => [ 'type' => 'text', 'class' => [ 'form-control' ] ],
          'active'               => [ 'type' => 'checkbox', 'class' => [ 'form-control' ] ],
          'sort'                 => [ 'type' => 'text', 'class' => [ 'form-control' ] ],
          'multi'                => [ 'type' => 'checkbox', 'class' => [ 'form-control' ] ],
          'required'             => [ 'type' => 'checkbox', 'class' => [ 'form-control' ] ],
          'filter'               => [ 'type' => 'checkbox', 'class' => [ 'form-control' ] ],
          'property_description' => [ 'type' => 'textarea', 'class' => [ 'form-control' ] ],
          'main'                 => [ 'type' => 'checkbox', 'class' => [ 'form-control' ] ],
          'type'                 => [ 'type' => 'text', 'class' => [ 'form-control' ] ],
          'template'             => [ 'type' => 'text', 'class' => [ 'form-control' ] ],
          'template_filter'      => [ 'type' => 'text', 'class' => [ 'form-control' ] ],
          'property_group_id'    => [ 'type' => 'text', 'class' => [ 'form-control' ] ],

      ];
      if( count( $data ) > 0 ) {
        $fieldsFilter = [];
        foreach( $data as $datum ) {
          if( isset( $fields[$datum] ) )
            $fieldsFilter[$datum] = $fields[$datum];
        }
        return $fieldsFilter;
      }
      return $fields;
    }

    public static function findByCode( $code )
    {
      $model = new Properties();
      return $model->where( [ 'code' => $code ] )->fetchOne();
    }

    protected function afterFetchAll( $properties )
    {
      if(count($properties)) {
        $propertyListType   = [];
        $propertiesGroupIds = [];
        foreach ( $properties as $property ) {
          if ( $property['type'] == DataHelper::PROPERTY_LIST_TYPE ) {
            $propertyListType[] = $property['id'];
          }
          if ( !in_array( $property['property_group_id'], $propertiesGroupIds ) ) {
            $propertiesGroupIds[] = $property['property_group_id'];
          }
        }
        if(count($propertiesGroupIds)){
          $propertyGroups = PropertyGroup::find( [ 'id' => $propertiesGroupIds ] );
          $propertyGroupsById = [];
          foreach ( $propertyGroups as $propertyGroup ) {
            $propertyGroupsById[$propertyGroup['id']] = $propertyGroup['name'];
          }
        }
        if(count($propertyListType)){
          $propertiesEnum = PropertyEnum::find( [ 'property_id' => $propertyListType ] );

          $propertyEnumByPropId = [];

          foreach ( $propertiesEnum as $propertyEnum ) {
            $propertyEnumByPropId[$propertyEnum['property_id']][$propertyEnum['id']] = $propertyEnum;
          }
        }

        foreach ( $properties as $key => $property ) {
          if ( isset( $propertyEnumByPropId[$property['id']] ) ) {
            $properties[$key]['property_list'] = $propertyEnumByPropId[$property['id']];
          }
          if ( isset( $propertyGroupsById[$property['property_group_id']] ) ) {
            $properties[$key]['property_group_name'] = $propertyGroupsById[$property['property_group_id']];
          }
        }
      }

      return $properties;
    }
	
	  public function afterDelete( $model )
	  {
      if(!parent::afterDelete($model)) return;
		  $properties = $this->getDeletedItems();
		  $ids = array_column($properties, 'id');
		  PropertyValue::deleteFilter(['property_id'=>$ids]);
	  }
   
  }
