<?php

namespace Application\Models;

use Application\Helpers\DataHelper;

class ReportTransaction extends \Application\Classes\Model implements \Application\Classes\Forms\Interfaces\FormBuilderInterface
{
  public function __construct()
  {
    parent::__construct( 'report_Transaction', $this->getTypes(), $this );
  }

  public function getTypes()
  {
    return [
      'id'         => 'int',
      'created_at' => 'int',
      'updated_at' => 'int',
      'type'       => 'int',
      'user_from'  => 'int',
      'user_to'    => 'int',
      'sum'        => 'str',
      'text'       => 'str',
      'complete'   => 'int',
      'fail'       => 'int'
    ];
  }

  public function getFields()
  {
    return array(
      'id' => array(
        'type'    => 'string',
        'visible' => true,
        'edited'  => false,
        'label'   => 'ID',
        'sort'    => 1
      ),

      'created_at' => array(
        'type'     => 'date',
        'visible'  => false,
        'edited'   => true,
        'label'    => 'Дата создания',
        'required' => true,
        'sort'     => 5,
        'multiple' => false,
      ),

      'updated_at' => array(
        'type'     => 'date',
        'visible'  => false,
        'edited'   => true,
        'label'    => 'Дата изменения',
        'required' => true,
        'sort'     => 500,
        'multiple' => false,
      ),

      'type' => array(
        'type'     => 'select',
        'visible'  => true,
        'edited'   => true,
        'values'   => DataHelper::TYPE_TRANSACTION,
        'id_key'   => 'id',
        'name_key' => 'name',
        'label'    => 'Тип',
        'required' => true,
        'sort'     => 500,
        'multiple' => false,
      ),

      'user_from' => array(
        'type'     => 'modals',
        'class'    => '\\Application\\Models\\User',
        'visible'  => false,
        'edited'   => true,
        'label'    => 'ID отправителя',
        'required' => false,
        'sort'     => 2,
        'multiple' => true,
        'fields'   => [
          'name',
          'last_name'
        ],
        'info'     => [
          'name',
          'last_name',
          'registration_date',
          'email',
          'phone',
          'balance'
        ],
      ),

      'user_to' => array(
        'type'     => 'modals',
        'class'    => '\\Application\\Models\\User',
        'visible'  => false,
        'edited'   => true,
        'label'    => 'ID получателя',
        'required' => false,
        'sort'     => 3,
        'multiple' => false,
        'fields'   => [
          'name',
          'last_name'
        ],
        'info'     => [
          'name',
          'last_name',
          'registration_date',
          'email',
          'phone',
          'balance'
        ],
      ),

      'sum'      => array(
        'type'     => 'string',
        'visible'  => true,
        'edited'   => true,
        'label'    => 'Сумма',
        'required' => true,
        'sort'     => 4,
        'multiple' => false,
      ),
      'text'     => array(
        'type'     => 'text',
        'visible'  => true,
        'edited'   => true,
        'label'    => 'Описание',
        'required' => false,
        'sort'     => 500,
        'multiple' => false,
      ),
      'complete' => [
        'type'     => 'read',
        'visible'  => true,
        'edited'   => true,
        'required' => false,
        'sort'     => 500,
        'label'    => 'Подтвержден'
      ],
    );
  }

  public function rules(): array
  {
    return [
      [
        [
          "type",
          "user_from",
          "user_to"
        ],
        'number'
      ],
      [
        [
          "sum",
          "user_from",
          "user_to"
        ],
        'required'
      ],
      [
        [ "sum" ],
        'double'
      ],
      [
        [
          "created_at",
          "updated_at"
        ],
        'date'
      ],
      [
        [ 'text' ],
        'string'
      ],
      [
        [
          "created_at",
          "updated_at"
        ],
        'required'
      ],
    ];
  }

  public function attributeLabels(): array
  {
    return [
      'created_at' => 'Дата создания',
      'updated_at' => 'Дата изменения',
      'type'       => 'Тип',
      'user_from'  => 'Отправитель',
      'user_to'    => 'Получатель',
      'sum'        => 'Сумма',
      'text'       => 'Описание',

    ];
  }

  public function formFields( array $data = [] )
  {
    $fields = [
      'created_at' => [
        'type'  => 'date',
        'class' => [ 'form-control' ]
      ],
      'updated_at' => [
        'type'  => 'date',
        'class' => [ 'form-control' ]
      ],
      'text'       => [
        'type'  => 'text',
        'class' => [ 'form-control' ]
      ],
      'type'       => [
        'type'   => 'select',
        'values' => DataHelper::TYPE_TRANSACTION,
        'id'     => 'id',
        'name'   => 'name',
        'class'  => [ 'form-control' ]
      ],
      'user_from'  => [
        'type'   => 'select',
        'values' => \Application\Models\User::find(),
        'id'     => 'id',
        'name'   => 'name',
        'class'  => [ 'form-control' ]
      ],
      'user_to'    => [
        'type'   => 'select',
        'values' => \Application\Modals\User::find(),
        'id'     => 'id',
        'name'   => 'name',
        'class'  => [ 'form-control' ]
      ],
      'sum'        => [
        'type'  => 'text',
        'class' => [ 'form-control' ]
      ],

    ];
    if ( count( $data ) > 0 ) {
      $fieldsFilter = [];
      foreach ( $data as $datum ) {
        if ( isset( $fields[$datum] ) )
          $fieldsFilter[$datum] = $fields[$datum];
      }
      return $fieldsFilter;
    }
    return $fields;
  }

  protected function beforeInsert( array $data ): array
  {
    $unixTime           = $this->getUnixTime();
    $data['created_at'] = $unixTime;
    $data['updated_at'] = $unixTime;
    return $data;
  }

  protected function beforeUpdate( array $data ): array
  {
    $unixTime           = $this->getUnixTime();
    $data['updated_at'] = $unixTime;
    return $data;
  }

  protected function setCreatedAt( $element )
  {
    return strtotime( $element );
  }

  protected function setUpdatedAt( $element )
  {
    return strtotime( $element );
  }

  protected function getCreatedAt( $element )
  {
    return date( 'd.m.Y', $element );
  }

  protected function getUpdatedAt( $element )
  {
    return date( 'd.m.Y', $element );
  }


}
