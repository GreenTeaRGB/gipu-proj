<?php

namespace Application\Models;
class OrderStatus extends \Application\Classes\Model implements \Application\Classes\Forms\Interfaces\FormBuilderInterface
{
  public function __construct()
  {
    parent::__construct( 'order_status', $this->getTypes(), $this );
  }

  public function getTypes()
  {
    return [
      'id'     => 'int',
      'name'   => 'str',
      'active' => 'int',
      'color'  => 'str',
    ];
  }

  public function getFields()
  {
    return array(
      'id' => array(
        'type'    => 'string',
        'visible' => true,
        'edited'  => false,
        'label'   => 'ID'
      ),

      'name' => array(
        'type'     => 'string',
        'visible'  => true,
        'edited'   => true,
        'label'    => 'Название',
        'required' => false,
        'multiple' => false,
      ),

      'active' => array(
        'type'     => 'string',
        'visible'  => false,
        'edited'   => false,
        'label'    => 'active',
        'required' => false,
        'multiple' => false,
      ),
      'color' => [
        'type' => 'color',
        'visible' => false,
        'edited' => true,
        'label' => 'Цвет статуса'
      ]

    );
  }

  public function rules(): array
  {
    return [
      [ [ "name" ], 'string' ],
      [ [ "active" ], 'number' ],

    ];
  }

  public function attributeLabels(): array
  {
    return [
      'name'   => 'Название',
      'active' => 'active',

    ];
  }

  public function formFields( array $data = [] )
  {
    $fields = [
      'name'   => [ 'type' => 'text', 'class' => [ 'form-control' ] ],
      'active' => [ 'type' => 'text', 'class' => [ 'form-control' ] ],

    ];
    if ( count( $data ) > 0 ) {
      $fieldsFilter = [];
      foreach ( $data as $datum ) {
        if ( isset( $fields[$datum] ) )
          $fieldsFilter[$datum] = $fields[$datum];
      }
      return $fieldsFilter;
    }
    return $fields;
  }

  protected function beforeInsert( array $data ): array
  {
    $unixTime = $this->getUnixTime();

    return $data;
  }


}
