<?php

namespace Application\Models;
class QuestionType extends \Application\Classes\Model implements \Application\Classes\Forms\Interfaces\FormBuilderInterface
{
  public function __construct()
  {
    parent::__construct( 'question_type', $this->getTypes(), $this );
  }

  public function getTypes()
  {
    return [
      'id'   => 'int',
      'name' => 'str',

    ];
  }

  public function getFields()
  {
    return array(
      'id' => array(
        'type'    => 'string',
        'visible' => true,
        'edited'  => false,
        'label'   => 'ID'
      ),

      'name' => array(
        'type'     => 'string',
        'visible'  => true,
        'edited'   => true,
        'label'    => 'Название типа',
        'required' => false,
        'multiple' => false,
      ),

    );
  }

  public function rules(): array
  {
    return [
      [ [ "name" ], 'string' ],

    ];
  }

  public function attributeLabels(): array
  {
    return [
      'name' => 'Название типа',

    ];
  }

  public function formFields( array $data = [] )
  {
    $fields = [
      'name' => [ 'type' => 'text', 'class' => [ 'form-control' ] ],

    ];
    if ( count( $data ) > 0 ) {
      $fieldsFilter = [];
      foreach ( $data as $datum ) {
        if ( isset( $fields[$datum] ) )
          $fieldsFilter[$datum] = $fields[$datum];
      }
      return $fieldsFilter;
    }
    return $fields;
  }

  protected function beforeInsert( array $data ): array
  {
    $unixTime = $this->getUnixTime();

    return $data;
  }

}
