<?php

namespace Application\Models;
/**
 * Class Region
 * @package Application\Models
 */
class Region extends \Application\Classes\Model implements \Application\Classes\Forms\Interfaces\FormBuilderInterface
{
  /**
   * Region constructor.
   */
  public function __construct()
  {
    parent::__construct( 'region', $this->getTypes(), $this );
  }

  /**
   * @return array|mixed
   */
  public function getTypes()
  {
    return [
      'id'     => 'int',
      'name'   => 'str',
      'active' => 'int',

    ];
  }

  /**
   * @return array
   */
  public function getFields()
  {
    return array(
      'id' => array(
        'type'    => 'string',
        'visible' => true,
        'edited'  => false,
        'label'   => 'ID'
      ),

      'name' => array(
        'type'     => 'string',
        'visible'  => true,
        'edited'   => true,
        'label'    => 'Название',
        'required' => true,
        'multiple' => false,
      ),

      'active' => array(
        'type'     => 'string',
        'visible'  => false,
        'edited'   => false,
        'label'    => 'Активность',
        'required' => false,
        'multiple' => false,
      ),

    );
  }

  /**
   * @return array
   */
  public function rules(): array
  {
    return [
      [ [ "name" ], 'required' ],
      [ [ "name" ], 'string' ],
      [ [ "active" ], 'number' ],

    ];
  }

  /**
   * @return array
   */
  public function attributeLabels(): array
  {
    return [
      'name'   => 'Название',
      'active' => 'Активность',

    ];
  }

  /**
   * @param array $data
   * @return array
   */
  public function formFields( array $data = [] )
  {
    $fields = [
      'name'   => [ 'type' => 'text', 'class' => [ 'form-control' ] ],
      'active' => [ 'type' => 'text', 'class' => [ 'form-control' ] ],

    ];
    if ( count( $data ) > 0 ) {
      $fieldsFilter = [];
      foreach ( $data as $datum ) {
        if ( isset( $fields[$datum] ) )
          $fieldsFilter[$datum] = $fields[$datum];
      }
      return $fieldsFilter;
    }
    return $fields;
  }

}
