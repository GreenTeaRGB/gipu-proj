<?php

  namespace Application\Models;

  use Application\Helpers\UserHelper;

  class Wishlist extends \Application\Classes\Model
  {
    private $afterFetchAll;

    public function __construct( $afterFetchAll = false )
    {
      parent::__construct( 'wishlist', $this->getTypes(), $this );

      $this->afterFetchAll = $afterFetchAll;
    }

    public function getTypes()
    {
      return array(
          'id'         => 'int',
          'user_id'    => 'int',
          'product_id' => 'str',
      );
    }

    protected function afterFetchAll( $data )
    {
      if( $this->afterFetchAll ) {
        $model = new Product();

        $sql = "
					SELECT prod.*,
					cat.name as category
					FROM `wishlist` as w
						LEFT JOIN `product` as prod ON prod.`id` = w.`product_id`
						LEFT JOIN `category` as cat ON prod.`category_id` = cat.`id`
					WHERE";

        foreach( $data as $wishlistItem ) {
          $sql .= " w.`id` = '{$wishlistItem["id"]}' OR";
        }

        $sql      = rtrim( $sql, 'OR' );
        $products = $model->sql( $sql )->fetchAll();

        $output = [];

        foreach( $data as $newWishlistItem ) {
          foreach( $products as $product ) {

            if( $newWishlistItem['product_id'] == $product['id'] ) {
              $newWishlistItem['product'] = $product;
              $output[]                   = $newWishlistItem;
            }
          }
        }
        return $output;
      }

      return $data;
    }

    public static function find( $filter = [] )
    {
      $model = new Wishlist();
      if( count( $filter ) > 0 ) {
        $model->where( $filter );
      }
      return $model->fetchAll();
    }

    public static function getCountForUser()
    {
      $user = UserHelper::getUser();
      if( !$user )
        return 0;
      $model = new Wishlist();
      return $model->where( [ 'user_id' => $user['id'] ] )->count();
    }

  }