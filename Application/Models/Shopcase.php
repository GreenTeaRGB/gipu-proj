<?php

namespace Application\Models;
class Shopcase extends \Application\Classes\Model implements \Application\Classes\Forms\Interfaces\FormBuilderInterface
{
  public function __construct()
  {
    parent::__construct( 'shopcase', $this->getTypes(), $this );
  }

  public function getTypes()
  {
    return [
      'id'         => 'int',
      'product_id' => 'str',

    ];
  }

  public function getFields()
  {
    return array(
      'id' => array(
        'type'    => 'string',
        'visible' => true,
        'edited'  => false,
        'label'   => 'ID'
      ),

      'product_id' => array(
        'type'     => 'modals',
        'visible'  => false,
        'edited'   => true,
        'required' => false,
        'label'    => 'Товар',
        'class'    => '\\Application\\Models\\Product',
        'fields'   => [ 'id', 'name' ],
        'sort'     => 500,
        'multiple' => true,
      ),

    );
  }

  public function rules(): array
  {
    return [
      [ [ "product_id" ], 'required' ],
    ];
  }

  protected function setProductId($element){
    return self::json_encode($element);
  }

  protected function getProductId($element){
    return self::json_decode($element);
  }

  public function attributeLabels(): array
  {
    return [
      'product_id' => 'Товар',

    ];
  }

  public function formFields( array $data = [] )
  {
    $fields = [
      'product_id' => [ 'type'   => 'select',
                        'values' => \Application\Models\Product::find(),
                        'id'     => 'id',
                        'name'   => 'name',
                        'class'  => [ 'form-control' ]
      ],

    ];
    if ( count( $data ) > 0 ) {
      $fieldsFilter = [];
      foreach ( $data as $datum ) {
        if ( isset( $fields[$datum] ) )
          $fieldsFilter[$datum] = $fields[$datum];
      }
      return $fieldsFilter;
    }
    return $fields;
  }


}
