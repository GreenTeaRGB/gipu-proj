<?php

  namespace Application\Models;
  class Warehouse extends \Application\Classes\Model
  {
    public function __construct()
    {
      parent::__construct( 'warehouse', $this->getTypes(), $this );
    }

    public function getTypes()
    {
      return array(
          'id'        => 'int',
          'name'      => 'str',
          'status_id' => 'int',
          'owner_id'  => 'int',
          'address'   => 'str',
      );
    }

    public function getFields()
    {
      return [
          'name'      => [
              'type'     => 'string',
              'visible'  => true,
              'edited'   => true,
              'required' => false,
              'label'    => 'Название'
          ],
          'status_id' => [
              'type'     => 'select',
              'visible'  => true,
              'edited'   => true,
              'values'   => WarehouseStatus::find(),
              'id_key'   => 'id',
              'name_key' => 'type',
              'required' => false,
              'label'    => 'Статус'
          ],
          //				'owner_id'  => [
          //					'type'     => 'string',
          //					'visible'  => true,
          //					'edited'   => true,
          //					'required' => false,
          //					'label'    => 'owner_id'
          //				],
          'owner_id'  => [
              'type'     => 'modals',
              'visible'  => true,
              'edited'   => true,
              'required' => false,
              'label'    => 'Владелец',
              'class'    => '\\Application\\Models\\User',
              'fields'   => [ 'name', 'last_name' ]
          ],
          'address'   => [
              'type'     => 'string',
              'visible'  => true,
              'edited'   => true,
              'required' => false,
              'label'    => 'Адрес'
          ],
      ];
    }
	
	  public function rules(){
		  return [
			  [['name', 'address'], 'safe', 'on' => 'cabinetUpdate'],
			  [['name', 'address'], 'string'],
			  [['status_id', 'owner_id'], 'number'],
			  [['name', 'status_id', 'owner_id', 'address'], 'required', 'on' => 'default'],
		  ];
	  }

    protected function afterFetchAll( $data )
    {
      $status = WarehouseStatus::find();
      $result = [];
      foreach( $data as $datum ) {
        $datum['status_name']   = isset( $status[$datum['status_id']]['type'] )
            ? $status[$datum['status_id']]['type']
            : 'Не выбрано';
        $result[$datum['id']] = $datum;
      }

      return $result;
    }

    public static function getWarehouses()
    {
      $model = new Warehouse();
      return $model->fetchAll();
    }

    public static function getWarehouseById( $id )
    {
      $model = new Warehouse();
      return $model->where( [ 'id' => (int) $id ] )->fetchOne();
    }

    public function afterDelete( $model )
    {
      if( !parent::afterDelete( $model ) )
        return;
      $warehouse = $this->getDeletedItems();
      $ids       = array_column( $warehouse, 'id' );
      //		WarehouseProduct::deleteFilter(['warehouse_id'=>$ids]);
      Product::deleteFilter( [ 'warehouse_id' => $ids ] );
    }

  }