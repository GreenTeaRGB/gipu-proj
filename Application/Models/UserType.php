<?php
	
	namespace Application\Models;
	
	class UserType extends \Application\Classes\Model
	{
		
		public function __construct()
		{
			parent::__construct('user_type', $this->getTypes(), $this);
		}
		
		public function getTypes()
		{
			return array(
				'id'        => 'int',
				'type_code' => 'str',
				'type'      => 'str',
			);
		}

		public static function find($filter = []){
			$model = new UserType();
			if(count($filter) > 0) $model->where($filter);
			return $model->fetchAll();
		}
		
		public function afterDelete( $model )
		{
      if(!parent::afterDelete($model)) return;
			$userType = $this->getDeletedItems();
			$ids = array_column($userType, 'id');
			User::deleteFilter(['type'=>$ids]);
		}
	}