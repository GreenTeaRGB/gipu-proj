<?php
	
	namespace Application\Models;
	class Restriction extends \Application\Classes\Model
	{
		public function __construct()
		{
			parent::__construct('restriction', $this->getTypes(), $this);
		}
		
		public function getTypes()
		{
			return array(
				'id'   => 'int',
				'type' => 'str'
			);
		}
		
		public static function getRestrictions()
		{
			$model = new Restriction();
			return $model->fetchAll();
		}
	}