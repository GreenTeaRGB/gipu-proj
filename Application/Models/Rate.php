<?php

	namespace Application\Models;
	class Rate extends \Application\Classes\Model
	{
		public function __construct()
		{
			parent::__construct( 'rate', $this->getTypes(), $this );
		}

		public function getTypes()
		{
			return [
				'id'               => 'int',
				'active'           => 'int',
				'name'             => 'str',
				'description'      => 'str',
				'price'            => 'str',
				'count_categories' => 'str',

			];
		}

		public function getFields()
		{
			return array(
				'id' => array(
					'type'    => 'string',
					'visible' => true,
					'edited'  => false,
					'label'   => 'ID'
				),

				'active' => array(
					'type'     => 'checkbox',
					'visible'  => false,
					'edited'   => true,
					'label'    => 'Активность',
					'required' => false,
					'multiple' => false,
				),

				'name' => array(
					'type'     => 'string',
					'visible'  => true,
					'edited'   => true,
					'label'    => 'Название',
					'required' => false,
					'multiple' => false,
				),

				'description' => array(
					'type'     => 'text',
					'visible'  => true,
					'edited'   => true,
					'label'    => 'Описание',
					'required' => false,
					'multiple' => false,
				),

				'price'            => array(
					'type'     => 'string',
					'visible'  => true,
					'edited'   => true,
					'label'    => 'Цена тарифа',
					'required' => false,
					'multiple' => false,
				),
				'count_categories' => array(
					'type'     => 'string',
					'visible'  => true,
					'edited'   => true,
					'label'    => 'Доступные баллы для выбора категорий',
					'required' => false,
					'multiple' => false,
				),

			);
		}
		
		public function rules()
		{
			return [
				[ [ "name" ], 'string' ],
				[ [ "active", "count_categories" ], 'number' ],
				[ [ "price" ], 'double' ],
				[ [ "name", "description", "price" ], 'required' ],
			
			];
		}

		public function attributeLabels()
		{
			return [
				'active'      => 'Активность',
				'name'        => 'Название',
				'description' => 'Описание',
				'price'       => 'Цена тарифа',
				'count_categories' => 'Количество категорий'

			];
		}

		public function formFields( array $data = [] )
		{
			$fields = [
				'active'      => [ 'type' => 'checkbox', 'class' => [ 'form-control' ] ],
				'name'        => [ 'type' => 'text', 'class' => [ 'form-control' ] ],
				'description' => [ 'type' => 'textarea', 'class' => [ 'form-control' ] ],
				'price'       => [ 'type' => 'text', 'class' => [ 'form-control' ] ],
				'count_categories'       => [ 'type' => 'text', 'class' => [ 'form-control' ] ],

			];
			if( count( $data ) > 0 ) {
				$fieldsFilter = [];
				foreach( $data as $datum ) {
					if( isset( $fields[ $datum ] ) )
						$fieldsFilter[ $datum ] = $fields[ $datum ];
				}
				return $fieldsFilter;
			}
			return $fields;
		}
		
		public function afterDelete( $model )
		{
      if(!parent::afterDelete($model)) return;
			$rate = $this->getDeletedItems();
			$ids = array_column($rate, 'id');
			$model = new Shop();
			$model->setFields(['rate'=>"0"])->where(['rate'=>$ids])->update();
		}
	}
