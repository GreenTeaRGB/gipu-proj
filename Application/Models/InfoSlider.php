<?php

  namespace Application\Models;
  class InfoSlider extends \Application\Classes\Model
  {
    public function __construct()
    {
      parent::__construct( 'info_slider', $this->getTypes(), $this );
    }

    public function getTypes()
    {
      return [
          'id'       => 'int',
          'active'   => 'int',
          'sort'     => 'int',
          'text'     => 'str',
          'image'    => 'str',
          'image_bg' => 'str',
          'color'    => 'str',
          'link'     => 'str',

      ];
    }

    public function getFields()
    {
      return array(
          'id' => array(
              'type'    => 'string',
              'visible' => true,
              'edited'  => false,
              'label'   => 'ID'
          ),

          'active' => array(
              'type'     => 'string',
              'visible'  => false,
              'edited'   => false,
              'label'    => 'active',
              'required' => false,
              'multiple' => false,
          ),

          'sort' => array(
              'type'     => 'string',
              'visible'  => false,
              'edited'   => false,
              'label'    => 'sort',
              'required' => false,
              'multiple' => false,
          ),

          'text' => array(
              'type'     => 'string',
              'visible'  => true,
              'edited'   => true,
              'label'    => 'Текст',
              'required' => false,
              'multiple' => false,
          ),

          'image' => array(
              'type'      => 'file',
              'visible'   => false,
              'edited'    => true,
              'label'     => 'Маленькое изображение',
              'required'  => false,
              'file-type' => 'image/*',
              'multiple'  => false,
          ),

          'image_bg' => array(
              'type'      => 'file',
              'visible'   => false,
              'edited'    => true,
              'label'     => 'Фоновое изображение',
              'required'  => false,
              'file-type' => 'image/*',
              'multiple'  => false,
          ),

          'color' => array(
              'type'     => 'color',
              'visible'  => false,
              'edited'   => true,
              'label'    => 'Цвет фона',
              'required' => false,
              'multiple' => false,
          ),
          'link'  => array(
              'type'     => 'string',
              'visible'  => false,
              'edited'   => true,
              'label'    => 'Ссылка',
              'required' => false,
              'multiple' => false,
          ),

      );
    }
	
	  public function rules()
	  {
		  return [
			  [ [ "text", "color", 'link' ], 'string' ],
			  [ [ "active", "sort" ], 'number' ],
			  [ [ "image", "image_bg" ], 'image', 'extensions' => [ 'png', 'jpg', 'jpeg' ] ],
			  [ [ "text" ], 'required' ],
		
		  ];
	  }

    public function attributeLabels()
    {
      return [
          'active'    => 'active',
          'sort'      => 'sort',
          'text'      => 'Текст',
          'text_bold' => 'Жирный текст',
          'image'     => 'Маленькое изображение',
          'image_bg'  => 'Фоновое изображение',
          'color'     => 'Цвет фона',

      ];
    }

    public function formFields( array $data = [] )
    {
      $fields = [
          'active'    => [ 'type' => 'text', 'class' => [ 'form-control' ] ],
          'sort'      => [ 'type' => 'text', 'class' => [ 'form-control' ] ],
          'text'      => [ 'type' => 'text', 'class' => [ 'form-control' ] ],
          'image'     => [ 'type' => 'file', 'class' => [ 'form-control' ] ],
          'image_bg'  => [ 'type' => 'file', 'class' => [ 'form-control' ] ],
          'color'     => [ 'type' => 'color', 'class' => [ 'form-control' ] ],

      ];
      if( count( $data ) > 0 ) {
        $fieldsFilter = [];
        foreach( $data as $datum ) {
          if( isset( $fields[$datum] ) )
            $fieldsFilter[$datum] = $fields[$datum];
        }
        return $fieldsFilter;
      }
      return $fields;
    }

    protected function afterDelete( $model )
    {
      if( !parent::afterDelete( $model ) )
        return;
      $slider = $this->getDeletedItems();
      self::removeFiles( array_column( $slider, 'image' ) );
      self::removeFiles( array_column( $slider, 'image_bg' ) );
    }


  }
