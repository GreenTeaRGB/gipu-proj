<?php

namespace Application\Models;

use Application\Classes\Session;
use Application\Helpers\DataHelper;
use Application\Helpers\UploadedFile;
use Application\Helpers\UserHelper;


/**
 * Class Product
 * @package Application\Models
 */
class Product extends \Application\Classes\Model
{
  /**
   * @var bool
   */
  private $afterFetchAll = false;

  /**
   * @var array
   */
  private $insertWarehouse = [];

  /**
   * @var int
   */
  private $insertPrice = 0;

  /**
   * @var bool|int
   */
  private static $default_count = 30;

  /**
   * @var int
   */
  private static $default_count_ajax = 20;

  /**
   * @var string
   */
  private static $sort_order = 'id';

  /**
   * @var string
   */
  private static $sort_by = 'desc';

  /**
   *
   */
  CONST MAX_FILES_CERTIFICATE = 10;

  /**
   *
   */
  CONST MAX_FILES_IMAGES = 10;

  /**
   * Product constructor.
   * @param bool $afterFetchAll
   */
  public function __construct( $afterFetchAll = false )
  {
    if ( $count = Session::getByKey( 'count' ) ) {
      self::$default_count = $count;
    }
    if ( $sort = Session::getByKey( 'sort' ) ) {

      if ( isset( $sort['order'] ) && isset( $sort['by'] ) ) {
        switch ( $sort['order'] ) {
          case 'price' :
            self::$sort_order = 'price';
            break;
          case 'rate' :
            self::$sort_order = 'rate';
            break;
          case 'popular' :
            self::$sort_order = 'popular';
            break;
          default:
            self::$sort_order = 'id';
        }
        self::$sort_order = $sort['order'];
        self::$sort_by    = $sort['by']
          ? 'asc'
          : 'desc';
      }
    }
    if ( $afterFetchAll ) {
      $this->afterFetchAll = $afterFetchAll;
    }
    parent::__construct( 'product', $this->getTypes(), $this );
  }

  /**
   * @return array|mixed
   */
  public function getTypes()
  {
    return array(
      'id'               => 'int',
      'name'             => 'str',
      'category_id'      => 'int',
      'created_at'       => 'int',
      'updated_at'       => 'int',
      'status_id'        => 'int',
      'sku'              => 'str',
      'price'            => 'int',
      'images'           => 'str',
      'description'      => 'str',
      'original'         => 'int',
      'certificate'      => 'str',
      'owner_id'         => 'str',
      'warehouse_id'     => 'int',
      'availability'     => 'int',
      'sale_id'          => 'int',
      'alias'            => 'str',
      'meta_title'       => 'str',
      'meta_description' => 'str',
      'meta_keywords'    => 'str',
      'commission'       => 'int',
      'type'             => 'int',
      'purchase_price'   => 'str',
      'agent_price'      => 'str',
      'active'           => 'int',
      'region_id'        => 'str',
      'old_price'        => 'int',
    );
  }

  /**
   * @return array
   */
  public function getFields()
  {
    return [
      'id'               => [
        'type'     => 'read',
        'visible'  => true,
        'edited'   => true,
        'required' => false,
        'label'    => 'ID',
        'sort'     => 2,
        'dsort'    => 10,
      ],
      'created_at'       => [
        'type'     => 'read',
        'visible'  => false,
        'edited'   => true,
        'required' => false,
        'label'    => 'Дата создания',
        'sort'     => 500,
        'dsort'    => 20,
      ],
      'updated_at'       => [
        'type'     => 'read',
        'visible'  => false,
        'edited'   => true,
        'required' => false,
        'label'    => 'Дата обновления',
        'sort'     => 500,
        'dsort'    => 30,
      ],
      'name'             => [
        'type'     => 'string',
        'visible'  => true,
        'edited'   => true,
        'required' => false,
        'label'    => 'Название',
        'sort'     => 2,
        'dsort'    => 40,
      ],
      'category_id'      => [
        'type'        => 'selectTree',
        'visible'     => false,
        'edited'      => true,
        'values'      => Category::getFullList(),
        'id_key'      => 'id',
        'name_key'    => 'name',
        'required'    => false,
        'label'       => 'Категория',
        'disableRoot' => true,
        'sort'        => 500,
        'dsort'       => 50,
      ],
      'status_id'        => [
        'type'     => 'selectTree',
        'visible'  => false,
        'values'   => ProductStatus::getProductStatuses(),
        'id_key'   => 'id',
        'name_key' => 'type',
        'edited'   => false,
        'required' => false,
        'label'    => 'Статус',
        'sort'     => 500,
        'dsort'    => 500,
      ],
      'region_id'        => [
        'type'     => 'aselect',
        'visible'  => false,
        'values'   => Region::find( [ 'active' => DataHelper::DEFAULT_ACTIVE ] ),
        'id_key'   => 'id',
        'name_key' => 'name',
        'edited'   => true,
        'required' => false,
        'multiple' => true,
        'label'    => 'Регионы',
        'sort'     => 500,
        'dsort'    => 90,
      ],
      'sku'              => [
        'type'     => 'string',
        'visible'  => true,
        'edited'   => true,
        'required' => false,
        'label'    => 'Артикул',
        'sort'     => 3,
        'dsort'    => 60,
      ],
      'price'            => [
        'type'     => 'price',
        'visible'  => true,
        'edited'   => true,
        'required' => false,
        'label'    => 'Розничная цена',
        'sort'     => 500,
        'dsort'    => 110,
      ],
      'agent_price'      => [
        'type'     => 'string',
        'visible'  => true,
        'edited'   => false,
        'required' => false,
        'label'    => 'Цена агента',
        'sort'     => 500,
        'dsort'    => 500,
      ],
      'purchase_price'   => [
        'type'     => 'string',
        'visible'  => true,
        'edited'   => false,
        'required' => false,
        'label'    => 'Цена поставщика',
        'sort'     => 500,
        'dsort'    => 500,
      ],
      'images'           => [
        'type'      => 'afile',
        'visible'   => true,
        'edited'    => true,
        'required'  => false,
        'label'     => 'Картинки',
        'multiple'  => true,
        'file-type' => 'image/*',
        'link'      => true,
        'maxFiles'  => self::MAX_FILES_IMAGES,
        'sort'      => 1,
        'dsort'     => 70,
      ],
      'description'      => [
        'type'     => 'text',
        'visible'  => false,
        'edited'   => false,
        'required' => false,
        'label'    => 'Описание',
        'sort'     => 500,
        'dsort'    => 500
      ],
      'original'         => [
        'type'     => 'checkbox',
        'visible'  => false,
        'edited'   => false,
        'required' => false,
        'label'    => 'Оригинальный продукт',
        'sort'     => 500,
        'dsort'    => 500,
      ],
      'certificate'      => [
        'type'      => 'file',
        'visible'   => false,
        'edited'    => false,
        'required'  => false,
        'label'     => 'Сертификат',
        'multiple'  => true,
        'file-type' => '*',
        'maxFiles'  => self::MAX_FILES_CERTIFICATE,
        'sort'      => 500,
        'dsort'     => 500,
      ],
      'owner_id'         => [
        'type'     => 'modals',
        'visible'  => false,
        'edited'   => true,
        'required' => false,
        'label'    => 'Поставщик',
        'class'    => '\\Application\\Models\\User',
        'fields'   => [ 'id', 'name', 'last_name' ],
        'where'    => [ 'type' => DataHelper::DEFAULT_SUPPLIER_TYPE_ID ],
        'sort'     => 500,
        'dsort'    => 80,
        'multiple' => true,
      ],
      'warehouse_id'     => [
        'type'     => 'aselect',
        'visible'  => false,
        'values'   => \Application\Models\Warehouse::find(),
        'id_key'   => 'id',
        'name_key' => 'name',
        'edited'   => false,
        'required' => false,
        'label'    => 'Склады',
        'multiple' => true,
        'sort'     => 500,
        'dsort'    => 500,
      ],
      'availability'     => [
        'type'     => 'string',
        'visible'  => false,
        'edited'   => true,
        'required' => false,
        'label'    => 'Кол-во в наличии',
        'sort'     => 500,
        'dsort'    => 120,
      ],
      'sale_id'          => [
        'type'     => 'modals',
        'visible'  => false,
        'edited'   => false,
        'required' => false,
        'label'    => 'Скидка',
        'class'    => '\\Application\\Models\\Discount',
        'fields'   => [ 'title', 'discount' ],
        'sort'     => 500,
        'dsort'    => 500,
      ],
      'alias'            => [
        'type'     => 'string',
        'visible'  => false,
        'edited'   => true,
        'required' => false,
        'label'    => 'Ссылка',
        'sort'     => 500,
        'dsort'    => 100,
      ],
      'meta_title'       => [
        'type'     => 'string',
        'visible'  => false,
        'edited'   => true,
        'required' => false,
        'label'    => 'Мета заголовок',
        'tab'      => 'SEO',
        'sort'     => 500,
        'dsort'    => 500,
      ],
      'meta_description' => [
        'type'     => 'text',
        'visible'  => false,
        'edited'   => true,
        'required' => false,
        'label'    => 'Мета описание',
        'tab'      => 'SEO',
        'sort'     => 500,
        'dsort'    => 500,
      ],
      'meta_keywords'    => [
        'type'     => 'string',
        'visible'  => false,
        'edited'   => true,
        'required' => false,
        'label'    => 'Мета ключевые слова',
        'tab'      => 'SEO',
        'sort'     => 500,
        'dsort'    => 500,
      ],
      'commission'       => [
        'type'     => 'string',
        'visible'  => false,
        'edited'   => false,
        'required' => false,
        'label'    => 'Комиссия',
        'sort'     => 500,
        'dsort'    => 500,
      ],
      'type'             => [
        'type'     => 'aselect',
        'visible'  => false,
        'values'   => [ [ 'id' => 1, 'name' => 'Товар' ], [ 'id' => 2, 'name' => 'Услуга' ] ],
        'id_key'   => 'id',
        'name_key' => 'name',
        'edited'   => true,
        'required' => false,
        'label'    => 'Тип',
        'multiple' => false,
        //        'empty'    => true,
        'sort'     => 500,
        'dsort'    => 500,
      ],
      'active'           => [
        'type'    => 'string',
        'visible' => true,
        'edited'  => false,
        'sort'    => 1000,
        'dsort'   => 500,
        'label'   => 'Активность'
      ],
      'old_price'        => [
        'type'    => 'string',
        'visible' => false,
        'edited'  => false,
        'sort'    => 400,
        'dsort'   => 500,
        'label'   => 'Старая цена'
      ],

    ];
  }

  /**
   * @return array
   */
  public function attributeLabels()
  {
    return [
      'id'               => 'ID',
      'name'             => 'Название',
      'category_id'      => 'Категория',
      'created_at'       => 'Дата создания',
      'updated_at'       => 'Дата обновления',
      'status_id'        => 'Статус',
      'sku'              => 'Артикул',
      'price'            => 'Цена',
      'images'           => 'Картинки',
      'description'      => 'Описание',
      'original'         => 'Оригинальный продукт',
      'certificate'      => 'Сертификат',
      'owner_id'         => 'Поставщик',
      'warehouse_id'     => 'Склады',
      'availability'     => 'Доступность',
      'sale_id'          => 'Скидка',
      'alias'            => 'Ссылка',
      'meta_title'       => 'Мета заголовок',
      'meta_description' => 'Мета описание',
      'meta_keywords'    => 'Мета ключевые слова',
      'commission'       => 'Комиссия',
      'type'             => 'Тип',
      'agent_price'      => 'Цена агента',
      'purchase_price'   => 'Закупочная цена'
    ];
  }

  /**
   * @return array
   */
  public function rules()
  {
    if ( isset( $_POST['original'] ) ) {
      if ( isset( $_POST['certificate'] ) ) {
        $skip = true;
      } else {
        $skip = false;
      }
    } else {
      $skip = true;
    }
    return [
      [ 'alias', 'unique' ],
      [ 'alias', 'link' ],
      [
        [ 'price', 'purchase_price', 'agent_price' ],
        'double',
        'min'            => 0.01,
        'bigOnAttribute' => 'purchase_price',
        'tooBig'         => 'Значение цены должно быть больше чем закупочная цена'
      ],
      [ 'category_id', 'exist', 'targetClass' => '\\Application\\Models\\Category', 'targetAttribute' => 'id' ],
      [
        'sale_id',
        'exist',
        'targetClass'     => '\\Application\\Models\\Discount',
        'targetAttribute' => 'id',
        'when'            => function ( $model, $attribute ) {
          return $model->$attribute == 0
            ? false
            : true;
        }
      ],
      [ [ 'category_id', 'status_id', 'sale_id', 'type', 'availability', 'commission' ], 'number' ],
      [ 'images', 'image', 'extensions' => [ 'png', 'jpg', 'jpeg' ], 'maxFiles' => self::MAX_FILES_IMAGES, ],
      [
        'certificate',
        'file',
        'extensions'  => [ 'png', 'jpg', 'jpeg', 'doc' ],
        'maxFiles'    => self::MAX_FILES_CERTIFICATE,
        'skipOnEmpty' => $skip
      ],
      [ [ 'name', 'meta_title', 'meta_description', 'meta_keywords', 'description', 'sku' ], 'string' ],
      [
        [
          'category_id',
          //          'status_id',
          'alias',
          'name',
          'type',
          //          'warehouse_id',
          'purchase_price',
          'agent_price',
          'price'
        ],
        'required'
      ],
    ];

  }


  /**
   * @param $element
   * @return false|string
   */
  protected function getCreatedAt( $element )
  {
    return self::dateFormat( $element );
  }

  /**
   * @param $element
   * @return false|string
   */
  protected function getUpdatedAt( $element )
  {
    return self::dateFormat( $element );
  }

  /**
   * @param $element
   * @return false|int
   */
  protected function setCreatedAt( $element )
  {
    return self::strtotime( $element );
  }

  /**
   * @param $element
   * @return array|mixed
   */
  protected function getRegionId( $element )
  {
    return self::unserialize( $element );
  }

  /**
   * @param $element
   * @return string
   */
  protected function setRegionId( $element )
  {
    return self::serialize( $element );
  }

  /**
   * @param $element
   * @return false|int
   */
  protected function setUpdatedAt( $element )
  {
    return self::strtotime( $element );
  }

  /**
   * @param $element
   * @return string
   */
  protected function setImages( $element )
  {
    return self::serialize( $element );
  }

  /**
   * @param $element
   * @return mixed
   */
  protected function getImages( $element )
  {
    return self::unserialize( $element );
    //      if( !isset( $images[0] ) )
    //        $images[] = '/images/no_photo.png';
  }

  /**
   * @param $element
   * @return mixed
   */
  protected function getOwnerId( $element )
  {
    return self::unserialize( $element );
  }

  /**
   * @param $element
   * @return string
   */
  protected function setOwnerId( $element )
  {
    return self::serialize( $element );
  }

  /**
   * @param $element
   * @return string
   */
  protected function setCertificate( $element )
  {
    return self::serialize( $element );
  }

  /**
   * @param $element
   * @return mixed
   */
  protected function getCertificate( $element )
  {
    return self::unserialize( $element );
  }

  /**
   * @param $id
   * @return mixed
   */
  public static function getProductById( $id, $ownerId, $warehouseId )
  {
    $model = new Product();
    $sql   = "SELECT p.*, wp.price as `price` FROM `product` as `p`
              JOIN `warehouse_product` as `wp` ON wp.product_id = p.id AND wp.warehouse_id = '" . (int)$warehouseId . "'
              WHERE p.owner_id = '" . (int)$ownerId . "' AND p.id = '" . $id . "';
              ";

    return $model->sql( $sql )->fetchOne();
    //      return $model->where( [ 'id' => (int) $id ] )->fetchOne();
  }

  /**
   * @param $ownerId
   * @param $warehouseId
   * @return array
   */
  public static function getProductsByOwnerIdAndWarehouseId( $ownerId, $warehouseId, $page = 1 )
  {
    $model = new Product();
    $start = ( $page - 1 ) * self::$default_count_ajax;
    $sql   = "SELECT p.*, wp.price as `price` FROM `product` as `p`
              JOIN `warehouse_product` as `wp` ON wp.product_id = p.id AND wp.warehouse_id = '" . (int)$warehouseId . "'
              WHERE p.owner_id = '" . (int)$ownerId . "' LIMIT $start, " . self::$default_count_ajax;
    return $model->sql( $sql )->fetchAll();
    //      return $model->where(
    //          [
    //              'owner_id'     => (int) $ownerId,
    //              'warehouse_id' => $warehouseId
    //          ]
    //      )->fetchAll();
  }

  /**
   * @param array $data
   * @return array
   */
  protected function beforeInsert( array $data ): array
  {
    $unixTime           = $this->getUnixTime();
    $data['created_at'] = $unixTime;
    $data['updated_at'] = $unixTime;
    if ( isset( $data['warehouse_id'] ) && count( $data['warehouse_id'] ) > 0 ) {
      $this->insertWarehouse = $data['warehouse_id'];
      $this->insertPrice     = $data['price'];
      unset( $data['warehouse_id'] );
    }
    return $data;
  }

  /**
   * @param $id
   */
  protected function afterInsert( $id )
  {
    if ( count( $this->insertWarehouse ) > 0 ) {
      foreach ( $this->insertWarehouse as $warehouseId ) {
        $insert[] = [
          'product_id'   => $id,
          'warehouse_id' => $warehouseId,
          'price'        => $this->insertPrice
        ];
      }
      $warehouseProduct = new WarehouseProduct();
      $warehouseProduct->setAllfields( $insert )->insertAll();
    }
  }

  /**
   * @param array $data
   * @return array
   */
  protected function beforeUpdate( array $data )
  {
    if ( UserHelper::isAdmin() ) {
      $where = $this->getWhereParams();
      if ( isset( $where['id'] ) ) {
        $warehouseProduct = new WarehouseProduct();
        $relations        = $warehouseProduct::find( [ 'product_id' => $where['id'] ] );
        foreach ( $relations as $relation )
          $saveData[$relation['warehouse_id']] = $relation;

        $warehouseProduct::deleteFilter( [ 'product_id' => $where['id'] ] );
        $insert = [];
        if ( isset( $data['warehouse_id'] ) && count( $data['warehouse_id'] ) > 0 )
          foreach ( $data['warehouse_id'] as $warehouseId ) {
            $insert[] = [
              'product_id'   => $where['id'],
              'warehouse_id' => $warehouseId,
              'price'        => isset( $saveData[$warehouseId]['price'] )
                ? $saveData[$warehouseId]['price']
                : $data['price']
            ];
          }
        $warehouseProduct->setAllfields( $insert )->insertAll();
        $data['warehouse_id'] = 0;
      }
    }
    $data['updated_at'] = $this->getUnixTime();
    return $data;
  }

  /**
   * @param array $filter
   * @param bool  $fetchOne
   * @param bool  $afterFetchAll
   * @return array|mixed
   */
  public static function find( $filter = [], $fetchOne = false, $afterFetchAll = false )
  {
    $model = new Product( $afterFetchAll );
    if ( count( $filter ) > 0 )
      $model->where( $filter );

    if ( $fetchOne ) {
      return $model->fetchOne();
    }
    return $model->fetchAll();
  }

  /**
   * @param $data
   * @return mixed
   */
  protected function afterFetchAll( $data )
  {
    $categories = Category::getFullList();

    if ( $this->afterFetchAll && $data ) {
      $user = UserHelper::getUser();
//      if ( $user ) {
      $compareIds = [];
      $productIds = [];
      $compare    = Session::getByKey( 'compare' );
      if ( is_array( $compare ) ) {
        foreach ( $compare as $category_name => $products ) {
          $compareIds = array_merge( $compareIds, array_column( $products, 'id' ) );
        }
      }
      $wishlistIds = Session::getByKey( 'wishlist' );
      if ( !is_array( $wishlistIds ) ) $wishlistIds = [];
      if ( $user ) {
        $wishlistModel = new Wishlist( true );
        $wishlist      = $wishlistModel->where( [ 'user_id' => $user['id'] ] )->fetchAll();
        $productIds    = array_column( $wishlist, 'product_id' );
        $wishlistIds   = array_merge( $wishlistIds, $productIds );
      }
      foreach ( $data as $key => $product ) {
        if ( in_array( $product['id'], $wishlistIds ) ) {
          $data[$key]['wishlist'] = true;
        }
        $data[$key]['category_name'] = isset( $categories[$product['category_id']] )
          ? $categories[$product['category_id']]['name']
          : 'Категория не выбрана';
        if ( in_array( $product['id'], $compareIds ) ) {
          $data[$key]['compare'] = true;
        }
      }
//      } else {
//        $wishlist = Session::getByKey('wishlist');
//        if($wishlist == false) $wishlist = [];
//        foreach ( $data as $key => $product ) {
//          if(in_array($product['id'], $wishlist))
//            $data[$key]['wishlist'] = true;
//          $data[$key]['category_name'] = isset( $categories[$product['category_id']] )
//            ? $categories[$product['category_id']]['name']
//            : 'Категория не выбрана';
//          if(isset($compare[$data[$key]['category_name']])){
//            $ids = array_column($compare[$data[$key]['category_name']], 'id');
//            if(in_array($product['id'], $ids)){
//              $data[$key]['compare'] = true;
//            }
//          }
//        }
//      }
    }
    return $data;
  }

  /**
   * @param $data
   * @return mixed
   */
  protected function afterFetchOne( $data )
  {
    if ( !$data || !isset( $data['id'] ) )
      return $data;
    $warehouses              = WarehouseProduct::find( [ 'product_id' => $data['id'] ] );
    $data['warehouses']      = $warehouses;
    $data['warehouse_id']    = array_column( $warehouses, 'warehouse_id' );
    $propertyValues          = PropertyValue::find( [ 'product_id' => $data['id'] ] );
    $data['property_values'] = $propertyValues;
    $propertyIds             = [];
    if ( $propertyValues && count( $propertyValues ) > 0 ) {
      foreach ( $propertyValues as $propertyValue ) {
        if ( !in_array( $propertyValue['property_id'], $propertyIds ) ) {
          $propertyIds[] = $propertyValue['property_id'];
        }
      }
    }
    $properties         = Properties::find( [ 'id' => $propertyIds ] );
    $data['properties'] = $properties;
    $user               = UserHelper::getUser();
    if ( $user ) {
      $wishlist   = Wishlist::find( [ 'user_id' => $user['id'] ] );
      $productIds = array_column( $wishlist, 'product_id' );
      if ( in_array( $data['id'], $productIds ) ) {
        $data['wishlist'] = true;
      }
    } else {
      $wishlist = Session::getByKey( 'wishlist' );
      if ( $wishlist != false ) {
        if ( in_array( $data['id'], $wishlist ) ) {
          $data['wishlist'] = true;
        }
      }
    }
    $compare = Session::getByKey( 'compare' );
    if ( is_array( $compare ) ) {
      $compareIds = [];
      foreach ( $compare as $category_name => $products ) {
        $compareIds = array_merge( $compareIds, array_column( $products, 'id' ) );
      }
      if ( in_array( $data['id'], $compareIds ) )
        $data['compare'] = true;
    }
    return $data;
  }

  /**
   * @param int $count
   * @return array
   */
  public static function findPopular( $count = 12, $type = 0 )
  {
    $model = new Product( true );
    $sql   = "SELECT p.*, `c`.name as `category_name`, COUNT(*) as `count`
                FROM `product` as `p` 
                JOIN `order_product` as `op` ON p.id = op.product_id
                JOIN `category` as `c` ON c.id = p.category_id
                WHERE p.active = 1 AND p.status_id = " . DataHelper::PRODUCT_STATUS_ALLOWED . " AND p.type = " . $type . "
                GROUP BY op.product_id
                ORDER BY count DESC
                LIMIT 0, " . (int)$count;
    $data  = $model->sql( $sql )->fetchAll();
//    $user  = UserHelper::getUser();
//    if ( $user ) {
//      $wishlistModel = new Wishlist( true );
//      $wishlist      = $wishlistModel->where( [ 'user_id' => $user['id'] ] )->fetchAll();
//      $productIds    = array_column( $wishlist, 'product_id' );
//      foreach ( $data as $key => $product ) {
//        if ( in_array( $product['id'], $productIds ) ) {
//          $data[$key]['wishlist'] = true;
//        }
//      }
//    }
    return $data;
  }

  /**
   * @return array
   */
  public static function getMinMaxByCategories( $categories ): array
  {
    $categories = (array)$categories;
    $model      = new Product();
    $where      = 'WHERE `active` = 1 AND `status_id` = 4 AND (';
    foreach ( $categories as $category ) {
      $where .= ' category_id = ' . (int)$category . ' OR';
    }
    $where = substr( $where, 0, -2 ) . ')';
    $sql   = "SELECT MAX(`price`) as `max`, MIN(`price`) as `min` FROM `product` " . $where;
    $data  = $model->sql( $sql )->fetchOne();
    return [ $data['min'], $data['max'] ];
  }

  /**
   * @return array
   */
  public static function getOrderProducts( $page = 1 )
  {
    $model = new Product();
    $start = ( $page - 1 ) * self::$default_count_ajax;
    $user  = UserHelper::getUser();
    $sql   = "SELECT p.*, so.created_at as `product_sale`, so.courier_id as `courier_id`, ps.type as `status_name`, SUM(op.count) as `sum`
                FROM `product` as `p` 
                JOIN `order_product` as `op` ON p.id = op.product_id
                JOIN `store_order` as `so` ON  so.id = op.order_id
                LEFT JOIN `product_status` as `ps` ON p.status_id = ps.id 
                WHERE p.status_id = " . DataHelper::PRODUCT_STATUS_ALLOWED . "
                  AND p.seller_id = " . $user['id'] . "
                GROUP BY op.product_id
                LIMIT $start, " . self::$default_count_ajax;
    echo $sql;
    return $model->sql( $sql )->fetchAll();
  }

  /**
   * @return array
   */
  public static function getOrderProductsWeek()
  {
    $date  = new \DateTime( '-7 days' );
    $model = new Product();
    $user  = UserHelper::getUser();
    $sql   = "SELECT p.*,
                so.shop_id as `shop_id`,
                so.created_at as `product_sale`,
                so.courier_id as `courier_id`,
                ps.type as `status_name`,
                 SUM(op.count) as `sum`,
                 so.pay_date as pay_date,
                 so.total as order_price
                FROM `product` as `p` 
                JOIN `order_product` as `op` ON p.id = op.product_id 
                JOIN `store_order` as `so` ON  so.id = op.order_id AND so.pay_date >= " . $date->getTimestamp() . "
                LEFT JOIN `product_status` as `ps` ON p.status_id = ps.id 
                WHERE `p`.active = 1 AND p.status_id = " . DataHelper::PRODUCT_STATUS_ALLOWED . "
                  AND p.owner_id = " . $user['id'] . "
                GROUP BY op.product_id
                ORDER BY so.pay_date DESC
                ";
    return $model->sql( $sql )->fetchAll();
  }

  /**
   * @param $model
   * @return bool|void
   */
  protected function afterDelete( $model )
  {
    if ( !parent::afterDelete( $model ) )
      return;
    $products = $this->getDeletedItems();
    $ids      = array_column( $products, 'id' );
    PropertyValue::deleteFilter( [ 'product_id' => $ids ] );
    Product::removeFiles( array_column( $products, 'images' ) );
    Product::removeFiles( array_column( $products, 'certificate' ) );
    //
    //	    //Удаляем обзор
    //	    Review::deleteFilter(['product_id'=>$ids]);
    //
    //	    //Удаляем лист покупок
    //	    Wishlist::deleteFilter(['product_id'=>$ids]);
    //
    //	    //Удаляем заказы
    //	    OrderProduct::deleteFilter(['product_id'=>$ids]);
  }

  /**
   * @param $id
   * @param $properties
   */
  public static function loadProperties( $id, $properties )
  {
    $settingsProperties = Properties::find( [ 'active' => 1 ] );

    $errors = [];
    $data   = [];
    foreach ( $settingsProperties as $settingsProperty ) {
      if ( !isset( $properties[$settingsProperty['code']] ) )
        continue;
      switch ( $settingsProperty['type'] ) {
        case 'L':
        case 'T':
        case 'D':
        case 'S':
          if ( $settingsProperty['multi'] ) {
            $type = 'multi';
          } else {
            $type = 'value';
          }
          if ( $settingsProperty['type'] == 'L' ) {
            $type = 'enum';
          }
          $data[] = [
            'product_id'  => $id,
            'property_id' => $settingsProperty['id'],
            'value'       => isset( $properties[$settingsProperty['code']]['value'] )
              ? $properties[$settingsProperty['code']]['value']
              : null,
            'type'        => $type,
            'description' => isset( $properties[$settingsProperty['code']]['description'] )
              ? $properties[$settingsProperty['code']]['description']
              : '',
            'id'          => isset( $properties[$settingsProperty['code']]['id'] )
              ? $properties[$settingsProperty['code']]['id']
              : [],
            'images'      => false,
          ];

          break;
        case 'F':
          if ( isset( $_FILES['property']['tmp_name'][$settingsProperty['code']]['value'] ) ) {
            $validator = new \Application\Validators\ImageValidator();
            $validator->init();
            $images = [];
            if ( is_array( $_FILES['property']['tmp_name'][$settingsProperty['code']]['value'] ) ) {
              foreach ( $_FILES['property']['tmp_name'][$settingsProperty['code']]['value'] as $index => $f ) {
                if ( $_FILES['property']['name'][$settingsProperty['code']]['value'][$index] == '' && !$settingsProperty['required'] )
                  continue;
                $file = UploadedFile::load(
                  'property' . $index,
                  [
                    'name'     => $_FILES['property']['name'][$settingsProperty['code']]['value'][$index],
                    'tempName' => $_FILES['property']['tmp_name'][$settingsProperty['code']]['value'][$index],
                    'type'     => $_FILES['property']['type'][$settingsProperty['code']]['value'][$index],
                    'error'    => $_FILES['property']['error'][$settingsProperty['code']]['value'][$index],
                    'size'     => $_FILES['property']['size'][$settingsProperty['code']]['value'][$index],
                  ]
                );
                $validator->validate(
                  $file, $error
                );
                if ( $error === null ) {
                  $images[] = $file->save( new self() );
                } else
                  $errors[$settingsProperty['code']] = $error;
              }
              if ( $settingsProperty['multi'] ) {
                $data[] = [
                  'product_id'  => $id,
                  'property_id' => $settingsProperty['id'],
                  'value'       => $images,
                  'type'        => 'multi',
                  'description' => isset( $properties[$settingsProperty['code']]['description'] )
                    ? $settingsProperty['multi']
                      ? $properties[$settingsProperty['code']]['description']
                      : $properties[$settingsProperty['code']]['description'][0]
                    : '',
                  'images'      => true,
                  'id'          => isset( $properties[$settingsProperty['code']]['id'] )
                    ? $properties[$settingsProperty['code']]['id']
                    : [],
                ];
              } else {
                $data[] = [
                  'product_id'  => $id,
                  'property_id' => $settingsProperty['id'],
                  'value'       => $images,
                  'type'        => 'value',
                  'description' => isset( $properties[$settingsProperty['code']]['description'] )
                    ? $properties[$settingsProperty['code']]['description']
                    : '',
                  'images'      => true,
                  'id'          => isset( $properties[$settingsProperty['code']]['id'] )
                    ? $properties[$settingsProperty['code']]['id']
                    : [],
                ];
              }
            }
          }
          break;
        default:
          break;
      }

      if ( $settingsProperty['required'] && !$settingsProperty['multi'] && isset( $properties[$settingsProperty['code']]['value'] ) && $properties[$settingsProperty['code']]['value'] == '' ) {
        $errors[$settingsProperty['code']] = '';
      }
      if ( $settingsProperty['required'] && $settingsProperty['multi'] && isset( $properties[$settingsProperty['code']]['value'] ) && count( $properties[$settingsProperty['code']]['value'] ) == 0 ) {
        $errors[$settingsProperty['code']] = '';
      }
    }
    if ( count( $errors ) == 0 )
      foreach ( $data as $number => $info ) {

        if ( $info['value'] === null ) {
          foreach ( $info['id'] as $id_prop ) {
            if ( $id_prop == '' )
              continue;
            PropertyValue::deleteById( $id_prop );
          }
          continue;
        }

        if ( $info['type'] == 'enum' ) {
          $propertyVal = PropertyValue::find(
            [
              'product_id'  => $info['product_id'],
              'property_id' => $info['property_id']
            ]
          );
          $updated     = [];
          foreach ( $info['value'] as $key => $item ) {
            $item      = trim( $item );
            $valPropId = 0;
            foreach ( $propertyVal as $prop_val ) {
              if ( $item == $prop_val['value'] ) {
                $valPropId = $prop_val['id'];
                $updated[] = $prop_val['id'];
              }
            }
            if ( $valPropId == 0 ) {
              $dataInfo = [
                'product_id'  => $info['product_id'],
                'property_id' => $info['property_id'],
                'value_enum'  => $item,
                'description' => isset( $info['description'][0] )
                  ? $info['description'][0]
                  : '',
                'type'        => $info['type']
              ];
              if ( $item != '' )
                PropertyValue::create( $dataInfo );
            } else {
              $dataInfo = [
                'product_id'  => $info['product_id'],
                'property_id' => $info['property_id'],
                'value_enum'  => $item,
                'description' => isset( $info['description'][0] )
                  ? $info['description'][0]
                  : '',
                'type'        => $info['type'],
              ];

              PropertyValue::updateById( $valPropId, $dataInfo );
            }
          }
          foreach ( $propertyVal as $isDelete ) {
            if ( !in_array( $isDelete['id'], $updated ) ) {
              PropertyValue::deleteById( $isDelete['id'] );
            }
          }
        } else {

          foreach ( $info['value'] as $key => $item ) {
            $item     = trim( $item );
            $dataInfo = [
              'product_id'  => $info['product_id'],
              'property_id' => $info['property_id'],
              'value'       => $item,
              'description' => isset( $info['description'][$key] )
                ? $info['description'][$key]
                : '',
              'type'        => $info['type']
            ];

            if ( isset( $info['id'][$key] ) && $info['id'][$key] != '' ) {
              if ( $item == '' ) {
                PropertyValue::deleteById( $info['id'][$key] );
              } else {
                if ( $info['images'] ) {
                  $delImg = PropertyValue::findOne( $info['id'][$key] );
                  if ( $delImg ) {
                    @unlink( FOLDER . $delImg['value'] );
                  }
                }
                PropertyValue::updateById( $info['id'][$key], $dataInfo );
              }
            } else {
              if ( $item != '' ) {
                PropertyValue::create( $dataInfo );
              }
            }
          }
        }
      }
    return $errors;
  }

  /**
   * @param      $properties
   * @param bool $product_id
   * @return array
   */
  public static function findErrorsProperties( $properties, $product_id = false )
  {
    $settingsProperties = Properties::find( [ 'active' => 1 ] );
    $errors             = [];

    foreach ( $settingsProperties as $settingsProperty ) {
      if ( !isset( $properties[$settingsProperty['code']] ) )
        continue;
      switch ( $settingsProperty['type'] ) {
        case 'L':
        case 'T':
        case 'D':
        case 'S':
          if ( !$settingsProperty['required'] )
            break;
          if ( !isset( $properties[$settingsProperty['code']]['value'][0] ) || $properties[$settingsProperty['code']]['value'][0] == '' && !$product_id ) {
            $errors[$settingsProperty['code']] = '';
          }
          if ( $product_id ) {
            $prop = PropertyValue::find( [ 'product_id' => $product_id, 'property_id' => $settingsProperty['id'] ] );
            if ( !$prop && !isset( $properties[$settingsProperty['code']]['value'][0] ) || ( isset( $properties[$settingsProperty['code']]['value'][0] ) && $properties[$settingsProperty['code']]['value'][0] == '' ) ) {
              $errors[$settingsProperty['code']] = '';
            }
            if ( count( $prop ) == 1 && !isset( $properties[$settingsProperty['code']]['value'] ) || !isset( $properties[$settingsProperty['code']]['value'][0] ) || $properties[$settingsProperty['code']]['value'][0] == '' ) {
              $errors[$settingsProperty['code']] = '';
            }
          }
          break;
        case 'F':
          if ( $settingsProperty['required'] ) {
            if ( ( !isset( $_FILES['property']['tmp_name'][$settingsProperty['code']]['value'][0] ) || $_FILES['property']['tmp_name'][$settingsProperty['code']]['value'][0] == '' && !$product_id ) ) {
              $errors[$settingsProperty['code']] = '';
              break;
            }
            if ( $product_id ) {
              $prop = PropertyValue::find(
                [
                  'product_id'  => $product_id,
                  'property_id' => $settingsProperty['id']
                ]
              );
              if ( !$prop && $_FILES['property']['tmp_name'][$settingsProperty['code']]['value'][0] == '' ) {
                $errors[$settingsProperty['code']] = '';
                break;
              }
            }
          }
          if ( isset( $_FILES['property']['tmp_name'][$settingsProperty['code']]['value'] ) ) {
            $validator = new \Application\Validators\ImageValidator();
            $validator->init();
            if ( is_array( $_FILES['property']['tmp_name'][$settingsProperty['code']]['value'] ) ) {
              foreach ( $_FILES['property']['tmp_name'][$settingsProperty['code']]['value'] as $index => $f ) {
                if ( $_FILES['property']['name'][$settingsProperty['code']]['value'][$index] == '' )
                  continue;
                $validator->validate(
                  UploadedFile::load(
                    'property' . $index,
                    [
                      'name'     => $_FILES['property']['name'][$settingsProperty['code']]['value'][$index],
                      'tempName' => $_FILES['property']['tmp_name'][$settingsProperty['code']]['value'][$index],
                      'type'     => $_FILES['property']['type'][$settingsProperty['code']]['value'][$index],
                      'error'    => $_FILES['property']['error'][$settingsProperty['code']]['value'][$index],
                      'size'     => $_FILES['property']['size'][$settingsProperty['code']]['value'][$index],
                    ]
                  ), $error
                );
                if ( !is_null( $error ) )
                  $errors[$settingsProperty['code']] = $error;
              }
            }
          }
          break;
        default:
          break;
      }
    }
    return $errors;
  }

  /**
   * @param     $categoryID
   * @param int $page
   * @return array
   */
  public static function findUseFilter( $categoryID, $page = 1 )
  {
    $model           = new Product( true );
    $whereCategories = '';
    if ( is_array( $categoryID ) ) {
      $whereCategories .= ' AND (';
      foreach ( $categoryID as $k => $id ) {
        if ( $k > 0 )
          $whereCategories .= ' OR ';
        $whereCategories .= ' `p`.`category_id` = ' . $id;
      }
      $whereCategories .= ' ) ';
    } else {
      $whereCategories = ' AND `p`.`category_id` = ' . $categoryID;
    }
    $having = '';

    $filter = $_GET;

    if ( isset( $filter['price']['min'] ) && isset( $filter['price']['max'] ) ) {
      $having = "HAVING p.price>=" . (int)$filter['price']['min'] . " AND p.price<=" . (int)$filter['price']['max'];
    } elseif ( isset( $filter['price']['min'] ) ) {
      $having = "HAVING p.price=>" . (int)$filter['price']['min'];
    } elseif ( isset( $filter['price']['max'] ) ) {
      $having = "HAVING p.price<=" . (int)$filter['price']['max'];
    }
    $join = '';
    if ( isset( $filter['properties'] ) && is_array( $filter['properties'] ) ) {
      foreach ( $filter['properties'] as $id => $property_values ) {
        if ( !is_array( $property_values ) )
          break;
        $uniq = uniqid( 'pref' );
        $join .= "INNER JOIN `property_value` as `" . $uniq . "` on `" . $uniq . "`.`product_id`=`p`.`id`";
        $join .= ' AND (`' . $uniq . '`.`property_id` = ' . (int)$id . ') AND (';
        foreach ( $property_values as $value ) {
          $join .= '(`' . $uniq . '`.`value_enum` = ' . (int)$value . ') OR';
        }
        $join = substr( $join, 0, -2 );
        $join .= ')';
      }
    }
    $start = ( $page - 1 ) * self::$default_count;
    $end   = self::$default_count;
    $sql   = "SELECT COUNT(*) as `count` 
                FROM `product` as `p`
                $join
                WHERE `p`.`status_id` = " . DataHelper::PRODUCT_STATUS_ALLOWED . " $whereCategories $having";
    $count = $model->sql( $sql )->fetchOne();
    if ( $count ) {
      $count = $count['count'];
    } else {
      $count = 0;
    }
    $sql   = "SELECT p.*
                FROM `product` as `p`
                $join
                WHERE `p`.`status_id` = " . DataHelper::PRODUCT_STATUS_ALLOWED . " 
                $whereCategories 
                $having 
                ORDER BY " . self::$sort_order . " " . self::$sort_by . "  LIMIT $start, $end";
    $items = $model->sql( $sql )->fetchAll();
    return [ $count, $items ];
  }

  /**
   * @return bool|int
   */
  public static function getDefaultCount()
  {
    return self::$default_count;
  }

  /**
   * @param $categories_id
   * @return array
   */
  public static function findByLimitCategories( $categories_id )
  {
    $model = new Product( true );
    $sql   = [];
    foreach ( $categories_id as $id ) {
      if ( is_array( $id ) && count( $id ) > 0 ) {
        $where = '';
        foreach ( $id as $category_id ) {
          $where .= ' `category_id` = ' . $category_id . ' OR ';
        }
        $where = substr( $where, 0, -3 );
        $sql[] = " (SELECT * FROM `product` WHERE `active` = 1 AND `status_id` = " . DataHelper::PRODUCT_STATUS_ALLOWED . " AND ($where) LIMIT 0, 10) ";
      } else {
        $sql[] = " (SELECT * FROM `product` WHERE `active` = 1 AND `status_id` = " . DataHelper::PRODUCT_STATUS_ALLOWED . " AND `category_id` = $id LIMIT 0, 10) ";
      }
    }
    $select = implode( ' UNION ', $sql );
    return $model->sql( $select )->fetchAll();
  }

  /**
   * @param       $count
   * @param array $filter
   * @param array $order
   * @return mixed
   */
  public static function findLast( $count, $filter = [], $order = [ 'id' => 'desc' ] )
  {
    $model = new self( true );
    if ( count( $filter ) > 0 )
      $model->where( $filter );
    return $model->order( $order )->limit( $count )->fetchAll();
  }

  /**
   * @param $categoriesId
   * @param $userId
   * @return array
   */
  public static function findByCategoriesOnAgentPrice( $categoriesId, $userId )
  {
    $model = new self();
    $sql   = "SELECT product.*, price.price_agent as `price_agent`, price.discount as `discount_agent`, price.active as `active_agent` 
    FROM `product` 
              LEFT JOIN `price` ON `price`.`product_id` = `product`.`id` AND `price`.`agent_id` = $userId  
              WHERE `product`.active = 1 AND `product`.`category_id` IN (" . implode( ',', $categoriesId ) . ")";
    return $model->sql( $sql )->fetchAll();
  }

  /**
   * @param $id
   * @param $agent_id
   * @return mixed
   */
  public static function findOneAgent( $id, $agent_id )
  {
    $model = new self();
    $sql   = "SELECT product.*, price.price_agent as `price_agent`, price.discount as `discount_agent`, price.active as `active_agent` FROM `product` 
              LEFT JOIN `price` ON `price`.`product_id` = `product`.`id` AND `price`.`agent_id` = $agent_id  
              WHERE  `product`.active = 1 AND product.id = $id";
    return $model->sql( $sql )->fetchOne();
  }

}