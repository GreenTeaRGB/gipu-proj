<?php

  namespace Application\Models;

  use Application\Helpers\DataHelper;

  class News extends \Application\Classes\Model
  {
    public function __construct()
    {
      parent::__construct( 'news', $this->getTypes(), $this );
    }

    public function getTypes()
    {
      return [
          'id'                => 'int',
          'category_id'       => 'int',
          'alias'             => 'str',
          'name'              => 'str',
          'short_description' => 'str',
          'description'       => 'str',
          'image'             => 'str',
          'created_at'        => 'str',
          'updated_at'        => 'str',
          'active'            => 'int',
          'sort'              => 'int',

      ];
    }

    public function getFields()
    {
      return array(
          'id' => array(
              'type'    => 'string',
              'visible' => true,
              'edited'  => false,
              'label'   => 'ID'
          ),

          'category_id' => array(
              'type'     => 'select',
              'visible'  => false,
              'edited'   => true,
              'values'   => \Application\Models\NewsCategory::find(),
              'id_key'   => 'id',
              'name_key' => 'name',
              'label'    => 'Категория',
              'required' => false,
          ),

          'alias' => array(
              'type'     => 'string',
              'visible'  => true,
              'edited'   => true,
              'label'    => 'Ссылка',
              'required' => true,
              'multiple' => false,
          ),

          'name' => array(
              'type'     => 'string',
              'visible'  => true,
              'edited'   => true,
              'label'    => 'Заголовок',
              'required' => true,
              'multiple' => false,
          ),

          'short_description' => array(
              'type'     => 'text',
              'visible'  => false,
              'edited'   => true,
              'label'    => 'Краткое описание',
              'required' => true,
              'multiple' => false,
          ),

          'description' => array(
              'type'     => 'text',
              'visible'  => false,
              'edited'   => true,
              'label'    => 'Описание',
              'required' => true,
              'multiple' => false,
          ),

          'image' => array(
              'type'      => 'file',
              'visible'   => false,
              'edited'    => true,
              'label'     => 'Изображение',
              'required'  => false,
              'file-type' => 'image/*',
              'multiple'  => false,
          ),

          'created_at' => array(
              'type'     => 'date',
              'visible'  => false,
              'edited'   => true,
              'label'    => 'Дата создания',
              'required' => false,
              'multiple' => false,
          ),

          'updated_at' => array(
              'type'     => 'date',
              'visible'  => false,
              'edited'   => true,
              'label'    => 'Дата изменения',
              'required' => false,
              'multiple' => false,
          ),

          'active' => array(
              'type'     => 'checkbox',
              'visible'  => false,
              'edited'   => true,
              'label'    => 'Активность',
              'required' => false,
              'multiple' => false,
          ),

          'sort' => array(
              'type'     => 'string',
              'visible'  => false,
              'edited'   => true,
              'label'    => 'Сортировка',
              'required' => false,
              'multiple' => false,
          ),

      );
    }
	
	  public function rules()
	  {
		  return [
			  [ [ "name" ], 'string' ],
			  [ [ 'alias' ], 'link' ],
			  [ 'alias', 'unique' ],
			  [ [ "active", "sort", 'category_id' ], 'number' ],
			  [ [ "image" ], 'image', 'extensions' => [ 'png', 'jpg', 'jpeg' ] ],
			  [ [ "created_at", "updated_at" ], 'date' ],
			  [ [ "name", "short_description", "description", 'alias' ], 'required' ],
		
		  ];
	  }

    public function attributeLabels()
    {
      return [
          'name'              => 'Заголовок',
          'category_id'       => 'Категория',
          'alias'             => 'Ссылка',
          'short_description' => 'Краткое описание',
          'description'       => 'Описание',
          'image'             => 'Изображение',
          'created_at'        => 'Дата создания',
          'updated_at'        => 'Дата изменения',
          'active'            => 'Активность',
          'sort'              => 'Сортировка',

      ];
    }

    public function formFields( array $data = [] )
    {
      $fields = [
          'name'              => [ 'type' => 'text', 'class' => [ 'form-control' ] ],
          'alias'             => [ 'type' => 'text', 'class' => [ 'form-control' ] ],
          'category_id'       => [ 'type' => 'int', 'class' => [ 'form-control' ] ],
          'short_description' => [ 'type' => 'textarea', 'class' => [ 'form-control' ] ],
          'description'       => [ 'type' => 'textarea', 'class' => [ 'form-control' ] ],
          'image'             => [ 'type' => 'file', 'class' => [ 'form-control' ] ],
          'created_at'        => [ 'type' => 'date', 'class' => [ 'form-control' ] ],
          'updated_at'        => [ 'type' => 'date', 'class' => [ 'form-control' ] ],
          'active'            => [ 'type' => 'checkbox', 'class' => [ 'form-control' ] ],
          'sort'              => [ 'type' => 'text', 'class' => [ 'form-control' ] ],

      ];
      if( count( $data ) > 0 ) {
        $fieldsFilter = [];
        foreach( $data as $datum ) {
          if( isset( $fields[$datum] ) )
            $fieldsFilter[$datum] = $fields[$datum];
        }
        return $fieldsFilter;
      }
      return $fields;
    }


    protected function setCreatedAt( $element )
    {
      return strtotime( $element );
    }

    protected function setUpdatedAt( $element )
    {
      return strtotime( $element );
    }

    protected function getCreatedAt( $element )
    {
      return DataHelper::redate( 'd M Yг.', $element );
      //			return date('d.F.Yг.', $element);
    }

    protected function getUpdatedAt( $element )
    {
      return DataHelper::redate( 'd M Yг.', $element );
    }


    public static function findByCategoryId( $id, $count = 4 )
    {
      $model = new News();
      return $model->where(
          [
              'active'      => 1,
              'category_id' => $id
          ]
      )->limit( $count )->order( [ 'id' => 'DESC' ] )->fetchAll();
    }


    protected function afterDelete( $model )
    {
      if( !parent::afterDelete( $model ) )
        return;
      $news = $this->getDeletedItems();
      self::removeFiles( array_column( $news, 'image' ) );
    }

  }
