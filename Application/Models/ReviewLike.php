<?php
	
	namespace Application\Models;
	class ReviewLike extends \Application\Classes\Model
	{
		public function __construct()
		{
			parent::__construct('review_like', $this->getTypes(), $this);
		}
		
		public function getTypes()
		{
			return array(
				'id'        => 'int',
				'user_id'   => 'int',
				'review_id' => 'int',
				'type'      => 'str',
			);
		}
	}