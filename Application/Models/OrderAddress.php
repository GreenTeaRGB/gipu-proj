<?php
	
	namespace Application\Models;
	class OrderAddress extends \Application\Classes\Model
	{
		public function __construct()
		{
			parent::__construct('order_address', $this->getTypes(), $this);
		}
		
		public function getTypes()
		{
			return array(
				'id'                 => 'int',
				'order_id'           => 'int',
				'delivery_name'      => 'str',
				'delivery_last_name' => 'str',
				'patronymic'         => 'str',
				'country'            => 'str',
				'region'             => 'str',
				'city'               => 'str',
				'street'             => 'str',
				'house_number'       => 'int',
				'apartment'          => 'int',
				'post_index'         => 'str',
				'phone'              => 'str',
			);
		}
		
		public static function find($filter = [], $fetchOne = false)
		{
			$model = new OrderAddress();
			if ( count($filter) > 0 ) $model->where($filter);
			
			if ( $fetchOne ) {
				return $model->fetchOne();
			}
			return $model->fetchAll();
		}
	}