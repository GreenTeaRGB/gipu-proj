<?php

namespace Application\Models;
class PropertyEnum extends \Application\Classes\Model
{
  public function __construct()
  {
    parent::__construct( 'property_enum', $this->getTypes(), $this );
  }

  public function getTypes()
  {
    return [
        'id'          => 'int',
        'property_id' => 'int',
        'value'       => 'str',
        'description' => 'str',
        'file'        => 'str',
    ];
  }

  public function getFields()
  {
    return array(
        'id' => array(
            'type'    => 'string',
            'visible' => true,
            'edited'  => false,
            'label'   => 'ID'
        ),

        'property_id' => array(
            'type'     => 'string',
            'visible'  => true,
            'edited'   => true,
            'label'    => 'Идентификатор свойства',
            'required' => false,
            'multiple' => false,
        ),

        'value' => array(
            'type'     => 'string',
            'visible'  => true,
            'edited'   => true,
            'label'    => 'Значение',
            'required' => false,
            'multiple' => false,
        ),

        'description' => array(
            'type'     => 'text',
            'visible'  => true,
            'edited'   => true,
            'label'    => 'Описание',
            'required' => false,
            'multiple' => false,
        ),

        'file' => array(
            'type'      => 'file',
            'visible'   => false,
            'edited'    => true,
            'required'  => false,
            'label'     => 'file',
            'multiple'  => false,
            'file-type' => 'image/*',
        ),

    );
  }
	
	public function rules()
	{
		return [
			[ [ "value", "file" ], 'string' ],
			[ [ "property_id" ], 'number' ],
			[ [ "property_id", "value" ], 'required' ],
		
		];
	}

  public function attributeLabels()
  {
    return [
        'property_id' => 'Идентификатор свойства',
        'value'       => 'Значение',
        'description' => 'Описание',
        'file'        => 'file',

    ];
  }

  public function formFields( array $data = [] )
  {
    $fields = [
        'property_id' => [ 'type' => 'text', 'class' => [ 'form-control' ] ],
        'value'       => [ 'type' => 'text', 'class' => [ 'form-control' ] ],
        'description' => [ 'type' => 'textarea', 'class' => [ 'form-control' ] ],
        'file'        => [ 'type' => 'text', 'class' => [ 'form-control' ] ],

    ];
    if( count( $data ) > 0 ) {
      $fieldsFilter = [];
      foreach( $data as $datum ) {
        if( isset( $fields[$datum] ) )
          $fieldsFilter[$datum] = $fields[$datum];
      }
      return $fieldsFilter;
    }
    return $fields;
  }

  public static function deleteByProperty( $propertyId )
  {
    $model = new PropertyEnum();
    return $model->where( [ 'property_id' => $propertyId ] )->delete();
  }
	
	public function afterDelete($model){
    if(!parent::afterDelete($model)) return;
		$propertyEnum = $this->getDeletedItems();
		PropertyEnum::removeFiles(array_column($propertyEnum, 'file'));
    PropertyValue::deleteFilter(['value_enum'=>array_column($propertyEnum, 'id')]);
//		$ids = array_column($propertyEnum, 'property_id');
//		$model = new PropertyEnum();
//		if(!($model->where( [ 'property_id' => $ids ] )->fetchOne())){
//			PropertyValue::DeleteFilter(['value_enum'=>$ids]);
//		}
	}
 
}
