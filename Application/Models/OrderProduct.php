<?php

namespace Application\Models;

/**
 * Class OrderProduct
 * @package Application\Models
 */
class OrderProduct extends \Application\Classes\Model
{
  /**
   * OrderProduct constructor.
   */
  public function __construct()
  {
    parent::__construct( 'order_product', $this->getTypes(), $this );
  }

  /**
   * @return array|mixed
   */
  public function getTypes()
  {
    return array(
      'id'         => 'int',
      'order_id'   => 'int',
      'product_id' => 'int',
      'buyer_id'   => 'int',
      'count'      => 'int',
      'shop_id'    => 'int',
      'properties' => 'str',
      'buy_price'  => 'str'
    );
  }

  public function getFields()
  {
    return [
      'order_id'   => [
        'type'     => 'string',
        'visible'  => true,
        'edited' => false,
        'label'    => 'ID заказа',
        'sort' => 1
      ],
      'product_id' => [
        'type'     => 'string',
        'visible'  => false,
        'edited' => false,
        'label'    => 'ID продукта',
        'sort' => 500
      ],
      'buyer_id'   => [
        'type'     => 'string',
        'visible'  => true,
        'edited' => false,
        'label'    => 'ID покупателя',
        'sort' => 4
      ],
      'count'      => [
        'type'     => 'string',
        'visible'  => false,
        'edited' => false,
        'label'    => 'Количество',
        'sort' => 500
      ],
      'shop_id'    => [
        'type'     => 'string',
        'visible'  => true,
        'edited' => false,
        'label'    => 'ID сайта продаж',
        'sort' => 500
      ],
      'buy_price'  => [
        'type'     => 'string',
        'visible'  => false,
        'edited' => false,
        'label'    => 'Цена покупки',
        'sort' => 500
      ],
    ];
  }

  /**
   * @param $element
   * @return mixed
   */
  protected function getProperties( $element )
  {
    return self::unserialize( $element );
  }

  /**
   * @param $element
   * @return string
   */
  protected function setProperties( $element )
  {
    return self::serialize( $element );
  }

  /**
   * @param array $filter
   * @param bool  $fetchOne
   * @return array|mixed
   */
  public static function find( $filter = [], $fetchOne = false )
  {
    $model = new OrderProduct();
    if ( count( $filter ) > 0 )
      $model->where( $filter );

    if ( $fetchOne ) {
      return $model->fetchOne();
    }
    return $model->fetchAll();
  }

  /**
   * @param $orderId
   * @return array
   */
  public static function findProductsByOrderId( $orderId )
  {
    $model = new Product();
    $sql   = "SELECT p.* FROM `product` as `p` JOIN `order_product` as `op` ON op.product_id = p.id WHERE op.order_id = $orderId";
    return $model->sql( $sql )->fetchAll();
  }


}