<?php
	
	namespace Application\Models;
	class PayType extends \Application\Classes\Model
	{
		public function __construct()
		{
			parent::__construct('pay_type', $this->getTypes(), $this);
		}
		
		public function getTypes()
		{
			return array(
				'id'   => 'int',
				'type' => 'str'
			);
		}
		
		public static function getTypeById($id)
		{
			$model = new PayType();
			$payType = $model->where(['id' => $id])->fetchOne();
			
			if ( $payType ) {
				return $payType['type'];
			}
			
			return false;
		}
		
		public static function find($filter = [], $fetchOne = false)
		{
			$model = new PayType();
			if ( count($filter) > 0 ) $model->where($filter);
			
			if ( $fetchOne ) {
				return $model->fetchOne();
			}
			return $model->fetchAll();
		}
	}