<?php

namespace Application\Models;

use Application\Helpers\DataHelper;

class Message extends \Application\Classes\Model
{
  public function __construct()
  {
    parent::__construct( 'message', $this->getTypes(), $this );
  }

  public function getTypes()
  {
    return array(
      'id'         => 'int',
      'user_id'    => 'int',
      'created_at' => 'int',
      'read_at'    => 'int',
      'dialog_id'  => 'int',
      'text'       => 'str',
      'attachment' => 'str',
      'status'     => 'int',
    );
  }

  public static function find( $filter = [], $fetchOne = false )
  {
    $model = new Message();
    if ( count( $filter ) > 0 ) $model->where( $filter );

    if ( $fetchOne ) {
      return $model->fetchOne();
    }
    return $model->fetchAll();
  }

  protected function beforeInsert( $fields )
  {
    $unixTime = $this->getUnixTime();
    $fields['created_at'] = $unixTime;
    $fields['status'] = DataHelper::DEFAULT_DIALOG_MESSAGE_STATUS;
    return $fields;
  }

  public function readMessages( $messageIds )
  {
    $unixTime = $this->getUnixTime();
    $done = $this->setFields( [
      'read_at' => $unixTime,
      'status'  => DataHelper::DIALOG_MESSAGE_STATUS_READ
    ] )->where( [ 'id' => $messageIds ] )->update();
    return $done;
  }
}