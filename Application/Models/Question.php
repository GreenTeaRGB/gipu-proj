<?php
	
	namespace Application\Models;
	class Question extends \Application\Classes\Model
	{
		public function __construct()
		{
			parent::__construct('question', $this->getTypes(), $this);
		}
		
		public function getTypes()
		{
			return [
				'id'        => 'int',
				'name'      => 'str',
				'parent_id' => 'int',
				'text'      => 'str',
			
			];
		}
		
		public function getFields()
		{
			return array(
				'id' => array(
					'type'    => 'string',
					'visible' => true,
					'edited'  => false,
					'label'   => 'ID'
				),
				
				'name' => array(
					'type'     => 'text',
					'visible'  => true,
					'edited'   => true,
					'label'    => 'Вопрос',
					'required' => true,
					'multiple' => false,
				),
				
				'parent_id' => array(
					'type'     => 'select',
					'visible'  => true,
					'edited'   => true,
					'label'    => 'Родительская категория',
					'required' => false,
					'values'   => Question::find(),
					'id_key'   => 'id',
					'name_key' => 'name',
				),
				
				'text' => array(
					'type'     => 'text',
					'visible'  => true,
					'edited'   => true,
					'label'    => 'Текст',
					'required' => false,
					'multiple' => false,
				),
			
			);
		}
		
		public function rules()
		{
			return [
				[["parent_id"], 'number'],
				[["name", "parent_id"], 'required'],
			
			];
		}
		
		public function attributeLabels()
		{
			return [
				'name'      => 'Вопрос',
				'parent_id' => 'Идентификатор родительской категории',
				'text'      => 'Текст',
			
			];
		}
		
		public function formFields(array $data = [])
		{
			$fields = [
				'name'      => ['type' => 'textarea', 'class' => ['form-control']],
				'parent_id' => ['type' => 'text', 'class' => ['form-control']],
				'text'      => ['type' => 'textarea', 'class' => ['form-control']],
			
			];
			if ( count($data) > 0 ) {
				$fieldsFilter = [];
				foreach ($data as $datum) {
					if ( isset($fields[$datum]) )
						$fieldsFilter[$datum] = $fields[$datum];
				}
				return $fieldsFilter;
			}
			return $fields;
		}
		
	}
