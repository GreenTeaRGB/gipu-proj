<?php
	
	namespace Application\Controllers;
	
	use \Application\Helpers\UserHelper;
	use \Application\Helpers\DataHelper;
	use \Application\Models\ReviewLike;
	use \Application\Models\Review;
	
	class LikeController extends \Application\Classes\ControllerBase
	{
		public function actionIndex()
		{
			return true;
		}
		
		public function actionToggle()
		{
			$response = [
				'success' => false,
				'message' => 'Ошибка'
			];
			
			$request = $_POST;
			$user = UserHelper::getUser();

			if ( isset($request['is_ajax'])
                && $request['is_ajax']
                && isset($request['type'])
                && $request['review_id']
                && $user
                && $user['type'] == DataHelper::DEFAULT_BUYER_TYPE_ID
            ) {
				$reviewLikeModel = new ReviewLike();
				$reviewLike = $reviewLikeModel->where(['review_id' => $request['review_id'], 'user_id' => $user['id']])->fetchOne();
				
				$reviewModel = new Review();

				if ( $reviewLike ) {
					$type = $reviewLike['type'];
					
					if ( $type == $request['type'] ) {
						$sql = "UPDATE `review` SET `{$request['type']}` = `{$request['type']}` - 1 WHERE `id` = {$request['review_id']} AND `{$request['type']}` > 0";
						$reviewLikeModel->where(['review_id' => $request['review_id'], 'user_id' => $user['id']])->delete();
					} else {
						$sql = "UPDATE `review` SET `{$request['type']}` = `{$request['type']}` + 1, `{$type}` = `{$type}` - 1 WHERE `id` = {$request['review_id']} AND `{$type}` > 0";
						$reviewLikeModel->where(['review_id' => $request['review_id'], 'user_id' => $user['id']])->setFields(['type' => $request['type']])->update();
					}
					
					$reviewModel->sql($sql)->exec();
				} else {
					$request['user_id'] = $user['id'];
					$reviewLikeId = $reviewLikeModel->setFields($request)->insert();
					
					if ( $reviewLikeId ) {
						
						$sql = "UPDATE `review` SET `{$request['type']}` = `{$request['type']}` + 1 WHERE `id` = {$request['review_id']}";
						
						$reviewModel->sql($sql)->exec();
					}
				}
				$reviewUpdated = $reviewModel->where(['id' => $request['review_id']])->fetchOne();
				
				$response = [
					'success'       => true,
					'message'       => 'Добавлено',
					'like_count'    => $reviewUpdated['like'],
					'dislike_count' => $reviewUpdated['dislike'],
				];
			}
			
			echo json_encode($response);
			return true;
		}
	}