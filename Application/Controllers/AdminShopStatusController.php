<?php
namespace Application\Controllers;
use \Application\Classes\AdminBuilder;
	use \Application\Models\ShopStatus;
class AdminShopStatusController extends \Application\Classes\AdminBase
{

	public function actionIndex($page = 1)
	{
		$builder = new AdminBuilder(new ShopStatus(), [] ,['activate'=>'active']);
		$builder->index($page);
		return true;
	}

	public function actionCreate()
	{
		$builder = new AdminBuilder(new ShopStatus());
		$builder->create();
		return true;
	}

	public function actionEdit($id)
	{
		$builder = new AdminBuilder(new ShopStatus());
		$builder->edit($id);
		return true;
	}
	
	public function actionActivate(){
		$builder = new AdminBuilder(new ShopStatus());
		$builder->active();
	}
	
	public function actionSortable(){
		$builder = new AdminBuilder(new ShopStatus());
		$builder->sort();
	}
	public function actionDelete(){
		$builder = new AdminBuilder(new ShopStatus());
		$builder->delete();
	}
	
	
}
?>