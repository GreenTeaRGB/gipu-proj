<?php

namespace Application\Controllers;

use Application\Helpers\DataHelper;
use Application\Helpers\UserHelper;
use Application\Models\Category;
use Application\Models\JuristicData;
use Application\Models\PayCard;
use Application\Models\Price;
use Application\Models\Product;
use Application\Models\ReportTransaction;
use Application\Models\RequestOutput;
use Application\Models\Shop;
use Application\Models\User;
use Application\Models\Warehouse;

class CabinetController extends \Application\Classes\ControllerBase
{
  private $user;

  CONST DEFAULT_COUNT_PRODUCT = 10;

  public function __construct()
  {
    $this->user = UserHelper::getUser();
    if ( !$this->user )
      $this->json( false );
  }

  function actionIndex()
  {
    // TODO: Implement actionIndex() method.
  }

  public function actionGetDashboardData()
  {

    $juristicModel             = new JuristicData();
    $juristicData              = $juristicModel->where( [ 'id' => $this->user['juristic_data_id'] ] )->fetchOne();
    $this->user['data']        = $juristicData;
    $this->user['shops']       = Shop::find( [
      'seller_id' => $this->user['id'],
      'status'    => DataHelper::SHOP_STATUS_ACTIVE
    ] );
    $products                  = [];
    $days                      = [];
    $day                       = new \DateTime();
    $days[$day->format( 'd' )] = [ 'count' => 0 ];
    for ( $i = 1; $i < 7; $i++ ) {
      $d = $day->modify( '-1 day' )->format( 'd' );
      foreach ( $this->user['shops'] as $shop ) {
        $days[$d][$shop['id']] = [ 'count' => 0 ];
      }
    }
//echo '<pre>';
//var_dump($this->user['shops'], Product::getOrderProductsWeek());
//die;
//    foreach ( Product::getOrderProductsWeek() as $product ) {
//      $day = new \DateTime($product['pay_date']);
//      $d = $day->format('d');
//      $days[$d][$product['shop_id']]['count'] += $product['order_price'];
//    }
//      $popularProducts = Product::getPopularProductMonth();
//      $warehouses      = Warehouse::find(['!status_id'=>3]);
    $this->json( true, 'Данные пользователя загружены', [
      'user'      => $this->user,
      'products'  => $products,
      'statistic' => $days
      //          'popularProducts' => $popularProducts,
      //          'warehouses'      => $warehouses
    ] );
  }

  public function actionGetWallet( $page = 1 )
  {
    $reportTransaction = new ReportTransaction();
    $where             = [
      'user_from' => $this->user['id'],
      'type'      => DataHelper::TYPE_TRANSACTION_INFO,
    ];
    $orWhere           = [
      'user_to' => $this->user['id'],
      'type'    => DataHelper::TYPE_TRANSACTION_INFO,
    ];
    if ( isset( $_POST['min-date'] ) && $_POST['min-date'] != '' ) {
      $where['>=created_at']   = strtotime( $_POST['min-date'] );
      $orWhere['>=created_at'] = strtotime( $_POST['min-date'] );
    }
    if ( isset( $_POST['max-date'] ) && $_POST['max-date'] != '' ) {
      $where['<=created_at']   = strtotime( $_POST['max-date'] );
      $orWhere['<=created_at'] = strtotime( $_POST['max-date'] );
    }

    $count = $data = $reportTransaction->where( $where )->orWhere( $orWhere )->count();

    $data      = $reportTransaction->where( $where )->orWhere( $orWhere )->order( [ 'id' => 'desc' ] )
                                   ->limit( ( ( $page - 1 ) * 10 ), 10 )->fetchAll( false );
    $week      = time() - ( 7 * 24 * 60 * 60 );
    $priceWeek = 0;
    foreach ( $data as $datum ) {
      if ( strtotime( $datum['created_at'] ) > $week ) {
        if ( $this->user['id'] == $datum['user_from'] ) {
          $priceWeek -= $datum['sum'];
        } elseif ( $this->user['id'] == $datum['user_to'] ) {
          $priceWeek += $datum['sum'];
        }
      }
    }
    $this->json( true, 'Статискика денежных переводов', [
      'data'      => $data,
      'priceWeek' => $priceWeek,
      'count'     => $count
    ] );
  }

  public function actionGetCards()
  {
    $this->json( true, 'Карты', [ 'cards' => PayCard::find( [ 'user_id' => $this->user['id'] ] ) ] );
  }

  public function actionAddCard()
  {
    $model = new PayCard();
    if ( $model->load( [ 'card_num' => $_POST['card'], 'user_id' => $this->user['id'] ], '' ) && $model->validate() ) {
      if ( $id = $model->created() ) {
        $this->json( true, 'Карт успешно добавлена', [ 'id' => $id ] );
      }
      $this->json( false, 'Ошибка добавления карты' );
    }
    $this->json( false, 'Ошибка добавления карты', [ 'errors' => $model->getErrors() ] );
  }

  public function actionDeleteCard( $id )
  {
    if ( PayCard::deleteById( $id ) ) {
      $this->json( true, 'Карта успешно удалена' );
    }
    $this->json( false, 'Ошибка удаления карты' );
  }

  public function actionUpdateData()
  {
    if ( isset( $_POST['User'] ) ) {
      $userModel = new User();
      $userModel->setScenario( 'update_in_cabiten' );
      $userModel->id = $this->user['id'];
      if ( $userModel->load( $_POST ) && $userModel->validate() ) {
        if ( $userModel->updated( $this->user['id'] ) ) {
          if ( !isset( $_POST['JuristicData'] ) )
            $this->json( true, 'Данные успешно сохранены' );
        } else
          $this->json( false, 'Ошибка при обновлении пользователя' );
      } else {
        $this->json( false, 'errors', [ 'errors' => $userModel->getErrors() ] );
      }
    }
    if ( isset( $_POST['JuristicData'] ) ) {
      $juristicModel = new JuristicData();
      if ( $juristicModel->load( $_POST ) && $juristicModel->validate() ) {
        if ( $juristicModel->updated( $this->user['juristic_data_id'] ) )
          $this->json( true, "Данные успешно обновлены" );
        else
          $this->json( false );
      } else {
        $this->json( false, 'errors', [ 'errors' => $juristicModel->getErrors() ] );
      }
    }
    $this->json( false, 'Данные отсутствуют' );
  }

  public function actionCreateWarehouse()
  {
    $model              = new Warehouse();
    $_POST['owner_id']  = $this->user['id'];
    $_POST['status_id'] = DataHelper::DEFAULT_STATUS_WAREHOUSE;
    if ( $model->load( $_POST, '' ) && $model->validate() ) {
      $id = $model->created();
      $this->json( true, 'Склад успешно добавлен', [ 'id' => $id ] );
    } else {
      $this->json( false, 'Ошибка при добавление склада', [ 'errors' => $model->getErrors() ] );
    }
    return true;
  }

  public function actionDeleteWarehouse( $id )
  {
    if ( Warehouse::deleteFilter( [ 'id' => $id, 'owner_id' => $this->user['id'] ] ) ) {
      $this->json( true, 'Склад успешно удален' );
    } else {
      $this->json( false, 'Ошибка удалениея склада' );
    }
  }

  public function actionUpdateWarehouse( $id )
  {
    $model = new Warehouse();
    $model->setScenario( 'cabinetUpdate' );
    if ( $model->load( $_POST, '' ) && $model->validate() ) {
      $model->updated( $id );
      $this->json( true, 'Склад успешно обновлен' );
    } else {
      $this->json( false, 'Ошибка при обновлении', [ 'errors' => $model->getErrors() ] );
    }
    $this->json( false );
  }

  public function actionGetDataWarehouse( $id, $page = 1 )
  {
    $user     = UserHelper::getUser();
    $verified = UserHelper::userIsVerified();
    if ( $verified ) {
      $warehouse = Warehouse::getWarehouseById( $id );
      $products  = Product::getProductsByOwnerIdAndWarehouseId( $user['id'], $warehouse['id'], $page );

//          Session::store( [ 'warehouseId' => $warehouse['id'] ] );

      $this->json( true, '', [
        'warehouse'  => $warehouse,
        'products'   => $products,
        'categories' => Category::getFullList()
      ] );
    } else {
      $this->json( false, self::DEFAULT_VERIFICATION_USER_MESSAGE );
    }
    return true;
  }

//  public function actionGetDataOrdersProduct($page = 1){
//    $products = Order::findProductBySellerId($this->user['id'], $page, self::DEFAULT_COUNT_PRODUCT);
//  }

  public function actionCheckUserExist( $id )
  {
    $user = new User();
    if ( $userData = $user->where( [ 'id' => $id ] )->fetchOne() ) {
      $this->json( true, 'Пользователь найден', [ 'user' => $userData ] );
    }
    $this->json( false, 'Ползователь не найден' );
  }

  public function actionCheckSmsCode( $code )
  {
    if ( isset( $_SESSION['sms_code'] ) && $_SESSION['sms_code'] == $code )
      $this->json( true );
    $this->json( false );
  }

  public function actionSendSmsCode()
  {
    $_SESSION['sms_code'] = 123;
    $this->json( true );
  }

  public function actionTransfer()
  {
    if ( $_POST['waitCode'] != true || $_POST['sms_code'] != $_SESSION['sms_code'] )
      $this->json( false, 'Не правильный код из смс' );
    if ( $this->user['balance'] < $_POST['sum'] )
      $this->json( false, 'Недостаточно денежных средств' );
    if ( $_POST['user_id'] == $this->user['id'] )
      $this->json( false, 'Нельзя переводить деньги себе' );
    if ( $user = User::getById( $_POST['user_id'] ) ) {
//      if ( User::updateById( $user['id'], [ 'balance' => ( $user['balance'] + $_POST['sum'] ) ] ) ) {
//        if ( User::updateById( $this->user['id'], [ 'balance' => ( $this->user['balance'] - $_POST['sum'] ) ] ) ) {
      $transaction = ReportTransaction::create(
        [
          'user_from' => $this->user['id'],
          'user_to'   => $user['id'],
          'type'      => DataHelper::TYPE_TRANSACTION_INFO,
          'sum'       => $_POST['sum'],
          'text'      => 'Перевод средств'
        ]
      );
      if ( $transaction )
        $this->json( true, 'Запрос успешно отправлен' );
      else
        $this->json( false, 'Ошибка выполнения операции' );
//        }
//      }
//      $this->json( false, 'Неудалось перевести денежные средства' );
    }
    $this->json( false, 'Пользователь не найден' );
//    User::updateById($this->user['id'], ['balance' => ])
  }

  public function actionRequestOutput()
  {
    if ( $_POST['sms_code'] != $_SESSION['sms_code'] )
      $this->json( false, 'Неверный код из смс' );
    if ( $this->user['balance'] < $_POST['sum'] )
      $this->json( false, 'Недостаточно денежных средств' );
    $juristicModel = new JuristicData();
    $juristicData  = $juristicModel->where( [ 'id' => $this->user['juristic_data_id'] ] )->fetchOne();
    if ( $id = RequestOutput::create( [
      'user_id'          => $this->user['id'],
      'sum'              => $_POST['sum'],
      'card'             => $_POST['card'],
      'juristic_type_id' => $this->user['juristic_type_id'],
      'bank_name'        => $juristicData['bank_name'],
      'status_id'        => DataHelper::DEFAULT_REQUEST_OUTPUT_STATUS
    ] ) ) {
      $this->json( true );
    }
    $this->json( false, 'Ошибка при выполнении операции' );
  }

  public function actionReferralList()
  {
    $verified = UserHelper::userIsVerified();

    if ( $verified ) {
      $referrals = User::find( [ 'referral_id' => $this->user['id'] ] );

      $ids = array_column( $referrals, 'id' );
//      $income = ReportTransaction::find(['user_from' => $ids, 'user_to' => $this->user['id']]);
//      $incomeTotal = 0;
//      $incomeMonth = 0;
//      $month = time() - (30 * 24 * 60 * 60);
//      foreach ( $income as $in ) {
//        $createTime = strtotime($in['created_at']);
//        if($in['user_from'] == $this->user['id']){
//          if($createTime > $month){
//            $incomeMonth -= $in['sum'];
//          }
//          $incomeTotal -= $in['sum'];
//        }else{
//          if($createTime < $month){
//            $incomeMonth += $in['sum'];
//          }
//          $incomeTotal += $in['sum'];
//        }
//      }
      $shops = Shop::find( [ 'seller_id' => $ids ] );
      $this->json( true, 'Даные загружены', [
        'referrals' => $referrals,
        'total'     => count( $referrals ),
        'month'     => count( array_filter( $referrals, function ( $value ) {
          return strtotime( $value['registration_date'] ) < time() - ( 30 * 24 * 60 * 60 );
        } ) ),
        //        'incomeTotal' => $incomeTotal,
        //        'incomeMonth' => $incomeMonth,
        'shops'     => $shops
      ] );
    } else {
      $this->json( false, self::DEFAULT_VERIFICATION_USER_MESSAGE );

    }
  }

  public function actionUpdateShop( $id )
  {
    $model = new Shop();
    $model->setScenario( 'update_name' );
    if ( $model->load( $_POST, '' ) && $model->validate() ) {
      if ( $model->updated( $id ) )
        $this->json( true, 'Данные успешно обновлены' );
      else
        $this->json( false, 'Ошибка обновления' );
    } else {
      $this->json( false, 'Ошибка обновления', [ 'errors' => $model->getErrors() ] );
    }
    $this->json( false );
  }

  public function actionGetCategoryAndProductByShopId( $shop_id )
  {
    $shop = Shop::findOne( $shop_id );
    list( $categories, $allId ) = Category::getTwoLevelByIds( $shop['categories'] );
    $products = Product::findByCategoriesOnAgentPrice( $allId, $this->user['id'] );
    if ( $products )
      $this->json( true, 'Данные загружены', [
        'shop'       => $shop,
        'categories' => $categories,
        'products'   => $products
      ] );
    else
      $this->json( false );
  }

  public function actionUpdatePriceProduct( $product_id )
  {
    if ( !isset( $_POST['price_agent'] ) || !isset( $_POST['shop_id'] ) )
      $this->json( false, 'oops' );
    $model               = new Price();
    $modelProduct        = new Product();
    $product             = $modelProduct->where( [ 'id' => $product_id ] )->fetchOne();
    $_POST['product_id'] = $product_id;
    $_POST['agent_id']   = $this->user['id'];
    if ( $product['agent_price'] > $_POST['price_agent'] ) {
      $this->json( false, 'Цена не может быть ниже чем ' . $product['agent_price'] );
    }
    $model->setScenario( 'update_price' );
    if ( $model->load( $_POST, '' ) && $model->validate() ) {
      if ( $price = Price::findOne( [
        'product_id' => $product_id,
        'agent_id'   => $this->user['id'],
        'shop_id'    => $_POST['shop_id']
      ] ) ) {
        if ( $model->updated( $price['id'] ) )
          $this->json( true, 'Цена успешно обновлена' );
      } else {
        if ( $model->created() ) {
          $this->json( true, 'Цена успешно создана' );
        }
        $this->json( false );
      }
    } else {
      $this->json( false, 'Ошибка сохранения', [ 'errors' => $model->getErrors() ] );
    }
    $this->json( false );
  }

  public function actionChangeActiveProduct( $product_id )
  {
    if ( !isset( $_POST['active'] ) )
      $this->json( false, '1' );
    if ( $_POST['active'] != 1 && $_POST['active'] != 0 )
      $this->json( false, '2' );
    if ( Price::updateByFilter( [
      'product_id' => $product_id,
      'agent_id'   => $this->user['id']
    ], [ 'active' => $_POST['active'] ] ) )
      $this->json( true, 'Операция прошла успешно' );
    else
      $this->json( false, 'Ошибка выполнения оперции' );
  }

  public function actionGetProduct( $product_id )
  {
    if ( $product = Product::findOneAgent( $product_id, $this->user['id'] ) ) {
      $this->json( true, 'Товар получен', [ 'product' => $product ] );
    }
    $this->json( false );
  }

  public function actionUpdateDiscount( $product_id )
  {
    if ( !isset( $_POST['discount'] ) )
      $this->json( false, 'oops' );
    $model               = new Price();
    $_POST['product_id'] = $product_id;
    $_POST['agent_id']   = $this->user['id'];
    if ( $model->load( $_POST, '' ) && $model->validate() ) {
      if ( $model->updated( [ 'product_id' => $product_id, 'agent_id' => $this->user['id'] ] ) )
        $this->json( true, 'Операция прошла успешно' );
    } else {
      $this->json( false, 'Ошибка сохранения', [ 'errors' => $model->getErrors() ] );
    }
    $this->json( false );
  }

  public function actionDeleteProduct( $product_id )
  {
    if ( Price::deleteFilter( [ 'product_id' => $product_id, 'agent_id' => $this->user['id'] ] ) )
      $this->json( true, 'Товар успешно удален' );
    else
      $this->json( false, 'Ошибка удаления товара' );
  }

}