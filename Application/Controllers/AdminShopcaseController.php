<?php

namespace Application\Controllers;

use \Application\Classes\AdminBuilder;
use \Application\Models\Shopcase;

class AdminShopcaseController extends \Application\Classes\AdminBase
{

  public function actionIndex( $page = 1 )
  {
    $builder = new AdminBuilder( new Shopcase(), [] );
    $builder->edit( 1 );
    return true;
  }

  public function actionModal()
  {
    $builder = new AdminBuilder( new Shopcase() );
    $builder->modal();
    return true;
  }

}

?>