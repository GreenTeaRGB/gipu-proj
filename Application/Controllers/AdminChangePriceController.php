<?php

namespace Application\Controllers;

use \Application\Classes\AdminBuilder;
use \Application\Models\Product;

class AdminChangePriceController extends \Application\Classes\AdminBase
{

  public function actionIndex( $page = 1 )
  {
    $builder = new AdminBuilder( new Class extends Product {
      public function getFields(){
        $fields = parent::getFields();
        $visibleFields = ['id', 'price', 'old_price', 'owner_id', 'active'];
        foreach ( $fields as $field => $settings ) {
          if(!in_array($field, $visibleFields)){
            $fields[$field]['visible'] = false;
          }else{
            $fields[$field]['visible'] = true;
          }
        }
        $fields['price']['label'] = 'Новая цена';
        $fields['price']['sort'] = 480;
        $fields['active']['label'] = 'Скрыть';
        $fields['id']['label'] = 'id продукта';
        $fields['owner_id']['label'] = 'Дестрибьютор';
        $fields['owner_id']['sort'] = 2;
        $fields['edit'] = [
          'type' => 'string',
          'visible' => true,
          'label' => 'Изменить',
          'no_filter' => true,
          'sort' => 490
        ];
        return $fields;
      }
    }, [], ['hide' => ['delete', 'view', 'edit' ] , 'activate' => 'active'] );
    $builder->setView('index');
    $builder->index( $page );
    $items = $builder->getItems();
    foreach ( $items as $id => $item )
      $items[$id]['edit'] = '<a href="/admin/product/edit/'.$id.'">Изменить</a>';
    $builder->setItems($items);
    $builder->render();
    return true;
  }

  public function actionCreate()
  {
    $builder = new AdminBuilder( new Product() );
    $builder->create();
    return true;
  }

  public function actionEdit( $id )
  {
    $builder = new AdminBuilder( new Product() );
    $builder->edit( $id );
    return true;
  }

  public function actionActivate()
  {
    $builder = new AdminBuilder( new Product() );
    $builder->active();
    return true;
  }

  public function actionSortable()
  {
    $builder = new AdminBuilder( new Product() );
    $builder->sort();
    return true;
  }

  public function actionDelete()
  {
    $builder = new AdminBuilder( new Product() );
    $builder->delete();
    return true;
  }

  public function actionModal()
  {
    $builder = new AdminBuilder( new Product() );
    $builder->modal();
    return true;
  }

}
?>