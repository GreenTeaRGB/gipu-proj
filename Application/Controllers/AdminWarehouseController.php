<?php
	namespace Application\Controllers;
	use \Application\Classes\AdminBuilder;
	use \Application\Models\Warehouse;
	class AdminWarehouseController extends \Application\Classes\AdminBase
	{

		public function actionIndex($page = 1)
		{
			$builder = new AdminBuilder(new Warehouse(), [] );
			$builder->index($page);
			return true;
		}

		public function actionCreate()
		{
			$builder = new AdminBuilder(new Warehouse());
			$builder->create();
			return true;
		}

		public function actionEdit($id)
		{
			$builder = new AdminBuilder(new Warehouse());
			$builder->edit($id);
			return true;
		}

		public function actionActivate(){
			$builder = new AdminBuilder(new Warehouse());
			$builder->active();
			return true;
		}

		public function actionSortable(){
			$builder = new AdminBuilder(new Warehouse());
			$builder->sort();
			return true;
		}
		
		public function actionDelete(){
			$builder = new AdminBuilder(new Warehouse());
			$builder->delete();
			return true;
		}

		public function actionModal(){
			$builder = new AdminBuilder(new Warehouse());
			$builder->modal();
			return true;
		}


	}
?>