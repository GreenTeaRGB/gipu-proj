<?php

namespace Application\Controllers;

use \Application\Classes\AdminBuilder;
use \Application\Models\Courier;
use \Application\Models\Order;
use \Application\Models\User;
use function date;
use function intval;
use function strtotime;

class AdminAppointController extends \Application\Classes\AdminBase
{

  public function actionChange( $order_id, $page = 1 )
  {
    $builder = new AdminBuilder( new class ( $order_id ) extends Courier
    {
      private $id;

      public function __construct( $order_id )
      {
        parent::__construct();
        $this->id = $order_id;
      }

      public function getFields()
      {
        $fields                = parent::getFields();
        $fields['order_today'] = [
          'type'    => 'string',
          'visible' => true,
          'edited'  => false,
          'label'   => 'Заказы на сегодня',
          'sort'    => 800
        ];
        $fields['order_now']   = [
          'type'    => 'string',
          'visible' => true,
          'edited'  => false,
          'label'   => 'Текущий заказ',
          'sort'    => 750
        ];
        $fields['link']        = [
          'type'    => 'string',
          'visible' => true,
          'label'   => 'Подробнее',
          'sort'    => 990,
        ];
        $fields['button']      = [
          'type'    => 'button',
          'visible' => true,
          'edited'  => false,
          'label'   => 'Назначить',
          'sort'    => 1000,
          'url'     => '/admin/appoint/apply/' . $this->id . '/'
        ];
        return $fields;
      }
    }, [], [ 'hide' => ['delete', 'view', 'edit' ]  ] );
    $builder->merge( new class extends User
    {
      public function getFields()
      {
        $fields        = parent::getFields();
        $visibleFields = [ 'name', 'phone' ];
        foreach ( $fields as $field => $settings ) {
          if ( !in_array( $field, $visibleFields ) ) {
            $fields[$field]['visible'] = false;
          } else {
            $fields[$field]['visible'] = true;
          }
        }
        $fields['phone']['sort'] = 100;
        return $fields;
      }
    } );
    $now = date( 'd-m-Y' );
    $builder->setView( 'index' );
    $builder->index( $page );
    $items     = $builder->getItems();
    $courID    = array_column( $items, 'user_id' );
    $orders    = Order::findSort( [
      'courier_id'     => $courID,
      '>=delivery_date' => self::strtotime( $now ),
    ], [ 'delivery_date' => 'asc' ] );
    $orderTime = [];
    foreach ( $orders as $order ) {
      $strtotime = strtotime( $order['delivery_date'] );
      $date      = date( 'H:i', $strtotime );

      $orderTime[$order['courier_id']]['ids'][]   = $date . ' id' . $order['id'];
      $orderTime[$order['courier_id']]['times'][] = $strtotime;
    }
    $now = self::strtotime( $now );
    foreach ( $items as $id => $item ) {
      if ( isset( $orderTime[$item['user_id']] ) ) {
        $nowOrderID = '';
        $tmp        = 0;
        $minDate    = 0;
        // подправить алгоритм
        foreach ( $orderTime[$item['user_id']]['times'] as $time ) {
          $tmp = $now - $time;
          if ( $minDate > $tmp ) {
            $minDate    = $tmp;
            $nowOrderID = 'id' . $order['id'];
          }
        }
        $items[$id]['order_today'] = implode( '<br>', $orderTime[$item['user_id']]['ids'] );
        $items[$id]['order_now']   = $nowOrderID;
      } else {
        $items[$id]['order_today'] = '';
        $items[$id]['order_now']   = '';
      }

      if ( $item['user_id'] != 0 ) {
        $items[$id]['link'] = '<a target="_blank" href="/admin/user/view/' . $item['user_id'] . '">Подробнее</a>';
      } else {
        $items[$id]['link'] = '';
      }
    }
    $builder->setItems( $items );
    $builder->render();
    return true;
  }

  public function actionApply( $orderID, $courierID )
  {
    $courier = Courier::findOne($courierID);
    $order = Order::findOne($orderID);
    if(!$order)
      $this->json(false, 'Заказ не найден');
    if(!$courier)
      $this->json(false, 'Курьер не найден');
    if($order['courier_id'] == $courier['user_id'])
      $this->json(false, 'Курьер уже назначен');
    $count_order = intval($courier['count_order'])+1;
    if ( Order::updateById( $orderID, [ 'courier_id' => $courier['user_id'], 'status_id' => 2] ) ) {
      Courier::updateById($courierID, ['count_order' => $count_order]);
      $this->json( true, "Курьер назначен", ['redirect' => '/admin/order'] );
    }
    else
      $this->json( false, 'Ошибка выполнения' );
  }

  public function actionIndex( $page = 1 )
  {
    $builder = new AdminBuilder( new Courier() );
    $builder->index( $page );
    return true;
  }

  public function actionCreate()
  {
    $builder = new AdminBuilder( new Courier() );
    $builder->create();
    return true;
  }

  public function actionEdit( $id )
  {
    $builder = new AdminBuilder( new Courier() );
    $builder->edit( $id );
    return true;
  }

  public function actionActivate()
  {
    $builder = new AdminBuilder( new Courier() );
    $builder->active();
  }

  public function actionSortable()
  {
    $builder = new AdminBuilder( new Courier() );
    $builder->sort();
  }

  public function actionDelete()
  {
    $builder = new AdminBuilder( new Courier() );
    $builder->delete();
  }


}

?>