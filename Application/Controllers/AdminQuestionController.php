<?php
	
	namespace Application\Controllers;
	
	use \Application\Classes\AdminBuilder;
	use \Application\Models\Question;
	
	class AdminQuestionController extends \Application\Classes\AdminBase
	{
		
		public function actionIndex($page = 1)
		{
			$builder = new AdminBuilder(new Question(), []);
			$builder->index($page);
			return true;
		}
		
		public function actionCreate()
		{
			$builder = new AdminBuilder(new Question());
			$builder->create();
			return true;
		}
		
		public function actionEdit($id)
		{
			$builder = new AdminBuilder(new Question());
			$builder->edit($id);
			return true;
		}
		
		public function actionActivate()
		{
			$builder = new AdminBuilder(new Question());
			$builder->active();
		}
		
		public function actionSortable()
		{
			$builder = new AdminBuilder(new Question());
			$builder->sort();
		}
		
		public function actionDelete()
		{
			$builder = new AdminBuilder(new Question());
			$builder->delete();
		}
		
		
	}
	
	?>