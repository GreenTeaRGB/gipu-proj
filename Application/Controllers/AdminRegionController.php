<?php
namespace Application\Controllers;
use \Application\Classes\AdminBuilder;
	use \Application\Models\Region;
class AdminRegionController extends \Application\Classes\AdminBase
{

	public function actionIndex($page = 1)
	{
		$builder = new AdminBuilder(new Region(), [] ,['activate'=>'active', 'hide'=>['edit', 'view']]);
		$builder->index($page);
		return true;
	}

	public function actionCreate()
	{
		$builder = new AdminBuilder(new Region());
		$builder->create();
		return true;
	}

	public function actionEdit($id)
	{
		$builder = new AdminBuilder(new Region());
		$builder->edit($id);
		return true;
	}
	
	public function actionActivate(){
		$builder = new AdminBuilder(new Region());
		$builder->active();
		 return true;
	}
	
	public function actionSortable(){
		$builder = new AdminBuilder(new Region());
		$builder->sort();
		 return true;
	}
	public function actionDelete(){
		$builder = new AdminBuilder(new Region());
		$builder->delete();
		 return true;
	}
		public function actionModal(){
    $builder = new AdminBuilder(new Region());
    $builder->modal();
    return true;
  }
	
}
?>