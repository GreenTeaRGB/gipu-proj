<?php

namespace Application\Controllers;

use \Application\Classes\AdminBuilder;
use Application\Helpers\DataHelper;
use Application\Helpers\UserHelper;
use Application\Models\Category;
use Application\Models\DeliveryStatus;
use \Application\Models\JuristicData;
use Application\Models\Order;
use Application\Models\OrderStatus;
use Application\Models\PayStatus;
use Application\Models\ReportTransaction;
use Application\Models\Shop;
use \Application\Models\User;
use function in_array;

/**
 * Class AdminUserController
 * @package Application\Controllers
 */
class AdminUserController extends \Application\Classes\AdminBase
{

  /**
   * @param int $page
   * @return bool
   */
  public function actionIndex( $page = 1 )
  {

    $builder = new AdminBuilder( new class extends Order
    {
      public function getFields()
      {
        $fields        = parent::getFields();
        $visibleFields = [ 'name', 'phone', 'email', ];
        foreach ( $fields as $field => $settings ) {
          if ( !in_array( $field, $visibleFields ) ) {
            $fields[$field]['visible'] = false;
            $fields[$field]['edited']  = false;
          } else {
            $fields[$field]['visible'] = true;
          }
        }

        return $fields;
      }
    }, [], [ 'hide' => [ 'delete', 'edit', 'create' ] ] );
//    $builder = new AdminBuilder( new class extends User
//    {
//      public function getFields()
//      {
//        $fields                   = parent::getFields();
//        $visibleFields            = [ 'name', 'login', 'position', 'password' ];
//        $fields['login']          = [
//          'type'         => 'string',
//          'visible'      => true,
//          'edited'       => false,
//          'label'        => 'Логин',
//          'visible_keys' => [ 'email', 'phone' ],
//          'sort'         => 2000
//        ];
//        $fields['password']['sort'] = 2100;
//        $fields['email']['label'] = 'Логин';
//        $fields['phone']['label'] = '';
//        $fields['name']['label'] = 'Имя пользователя';
//        foreach( $fields as $field => $settings ) {
//          if( !in_array( $field, $visibleFields ) )
//            $fields[$field]['visible'] = false;
//          else
//            $fields[$field]['visible'] = true;
//        }
//        return $fields;
//      }
//    }, [], ['hide' => ['view']] );
    $builder->index( $page );
    return true;
  }

  /**
   * @return bool
   */
  public function actionCreate()
  {
    $builder = new AdminBuilder( new User() );
//    $builder->merge( new JuristicData() );
    $builder->create();
    return true;
  }

  /**
   * @param $id
   * @return bool
   */
  public function actionEdit( $id )
  {
    $builder = new AdminBuilder( new User() );
//    $builder->merge( new JuristicData() );
    $builder->edit( $id );
    return true;
  }

  /**
   * @param int $page
   * @return bool
   */
  public function actionSupplier( $page = 1 )
  {
    $builder = new AdminBuilder( new Class extends User
    {
//			  private $juristic_data;
//			  public function __construct($juristic_data)
//        {
//          $this->juristic_data = $juristic_data;
//        }
      public function getFields()
      {
        $fields = parent::getFields();

        $fields['position']['visible'] = false;
        $fields['email']['visible']    = true;

        return $fields;
      }
    }, [], [ 'activate' => 'active', 'hide' => [ 'view' ] ] );
    $builder->merge( new JuristicData() );

//      echo '<pre>';
//      var_dump($builder);
//      die;
    $builder->setWhere( [ 'type' => DataHelper::DEFAULT_SUPPLIER_TYPE_ID ] );
    $builder->index( $page );

    return true;
  }

  /**
   * @return bool
   */
  public function actionCreateSupplier()
  {
    $_POST['type'] = DataHelper::DEFAULT_SUPPLIER_TYPE_ID;
    $builder       = new AdminBuilder( new User() );
//    $builder->merge( new JuristicData() );
    $builder->create();
    return true;
  }

  /**
   * @param $id
   * @return bool
   */
  public function actionEditSupplier( $id )
  {
    $builder = new AdminBuilder( new User() );
//    $builder->merge( new JuristicData() );
    $builder->edit( $id );
    return true;
  }

  /**
   * @param $id
   * @return bool
   */
  public function actionViewSupplier( $id )
  {
    $builder = new AdminBuilder( new User(), [], [] );
//    $builder->merge( new JuristicData() );
    $builder->view( $id );
    return true;
  }

  /**
   * @param int $page
   * @return bool
   */
  public function actionByer( $page = 1 )
  {
    $builder = new AdminBuilder( new Class extends User
    {
      public function getFields()
      {
        $fields                          = parent::getFields();
        $fields['last_buy']              = [
          'type'      => 'string',
          'visible'   => true,
          'edited'    => false,
          'label'     => 'Дата последней покупки',
          'no_filter' => true
        ];
        $fields['history_buy']           = [
          'type'      => 'string',
          'visible'   => true,
          'edited'    => false,
          'label'     => 'История покупок',
          'no_filter' => true
        ];
        $fields['name']['visible_keys']  = [ 'name', 'last_name' ];
        $fields['name']['label']         = "ФИО";
        $fields['position']['visible']   = false;
        $fields['email']['visible']      = false;
        $fields['phone']['visible_keys'] = [ 'phone', 'email' ];
        $fields['phone']['label']        = 'Номер телефона, почта';

        return $fields;
      }
    }, [], [ 'activate' => 'active', 'hide' => [ 'view', 'edit' ] ] );
    $builder->setWhere( [ 'type' => DataHelper::DEFAULT_BUYER_TYPE_ID ] );
    $builder->setView( 'index' );
    $builder->index( $page );
    $items      = $builder->getItems();
    $userIDs    = array_column( $items, 'id' );
    $orderModel = new Order();
    $orders     = $orderModel->where( [ 'buyer_id' => $userIDs ] )->order( [ 'created_at' => 'desc' ] )
                             ->group( " GROUP BY buyer_id" )->fetchAll();
    $sortOrders = [];
    foreach ( $orders as $order ) {
      $sortOrders[$order['buyer_id']] = $order['created_at'];
    }
    foreach ( $items as $index => $item ) {
      $items[$index]['history_buy'] = '<a href="/admin/user/buyer/historybuy/' . $index . '">История покупок</a>';
      if ( isset( $sortOrders[$item['id']] ) )
        $items[$index]['last_buy'] = $sortOrders[$item['id']];
      else
        $items[$index]['last_buy'] = 'Не покупал';
    }
    $builder->setItems( $items );
    $builder->render();
    return true;
  }

  /**
   * @return bool
   */
  public function actionCreateByer()
  {
    $_POST['type'] = DataHelper::DEFAULT_BUYER_TYPE_ID;
    $builder       = new AdminBuilder( new User() );
//    $builder->merge( new JuristicData() );
    $builder->create();
    return true;
  }

  /**
   * @param $id
   * @return bool
   */
  public function actionEditByer( $id )
  {
    $builder = new AdminBuilder( new User() );
    $builder->setDetailTables( [
      'name'    => 'История покупок',
      'id'      => 'orders',
      'content' => $this->render( 'orders', [
        'orders'         => Order::find( [ 'buyer_id' => $id ] ),
        'status'         => OrderStatus::find(),
        'deliveryStatus' => DeliveryStatus::find(),
        'payments'       => PayStatus::find()
      ], true )
    ] );
//    $builder->merge( new JuristicData() );
    $builder->edit( $id );
    return true;
  }

  /**
   * @param $id
   * @return bool
   */
  public function actionViewByer( $id )
  {
    $builder = new AdminBuilder( new User() );
    $builder->setDetailTables( [
      'name'    => 'История покупок',
      'id'      => 'orders',
      'content' => $this->render( 'orders', [
        'orders'         => Order::find( [ 'buyer_id' => $id ] ),
        'status'         => OrderStatus::find(),
        'deliveryStatus' => DeliveryStatus::find(),
        'payments'       => PayStatus::find()
      ], true )
    ] );
//    $builder->merge( new JuristicData() );
    $builder->view( $id );
    return true;
  }

  public function actionHistoryBuy( $id )
  {
    $builder = new AdminBuilder( new User() );
    $builder->setDetailTables( [
      'name'    => 'История покупок',
      'id'      => 'orders',
      'content' => $this->render( 'orders', [
        'orders'         => Order::find( [ 'buyer_id' => $id ] ),
        'status'         => OrderStatus::find(),
        'deliveryStatus' => DeliveryStatus::find(),
        'payments'       => PayStatus::find()
      ], true )
    ] );
//    $builder->merge( new JuristicData() );
    $builder->renderTabs( $id );
    return true;
//    $builder->render();
  }

  /**
   * @param int $page
   * @return bool
   */
  public function actionSeller( $page = 1 )
  {
    $shops   = \Application\Models\Shop::find();
    $builder = new AdminBuilder( new class( $shops ) extends User
    {
      private $shops;

      public function __construct( $shops )
      {
        parent::__construct();
        $this->shops = $shops;
      }

      public function getFields()
      {
        $fields        = parent::getFields();
        $visibleFields = [ 'name', 'registration_date', 'balance', 'referral_level' ];
        foreach ( $fields as $field => $options ) {
          if ( $field == 'name' ) {
            $fields[$field]['visible_keys'] = [ 'name', 'last_name' ];
          }

          if ( in_array( $field, $visibleFields ) ) {
            $fields[$field]['visible'] = true;
          } else {
            $fields[$field]['visible'] = false;
          }
        }
        $fields['shop_id'] = [
          'type'         => 'mselect',
          'visible'      => true,
          'values'       => $this->shops,
          'multiple'     => true,
          'view'         => true,
          'id_key'       => 'id',
          'name_key'     => 'date_active_start',
          'visible_keys' => [ 'id', 'date_active_start' ],
          'label'        => 'Сайты ID',
          'sort'         => 300
        ];

//        $fields['link']                = [
//          'type'      => 'link',
//          'visible'   => true,
//          'label'     => 'Подробнее',
//          'no_filter' => true,
//          'url'       => '/admin/seller/view/',
//          'sort'      => 9000
//        ];
        $fields['position']['visible'] = false;

        $fields['id']['label']       = 'ID сайта | ';
        $fields['active']['visible'] = true;
        $fields['active']['sort']    = 10000;
        $fields['date_active_start'] = [
          'label'   => 'Дата начала кативности сайта',
          'visible' => false
        ];
        return $fields;
      }
    }, [], [ 'activate' => 'active', 'hide' => [ 'delete', 'edit', 'create' ] ] );
    foreach ( $shops as $shop ) {
      $sortShops[$shop['seller_id']][] = $shop['id'];
    }
    $builder->setWhere( [ 'type' => DataHelper::DEFAULT_SELLER_TYPE_ID ] );
    $builder->setView( 'index' );
    $builder->index( $page );
    $items = $builder->getItems();
    foreach ( $items as $index => $item ) {
      $items[$index]['link'] = '<a href="/admin/seller/view/' . $index . '">Подробнее</a>';
      if ( !isset( $sortShops[$item['id']] ) ) {
        $items[$index]['shop_id'] = [];
        continue;
      } else {
        $items[$index]['shop_id'] = $sortShops[$item['id']];
      }
    }
    $builder->setItems( $items );
    $builder->render();
    return true;
  }

  /**
   * @return bool
   */
  public function actionCreateSeller()
  {
    $_POST['type'] = DataHelper::DEFAULT_SELLER_TYPE_ID;
    $builder       = new AdminBuilder( new User() );
//    $builder->merge( new JuristicData() );
    $builder->create();
    return true;
  }

  /**
   * @param $id
   * @return bool
   */
  public function actionEditSeller( $id )
  {
    $builder = new AdminBuilder( new User() );
    $builder->setDetailTables( [
      'name'    => 'Сайты',
      'id'      => 'sites',
      'content' => $this->render( 'sites', [
        'sites'      => Shop::find( [ 'seller_id' => $id ] ),
        'categories' => Category::getFullList()
      ], true )
    ] );
    $reportTransaction = new ReportTransaction();
    $builder->setDetailTables( [
      'name'    => 'История финансов',
      'id'      => 'fin',
      'content' => $this->render( 'transactions', [
        'transactions' => $reportTransaction->where( [ 'user_from' => $id ] )->orWhere( [ 'user_to' => $id ] )
                                            ->order( [ 'id' => 'desc' ] )->fetchAll(),
        'user'         => UserHelper::getUser()
      ], true )
    ] );
    $builder->setDetailTables( [
      'name'    => 'Партнерская сеть',
      'id'      => 'sites',
      'content' => $this->render( 'partners', [
      ], true )
    ] );
//    $builder->merge( new JuristicData() );
    $builder->edit( $id );
    return true;
  }

  /**
   * @param $id
   * @return bool
   */
  public function actionViewSeller( $id )
  {
    $builder = new AdminBuilder( new User() );
    $builder->setDetailTables( [
      'name'    => 'Сайты',
      'id'      => 'sites',
      'content' => $this->render( 'sites', [
        'sites'      => Shop::find( [ 'seller_id' => $id ] ),
        'categories' => Category::getFullList()
      ], true )
    ] );
    $reportTransaction = new ReportTransaction();
    $builder->setDetailTables( [
      'name'    => 'История финансов',
      'id'      => 'fin',
      'content' => $this->render( 'transactions', [
        'transactions' => $reportTransaction->where( [ 'user_from' => $id ] )->orWhere( [ 'user_to' => $id ] )
                                            ->order( [ 'id' => 'desc' ] )->fetchAll(),
        'user'         => UserHelper::getUser()
      ], true )
    ] );
    $builder->setDetailTables( [
      'name'    => 'Партнерская сеть',
      'id'      => 'sites',
      'content' => $this->render( 'partners', [
      ], true )
    ] );
//    $builder->merge( new JuristicData() );
    $builder->view( $id );
    return true;
  }

  /**
   * @return bool
   */
  public function actionActivate()
  {
    $builder = new AdminBuilder( new User() );
    $builder->active();
    return true;
  }

  /**
   * @return bool
   */
  public function actionDeleteFile()
  {
    $builder = new AdminBuilder( new User() );
    $builder->deleteFile();
    return true;
  }

  /**
   * @return bool
   */
  public function actionSortable()
  {
    $builder = new AdminBuilder( new User() );
    $builder->sort();
    return true;
  }

  /**
   * @return bool
   */
  public function actionDelete()
  {
    $builder = new AdminBuilder( new User() );
    $builder->delete();
    return true;
  }

  /**
   * @return bool
   */
  public function actionGetMergeData()
  {
    $builder = new AdminBuilder( new User() );
//    $builder->merge( new JuristicData() );
    $builder->getMergeData();
    return true;
  }
}

?>