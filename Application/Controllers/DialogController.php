<?php

namespace Application\Controllers;

use Application\Classes\Session;
use Application\Helpers\AppHelper;
use Application\Helpers\DataHelper;
use Application\Helpers\UserHelper;
use Application\Models\Dialog;
use Application\Models\Message;
use Application\Models\User;

class DialogController extends \Application\Classes\ControllerBase
{

  private $user = false;

  public function actionIndex()
  {
    return true;
  }

  public function __construct()
  {
    $this->user = UserHelper::getUser();
    if ( !$this->user )
      $this->json( false );
    if ( UserHelper::isAdmin() ) {
      $this->user['id']   = DataHelper::SUPPORT_ID;
      $this->user['type'] = DataHelper::DEFAULT_SELLER_TYPE_ID;
    }
  }

  public function actionDialogs()
  {
    if ( $this->user && $this->user['type'] != DataHelper::DEFAULT_BUYER_TYPE_ID ) {

      $verified = UserHelper::userIsVerified();

      if ( $verified ) {

        Session::remove( 'last_msg_id' );
        $this->render( '/chat', [
          'user' => $this->user
        ] );
      } else {
        AppHelper::setMessage( 'error', 'Чтобы получить доступ, пройдите верификацию' );

        $this->redirect( '/user/dashboard' );
      }

    } else {
      AppHelper::setMessage( 'error', 'Чтобы получить доступ, войдите в аккаунт' );

      $this->redirect( '/user/login' );
    }

    return true;
  }

  public function actionSearchUser()
  {
    $request  = $_POST;
    $response = [
      'success' => false,
      'message' => 'Ошибка поиска пользователя'
    ];


    if ( $this->user && $this->user['type'] != DataHelper::DEFAULT_BUYER_TYPE_ID ) {
      if ( isset( $request['user_id'] ) && $request['user_id'] ) {
        if ( $request['user_id'] != $this->user['id'] ) {
          $searchedUser = User::findOne( [ 'id' => $request['user_id'], 'type' => $this->user['type'] ] );
          if ( $searchedUser ) {
            $userData = [
              'id'      => $searchedUser['id'],
              'name'    => $searchedUser['name'],
              'avatar'  => $searchedUser['avatar'],
              'message' => ''
            ];
            if ( !Dialog::findOne( [ 'sender_id' => $this->user['id'], 'receiver_id' => $searchedUser['id'] ] ) )
              Dialog::create( [ 'sender_id' => $this->user['id'], 'receiver_id' => $searchedUser['id'] ] );
            $response = [
              'success'       => true,
              'searched_user' => $userData,
              'message'       => ''
            ];
          } else {
            $response = [
              'success' => false,
              'message' => 'Пользователь с таким ID не найден или относится к другой категории'
            ];
          }
        } else {
          $response = [
            'success' => false,
            'message' => 'Нельзя отправлять сообщения самому себе'
          ];
        }
      }
    }
    $this->json( $response['success'], $response['message'], $response );
  }

  public
  function actionSendMessage()
  {
    $request  = $_POST;
    $response = [
      'success' => false,
      'message' => 'Ошибка отправки сообщения'
    ];

    if ( $this->user && $this->user['type'] != DataHelper::DEFAULT_BUYER_TYPE_ID ) {
      if ( $request['receiver_id'] && $request['message'] ) {
        $messageModel = new Message();
        $messageId    = '';
        $dialogId     = '';
        if ( isset( $request['dialog_id'] ) && $request['dialog_id'] ) {
          $messageData = [
            'dialog_id' => $request['dialog_id'],
            'user_id'   => $this->user['id'],
            'text'      => $request['message']
          ];

          $dialogId = $request['dialog_id'];

          $messageId = $messageModel->setFields( $messageData )->insert();
        } else {
          $model = new Dialog();

          $dialog = $model->where( [ 'sender_id' => $this->user['id'], 'receiver_id' => $request['receiver_id'] ] )
                          ->orWhere( [ 'sender_id' => $request['receiver_id'], 'receiver_id' => $this->user['id'] ] )
                          ->fetchOne();

          if ( !$dialog ) {
            $dialogData = [
              'sender_id'   => $this->user['id'],
              'receiver_id' => $request['receiver_id']
            ];

            $dialogId = $model->setFields( $dialogData )->insert();
          } else {
            $dialogId = $dialog['id'];
          }

          if ( $dialogId ) {
            $messageData = [
              'dialog_id' => $dialogId,
              'user_id'   => $this->user['id'],
              'text'      => $request['message'],
            ];

            $messageId = $messageModel->setFields( $messageData )->insert();
          }
        }

        if ( $messageId ) {

          $response = [
            'success'    => true,
            'dialog_id'  => $dialogId,
            'message_id' => $messageId
          ];
        }
      } else {
        $response = [
          'success' => false,
          'message' => 'Ошибка, сообщение не должно быть пустым'
        ];
      }
    }
    $this->json( $response['success'], '', $response );
  }

  public
  function actionLoadDialogs()
  {
    $request  = $_POST;
    $response = [
      'success' => false,
      'message' => 'Ошибка загрузки диалогов'
    ];

    if ( $this->user && $this->user['type'] != DataHelper::DEFAULT_BUYER_TYPE_ID ) {
//				if ( isset($request['is_ajax']) && $request['is_ajax'] ) {
      $dialogModel = new Dialog();

      $dialogsWithMessages = $dialogModel->where( [ 'sender_id' => $this->user['id'] ] )
                                         ->orWhere( [ 'receiver_id' => $this->user['id'] ] )->fetchAll();

//					$messagesForUpdateExists = false;
//					foreach ($dialogsWithMessages as $dialogsWithMessage) {
//						if ( isset($dialogsWithMessage['messages']) && count($dialogsWithMessage['messages']) > 0 ) {
//							$messagesForUpdateExists = true;
//						}
//					}
      $dialogs = [];
      foreach ( $dialogsWithMessages as $dialogsWithMessage ) {
        if ( strtotime( $dialogsWithMessage['companion']['last_activity'] ) > time() - 60 ) {
          $dialogsWithMessage['companion']['online'] = true;
        } else {
          $dialogsWithMessage['companion']['online'] = false;
        }
        $dialogs[$dialogsWithMessage['companion']['id']] = $dialogsWithMessage;
      }
//					if ( $messagesForUpdateExists ) {
      $response = [
        'success' => true,
        'dialogs' => $dialogs
      ];
//					} else {
//						$response = [
//							'success' => true,
//						];
//					}
//				}
    }
    $this->json( $response['success'], '', $response );
  }

  public
  function actionReadMessage()
  {
    $request  = $_POST;
    $response = [
      'success' => false,
      'message' => 'Ошибка чтения сообщения'
    ];
    if ( $this->user && isset( $request['message_ids'] ) && $request['message_ids'] ) {
      if ( count( $request['message_ids'] ) <= 0 ) {
        $this->json( false, 'Нет новых сообщений' );
      }
      $messageModel = new Message();
      $done         = $messageModel->readMessages( $request['message_ids'] );

      if ( $done ) {
        $response = [
          'success' => true
        ];
      }
    }

    echo json_encode( $response );
    return true;
  }
}