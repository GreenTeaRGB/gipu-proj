<?php

namespace Application\Controllers;

use \Application\Classes\AdminBuilder;
use Application\Models\Rate;
use Application\Models\Shop;
use Application\Helpers\DataHelper;
use Application\Models\User;
use function array_column;
use function array_filter;
use function implode;
use function is_array;
use function rsort;
use function sort;
use function var_dump;

class AdminStatementAgentController extends \Application\Classes\AdminBase
{
  public function actionIndex( $page = 1 )
  {
    $builder = new AdminBuilder( new class extends User
    {
      public function getFields()
      {
        $fields        = parent::getFields();
        $visibleFields = [ 'name', 'id' ];
        foreach ( $fields as $field => $settings ) {
          if ( !in_array( $field, $visibleFields ) ) {
            $fields[$field]['visible'] = false;
          } else {
            $fields[$field]['visible'] = true;
          }
        }
        $fields['date_pay'] = [
          'type'    => 'string',
          'visible' => true,
          'label'   => 'Дата оплаты',
          'sort'    => 500
        ];
        $fields['sum_pay']  = [
          'type'    => 'string',
          'visible' => true,
          'label'   => 'Сумма оплаты',
          'sort'    => 500
        ];

        $fields['sum_income'] = [
          'type'    => 'string',
          'visible' => true,
          'label'   => 'Сумма дохода с агента',
          'sort'    => 500
        ];

        $fields['agent_id_first_level'] = [
          'type'    => 'string',
          'visible' => true,
          'label'   => 'id агента глубины I, его коммисия, процент за звание',
          'sort'    => 1000
        ];

        $fields['agent_id_second_level'] = [
          'type'    => 'string',
          'visible' => true,
          'label'   => 'id агента глубины II, его коммисия, процент за звание',
          'sort'    => 1100
        ];

        return $fields;
      }
    }, [], [ 'hide' => ['create', 'delete', 'view', 'edit'] ] );
    $builder->setView( 'index' );
    $builder->setWhere( [ 'type' => 4 ] );
    $builder->index( $page );
    $items            = $builder->getItems();
    $userIds          = array_column( $items, 'id' );
    $referralIds      = array_column( $items, 'referral_id' );
    $shops            = Shop::find( [ 'seller_id' => $userIds ] );
    $rates            = Rate::find();
    $agentsFirstLevel = User::find( [ 'id' => $referralIds ] );
    $sortFirstLevel   = [];

    foreach ( $agentsFirstLevel as $first ) {
      $sortFirstLevel[$first['id']] = $first;
    }
//    $agentsSecondLevel = User::find(['id' => $referralIds]);
    $sortSecondLevel = [];
    $secondLevel     = User::find( [ 'id' => array_column( $sortFirstLevel, 'referral_id' ) ] );
    foreach ( $secondLevel as $second ) {
      $sortSecondLevel[$second['id']] = $second;
    }
    $sortShops = [];
    foreach ( $shops as $shop ) {
      $sortShops[$shop['seller_id']] = $shop;
    }
    foreach ( $items as $key => $item ) {
      $firstLevelVal = '';
      if ( isset( $sortFirstLevel[$item['referral_id']] ) ) {
        $firstLevelVal = $item['referral_id'] . ', ' . DataHelper::DEPTH_ONE_PERCENT . ' ' . DataHelper::getPercentByRank( $sortFirstLevel[$item['referral_id']]['referral_level'] );
      } else {
        $firstLevelVal = 'Нет';
      }
      if ( isset( $sortFirstLevel[$item['referral_id']] ) && isset( $sortSecondLevel[$sortFirstLevel[$item['referral_id']]['referral_id']] ) ) {
        $secondLevelVal = $sortFirstLevel[$item['referral_id']]['referral_id']
          . ' ' . DataHelper::DEPTH_TWO_PERCENT
          . ' ' . DataHelper::getPercentByRank( $sortSecondLevel[$sortFirstLevel[$item['referral_id']]['referral_id']]['referral_level'] );
      } else {
        $secondLevelVal = 'Нет';
      }

      $items[$key]['date_pay']              = isset( $sortShops[$key]['date_active_start'] )
        ? $sortShops[$key]['date_active_start'] : 'Отсутствует';
      $items[$key]['sum_pay']               = isset( $sortShops[$key]['rate'] ) && isset( $rates[$sortShops[$key]['rate']]['price'] )
        ? $rates[$sortShops[$key]['rate']]['price'] : 0;
      $items[$key]['sum_income']            = 0;
      $items[$key]['agent_id_first_level']  = $firstLevelVal;
      $items[$key]['agent_id_second_level'] = $secondLevelVal;
    }
    $builder->setItems( $items );
    $builder->render();
    return true;
  }


}