<?php

  namespace Application\Controllers;

  use \Application\Classes\AdminBuilder;
  use \Application\Models\Discount;

  class AdminDiscountController extends \Application\Classes\AdminBase
  {

    public function actionIndex( $page = 1 )
    {
      $builder = new AdminBuilder( new Discount(), [] );
      $builder->index( $page );
      return true;
    }

    public function actionCreate( $type = false, $id = 0 )
    {
      $builder = new AdminBuilder( new Discount() );
      $builder->create( $id, $type );
      return true;
    }

    public function actionEdit( $id )
    {
      $builder = new AdminBuilder( new Discount() );
      $builder->edit( $id );
      return true;
    }

    public function actionActivate()
    {
      $builder = new AdminBuilder( new Discount() );
      $builder->active();
      return true;
    }

    public function actionSortable()
    {
      $builder = new AdminBuilder( new Discount() );
      $builder->sort();
      return true;
    }

    public function actionDelete()
    {
      $builder = new AdminBuilder( new Discount() );
      $builder->delete();
      return true;
    }

    public function actionDeleteFile()
    {
      $builder = new AdminBuilder( new Discount() );
      $builder->deleteFile();
      return true;
    }
    public function actionModal(){
      $builder = new AdminBuilder(new Discount());
      $builder->modal();
      return true;
    }


  }

  ?>