<?php

  namespace Application\Controllers;

  use Application\Classes\Pagination;
  use Application\Classes\Session;
  use Application\Helpers\DataHelper;
  use Application\Helpers\UserHelper;
  use Application\Models\Category;
  use Application\Models\Product;
  use Application\Models\Properties;
  use Application\Models\Review;

  /**
   * Class CategoryController
   * @package Application\Controllers
   */
  class CategoryController extends \Application\Classes\ControllerBase
  {

    /**
     *
     */
    CONST BREADCRUMB_CATALOG = [ [ 'label' => 'Каталог', 'url' => '/catalog' ] ];
    /**
     *
     */
    CONST BREADCRUMB_CATALOG_URL = '/category/';

    /**
     * @return bool
     */
    public function actionIndex()
    {
      return true;
    }

    /**
     * @return bool
     */
    public function actionList()
    {
    	$maxCountCategories = 5;
      $categories = Category::getFullList();
      
      foreach( $categories as $cat ){
      	if($cat['parent_id'] == 0)
            $parent_categories[] = $cat;
        }
        
      $parent_id = array_rand($parent_categories, $maxCountCategories);
      
      foreach ($categories as $cat){
        if(in_array($cat['parent_id'], $parent_id))
        {
      	    $child_categories[] = $cat;
        }
      }
      $categories = array_merge($parent_categories, $child_categories);
      $this->render(
          '/list',
          [
//              'categories' => $parent_categories
		        'categories' => $categories
          ]
      );

      return true;
    }

    /**
     * @param $alias
     * @return bool
     */
    public function actionShow( $alias, $page = 1 )
    {
      $categories        = Category::getFullList();
      $activeCategory    = false;
      $parent_categories = [];
      foreach( $categories as $cat ) {
        $parent_categories[$cat['parent_id']][$cat['id']] = $cat;
        if( $cat['alias'] === $alias ) {
          $activeCategory = $cat;
        }
      }
      if( $activeCategory == false )
        $this->error404();
      list( $activeCategory, $breadcrumb ) = $this->makeParentCategories( $activeCategory, $categories, self::BREADCRUMB_CATALOG );
      list( $activeCategory, $whereID ) = $this->makeChildCategories( $activeCategory, $categories, $parent_categories, [] );
      if( !isset( $activeCategory['child'] ) ) {
        list( $activeCategory, $null ) = $this->makeChildCategories( $activeCategory['parent'], $categories, $parent_categories, [] );
      }
      if( $_SERVER['REQUEST_URI'] == '/category/'.$activeCategory['alias'] ) {
        $countChildCategories = count( $activeCategory['child'] );
        if( $countChildCategories > 4 )
          $countChildCategories = 4;
        $indexPreviewCategories = array_rand( $activeCategory['child'], $countChildCategories );
        $indexPreviewCategoriesForProduct = array_rand( $activeCategory['child'], $countChildCategories );
	      $categoriesIdForProduct  = [];
	      foreach( $indexPreviewCategoriesForProduct as $indexPreviewCategoryForProduct ) {
		      $categoriesIdForProduct[] = $activeCategory['child'][$indexPreviewCategoryForProduct]['id'];
		      if(isset($activeCategory['child'][$indexPreviewCategoryForProduct]['child'])){
			      $merge = array_column($activeCategory['child'][$indexPreviewCategoryForProduct]['child'], 'id');
			      $mergeCategories[$activeCategory['child'][$indexPreviewCategoryForProduct]['id']] = $merge;
			      $categoriesIdForProduct[] = $merge;
		      }
	      }
        foreach( $indexPreviewCategories as $indexPreviewCategory ) {
          $categories_id[] = $activeCategory['child'][$indexPreviewCategory]['id'];
        }
       
        $products = Product::findByLimitCategories( $categoriesIdForProduct );

        $sortProducts = [];
        foreach( $products as $product ){
          $sortProducts[$product['category_id']][] = $product;
        }
        if(isset($mergeCategories) && count($mergeCategories) > 0){
          foreach( $mergeCategories as $rootCategoryID => $mergeCategoryID ) {
            foreach( $mergeCategoryID as $catID ) {
              if( isset($sortProducts[$catID])){
                if(!isset($sortProducts[$rootCategoryID])) $sortProducts[$rootCategoryID] = [];
                $sortProducts[$rootCategoryID] = array_merge($sortProducts[$rootCategoryID], $sortProducts[$catID]);
              }
            }
          }
        }
        $this->render(
            '/first_level', [
                              'category'    => $activeCategory,
                              'categories'  => $categories,
                              'breadcrumbs' => $breadcrumb,
                              'products'    => $sortProducts,
                              'mainVisibleCategoryID' => $categories_id,
                              'mainVisibleCategoryIDProduct' => array_filter($categoriesIdForProduct, function($val){
                                  return !is_array($val);
                              })
                          ]
        );

      } else {
        list( $count, $products ) = Product::findUseFilter(
            $whereID,
            $page
        );
        list( $min, $max ) = Product::getMinMaxByCategories( $whereID );
        $pagination        = new Pagination( $count, $page, Product::getDefaultCount(), 'page-' );
        $filter_properties = Properties::find(
            [
                'category_id' => $whereID,
                'active'      => 1,
                'type'        => 'L',
                'filter'      => 1,
            ]
        );

        $user = UserHelper::getUser();
        return $this->render(
            '/show',
            [
                'category'          => $activeCategory,
                'products'          => $products,
                'user'              => $user,
                'categories'        => $categories,
                'filter_properties' => $filter_properties,
                'min'               => $min,
                'max'               => $max,
                'pagination'        => $pagination,
                'breadcrumbs'       => $breadcrumb
            ]
        );
      }
    }

    /**
     * @param $alias
     * @return bool
     */
    public function actionProduct( $alias )
    {
//      $product = Product::findOne( [ 'alias' => $alias, 'status_id' => DataHelper::PRODUCT_STATUS_ALLOWED ] );
      $model = new Product();
      $sql = " SELECT * FROM `product` WHERE `alias` = '$alias' AND `status_id` = ".DataHelper::PRODUCT_STATUS_ALLOWED;
      $product = $model->sql($sql)->fetchOne();
      if( !$product )
        $this->error404();
      $product_browsing = Product::findLast( 10, [ 'category_id' => $product['category_id'] ] );
      Session::setViewedProduct( $product );
      $reviews    = Review::find( [ 'product_id' => $product['id'], 'active' => DataHelper::REVIEW_STATUS_ACTIVE ] );
      $categories = Category::getFullList();
      list( $null, $breadcrumbs ) = $this->makeParentCategories( $categories[$product['category_id']], $categories, self::BREADCRUMB_CATALOG );
      $user = UserHelper::getUser();
      return $this->render(
          '/product/show',
          [
              'product'          => $product,
              'reviews'          => $reviews,
              'user'             => $user,
              'breadcrumbs'      => $breadcrumbs,
              'product_browsing' => $product_browsing,
          ]
      );
    }

    /**
     * @return bool
     */
    public function actionSort()
    {
      if( isset( $_POST['sort'] ) ) {
        $sort        = $_POST['sort'];
        $sortSession = Session::getByKey( 'sort' );
        if( !in_array( $sort, [ 'rate', 'price', 'popular' ] ) ) {
          Session::remove( 'sort' );
        }
        if( !$sortSession || $sortSession['order'] != $sort ) {
          Session::store( [ 'sort' => [ 'order' => $sort, 'by' => 0 ] ] );
        } else {
          $sortSession['by'] = !$sortSession['by'];
          Session::store( [ 'sort' => $sortSession ] );
        }
      }
      if( isset( $_POST['count'] ) ) {
        $count = $_POST['count'];
        if( !in_array( $count, [ 30, 60, 90 ] ) ) {
          $count = 30;
        }
        Session::store( [ 'count' => $count ] );
      }
      echo json_encode( [] );
      return true;
    }

    /**
     * @param int $page
     * @return bool
     */
    public function actionSearch( $page = 1 )
    {
      if( isset( $_GET['q'] ) && $_GET['q'] != '' && isset( $_GET['category'] ) ) {
        $model    = new Product( true );
        $category = (int) $_GET['category'];
        if( $category > 0 )
          $where['category_id'] = $category;
        $where['status_id'] = DataHelper::PRODUCT_STATUS_ALLOWED;
        $model->where( $where );
        $model->like( [ 'name' => $_GET['q'] ] );
        $count = $model->count();
        if( $count == 0 ) {
          $model->where( $where );
          $recomended = $model->order( [ 'created_at' => 'desc' ] )->limit( 0, 12 )->fetchAll();
          $products   = [];
          $pagination = '';
        } else {
          $recomended = [];
          $start      = ( $page - 1 ) * Product::getDefaultCount();
          $end        = Product::getDefaultCount();
          $model->where( $where );
          $model->like( [ 'name' => $_GET['q'] ] );
          $products   = $model->limit( $start, $end )->fetchAll();
          $pagin      = new Pagination( $count, $page, Product::getDefaultCount(), 'page-' );
          $pagination = $pagin->get();
        }

      } else {
        $products   = [];
        $pagination = '';
        $recomended = [];
      }
      return $this->render(
          'search', [
                      'products'   => $products,
                      'pagination' => $pagination,
                      'recomended' => $recomended
                  ]
      );
    }

    //    private function makeCategoriesLevel( $activeCategory, $categories, $parent_categories )
    //    {
    //      $parent = $this->makeParentCategories( $activeCategory, $categories );
    //      $child = $this->makeChildCategories($activeCategory, $categories, $parent_categories);
    //      return [ $parent, $child ];
    //      //      $activeCategory['child'] = isset($parent_categories[$activeCategory['id']]) ? $parent_categories[$activeCategory['id']] : [];
    //      //      $mainCategories = array_shift($parent_categories);
    //      //      foreach( $parent_categories as $parent_category ) {
    //      //        if($parent_categories)
    //      //      }
    //    }

    /**
     * @param $activeCategory
     * @param $categories
     * @param $parent_categories
     * @param $ids
     * @return array
     */
    private function makeChildCategories( $activeCategory, $categories, $parent_categories, $ids )
    {
      if( !isset( $parent_categories[$activeCategory['id']] ) ) {
        return [ $activeCategory, $activeCategory['id'] ];
      }
      foreach( $parent_categories[$activeCategory['id']] as $parent ) {
        $ids[] = $parent['id'];
        if( isset( $parent_categories[$parent['id']] ) ) {
          list( $data, $id ) = $this->makeChildCategories( $parent, $categories, $parent_categories, $ids );
          $activeCategory['child'][] = $data;
          $ids                       = array_merge( $id, $ids );
        } else {
          $activeCategory['child'][] = $parent;
        }
      }
      return [ $activeCategory, $ids ];
    }

    /**
     * @param $activeCategory
     * @param $categories
     * @param $breadcrumb
     * @return array
     */
    private function makeParentCategories( $activeCategory, $categories, $breadcrumb )
    {
      if( $activeCategory['parent_id'] == 0 ) {
        array_push(
            $breadcrumb, [
                           'label' => $activeCategory['name'],
                           'url'   => self::BREADCRUMB_CATALOG_URL.$activeCategory['alias']
                       ]
        );
        return [ $activeCategory, $breadcrumb ];
      }
      list( $activeCategory['parent'], $breadcrumbs ) = $this->makeParentCategories( $categories[$activeCategory['parent_id']], $categories, $breadcrumb );
      $breadcrumbs[] = [
          'label' => $activeCategory['name'],
          'url'   => self::BREADCRUMB_CATALOG_URL.$activeCategory['alias']
      ];
      return [ $activeCategory, $breadcrumbs ];
    }

  }