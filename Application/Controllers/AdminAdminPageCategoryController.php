<?php
namespace Application\Controllers;
use \Application\Classes\AdminBuilder;
	use \Application\Models\AdminPageCategory;
class AdminAdminPageCategoryController extends \Application\Classes\AdminBase
{

	public function actionIndex($page = 1)
	{
		$builder = new AdminBuilder(new AdminPageCategory(), [] ,['sortable'=>'sort']);
		$builder->index($page);
		return true;
	}

	public function actionCreate()
	{
		$builder = new AdminBuilder(new AdminPageCategory());
		$builder->create();
		return true;
	}

	public function actionEdit($id)
	{
		$builder = new AdminBuilder(new AdminPageCategory());
		$builder->edit($id);
		return true;
	}
	
	public function actionActivate(){
		$builder = new AdminBuilder(new AdminPageCategory());
		$builder->active();
		 return true;
	}
	
	public function actionSortable(){
		$builder = new AdminBuilder(new AdminPageCategory());
		$builder->sort();
		 return true;
	}
	public function actionDelete(){
		$builder = new AdminBuilder(new AdminPageCategory());
		$builder->delete();
		 return true;
	}
		public function actionModal(){
    $builder = new AdminBuilder(new AdminPageCategory());
    $builder->modal();
    return true;
  }
	
}
?>