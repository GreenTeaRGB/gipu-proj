<?php
	
	namespace Application\Controllers;

	use Application\Models\QuestionCategory;
	use \Application\Models\Question;
  use Application\Models\QuestionType;
  use \Application\Models\SupportQuestion;
	use \Application\Helpers\AppHelper;
	
	class QuestionController extends \Application\Classes\ControllerBase
	{
		public function actionIndex()
		{
			$questions = Question::find();
			$questionsTree = AppHelper::buildTree($questions);

			$questions_type = QuestionType::find();
			$questions_category = QuestionCategory::find();
			
			return $this->render(
				'index',
				[
					'questions' => $questionsTree,
					'questions_type' => $questions_type,
					'questions_category' => $questions_category,
				]
			);
		}
		
		public function actionAddQuestion()
		{
			$request = $_POST;
;
			foreach ($request as $key => $value) {
				$request[$key] = htmlspecialchars(strip_tags($value), true);
			}
			$type = 'error';
			$message = 'Ошибка сохранения вопроса';
			if ( isset($request['allow']) && ($request['allow'] == 1) && isset($request['email']) && isset($request['question_category']) && isset($request['question_type']) && isset($request['question']) ) {
				$model = new SupportQuestion();
				$questionId = $model->setFields($request)->insert();
				if (isset($questionId)) {
					$type = 'success';
					$message = 'Ваш вопрос принят, Вам ответят в ближайшее время';
				}
			} else {
				$message = 'Не заполнены обязательные поля';
			}
			
//			AppHelper::setMessage( $type, $message );
			
			echo json_encode($message);
			die();
//			$this->redirect( '/questions' );
		}
	}