<?php

namespace Application\Controllers;

use \Application\Classes\AdminBuilder;
use \Application\Models\QuestionCategory;

class AdminQuestionCategoryController extends \Application\Classes\AdminBase
{

  public function actionIndex( $page = 1 )
  {
    $builder = new AdminBuilder( new QuestionCategory(), [] );
    $builder->index( $page );
    return true;
  }

  public function actionCreate()
  {
    $builder = new AdminBuilder( new QuestionCategory() );
    $builder->create();
    return true;
  }

  public function actionEdit( $id )
  {
    $builder = new AdminBuilder( new QuestionCategory() );
    $builder->edit( $id );
    return true;
  }

  public function actionActivate()
  {
    $builder = new AdminBuilder( new QuestionCategory() );
    $builder->active();
    return true;
  }

  public function actionSortable()
  {
    $builder = new AdminBuilder( new QuestionCategory() );
    $builder->sort();
    return true;
  }

  public function actionDelete()
  {
    $builder = new AdminBuilder( new QuestionCategory() );
    $builder->delete();
    return true;
  }

  public function actionModal()
  {
    $builder = new AdminBuilder( new QuestionCategory() );
    $builder->modal();
    return true;
  }

}

?>