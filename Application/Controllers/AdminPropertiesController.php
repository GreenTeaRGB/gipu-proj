<?php
	
	namespace Application\Controllers;
	
	use \Application\Classes\AdminBuilder;
	use \Application\Models\Properties;
	use \Application\Models\PropertyGroup;
	use \Application\Models\PropertyEnum;
	use \Application\Models\PropertyValue;
	use \Application\Models\Category;

	class AdminPropertiesController extends \Application\Classes\AdminBase
	{
		
		public function actionIndex($page = 1)
		{
			$builder = new AdminBuilder(new Properties(), [], ['activate' => 'active', 'sortable' => 'sort']);
			$builder->index($page);
			return true;
		}
		
		public function actionCreate()
		{
//			$builder = new AdminBuilder(new Properties());
//			$builder->create();
//			return true;
			$error = false;

			if(isset($_POST['save'])){
				if($_POST['name'] == '') $error['name'] = 'name';
				if($_POST['code'] == '') $error['code'] = 'code';
				if(!$error){
					$code = Properties::findByCode($_POST['code']);
					if(!$code){

						$id = Properties::create($_POST);
						if($_POST['type'] == 'L'){
							foreach ($_POST['value'] as $key => $value) {
								$value = trim($value);
								if($value != ''){
									$file = '';
									if(is_uploaded_file($_FILES['file']['tmp_name'][$key])){
										$uniq = uniqid();
										move_uploaded_file($_FILES['file']['tmp_name'][$key], FOLDER . 'images/'.$uniq.'.jpg');
										$file = '/images/'.$uniq.'.jpg';
									}
									PropertyEnum::create(['value'=>$value, 'property_id'=>$id, 'xml_id'=>uniqid(), 'description'=>$_POST['description'][$key], 'file'=>$file]);
								}
							}
						}
						$this->template->vars('success', true);
					}else{
						$error['code'] = 'code';
						$this->template->vars('property', $_POST);
						$message[] = 'Свойство с таким символьным кодом уже существует';
						$this->template->vars('message', $message);
						$this->template->vars('error', $error);
					}
				}else{
					$this->template->vars('error', $error);
				}
			}
			$groups = PropertyGroup::find(['active' => 1]);
			$categories = Category::find(['active' => 1]);
			
			$this->breadcrumbs ['Админпанель'] = '/admin';
			$this->breadcrumbs ['Добавление новго свойтсва'] = '#';
			$this->render('create',[
				'title_page' => 'Добавление новго свойтсва',
				'breads' => $this->getBreadcrumbs(),
				'groups' => $groups,
				'categories' => $categories,
			]);

			return true;
		}
		
		public function actionEdit($id)
		{
//			$builder = new AdminBuilder(new Properties());
//			$builder->edit($id);
//			return true;

			if(isset($_POST['save'])){
				$property = Properties::findOne($id);
				$_POST['active'] = isset($_POST['active'])?1:0;
				$_POST['multi'] = isset($_POST['multi'])?1:0;
				$_POST['required'] = isset($_POST['required'])?1:0;
				$_POST['filter'] = isset($_POST['filter'])?1:0;
				$_POST['description_field'] = isset($_POST['description_field'])?1:0;
				$_POST['main'] = isset($_POST['main'])?1:0;
				Properties::updateById($id, $_POST);
				if($property['type'] == 'L' && $_POST['type'] != 'L') {
					PropertyEnum::deleteByProperty($id);
					PropertyValue::deleteByProperty($id);
				}
				if($property['type'] != $_POST['type']) {
					PropertyValue::deleteByProperty($id);
				}
//			if($property['multi'] != $_POST['type']) {
//				PropertyValue::deleteByProperty($id);
//			}
				if($_POST['type'] == 'L'){
					foreach ($_POST['value'] as $key => $value){
						$value = trim($value);
						if($value == '' && isset($_POST['id'][$key])) {
							PropertyEnum::deleteById($_POST['id'][$key]);
							PropertyValue::deleteFilter(['property_id'=>$id, 'value_enum'=>$_POST['id'][$key]]);
							continue;
						}

						if(isset($_POST['id'][$key])){
							$enum = PropertyEnum::findOne($_POST['id'][$key]);
							$file = $enum['file'];
						}else{
							$file = '';
						}

						if(is_uploaded_file($_FILES['file']['tmp_name'][$key])){
							$uniq = uniqid();
							move_uploaded_file($_FILES['file']['tmp_name'][$key], FOLDER . 'images/'.$uniq.'.jpg');
							$file = '/images/'.$uniq.'.jpg';
						}

						if(isset($_POST['id'][$key])){
							PropertyEnum::updateById($_POST['id'][$key], ['value'=>$value, 'file'=>$file, 'description'=>$_POST['description'][$key]]);
						}else{
							if($value != '')
								PropertyEnum::create(['value'=>$value, 'property_id'=>$id, 'xml_id'=>uniqid(), 'file'=>$file, 'description'=>$_POST['description'][$key]]);
						}
					}
				}
				$this->template->vars('success', true);
			}
			$property = Properties::findOne($id);
			if(!$property) header('Location: /admin/properties');
			if($property['type'] == 'L'){
				$propertyEnum = PropertyEnum::find(['property_id'=>$id]);
				$this->template->vars('propertyEnum', $propertyEnum);
			}

			$this->breadcrumbs ['Админпанель'] = '/admin';
			$this->breadcrumbs ['Редактирование свойтсва'] = '#';
			$groups = PropertyGroup::find(['active' => 1]);
			$categories = Category::find(['active' => 1]);
			$this->render('create',[
				'title_page' => 'Редактирование свойтсва',
				'property' => $property,
				'groups' => $groups,
				'breads' => $this->getBreadcrumbs(),
				'categories' => $categories,
			]);
			return true;


		}
		
		public function actionActivate()
		{
			$builder = new AdminBuilder(new Properties());
			$builder->active();
		}
		
		public function actionSortable()
		{
			$builder = new AdminBuilder(new Properties());
			$builder->sort();
		}
		
		public function actionDelete()
		{
			$builder = new AdminBuilder(new Properties());
			$builder->delete();
		}
		
		
	}
	
	?>