<?php

namespace Application\Controllers;

use \Application\Classes\AdminBuilder;
use \Application\Models\RequestOutput;
use Application\Models\User;
use function in_array;
use function time;

class AdminRequestOutputController extends \Application\Classes\AdminBase
{

  public function actionIndex( $page = 1 )
  {
    $builder = new AdminBuilder( new RequestOutput(), [], [ 'hide' => ['delete', 'view', 'create', 'edit'] ] );
    $builder->merge( new class extends User
    {
      public function getFields()
      {
        $fields = parent::getFields();
        foreach ( $fields as $field => $settings ) {
          $fields[$field]['visible'] = false;
        }
        $fields['name']['visible']             = true;
        $fields['juristic_type_id']['visible'] = true;
        return $fields;
      }
    } );
    $builder->index( $page );
    return true;
  }

  public function actionIndexNotice( $page = 1 )
  {
    $builder = new AdminBuilder( new class extends RequestOutput
    {
      public function getFields()
      {
        $fields        = parent::getFields();
        $visibleFields = [ 'id', 'user_id', 'created_at', 'sum' ];
        foreach ( $fields as $field => $settings ) {
          if ( !in_array( $field, $visibleFields ) ) {
            $fields[$field]['visible'] = false;
          } else {
            $fields[$field]['visible'] = true;
          }
        }
        $fields['user_id']['label'] = 'id агента';
        $fields['user_id']['sort']  = 2;
        $fields['sum']['sort']      = 3;

        $fields['detail'] = [
          'type'    => 'string',
          'visible' => true,
          'label'   => 'подробнее об агенте',
          'sort'    => 10000
        ];

        $fields['confirm'] = [
          'type'    => 'button',
          'visible' => true,
          'edited'  => false,
          'label'   => 'Принять',
          'sort'    => 11000,
          'url'     => '/admin/noticerequestoutput/confirm/'
        ];
        $fields['fail']    = [
          'type'    => 'button',
          'visible' => true,
          'edited'  => false,
          'label'   => 'Отклонить',
          'sort'    => 11000,
          'url'     => '/admin/noticerequestoutput/fail/'
        ];
        return $fields;
      }
    }, [], [ 'hide' => ['delete', /*'view',*/ 'edit' ]  ] );
    
    $builder->setView( 'index' );
    $builder->setWhere(['complete' => 0, 'fail' => 0]);
    $builder->index( $page );
    $items = $builder->getItems();
    foreach ( $items as $id => $item )
      $items[$id]['detail'] = '<a target="_blank" href="/admin/seller/view/' . $item['user_id'] . '">Подробнее</a>';
    $builder->setItems( $items );
    $builder->render();
    return true;
  }

  public function actionCreate()
  {
    $builder = new AdminBuilder( new RequestOutput() );
    $builder->create();
    return true;
  }

  public function actionEdit( $id )
  {
    $builder = new AdminBuilder( new RequestOutput() );
    $builder->edit( $id );
    return true;
  }

  public function actionActivate()
  {
    $builder = new AdminBuilder( new RequestOutput() );
    $builder->active();
    return true;
  }

  public function actionSortable()
  {
    $builder = new AdminBuilder( new RequestOutput() );
    $builder->sort();
    return true;
  }

  public function actionDelete()
  {
    $builder = new AdminBuilder( new RequestOutput() );
    $builder->delete();
    return true;
  }

  public function actionModal()
  {
    $builder = new AdminBuilder( new RequestOutput() );
    $builder->modal();
    return true;
  }

  public function actionConfirm( $id )
  {
    $this->operation( $id, 'confirm' );
  }

  public function actionFail( $id )
  {
    $this->operation( $id, 'fail' );
  }

  private function operation( $id, $type )
  {
    $request = RequestOutput::findOne( $id );
    if ( !$request )
      $this->json( false, 'Операция не найдена' );
    if ( $request['complete'] != 0 || $request['fail'] != 0 )
      $this->json( false, 'Операция уже выполнена' );
    $user = User::findOne( $request['user_id'] );
    if ( !$user )
      $this->json( false, 'Пользователь не найден' );
    if ( $type == 'confirm' ) {
      if ( $user['balance'] < $request['sum'] )
        $this->json( false, 'Недостаточно средств' );
      if ( RequestOutput::updateById( $id, [ 'complete' => 1, 'success_date' => date( 'd-m-Y H:i:s' ) ] ) )
        $this->json( true, 'Заявка принята' );
      else
        $this->json( false, 'Ошибка выполнение операции' );
    } elseif ( $type == 'fail' ) {
      if ( RequestOutput::updateById( $id, [ 'fail' => 1 ] ) )
        $this->json( true, 'Заявка успешно отклонена' );
      else
        $this->json( false, 'Ошибка выполнение операции' );
    }
    $this->json( false, 'Ошибка выполнения операции' );
  }

}

?>