<?php
	
	namespace Application\Controllers;
	
	use \Application\Classes\AdminBuilder;
	use \Application\Models\Review;
	
	class AdminReviewController extends \Application\Classes\AdminBase
	{
		
		public function actionIndex($page = 1)
		{
			$builder = new AdminBuilder(new Review(), [], ['activate' => 'active', 'hide' => ['view', 'edit' ] ]);
			$builder->index($page);
			return true;
		}
		
		public function actionCreate()
		{
			$builder = new AdminBuilder(new Review());
			$builder->create();
			return true;
		}
		
		public function actionEdit($id)
		{
			$builder = new AdminBuilder(new Review());
			$builder->edit($id);
			return true;
		}
		
		public function actionActivate()
		{
			$builder = new AdminBuilder(new Review());
			$builder->active();
		}
		
		public function actionSortable()
		{
			$builder = new AdminBuilder(new Review());
			$builder->sort();
		}
		
		public function actionDelete()
		{
			$builder = new AdminBuilder(new Review());
			$builder->delete();
		}

    public function actionModal()
    {
      $builder = new AdminBuilder( new Review() );
      $builder->modal();
      return true;
    }
		
	}
	
	?>