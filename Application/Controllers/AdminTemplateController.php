<?php
namespace Application\Controllers;
use \Application\Classes\AdminBuilder;
	use \Application\Models\Template;
class AdminTemplateController extends \Application\Classes\AdminBase
{

	public function actionIndex($page = 1)
	{
		$builder = new AdminBuilder(new Template(), [], ['hide' => 'view'] );
		$builder->index($page);
		return true;
	}

	public function actionCreate()
	{
		$builder = new AdminBuilder(new Template());
		$builder->create();
		return true;
	}

	public function actionEdit($id)
	{
		$builder = new AdminBuilder(new Template());
		$builder->edit($id);
		return true;
	}
	
	public function actionActivate(){
		$builder = new AdminBuilder(new Template());
		$builder->active();
		 return true;
	}
	
	public function actionSortable(){
		$builder = new AdminBuilder(new Template());
		$builder->sort();
		 return true;
	}
	public function actionDelete(){
		$builder = new AdminBuilder(new Template());
		$builder->delete();
		 return true;
	}
		public function actionModal(){
    $builder = new AdminBuilder(new Template());
    $builder->modal();
    return true;
  }
	
}
?>