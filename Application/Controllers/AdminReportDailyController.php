<?php
	namespace Application\Controllers;
	use \Application\Classes\AdminBuilder;
	use \Application\Models\ReportDaily;
	class AdminReportDailyController extends \Application\Classes\AdminBase
	{
		
		public function actionIndex($page = 1)
		{
			$model = new ReportDaily();
			$report_daily = $model->getReportDaily();
			return $this->render(
				'/report_daily',
				[
					'report_daily' => $report_daily,
				]);
		}
		
		public function actionEdit($id)
		{
			$builder = new AdminBuilder(new ReportDaily());
			$builder->edit($id);
			return true;
		}
		
		public function actionActivate(){
			$builder = new AdminBuilder(new ReportDaily());
			$builder->active();
			return true;
		}
		
		public function actionSortable(){
			$builder = new AdminBuilder(new ReportDaily());
			$builder->sort();
			return true;
		}
		public function actionDelete(){
			$builder = new AdminBuilder(new ReportDaily());
			$builder->delete();
			return true;
		}
		public function actionModal(){
			$builder = new AdminBuilder(new ReportDaily());
			$builder->modal();
			return true;
		}
		public function actionInfoByProductID($id){
				$model = new ReportDaily();
				echo json_encode($model->getProducts($id));
		}
	}
	?>