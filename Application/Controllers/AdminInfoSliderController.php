<?php
namespace Application\Controllers;
use \Application\Classes\AdminBuilder;
	use \Application\Models\InfoSlider;
class AdminInfoSliderController extends \Application\Classes\AdminBase
{

	public function actionIndex($page = 1)
	{
		$builder = new AdminBuilder(new InfoSlider(), [] ,['activate'=>'active', 'sortable'=>'sort']);
		$builder->index($page);
		return true;
	}

	public function actionCreate()
	{
		$builder = new AdminBuilder(new InfoSlider());
		$builder->create();
		return true;
	}

	public function actionEdit($id)
	{
		$builder = new AdminBuilder(new InfoSlider());
		$builder->edit($id);
		return true;
	}
	
	public function actionActivate(){
		$builder = new AdminBuilder(new InfoSlider());
		$builder->active();
	}
	
	public function actionSortable(){
		$builder = new AdminBuilder(new InfoSlider());
		$builder->sort();
	}
	public function actionDelete(){
		$builder = new AdminBuilder(new InfoSlider());
		$builder->delete();
	}
	
	
}
?>