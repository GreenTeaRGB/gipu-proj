<?php
	
	namespace Application\Controllers;
	
	use \Application\Models\Promotion;
	use \Application\Helpers\DataHelper;
	
	class PromotionController extends \Application\Classes\ControllerBase
	{
		public function actionIndex()
		{
			$promotions = Promotion::getActivePromotions();
			
			$this->render(
				'/list',
				[
					'promotions' => $promotions
				]
			);
			
			return true;
		}
		
		public function actionShow($alias)
		{
			$promotion = Promotion::find(
				[
					'alias' => strip_tags($alias)
				],
				true
			);
			
			$this->render(
				'/show',
				[
					'promotion' => $promotion
				]
			);
			
			return true;
		}
	}