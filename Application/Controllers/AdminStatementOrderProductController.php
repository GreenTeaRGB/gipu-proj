<?php

namespace Application\Controllers;

use \Application\Classes\AdminBuilder;
use Application\Models\Order;
use \Application\Models\OrderProduct;
use Application\Models\Product;

class AdminStatementOrderProductController extends \Application\Classes\AdminBase
{

  /**
   * @param int $page
   * @return bool
   */
  public function actionIndex( $page = 1 )
  {
    $builder = new AdminBuilder( new OrderProduct(), [], [ 'hide' => ['delete', 'view', 'create', 'edit'] ] );
    $builder->merge(new class extends Product {
      public function getFields()
      {
        $fields = parent::getFields();
        $visibleFields = ['name', 'purchase_price'];
        foreach ( $fields as $field => $settings ) {
          if(!in_array($field, $visibleFields)){
            $fields[$field]['visible'] = false;
          }else{
            $fields[$field]['sort'] = 500;
            if($field == 'name'){
              $fields[$field]['sort'] = 2;
              $fields[$field]['name'] = 'Наименование товара';
            }
            $fields[$field]['visible'] = true;
          }
        }
        $fields['purchase_price']['sort'] = 11;
        return $fields;
      }
    });
    $builder->merge(new class extends Order {
      public function getFields()
      {
        $fields = parent::getFields();
        $visibleFields = ['created_at', 'total', 'pay_type_id', 'courier_id', 'supplier_id', 'delivery_type_id', ];
        foreach ( $fields as $field => $settings ) {
          $fields[$field]['sort'] = 500;
          if(!in_array($field, $visibleFields)){
            $fields[$field]['visible'] = false;
          }else{
            $fields[$field]['visible'] = true;
          }
        }
        $fields['supplier_id']['sort'] = 5;
        $fields['created_at']['sort'] = 6;
        $fields['created_at']['label'] = "Дата заказа";
        $fields['delivery_type_id']['sort'] = 7;
        $fields['pay_type_id']['sort'] = 8;
        $fields['pay_type_id']['sort'] = 9;
        $fields['pay_type_id']['label'] = 'Тип олаты';
        $fields['total']['sort'] = 10;
        $fields['total']['label'] = 'Сумма заказа';
        return $fields;
      }
    });
    $builder->index( $page );
    return true;
  }

  public function actionCreate()
  {
    $builder = new AdminBuilder( new OrderProduct() );
    $builder->create();
    return true;
  }

  public function actionEdit( $id )
  {
    $builder = new AdminBuilder( new OrderProduct() );
    $builder->edit( $id );
    return true;
  }

  public function actionActivate()
  {
    $builder = new AdminBuilder( new OrderProduct() );
    $builder->active();
    return true;
  }

  public function actionSortable()
  {
    $builder = new AdminBuilder( new OrderProduct() );
    $builder->sort();
    return true;
  }

  public function actionDelete()
  {
    $builder = new AdminBuilder( new OrderProduct() );
    $builder->delete();
    return true;
  }

  public function actionModal()
  {
    $builder = new AdminBuilder( new OrderProduct() );
    $builder->modal();
    return true;
  }

  public function actionView( $id )
  {
    $builder = new AdminBuilder( new OrderProduct() );
    $builder->view( $id );
    return true;
  }
}

?>