<?php
namespace Application\Controllers;
use \Application\Classes\AdminBuilder;
	use \Application\Models\Block;
class AdminBlockController extends \Application\Classes\AdminBase
{

	public function actionIndex($page = 1)
	{
		$builder = new AdminBuilder(new Block(), [], ['hide' => ['view']] );
		$builder->index($page);
		return true;
	}

	public function actionCreate()
	{
		$builder = new AdminBuilder(new Block());
		$builder->create();
		return true;
	}

	public function actionEdit($id)
	{
		$builder = new AdminBuilder(new Block());
		$builder->edit($id);
		return true;
	}
	
	public function actionActivate(){
		$builder = new AdminBuilder(new Block());
		$builder->active();
		 return true;
	}
	
	public function actionSortable(){
		$builder = new AdminBuilder(new Block());
		$builder->sort();
		 return true;
	}
	public function actionDelete(){
		$builder = new AdminBuilder(new Block());
		$builder->delete();
		 return true;
	}
		public function actionModal(){
    $builder = new AdminBuilder(new Block());
    $builder->modal();
    return true;
  }
	
}
?>