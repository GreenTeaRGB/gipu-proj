<?php
	
	namespace Application\Controllers;
	
	use \Application\Classes\AdminBuilder;
	use \Application\Models\PropertyGroup;
	
	class AdminPropertyGroupController extends \Application\Classes\AdminBase
	{
		
		public function actionIndex($page = 1)
		{
			$builder = new AdminBuilder(new PropertyGroup(), [], ['activate' => 'active', 'sortable' => 'sort']);
			$builder->index($page);
			return true;
		}
		
		public function actionCreate()
		{
			$builder = new AdminBuilder(new PropertyGroup());
			$builder->create();
			return true;
		}
		
		public function actionEdit($id)
		{
			$builder = new AdminBuilder(new PropertyGroup());
			$builder->edit($id);
			return true;
		}
		
		public function actionActivate()
		{
			$builder = new AdminBuilder(new PropertyGroup());
			$builder->active();
		}
		
		public function actionSortable()
		{
			$builder = new AdminBuilder(new PropertyGroup());
			$builder->sort();
		}
		
		public function actionDelete()
		{
			$builder = new AdminBuilder(new PropertyGroup());
			$builder->delete();
		}
		
		
	}
	
	?>