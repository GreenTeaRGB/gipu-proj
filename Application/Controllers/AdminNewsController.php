<?php
namespace Application\Controllers;
use \Application\Classes\AdminBuilder;
use \Application\Models\News;
class AdminNewsController extends \Application\Classes\AdminBase
{

	public function actionIndex($page = 1)
	{
		$builder = new AdminBuilder(new News(), [['name'=>'Категории', 'link'=>'/admin/newscategory'], ['name'=>'Новости', 'link'=>'/admin/news']] ,['activate'=>'active', 'sortable'=>'sort']);
		$builder->index($page);
		return true;
	}

	public function actionCreate()
	{
		$builder = new AdminBuilder(new News());
		$builder->create();
		return true;
	}

	public function actionEdit($id)
	{
		$builder = new AdminBuilder(new News());
		$builder->edit($id);
		return true;
	}
	
	public function actionActivate(){
		$builder = new AdminBuilder(new News());
		$builder->active();
	}
	
	public function actionSortable(){
		$builder = new AdminBuilder(new News());
		$builder->sort();
	}
	public function actionDelete(){
		$builder = new AdminBuilder(new News());
		$builder->delete();
	}
	
	
}
?>