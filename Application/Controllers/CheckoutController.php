<?php

namespace Application\Controllers;

use Application\Classes\Session;
use Application\Helpers\AppHelper;
use Application\Helpers\DataHelper;
use Application\Helpers\UserHelper;
use Application\Models\Order;

class CheckoutController extends \Application\Classes\ControllerBase
{

  private $user = false;

  public function __construct()
  {
    parent::__construct();
    $this->user = UserHelper::getUser();
    if ( !$this->user )
    {
      $this->user['id'] = 0;
      $this->user['type'] = DataHelper::DEFAULT_BUYER_TYPE_ID;
    }
  }

  public function actionIndex()
  {
//      $addressModel = new DeliveryAddress();
//      $addresses    = $addressModel
//        ->where( [ 'user_id' => $this->user['id'] ] )
//        ->fetchAll();

    $cartProducts = Session::getByKey( 'cart' );
    if ( !$cartProducts )
      $this->redirect( '/cart' );
    $defaultDeliveryPrice = \Application\Helpers\DataHelper::DEFAULT_DELIVERY_PRICE;
    $discount             = \Application\Classes\Session::getByKey( 'discount' );
    if(!$discount) $discount = 0;
    else $discount = $discount['discount'];
    $payments             = \Application\Models\PayType::find();
    return $this->render(
      '/show',
      [
        'user'                 => $this->user,
        //          'saved_addresses'      => $addresses,
        'cartProducts'         => $cartProducts,
        'defaultDeliveryPrice' => $defaultDeliveryPrice,
        'discount'             => $discount,
        'payments'             => $payments
      ]
    );
  }

//  public function actionSaveOrderDelivery()
//  {
//    $request  = $_POST;
//    $response = [
//      'success' => false,
//      'message' => 'Ошибка сохранения адреса'
//    ];
//
//    if ( count( $request ) > 0 ) {
//      foreach ( $request as $key => $value ) {
//        $request[$key] = htmlspecialchars( strip_tags( $value ), true );
//      }
//
//      Session::store( [ 'delivery_address' => $request ] );
//      $response = [
//        'success' => true,
//        'message' => 'Адрес успешно сохранен',
//        'address' => $request
//      ];
//    }
//
//    echo json_encode( $response );
//  }

  public function actionPlaceOrder()
  {
    $orderProducts = Session::getByKey( 'cart' );
    if ( $orderProducts && count( $orderProducts ) > 0 ) {
      $_POST['buyer_id']           = $this->user['id'];
      $_POST['pay_type_id']        = isset( $_POST['payment'] ) ? $_POST['payment'] : DataHelper::DEFAULT_PAY_TYPE_ID;
      $_POST['status_id']          = DataHelper::DEFAULT_ORDER_STATUS_ID;
      $_POST['delivery_status_id'] = DataHelper::DEFAULT_DELIVERY_STATUS_ID;
      if($this->user['type'] == DataHelper::DEFAULT_SELLER_TYPE_ID){
        $_POST['internal_account'] = 1;
      }
      $orderModel                  = new Order();
      $orderModel->setScenario( 'order' );
      if ( $orderModel->load( $_POST, '' ) && $orderModel->validate() ) {
        $orderModel->storeProducts( $orderProducts );
        $discount = Session::getByKey( 'discount' );
        if ( $discount ) {
          $orderModel->storeDiscount( $discount );
        }
        $orderId = $orderModel->save( $_POST );
        if ( $orderId ) {
          if($this->user['id'] == 0){
            $ordersId = Session::getByKey('user_orders_id');
            if(!$ordersId){
              $ordersId = [$orderId];
            }else{
              $ordersId[] = $orderId;
            }
            Session::store(['user_orders_id' => $ordersId]);
          }
          Session::store( [ 'show_thanks' => true ] );
          Session::remove( 'cart' );
          $this->redirect( '/user/order-preview/' . $orderId );
        } else{
          $this->redirect( '/cart' );
        }
      } else {
        $message = '';
        foreach ( $orderModel->getErrors() as $error )
          $message .= $error[0].'<br>';
          AppHelper::setMessage( 'error', $message );
          Session::store(['user_data_cart' => $_POST]);
        $this->redirect( '/checkout', 301, $orderModel->getErrors() );
      }

//
//        $deliveryAddress = Session::getByKey( 'delivery_address' );
//        $orderModel->storeDeliveryAddress( $deliveryAddress );
//
//        $discount = Session::getByKey( 'discount' );
//        if ( $discount ) {
//          $orderModel->storeDiscount( $discount );
//        }

//        $orderId = $orderModel->save( $order );
//        if ( $orderId ) {
//          Session::store( [ 'show_thanks' => true ] );
//          Session::remove( 'cart' );
//
//          $this->redirect( '/user/order-preview/' . $orderId );
//
//          return true;
//        } else {
//          AppHelper::setMessage( 'error', 'Ошибка создания ордера' );
//          $this->redirect( '/checkout' );
//        }
    } else {
      AppHelper::setMessage( 'error', 'Для оформления заказа необходимо выбрать товар(ы)' );
      $this->redirect( '/cart' );
    }
  }
}