<?php

namespace Application\Controllers;

class AdminMessagesController extends \Application\Classes\AdminBase
{

  public function actionIndex(){
    $this->breadcrumbs ['Админпанель'] = '/admin';
    $this->breadcrumbs ['Сообщения поддержки'] = '#';
    return $this->render('index', [
      'breads'=> $this->getBreadcrumbs(),
      'title_page' => 'Сообщения поддержки'
    ]);
  }

}
