<?php
	
	namespace Application\Controllers;
	
	use \Application\Classes\Pagination;
	use \Application\Models\News;
	use \Application\Models\NewsCategory;
	use \Application\Helpers\AppHelper;
	
	class NewsController extends \Application\Classes\ControllerBase
	{
		const DEFAULT_ACTIVE_STATUS = 1;
		private $limit = 2;
		
		public function actionIndex($categoryAlias = '', $page = 1)
		{
			$model = new News();
			$newsCategories = NewsCategory::find();
			$categoryId = 1;
			
			if ( $categoryAlias ) {
				foreach ($newsCategories as $newsCategory) {
					if ( $newsCategory['alias'] == $categoryAlias ) {
						$categoryId = $newsCategory['id'];
						break;
					}
				}
			}

			$model->where(['active' => self::DEFAULT_ACTIVE_STATUS, 'category_id' => $categoryId]);
			
			$start = ($page - 1) * $this->limit;
			$news = $model->limit($start, $this->limit)->fetchAll();
			
			$count = $model->where(['active' => self::DEFAULT_ACTIVE_STATUS, 'category_id' => $categoryId])->count();
			$pagination = new Pagination($count, $page, $this->limit, 'page-');
			$count_pages = ceil($count / $this->limit);
			
			return $this->render(
				'index',
				[
					'news_categories' => $newsCategories,
					'news'            => $news,
					'pagination'      => $count_pages > 1 ? $pagination->get() : ''
				]
			);
		}
		
		public function actionShow($alias)
		{
			$model = new News();
			$newsItem = $model->where(['alias' => $alias, 'active' => self::DEFAULT_ACTIVE_STATUS])->fetchOne();
			
			if ( $newsItem && count($newsItem) > 0 ) {
				return $this->render(
					'show',
					[
						'news_item' => $newsItem
					]
				);
			} else {
				AppHelper::setMessage('error', 'Такой новости не существует');
				
				$this->redirect('/news');
			}
		}

		public function actionGetByCategory($id){
      $news = News::findByCategoryId( $id );
      die(json_encode($news));
    }
	}