<?php
	namespace Application\Controllers;
	use \Application\Classes\AdminBuilder;
	use \Application\Models\UserGroup;
	use \Application\Models\UserGroupRights;
	class AdminUserGroupController extends \Application\Classes\AdminBase
	{

		public function actionIndex($page = 1)
		{
			$builder = new AdminBuilder(new UserGroup(), [] );
			$builder->index($page);
			return true;
		}

		public function actionCreate()
		{
			$builder = new AdminBuilder(new UserGroup());
			$builder->create();
			if($id = $builder->getResult() && isset($_POST['pages'])){
				UserGroupRights::createGroup($_POST['pages'], $id);
			}
			return true;
		}

		public function actionEdit($id)
		{
			$builder = new AdminBuilder(new UserGroup());
			if(isset($_POST['submit'])){
				$pages = !is_array($_POST['pages']) ? unserialize($_POST['pages']): $_POST['pages'];
				$group_rights = UserGroupRights::getListByGroup($id);
				$forDelete = array_diff($group_rights, $pages);
				foreach ($forDelete as $catId) UserGroupRights::deleteByIdGroup($catId,$id);
				$forCreate = array_diff($pages, $group_rights);
				UserGroupRights::createGroup($forCreate, $id);
			}
			$builder->edit($id);


			return true;
		}

		public function actionActivate(){
			$builder = new AdminBuilder(new UserGroup());
			$builder->active();
		}

		public function actionSortable(){
			$builder = new AdminBuilder(new UserGroup());
			$builder->sort();
		}
		public function actionDelete(){
			$builder = new AdminBuilder(new UserGroup());
			$builder->delete();
		}


	}
	?>