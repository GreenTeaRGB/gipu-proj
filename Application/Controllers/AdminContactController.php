<?php

namespace Application\Controllers;

use \Application\Classes\AdminBuilder;
use \Application\Models\Contact;

class AdminContactController extends \Application\Classes\AdminBase
{

  public function actionIndex( $page = 1 )
  {
    $builder = new AdminBuilder( new Contact(), [] );
    $builder->edit( 1 );
//		$builder->index($page);
    return true;
  }

  public function actionCreate()
  {
    $builder = new AdminBuilder( new Contact() );
    $builder->create();
    return true;
  }

  public function actionEdit( $id )
  {
    $builder = new AdminBuilder( new Contact() );
    $builder->edit( $id );
    return true;
  }

  public function actionActivate()
  {
    $builder = new AdminBuilder( new Contact() );
    $builder->active();
    return true;
  }

  public function actionSortable()
  {
    $builder = new AdminBuilder( new Contact() );
    $builder->sort();
    return true;
  }

  public function actionDelete()
  {
    $builder = new AdminBuilder( new Contact() );
    $builder->delete();
    return true;
  }

  public function actionModal()
  {
    $builder = new AdminBuilder( new Contact() );
    $builder->modal();
    return true;
  }

}

?>