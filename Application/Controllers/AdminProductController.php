<?php

namespace Application\Controllers;

use Application\Classes\AdminBuilder;
use Application\Models\Product;
use Application\Models\Properties;
use Application\Models\PropertyValue;
use Application\Models\Warehouse;

class AdminProductController extends \Application\Classes\AdminBase
{

  public function actionIndex( $page = 1 )
  {
    $builder = new AdminBuilder( new Product(), [], [ 'activate' => 'active', 'hide' => [ 'delete', 'view' ] ] );
    $builder->index( $page );
    return true;
  }

  public function actionCreate( $type = false, $id = 0 )
  {
    $errors     = [];
    $request    = $_POST;
    $properties = new Properties();
    $builder    = new AdminBuilder( new Product() );
    if ( isset( $_POST['submit'] ) && isset( $_POST['property'] ) ) {
      $errors = Product::findErrorsProperties( $_POST['property'] );
      if ( count( $errors ) > 0 ) {
        unset( $_POST['submit'] );
        $builder->setResult( false );
        $builder->setErrors( $errors );
      }
    }

    if ( isset( $_POST['category_id'] ) ) {
      $builder->setDetailTables(
        [
          'name'    => 'Свойства',
          'id'      => 'divProperty',
          'content' => $this->render(
              'properties', [
              'properties' => $properties::find(
                [
                  'category_id' => \Application\Models\Category::makeParentCategories( (int)$_POST['category_id'] ),
                  'active'      => 1
                ]
              ),
              'values'     => [],
              'error'      => $errors
            ], true
            ) . '<script>' . $properties->getClientValidation() . '</script>'
        ]
      );
    } else {
      $builder->setDetailTables(
        [
          'name'    => 'Свойства',
          'id'      => 'divProperty',
          'content' => ''
        ]
      );
    }


    $builder->create( $id, $type );
    if ( isset( $request['submit'] ) && isset( $request['property'] ) && count( $errors ) == 0 ) {
      Product::loadProperties( $builder->getResult(), $request['property'] );
    }
    return true;
  }

  public function actionEdit( $id )
  {
    if ( isset( $_POST['delete'] ) && $_POST['delete'] == 'image_prop' && isset( $_POST['delete_id'] ) ) {
      if ( PropertyValue::deleteById( $_POST['delete_id'] ) )
        echo json_encode( [ 'result' => true ] );
      else echo json_encode( [ 'result' => false ] );
      return true;
    }
    $product = new Product();
    $builder = new AdminBuilder( $product );
    //для исключения проверки уникальности alias
    $product->id = $id;
//  $builder->merge(new Warehouse());
    $errors = [];
    if ( isset( $_POST['submit'] ) && isset( $_POST['property'] ) ) {
      $errors = Product::findErrorsProperties( $_POST['property'], $id );
      if ( count( $errors ) > 0 )
        unset( $_POST['submit'] );
      else {
        $errors = Product::loadProperties( $id, $_POST['property'] );
      }
    }

    $builder->setView( 'edit' );
    $builder->edit( $id );
    if ( count( $errors ) > 0 ) {
      $builder->setErrorMessage( 'Ошибка сохранения' );
      $builder->setResult( false );
    }
    $product    = $builder->getItem();
    $properties = new Properties();
    $builder->setDetailTables(
      [
        'name'    => 'Свойства',
        'id'      => 'divProperty',
        'content' => $this->render(
            'properties', [
            'properties' => Properties::find(
              [
                'category_id' => \Application\Models\Category::makeParentCategories( isset( $_POST['category_id'] )
                  ? (int)$_POST['category_id'] : $product['category_id'] ),
                'active'      => 1
              ]
            ),
            'values'     => PropertyValue::find( [ 'product_id' => $id ] ),
            'error'      => $errors
          ], true
          ) . '<script>' . $properties->getClientValidation() . '</script>'
      ]
    );
    $builder->render();
    return true;
  }

  public function actionActivate()
  {
    $builder = new AdminBuilder( new Product() );
    $builder->active();
    return true;
  }

  public function actionSortable()
  {
    $builder = new AdminBuilder( new Product() );
    $builder->sort();
    return true;
  }

  public function actionDelete()
  {
    $builder = new AdminBuilder( new Product() );
    $builder->delete();
    return true;
  }

  public function actionDeleteFile()
  {
    $builder = new AdminBuilder( new Product() );
    $builder->deleteFile();
    return true;
  }

  public function actionModal()
  {
    $builder = new AdminBuilder( new Product() );
    $builder->modal();
    return true;
  }

  public function actionGetContentPropertiesByCategoryId( $id, $product_id = false )
  {
    $properties = new Properties();
    echo $this->render(
        'properties', [
        'properties' => Properties::find(
          [
            'category_id' => \Application\Models\Category::makeParentCategories( $id ),
            'active'      => 1,
          ]
        ),
        'values'     => ( $product_id !== false ? PropertyValue::find( [ 'product_id' => $product_id ] ) : [] ),
        'error'      => []
      ], true
      ) . '<script src="/media/js/admin/formValidatorAdmin.js"></script>
        <script>' . $properties->getClientValidation() . '</script>';
    return true;
  }

  public function actionUpload()
  {

  }
}

?>