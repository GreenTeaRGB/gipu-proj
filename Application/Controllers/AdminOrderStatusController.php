<?php
namespace Application\Controllers;
use \Application\Classes\AdminBuilder;
	use \Application\Models\OrderStatus;
class AdminOrderStatusController extends \Application\Classes\AdminBase
{

	public function actionIndex($page = 1)
	{
		$builder = new AdminBuilder(new OrderStatus(), [] ,['activate'=>'active']);
		$builder->index($page);
		return true;
	}

	public function actionCreate()
	{
		$builder = new AdminBuilder(new OrderStatus());
		$builder->create();
		return true;
	}

	public function actionEdit($id)
	{
		$builder = new AdminBuilder(new OrderStatus());
		$builder->edit($id);
		return true;
	}
	
	public function actionActivate(){
		$builder = new AdminBuilder(new OrderStatus());
		$builder->active();
		 return true;
	}
	
	public function actionSortable(){
		$builder = new AdminBuilder(new OrderStatus());
		$builder->sort();
		 return true;
	}
	public function actionDelete(){
		$builder = new AdminBuilder(new OrderStatus());
		$builder->delete();
		 return true;
	}
		public function actionModal(){
    $builder = new AdminBuilder(new OrderStatus());
    $builder->modal();
    return true;
  }
	
}
?>