<?php

  namespace Application\Controllers;

  use Application\Classes\Session;
  use Application\Helpers\AppHelper;
  use Application\Helpers\DataHelper;
  use Application\Helpers\UploadedFile;
  use Application\Helpers\UserHelper;
  use Application\Models\Category;
  use Application\Models\Product;
  use Application\Models\Properties;
  use Application\Models\PropertyValue;
  use Application\Models\WarehouseProduct;
  use function Sodium\crypto_sign_detached;

  class ProductController extends \Application\Classes\ControllerBase
  {
    const DEFAULT_LOGIN_USER_MESSAGE        = 'Чтобы получить доступ, войдите в аккаунт';
    const DEFAULT_VERIFICATION_USER_MESSAGE = 'Чтобы получить доступ, пройдите верификацию';

    public function actionIndex()
    {
      return true;
    }

    public function actionCreate()
    {
      $user = UserHelper::getUser();

      if( $user ) {
        $verified = UserHelper::userIsVerified();

        if( $verified ) {
          $categories = Category::getFullList();
          $this->render(
              '/edit',
              [
                  'categories' => $categories
              ]
          );

        } else {
          AppHelper::setMessage( 'error', self::DEFAULT_VERIFICATION_USER_MESSAGE );
          $this->redirect( '/user/dashboard' );
        }
      } else {
        AppHelper::setMessage( 'error', self::DEFAULT_LOGIN_USER_MESSAGE );
        $this->redirect( '/user/login' );
      }

      return true;
    }

    public function actionEdit( $id )
    {
      $user = UserHelper::getUser();

      if( $user ) {
        $verified = UserHelper::userIsVerified();

        if( $verified ) {
          $product    = Product::getProductById( $id, $user['id'], Session::getByKey( 'warehouseId' ) );
          $categories = Category::getFullList();

          $this->render(
              '/edit',
              [
                  'product'    => $product,
                  'categories' => $categories,
              ]
          );

        } else {
          AppHelper::setMessage( 'error', self::DEFAULT_VERIFICATION_USER_MESSAGE );
          $this->redirect( '/user/dashboard' );
        }
      } else {
        AppHelper::setMessage( 'error', self::DEFAULT_LOGIN_USER_MESSAGE );
        $this->redirect( '/user/login' );
      }

      return true;
    }

    public function actionSave( $id = '' )
    {
      $user = UserHelper::getUser();

      if( $user ) {
        $verified = UserHelper::userIsVerified();
        if( $verified ) {

          $request              = $_POST;
          $request['status_id'] = DataHelper::DEFAULT_PRODUCT_STATUS_ID;
          $model                = new Product();
          if( $id !== '' ) {
            $model->id = $id;
          }
          if( !$model->load( $request, '' ) || !$model->validate() ) {
            if( $id == '' )
              $this->redirect( '/product/create' );
            else
              $this->redirect('/product/edit/'.$id);
          }
          $warehouseId         = Session::getByKey( 'warehouseId' );
          $request['owner_id'] = $user['id'];
          $warehouseModel      = new WarehouseProduct();

          $prodId = '';
          if( $id ) {
            $product = $model->where(['id' => $id])->fetchOne();
            $request['images'] = $product['images'];

            $request['certificate'] = $product['certificate'];
            $images = UploadedFile::getInstances( $model, 'images' );
            
            if( count( $images ) > 0 ) {
              foreach( $images as $image ) {
                $request['images'][] = $image->save( $model );
                
              }
            }
            $certificate = UploadedFile::getInstances( $model, 'certificate' );
            if( count( $certificate ) > 0 ) {
	            foreach ($certificate as $certificat) {
		            $request['certificate'][] = $certificat->save($model);
	            }
            }
	
            $done = $model->where( [ 'id' => (int) $id ] )->setFields( $request )->update();
            if( $done ) {
              $warehouseModel->setFields( [ 'price' => $request['price'] ] )->where(
                  [
                      'product_id'   => $id,
                      'warehouse_id' => $warehouseId
                  ]
              )->update();
              $prodId = $id;
            }
          } else {
            if( $warehouseId ) {
                            $request['warehouse_id'] = $warehouseId;
              $images = UploadedFile::getInstances( $model, 'images' );
              if( count( $images ) > 0 ) {
                foreach( $images as $image ) {
                  $request['images'][] = $image->save( $model );
                }
              }
              $prodId = $model->setFields( $request )->insert();
              if( $prodId )
                $warehouseModel::create(
                    [
                        'warehouse_id' => $warehouseId,
                        'product_id'   => $prodId,
                        'price'        => $request['price']
                    ]
                );
            }
          }
          if( $prodId ) {
            PropertyValue::deleteByProductId( $prodId );
            $propertyEnum       = $request['property_enum'];
            $propertyValueModel = new PropertyValue();

//	          $images = UploadedFile::getInstances( $propertyValueModel, 'value' );
//	          $request['value'] = $propertyValueModel['value'];
	          
//	          $images = UploadedFile::getInstances( $model, 'images' );
//	          if( count( $images ) > 0 ) {
//		          foreach( $images as $image ) {
//			          $request['images'][] = $image->save( $model );
//		          }
//	          }
            
            if( $propertyEnum && is_array( $propertyEnum ) && count( $propertyEnum ) > 0 ) {
	            $fields = [];
	            foreach ($propertyEnum as $propertyId => $enumArray) {
		            if ( $enumArray && is_array($enumArray) && count($enumArray) > 0 ) {
			            foreach ($enumArray as $enum) {
				            $fields[] = [
					            'property_id' => $propertyId,
					            'value_enum' => $enum,
					            'product_id' => $prodId,
					            'value' => '',
					            'description' => ''
				            ];
			            }
		            }
		
	            }
            }
            $propertyValues = $request['property_value'];

            if( $propertyValues && is_array( $propertyValues ) && count( $propertyValues ) > 0 ) {
              foreach( $propertyValues as $propertyId => $propertyValueArray ) {
                if( $propertyValueArray && is_array( $propertyValueArray ) && count( $propertyValueArray ) > 0 ) {
                  if( isset( $propertyValueArray['value'] ) ) {
                  	if ( !isset( $propertyValueArray['description'] )){
	                    $propertyValueArray['description'] = "";
                    }
	                    if ( is_array($propertyValueArray['value']) ) {
		                    foreach ($propertyValueArray['value'] as $key => $val_tmp) {
			                    $fields[] = [
				                    'property_id' => $propertyId,
				                    'value' => $val_tmp,
				                    'description' => isset($propertyValueArray['description'][$key]) ? $propertyValueArray['description'][$key] : '',
				                    'product_id' => $prodId
			                    ];
		                    }
	                    } else {
		                    $fields[] = [
			                    'property_id' => $propertyId,
			                    'value' => $propertyValueArray['value'],
			                    'description' => $propertyValueArray['description'],
			                    'product_id' => $prodId
		                    ];
	                    }
                  }
                }
              }
                if( count( $fields ) > 0 ) {
                    $propertyValueModel->setAllFields( $fields )->insertAll();
                }
            }

          } else {
            /** ToDo show errors */
          }

          if( $warehouseId ) {
            $this->redirect( 'user/show-warehouse/'.$warehouseId );
          } else {
            $this->redirect( 'user/warehouse-list' );
          }
        } else {
          AppHelper::setMessage( 'error', self::DEFAULT_VERIFICATION_USER_MESSAGE );
          $this->redirect( '/user/dashboard' );
        }
      } else {
        AppHelper::setMessage( 'error', self::DEFAULT_LOGIN_USER_MESSAGE );
        $this->redirect( '/user/login' );
      }

      return true;
    }

    public function actionGetPropertiesByCategoryId()
    {
	    $request = $_POST;
	    $response = [
		    'success' => false, 'message' => ''
	    ];
	
	    if ( isset($request['is_ajax']) && $request['is_ajax'] && isset($request['category_id']) && $request['category_id'] ) {
		    $propertyModel = new Properties();
		
		    $properties = $propertyModel->where(['category_id' => $request['category_id']])->fetchAll();
		    $response = [
			    'success' => true, 'properties' => $properties
		    ];
	    }
	    echo json_encode($response);
    }
  }