<?php
	namespace Application\Controllers;
	use \Application\Classes\AdminBuilder;
	use \Application\Models\Category;
	class AdminCategoryController extends \Application\Classes\AdminBase
	{

		public function actionIndex($page = 1)
		{
			$builder = new AdminBuilder(new Category(), [], ['sortable'=>'sort', 'parent'=>'parent_id', 'activate'=>'active'] );
			$builder->tree = true;
			$builder->index($page);
			return true;
		}

		public function actionCreate($type = false, $id = 0)
		{
			$builder = new AdminBuilder(new Category());
			$builder->create($id, $type);
			return true;
		}

		public function actionEdit($id)
		{
			$builder = new AdminBuilder(new Category());
			$builder->edit($id);
			return true;
		}

		public function actionActivate(){
			$builder = new AdminBuilder(new Category());
			$builder->active();
			return true;
		}

		public function actionDeleteFile(){
      $builder = new AdminBuilder(new Category());
      $builder->deleteFile();
      return true;
    }

		public function actionSortable(){
			$builder = new AdminBuilder(new Category());
			$builder->sort();
			return true;
		}
		
		public function actionDelete(){
			$builder = new AdminBuilder(new Category());
			$builder->delete();
			return true;
		}
	}
	?>