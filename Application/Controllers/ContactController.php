<?php

namespace Application\Controllers;


use Application\Classes\ControllerBase;
use Application\Models\Contact;

class ContactController extends ControllerBase
{
  function actionIndex()
  {
    $this->render( 'contact', ['contact' => Contact::get()] );
  }
}