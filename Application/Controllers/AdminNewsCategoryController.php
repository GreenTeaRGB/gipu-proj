<?php
namespace Application\Controllers;
use \Application\Classes\AdminBuilder;
	use \Application\Models\NewsCategory;
class AdminNewsCategoryController extends \Application\Classes\AdminBase
{

	public function actionIndex($page = 1)
	{
		$builder = new AdminBuilder(new NewsCategory(), [['name'=>'Категории', 'link'=>'/admin/newscategory'], ['name'=>'Новости', 'link'=>'/admin/news']] ,['sortable'=>'sort']);
		$builder->index($page);
		return true;
	}

	public function actionCreate()
	{
		$builder = new AdminBuilder(new NewsCategory());
		$builder->create();
		return true;
	}

	public function actionEdit($id)
	{
		$builder = new AdminBuilder(new NewsCategory());
		$builder->edit($id);
		return true;
	}
	
	public function actionActivate(){
		$builder = new AdminBuilder(new NewsCategory());
		$builder->active();
	}
	
	public function actionSortable(){
		$builder = new AdminBuilder(new NewsCategory());
		$builder->sort();
	}
	public function actionDelete(){
		$builder = new AdminBuilder(new NewsCategory());
		$builder->delete();
	}
	
	
}
?>