<?php
	
	namespace Application\Controllers;
	
	use \Application\Classes\Session;
	use \Application\Models\Product;
	use \Application\Models\Category;
	use \Application\Models\Discount;
	use \Application\Helpers\AppHelper;
	
	class CartController extends \Application\Classes\ControllerBase
	{
		public function actionIndex()
		{

			$data = $this->getCartData();
			$this->render('/show', ['products' => $data['products'], 'discount' => $data['discount']]);
			
			return true;
		}
		
		public function actionAdd()
		{
			$message = 'Ошибка добавления товара в корзину';
			$request = $_POST;
			$response = [
				'success' => false,
				'message' => $message
			];

//			Session::remove('cart');
//			die;
			if ( isset($request['is_ajax']) && $request['is_ajax'] ) {
				$cartData = Session::getByKey('cart');
				$product = Product::find(['id' => $request['product_id']], true);
//				if(isset($request['warehouse_id']) && isset($product['warehouses'][$request['warehouse_id']])) {
//          $product['price']        = $product['warehouses'][$request['warehouse_id']]['price'];
//          $product['warehouse_id_cart'] = $product['warehouses'][$request['warehouse_id']]['warehouse_id'];
//        }
//				else
//          die(json_encode($response));
				$productPriceWithCommission = AppHelper::calculatePriceWithCommission($product['price'], $product['commission']);
				if(!isset($request['property'])){
          $request['property'] = [];
        }
				$product['full_price'] = $productPriceWithCommission;
				$category = Category::findOne($product['category_id']);
				if ( !$cartData ) {
					$cartData = [
						[
							'product'  => $product,
							'count'    => 1,
							'category' => $category['name'],
//              'warehouse_id' => $request['warehouse_id'],
              'properties' => serialize($request['property'])
						]
					];
					Session::store(['cart' => $cartData]);
					
				} else {
					$updated = false;
					foreach ($cartData as $key => $cartProduct) {
						if(isset($cartProduct['product']['id'])){
							if ( $cartProduct['product']['id'] == $request['product_id'] ) {
								$cartData[$key]['count'] = ((int)$cartProduct['count'] + (int)$request['count']);
								$updated = true;
							}
						}
					}
					
					if ( !$updated ) {
						$cartData[] = [
							'product'  => $product,
							'count'    => 1,
							'category' => $category['name'],
//              'warehouse_id' => $request['warehouse_id'],
              'properties' => serialize($request['property'])
						];
					}
					
					Session::store(['cart' => $cartData]);
				}
				
				$message = 'Товар успешно добавлен в корзину';
				
				$response = [
					'success' => true,
					'message' => $message
				];
			}
			
			echo json_encode($response);
			return true;
		}
		
		public function actionSave()
		{
			$message = 'Ошибка сохранения корзины';
			$response = [
				'success' => false,
				'message' => $message
			];
			
			$request = $_POST;
			
			if ( isset($request['is_ajax']) && $request['is_ajax'] && isset($request['cart_data']) && $request['cart_data'] ) {
				$cartData = $request['cart_data'];
				
				Session::store(['cart' => $cartData]);
				
				$response = [
					'success' => true,
					'message' => 'Успешно сохранено'
				];
			}
			
			echo json_encode($response);
			return true;
		}
		
		public function actionRemove()
		{
			$message = 'Ошибка удаления товара из корзины';
			$request = $_POST;
			
			$response = [
				'success' => false,
				'message' => $message
			];
			
			if ( isset($request['is_ajax']) && $request['is_ajax'] ) {
				$cartData = Session::getByKey('cart');
				if ( $cartData ) {
					$newCartData = [];
					foreach ($cartData as $key => $cartProduct) {
						if ( $cartProduct['product']['id'] != $request['product_id'] ) {
							$newCartData[] = $cartProduct;
						}
					}

					Session::store(['cart' => $newCartData]);
				}

				$message = 'Товар успешно удален из корзины';
				
				$response = [
					'success' => true,
					'message' => $message
				];
			}
			
			echo json_encode($response);
			return true;
		}
		
		public function actionAddDiscount()
		{
			$request = $_POST;
			$response = [
				'success' => false,
				'message' => 'Ошибка добавления промокода'
			];

				$promoCode = $request['promo_code'];
				
				$discount = Discount::find(['title' => $promoCode, 'has_expired' => 0], true);
				
				if ( $discount && count($discount) > 0 ) {
					Session::store(['discount' => $discount]);
					$response = [
						'success'  => true,
						'message'  => 'Промокод успешно добавлен',
						'discount' => $discount
					];
				} else {
					$response = [
						'success' => false,
						'message' => 'Такого промокода не существует'
					];
				}
			
			echo json_encode($response);
			return true;
		}
		
		public function actionRemoveDiscount()
		{
			$request = $_POST;
			$response = [
				'success' => false,
				'message' => 'Ошибка добавления промокода'
			];
			
			if ( isset($request['is_ajax']) && $request['is_ajax'] ) {
				Session::remove('discount');
				
				$response = [
					'success' => true,
					'message' => 'Промокод успешно отменен'
				];
			}
			
			echo json_encode($response);
			return true;
		}
		
		public function getCartData()
		{
			$request = $_POST;
			$response = [
				'success' => false,
				'message' => 'Ошибка загрузки корзины'
			];
			
//			if ( isset($request['is_ajax']) && $request['is_ajax'] ) {
				$products = Session::getByKey('cart');
				$discount = Session::getByKey('discount');
				
				$response = [
					'success'  => true,
					'message'  => 'Данные загружены успешно',
					'products' => $products,
					'discount' => $discount
				];
//			}
			return $response;
			echo json_encode($response);
			return true;
		}

		public function actionLastDeleted($id){
      $products = Session::getByKey('cart');
      foreach ( $products as $index => $product ) {
        if($product['product']['id'] == $id){
          Session::store(['lastDelete' => $product]);
          unset($products[$index]);
        }
      }
      Session::store(['cart' => $products]);
      $this->json(true);
    }

    public function actionAddDeleted(){
		  $lastDeleted = Session::getByKey('lastDelete');
		  if($lastDeleted){
		    $cart = Session::getByKey('cart');
		    $cart[] = $lastDeleted;
		    Session::store(['cart' => $cart]);
		    $this->json(true);
      }
      $this->json(false);
    }

	}