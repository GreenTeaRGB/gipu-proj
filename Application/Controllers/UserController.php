<?php

namespace Application\Controllers;

use Application\Classes\Session;
use Application\Helpers\AppHelper;
use Application\Helpers\DataHelper;
use Application\Helpers\UserHelper;
use Application\Models\DeliveryAddress;
use Application\Models\JuristicData;
use Application\Models\JuristicType;
use Application\Models\Order;
use Application\Models\OrderProduct;
use Application\Models\PayCard;
use Application\Models\Product;
use Application\Models\Promotion;
use Application\Models\Rate;
use Application\Models\RequestOutput;
use Application\Models\SellerType;
use Application\Models\Shop;
use Application\Models\User;
use Application\Models\Warehouse;
use Application\Models\WarehouseStatus;

class UserController extends \Application\Classes\ControllerBase
{
  const DEFAULT_LOGIN_USER_MESSAGE        = 'Чтобы получить доступ, войдите в аккаунт';
  const DEFAULT_VERIFICATION_USER_MESSAGE = 'Чтобы получить доступ, пройдите верификацию';

  public function actionIndex()
  {
    $user = UserHelper::getUser();

    if ( $user ) {
      if ( !UserHelper::isAdmin() ) {
        $verified = UserHelper::userIsVerified();
        switch ( $user['type_code'] ) {
          case 'buyer':

            $this->render(
              '/buyer/dashboard', [
                'user' => $user
              ]
            );

            break;
          case 'seller':

            if ( !$verified ) {
              $this->render(
                '/seller/unverified', [
                  'user'           => $user,
                  'seller_types'   => SellerType::getSellerTypes(),
                  'juristic_types' => JuristicType::getJuristicTypes()
                ]
              );
            } else {
              $juristicModel = new JuristicData();
              $juristicData  = $juristicModel->where( [ 'id' => $user['juristic_data_id'] ] )->fetchOne();
              self::setModule( 'cabinet' );
              $this->render(
                '/seller/dashboard', [
                  'user'          => $user,
                  'juristic_data' => $juristicData
                ]
              );
            }

            break;
          case 'supplier':

            if ( !$verified ) {
              $this->render(
                '/supplier/unverified', [
                  'user'           => $user,
                  'juristic_types' => JuristicType::getJuristicTypes()
                ]
              );
            } else {
              //                $juristicModel   = new JuristicData();
              //                $juristicData    = $juristicModel->where( [ 'id' => $user['juristic_data_id'] ] )->fetchOne();
              //                $popularProducts = Product::getPopularProductMonth();
              //                $warehouses      = Warehouse::getWarehouses();
              $this->render(
                '/supplier/dashboard', [
                  //                    'user'            => $user,
                  //                    'juristic_data'   => $juristicData,
                  //                    'popularProducts' => $popularProducts,
                  //                    'warehouses'      => $warehouses,
                ]
              );
            }

            break;
          default:
            break;
        }
      } else {
        $this->redirect( '/admin' );
      }
    } else {
      $this->login();
    }

    return true;
  }

  public function actionShowLogin()
  {
    $user = UserHelper::getUser();

    if ( $user ) {
      $this->dashboard();
    }
    $use = new User();
    $use->setScenario( 'login' );
    $form = $use->form( [ 'login', 'password', 'submit' ] )
                ->action( '/user/auth' )
                ->setLinks(
                  [
                    [
                      'name' => 'Забыли пароль?',
                      'url'  => '/forgot_password',
                      'attr' => [ 'class' => 'forgot' ]
                    ]
                  ]
                )
                ->setAttrForm( [ 'class' => 'registration__form login__form' ] )
                ->createForm();
    //			echo $form;
    $this->render( 'login', [ 'form' => $form ] );

    return true;
  }

  public function actionShowRegister()
  {

    $user = UserHelper::getUser();
    if ( $user ) {
      $this->dashboard();
    }
    $userTypes = UserHelper::getUserTypes();
    $regions   = DataHelper::getRegions();
    $use       = new User();
    //			$sup = new Supplier();
    //
    //			if($use->form([],'registerForm')->isSubmit() && $use->load($_POST) && $use->validate() && $sup->load($_POST) && $sup->validate())
    //			{
    //				$userId = User::create($_POST['User']);
    //				if ( $userId = $use->created() ) {
    //					$_POST['User']['id'] = $userId;
    //					Session::setSessionForUser($_POST['User']);
    //					$this->>dashboard();
    //					return true;
    //				} else {
    //					/** todo show errors */
    //					/* user with that phone or email already exists */
    //				}
    //			}
    //			else{
    //
    //			}


    $use->setScenario( 'register' );

    //$form = $use->form('registerForm')

    $form = $use->form( [], 'registerForm' )->ajax()/*->merge($sup->form())*/
                ->action( '/user/registration' )
                ->setAttrForm( [ 'class' => 'registration__form' ] )
                ->createForm();
    $this->render(
      'register', [
        'user_types' => $userTypes,
        'regions'    => $regions,
        'form'       => $form
      ]
    );

    return true;
  }

  public function actionAuthentication()
  {
    $request = $_POST;

    $use = new User();
    $use->setScenario( 'login' );
    //			echo '<pre>';
    //			var_dump($use->form()->isSubmit() && $use->load($request) && $use->validate(), $use->getErrors());
    //			die;

    if ( $use->form()->isSubmit() && $use->load( $request ) && $use->validate() ) {
      $login          = $request['User']['login'];
      $password       = $request['User']['password'];
      $hashedPassword = UserHelper::hashPassword( $password );
      $user           = User::getUserByLoginAndPass( $login, $hashedPassword );

      if ( $user ) {
        Session::setSessionForUser( $user );
        $this->dashboard();

      } else {
        if ( !$use->hasErrors( 'login' ) )
          $use->addError( 'login', $use->getFirstError( 'password' ) );
        $use->clearErrors( 'password' );
        $this->login();
      }
      //				$this->>dashboard();
    } else {
      if ( !$use->hasErrors( 'login' ) )
        $use->addError( 'login', $use->getFirstError( 'password' ) );
      $use->clearErrors( 'password' );
      $this->login();
    }

    //			echo $use->form('login')->createForm();

    //			$request = $_POST;
    //
    //			if ( $request['login'] && $request['password'] ) {
    //				$login = trim(htmlspecialchars(strip_tags($request['login']), true), ' ');
    //				$password = trim(htmlspecialchars(strip_tags($request['password']), true), ' ');
    //				$hashedPassword = md5(DataHelper::SALT . $password);
    //
    //				$user = User::getUserByLoginAndPass($login, $hashedPassword);
    //
    //				if ( $user ) {
    //					Session::setSessionForUser($user);
    //					$this->>dashboard();
    //
    //				} else {
    //					$this->login();
    //				}
    //			}

    return true;
  }

  public function actionRegistration()
  {

    $user = UserHelper::getUser();

    if ( $user ) {
      $this->dashboard();
    }

    $request = $_POST['User'];

    //			foreach ($request as $key => $value) {
    //				$request[$key] = htmlspecialchars(strip_tags($value), true);
    //			}
    $user = new User();
    if ( $request['password'] == $request['password_repeat'] && $user->load( $_POST ) && $user->validate() ) {
      if ( isset( $request['organization_name'] ) && isset( $request['juristic_address'] ) && isset( $request['contact_name'] ) ) {
        $juristicModel  = new JuristicData();
        $juristicDataId = $juristicModel->setFields( $request )->insert();
        unset( $request['organization_name'] );
        unset( $request['juristic_address'] );
        unset( $request['contact_name'] );
      }

      if ( isset( $juristicDataId ) && $juristicDataId ) {
        $request['juristic_data_id'] = $juristicDataId;
      }

      $request['status_id']      = DataHelper::DEFAULT_USER_STATUS;
      $request['type']           = DataHelper::DEFAULT_SELLER_TYPE_ID;
      $request['referral_level'] = DataHelper::DEFAULT_REFERRAL_LEVEL;
      $userId                    = User::create( $request );
      if ( $userId ) {
        $request['id'] = $userId;
        Session::setSessionForUser( $request );
        /*Session::setSessionForUser($userId);*/
        die( json_encode( [ 'success' => true, 'redirect' => '/user/dashboard' ] ) );
        //					$this->>dashboard();

        return true;
      } else {
        //					AppHelper::setMessage('error', 'Пользователь с таким телефоном или Email уже существует');
      }
    } else {
      $errors = $user->getErrors();
      $user->clearErrors();
      $user->clearValidators();
      die( json_encode( [ 'error' => $errors ] ) );
      //				AppHelper::setMessage('error', 'Пароли не совпадают');
    }

    $this->redirect( '/user/register' );

    return true;
  }

  public function actionLogout()
  {
    Session::logout();
    $this->login();
    return true;
  }

  public function actionEdit()
  {
    $user = UserHelper::getUser();

    if ( $user ) {
      if ( !UserHelper::isAdmin() ) {
        $verified = UserHelper::userIsVerified();

        if ( !$verified ) {
          if ( $user['type'] != DataHelper::DEFAULT_BUYER_TYPE_ID ) {
            $this->dashboard();
          }
        }

        $this->render(
          '/edit', [
            'user' => $user
          ]
        );

      } else {
        $this->redirect( '/admin' );
      }
    } else {
      AppHelper::setMessage( 'error', self::DEFAULT_LOGIN_USER_MESSAGE );

      $this->login();
    }

    return true;
  }

  public function actionSave()
  {
    $request = $_POST;

    foreach ( $request as $key => $value ) {
      $request[$key] = htmlspecialchars( strip_tags( $value ), true );
    }

    $user = UserHelper::getUser();
    if ( $user ) {
      $model = new User();
      $done  = $model->where( [ 'id' => $user['id'] ] )->setFields( $request )->update();

      if ( $done ) {
        foreach ( $request as $key => $value ) {
          Session::UpdateUserSessionByKey( $key, $value );
        }

        AppHelper::setMessage( 'success', 'Вы успешно зарегестрированы' );

        $this->dashboard();
      } else {
        AppHelper::setMessage( 'error', 'Ошибка регистрации пользователя' );
      }
    } else {
      AppHelper::setMessage( 'error', self::DEFAULT_LOGIN_USER_MESSAGE );

      $this->login();
    }

    return true;
  }

  public function actionOrderHistory()
  {
    $user = UserHelper::getUser();

    if ( $user ) {
      $orderModel = new Order();
      $orders     = $orderModel->where(
        [
          'buyer_id'  => $user['id'],
          'parent_id' => DataHelper::DEFAULT_ORDER_PARENT_ID
        ]
      )->fetchAll();

      $this->render(
        $user['type_code'] . '/order_history', [
          'user'   => $user,
          'orders' => $orders
        ]
      );
    } else {
      AppHelper::setMessage( 'error', self::DEFAULT_LOGIN_USER_MESSAGE );

      $this->login();
    }

    return true;
  }

  public function actionOrderPreview( $orderId )
  {
    $user      = UserHelper::getUser();
    $orders    = Session::getByKey( 'user_orders_id' );
    $anonymous = false;
    if ( $orders && in_array( $orderId, $orders ) ) {
      $anonymous = true;
    }
    if ( $user || $anonymous ) {
//      $orders   = Order::getOrderWithChild( $orderId, $user['id'] );
      $where = [ 'id' => $orderId, 'buyer_id' => $anonymous ? 0 : $user['id'] ];
      $order = Order::findOne( $where );
      if ( !$order )
        $this->redirect( '/cart' );
      $products = $order['order_additional'];
//      $products = $orders['products'];
//      $address  = $orders['address'];
//      $orderIds = $orders['all_ids'];

//      unset( $orders['products'] );
//      unset( $orders['address'] );
//      unset( $orders['all_ids'] );

//      $ordersTree = AppHelper::buildTree( $orders );
      $showThanks = Session::getByKey( 'show_thanks' );
      $this->render(
        '/buyer/order_preview', [
          'user'        => $user,
          'show_thanks' => $showThanks,
          'products'    => $products,
          //          'order_ids'     => $orderIds,
          //          'order_address' => $address,
          //            'order'         => $ordersTree[DataHelper::DEFAULT_ORDER_INDEX]
          //          'order'         => array_shift( $ordersTree )
          'order'       => $order
        ]
      );
    } else {
      AppHelper::setMessage( 'error', self::DEFAULT_LOGIN_USER_MESSAGE );
      $this->login();
    }

    return true;
  }

  public function actionBalance()
  {
    $user = UserHelper::getUser();

    if ( $user ) {
      $bonuses = [];
      if ( $user['type'] == DataHelper::DEFAULT_BUYER_TYPE_ID ) {
        $this->render(
          $user['type_code'] . '/balance', [
            'user'    => $user,
            'bonuses' => $bonuses
          ]
        );
      } else {
        $verified = UserHelper::userIsVerified();

        if ( $verified ) {
          $this->render(
            '/balance', [
              'user'           => $user,
              'outputRequests' => RequestOutput::find( [ 'user_id' => $user['id'] ] ),
            ]
          );
        } else {
          AppHelper::setMessage( 'error', self::DEFAULT_VERIFICATION_USER_MESSAGE );
          $this->dashboard();
        }
      }
    } else {
      AppHelper::setMessage( 'error', self::DEFAULT_LOGIN_USER_MESSAGE );
      $this->login();
    }

    return true;
  }

  public function actionAddresses()
  {
    $user = UserHelper::getUser();

    if ( $user && $user['type'] == DataHelper::DEFAULT_BUYER_TYPE_ID ) {

      $addressModel = new DeliveryAddress();
      $addresses    = $addressModel->where( [ 'user_id' => $user['id'] ] )->fetchAll();

      $this->render(
        '/buyer/addresses', [
          'user'      => $user,
          'addresses' => $addresses
        ]
      );

    } else {
      AppHelper::setMessage( 'error', self::DEFAULT_LOGIN_USER_MESSAGE );

      $this->login();
    }

    return true;
  }

  public function actionAddAddress()
  {
    $user = UserHelper::getUser();

    if ( $user && $user['type'] == DataHelper::DEFAULT_BUYER_TYPE_ID ) {

      $this->render( 'buyer/address/edit' );
    } else {
      AppHelper::setMessage( 'error', self::DEFAULT_LOGIN_USER_MESSAGE );

      $this->login();
    }

    return true;
  }

  public function actionSaveAddress( $id = '' )
  {
    $request = $_POST;
    $user    = UserHelper::getUser();

    if ( $user && $user['type'] == DataHelper::DEFAULT_BUYER_TYPE_ID ) {
      if ( $id ) {
        $model   = new DeliveryAddress();
        $address = $model->where( [ 'id' => $id ] )->fetchOne();

        if ( $address && $address['user_id'] == $user['id'] ) {
          $done = $model->where( [ 'id' => $id ] )->setFields( $request )->update();

          if ( $done ) {
            /** ToDo send success msg */
          } else {
            /** ToDo show errors */
          }
        } else {
          $this->redirect( '/user/addresses' );
          /** ToDo show errors */
        }

      } else {
        $request['user_id'] = $user['id'];

        $model     = new DeliveryAddress();
        $addressId = $model->setFields( $request )->insert();

        if ( $addressId ) {
          AppHelper::setMessage( 'success', 'Адрес успешно сохранен' );
        } else {
          AppHelper::setMessage( 'error', 'Ошибка сохранения адреса' );
        }
      }

      $this->redirect( '/user/addresses' );

    } else {
      AppHelper::setMessage( 'error', self::DEFAULT_LOGIN_USER_MESSAGE );

      $this->login();
    }

    return true;
  }

  public function actionEditAddress( $id )
  {
    $user = UserHelper::getUser();

    if ( $id && $user && $user['type'] == DataHelper::DEFAULT_BUYER_TYPE_ID ) {
      $addressModel = new DeliveryAddress();
      $address      = $addressModel->where( [ 'id' => (int)$id ] )->fetchOne();

      if ( $address && $address['user_id'] == $user['id'] ) {
        $this->render(
          'buyer/address/edit', [
            'address' => $address
          ]
        );
      } else {
        AppHelper::setMessage( 'error', 'Такого адреса не существует' );

        $this->redirect( '/user/addresses' );
      }
    } else {
      AppHelper::setMessage( 'error', self::DEFAULT_LOGIN_USER_MESSAGE );

      $this->dashboard();
    }

    return true;
  }

  public function actionRemoveAddress( $id )
  {
    $user = UserHelper::getUser();

    if ( $id && $user && $user['type'] == DataHelper::DEFAULT_BUYER_TYPE_ID ) {
      $addressModel = new DeliveryAddress();
      $address      = $addressModel->where( [ 'id' => $id ] )->fetchOne();

      if ( $address && $address['user_id'] == $user['id'] ) {
        $addressModel->where( [ 'id' => (int)$id ] )->delete();

        AppHelper::setMessage( 'error', 'Адрес успешно удален' );
      } else {
        AppHelper::setMessage( 'error', 'Ошибка доступа' );
      }

      $this->redirect( '/user/addresses' );
    } else {
      AppHelper::setMessage( 'error', self::DEFAULT_LOGIN_USER_MESSAGE );

      $this->dashboard();
    }

    return true;
  }

  public function actionCards()
  {
    $user = UserHelper::getUser();

    if ( $user && $user['type'] == DataHelper::DEFAULT_SELLER_TYPE_ID && $user['seller_type'] == DataHelper::SELLER_PERSON_TYPE_ID ) {
      $cardModel = new PayCard();
      $cards     = $cardModel->where( [ 'user_id' => (int)$user['id'] ] )->fetchAll();

      $this->render(
        'seller/cards', [
          'cards' => $cards
        ]
      );
    } else {
      AppHelper::setMessage( 'error', self::DEFAULT_LOGIN_USER_MESSAGE );

      $this->login();
    }
  }

  public function actionAddCard()
  {
    $user = UserHelper::getUser();

    if ( $user && $user['type'] == DataHelper::DEFAULT_SELLER_TYPE_ID && $user['seller_type'] == DataHelper::SELLER_PERSON_TYPE_ID ) {

      $this->render( 'seller/card/edit' );
    } else {
      AppHelper::setMessage( 'error', self::DEFAULT_LOGIN_USER_MESSAGE );

      $this->login();
    }
  }

  public function actionEditCard( $id )
  {
    $user = UserHelper::getUser();

    if ( $id && $user && $user['type'] == DataHelper::DEFAULT_SELLER_TYPE_ID && $user['seller_type'] == DataHelper::SELLER_PERSON_TYPE_ID ) {
      $cardModel = new PayCard();
      $card      = $cardModel->where( [ 'id' => (int)$id ] )->fetchOne();

      if ( $card && $card['user_id'] == $user['id'] ) {
        $this->render(
          'seller/card/edit', [
            'card' => $card
          ]
        );
      } else {
        AppHelper::setMessage( 'error', 'Ошибка доступа' );

        $this->redirect( '/user/pay-cards' );
      }
    } else {
      AppHelper::setMessage( 'error', self::DEFAULT_LOGIN_USER_MESSAGE );

      $this->dashboard();
    }

    return true;
  }

  public function actionSaveCard( $id = '' )
  {
    $request = $_POST;
    $user    = UserHelper::getUser();

    if ( $user && $user['type'] == DataHelper::DEFAULT_SELLER_TYPE_ID && $user['seller_type'] == DataHelper::SELLER_PERSON_TYPE_ID ) {
      if ( $id ) {
        $model = new PayCard();
        $card  = $model->where( [ 'id' => $id ] )->fetchOne();

        if ( $card && $card['user_id'] == $user['id'] ) {
          $done = $model->where( [ 'id' => $id ] )->setFields( $request )->update();

          if ( $done ) {
            AppHelper::setMessage( 'success', 'Данные платежной карты успешно обновлены' );
          } else {
            AppHelper::setMessage( 'error', 'Ошибка сохранения платежной карты' );
          }
        } else {
          AppHelper::setMessage( 'error', 'Ошибка доступа' );
          $this->redirect( '/user/pay-cards' );
        }
      } else {
        $request['user_id'] = $user['id'];

        $model  = new PayCard();
        $cardId = $model->setFields( $request )->insert();

        if ( $cardId ) {
          AppHelper::setMessage( 'success', 'Платежная карта успешно сохранена' );
        } else {
          AppHelper::setMessage( 'error', 'Ошибка сохранения платежной карты' );
        }
      }

      $this->redirect( '/user/pay-cards' );

    } else {
      AppHelper::setMessage( 'error', self::DEFAULT_LOGIN_USER_MESSAGE );

      $this->login();
    }

    return true;
  }

  public function actionRemoveCard( $id )
  {
    $user = UserHelper::getUser();

    if ( $id && $user && $user['type'] == DataHelper::DEFAULT_SELLER_TYPE_ID && $user['seller_type'] == DataHelper::SELLER_PERSON_TYPE_ID ) {
      $cardModel = new PayCard();
      $card      = $cardModel->where( [ 'id' => (int)$id ] )->fetchOne();

      if ( $card && $card['user_id'] == $user['id'] ) {
        $cardModel->where( [ 'id' => (int)$id ] )->delete();

        AppHelper::setMessage( 'success', 'Платежная карта успешно удалена' );
      } else {
        AppHelper::setMessage( 'error', 'Ошибка удаления платежной карты' );
      }

      $this->redirect( '/user/pay-cards' );
    } else {
      AppHelper::setMessage( 'error', self::DEFAULT_LOGIN_USER_MESSAGE );

      $this->dashboard();
    }

    return true;
  }

  public function actionVerifySeller()
  {
    $request = $_POST;
    $user    = UserHelper::getUser();

    if ( $user ) {

      foreach ( $request as $key => $value ) {
        $request[$key] = htmlspecialchars( strip_tags( $value ), true );
      }

      if ( $request['seller_type'] == DataHelper::DEFAULT_SELLER_TYPE ) {
        $sellerType = $request['seller_type'];
        unset( $request['seller_type'] );

        /*if ( $_FILES && count($_FILES) > 0 ) {
          $files = FileUploadHelper::uploadFiles($_FILES, '/images/user/' . $user['id']);
          //var_dump($files);

        }*/
        //unset($_FILES['passport_data']);
        //echo '<pre>';
        //var_dump($_SESSION);

        $data = [
          'seller_type'   => $sellerType,
          'passport_data' => json_encode( $request ),
          'status_id'     => DataHelper::USER_WAITING_FOR_REVIEW
        ];

        $model = new User();
        $done  = $model->where( [ 'id' => $user['id'] ] )->setFields( $data )->update();

        if ( $done ) {
          foreach ( $data as $key => $value ) {
            Session::UpdateUserSessionByKey( $key, $value );
          }

          AppHelper::setMessage( 'success', 'Верификационные данные успешно сохранены' );

          $this->dashboard();
        } else {
          AppHelper::setMessage( 'error', 'Ошибка верификации пользователя' );
        }
      } else {
        $juristicModel  = new JuristicData();
        $juristicDataId = $juristicModel->setFields( $request )->insert();

        if ( $juristicDataId ) {
          $juristicTypeId = $request['juristic_type_id'];
          $sellerType     = $request['seller_type'];

          $data = [
            'seller_type'      => $sellerType,
            'status_id'        => DataHelper::USER_WAITING_FOR_REVIEW,
            'juristic_type_id' => $juristicTypeId,
            'juristic_data_id' => $juristicDataId
          ];

          $model = new User();
          $done  = $model->where( [ 'id' => $user['id'] ] )->setFields( $data )->update();
          if ( $done ) {
            foreach ( $data as $key => $value ) {
              Session::UpdateUserSessionByKey( $key, $value );
            }

            AppHelper::setMessage( 'success', 'Верификационные данные успешно сохранены' );
          } else {
            AppHelper::setMessage( 'error', 'Ошибка верификации' );
          }

        } else {
          AppHelper::setMessage( 'error', self::DEFAULT_VERIFICATION_USER_MESSAGE );
        }
      }

      $this->dashboard();
    } else {
      AppHelper::setMessage( 'error', self::DEFAULT_LOGIN_USER_MESSAGE );

      $this->login();
    }

    return true;
  }

  public function actionOrdersStatistic()
  {
    $user = UserHelper::getUser();

    if ( $user ) {
      $verified = UserHelper::userIsVerified();

      if ( $verified ) {

        $orders = Order::getOrdersWithProductsByOwner( $user['id'], $user['type'] == DataHelper::DEFAULT_SUPPLIER_TYPE_ID );

        $this->render(
          '/orders_statistic', [
            'user'   => $user,
            'orders' => $orders
          ]
        );
      } else {
        AppHelper::setMessage( 'error', self::DEFAULT_VERIFICATION_USER_MESSAGE );

        $this->dashboard();
      }

    } else {
      AppHelper::setMessage( 'error', self::DEFAULT_LOGIN_USER_MESSAGE );

      $this->login();
    }

    return true;
  }

  public function actionOrderStatistic( $orderId )
  {
    $user = UserHelper::getUser();

    if ( $user ) {
      $verified = UserHelper::userIsVerified();

      if ( $verified ) {
        $orderModel = new Order();

        $order = $orderModel->where( [ 'id' => $orderId ] )->fetchOne();
        $this->render(
          '/order_statistic', [
            'user'  => $user,
            'order' => $order
          ]
        );
      } else {
        AppHelper::setMessage( 'error', self::DEFAULT_VERIFICATION_USER_MESSAGE );

        $this->dashboard();
      }

    } else {
      AppHelper::setMessage( 'error', self::DEFAULT_LOGIN_USER_MESSAGE );

      $this->login();
    }

    return true;
  }

  public function actionSitesList()
  {
    $user = UserHelper::getUser();

    if ( $user ) {
      $verified = UserHelper::userIsVerified();

      if ( $verified ) {
        $sites = Shop::find( [ 'seller_id' => $user['id'] ] );
        $this->render(
          '/seller/sites_list', [
            'sites' => $sites
          ]
        );
      } else {
        AppHelper::setMessage( 'error', self::DEFAULT_VERIFICATION_USER_MESSAGE );

        $this->dashboard();
      }
    } else {
      AppHelper::setMessage( 'error', self::DEFAULT_LOGIN_USER_MESSAGE );

      $this->login();
    }

    return true;
  }

  public function actionReferralList()
  {
    $user = UserHelper::getUser();

    if ( $user ) {
      $verified = UserHelper::userIsVerified();

      if ( $verified ) {
        $referrals = User::find( [ 'referral_id' => $user['id'] ] );

        $this->render(
          '/seller/referral_list', [
            'referrals' => $referrals
          ]
        );
      } else {
        AppHelper::setMessage( 'error', self::DEFAULT_VERIFICATION_USER_MESSAGE );

        $this->dashboard();
      }

    } else {
      AppHelper::setMessage( 'error', self::DEFAULT_LOGIN_USER_MESSAGE );

      $this->login();
    }

    return true;
  }

  public function actionSupplierList()
  {
    $user = UserHelper::getUser();

    if ( $user ) {
      $verified = UserHelper::userIsVerified();

      if ( $verified ) {
        $suppliers = [];
        $this->render(
          '/seller/supplier_list', [
            'suppliers' => $suppliers
          ]
        );
      } else {
        AppHelper::setMessage( 'error', self::DEFAULT_VERIFICATION_USER_MESSAGE );

        $this->dashboard();
      }
    } else {
      AppHelper::setMessage( 'error', self::DEFAULT_LOGIN_USER_MESSAGE );

      $this->login();
    }

    return true;
  }

  public function actionVerifySupplier()
  {
    $request = $_POST;
    $user    = UserHelper::getUser();

    if ( $user ) {
      foreach ( $request as $key => $value ) {
        $request[$key] = htmlspecialchars( strip_tags( $value ), true );
      }

      $juristicModel = new JuristicData();
      $juristicSaved = $juristicModel->where( [ 'id' => $user['juristic_data_id'] ] )->setFields( $request )->update();
      if ( $juristicSaved ) {
        $model = new User();
        $done  = $model->where( [ 'id' => $user['id'] ] )->setFields(
          [
            'status_id' => DataHelper::USER_WAITING_FOR_REVIEW,
          ]
        )->update();
        if ( $done ) {
          AppHelper::setMessage( 'success', 'Верификационные данные успешно сохранены' );

          Session::UpdateUserSessionByKey( 'status_id', DataHelper::USER_WAITING_FOR_REVIEW );
        } else {
          AppHelper::setMessage( 'error', 'Ошибка верификации' );
        }
      }

      $this->dashboard();
    } else {
      AppHelper::setMessage( 'error', self::DEFAULT_LOGIN_USER_MESSAGE );

      $this->login();
    }

    return true;
  }

  public function actionPromotions()
  {
    $user = UserHelper::getUser();

    if ( $user ) {
      $verified = UserHelper::userIsVerified();

      if ( $verified ) {
        $promotionModel = new Promotion();
        $promotions     = $promotionModel->where( [ 'owner_id' => $user['id'] ] )->fetchAll();
        $this->render(
          '/supplier/promotions', [
            'promotions' => $promotions
          ]
        );
      } else {
        AppHelper::setMessage( 'error', self::DEFAULT_VERIFICATION_USER_MESSAGE );

        $this->dashboard();
      }
    } else {
      AppHelper::setMessage( 'error', self::DEFAULT_LOGIN_USER_MESSAGE );

      $this->login();
    }

    return true;
  }

  public function actionAddPromotion()
  {
    $user = UserHelper::getUser();

    if ( $user ) {
      $verified = UserHelper::userIsVerified();

      if ( $verified ) {
        $productModel    = new Product();
        $relatedProducts = $productModel->where(
          [
            'owner_id'  => $user['id'],
            'status_id' => DataHelper::PRODUCT_STATUS_ALLOWED
          ]
        )->fetchAll();

        $this->render(
          '/supplier/promotion/edit', [
            'related_products' => $relatedProducts
          ]
        );
      } else {
        AppHelper::setMessage( 'error', self::DEFAULT_VERIFICATION_USER_MESSAGE );

        $this->dashboard();
      }
    } else {
      AppHelper::setMessage( 'error', self::DEFAULT_LOGIN_USER_MESSAGE );

      $this->login();
    }

    return true;
  }

  public function actionEditPromotion( $id )
  {
    $user = UserHelper::getUser();

    if ( $user ) {
      $verified = UserHelper::userIsVerified();

      if ( $verified && $id ) {
        $promotionModel = new Promotion();

        $promotion = $promotionModel->where( [ 'id' => (int)$id ] )->fetchOne();

        $productModel    = new Product();
        $relatedProducts = $productModel->where(
          [
            'owner_id'  => $user['id'],
            'status_id' => DataHelper::PRODUCT_STATUS_ALLOWED
          ]
        )->fetchAll();

        if ( $promotion && $promotion['owner_id'] == $user['id'] ) {
          $this->render(
            '/supplier/promotion/edit', [
              'promotion'        => $promotion,
              'related_products' => $relatedProducts
            ]
          );
        } else {
          AppHelper::setMessage( 'error', 'Ошибка доступа' );

          $this->redirect( '/user/promotions' );
        }
      } else {
        AppHelper::setMessage( 'error', self::DEFAULT_VERIFICATION_USER_MESSAGE );

        $this->dashboard();
      }
    } else {
      AppHelper::setMessage( 'error', self::DEFAULT_LOGIN_USER_MESSAGE );

      $this->login();
    }
    return true;
  }

  public function actionSavePromotion( $id = '' )
  {
    $user = UserHelper::getUser();

    if ( $user ) {
      $verified = UserHelper::userIsVerified();

      if ( $verified ) {
        $request = $_POST;

        foreach ( $request as $key => $value ) {
          if ( !is_array( $value ) ) {
            $request[$key] = htmlspecialchars( strip_tags( $value ), true );
          }
        }

        $model = new Promotion();

        ( $id )
          ? $request['promotion_id'] = $id
          : '';
        $model->setTempRequest( $request );

        if ( $id ) {
          $done = $model->where( [ 'id' => (int)$id ] )->setFields( $request )->update();
        } else {
          $request['owner_id'] = $user['id'];
          $done                = $model->setFields( $request )->insert();
        }

        if ( $done ) {
          AppHelper::setMessage( 'success', 'Акция успешно сохранена' );

        } else {
          AppHelper::setMessage( 'error', 'Ошибка сохранения акции' );
        }

        $this->redirect( '/user/promotions' );
      } else {
        AppHelper::setMessage( 'error', self::DEFAULT_VERIFICATION_USER_MESSAGE );

        $this->dashboard();
      }
    } else {
      AppHelper::setMessage( 'error', self::DEFAULT_LOGIN_USER_MESSAGE );

      $this->login();
    }

    return true;
  }

  public function actionRemovePromotion( $id )
  {
    $user = UserHelper::getUser();

    if ( $user ) {
      $verified = UserHelper::userIsVerified();

      if ( $verified ) {
        $model = new Promotion();

        if ( $id ) {
          $model->setTempRequest( [ 'promotion_id' => $id ] );
          $done = $model->where( [ 'id' => (int)$id ] )->delete();

          if ( $done ) {
            AppHelper::setMessage( 'success', 'Акция успешно удалена' );
          } else {
            AppHelper::setMessage( 'error', 'Ошибка сохранения акции' );
          }
        } else {
          AppHelper::setMessage( 'error', 'Такой акции не существует' );

          $this->redirect( '/user/promotions' );
        }

        $this->redirect( '/user/promotions' );
      } else {
        AppHelper::setMessage( 'error', self::DEFAULT_VERIFICATION_USER_MESSAGE );

        $this->dashboard();
      }
    } else {
      AppHelper::setMessage( 'error', self::DEFAULT_LOGIN_USER_MESSAGE );

      $this->login();
    }

    return true;
  }

  public function actionWarehouseList()
  {
    $user = UserHelper::getUser();

    if ( $user ) {
      $verified = UserHelper::userIsVerified();

      if ( $verified ) {
        $warehouses = Warehouse::getWarehouses();
        $this->render(
          '/supplier/warehouse_list', [
            'warehouses' => $warehouses
          ]
        );
      } else {
        AppHelper::setMessage( 'error', self::DEFAULT_VERIFICATION_USER_MESSAGE );

        $this->dashboard();
      }
    } else {
      AppHelper::setMessage( 'error', self::DEFAULT_LOGIN_USER_MESSAGE );

      $this->login();
    }
  }

  public function actionAddWarehouse()
  {
    $user = UserHelper::getUser();

    if ( $user ) {
      $verified = UserHelper::userIsVerified();

      if ( $verified ) {
        $this->render(
          '/supplier/warehouse/edit', [
            'warehouse_statuses' => WarehouseStatus::getWarehouseStatuses()
          ]
        );
      } else {
        AppHelper::setMessage( 'error', self::DEFAULT_VERIFICATION_USER_MESSAGE );

        $this->dashboard();
      }
    } else {
      AppHelper::setMessage( 'error', self::DEFAULT_LOGIN_USER_MESSAGE );

      $this->login();
    }
  }

  public function actionEditWarehouse( $id )
  {
    $user = UserHelper::getUser();

    if ( $user ) {
      $verified = UserHelper::userIsVerified();

      if ( $verified ) {
        $warehouse = Warehouse::getWarehouseById( $id );

        if ( $warehouse ) {
          $this->render(
            '/supplier/warehouse/edit', [
              'warehouse'          => $warehouse,
              'warehouse_statuses' => WarehouseStatus::getWarehouseStatuses()
            ]
          );
        } else {
          AppHelper::setMessage( 'error', 'Такого склада не существует' );

          $this->redirect( '/user/warehouse-list' );
        }
      } else {
        AppHelper::setMessage( 'error', self::DEFAULT_VERIFICATION_USER_MESSAGE );

        $this->dashboard();
      }
    } else {
      AppHelper::setMessage( 'error', self::DEFAULT_LOGIN_USER_MESSAGE );

      $this->login();
    }

    return true;
  }

  public function actionSaveWarehouse( $id = '' )
  {
    $user = UserHelper::getUser();

    if ( $user ) {
      $verified = UserHelper::userIsVerified();

      if ( $verified ) {
        $request = $_POST;

        foreach ( $request as $key => $value ) {
          $request[$key] = htmlspecialchars( strip_tags( $value ), true );
        }

        $model = new Warehouse();

        if ( $id ) {
          $done = $model->where( [ 'id' => (int)$id ] )->setFields( $request )->update();
        } else {
          $request['owner_id'] = $user['id'];
          $done                = $model->setFields( $request )->insert();
        }

        if ( $done ) {
          AppHelper::setMessage( 'success', 'Склад успешно сохранен' );
        } else {
          AppHelper::setMessage( 'error', 'Ошибка сохранения склада' );
        }

        $this->redirect( '/user/warehouse-list' );
      } else {
        AppHelper::setMessage( 'error', self::DEFAULT_VERIFICATION_USER_MESSAGE );

        $this->dashboard();
      }
    } else {
      AppHelper::setMessage( 'error', self::DEFAULT_LOGIN_USER_MESSAGE );

      $this->login();
    }

    return true;
  }

  public function actionShowWarehouse( $id )
  {
    $user = UserHelper::getUser();

    if ( $user ) {
      $verified = UserHelper::userIsVerified();

      if ( $verified ) {
        $warehouse = Warehouse::getWarehouseById( $id );
        $products  = Product::getProductsByOwnerIdAndWarehouseId( $user['id'], $warehouse['id'] );

        Session::store( [ 'warehouseId' => $warehouse['id'] ] );

        $this->render(
          '/supplier/warehouse/show', [
            'warehouse' => $warehouse,
            'products'  => $products
          ]
        );
      } else {
        AppHelper::setMessage( 'error', self::DEFAULT_VERIFICATION_USER_MESSAGE );

        $this->dashboard();
      }
    } else {
      AppHelper::setMessage( 'error', self::DEFAULT_LOGIN_USER_MESSAGE );

      $this->login();
    }
    return true;
  }

  public function actionOutputBalance()
  {
    $user = UserHelper::getUser();
    if ( $user ) {
      $verified = UserHelper::userIsVerified();
      if ( $verified ) {
        $output = new RequestOutput();
        $form   = $output->form( [ 'sum' ] )->action( '/user/balance/output/save' )->createForm();
        return $this->render(
          'outputBalance',
          [
            'form' => $form,
            'user' => $user,
          ]
        );
      } else {
        AppHelper::setMessage( 'error', self::DEFAULT_VERIFICATION_USER_MESSAGE );
        $this->dashboard();
      }
    }
  }

  public function actionSaveOutputBalance()
  {
    $user = UserHelper::getUser();
    if ( !$user )
      $this->login();
    $verified = UserHelper::userIsVerified();
    if ( !$verified )
      $this->dashboard();
    $output = new RequestOutput();
    $output->setAttributes( [ 'user_id' => $user['id'], 'status_id' => DataHelper::DEFAULT_REQUEST_OUTPUT_STATUS ] );
    if ( $output->load( $_POST ) && $output->validate() ) {
      $output->created();
    }
    $this->redirect( '/user/balance/output' );
  }

  public function actionCreateSite( $rate = 0 )
  {
    if ( $rate !== 0 )
      $rateElement = Rate::findOne( $rate );
    if ( $rate == 0 || !$rateElement ) {
      return $this->render(
        '/seller/site/create', [
          'rate' => Rate::find( [ 'active' => 1 ] ),
        ]
      );
    }
    $shop                 = new Shop();
    $shop::$maxCategories = $rateElement['count_categories'];
    $shop->rate           = $rate;
    if ( $shop->form()->isSubmit() && $shop->load( $_POST ) && $shop->validate() ) {
      if ( $id = $shop->created() ) {
        return $this->redirect( '/user/seller/site/paid-site/' . $id );
      }
    }
    $form = $shop->form( [ 'name', 'url', 'rate', 'categories' ] )->createForm();
    return $this->render(
      '/seller/site/createForm', [
        'form' => $form
      ]
    );

  }

  public function actionPaidSite( $id )
  {
    return $this->render( '/seller/site/paid-site', [ 'id' => $id ] );
  }

  public function actionPaid( $id )
  {
    $shop = Shop::findOne( $id );
    if ( !$shop )
      $this->dashboard();
    $user = UserHelper::getUser();
    if ( !$user || $user['id'] != $shop['seller_id'] )
      $this->dashboard();
    $rate = Rate::findOne( $shop['rate'] );
    if ( !$rate )
      $this->dashboard();
   
    User::regProfit( $user['id'], $rate['price'] );
    if ( Shop::updateById( $id, [ 'paid' => 1 ] ) ) {
      return $this->redirect( '/user/seller/site/' . $id );
    } else {
      return $this->redirect( '/user/seller/site/paid-site/' . $id );
    }
  }

  public function actionSite( $id )
  {
    $shop = Shop::findData( $id );
    if ( !$shop )
      return $this->redirect( '/user/sites-list' );
    if ( $shop['paid'] != 1 )
      return $this->redirect( '/user/seller/site/paid-site/' . $shop['id'] );

    return $this->render(
      'seller/site/site', [
        'shop' => $shop
      ]
    );
  }

  public function actionSiteEdit( $id )
  {
    $shop = new Shop();
    $rate = Rate::find();
    $post = isset( $_POST[$shop->formName()] )
      ? $_POST[$shop->formName()]
      : [];
    if ( $shop->form()->isSubmit() ) {
      if ( isset( $post['rate'] ) ) {
        foreach ( $rate as $item ) {
          if ( $item['id'] == $post['rate'] ) {
            $shop::$maxCategories = $item['count_categories'];
          }
        }
      }
      $shop->clearValidators();
      if ( $shop->load( $_POST ) && $shop->validate() ) {
        $shop::updateById( $id, $post );
        return $this->redirect( '/user/seller/site/' . $id );
      }
    }
    $data = $shop::findData( $id );
    $shop->clearValidators();
    $data['categories'] = array_column( $data['categories'], 'id' );
    foreach ( $rate as $item ) {
      if ( $item['id'] == $data['rate'] ) {
        $shop::$maxCategories = $item['count_categories'];
      }
    }
    if ( isset( $post ) )
      $data = array_merge( $data, $post );
    $shop->load( $data, '' );
    $form = $shop->form( [ 'name', 'url', 'rate_id', 'rate', 'categories' ] )->createForm();
    return $this->render(
      '/seller/site/edit', [
        'form' => $form,
        'id'   => $id
      ]
    );
  }

  public function actionConstruct( $id )
  {
    self::setModule( 'cabinet' );
    $shop = Shop::findInfo( $id );
    if ( !$shop )
      return false;
//    $config = $this->getConfig( 'config_sites/' . $id );
//    if ( $config === false )
//      $config = $this->getConfig( 'config_sites/default' );
    return $this->render(
      '/seller/site/construct', [
        'config' => $shop
      ]
    );
  }

  public function actionConfigsConstruct( $template )
  {
    $path = 'template_sites/' . $template;
    if ( !file_exists( FOLDER . $path ) )
      return false;
    $dirs      = scandir( FOLDER . $path );
    $templates = [];
    foreach ( $dirs as $dir ) {
      if ( $dir == '.' || $dir == '..' )
        continue;
      $templates[] = $this->getConfig( $path . '/' . $dir . '/config' );
    }
    echo json_encode( $templates );
    return true;
  }

  public function actionTemplatesConstruct( $template, $code )
  {
    $path = FOLDER . 'template_sites/' . $template . '/' . $code . '/index.php';
    if ( !file_exists( $path ) )
      return false;
    echo file_get_contents( $path );
    return true;
  }


}