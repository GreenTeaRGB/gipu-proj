<?php

namespace Application\Controllers;

use \Application\Classes\AdminBuilder;
use \Application\Models\Courier;
use Application\Models\User;

class AdminCourierController extends \Application\Classes\AdminBase
{

  public function actionIndex( $page = 1 )
  {
    $builder = new AdminBuilder( new Courier(), [], [ 'hide' => [ 'edit', 'view' ] ] );
    $builder->merge( new class extends User
    {
      public function getFields()
      {
        $fields = parent::getFields();
        foreach ( $fields as $field => $settings ) {
          if ( $field != 'name' )
            $fields[$field]['visible'] = false;
        }
        $fields['login'] = [
          'type'         => 'read',
          'visible'      => true,
          'edited'       => false,
          'label'        => 'Логин',
          'sort'         => 3,
          'visible_keys' => [ 'User[email]', 'User[phone]' ]
        ];
        $fields['edit']  = [
          'type'    => 'string',
          'visible' => true,
          'label'   => 'Изменить',
          'sort'    => 1000
        ];
        return $fields;
      }
    } );
    $builder->setView( 'index' );
    $builder->index( $page );
    $items = $builder->getItems();
    foreach ( $items as $id => $item ) {
      $items[$id]['User[edit]'] = '<a href="/admin/courier/edit/' . $item['id'] . '">Изменить</a>';
    }
    $builder->setItems( $items );
    $builder->render();
    return true;
  }

  public function actionCreate()
  {
    $builder = new AdminBuilder( new class extends Courier
    {
      public function getFields()
      {
        $fields = parent::getFields();
        foreach ( $fields as $field => $settings ) {
          $fields[$field]['edited'] = false;
        }
        $fields['car_type']['edited']      = true;
        $fields['mark']['edited']          = true;
        $fields['license_plate']['edited'] = true;
        return $fields;
      }
    } );
    $builder->merge( new User() );
//    $builder->merge(new class extends Courier {
//      public function getFields(){
//        $fields = parent::getFields();
//        foreach ( $fields as $field => $settings ) {
//            $fields[$field]['visible'] = false;
//        }
//        $fields['car_type']['visible'] = true;
//        $fields['mark']['visible'] = true;
//        $fields['license_plate']['visible'] = true;
//        return $fields;
//      }
//    });
//    $builder->setView( 'create' );
    $builder->create();
//    if($id = $builder->getResult()){
//      Courier::create(['user_id' => $id]);
//    }
//    $builder->render();
    return true;
  }

  public function actionEdit( $id )
  {

    $courier = Courier::findOne($id );
    $courierClass = new class extends Courier
    {
      public function getFields()
      {
        $fields = parent::getFields();
        foreach ( $fields as $field => $settings ) {
          $fields[$field]['edited'] = false;
        }
        $fields['car_type']['edited']      = true;
        $fields['mark']['edited']          = true;
        $fields['license_plate']['edited'] = true;
        return $fields;
      }
    };
    $courierClass->user_id = $courier['user_id'];
    $builder  = new AdminBuilder( $courierClass );
    $user     = new User();
    $user->id = $courier['user_id'];
    $builder->merge( $user );
//    $builder = new AdminBuilder( new Courier() );
//    $builder->merge(new class extends User {
//      public function getFields(){
//        $fields = parent::getFields();
//        foreach ( $fields as $field => $settings ) {
//          $fields[$field]['visible'] = false;
//        }
//        return $fields;
//      }
//    });
    $builder->edit( $id );
    return true;
  }

  public function actionActivate()
  {
    $builder = new AdminBuilder( new Courier() );
    $builder->active();
    return true;
  }

  public function actionSortable()
  {
    $builder = new AdminBuilder( new Courier() );
    $builder->sort();
    return true;
  }

  public function actionDelete()
  {
    $builder = new AdminBuilder( new Courier() );
    $builder->delete();
    return true;
  }

  public function actionModal()
  {
    $builder = new AdminBuilder( new Courier() );
    $builder->modal();
    return true;
  }

}

?>