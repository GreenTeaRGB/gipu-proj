<?php
	
	namespace Application\Controllers;
	
	use \Application\Classes\AdminBuilder;
	use \Application\Models\SupportQuestion;
	
	class AdminSupportQuestionController extends \Application\Classes\AdminBase
	{
		
		public function actionIndex($page = 1)
		{
			$builder = new AdminBuilder(new class extends SupportQuestion {
			  public function getFields(){
			    $fields = parent::getFields();
          $fields['answer_email'] = [
            'type' => 'string',
            'visible' => true,
            'label' => 'Ответить',
            'sort' => 10000
          ];
			    return $fields;
        }
      }, [], ['hide' => ['delete', 'view', 'edit' ] ]);
			$builder->setView('index');
			$builder->index($page);
			$items = $builder->getItems();
      foreach ( $items as $key => $item ) {
        $items[$key]['answer_email'] = '<a href="mailto:'.$item['email'].'">Ответить</a>';
			}
			$builder->setItems($items);
			$builder->render();
			return true;
		}
		
		public function actionCreate()
		{
			$builder = new AdminBuilder(new SupportQuestion());
			$builder->create();
			return true;
		}
		
		public function actionEdit($id)
		{
			$builder = new AdminBuilder(new SupportQuestion());
			$builder->edit($id);
			return true;
		}
		
		public function actionActivate()
		{
			$builder = new AdminBuilder(new SupportQuestion());
			$builder->active();
		}
		
		public function actionSortable()
		{
			$builder = new AdminBuilder(new SupportQuestion());
			$builder->sort();
		}
		
		public function actionDelete()
		{
			$builder = new AdminBuilder(new SupportQuestion());
			$builder->delete();
		}
		
		
	}
	
	?>