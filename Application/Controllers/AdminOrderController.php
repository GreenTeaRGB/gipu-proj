<?php

namespace Application\Controllers;

use Application\Classes\AdminBuilder;
use Application\Models\Order;
use Application\Models\User;
use Application\Models\OrderProduct;
use Application\Models\Properties;
use DateTime;
use function strtotime;

/**
 * Class AdminOrderController
 * @package Application\Controllers
 */
class AdminOrderController extends \Application\Classes\AdminBase
{

  /**
   * @param int $page
   * @return bool
   */
  public function actionIndex( $page = 1 )
  {
    $builder = new AdminBuilder( new CLass extends Order
    {
      public function getFields()
      {
        $fields               = parent::getFields();
        $fields['product_id'] = [
          'type'    => 'string',
          'visible' => 'true',
          'label'   => 'ID товара',
          'sort'    => 20
        ];
        $fields['process']    = [
          'type'    => 'string',
          'visible' => true,
          'sort'    => 1100,
          'label'   => 'Обработать'
        ];
        return $fields;
      }
    }, [], [ 'hide' => ['delete', 'view', 'edit' ]  ] );
    $builder->setWhere( [ 'internal_account' => 0, 'confirm_internal' => 0 ] );
    $builder->setOrWhere( [ 'internal_account' => 1, 'confirm_internal' => 1 ] );
    $builder->setView( 'index' );
    $builder->index( $page );
    $orders        = $builder->getItems();
    $ids           = array_column( $orders, 'id' );
    $productIdSort = [];
    $product_id    = OrderProduct::find( [ 'order_id' => $ids ] );
    foreach ( $product_id as $item ) {
      $productIdSort[$item['order_id']][] = $item['product_id'];
    }
    foreach ( $orders as $index => $order ) {
      $orders[$index]['product_id'] = implode( ',', $productIdSort[$order['id']] );
      $orders[$index]['process']    = '<a href="/admin/order/process/' . $order['id'] . '">Обработать</a>';
    }
    $builder->setItems( $orders );
    $builder->render();
    return true;
  }

  /**
   * @return bool
   */
  public function actionCreate()
  {
    $builder = new AdminBuilder( new Order() );
    $builder->create();
    return true;
  }

  /**
   * @param $id
   * @return bool
   */
  public function actionEdit( $id )
  {
    $builder = new AdminBuilder( new Order() );
//    $errors  = [];
//    if ( isset( $_POST['submit'] ) && isset( $_POST['count'] ) && is_array( $_POST['count'] ) ) {
//      $total_price = 0;
//      foreach ( $_POST['count'] as $product_id => $count ) {
//        if ( $count <= 0 || $count == '' ) {
//          $errors['count'] = 'Количество не может меньше 1';
//          $builder->setErrorMessage( 'Количество не может меньше 1' );
//          $builder->setErrors( [ 'count' => 'Количество не может меньше 1' ] );
//          break;
//        }
//        OrderProduct::updateByFilter( [ 'order_id' => $id, 'product_id' => $product_id ], [
//          'count'      => $count,
//          'properties' => isset( $_POST['property'][$product_id] )
//            ? $_POST['property'][$product_id]
//            : []
//        ] );
//        $total_price += ( $count * $_POST['_price_'][$product_id] );
//      }
//      $_POST['total'] = $total_price;
//    }
    $builder->setView( 'edit' );
    $builder->edit( $id );
    $item = $builder->getItem();
    $builder->setDetailTables( [
      'name'    => 'Товары',
      'content' => $this->render( 'products', [
        'products'        => $item['order_additional'],
        'property_values' => $item['property_values'],
        'properties'      => Properties::find( [ 'type' => 'L', 'main' => 1 ] )
      ], true )
    ] );
    $builder->render();
    return true;
  }

  /**
   * @return bool
   */
  public function actionActivate()
  {
    $builder = new AdminBuilder( new Order() );
    $builder->active();
    return true;
  }

  /**
   * @return bool
   */
  public function actionSortable()
  {
    $builder = new AdminBuilder( new Order() );
    $builder->sort();
    return true;
  }

  /**
   * @return bool
   */
  public function actionDelete()
  {
    $builder = new AdminBuilder( new Order() );
    $builder->delete();
    return true;
  }

  /**
   * @return bool
   */
  public function actionModal()
  {
    $builder = new AdminBuilder( new Order() );
    $builder->modal();
    return true;
  }

  public function actionProcess( $id )
  {
    $order = Order::findOne( $id );
    if ( !$order )
      $this->error404();
    $supplier = User::findOne( $order['supplier_id'] );
    return $this->render( 'process', [ 'order'      => $order,
                                       'supplier'   => $supplier,
                                       'title_page' => 'Обработка заказа'
    ] );
  }

  public function actionChangeDelivery($id){
   if(!isset($_POST['date']))
     $this->json(false);
    Order::updateById($id, ['delivery_date' => $_POST['date']]);
  }
}

?>