<?php
namespace Application\Controllers;
use \Application\Classes\AdminBuilder;
use \Application\Models\Shop;

class AdminShopController extends \Application\Classes\AdminBase
{

	public function actionIndex($page = 1)
	{
		$builder = new AdminBuilder(new Shop(), [] );
		$builder->index($page);
		return true;
	}

	public function actionCreate()
	{
		$builder = new AdminBuilder(new Shop());
		$builder->create();
		return true;
	}

	public function actionEdit($id)
	{
	  if(isset($_POST['rate']))
	  switch($_POST['rate']){
      case 1 : Shop::$maxCategories = 1; break;
      case 2 : Shop::$maxCategories = 2; break;
      case 3 : Shop::$maxCategories = 3; break;
      default : Shop::$maxCategories = 1;
    }
		$builder = new AdminBuilder(new Shop());
		$builder->edit($id);
		return true;
	}
	
	public function actionActivate(){
		$builder = new AdminBuilder(new Shop());
		$builder->active();
	}
	
	public function actionSortable(){
		$builder = new AdminBuilder(new Shop());
		$builder->sort();
	}
	public function actionDelete(){
		$builder = new AdminBuilder(new Shop());
		$builder->delete();
	}
	
	
}

?>




























