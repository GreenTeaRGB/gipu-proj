<?php
	
	namespace Application\Controllers;
	
	use \Application\Models\Review;
	use \Application\Models\OrderProduct;
	use \Application\Helpers\UserHelper;
	use \Application\Helpers\AppHelper;
	
	class ReviewController extends \Application\Classes\ControllerBase
	{
		public function actionIndex()
		{
			return true;
		}
		
		public function actionAdd()
		{
			$request = $_POST;
			$response = [
				'success' => false,
				'message' => 'Ошибка добавления отзыва'
			];
			
			$user = UserHelper::getUser();
			
			if ( isset($request['is_ajax']) && $request['is_ajax'] && $user && $request['product_id'] ) {
				$order = OrderProduct::find(['buyer_id' => $user['id'], 'product_id' => $request['product_id']], true);
				
				if ( $order && count($order) > 0 ) {
					$request['buyer_id'] = $user['id'];
					$request['updater_id'] = $user['id'];
					$request['order_id'] = $order['order_id'];
					
					$model = new Review();
					
					if ( $model->load($request, '') && $model->validate() ) {
						$reviewId = $model->created();
						if ( $reviewId ) {
							$response = [
								'success' => true,
								'message' => 'Отзыв сохранен. Отображение на сайте после модерации',
							];
							AppHelper::setMessage( 'success', 'Отзыв сохранен. Отображение на сайте после модерации' );
						}
					} else {
						$errors = $model->getErrors();
						$response = [
							'success' => false,
							'message' => 'Ошибка добавления отзыва',
							'errors'  => $errors
						];
						AppHelper::setMessage( 'error', $errors);
					}
				} else {
					$response = [
						'success' => false,
						'message' => 'Оставлять коментарии к товару могут пользователи заказавшие данный товар'
					];
					AppHelper::setMessage( 'error', 'Оставлять коментарии к товару могут пользователи заказавшие данный товар');
				}
			}
			
			echo json_encode($response);
			return true;
		}
	}