<?php
	
	namespace Application\Controllers;
	
	use \Application\Classes\Session;
	use \Application\Helpers\UserHelper;
	use \Application\Helpers\AppHelper;
	use \Application\Models\Product;
	use \Application\Models\Category;
	use \Application\Models\Properties;
	
	class CompareController extends \Application\Classes\ControllerBase
	{
		const DEFAULT_COUNT_PRODUCTS_FOR_COMPARE = 4;
		
		public function actionIndex($part = '')
		{
		  $part = urldecode($part);
//			$user = UserHelper::getUser();
			//Session::remove('compare');
//			if ( $user && isset($user['id']) ) {
				$itemsToCompare = Session::getByKey('compare');
				if ( $itemsToCompare && count($itemsToCompare) > 0 ) {

					$compareCategories = array_keys($itemsToCompare);
					if ( !$part ) {
						$defaultCategory = key($itemsToCompare);
						$itemsByCategory = reset($itemsToCompare);
					} else {
						$defaultCategory = $part;
						$itemsByCategory = $itemsToCompare[$defaultCategory];
					}
					return $this->render(
						'index',
						[
							'categories'       => $compareCategories,
							'default_category' => $defaultCategory,
							'items'            => $itemsByCategory,
              'properties' => $this->GetCategoryProperties($defaultCategory)
						]
					);
				} else {
					return $this->render(
						'index',
						[
							'no_items' => true
						]
					);
				}
//			} else {
//				AppHelper::setMessage('error', 'Чтобы добавить в список сравнения, войдите в аккаунт');
//
//				$this->redirect('/user/login');
//			}
		}
		
		public function actionAdd()
		{
			$request = $_POST;
			$response = [
				'success' => false,
				'message' => 'Ошибка добавления товара в список сравнения'
			];
			
//			$user = UserHelper::getUser();
//
//			if ( isset($request['is_ajax']) && $request['is_ajax'] && $request['product_id'] && $user['id'] ) {
				$compareData = Session::getByKey('compare');
				$product = Product::find(['id' => $request['product_id']], true);
				
				$productPriceWithCommission = AppHelper::calculatePriceWithCommission($product['price'], $product['commission']);
				$product['full_price'] = $productPriceWithCommission;
				
				$category = Category::findOne($product['category_id']);
				if ( !$compareData ) {
					$compareData[$category['name']][] = $product;
					Session::store(['compare' => $compareData]);
					
					$response = [
						'success' => true,
            'delete' => false,
						'message' => 'Товар успешно добавлен в список сравнения'
					];
					
				} else {
          $exists = false;
          foreach ($compareData[$category['name']] as $key => $compareProduct) {
            if ( $compareProduct['id'] == $request['product_id'] ) {
              $exists = true;
              unset($compareData[$category['name']][$key]);
              $response = [
                'success' => true,
                'delete' => true,
                //									'message' => 'Товар уже добавлен в список сравнения'
                'message' => 'Товар успешно удален из сравнения'
              ];
              break;
            }
          }
					if ( count($compareData[$category['name']]) < self::DEFAULT_COUNT_PRODUCTS_FOR_COMPARE ) {
						if ( !$exists ) {
							$compareData[$category['name']][] = $product;
							$response = [
								'success' => true,
                'delete' => false,
								'message' => 'Товар успешно добавлен в список сравнения'
							];
						}
						Session::store(['compare' => $compareData]);
					} else {
						$response = [
							'success' => false,
							'message' => 'Максимальное количество товаров для сравнения - ' . self::DEFAULT_COUNT_PRODUCTS_FOR_COMPARE
						];
					}
//				}
			}
			
			echo json_encode($response);
			return true;
		}
		
		public function actionRemoveItem()
		{
			$request = $_POST;
			$response = [
				'success' => false,
				'message' => 'Ошибка удаления товара из списка сравнения'
			];
			
			if ( isset($request['is_ajax']) && $request['is_ajax'] && $request['product_id'] && $request['category_name'] ) {
				$compareData = Session::getByKey('compare');
				
				$removeCategory = false;
				
				foreach ($compareData as $categoryName => $compareProducts) {
					if ( $categoryName != $request['category_name'] ) {
						continue;
					} else {
						foreach ($compareProducts as $productKey => $compareProduct) {
							if ( $compareProduct['id'] == $request['product_id'] ) {
								unset($compareData[$categoryName][$productKey]);
								
								if (count($compareData[$categoryName]) == 0) {
									$removeCategory = true;
									unset($compareData[$categoryName]);
								}
							}
						}
					}
				}
				
				Session::store(['compare' => $compareData]);
				
				$message = 'Элемент удален';
				$response = [
					'success' => true,
					'message' => $message,
				];
				
				if ($removeCategory) {
					$response['category_removed'] = true;
				}
			}
			
			echo json_encode($response);
			return true;
		}
		
		public function actionRemove($category)
		{
			$category = urldecode($category);
			$compareData = Session::getByKey('compare');
			
			foreach ($compareData as $categoryName => $compareProducts) {
				if ( $categoryName == $category ) {
					unset($compareData[$categoryName]);
				}
			}
			Session::store(['compare' => $compareData]);
			
			AppHelper::setMessage('success', 'Список сравнения для категории ' . $category . ' успешно очищен');
			
			return $this->redirect('/compare');
		}
		
		public function actionLoadItemsForCompare()
		{
			$request = $_POST;
			$response = [
				'success' => false,
				'message' => 'Ошибка загрузки списка сравнения'
			];
			
			if ( isset($request['is_ajax']) && $request['is_ajax'] ) {
				$compareItems = Session::getByKey('compare');
				
				$response = [
					'success'       => true,
					'message'       => 'Данные загружены успешно',
					'compare_items' => $compareItems
				];
			}
			
			echo json_encode($response);
			return true;
		}
		
		public function GetCategoryProperties($category_name)
		{
			$request = $_POST;
			$response = [
				'success' => false,
				'message' => 'Ошибка загрузки свойств категории'
			];
			
//			if ( isset($request['is_ajax']) && $request['is_ajax'] && isset($request['category_name']) && $request['category_name'] ) {
				$category = Category::find(['name' => $category_name], true);
				
				$properties = Properties::find(['category_id' => $category['id']]);
				
				$propertyByGroup = [];
				foreach ($properties as $property) {
					$propertyByGroup[] = $property;
				}
				
//				$response = [
//					'success'    => true,
//					'message'    => 'Данные загружены успешно',
//					'properties' => $propertyByGroup
//				];
//			}
			return $propertyByGroup;
//			echo json_encode($response);
//			return true;
		}
	}