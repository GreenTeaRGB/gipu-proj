<?php

namespace Application\Controllers;

use Application\Models\InfoSlider;
use Application\Models\News;
use Application\Models\NewsCategory;
use Application\Models\Product;
use Application\Models\Shopcase;
use Application\Models\Slider;

class SiteController extends \Application\Classes\ControllerBase
{
  public function actionIndex()
  {
    $slider         = Slider::findSort( [ 'active' => 1 ], [ 'sort' => 'asc' ] );
    $infoSlider     = InfoSlider::findSort( [ 'active' => 1 ], [ 'sort' => 'asc' ] );
    $popular        = Product::findPopular();
    $shopcase       = Shopcase::findOne( 1 )['product_id'];
    $shopcaseCount = count($shopcase);
    if($shopcaseCount < 6)
      $count = $shopcaseCount;
    else
      $count = 6;
    $shopcaseIds = array_rand($shopcase, $count);
    $productId = [];
    foreach ( $shopcaseIds as $shopcaseId ) {
      $productId[] = $shopcase[$shopcaseId];
    }
    $product        = Product::find( [ 'id' => $productId ], false, true );
    $popular        = array_merge( $popular, $product );
    $service        = Product::findPopular( 12, 2 );
    $newsCategories = NewsCategory::find();
    $news           = false;
    $firstCategory  = current( $newsCategories );
    if ( $firstCategory ) {
      $news = News::findByCategoryId( $firstCategory['id'] );
    }
    return $this->render(
      'index',
      [
        'slider'         => $slider,
        'infoSlider'     => $infoSlider,
        'popular'        => $popular,
        'services'       => $service,
        'firstCategory'  => $firstCategory,
        'newsCategories' => $newsCategories,
        'news'           => $news !== false
          ? $news
          : [],
      ]
    );
  }

  public function actionAgent()
  {
    return $this->render( 'agent' );
  }
}