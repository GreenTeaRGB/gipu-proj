<?php

namespace Application\Controllers;

use \Application\Classes\AdminBuilder;
use \Application\Models\ReportTransaction;
use Application\Models\User;
use function in_array;

class AdminReportTransactionController extends \Application\Classes\AdminBase
{

  public function actionIndex( $page = 1 )
  {

    $builder = new AdminBuilder( new class extends ReportTransaction
    {
      public function getFields()
      {
        $fields        = parent::getFields();

        $visibleFields = [ 'id', 'user_from', 'user_to', 'sum', 'created_at', 'transaction' ];
        foreach ( $fields as $field => $settings ) {
          if ( !in_array( $field, $visibleFields ) ) {
            $fields[$field]['visible'] = false;
          } else {
            $fields[$field]['visible'] = true;
          }
        }
        $fields['detail']           = [
          'type'      => 'string',
          'visible'   => true,
          'label'     => 'Подробнее',
          'no_filter' => true,
          'sort'      => 6
        ];
        $fields['transaction']      = [
          'type'    => 'button',
          'visible' => true,
          'edited'  => false,
          'url'     => '/admin/reporttransaction/confirm/',
          'label'   => 'Принять',
          'sort'    => 7
        ];
        $fields['transaction_fail'] = [
          'type'    => 'button',
          'visible' => true,
          'edited'  => false,
          'url'     => '/admin/reporttransaction/fail/',
          'label'   => 'Отклонить',
          'sort'    => 8
        ];
        return $fields;
      }
    }, [], [ 'hide' => ['delete', 'view', 'edit' ] ] );
    $builder->setView( 'index' );
    $builder->setWhere(['complete' => 0, 'fail' => 0]);
    $builder->index( $page );
    $items = $builder->getItems();
    foreach ( $items as $id => $item ) {
      $items[$id]['detail'] = '<a target="_blank" href="/admin/seller/view/' . $item['user_from'] . '">Подробнее</a>';
    }

    $builder->setItems( $items );
    $builder->render();
    return true;
  }

  public function actionCreate()
  {
    $builder = new AdminBuilder( new ReportTransaction() );
    $builder->create();
    return true;
  }

  public function actionEdit( $id )
  {
    $builder = new AdminBuilder( new ReportTransaction() );
    $builder->edit( $id );
    return true;
  }

  public function actionActivate()
  {
    $builder = new AdminBuilder( new ReportTransaction() );
    $builder->active();
    return true;
  }

  public function actionSortable()
  {
    $builder = new AdminBuilder( new ReportTransaction() );
    $builder->sort();
    return true;
  }

  public function actionDelete()
  {
    $builder = new AdminBuilder( new ReportTransaction() );
    $builder->delete();
    return true;
  }

  public function actionModal()
  {
    $builder = new AdminBuilder( new ReportTransaction() );
    $builder->modal();
    return true;
  }

  public function actionConfirm( $id )
  {
    $this->operation( $id, 'confirm' );
  }

  public function actionFail( $id )
  {
    $this->operation( $id, 'fail' );
  }

  private function operation( $id, $type )
  {
    $transaction = ReportTransaction::findOne( $id );
    if ( !$transaction )
      $this->json( false, 'Операция не найдена' );
    if ( $transaction['user_from'] == 0 || $transaction['user_to'] == 0 || $transaction['complete'] == 1 || $transaction['fail'] == 1 )
      $this->json( true, 'Операция уже была выполнена' );
    $users = User::find( [ 'id' => [ $transaction['user_from'], $transaction['user_to'] ] ] );
    if ( !$users )
      $this->json( true, 'Пользователи не найдены' );
    if ( count( $users ) !== 2 )
      $this->json( true, 'Один из пользователей не найден' );
    if ( $type == 'confirm' ) {
      if ( $users[$transaction['user_from']]['balance'] < $transaction['sum'] )
        $this->json( true, 'Недостаточно средств' );
      if ( User::updateById( $transaction['user_to'], [ 'balance' => ( $users[$transaction['user_to']]['balance'] + $transaction['sum'] ) ] ) ) {
        if ( User::updateById( $transaction['user_from'], [ 'balance' => ( $users[$transaction['user_from']]['balance'] - $transaction['sum'] ) ] ) ) {
          if ( !ReportTransaction::updateById( $id, [ 'complete' => 1 ] ) ) {
            $this->json( true, 'Операция прошла успешно но статус транзакции обновить не удалось' );
          }
          $this->json( true, 'Операция прошла успешно' );
        } else {
          User::updateById( $transaction['user_to'], [ 'balance' => $users[$transaction['user_to']]['balance'] ] );
          $this->json( false, 'Ошибка выполнеия не удалось списать средства' );
        }
      } else {
        $this->json( false, 'Ошибка выполнеия не удалось перевести средства' );
      }
    }elseif ( $type == 'fail' ) {
      if ( ReportTransaction::updateById( $id, [ 'fail' => 1 ] ) ) {
        $this->json( true, 'Запрос успешно откланен' );
      } else {
        $this->json( false, 'Ошибка выполнения' );
      }
    }
    $this->json( false, 'Ошибка выполнения!' );
  }
}

?>