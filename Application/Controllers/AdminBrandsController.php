<?php

namespace Application\Controllers;

use \Application\Classes\AdminBuilder;
use \Application\Models\Brands;

class AdminBrandsController extends \Application\Classes\AdminBase
{

  public function actionIndex( $page = 1 )
  {
    $builder = new AdminBuilder( new Brands(), [], [ 'activate' => 'active', 'sortable' => 'sort' ] );
    $builder->index( $page );
    return true;
  }

  public function actionCreate()
  {
    $builder = new AdminBuilder( new Brands() );
    $builder->create();
    return true;
  }

  public function actionEdit( $id )
  {
    $builder = new AdminBuilder( new Brands() );
    $builder->edit( $id );
    return true;
  }

  public function actionActivate()
  {
    $builder = new AdminBuilder( new Brands() );
    $builder->active();
  }

  public function actionSortable()
  {
    $builder = new AdminBuilder( new Brands() );
    $builder->sort();
  }

  public function actionDelete()
  {
    $builder = new AdminBuilder( new Brands() );
    $builder->delete();
  }


}

?>