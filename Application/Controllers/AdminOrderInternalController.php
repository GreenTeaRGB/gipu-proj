<?php

namespace Application\Controllers;

use \Application\Classes\AdminBuilder;
use \Application\Models\Order;
use Application\Models\User;

class AdminOrderInternalController extends \Application\Classes\AdminBase
{

  public function actionIndex( $page = 1 )
  {
    $builder = new AdminBuilder( new class extends Order
    {
      public function getFields()
      {
        return array_merge( parent::getFields(), [
          'confirm' => [
            'type'     => 'button',
            'visible'  => true,
            'edited'   => false,
            'required' => false,
            'label'    => "Подтвердить",
            'url'      => '/admin/orderinternal/confirm/'
          ]
        ] );
      }

    }, [] );
    $builder->setWhere( [ 'internal_account' => 1, 'confirm_internal' => 0 ] );
    $builder->index( $page );
    return true;
  }

  public function actionCreate()
  {
    $builder = new AdminBuilder( new Order() );
    $builder->create();
    return true;
  }

  public function actionEdit( $id )
  {
    $builder = new AdminBuilder( new Order() );
    $builder->edit( $id );
    return true;
  }

  public function actionActivate()
  {
    $builder = new AdminBuilder( new Order() );
    $builder->active();
    return true;
  }

  public function actionSortable()
  {
    $builder = new AdminBuilder( new Order() );
    $builder->sort();
    return true;
  }

  public function actionDelete()
  {
    $builder = new AdminBuilder( new Order() );
    $builder->delete();
    return true;
  }

  public function actionModal()
  {
    $builder = new AdminBuilder( new Order() );
    $builder->modal();
    return true;
  }

  public function actionConfirm( $id )
  {
    $order = Order::findOne( $id );
    if ( !$order )
      $this->json( false, 'Заказ не найден' );
    if ( $order['internal_account'] != 1 )
      $this->json( false, 'Заказ оформлена не на внутренние деньги' );
    if ( $order['confirm_internal'] == 1 )
      $this->json( false, 'Операция уже была выполнена' );
    if ( Order::updateById( $id, [ 'confirm_internal' => 1 ] ) )
      $this->json( true, 'Операция прошла успешно' );
    else
      $this->json( false, 'Ошибка выполнения' );
  }

}

?>