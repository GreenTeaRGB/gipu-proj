<?php

	namespace Application\Controllers;

  use Application\Models\Shop;
	use \Application\Models\Order;

	class AdminController extends \Application\Classes\AdminBase
	{
		public function actionIndex()
		{
		  $orderModel = new Order();
		  $sum = $orderModel->selectFields(' SUM(total) as sum ')->fetchOne();
			$this->breadcrumbs ['Админпанель'] = '#';
			$this->template->vars('title_page', 'Админпанель');
			$month = date("Y-m-d", strtotime("-1 month", time()));
			$shops = Shop::find(['>date_active_start' => $month]);
			$countShops = [];
      foreach( $shops as $shop ) {
        $day = explode('.', $shop['date_active_start']);
        $countShops[(int)$day[0]][] = $shop;
			}
      $countOrders = [];
			$orders = Order::find(['>created_at' => $month]);
      foreach( $orders as $order ) {
        $day = explode('.', $order['created_at']);
        if(!isset($countOrders[(int)$day[0]]))
          $countOrders[(int)$day[0]] = 0;
          $countOrders[(int)$day[0]] += $order['count_products'];
      }

		return $this->render('index', [
			'breads'=> $this->getBreadcrumbs(),
      'sum' => $sum['sum'],
      'countShops' => $countShops,
      'countOrders' => $countOrders
		]);
		}

	}
