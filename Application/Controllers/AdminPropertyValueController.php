<?php
	
	namespace Application\Controllers;
	
	use \Application\Classes\AdminBuilder;
	use \Application\Models\PropertyValue;
	
	class AdminPropertyValueController extends \Application\Classes\AdminBase
	{
		
		public function actionIndex($page = 1)
		{
			$builder = new AdminBuilder(new PropertyValue(), []);
			$builder->index($page);
			return true;
		}
		
		public function actionCreate()
		{
			$builder = new AdminBuilder(new PropertyValue());
			$builder->create();
			return true;
		}
		
		public function actionEdit($id)
		{
			$builder = new AdminBuilder(new PropertyValue());
			$builder->edit($id);
			return true;
		}
		
		public function actionActivate()
		{
			$builder = new AdminBuilder(new PropertyValue());
			$builder->active();
		}
		
		public function actionSortable()
		{
			$builder = new AdminBuilder(new PropertyValue());
			$builder->sort();
		}
		
		public function actionDelete()
		{
			$builder = new AdminBuilder(new PropertyValue());
			$builder->delete();
		}
		
		
	}
	
	?>