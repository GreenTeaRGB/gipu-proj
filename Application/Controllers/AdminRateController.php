<?php
namespace Application\Controllers;
use \Application\Classes\AdminBuilder;
	use \Application\Models\Rate;
class AdminRateController extends \Application\Classes\AdminBase
{

	public function actionIndex($page = 1)
	{
		$builder = new AdminBuilder(new Rate(), [] ,['activate'=>'active']);
		$builder->index($page);
		return true;
	}

	public function actionCreate()
	{
		$builder = new AdminBuilder(new Rate());
		$builder->create();
		return true;
	}

	public function actionEdit($id)
	{
		$builder = new AdminBuilder(new Rate());
		$builder->edit($id);
		return true;
	}
	
	public function actionActivate(){
		$builder = new AdminBuilder(new Rate());
		$builder->active();
	}
	
	public function actionSortable(){
		$builder = new AdminBuilder(new Rate());
		$builder->sort();
	}
	public function actionDelete(){
		$builder = new AdminBuilder(new Rate());
		$builder->delete();
	}
	
	
}
?>