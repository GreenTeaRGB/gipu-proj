<?php
	
	namespace Application\Controllers;
	
	use Application\Classes\Session;
  use Application\Models\Product;
  use \Application\Models\Wishlist;
	use \Application\Helpers\UserHelper;
	use \Application\Helpers\AppHelper;

	class WishlistController extends \Application\Classes\ControllerBase
	{
		public function actionIndex()
		{
			$user = UserHelper::getUser();
			
			if ( $user ) {
				if ( !UserHelper::isAdmin() ) {
					$model = new Wishlist(true);
					$wishlistItems = $model->where(['user_id' => $user['id']])->fetchAll();
					
					return $this->render(
						'index',
						[
							'items' => $wishlistItems
						]
					);
				} else {
					$this->redirect('/admin');
				}
			} else {
        $wishlistItems = Session::getByKey('wishlist');
        if(!$wishlistItems)
          $products = [];
        else
          $products = Product::find(['id' => $wishlistItems], false, true);
        return $this->render('index', ['products' => $products]);
			}
		}
		
		public function actionToggle()
		{
			$message = 'Ошибка добавления товара в список избранных';
			$request = $_POST;
			$response = [
				'success' => false,
        'delete' => false,
				'message' => $message
			];
			
			$user = UserHelper::getUser();
			
			if ( $request['product_id'] ) {
			  if($user){
          $model = new Wishlist();
          $data = [
            'product_id' => $request['product_id'],
            'user_id'    => $user['id']
          ];

          $wishlistItem = $model->where($data)->fetchOne();
          if ( $wishlistItem ) {
            $response = $this->actionRemove($request['product_id'], $user['id']);
          } else {
            $done = $model->setFields($data)->insert();

            if ( $done ) {
              $message = 'Товар успешно добавлен в список избранных';

              $response = [
                'success' => true,
                'delete' => false,
                'message' => $message
              ];
            }
          }
        }else{
			    $wishlistData = Session::getByKey('wishlist');
          if ( !$wishlistData ) {
            $wishlistData[] = $request['product_id'];
            Session::store(['wishlist' => $wishlistData]);
            $this->json(true, 'Товар успешно добавлен в избранное', ['delete' => false]);
          }else{
            if(!in_array($request['product_id'], $wishlistData)){
              $wishlistData[] = $request['product_id'];
              Session::store(['wishlist' => $wishlistData]);
              $this->json(true, 'Товар успешно добавлен в избранное', ['delete' => false]);
            }else{
              $wishlistData  = array_filter($wishlistData, function($id){
                return $id != $_POST['product_id'];
              });
              Session::store(['wishlist' => $wishlistData]);
              $this->json(true, 'Товар успешно удален из избранного', ['delete' => true]);
            }
          }
			  }
			}
			
			echo json_encode($response);
			return true;
		}
		
		private function actionRemove($productId, $userId)
		{
			$message = 'Ошибка удаления товара из списка избранных';
			$request = $_POST;
			
			$response = [
				'success' => false,
        'delete' => false,
				'message' => $message
			];
			
			if ( $productId && $userId ) {
				$model = new Wishlist();
				$done = $model->where(['product_id' => $request['product_id'], 'user_id' => $userId])->delete();
				
				if ( $done ) {
					$message = 'Товар успешно удален из списка избранных';
					
					$response = [
						'success' => true,
            'delete' => true,
						'message' => $message
					];
				}
			}
			
			return $response;
		}
	}