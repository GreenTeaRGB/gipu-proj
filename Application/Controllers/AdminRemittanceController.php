<?php

  namespace Application\Controllers;

  use Application\Classes\AdminBuilder;
  use Application\Helpers\DataHelper;
  use Application\Models\Remittance;
  use Application\Models\ReportTransaction;
  use Application\Models\User;

  class AdminRemittanceController extends \Application\Classes\AdminBase
  {

    public function actionIndex( $page = 1 )
    {
      $builder = new AdminBuilder( new Remittance() );
      $builder->setSuccessMessage( 'Операция прошла успешно!' );
      if( isset( $_POST['submit'] ) ) {
        $from = (int) $_POST['user_from'];
        $to   = (int) $_POST['user_to'];
        $sum  = (double) $_POST['sum'];
        if( $from == $to ) {
          $builder->setErrors( [ 'user_from' => 'Пользователь не может сделать перевод себе' ] );
          $builder->setErrors( [ 'user_to' => 'Пользователь не может сделать перевод себе' ] );
        } else {
          $users = User::find( [ 'id' => [ $from, $to ], 'type' => DataHelper::DEFAULT_SELLER_TYPE_ID ] );
          if( !isset( $users[$from] ) ) {
            $builder->setErrors( [ 'user_from' => 'Пользователь не найден' ] );
          }
          if( !isset( $users[$to] ) ) {
            $builder->setErrors( [ 'user_to' => 'Пользователь не найден' ] );
          }
          if( $sum == 0 ) {
            $builder->setErrors( [ 'sum' => 'Укажите сумму' ] );
          }
          if( isset( $users[$from] ) && $users[$from]['balance'] < $sum && $sum !== 0 ) {
            $builder->setErrors( [ 'sum' => 'Не достаточно средств '.( $users[$from]['balance'] - $sum ) ] );
          }
          if( !$builder->hasErrors() ) {
            $builder->setResult( true );
            if( User::updateById( $from, [ 'balance' => ( $users[$from]['balance'] - $sum ) ] ) ) {
              if( User::updateById( $to, [ 'balance' => ( $users[$to]['balance'] + $sum ) ] ) ) {
                ReportTransaction::create(
                    [
                        'user_from' => $from,
                        'user_to'   => $to,
                        'type'      => DataHelper::TYPE_TRANSACTION_INFO,
                        'sum'       => $sum,
                        'text'      => 'Перевод средств'
                    ]
                );
                $builder->setResult( true );
                $this->refresh();
              } else {
                //Вернем бабки
                User::updateById( $from, [ 'balance' => $users[$from]['balance'] ] );
                ReportTransaction::create(
                    [
                        'user_from' => $from,
                        'user_to'   => $to,
                        'type'      => DataHelper::TYPE_TRANSACTION_ERROR,
                        'sum'       => $sum,
                        'text'      => 'Не удалось сделать перевод'
                    ]
                );
                $this->setResult( false );
              }
            }
          }
        }
        unset( $_POST['submit'] );
      }
      $builder->create();
      return true;
    }

    private function refresh()
    {
      header( "Location: /admin/remittance?success=true" );
    }
    //	public function actionCreate()
    //	{
    //		$builder = new AdminBuilder(new Remittance());
    //		$builder->create();
    //		return true;
    //	}
    //
    //	public function actionEdit($id)
    //	{
    //		$builder = new AdminBuilder(new Remittance());
    //		$builder->edit($id);
    //		return true;
    //	}
    //
    //	public function actionActivate(){
    //		$builder = new AdminBuilder(new Remittance());
    //		$builder->active();
    //		 return true;
    //	}
    //
    //	public function actionSortable(){
    //		$builder = new AdminBuilder(new Remittance());
    //		$builder->sort();
    //		 return true;
    //	}
    	public function actionDelete(){
    		$builder = new AdminBuilder(new Remittance());
    		$builder->delete();
    		 return true;
    	}
    		public function actionModal(){
        $builder = new AdminBuilder(new Remittance());
        $builder->modal();
        return true;
      }
    //
  }

  ?>