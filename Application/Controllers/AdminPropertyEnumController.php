<?php
	
	namespace Application\Controllers;
	
	use \Application\Classes\AdminBuilder;
	use \Application\Models\PropertyEnum;
	
	class AdminPropertyEnumController extends \Application\Classes\AdminBase
	{
		
		public function actionIndex($page = 1)
		{
			$builder = new AdminBuilder(new PropertyEnum(), []);
			$builder->index($page);
			return true;
		}
		
		public function actionCreate()
		{
			$builder = new AdminBuilder(new PropertyEnum());
			$builder->create();
			return true;
		}
		
		public function actionEdit($id)
		{
			$builder = new AdminBuilder(new PropertyEnum());
			$builder->edit($id);
			return true;
		}
		
		public function actionActivate()
		{
			$builder = new AdminBuilder(new PropertyEnum());
			$builder->active();
		}
		
		public function actionSortable()
		{
			$builder = new AdminBuilder(new PropertyEnum());
			$builder->sort();
		}
		
		public function actionDelete()
		{
			$builder = new AdminBuilder(new PropertyEnum());
			$builder->delete();
		}
		
		
	}
	
	?>