<?php

use \Application\Helpers\UserHelper;
use \Application\Models\AdminPage;
use \Application\Models\AdminPageCategory;
use \Application\Models\RequestOutput;
use \Application\Models\ReportTransaction;

$user       = UserHelper::getUser();
$pages      = AdminPage::getListByUserGroup( $user['user_group_id'] );
$categories = AdminPageCategory::findSort( [], [ 'sort' => 'asc' ] );
$modelRequestOutput = new RequestOutput();
$requestoutput = $modelRequestOutput->where(['complete' => 0, 'fail' => 0])->count();
$modelReportTransaction = new ReportTransaction();
$reporttransaction = $modelReportTransaction->where(['complete' => 0, 'fail' => 0])->count();
$js         = '';
?>
<div class="col-xs-12 col-sm-3 no-padding">
    <div class="collapse navbar-collapse" id="navbar-tom-menu">
        <ul class="nav nav-list left-admin-nav" id="nav" role="navigation">
            <li <?= $_SERVER["REQUEST_URI"] == '/admin' ? ' class="active"' : '' ?>><a href="/admin">Главная</a></li>
          <?php foreach( $categories as $category ) : ?>
              <li>
                  <a role="button" data-toggle="collapse" href="#collapse<?= $category['id'] ?>" aria-expanded="false"
                     aria-controls="collapseExample">
                    <?= $category['name'] ?>
                      <i style="float: right" class="fa fa-angle-down" aria-hidden="true"></i>
                  </a>
                  <div class="collapse menu_collapse" id="collapse<?= $category['id'] ?>">
                      <ul>
                        <?php
                        $urls = [];
                        foreach( $category['pages'] as $page ):
                          if( !isset( $pages[$page] ) ) continue;
                          $urls[] = $pages[$page]['uri'];
                          if($_SERVER["REQUEST_URI"] == '/admin/' . $pages[$page]['uri']){
                            $active = true;
                          }else{
                            $active = preg_match('#^/admin/'.$pages[$page]['uri'].'/(.*?)#', $_SERVER["REQUEST_URI"]);
                          }
                          if( $active ) {
                            $js .= '$("a[href=\'#collapse' . $category['id'] . '\']").trigger("click");';
                          }
                          ?>
                            <li <?= $active ? 'class="active"'
                              : ''; ?>>
                                <a href="/admin/<?= $pages[$page]['uri']; ?>"><?= $pages[$page]['icon']; ?>
                                    <span style="padding-left: 10px;"><?= $pages[$page]['name']; ?></span>
                                    <?php if($category['name'] == 'Уведомления'){
                                        switch($pages[$page]['uri']){
                                          case "noticerequestoutput": echo "<span class='bridge'>$requestoutput</span>"; break;
                                          case "reporttransaction": echo "<span class='bridge'>$reporttransaction</span>"; break;
                                        }
                                    }
                                    ?>
                                </a>
                            </li>
                        <?php endforeach; ?>
                        <?php
                        foreach( $urls as $url ) {
                          switch( $url ) {
                            case "product" :
                              echo '<li><a href="/admin/product/create">Добавить товар</a></li>';
                              break;
                            case "category" :
                              echo '<li><a href="/admin/category/create">Добавить категорию</a></li>';
                              break;
                          }
                        }
                        ?>
                      </ul>
                  </div>
              </li>
          <?php endforeach; ?>
        </ul>
    </div>
</div>
<script>
  <?=$js?>
</script>
<style>
    .menu_collapse ul li {
        display: flex;
    }

    .left-admin-nav li:hover a {
        color: #999;
        background: transparent;
    }

    .left-admin-nav li:hover > a {
        color: #999;
        background: transparent;
    }

    .left-admin-nav a:hover {
        color: #fff !important;
    }

    .nav > li > a:hover, .nav > li > a:focus {
        text-decoration: none;
        background-color: transparent;
    }

    .left-admin-nav li.active a {
        background: #0866c6 !important;
        color: #fff;
        width: 100%;
    }
</style>