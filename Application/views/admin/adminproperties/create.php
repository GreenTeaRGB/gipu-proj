<h1 class="col-xs-12"><?= $title_page; ?></h1>
<div class="clr colx-xs-12 col-md-12 "></div>
<div class="col-md-12"><a href="/admin/properties" class="btn btn-primary">Вернуться к списку</a></div>
<div class="col-xs-12 clr"></div>
<div class="colx-xs-12 col-md-8">
  <form role="form" class="form-create news-form-create" method="POST" action="#" enctype="multipart/form-data">
		<?php if( isset( $message ) ) { ?>
      <div class="alert alert-danger alert-dismissable">
				<?php foreach( $message as $mess ) {
					echo $mess . '<br>';
				} ?>
      </div>
		<?php } ?>
		<?php if( isset( $error ) && !is_array( $error ) ): ?>
      <div class="alert alert-danger alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <i class="fa fa-times"></i> Ошибка сохранения!
      </div>
		<?php endif; ?>
		<?php if( isset( $success ) ): ?>
      <div class="alert alert-success alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <i class="fa fa-check"></i> Страница успешно сохранена
        <a class="alert-link" style="text-decoration:underline;" href="/admin/properties">Вернуться к списку страниц</a>
      </div>
		<?php endif; ?>

    <div class="form-group <?= isset( $error ) && is_array( $error ) && in_array( 'type', $error ) ? ' has-error has-feedback' : ''; ?> ">
      <label for="title">Тип свойства</label>
      <select class="form-control" name="type" id="type">
        <option value="S" <?= isset( $property ) && $property[ 'type' ] == 'S' ? ' selected ' : '' ?>>Строка</option>
        <option value="L" <?= isset( $property ) && $property[ 'type' ] == 'L' ? ' selected ' : '' ?>>Список</option>
        <option value="F" <?= isset( $property ) && $property[ 'type' ] == 'F' ? ' selected ' : '' ?>>Файл</option>
        <option value="T" <?= isset( $property ) && $property[ 'type' ] == 'T' ? ' selected ' : '' ?>>Текст</option>
        <option value="D" <?= isset( $property ) && $property[ 'type' ] == 'D' ? ' selected ' : '' ?>>Дата</option>
      </select>
			<?= isset( $error ) && is_array( $error ) && in_array( 'type', $error ) ? '<span class="glyphicon glyphicon-remove form-control-feedback"></span>' : ''; ?>
    </div>

    <div class="form-group <?= isset( $error ) && is_array( $error ) && in_array( 'active', $error ) ? ' has-error has-feedback' : ''; ?> ">
      <label for="title">Свойство активно</label>
      <input type="checkbox" <?= isset( $property ) && $property[ 'active' ] == 1 ? ' checked ' : '' ?> class=" input-fill" id="active" data-toggle="tooltip" data-placement="right" title="" name="active" value="1">
			<?= isset( $error ) && is_array( $error ) && in_array( 'filter', $error ) ? '<span class="glyphicon glyphicon-remove form-control-feedback"></span>' : ''; ?>
    </div>

    <div class="form-group <?= isset( $error ) && is_array( $error ) && in_array( 'name', $error ) ? ' has-error has-feedback' : ''; ?> ">
      <label for="title">Название свойства <em>*</em></label>
      <input type="text" class="form-control input-fill" id="name" placeholder="" data-toggle="tooltip" data-placement="right" title="Название свойства" name="name" value="<?= isset( $property ) ? $property[ 'name' ] : '' ?>">
			<?= isset( $error ) && is_array( $error ) && in_array( 'name', $error ) ? '<span class="glyphicon glyphicon-remove form-control-feedback"></span>' : ''; ?>
    </div>

    <div class="form-group <?= isset( $error ) && is_array( $error ) && in_array( 'code', $error ) ? ' has-error has-feedback' : ''; ?> ">
      <label for="title">Символьный код <em>*</em></label>
      <input type="text" class="form-control input-fill" id="code" placeholder="" data-toggle="tooltip" data-placement="right" title="Код свойтсва" name="code" value="<?= isset( $property ) ? $property[ 'code' ] : '' ?>">
			<?= isset( $error ) && is_array( $error ) && in_array( 'code', $error ) ? '<span class="glyphicon glyphicon-remove form-control-feedback"></span>' : ''; ?>
    </div>


    <div class="form-group <?= isset( $error ) && is_array( $error ) && in_array( 'sort', $error ) ? ' has-error has-feedback' : ''; ?> ">
      <label for="title">Поядок сортировки</label>
      <input type="text" class="form-control input-fill" id="sort" placeholder="" data-toggle="tooltip" data-placement="right" title="" name="sort" value="<?= isset( $property ) ? $property[ 'sort' ] : '' ?>">
			<?= isset( $error ) && is_array( $error ) && in_array( 'sort', $error ) ? '<span class="glyphicon glyphicon-remove form-control-feedback"></span>' : ''; ?>
    </div>

    <div class="form-group <?= isset( $error ) && is_array( $error ) && in_array( 'multi', $error ) ? ' has-error has-feedback' : ''; ?> ">
      <label for="title">Множественное</label>
      <input type="checkbox" <?= isset( $property ) && $property[ 'multi' ] == 1 ? ' checked ' : '' ?> class=" input-fill" id="multi" data-toggle="tooltip" data-placement="right" title="У данного свойтсва будет возможноть иметь несколько значений" name="multi" value="1">
			<?= isset( $error ) && is_array( $error ) && in_array( 'multi', $error ) ? '<span class="glyphicon glyphicon-remove form-control-feedback"></span>' : ''; ?>
    </div>

    <div class="form-group <?= isset( $error ) && is_array( $error ) && in_array( 'required', $error ) ? ' has-error has-feedback' : ''; ?> ">
      <label for="title">Обязательное</label>
      <input type="checkbox" <?= isset( $property ) && $property[ 'required' ] == 1 ? ' checked ' : '' ?> class=" input-fill" id="required" data-toggle="tooltip" data-placement="right" title="Данное свойтсво будет обязательным для заполнения" name="required" value="1">
			<?= isset( $error ) && is_array( $error ) && in_array( 'required', $error ) ? '<span class="glyphicon glyphicon-remove form-control-feedback"></span>' : ''; ?>
    </div>

    <div class="form-group list <?= isset( $error ) && is_array( $error ) && in_array( 'filter', $error ) ? ' has-error has-feedback' : ''; ?> ">
      <label for="title">Отображать в фильтре</label>
      <input type="checkbox" <?= isset( $property ) && $property[ 'filter' ] == 1 ? ' checked ' : '' ?> class=" input-fill" id="filter" data-toggle="tooltip" data-placement="right" title="Данное свойство будет отображаться в фильтре на странице каталога" name="filter" value="1">
			<?= isset( $error ) && is_array( $error ) && in_array( 'filter', $error ) ? '<span class="glyphicon glyphicon-remove form-control-feedback"></span>' : ''; ?>
    </div>

    <div class="form-group <?= isset( $error ) && is_array( $error ) && in_array( 'filter', $error ) ? ' has-error has-feedback' : ''; ?> ">
      <label for="title">Отображать дополнительное поле с описанием</label>
      <input type="checkbox" <?= isset( $property ) && $property[ 'description_field' ] == 1 ? ' checked ' : '' ?> class=" input-fill" id="description_field" data-toggle="tooltip" data-placement="right" title="Отображать дополнительное поле с описанием" name="description_field" value="1">
			<?= isset( $error ) && is_array( $error ) && in_array( 'description_field', $error ) ? '<span class="glyphicon glyphicon-remove form-control-feedback"></span>' : ''; ?>
    </div>
    <div <?= isset( $property[ 'type' ] ) && $property[ 'type' ] !== 'T' ? 'style="display:none;"' : '' ?> class="redactor form-group <?= isset( $error ) && is_array( $error ) && in_array( 'redactor', $error ) ? ' has-error has-feedback' : ''; ?> ">
      <label for="title">Разрешить использование редактора</label>
      <input type="checkbox" <?= isset( $property ) && $property[ 'redactor' ] == 1 ? ' checked ' : '' ?> class=" input-fill" id="redactor" data-toggle="tooltip" data-placement="right" title="Для этого поля будет доспупен html редактор" name="redactor" value="1">
			<?= isset( $error ) && is_array( $error ) && in_array( 'redactor', $error ) ? '<span class="glyphicon glyphicon-remove form-control-feedback"></span>' : ''; ?>
    </div>


      <div class="form-group <?= isset( $error ) && is_array( $error ) && in_array( 'main', $error ) ? ' has-error has-feedback' : ''; ?> ">
        <label for="title">Основное свойство предложения</label>
        <input type="checkbox" <?= isset( $property ) && $property[ 'main' ] == 1 ? ' checked ' : '' ?> class=" input-fill" id="main" data-toggle="tooltip" data-placement="right" title="Данное свойство будет отображаться в карточке товара и может влиять на цену товара" name="main" value="1">
				<?= isset( $error ) && is_array( $error ) && in_array( 'main', $error ) ? '<span class="glyphicon glyphicon-remove form-control-feedback"></span>' : ''; ?>
      </div>

    <div class="form-group list <?= isset( $error ) && is_array( $error ) && in_array( 'template', $error ) ? ' has-error has-feedback' : ''; ?> ">
      <label for="title">Вид отображения</label>
      <select class="form-control" name="template" id="template">
        <option value="L" <?= isset( $property ) && $property[ 'template' ] == 'L' ? ' selected ' : '' ?>>Выпадающий
                                                                                                          список
        </option>
        <option value="S" <?= isset( $property ) && $property[ 'template' ] == 'S' ? ' selected ' : '' ?>>Флажки
        </option>
        <option value="F" <?= isset( $property ) && $property[ 'template' ] == 'F' ? ' selected ' : '' ?>>Радио кнопки
        </option>
      </select>
			<?= isset( $error ) && is_array( $error ) && in_array( 'template', $error ) ? '<span class="glyphicon glyphicon-remove form-control-feedback"></span>' : ''; ?>
    </div>

    <div class="form-group list <?=isset($error) && is_array($error) && in_array('template', $error) ? ' has-error has-feedback': '';?> ">
      <label for="title">Вид отображения в фильтре</label>
      <select class="form-control" name="template_filter" id="template_filter">
        <option value="CH" <?=isset($property) && $property['template_filter'] == 'CH'?' selected ':''?>>Чекбокс</option>
        <option value="PU" <?=isset($property) && $property['template_filter'] == 'PU'?' selected ':''?>>Картинки (картинка загружается для каждого элемента)</option>
        <option value="CO" <?=isset($property) && $property['template_filter'] == 'CO'?' selected ':''?>>Цвета (код цвета находится в описании каждого элемента)</option>
        <option value="BU" <?=isset($property) && $property['template_filter'] == 'BU'?' selected ':''?>>Кнопки</option>
<!--        <option value="SE" --><?//=isset($property) && $property['template_filter'] == 'SE'?' selected ':''?><!-->Выпадающий список</option>-->
      </select>
			<?=isset($error) && is_array($error) && in_array('template_filter', $error) ? '<span class="glyphicon glyphicon-remove form-control-feedback"></span>': '';?>
    </div>
    <div class="form-group <?= isset( $error ) && is_array( $error ) && in_array( 'property_group_id', $error ) ? ' has-error has-feedback' : ''; ?> ">
      <label for="title">Группа свойства</label>
      <select class="form-control" name="property_group_id" id="property_group_id">
				<?php foreach($groups  as $group ) { ?>
          <option value="<?= $group[ 'id' ] ?>" <?=(isset($property) && $group['id'] == $property['property_group_id']) ? 'selected' : ''?> ><?=$group['name']?></option>
        <?php  } ?>
      </select>
			<?= isset( $error ) && is_array( $error ) && in_array( 'property_group_id', $error ) ? '<span class="glyphicon glyphicon-remove form-control-feedback"></span>' : ''; ?>
    </div>
	  
    <div class="form-group <?= isset( $error ) && is_array( $error ) && in_array( 'category_id', $error ) ? ' has-error has-feedback' : ''; ?> ">
      <label for="title">Категория</label>
      <select class="form-control" name="category_id" id="category_id">
		  <option>Применить к категории</option>
				<?php foreach($categories  as $category ) { ?>
          <option value="<?= $category[ 'id' ] ?>" <?=(isset($property) && $category['id'] == $property['category_id']) ? 'selected' : ''?> ><?=$category['name']?></option>
        <?php  } ?>
      </select>
			<?= isset( $error ) && is_array( $error ) && in_array( 'property_group_id', $error ) ? '<span class="glyphicon glyphicon-remove form-control-feedback"></span>' : ''; ?>
    </div>

    <div class="form-group <?= isset( $error ) && is_array( $error ) && in_array( 'property_description', $error ) ? ' has-error has-feedback' : ''; ?> ">
      <label for="title">Описание</label>
      <textarea name="property_description" class="form-control" id="property_description" cols="30" rows="10"><?=isset($property)?$property['property_description']:''?></textarea>
			<?= isset( $error ) && is_array( $error ) && in_array( 'property_description', $error ) ? '<span class="glyphicon glyphicon-remove form-control-feedback"></span>' : ''; ?>
    </div>

    <div class="form-group list" <?= isset( $property ) && $property[ 'type' ] == 'L' ? ' style="display: block;" ' : 'style="display: none;"' ?>>
      <label for="title">Значения списка</label>
			<?php if( isset( $propertyEnum ) ) { ?>
				<?php
				$count = 1;
				foreach( $propertyEnum as $item ) { ?>
          <input type="text" class="form-control values input-fill" id="value" data-toggle="tooltip" data-placement="right" title="" name="value[<?= $count ?>]" value="<?= $item[ 'value' ] ?>" placeholder="Значение">
          <input type="text" class="form-control desc input-fill" id="value" data-toggle="tooltip" data-placement="right" title="" data-count="<?= $count ?>" name="description[<?= $count ?>]" value="<?= $item[ 'description' ] ?>" placeholder="Описание">
          <input type="color" data-count="<?= $count ?>" value="<?= strpos( $item[ 'description' ], '#' ) == false ? $item[ 'description' ] : '' ?>" class="color_picker">
          <input type="file" class="form-control input-fill" id="value" data-toggle="tooltip" data-placement="right" title="" name="file[<?= $count ?>]" value="<?= $item[ 'description' ] ?>" placeholder="Значение">
					<?php if( $item[ 'file' ] != '' ) { ?> <img width="30px" src="<?= $item[ 'file' ] ?>">
            <a data-id="<?= $item[ 'id' ] ?>" class="delete_image">X</a> <?php } ?>
          <input type="hidden" class="form-control input-fill" id="value" data-toggle="tooltip" data-placement="right" title="" name="id[<?= $count ?>]" value="<?= $item[ 'id' ] ?>" placeholder="Значение">
					<?php $count++;
				} ?>
			<?php } ?>
      <input type="text" class="form-control values input-fill" id="value" data-toggle="tooltip" data-placement="right" title="" name="value[0]" value="" placeholder="Значение">
      <input type="text" class="form-control values desc input-fill" id="value" data-toggle="tooltip" data-placement="right" title="" data-count="0" name="description[0]" value="" placeholder="Описание">
      <input type="color" data-count="0" class="color_picker">

      <input type="file" class="form-control values input-fill" id="value" data-toggle="tooltip" data-placement="right" title="" name="file[0]" value="" placeholder="Значение">
      <a class="btn btn-default add">Еще</a>
    </div>

    <input type="submit" name="save" class="btn btn-success" value="Сохранить">
  </form>
</div>

<script>

    $( '.list' ).on( 'change', '.color_picker', function () {
        $( '.desc[data-count="' + $( this ).data( 'count' ) + '"]' ).val( $( this ).val() );
    } );
    $( "#type" ).on( 'change', function () {
        if ( $( this ).val() === 'L' ) {
            $( '.redactor' ).css( 'display', 'none' );
            $( '.list' ).css( 'display', 'block' );
        }
        else if ( $( this ).val() === 'T' ) {
            $( '.list' ).css( 'display', 'none' );
            $( '.redactor' ).css( 'display', 'block' );
        }
        else {
          $( '.list' ).css( 'display', 'none' );
            $( '.redactor' ).css( 'display', 'none' );
        }
    } );
    $( '.add' ).on( 'click', function () {
        var count = $( '.values' ).length;
        $( this ).before( '<input type="text" class="form-control values input-fill" id="value" data-toggle="tooltip" data-placement="right" title="" name="value[' + count + ']" value="" placeholder="Значение"> <input type="text" class="form-control desc input-fill" id="value" data-toggle="tooltip" data-placement="right" title="" data-count="' + count + '" name="description[' + count + ']" value="" placeholder="Описание"><input type="color" data-count="' + count + '" class="color_picker">' +
            '          <input type="file" class="form-control  input-fill" id="value" data-toggle="tooltip" data-placement="right" title="" name="file[' + count + ']" value="" placeholder="Значение">' );
    } );
    $( '.delete_image' ).on( 'click', function () {
        var id = $( this ).data( 'id' );
        $.ajax( {
            type: "POST",
            url: '/admin/properties/deleteimg',
            data: 'id=' + id,
            dataType: 'json',
            beforeSend: function () {
                $( '.alert-form' ).remove();
                $( '.load-full-page' ).css( 'display', 'block' );
            }
        } )
            .done( function ( data ) {
                $( '.load-full-page' ).css( 'display', 'none' );
                if ( data.result === true ) window.location.reload();
                else alert( "Ошибка удаления" );

            } )
            .error( function ( data ) {
                $( '.load-full-page' ).css( 'display', 'none' );
            } );
    } );
</script>
