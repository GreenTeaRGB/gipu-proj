<table class="table table-bordered table-hover">
  <tr>
    <th>№</th>
    <th>Дата</th>
    <th>Сумма</th>
    <th>Сатус</th>
    <th>Статус доставки</th>
    <th>Способ оплаты</th>
  </tr>

  <?php foreach ( $orders as $order ) : ?>
    <tr>
      <td><?=$order['id']?></td>
      <td><?=$order['created_at']?></td>
      <td><?=$order['total']?></td>
      <td><?=$status[$order['status_id']]['name']?></td>
      <td><?=$deliveryStatus[$order['delivery_status_id']]['type']?></td>
      <td><?=$payments[$order['pay_type_id']]['type']?></td>
    </tr>
  <?php endforeach; ?>
</table>