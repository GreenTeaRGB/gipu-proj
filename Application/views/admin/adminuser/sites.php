<?php foreach ( $sites as $site ) : ?>
  <div class="form-group">
    <label for="">Сайт "<?= $site['name'] ?>" №<?= $site['id'] ?></label>
    <ul>
      <li><b>Сслыка:</b><?=$site['url']?></li>
      <li><b>Дата активации:</b><?=$site['date_active_start']?></li>
      <li><b>Дата окончания аренды:</b><?=$site['date_active_end']?></li>
      <li><b>Статус:</b><?=$site['status_name']?></li>
      <li><b>Тариф:</b><?=$site['rate_name']?></li>
      <li><b>Категории:</b>
        <?php if(isset($site['categories'])) foreach ( $site['categories'] as $category ) : ?>
          <?=$categories[$category['category_id']]['name']?>,
        <?php endforeach; ?>
      </li>
    </ul>
  </div>
<?php endforeach; ?>
