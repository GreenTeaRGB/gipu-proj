<table class="table table-bordered table-hover">
<tr>
  <th>Дата</th>
  <th>Операция</th>
  <th>От кого</th>
  <th>Кому</th>
  <th>Сумма</th>
</tr>

<?php foreach ( $transactions as $transaction ) : ?>
  <tr>
    <td><?=$transaction['created_at']?></td>
    <td><?=$transaction['text']?></td>
    <td><?=$transaction['user_from']?></td>
    <td><?=$transaction['user_to']?></td>
    <td><?=$transaction['sum']?></td>
  </tr>
<?php endforeach; ?>
</table>