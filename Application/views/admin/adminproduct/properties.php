<div class="properties"><?php
  if($properties && count($properties) > 0):
  foreach ( $properties as $property ) { ?>
    <?php
    $valueProp = [];
    if ( isset( $values ) )
      foreach ( $values as $value ) {
        if ( $property['id'] == $value['property_id'] ) {
          $valueProp[] = $value;
        }

      }
    if ( isset( $_POST['property'][$property['code']]['value'] ) && is_array( $_POST['property'][$property['code']]['value'] ) && count( $valueProp ) == 0 ) {
      foreach ( $_POST['property'][$property['code']]['value'] as $key => $item ) {
        $valueProp[] = [ 'value'       => $item,
                         'id'          => '',
                         'value_enum'  => $item,
                         'description' => isset( $_POST['property'][$property['code']]['description'][$key] )
                           ? $_POST['property'][$property['code']]['description'][$key] : ''
        ];
      }
    }
    ?>
    <?php if ( $property['type'] == 'S' ) { ?>
      <div class="form-group <?= is_array( $error ) && isset( $error[$property['code']] ) ? ' has-error has-feedback'
        : ''; ?> ">
        <label for="name"><?= $property['name'] ?> <?php if ( $property['required'] == 1 ) { ?><em>*</em><?php } ?>
        </label>
        <?php if ( !$property['multi'] ) { ?>
          <input type="text" autocomplete="off" class="form-control fields" id="<?= $property['code'] ?>" placeholder=""
                 data-toggle="tooltip" data-placement="right" title=""
                 name="property[<?= $property['code'] ?>][value][]"
                 value="<?= isset( $valueProp[0] ) ? $valueProp[0]['value'] : '' ?>">
          <input type="hidden" autocomplete="off" class="form-control fields" id="<?= $property['code'] ?>"
                 placeholder="" data-toggle="tooltip" data-placement="right" title=""
                 name="property[<?= $property['code'] ?>][id][]"
                 value="<?= isset( $valueProp[0] ) ? $valueProp[0]['id'] : '' ?>">
          <?php if ( $property['description_field'] == 1 ) { ?>
            <input type="text" autocomplete="off" class="form-control fields" id="<?= $property['code'] ?>"
                   placeholder="Описание" data-toggle="tooltip" data-placement="right" title=""
                   name="property[<?= $property['code'] ?>][description][]"
                   value="<?= isset( $valueProp[0] ) ? $valueProp[0]['description'] : '' ?>">
          <?php }
        } else {
          if ( count( $valueProp ) == 0 ) { ?>
            <input type="text" autocomplete="off" class="form-control fields" id="<?= $property['code'] ?>"
                   placeholder="" data-toggle="tooltip" data-placement="right" title=""
                   name="property[<?= $property['code'] ?>][value][]"
                   value="<?= isset( $_POST['property'][$property['code']]['value'][0] )
                     ? $_POST['property'][$property['code']]['value'][0] : '' ?>">
            <input type="hidden" autocomplete="off" class="form-control fields" id="<?= $property['code'] ?>"
                   placeholder="" data-toggle="tooltip" data-placement="right" title=""
                   name="property[<?= $property['code'] ?>][id][]" value="">
            <?php if ( $property['description_field'] == 1 ) { ?>
              <input type="text" autocomplete="off" class="form-control fields" id="<?= $property['code'] ?>"
                     placeholder="Описание" data-toggle="tooltip" data-placement="right" title=""
                     name="property[<?= $property['code'] ?>][description][]" value="">
            <?php }
          } else {
            $count = 0;
            foreach ( $valueProp as $key => $field_val ) { ?>
              <input type="text" autocomplete="off" class="form-control <?= $count == 0 ? 'fields' : '' ?>"
                     id="<?= $property['code'] ?>" placeholder="" data-toggle="tooltip" data-placement="right" title=""
                     name="property[<?= $property['code'] ?>][value][]" value="<?= $field_val['value'] ?>">
              <input type="hidden" autocomplete="off" class="form-control <?= $count == 0 ? 'fields' : '' ?>"
                     id="<?= $property['code'] ?>" placeholder="" data-toggle="tooltip" data-placement="right" title=""
                     name="property[<?= $property['code'] ?>][id][]" value="<?= $field_val['id'] ?>">
              <?php if ( $property['description_field'] == 1 ) { ?>
                <input type="text" autocomplete="off" class="form-control <?= $count == 0 ? 'fields' : '' ?>"
                       id="<?= $property['code'] ?>" placeholder="Описание" data-toggle="tooltip" data-placement="right"
                       title="" name="property[<?= $property['code'] ?>][description][]"
                       value="<?= $field_val['description'] ?>">
              <?php } ?>
              <?php $count++;
            }
          } ?>
          <?php if ( $property['multi'] ) { ?>
            <a class="btn btn-default add_field">Еще</a>
          <?php } ?>
        <?php } ?> </div>
    <?php } ?>
    <?php if ( $property['type'] == 'L' ) { ?>
      <div class="form-group <?= is_array( $error ) && isset( $error[$property['code']] ) ? ' has-error has-feedback'
        : ''; ?> ">
        <label for="name"><?= $property['name'] ?> <?php if ( $property['required'] == 1 ) { ?><em>*</em><?php } ?>
        </label>
        <?php
        $valuesSelect       = \Application\Models\PropertyEnum::find( [ 'property_id' => $property['id'] ] );
        $id_values['value'] = [];
        $id_values['id']    = [];
        $description        = '';
        foreach ( $valueProp as $fieldValue ) {
          $id_values['value'][]                       = $fieldValue['value_enum'];
          $id_values['id'][$fieldValue['value_enum']] = $fieldValue['id'];
          $description                                = $fieldValue['description'];
        }
        //
        if ( $property['template'] == 'S' ) {
          foreach ( $valuesSelect as $valuesSel ) { ?>
            <input type="checkbox" <?= in_array( $valuesSel['id'], $id_values['value'] )
              ? ' checked '
              : ''
            ?> name="property[<?= $property['code'] ?>][value][]" value="<?= $valuesSel['id'] ?>">
            <input type="hidden" name="property[<?= $property['code'] ?>][id][]"
                   value="<?= isset( $id_values['id'][$valuesSel['id']] ) ? $id_values['id'][$valuesSel['id']]
                     : '' ?>"><?= $valuesSel['value'] ?><br>
          <?php }
        } elseif ( $property['template'] == 'F' ) { ?>
          <?php foreach ( $valuesSelect as $valuesSel ) { ?>
            <input type="radio" name="property[<?= $property['code'] ?>][value][]"
                   value="<?= $valuesSel['id'] ?>" <?= in_array( $valuesSel['id'], $id_values['value'] )
              ? ' checked '
              : ''
            ?>> <?= $valuesSel['value'] ?>
          <?php } ?>
        <?php } else { ?>
          <select class="form-control fields" <?= $property['multi'] ? 'multiple' : '' ?> id="<?= $property['code'] ?>"
                  name="property[<?= $property['code'] ?>][value][]">
            <option value="">-- Выбрать --</option>
            <?php foreach ( $valuesSelect as $valuesSel ) { ?>
              <option value="<?= $valuesSel['id'] ?>"
                <?= in_array( $valuesSel['id'], $id_values['value'] )
                  ? ' selected '
                  : ''
                ?>
              ><?= $valuesSel['value'] ?></option>
            <?php } ?>
          </select>
          <?php if ( !$property['multi'] ) foreach ( $valueProp as $val ) { ?>
            <input type="hidden" name="property[<?= $property['code'] ?>][id][]" value="<?= $val['id'] ?>">
          <?php } ?>
          <?php if ( $property['description_field'] == 1 ) { ?>
            <input type="text" class="form-control fields" id="<?= $property['code'] ?>" placeholder="Описание"
                   data-toggle="tooltip" data-placement="right" title=""
                   name="property[<?= $property['code'] ?>][description][]" value="<?= $description ?>">
          <?php }
        } ?>
        <?php if ( is_array( $error ) && isset( $error[$property['code']] ) ): ?>
          <span class="glyphicon glyphicon-remove form-control-feedback"></span>
          <p class="help-block"><?= $error[$property['code']] != '' ? $error[$property['code']]
              : 'Поле обязательно для заполнения' ?></p>
        <?php endif; ?>
      </div>
    <?php } ?>
    <?php if ( $property['type'] == 'F' ) { ?>
    <div class="form-group <?= is_array( $error ) && isset( $error[$property['code']] ) ? ' has-error has-feedback'
      : ''; ?> ">
      <label for="name"><?= $property['name'] ?> <?php if ( $property['required'] == 1 ) { ?><em>*</em><?php } ?>
      </label>
      <?php if ( !$property['multi'] ) { ?>
        <input type="file" autocomplete="off" class="form-control fields"
               id="file<?= isset( $valueProp[0] ) ? $valueProp[0]['id'] : '' ?>" placeholder="" data-toggle="tooltip"
               data-placement="right" title="" name="property[<?= $property['code'] ?>][value][]" value="">
        <input type="hidden" class="form-control fields" placeholder="" data-toggle="tooltip" data-placement="right"
               title="" name="property[<?= $property['code'] ?>][hidden][]" value="">
        <input type="hidden" class="form-control fields" id="" placeholder="" data-toggle="tooltip"
               data-placement="right" title="" name="property[<?= $property['code'] ?>][id][]"
               value="<?= isset( $valueProp[0] ) ? $valueProp[0]['id'] : '' ?>">
        <?php if ( $property['description_field'] == 1 ) { ?>
          <input type="text" class="form-control fields" placeholder="Описание" data-toggle="tooltip"
                 data-placement="right" title="" name="property[<?= $property['code'] ?>][description][]" value="">
        <?php } ?>
        <img id="img<?= isset( $valueProp[0] ) ? $valueProp[0]['id'] : '' ?>"
             src="<?= isset( $valueProp[0] ) ? $valueProp[0]['value'] : '' ?>" />
        <?php if ( $property['description_field'] == 1 ) { ?>
          <div id="desc<?= $valueProp[0]['id'] ?>"><input type="text" class="form-control change_description_image"
                                                          data-id="<?= $valueProp[0]['id'] ?>"
                                                          value="<?= $valueProp[0]['description'] ?>"></div>
        <?php } ?>
        <?php if ( isset( $valueProp[0] ) ) { ?><a data-id="<?= isset( $valueProp[0] ) ? $valueProp[0]['id'] : '' ?>"
                                                   class="delete_image" >X</a> <?php } ?>
      <?php } else { ?>
        <input type="file" autocomplete="off" class="form-control fields" id="file" placeholder="" data-toggle="tooltip"
               data-placement="right" title="" name="property[<?= $property['code'] ?>][value][]" value="">
        <input type="hidden" class="form-control fields" placeholder="" data-toggle="tooltip" data-placement="right"
               title="" name="property[<?= $property['code'] ?>][hidden][]" value="">
        <?php if ( $property['description_field'] == 1 ) { ?>
          <input type="text" class="form-control fields" placeholder="Описание" data-toggle="tooltip"
                 data-placement="right" title="" name="property[<?= $property['code'] ?>][description][]" value="">
        <?php } ?>
        <?php foreach ( $valueProp as $img ) { ?>
          <img id="img<?= $img['id'] ?>" src="<?= $img['value'] ?>" alt="">
          <?php if ( $property['description_field'] == 1 ) { ?>
            <div id="desc<?= $img['id'] ?>"><input type="text" class="form-control change_description_image"
                                                   data-id="<?= $img['id'] ?>" value="<?= $img['description'] ?>"></div>
          <?php } ?>
          <a data-id="<?= $img['id'] ?>" class="delete_image">X</a>
        <?php } ?>
      <?php } ?>
      <?php if ( $property['multi'] ) { ?>
        <a class="btn btn-default add_field">Еще</a>
      <?php } ?>
      <?php if ( is_array( $error ) && isset( $error[$property['code']] ) ): ?>
        <span class="glyphicon glyphicon-remove form-control-feedback"></span>
        <p class="help-block"><?= $error[$property['code']] !== '' ? $error[$property['code']] : 'Выберите файл!' ?></p>
      <?php endif; ?>
      </div><?php } ?>
    <?php if ( $property['type'] == 'T' ) { ?>
      <div class="form-group <?= is_array( $error ) && isset( $error[$property['code']] ) ? ' has-error has-feedback'
        : ''; ?> ">
        <label for="name"><?= $property['name'] ?> <?php if ( $property['required'] == 1 ) { ?><em>*</em><?php } ?>
        </label>

        <?php if ( !$property['multi'] ) { ?>
          <textarea class="form-control fields" name="property[<?= $property['code'] ?>][value][]"
                    id="<?= $property['code'] ?>" cols="30" rows="10"><?= isset( $valueProp[0] )
              ? $valueProp[0]['value'] : '' ?></textarea>
          <input type="hidden" autocomplete="off" class="form-control fields" id="<?= $property['code'] ?>"
                 placeholder="" data-toggle="tooltip" data-placement="right" title=""
                 name="property[<?= $property['code'] ?>][id][]"
                 value="<?= isset( $valueProp[0] ) ? $valueProp[0]['id'] : '' ?>">
          <?php if ( $property['description_field'] == 1 ) { ?>
            <input type="text" autocomplete="off" class="form-control fields" id="<?= $property['code'] ?>"
                   placeholder="Описание" data-toggle="tooltip" data-placement="right" title=""
                   name="property[<?= $property['code'] ?>][description][]"
                   value="<?= isset( $valueProp[0] ) ? $valueProp[0]['description'] : '' ?>">
          <?php }
        } else {
          if ( count( $valueProp ) == 0 ) { ?>
            <textarea class="form-control fields" name="property[<?= $property['code'] ?>][value][]"
                      id="<?= $property['code'] ?>" cols="30" rows="10"></textarea>
            <input type="hidden" autocomplete="off" class="form-control fields" id="<?= $property['code'] ?>"
                   placeholder="" data-toggle="tooltip" data-placement="right" title=""
                   name="property[<?= $property['code'] ?>][id][]" value="">
            <?php if ( $property['description_field'] == 1 ) { ?>
              <input type="text" autocomplete="off" class="form-control <?= $count == 0 ? 'fields' : '' ?>"
                     id="<?= $property['code'] ?>" placeholder="Описание" data-toggle="tooltip" data-placement="right"
                     title="" name="property[<?= $property['code'] ?>][description][]" value="">
            <?php }
          } else {
            $count = 0;
            foreach ( $valueProp as $key => $field_val ) { ?>
              <textarea class="form-control  <?= $count == 0 ? 'fields' : '' ?>"
                        name="property[<?= $property['code'] ?>][value][]" id="<?= $property['code'] ?>" cols="30"
                        rows="10"><?= $field_val['value'] ?></textarea>
              <input type="hidden" autocomplete="off" class="form-control <?= $count == 0 ? 'fields' : '' ?>"
                     id="<?= $property['code'] ?>" placeholder="" data-toggle="tooltip" data-placement="right" title=""
                     name="property[<?= $property['code'] ?>][id][]" value="<?= $field_val['id'] ?>">
              <?php if ( $property['description_field'] == 1 ) { ?>
                <input type="text" autocomplete="off" class="form-control <?= $count == 0 ? 'fields' : '' ?>"
                       id="<?= $property['code'] ?>" placeholder="Описание" data-toggle="tooltip" data-placement="right"
                       title="" name="property[<?= $property['code'] ?>][description][]"
                       value="<?= $field_val['description'] ?>">
              <?php } ?>
              <?php $count++;
            }
          } ?>

          <?php if ( $property['multi'] ) { ?>
            <a class="btn btn-default add_field">Еще</a>
          <?php } ?>
          <?php if ( is_array( $error ) && isset( $error[$property['code']] ) ): ?>
            <span class="glyphicon glyphicon-remove form-control-feedback"></span>
            <p class="help-block"><?= $error[$property['code']] !== '' ? $error[$property['code']]
                : 'Выберите файл!' ?></p>
          <?php endif; ?>
        <?php } ?>
      </div>
    <?php } ?>
    <?php if ( $property['type'] == 'D' ) { ?>
    <div class="form-group <?= is_array( $error ) && isset( $error[$property['code']] ) ? ' has-error has-feedback'
      : ''; ?> ">
      <label for="name"><?= $property['name'] ?> <?php if ( $property['required'] == 1 ) { ?><em>*</em><?php } ?>
      </label>

      <?php if ( !$property['multi'] ) { ?>
        <input type="text" autocomplete="off" class="date-picker form-control fields" id="<?= $property['code'] ?>"
               placeholder="" data-toggle="tooltip" data-placement="right" title=""
               name="property[<?= $property['code'] ?>][value][]"
               value="<?= isset( $valueProp[0] ) ? $valueProp[0]['value'] : '' ?>">
        <input type="hidden" autocomplete="off" class="form-control fields" id="<?= $property['code'] ?>" placeholder=""
               data-toggle="tooltip" data-placement="right" title="" name="property[<?= $property['code'] ?>][id][]"
               value="<?= isset( $valueProp[0] ) ? $valueProp[0]['id'] : '' ?>">
        <?php if ( $property['description_field'] == 1 ) { ?>
          <input type="text" autocomplete="off" class="form-control fields" id="<?= $property['code'] ?>"
                 placeholder="Описание" data-toggle="tooltip" data-placement="right" title=""
                 name="property[<?= $property['code'] ?>][description][]"
                 value="<?= isset( $valueProp[0] ) ? $valueProp[0]['description'] : '' ?>">
        <?php }
      } else {
        if ( count( $valueProp ) == 0 ) { ?>
          <input type="text" autocomplete="off" class="date-picker form-control fields" id="<?= $property['code'] ?>"
                 placeholder="" data-toggle="tooltip" data-placement="right" title=""
                 name="property[<?= $property['code'] ?>][value][]" value="">
          <input type="hidden" autocomplete="off" class="form-control fields" id="<?= $property['code'] ?>"
                 placeholder="" data-toggle="tooltip" data-placement="right" title=""
                 name="property[<?= $property['code'] ?>][id][]" value="">
          <?php if ( $property['description_field'] == 1 ) { ?>
            <input type="text" autocomplete="off" class="form-control <?= $count == 0 ? 'fields' : '' ?>"
                   id="<?= $property['code'] ?>" placeholder="Описание" data-toggle="tooltip" data-placement="right"
                   title="" name="property[<?= $property['code'] ?>][description][]" value="">
          <?php }
        } else {
          $count = 0;
          foreach ( $valueProp as $key => $field_val ) { ?>
            <input type="text" autocomplete="off" class="date-picker form-control <?= $count == 0 ? 'fields' : '' ?>"
                   id="<?= $property['code'] ?>" placeholder="" data-toggle="tooltip" data-placement="right" title=""
                   name="property[<?= $property['code'] ?>][value][]" value="<?= $field_val['value'] ?>">
            <input type="hidden" autocomplete="off" class="form-control <?= $count == 0 ? 'fields' : '' ?>"
                   id="<?= $property['code'] ?>" placeholder="" data-toggle="tooltip" data-placement="right" title=""
                   name="property[<?= $property['code'] ?>][id][]" value="<?= $field_val['id'] ?>">
            <?php if ( $property['description_field'] == 1 ) { ?>
              <input type="text" autocomplete="off" class="form-control <?= $count == 0 ? 'fields' : '' ?>"
                     id="<?= $property['code'] ?>" placeholder="Описание" data-toggle="tooltip" data-placement="right"
                     title="" name="property[<?= $property['code'] ?>][description][]"
                     value="<?= $field_val['description'] ?>">
            <?php } ?>
            <?php $count++;
          }
        }
      } ?>

      <?php if ( $property['multi'] ) { ?>
        <a class="btn btn-default add_field">Еще</a>
      <?php } ?>
      </div><?php } ?>
  <?php } ?>
  <?php else: ?>
    В выбранной категории нет свойств
  <?php endif; ?>
</div>
<script>
  $( '.add_field' ).click( function () {
    $( this ).before( $( '.fields', $( this ).parent() ).clone().removeClass( 'fields' ).val( '' ) );
    if ( $( '.date-picker', $( this ).parent() ).length ) {
      $( '.date-picker' ).datetimepicker( {
        locale: 'ru',
        format: 'DD.MM.YYYY',
      } );
    }
  } );
  $( '.properties' ).on( 'click', '.delete_image', function () {
    var id = $( this ).data( 'id' );
    var _this = this;
    $.ajax( {
      type: "POST",
      url: '#',
      data: 'delete=image_prop&delete_id=' + id,
      dataType: 'json',
      beforeSend: function () {
        $( '.alert-form' ).remove();
        $( '.load-full-page' ).css( 'display', 'block' );
      }
    } )
      .done( function ( data ) {
        $( '.load-full-page' ).css( 'display', 'none' );
        $( _this ).remove();
        $( '#img' + id ).remove();
        $( '#file' + id ).val( '' );
        $( '#desc' + id ).val( '' );
      } )
      .error( function ( data ) {
        $( '.load-full-page' ).css( 'display', 'none' );
      } );
  } );
  $( '.properties' ).on( 'blur', '.change_description_image', function () {
    var description = $( this ).val();
    var id = $( this ).data( 'id' );
    $.ajax( {
      type: "POST",
      url: '#',
      data: 'change=description&description_image=' + description + '&id=' + id,
      dataType: 'json',
      beforeSend: function () {
        $( '.alert-form' ).remove();
        $( '.load-full-page' ).css( 'display', 'block' );
      }
    } )
      .done( function ( data ) {
        $( '.load-full-page' ).css( 'display', 'none' );
      } )
      .error( function ( data ) {
        $( '.load-full-page' ).css( 'display', 'none' );
      } );
  } );

</script>
<style>
  .form-group label {
    width: 100%;
  }

  .properties img {
    max-width: 70px;
  }
</style>