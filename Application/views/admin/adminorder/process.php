<?php
//echo '<pre>';
//var_dump($order);
//die;
?>
<div class="col-xs-12">
  <h1 class="col-xs-12 col-md-12 ">Обработка заказа #<?= $order['id']; ?></h1>
</div>
<div class="col-xs-12 clr"></div>
<div class="col-xs-12 col-md-12 order">
  <div class="row">
    <div class="col-md-12">
      <h4>Данные о заказе</h4>
      <ul>
        <?php foreach ( $order['order_additional'] as $product ) : ?>
          <p>id <?=$product['id']?></p>
          <p>Наименование <?=$product['name']?></p>
          <?php if($product['sku'] != ''): ?>
          <p>Артикул <?=$product['sku']?></p>
          <?php endif; ?>
        <?php endforeach; ?>
      </ul>
    </div>
  </div>
  <div class="row">
    <div class="col-md-6">
      <h4>Данные о покупателе</h4>
      <ul>
        <li>ФИО: <?=$order['name']?></li>
        <li>Номер телефона: <?=$order['phone']?></li>
        <li>E-mail: <?=$order['email']?></li>
        <li>Тип доставки: <?=$order['delivery_name']?></li>
        <li>Адрес доставки: <?=$order['address']?></li>
        <li>Способ оплаты: <?=$order['payment_name']?></li>
      </ul>
    </div>
    <div class="col-md-6">
      <h4>Данные о поставщике</h4>
    <?php if($supplier): ?>
      <ul>
        <li>Наименование: <?=$supplier['name']?></li>
        <li>Номер телефона: <?=$supplier['phone']?></li>
        <li></li>
      </ul>
      <?php else: ?>
        <p>Поставщик не установлен</p>
      <?php endif; ?>
    </div>
  </div>
  <div class="row">
    <div class="col-md-4">
      <p>Дата доставки *</p>
      <input type="text" class="changeDelivery form-control" value="<?=$order['delivery_date']?>">
    </div>
  </div>
  <div class="col-xs-12 clr"></div>
  <div class="row">
    <div class="col-md-6 col-md-offset-3">
      <a style="width: 100%" class="btn btn-default" href="/admin/appoint/<?=$order['id']?>">Назначить курьера</a>
    </div>
  </div>
</div>
<style>
  .order ul li {
    list-style: none;
  }
  .order ul {
    padding: 0;
  }
</style>
<script>
  $(document).ready(function(){
    $('.changeDelivery').datetimepicker({
      locale: 'ru',
      format: 'DD-MM-YYYY'
    });
  });
  $('.changeDelivery').on('change blur inp', function(){
    $.ajax({
      url: '/admin/order/changeDelivery/<?=$order['id']?>',
      data:'date='+$(this).val(),
      type: 'POST',
      success: function(data){
      },
      error: function(err){

      }
    });
  });
</script>