<div id="page_product">
  <?php
  foreach ( $products as $product ): ?>
    <?= $this->block( 'adminorder/product_template', [ 'product' => $product ] ) ?>
  <?php endforeach; ?>
</div>
<br><br><br>

<script>
  $( '.add_order_product' ).on( 'click', function ( e ) {
    e.preventDefault();
    $
    return false;
  } );
</script>

<style>
  .block {
    display: inline-block;
    margin-bottom: 50px;
  }

  @font-face {
    font-family: 'icomoon';
    src: url("/media/css/default/fonts/icomoon.eot");
    src: url("/media/css/default/fonts/icomoon.eot") format("embedded-opentype"),
    url("/media/css/default/fonts/icomoon.ttf") format("truetype"),
    url("/media/css/default/fonts/icomoon.woff") format("woff"),
    url("/media/css/default/fonts/icomoon.svg") format("svg");
    font-weight: normal;
    font-style: normal
  }

  .list_colors {
    max-height: 70px
  }

  .list_colors li {

    width: 50px;
    height: 28px;
    margin-right: 2%;
    margin-top: 5px;
    display: inline-block;
    background-color: #000;
    border-radius: 10px;
    position: relative;
    transition: all .2s

  }

  .list_colors li:hover {
    opacity: .9
  }

  .list_colors li > label {
    position: absolute;
    left: 0;
    display: inline-block;
    width: 100%;
    min-width: 50px;
    height: 100%;
    color: transparent
  }

  .list_colors li > label:before {
    display: none
  }

  .list_colors li > input {
    margin-top: 0;
    display: none;
  }

  .list_colors li > input:checked + label:before {
    display: none
  }

  .list_colors li > input:checked + label:after {
    content: '\e908';
    font-family: 'icomoon' !important;
    color: #fff;
    display: inline-block;
    width: 100%;
    text-align: center;
    margin-top: 4px;
  }

  .list__radio input {
    display: none
  }

  .list__radio li {
    list-style: none;
    float: left;
    margin-right: 5px;
  }

  .list__radio input:checked + label {
    background: #d94140;
    color: white;
    border: none;
    height: 19px
  }

  .list__radio label {
    left: 0;
    text-align: center;
    display: inline-block;
    width: 40px;
    height: 20px;
    line-height: 19px;
    text-transform: uppercase;
    background: white;
    border: 1px solid #d1d1d2;
    transition: all .2s
  }

  .list__radio label:before {
    display: none
  }

  .list__radio label:hover {
    border: 1px solid #b7b7b9
  }

  .list_colors label:hover, .list__radio label:hover {
    cursor: pointer;
  }

</style>
