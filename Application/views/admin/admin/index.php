<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<h1 class="colx-xs-12 col-md-12 ">Административная панель</h1>
<div class="col-xs-12 clr"></div>
<div class="row">
  <div class="col-md-6">
      <b>Общий финансовый оборот: </b> <?=$sum?>
  </div>
    <div class="col-md-6">
        <b>Выручка за обслуживание: </b> 0<?//=$service?>
    </div>
    <div class="col-md-6">
        <p>Количество проданых сайтов за месяц</p>
        <div id="site"></div>
    </div>
    <div class="col-md-6">
        <p>Количество проданных единиц товаров за месяц</p>
        <div id="order"></div>
    </div>
</div>
<script>
    google.charts.load('current', {'packages':['corechart']});
    google.charts.setOnLoadCallback(drawChart);

    function drawChart() {
        var data = google.visualization.arrayToDataTable([
            ['Дни', 'Количество'],
            <?php
          $countDay = cal_days_in_month(CAL_GREGORIAN, date('m'), date('Y'));
            for($i = 1; $i <= $countDay; $i++){
              $countShop = 0;
                if(isset($countShops[$i])){
                    $countShop = count($countShops[$i]);
                }
              echo "[$i,$countShop],";
            }
          ?>
        ]);

        var options = {
            title: 'Статистика за последний месяц',
            hAxis: {title: '',  titleTextStyle: {color: '#333'}},
            vAxis: {minValue: 0}
        };

        var chart = new google.visualization.AreaChart(document.getElementById('site'));
        chart.draw(data, options);
    }
    google.charts.load('current', {'packages':['corechart']});
    google.charts.setOnLoadCallback(drawChartProduct);

    function drawChartProduct() {
        var data = google.visualization.arrayToDataTable([
            ['Дни', 'Количество'],
            <?php
          $countDay = cal_days_in_month(CAL_GREGORIAN, date('m'), date('Y'));
            for($i = 1; $i <= $countDay; $i++){
              $countProduct = 0;
                if(isset($countOrders[$i])){
                  $countProduct = $countOrders[$i];
                }
              echo "[$i,$countProduct],";
            }
          ?>
        ]);

        var options = {
            title: 'Статистика за последний месяц',
            hAxis: {title: '',  titleTextStyle: {color: '#333'}},
            vAxis: {minValue: 0}
        };

        var chart = new google.visualization.AreaChart(document.getElementById('order'));
        chart.draw(data, options);
    }
</script>