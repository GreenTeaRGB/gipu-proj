<div class="form-group ">
  <?php if ( is_array( $item[$field] ) ): ?>
    <p><b><?= $options['label'] ?>: </b></p>
    <ul>
      <?php foreach ( $item[$field] as $element ) : ?>
        <li><?= $element ?></li>
      <?php endforeach; ?>
    </ul>
  <?php elseif ( is_numeric( $item[$field] ) ) :
    switch ( $item[$field] ) {
      case 0 :
        $value = 'Нет';
        break;
      case 1 :
        $value = 'да';
        break;
      default:
        $value = $item[$field];
    }?>
    <p><b><?= $options['label'] ?>: </b><?= $value ?></p>
  <?php else: ?>
    <p><b><?= $options['label'] ?>: </b><?= $item[$field] ?></p>
  <?php endif; ?>
  <input type="hidden" name="<?=$field?>" value="<?=$item[$field]?>">
</div>