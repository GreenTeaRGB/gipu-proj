<div class="form-group <?= is_array( $errors ) && isset( $errors[ $field ] ) ? 'has-error has-feedback' : ''; ?>">
  <label for="title"><?= $options[ 'label' ]; ?> <?= $options[ 'required' ] ? '<em>*</em>' : ''; ?></label>
  <select class="form-control" multiple size="10" name="<?= $field; ?>[]">
		<?php foreach( $options[ 'values' ] as $key => $value ) { ?>
      <option value="<?= $value[ $options[ 'id_key' ] ] ?>"
				<?= (!$result &&  isset($model->$field) ? $model->$field : $item[$field]) ? in_array( $value[ $options[ 'id_key' ] ], $item[ $field ] ) ? 'selected' : '' : '' ?>>
				<?= $value[ $options[ 'name_key' ] ] ?>
      </option>
		<?php } ?>
  </select>
	<?php if( is_array( $errors ) && isset( $errors[ $field ] ) ): ?>
    <span class="glyphicon glyphicon-remove form-control-feedback"></span>
        <p class="help-block"><?=$errors[$field] !== ''? $errors[$field] : 'Обязательное поле'?></p>
	<?php endif; ?>
</div>
