<div class="form-group">
  <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#products">
		<?= $options[ 'label' ]; ?>
  </button>
  <table id="related_category_list" class="table table-bordered" style="display: none;">
    <tbody>
    <tr>
      <th>Категория</th>
      <th></th>
    </tr>
		<?php if( count( $item[ 'related_categories' ] ) > 0 ): ?>
			<?php foreach( $item[ 'related_categories' ] as $categoryItem ): ?>
        <tr class="">
          <td><?= $categoryItem[ 'category_name' ]; ?></td>
          <td><input type="hidden" name="related_category_ids[]" class="related_categories"
                     value="<?= (int) $categoryItem[ 'category_id' ]; ?>"><a class="add added"
                                                                             data-id="<?= $categoryItem[ 'category_id' ]; ?>">Удалить</a>
          </td>
        </tr>
			<?php endforeach; ?>
		<?php endif; ?>
    </tbody>
  </table>

  <table id="related_product_list" class="table table-bordered" style="display: none;">
    <tbody>
    <tr>
      <th>Наименование Товара</th>
      <th>Артикул товара</th>
      <th></th>
    </tr>
		<?php if( count( $item[ 'related_products' ] ) > 0 ): ?>
			<?php foreach( $item[ 'related_products' ] as $product ): ?>
				<?php $prodImage = Product::getImage( $product[ 'product_images' ] ); ?>
        <tr class="">
          <td><img width="30px" src="<?= $prodImage; ?>"><?= $product[ 'product_name' ]; ?></td>
          <td><?= $product[ 'product_article' ]; ?></td>
          <td><input type="hidden" name="related_product_ids[]" class="related_products"
                     value="<?= (int) $product[ 'product_id' ]; ?>"><a class="add added"
                                                                       data-id="<?= $product[ 'product_id' ]; ?>">Удалить</a>
          </td>
        </tr>
			<?php endforeach; ?>
		<?php endif; ?>
    </tbody>
  </table>

  <div class="modal fade" id="products" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title" id="myModalLabel"><?= $options[ 'modal_title' ]; ?></h4>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-md-3"> <?= Views::buildAdminProducts( Category::getFullList(), $level = 1, $pid = 0, $showInput = true ) ?></div>
            <div class="col-md-9">
              <div id="content_products"></div>
              <div class="load-full-page"><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i></div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
          <button type="button" class="btn btn-primary add-btn" data-dismiss="modal">Добавить</button>
        </div>
      </div>
    </div>
  </div>
  <script>
      var cloneTableRow = function (row, cloneTo) {
          var row = row.clone();
          row.addClass('row-hidden');

          $(cloneTo).append(row);
          toggleTable($(cloneTo).parent());
      };

      var removeRowFromTable = function (id, removeFrom) {
          var row = $(removeFrom).find('[data-id="' + id + '"]').parent().parent();

          row.remove();
          toggleTable($(removeFrom).parent());
      };


      $(document).ready(function () {
          $('.show-next').on('click', function () {
              var item = $(this).parent().children('ul');
              if (item.is(':visible') == true) {
                  $(this).html('+');
                  item.fadeOut(0);
              }
              else {
                  $(this).html('-');
                  item.fadeTo(0, 1);
              }
          });

          $('.category_name').click(function () {
              $('.active_category').removeClass('active_category');
              $(this).addClass('active_category');
              var id = $(this).data('id');
              $.ajax({
                  url: '/admin/blog/load',
                  data: 'get_product=' + id,
                  type: 'POST',
                  beforeSend: function () {
                      $('.alert-form').remove();
                      $('.load-full-page').css('display', 'block');
                  },
                  success:
                      function (data) {
                          $('.load-full-page').css('display', 'none');

                          $('#content_products').html('');
                          $('#content_products').append(data);
                      },
                  error:
                      function (data) {
                          $('.load-full-page').css('display', 'none');
                          alert('Ошибка при выполнении операции');
                      }
              });
          });

          $('.modal').on('click', 'a.add', function () {
              $(this).toggleClass('added');

              if ($(this).hasClass('added')) {
                  $(this).text('Удалить');

                  cloneTableRow($(this).parent().parent(), '#related_product_list tbody');
              }
              else {
                  $(this).text('Добавить');

                  removeRowFromTable($(this).data('id'), '#related_product_list tbody');
              }

              return false;
          });

          $('#products').on('hidden.bs.modal', function () {
              if ($('#related_product_list .row-hidden')) {
                  var rows = $('#related_product_list .row-hidden');
                  rows.each(function (index, row) {
                      $(row).remove();
                  });
              }

              if ($('#related_category_list .row-hidden')) {
                  var rows = $('#related_category_list .row-hidden');
                  rows.each(function (index, row) {
                      $(row).remove();
                  });
              }

              $('input.related_categories').removeAttr('checked');

              $('.categories-filter span.category_name.active_category').removeClass('active_category');
              $('#content_products table').remove();
          });

          $('.add-btn').on('click', function () {
              if ($('#related_product_list .row-hidden')) {

                  var rows = $('#related_product_list .row-hidden');
                  rows.each(function (index, row) {
                      $(row).removeClass('row-hidden');
                  })
              }

              if ($('#related_category_list .row-hidden')) {

                  var rows = $('#related_category_list .row-hidden');
                  rows.each(function (index, row) {
                      $(row).removeClass('row-hidden');
                  })
              }

              $('#products').modal('hide');

              return false;
          });

          $('#related_product_list').on('click', '.added', function () {
              $(this).parent().parent().remove();

              toggleTable($('#related_product_list'));

              return false;
          });

          $('input.related_categories').on('change', function () {
              if ($(this).is(":checked")) {
                  var categoryName = $(this).prev().text();
                  var categoryId = $(this).val();

                  var row = generateCategoryRow(categoryName, categoryId);
                  cloneTableRow(row, '#related_category_list tbody');

              }
              else {
                  removeRowFromTable($(this).data('catid'), '#related_category_list tbody');
              }
          });

          $('#related_category_list').on('click', '.added', function () {
              $(this).parent().parent().remove();

              toggleTable($('#related_category_list'));

              return false;
          });
      });

      var generateCategoryRow = function (categoryName, categoryId) {
          var row = $('<tr />');
          $('<td />', {text: categoryName}).appendTo(row);

          $('<td />').append('<input type="hidden" name="related_category_ids[]" class="related_categories" value="' + categoryId + '"><a class="add added" data-id="' + categoryId + '">Удалить</a>').appendTo(row);

          return row;
      };

      var toggleTable = function (table) {
          if (table.find('tr').length > 1) {
              table.show();
          }
          else {
              table.hide();
          }
      };

      $(window).load(function () {
          var relatedCategoriesData = $('#related_category_list tbody tr');
          var relatedProductsData = $('#related_product_list tbody tr');

          if (relatedCategoriesData.length > 1) {
              relatedCategoriesData.parent().parent().show();
          }

          if (relatedProductsData.length > 1) {
              relatedProductsData.parent().parent().show();
          }
      });
  </script>
  <style>
    @media (min-width: 768px) {
      .modal-dialog {
        width: 90% !important;
      }
    }

    .colors {
      width: auto !important;
    }

    .sizes {
      width: auto !important;
    }

    .category_name:hover {
      color: #0000F0;
      cursor: pointer;
    }

    .active_category {
      color: #0000F0;
    }

    .product {
      padding: 0 5px;
    }

    .product.active {
      background: green;
    }
  </style>
</div>