<?php
$class       = new $options['class']();
$lim         = 15;
$reflection  = new \ReflectionClass( $class );
$modalFields = $class->getFields();
if ( !$result && isset( $model->$field ) ) {
  $item[$field] = (array)$model->$field;
} else {
  $item[$field] = (array)$item[$field];
}
$element    = $class->where( [ 'id' => $item[$field] ] )->fetchAll();
$modelThis  = strtolower( $reflection->getShortName() );
$id         = uniqid( 'id' );
$head       = [];
$link       = '';
$attributes = method_exists( $class, 'attributeLabels' )
  ? $class->attributeLabels()
  : [];
$info       = isset( $options['info'] )
  ? $options['info']
  : false;
foreach ( $options['fields'] as $it ) {
  $head[$it] = $modalFields[$it]['label'];
}
$multiple = isset( $options['multiple'] ) && $options['multiple'];

?>
<script src="/media/js/admin/modal_field.js"></script>
<div style="margin: 25px 0;" class="form-inline <?= is_array( $errors ) && isset( $errors[$field] )
  ? 'has-error has-feedback'
  : ''; ?>">
  <div class="form-group element<?= $id ?>" >
    <?php
    if(count($item[$field]) == 0) $item[$field][] = '';
    foreach ( $item[$field] as $element_id ) :
      $link = '';
      foreach ( $options['fields'] as $it ) {
        if ( $element_id == 0 ) break;
        $link .= $element[$element_id][$it] . ' ';
      }
      ?>
      <div class="element">
        <label for="title"><?= $options['label']; ?> <?= $options['required']
            ? '<em>*</em>'
            : ''; ?></label>
        <input disabled class="form-control input-sm" id="<?= $field; ?>_name" type="text"
               value="<?= $link ?>">
        <input name="<?= $field; ?><?= $multiple ? '[]' : '' ?>" class="form-control input-sm" id="<?= $field; ?>"
               type="hidden"
               value="<?= $element_id; ?>">
        <button type="button" class="btn btn-default btn-sm btn-modal"  data-toggle="modal"
                data-parent="<?= $field ?>"
                data-target="#<?= $id ?>">...
        </button>
        <a href="/admin/<?= $modelThis ?>/edit/<?= $element_id ?>" class="btn btn-default btn-sm" target="_blank">Подробнее</a>
        <button class="btn btn-danger btn-sm deleteElement<?= $id ?>">Х</button>
      </div>

      <?php if ( $info !== false && $item[$field] ): ?>
      <ul>
        <?php
        if($element_id != false)
          foreach ( $info as $i ) :?>
          <li><strong><?= isset( $attributes[$i] )
                ? $attributes[$i]
                : $i ?>: </strong> <?= $element[$element_id][$i] ?></li>
        <?php endforeach; ?>
      </ul>
    <?php endif; ?>
    <?php endforeach; ?>
    <?php if ( $multiple ): ?>
      <button id="add<?= $id ?>" class="btn btn-default add_modal_button">Еще</button>
    <?php endif; ?>
    <?php if ( is_array( $errors ) && isset( $errors[$field] ) ): ?>
      <span class="glyphicon glyphicon-remove form-control-feedback"></span>
      <p class="help-block"><?= $errors[$field] !== ''
          ? $errors[$field]
          : 'Обязательное поле' ?></p>
    <?php endif; ?>
  </div>

</div>

<div class="modal fade" id="<?= $id ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
            aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title">Поиск</h4>
      </div>
      <div class="modal-body">
        <div class="form-inline">
          <input type="text" class="form-control input-modal" style="width: 70%;" placeholder="Поиск...">
          <button type="button" class="search btn btn-default">Полный поиск</button>
        </div>
        <table id="modal_table" class="table table-hover">

        </table>
        <div class="pagination">

        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
        <button type="button" class="btn btn-primary select_id" data-dismiss="modal" data-id="<?= $field ?>">
          Выбрать
        </button>
      </div>
    </div>
  </div>
</div>
<script>

  var modal = new ModalClass( {
    model: '<?=$modelThis?>',
    //elements: <?//=json_encode( $elements )?>//,
    elements: {},
    //count: <?//=$count?>//,
    fields: <?=json_encode( $options['fields'] )?>,
    id: '<?=$id?>',
    field: '<?=$field?>',
    head: <?=json_encode( $head )?>,
    url: '<?=strtolower( $url )?>',
    limit: <?=$lim?>,
    info: <?=( $info
      ? json_encode( $info )
      : 'false' )?>,
    attributes: <?=json_encode( $attributes )?>
  } );
</script>