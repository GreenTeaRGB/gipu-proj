<div class="form-group <?= is_array( $errors ) && isset( $errors[$field] ) ? 'has-error has-feedback' : ''; ?>">
  <label for="title"><?= $options['label']; ?> <?= $options['required'] ? '<em>*</em>' : ''; ?></label>
  <input name="<?= $field; ?><?= $options['multiple'] ? '[]' : ''; ?>" class="form-control input-sm" id="<?= $field; ?>"
         type="file" value="" <?= $options['multiple'] ? 'multiple'
    : ''; ?> <?= isset( $options['file-type'] ) ? 'accept="' . $options['file-type'] . '"' : ''; ?>>

  <?php if ( $options['type'] == 'file' ): ?>

    <div class="builder-files">
      <?php if ( $options['multiple'] === false ): ?>
        <?php if ( is_file( FOLDER . $item[$field] ) ): ?>
          <?php if ( $options['file-type'] == 'image/*' ): ?>
            <div class="image-builder single-image">
              <img src="<?= $item[$field]; ?>" class="img-thumbnail" />
              <a class="btn btn-xs btn-danger delete-file" data-field="<?= $field; ?>"
                 data-file="<?= $item[$field]; ?>"><i class="fa fa-trash-o"></i></a>
              <input type="hidden" name="is_<?=$field?>">
            </div>
          <?php else: ?>
            <p class="help-block file-item">
              <a href="<?= $item[$field]; ?>" target="_blank">Файл <?= $item[$field]; ?></a>
              <a class="btn btn-xs btn-danger delete-file" data-field="<?= $field; ?>"
                 data-file="<?= $item[$field]; ?>"><i class="fa fa-trash-o"></i></a>
              <input type="hidden" name="is_<?=$field?>">
            </p>
          <?php endif; ?>
        <?php endif; ?>
      <?php else: ?>
        <?php $files = $item[$field];
        if ( is_array( $files ) ):?>

          <?php foreach ( $files as $file ): ?>
            <?php if ( $options['file-type'] == 'image/*' ): ?>
              <div class="image-builder">
                <img src="<?= $file; ?>" class="img-thumbnail" />
                <a class="btn btn-xs btn-danger delete-file" data-field="<?= $field; ?>" data-file="<?= $file; ?>"><i
                    class="fa fa-trash-o"></i></a>
                <input type="hidden" name="is_<?=$field?>">
              </div>
            <?php else: ?>
              <p class="help-block file-item">
                <a href="<?= $file; ?>" target="_blank">Файл <?= $file; ?></a>
                <a class="btn btn-xs btn-danger delete-file" data-field="<?= $field; ?>" data-file="<?= $file; ?>"><i
                    class="fa fa-trash-o"></i></a>
                <input type="hidden" name="is_<?=$field?>">
              </p>
            <?php endif; ?>
          <?php endforeach; ?>

        <?php endif; ?>
      <?php endif; ?>
    </div>

  <?php endif; ?>

  <?php if ( is_array( $errors ) && isset( $errors[$field] ) ): ?>
    <span class="glyphicon glyphicon-remove form-control-feedback"></span>
    <p class="help-block"><?= $errors[$field] !== '' ? $errors[$field] : 'Выберите файл!' ?></p>
  <?php endif; ?>

</div>