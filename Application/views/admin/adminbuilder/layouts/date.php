<?php if(isset($options['unix']) && $options['unix'] == true) $item[$field] = date('d.m.Y', $item[$field]) ?>
<div class="form-group  <?=is_array($errors)&&isset($errors[$field]) ? 'has-error has-feedback' : '';?>">
	<label for="title"><?=$options['label'];?> <?=$options['required'] ? '<em>*</em>' : '';?></label>
	<input name="<?=$field;?>" class="form-control input-sm date-picker" id="<?=$field;?>" type="text" value="<?=!$result &&  isset($model->$field) ? $model->$field : $item[$field];?>" >
	<?php if(is_array($errors)&&isset($errors[$field])):?>
		<span class="glyphicon glyphicon-remove form-control-feedback"></span>
        <p class="help-block"><?=$errors[$field] !== ''? $errors[$field] : 'Обязательное поле'?></p>
	<?php endif;?>
</div>
<script>
    $(document).ready(function(){
        $('.date-picker').datetimepicker({
            locale: 'ru',
            format: 'DD.MM.YYYY',

        });
    });
</script>