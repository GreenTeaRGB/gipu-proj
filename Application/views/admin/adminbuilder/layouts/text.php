<?php $id = uniqid('redactor'); ?>
<div class="form-group <?=is_array($errors)&&isset($errors[$field]) ? 'has-error has-feedback' : '';?>">
	<label for="title"><?=$options['label'];?> <?=$options['required'] ? '<em>*</em>' : '';?></label>
	<textarea class="form-control" id="<?=$id?>" rows="10" name="<?=$field;?>"><?=!$result &&  isset($model->$field) ? $model->$field : $item[$field];?></textarea>
	<?php if(is_array($errors)&&isset($errors[$field])):?>
    <span class="glyphicon glyphicon-remove form-control-feedback"></span>
        <p class="help-block"><?=$errors[$field] !== ''? $errors[$field] : 'Обязательное поле'?></p>
	<?php endif;?>
</div>
<?php if(isset($options['html']) && $options['html'] == true): ?>
  <script>
    // $(document).ready(function() {
      $('#<?=$id?>').summernote({
        height: 300,
        tabsize: 2
      });
    // });
  </script>
<?php endif; ?>
