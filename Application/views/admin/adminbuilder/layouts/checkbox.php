<div class="form-group <?=is_array($errors)&&isset($errors[$field]) ? 'has-error has-feedback' : '';?>">
	<label for="title"><?=$options['label'];?> <?=$options['required'] ? '<em>*</em>' : '';?></label>
	<input name="<?=$field;?>" <?=(!$result &&  isset($model->$field) ? $model->$field : $item[$field]) == 1?' checked ':''?>  id="<?=$field;?>" type="checkbox" value="1" >
	<?php if(is_array($errors)&&isset($errors[$field])):?>
		<span class="glyphicon glyphicon-remove form-control-feedback"></span>
        <p class="help-block"><?=$errors[$field] !== ''? $errors[$field] : 'Обязательное поле'?></p>
	<?php endif;?>
</div>