<?php
$id = uniqid('id');
?>
<script>
  PoolFields.add('select', {
    options: <?=json_encode($options)?>,
    item: <?=json_encode($item)?>,
    field: <?=json_encode($field)?>,
    id: "<?=$id?>",
  })
</script>
<div class="form-group <?=is_array($errors)&&isset($errors[$field]) ? 'has-error has-feedback' : '';?>">
  <label><?=$options['label']?></label>
  <div id="<?=$id?>"></div>
    <?php if(is_array($errors)&&isset($errors[$field])):?>
<!--      <span class="glyphicon glyphicon-remove form-control-feedback"></span>-->
      <p class="help-block"><?=$errors[$field] !== ''? $errors[$field] : 'Обязательное поле'?></p>
    <?php endif;?>
  </div>

