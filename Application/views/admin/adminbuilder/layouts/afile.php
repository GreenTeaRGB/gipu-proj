<?php
$id = uniqid('id');
?>
<script>
  PoolFields.add('file', {
    options: <?=json_encode($options)?>,
    item: <?=json_encode($item)?>,
    field: <?=json_encode($field)?>,
    id: "<?=$id?>",
    url: "<?=$page_current['uri'];?>",
  })
</script>
<div class="form-group  <?=is_array($errors) && isset($errors[$field]) ? 'has-error has-feedback' : '';?>">
  <label><?=$options['label']?></label>
  <div id="<?=$id?>"></div>
  <?php if(is_array($errors)&&isset($errors[$field])):?>
  <p class="help-block"><?=isset($errors[$field]) && $errors[$field] !== ''? $errors[$field] : 'Обязательное поле'?></p>
  <?php endif ?>
</div>
