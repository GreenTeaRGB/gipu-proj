<?php $id = uniqid('id');
  ?>
  <div class="form-group">
<!--  <label for="">--><?//=$options['label']?><!--</label>-->
  <button class="transaction_confirm<?=$id?> btn btn-sm btn-default" data-id="<?=$item['id']?>"><?=$options['label']?></button>
</div>

<script>
$(document).ready(function(){
  $('.transaction_confirm<?=$id?>').on('click', function(e){
    e.preventDefault();
    $.ajax({
      method: 'POST',
      url: '<?=$options['url']?>'+$(this).data('id'),
      dataType: 'json',
      success: function(data){
        if(data.success){
          alert(data.message);
          if(data.redirect != undefined){
            window.location.href = data.redirect;
          }else{
            window.location.reload();
          }
        }else{
          alert(data.message);
        }
      },
      error: function(err){
        console.log(err);
      }
    })
    return false;
  });
});
</script>
