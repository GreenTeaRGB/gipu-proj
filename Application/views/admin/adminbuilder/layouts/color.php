<?php
$id = uniqid('id');
  $item[$field] =  !$result &&  isset($model->$field) ? $model->$field : $item[$field];
if(strpos($item[$field], ':') !== false)
list($r,$g,$b,$a) = explode(':', $item[$field]);
else
  list($r,$g,$b,$a) = [255,255,255,255];
if($item[$field] == '') {
  $item[$field] = '255:255:255:255';
}
?>
<div class="form-group <?=is_array($errors)&&isset($errors[$field]) ? 'has-error has-feedback' : '';?>">
  <label for="title"><?=$options['label'];?></label>
  <input type="hidden" data-id="<?=$id?>" name="<?=$field;?>" value="<?=$item[$field];?>">
  <div class="color" id="<?=$id?>">

  </div>
  <?php if(is_array($errors)&&isset($errors[$field])):?>
    <span class="glyphicon glyphicon-remove form-control-feedback"></span>
      <p class="help-block"><?=$errors[$field] !== ''? $errors[$field] : 'Обязательное поле'?></p>
  <?php endif;?>
</div>
<script>
    $('#<?=$id?>').jPicker(
        {
          window:
              {
                  expandable: true,
                  alphaSupport: true,
              },
            color:
                {
                    active: new $.jPicker.Color({ r:<?=$r?> ,g:<?=$g?> , b:<?=$b?> ,a:<?=$a?> }),
                },
        },function(color){
            var val = color.val();
            var rgba = val.r+':'+val.g+':'+val.b+':'+val.a;
            $('input[data-id="<?=$id?>"]').val(rgba);
        },
        function(color){
            var val = color.val();
            var rgba = val.r+':'+val.g+':'+val.b+':'+val.a;
            $('input[data-id="<?=$id?>"]').val(rgba);
        });
</script>