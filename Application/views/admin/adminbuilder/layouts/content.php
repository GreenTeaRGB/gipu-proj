<div class="form-group col-md-6">
  <label>Контент <?=$options['required'] ? '<em>*</em>' : '';?></label>
	<?php if(is_array($errors)&&isset($errors[$field])):?>
        <p class="help-block"><?=$errors[$field] !== ''? $errors[$field] : 'Обязательное поле'?></p>
	<?php endif;?>
	<?=Content::show_content(!$result &&  isset($model->$field) ? $model->$field : $item[$field]);?>
</div>

