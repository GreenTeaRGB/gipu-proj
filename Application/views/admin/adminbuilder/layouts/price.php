<?php
if(!isset($item['purchase_price']) || !$item['purchase_price']) $item['purchase_price'] = 0;
if(!isset($item['agent_price']) || !$item['agent_price']) $item['agent_price'] = 0;
if(!isset($item['price']) || !$item['price']) $item['price'] = 0;
?>
<div class="row prices">
    <div class="col-md-4">
        <div class="form-group <?=is_array($errors)&&isset($errors['purchase_price']) ? 'has-error has-feedback' : '';?>">
            <label for="title">Закупочная цена</label>
            <input name="purchase_price" class="form-control input-sm" id="purchase_price" type="text" value="<?= !$result && isset($model->purchase_price) ? $model->purchase_price : $item['purchase_price'];?>" >
          <?php if(is_array($errors)&&isset($errors['purchase_price'])):?>
              <span class="glyphicon glyphicon-remove form-control-feedback"></span>
              <p class="help-block"><?=$errors['purchase_price'] !== ''? $errors['purchase_price'] : 'Обязательное поле'?></p>
          <?php endif;?>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group <?=is_array($errors)&&isset($errors['agent_price']) ? 'has-error has-feedback' : '';?>">
            <label for="title">Цена агента</label>
            <input name="agent_price" class="form-control input-sm" id="agent_price" type="text" value="<?= !$result && isset($model->agent_price) ? $model->agent_price : $item['agent_price'];?>" >
          <?php if(is_array($errors)&&isset($errors['agent_price'])):?>
              <span class="glyphicon glyphicon-remove form-control-feedback"></span>
              <p class="help-block"><?=$errors['agent_price'] !== ''? $errors['agent_price'] : 'Обязательное поле'?></p>
          <?php endif;?>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group <?=is_array($errors)&&isset($errors[$field]) ? 'has-error has-feedback' : '';?>">
            <label for="title"><?=$options['label'];?> <?=$options['required'] ? '<em>*</em>' : '';?></label>
            <input name="<?=$field;?>" class="form-control input-sm" id="<?=$field;?>" type="text" value="<?= !$result &&  isset($model->$field) ? $model->$field : $item[$field];?>" >
          <?php if(is_array($errors)&&isset($errors[$field])):?>
              <span class="glyphicon glyphicon-remove form-control-feedback"></span>
              <p class="help-block"><?=$errors[$field] !== ''? $errors[$field] : 'Обязательное поле'?></p>
          <?php endif;?>
        </div>
    </div>
</div>
<?php

  $e = $item['agent_price'] - $item['purchase_price'];
  $f = ($item[$field] - $item['agent_price']) * 0.3;
?>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label for="">Разница цены агента с закупочной ценой</label>
            <input type="text" id="e" class="form-control input-sm" disabled="disabled" value="<?=$e?>">
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for="">Разница цены сайта<br> с ценой агента</label>
            <input type="text" id="f" class="form-control input-sm" disabled="disabled" value="<?=($item[$field] - $item['agent_price'])?>">
        </div>
    </div>
    <div class="col-md-12">
        <div class="form-group">
            <label for="">Доход</label>
            <input type="text" id="res" class="form-control" disabled="disabled" value="<?=($e-$f)?>">
        </div>
    </div>
</div>
<script>
    $('#agent_price, #purchase_price, #price').on('change input blur', function(e){
        e.preventDefault();
        if($('.prices .has-error').length != 0) return false;
        var e = parseFloat($('#agent_price').val()) - parseFloat($('#purchase_price').val());
        var f = parseFloat($('#price').val()) - parseFloat($('#agent_price').val());
        var res = f*0.3;
        if(isNaN(e) || isNaN(f) || isNaN(res)) return false;
        $('#e').val(e);
        $('#f').val(f);
        $('#res').val(e-res);
    });

    $('.form-group').on('change', '#category_id', function(){
       setTimeout(updatePropertyList(),100);
    });

    function updatePropertyList(){
      return function(){
        var id = $('input[name="category_id"]').val();
        if(id == '' || id == 0) return false;

        $.ajax({
          url: '/admin/product/getContentPropertiesByCategoryId/'+id<?=isset($item['id']) ? '+"/'.$item['id'].'"':''?>,
          type:'post',
          contentType: 'html',
          success:function(data){
            $('#divProperty').html('');
            $('#divProperty').html(data)
          },
          error: function(err){
            console.log(err);
          }
        });
      }
    }

   $(document).ready(function(){
       if($('#divProperty .has-error').length > 0){
           $('ul[role="tablist"] li').removeClass('active');
           $('div[role="tabpanel"]').removeClass('active');
           $('a[href="#divProperty"]').parent().addClass('active');
           $('#divProperty').addClass('active');
       }
   });

</script>