<?php $isError = is_array($errors)&&isset($errors[$field]); ?>
<div class="form-group <?=$isError ? 'has-error has-feedback' : '';?>">
	<label for="title"><?=$options['label'];?> <?=$options['required'] ? '<em>*</em>' : '';?></label>
	<input name="<?=$field;?>" class="form-control input-sm" id="<?=$field;?>" type="text" value='<?=!$result &&  isset($model->$field) ? $model->$field : $item[$field];?>' >
	<?php if($isError):?>
    <span class="glyphicon glyphicon-remove form-control-feedback"></span>
  <p class="help-block"><?=$errors[$field] !== ''? $errors[$field] : 'Обязательное поле'?></p>
  <?php endif;?>
</div>