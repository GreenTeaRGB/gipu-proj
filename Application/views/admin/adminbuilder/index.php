<?php
//    if(!function_exists('showValue')){
//      function showValue($value, $key, $values, $key_name)
//      {
//        foreach ($values as $i => $v)
//        {
//          if ($v[$key]==$value) break;
//        }
//        return $values[$i][$key_name];
//      }
//    }

$filterUrl = $_GET;
if ( isset( $filterUrl['sort'] ) ) unset( $filterUrl['sort'] );
if ( isset( $filterUrl['by'] ) ) unset( $filterUrl['by'] );
function echoVisibleFields( $item, $fieldSettings )
{
  if ( isset( $fieldSettings['visible_keys'] ) ) {
    foreach ( $fieldSettings['visible_keys'] as $visible_key ) {
      echo $item[$visible_key] . ' | ';
    }
  } else {
    if ( isset( $fieldSettings['color'] ) ) {
      $color = explode( ':', $item[$fieldSettings['color']] );
      echo '<span style="color: rgba(' . implode( ',', $color ) . ')">' . $item[$fieldSettings['name_key']] . '</span>';
    } else {
      echo $item[$fieldSettings['name_key']];
    }
  }
  echo '<br>';
}

?>
<h1 class="colx-xs-12 col-md-12 ">Управление <?= $page_current['name']; ?></h1>
<div class="col-xs-12 clr"></div>
<link href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="stylesheet"
      type="text/css" />
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.5/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js"></script>
<!--<script src="https://ilikenwf.github.io/jquery.mjs.nestedSortable.js"></script>-->
<script src="/media/js/admin/Sortable.js"></script>
<?php if ( !isset( $options['hide'] ) || ( isset( $options['hide'] ) && !in_array( 'create', $options['hide'] ) ) ): ?>
  <a href="/admin/<?= $page_current['uri']; ?>/create" class="btn btn-primary btn-sm">+ Добавить запись</a>
<?php endif; ?>
<a href="/admin/<?= $page_current['uri']; ?>" class="btn btn-primary btn-sm pull-right">Сбросить фильтр</a>

<div class="col-xs-12 clr"></div>
<?php if ( count( $tabs ) > 0 ) { ?>
  <div class="col-xs-12 clr"></div>
  <div class="col-md-12">
    <ul class="nav nav-tabs">
      <?php foreach ( $tabs as $tab ) { ?>
        <li <?= $_SERVER['REDIRECT_URL'] == $tab['link'] ? ' class="active" ' : '' ?>><a
            href="<?= $tab['link'] ?>"><?= $tab['name'] ?></a></li>
      <?php } ?>
    </ul>
  </div>
<?php } ?>
<?php if ( $tree ) {
  echo $treeHtml;
} else {
  $countFields = 0;
  $filter      = '';
  ?>
  <form action="">
    <table class="table table-bordered table-hover general-table sortable-table " id="tablesortable">
      <tr>
        <!--        <th></th>-->
        <!--        --><?php //if ( isset( $options['activate'] ) ) {
        //          $filter .= "<td></td>";
        //          ?>
        <!--          <th style="width:10px"></th>-->
        <!--        --><?php //} ?>
        <!--        --><?php //if ( isset( $options['sortable'] ) ) {
        //          $filter .= "<td></td>";
        //          ?>
        <!--          <th style="width:10px"></th>-->
        <!--        --><?php //} ?>
        <?php
        foreach ( $fields as $key => $field ):
          ?>
          <?php if ( $field['visible'] ): $countFields++; ?>
          <th class="<?= ( isset( $_GET['sort'] ) && $_GET['sort'] == $key ) ? ' active ' : '' ?> <?= $key == 'id'
            ? 'id-td' : '' ?>">
            <?php
            if ( !isset( $field['visible_keys'] ) && ( strpos( $key, ']' ) === false ) && !in_array( $key, $options ) && !in_array( $field['type'], [
                'file',
                'afile',
                'button'
              ] ) && !isset( $field['no_filter'] ) ) {
              if ( $field['type'] == 'select'
                || ( ( $field['type'] == 'selectTree'
                    || $field['type'] == 'aselect' )
                  && isset( $field['multiple'] )
                  && $field['multiple'] == false ) ) {
                $selectId = uniqid( 'id' );
                $dataJson = [
                  'options' => $field,
                  'id'      => $selectId,
                  'field'   => 'filter[' . $key . ']',
                  'item'    => [
                    'filter[' . $key . ']' => isset( $_GET['filter'][$key] ) ? $_GET['filter'][$key] : false
                  ]
                ];
                $json     = json_encode( $dataJson );
                $filter   .= '<td><script>PoolFields.add("selectTree", ' . $json . ')</script><div id="' . $selectId . '"></div></td>';
//                $optionSelect = '';
//                foreach ( $field['values'] as $vs ) {
//                  $optionSelect .= '<option value="' . $vs[$field['id_key']] . '" ' . ( isset( $_GET['filter'][$key] ) && $_GET['filter'][$key] == $vs[$field['id_key']]
//                      ? 'selected' : '' ) . '>' . $vs[$field['name_key']] . '</option>';
//                }
//                $filter .= '<td><select class="form-control" name="filter[' . $key . ']">
//                            <option value="">-- Выбрать --</option>
//                            ' . $optionSelect . '
//                            </select></td>';
              } elseif ( $field['type'] == 'mselect'
                || ( ( $field['type'] == 'aselect' )
                  && isset( $field['multiple'] )
                  && $field['multiple'] == true ) ) {
                $selectId = uniqid( 'id' );
                $dataJson = [
                  'options' => $field,
                  'id'      => $selectId,
                  'field'   => 'filter[' . $key . ']',
                  'item'    => [
                    'filter[' . $key . ']' => isset( $_GET['filter'][$key] ) ? $_GET['filter'][$key] : false
                  ]
                ];
                $json     = json_encode( $dataJson );
                $filter   .= '<td><script>PoolFields.add("select", ' . $json . ')</script><div id="' . $selectId . '"></div></td>';
              } else {
                if ( $field['type'] == 'date' ) {
                  $date  = true;
                  $width = 'width:50%; float: left;';
                  $value = isset( $_GET['filter'][$key]['from'] )
                    ? $_GET['filter'][$key]['from']
                    : '';
                } else {
                  $width = 'width:100%; float: left;';
                  $date  = false;
                  $value = isset( $_GET['filter'][$key] ) ? $_GET['filter'][$key] : '';
                }
                if ( isset( $field['visible_keys'] ) ) {
                  $field['label'] = '';
                  foreach ( $field['visible_keys'] as $visible_key ) {
                    $field['label'] .= $fields[$visible_key]['label'] . ' ';
                  }
                }
                $filter .= '
                        <td>
                        <input placeholder="' . $field['label'] . ( $date ? " от" : '' ) . '" 
                               style="' . $width . '" 
                               type="text"
                               value="' . $value . '"
                               name="filter[' . $key . ']' . ( $date ? '[from]' : '' ) . '" 
                               class="form-control ' . ( $date ? 'date-picker' : '' ) . '">' . ( $date ? '
                        <input placeholder="' . $field['label'] . ' до" 
                               style="' . $width . '" 
                               type="text"
                               value="' . ( isset( $_GET['filter'][$key]['to'] ) ? $_GET['filter'][$key]['to'] : '' ) . '" 
                               name="filter[' . $key . ']' . ( $date ? '[to]' : '' ) . '" 
                               class="form-control ' . ( $date ? 'date-picker' : '' ) . '">
                        ' : '' ) . '
                        </td>';
              }
            } else {
              $filter .= '<td></td>';
            }
            ?>
            <?php $by = isset( $_GET['by'] ) && $_GET['by'] == 'desc' ? 'asc' : 'desc'; ?>
            <a
              href="<?= $_SERVER['REDIRECT_URL'] . '?sort=' . $key . '&by=' . $by . '&' . http_build_query( $filterUrl, '', '&amp;' ) ?>"
              class="sort_by"
              data-by="<?= ( isset( $_GET['by'] ) && $_GET['by'] == 'desc' ) ? 'asc' : 'desc' ?>"
              data-sort="<?= $key ?>">
              <!--                --><?php //if(isset($field['visible_keys'])){
              //                  foreach ( $field['visible_keys'] as $visible_key ) {
              //                    echo $fields[$visible_key]['label'].' ';
              //                  }
              //                }else{
              echo $field['label'];
              //                } ?>
              <?= ( isset( $_GET['sort'] ) && isset( $_GET['by'] ) && $_GET['sort'] == $key )
                ? ( $_GET['by'] == 'desc' ) ? ' <i class="fa fa-caret-down" aria-hidden="true"></i>'
                  : ' <i class="fa fa-caret-up" aria-hidden="true"></i>' : '' ?>
            </a>
          </th>
        <?php endif; ?>
        <?php endforeach; ?>
        <th></th>
      </tr>
      <tr>
        <!--        <td><input name="selectAll" class="selectAll" type="checkbox" /></td>-->
        <?= $filter ?>
        <td>
          <button type="submit" class="btn btn-sm btn-success">Поиск</button>
        </td>
      </tr>
      <?php foreach ( $items as $item ): ?>
        <tr class="sortable_table" data-id="<?= $item['id'] ?>">
          <!--          <td><input type="checkbox" class="checked_box" name="action" data-id="-->
          <? //= $item['id'] ?><!--"></td>-->
          <?php foreach ( $fields as $field => $fieldSettings ) :
            if ( !isset( $fieldSettings['visible'] )
              || ( isset( $fieldSettings['visible'] )
                && $fieldSettings['visible'] != true ) ) continue;
            ?>
            <?php if ( isset( $options['sortable'] ) && $options['sortable'] == $field ) { ?>
            <td><a class="btn btn-default btn-xs sort" data-id="<?= $item['id'] ?>"><i class='fa fa-bars'></i></a></td>
            <?php continue;
          } ?>
            <?php if ( isset( $options['activate'] ) && $options['activate'] == $field ) { ?>
            <td><a
                class="btn btn-<?= $item[$options['activate']] == '1' ? 'success' : 'default'; ?> btn-xs change_active"
                data-active="<?= $item[$options['activate']] == '0' ? '1' : '0'; ?>" data-id="<?= $item['id']; ?>"
                title="<?= $item[$options['activate']] == '1' ? 'Выключить отображение на сайте'
                  : 'Включить отображение на сайте'; ?>" data-toggle="tooltip" data-placement="right"><i
                  class="fa fa-power-off" aria-hidden="true"></i></a></td>
            <?php continue;
          } ?>
            <td>
              <?php
              switch ( $fieldSettings['type'] ) {
                case 'file':
                case 'afile':
                  $file = is_array( $item[$field] ) && isset( $item[$field][0] ) ? $item[$field][0] : $item[$field];
                  echo '<img width="100px" src="' .
                    ( !is_array( $file ) && file_exists( FOLDER . $file )
                      ? $file : '/images/no_photo.png' ) . '" />';
                  break;
                case 'select':
                case 'mselect':
                case 'aselect':
                  // Если $fieldSettings['id_key'] != id будет дольше работать!!!!!!
                  if ( isset( $fieldSettings['multiple'] ) && $fieldSettings['multiple'] == true ) {
                    if ( $fieldSettings['id_key'] == 'id' && isset( $item[$field] ) ) {
                      foreach ( (array)$item[$field] as $idSelect ) {
                        echoVisibleFields( $fieldSettings['values'][$idSelect], $fieldSettings );
                      }
                    } else {
                      foreach ( $fieldSettings['values'] as $values ) {
                        if ( isset( $item[$field] ) && in_array( $values[$fieldSettings['id_key']], $item[$field] ) ) {
                          echoVisibleFields( $values, $fieldSettings );
                        }
                      }
                    }
                  } else {
                    if ( $fieldSettings['id_key'] == 'id' ) {
                      if ( isset( $fieldSettings['values'][$item[$field]] ) )
                        echoVisibleFields( $fieldSettings['values'][$item[$field]], $fieldSettings );
                    } else {
                      foreach ( $fieldSettings['values'] as $values ) {
                        if ( $values[$fieldSettings['id_key']] == $item[$field] ) {
                          echoVisibleFields( $values[$item[$field]], $fieldSettings );
                          break;
                        }
                      }
                    }
                  }
                  break;
                default:
                  if ( isset( $fieldSettings['visible_keys'] ) ) {
                    foreach ( $fieldSettings['visible_keys'] as $visible_key ) {
                      echo $item[$visible_key] . ' ';
                    }
                  } else {
                    if ( $fieldSettings['type'] == 'button' ) {
                      echo $this->block( 'adminbuilder/layouts/button', [
                        'options' => $fieldSettings,
                        'item'    => [ 'id' => $item['id'] ]
                      ] );
                    } else {
                      if ( $fieldSettings['type'] == 'read' ) { ?>
                        <?php if ( is_array( $item[$field] ) ): ?>
                          <ul>
                            <?php foreach ( $item[$field] as $element ) : ?>
                              <li><?= $element ?></li>
                            <?php endforeach; ?>
                          </ul>
                        <?php elseif ( is_numeric( $item[$field] ) ) :
                          switch ( $item[$field] ) {
                            case 0 :
                              $value = 'Нет';
                              break;
                            case 1 :
                              $value = 'да';
                              break;
                            default:
                              $value = $item[$field];
                          }
                          echo $value;
                        endif;
                      } else {
                        if ( is_array( $item[$field] ) )
                          echo implode( ',', $item[$field] );
                        else
                          echo $item[$field];
                      }
                    }
                  }
              }
              ?>
            </td>
          <?php endforeach; ?>
          <td>
            <?php if ( !isset( $options['hide'] ) || ( isset( $options['hide'] ) && !in_array( 'edit', $options['hide'] ) ) ): ?>
              <a href="/admin/<?= $page_current['uri']; ?>/edit/<?= $item['id'] ?>"
                 class="btn btn-default btn-xs edit-button"><i class="fa fa-pencil-square-o"></i></a>
            <?php endif; ?>
            <?php if ( !isset( $options['hide'] ) || ( isset( $options['hide'] ) && !in_array( 'view', $options['hide'] ) ) ):
              $uniqDiv  = uniqid( 'div' );
              $viewData = [ 'item' => $item, 'options' => $fields, 'id' => $uniqDiv ];
              ?>
              <div id="<?= $uniqDiv ?>"></div>
              <script>
                PoolFields.add( 'views', <?=json_encode( $viewData )?>)
              </script>
              <!--                <a href="/admin/--><? //= $page_current['uri'];
                                                     ?><!--/view/--><? //= $item['id']
                                                                    ?><!--"-->
              <!--                   class="btn btn-default btn-xs edit-button"><i class="fa fa-eye"></i></a>-->
            <?php endif; ?>
            <?php if ( !isset( $options['hide'] ) || ( isset( $options['hide'] ) && !in_array( 'delete', $options['hide'] ) ) ): ?>
              <a class="btn btn-default btn-xs delete" data-id="<?= $item['id']; ?>"><i class="fa fa-trash-o"></i></a>
            <?php endif; ?>
          </td>
        </tr>
      <?php endforeach; ?>
    </table>
  </form>
  <?= $pagination; ?>
  <!--  <div class="actions">-->
  <!--    --><?php //if ( isset( $options['activate'] ) ): ?>
  <!--      <button class="btn btn-success btn-xs" data-action="activate">Активировать</button>-->
  <!--      <button class="btn btn-danger btn-xs" data-action="deactivate">Деактивировать</button>-->
  <!--    --><?php //endif; ?>
  <!--    <button class="btn btn-danger btn-xs hidden_btn" data-action="delete">Удалить</button>-->
  <!--  </div>-->
<?php } ?>

<div class="load-full-page"><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i></div>
<style>

  .sortable-table th {
    /* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/#ffffff+0,e5e5e5+100;White+3D */
    background: rgb(255, 255, 255); /* Old browsers */
    background: -moz-linear-gradient(top, rgba(255, 255, 255, 1) 0%, rgba(229, 229, 229, 1) 100%); /* FF3.6-15 */
    background: -webkit-linear-gradient(top, rgba(255, 255, 255, 1) 0%, rgba(229, 229, 229, 1) 100%); /* Chrome10-25,Safari5.1-6 */
    background: linear-gradient(to bottom, rgba(255, 255, 255, 1) 0%, rgba(229, 229, 229, 1) 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ffffff', endColorstr='#e5e5e5', GradientType=0); /* IE6-9 */
    font-weight: bold;

    vertical-align: middle;
    padding: 6px 0 4px 0;
  }

  .actions {
    display: inline-block;
    margin-left: 50px;
    margin-top: 5px;
  }

  .sortable-table th a {
    color: #000;
    display: block;
    width: 100%;
  }

  .sort {
    cursor: move
  }

  .change_active {
    border-radius: 50%
  }

  .sortable-table td {
    font-size: 13px
  }

  .sortable-table th.active {
    /* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/#e5e5e5+0,ffffff+100 */
    background: #fff; /* Old browsers */

    font-weight: bold;
    position: relative;
    z-index: 2;
  }

  .placeh {
    height: 50px;
  }

  .sortable-table th:not(.active):hover {
    background: #e5e5e5;
  }

  <?php if($countFields == 0):?>
  .sortable-table, .hidden_btn {
    display: none;
  }

  <?php endif; ?>

</style>
<!--<ul class="sortables">-->
<!--  <li><div>Some content</div></li>-->
<!--  <li>-->
<!--    <div>Some content</div>-->
<!--    <ul>-->
<!--      <li><div>Some sub-item content</div></li>-->
<!--      <li><div>Some sub-item content</div></li>-->
<!--    </ul>-->
<!--  </li>-->
<!--  <li><div>Some content</div></li>-->
<!--</ul>-->
<?php if ( $tree && isset( $options['parent'] ) ) { ?>

<?php } ?>
<script>
  $( '.selectAll' ).click( function () {
    if ( $( this ).is( ':checked' ) === true ) {
      $( '.checked_box' ).prop( 'checked', true );
    } else {
      $( '.checked_box' ).prop( 'checked', false );
    }
  } );
  <?php if($tree && isset( $options['parent'] )) { ?>

  var item = $( "div" ).find( "[data-id='0']" );
  item.next( 'ul' ).css( 'display', 'block' );
  showHidden( item );
  $( '.show-next' ).click( function () {
    var item = $( this ).parent().next( 'ul' );
    if ( item.is( ':visible' ) == true ) {
      $( this ).html( '+' );
      item.fadeOut( 0 );
    }
    else {
      $( this ).html( '-' );
      item.fadeTo( 0, 1 );
    }
  } );

  function showHidden( item ) {
    item.children( '.show-next' ).html( '-' );
    item.parent().css( 'display', 'block' );
    if ( item.parent().parent().is( 'ul' ) ) showHidden( item.parent().parent().prev() );
  }

  // $('.sortables').nestedSortable({
  //     handle: 'div',
  //     items: 'li',
  //     listType : 'ul',
  //     // toleranceElement: '> div'
  // });
  Sortable.create( document.getElementById( "categories" ), {
    group: "cat",
    animation: 150,
    handle: ".sort-item",
    draggable: ".sort-item",
    ghostClass: 'ghost',
    onUpdate: function ( evt/**Event*/ ) {
      var parent_id = $( evt.item ).parent().parent().data( 'id' );
      if ( !parent_id ) parent_id = 0;
      var id = $( evt.item ).attr( 'data-id' );
      var items = [];
      $( '#' + $( $( evt.item ).parent() ).attr( 'id' ) + ' > li' ).each( function ( index, li ) {
        items.push( [
          $( li ).data( 'id' ),
          index
        ] )
      } );
      sendSort( id, parent_id, items );
    },
  } );

  $( '.subs' ).each( function () {
    if ( $( '.sort-item', this ).length == 0 ) {
      $( this ).append( $( '<li>', { class: 'sort-item', style: 'width: 100%; height: 20px;' } ) );
    }
    Sortable.create( document.getElementById( $( this ).attr( 'id' ) ), {
      group: "cat",
      animation: 150,
      handle: ".sort-item",
      draggable: ".sort-item",
      ghostClass: 'ghost',
      onSort: function ( /**Event*/evt ) {
        var parent_id = $( evt.item ).parent().parent().data( 'id' );
        if ( !parent_id ) parent_id = 0;
        var id = $( evt.item ).attr( 'data-id' );
        var items = [];
        $( '#' + $( $( evt.item ).parent() ).attr( 'id' ) + ' > li' ).each( function ( index, li ) {
          items.push( [
            $( li ).data( 'id' ),
            index
          ] )
        } );
        sendSort( id, parent_id, items );
      },
    } )
  } );

  function sendSort( id, parent_id, elements ) {
    $.ajax( {
      url: '/admin/<?=$page_current['uri'];?>/sortable',
      dataType: 'text',
      data: {
        id: id,
        parent_id: parent_id,
        items: JSON.stringify( elements ),
        field: "<?=$options['sortable']?>",
        parent_field: "<?=$options['parent']?>"
      },
      type: 'POST',
      beforeSend:
        function () {
          $( '.load-full-page' ).fadeTo( 0, 1 );
        },
      success:
        function ( data ) {
          $( '.load-full-page' ).fadeOut( 0 );
        },
      error:
        function () {
          item.sortable( "cancel" );
          if ( ui.sender ) ui.sender.sortable( 'cancel' );
          //changeLevels(ui.utem);
          alert( 'Ошибка при выполнении операции' );
          $( '.load-full-page' ).fadeOut( 0 );
        },
    } );
  }

  // var item = $(".sortables, .subs");
  //item.sortable({
  //    connectWith:"ul",
  //    placeholder:"placeh ui-state-highlight",
  //    handle : '.sort',
  //    helper:'clone',
  //    items: 'li:not(.create-category)',
  //    axis: 'y',
  //    opacity: .8,
  //    forcePlaceholderSize: true,
  //    // revert: 300,
  //    delay: 500,
  //    scroll: true,
  //    scrollSensitivity: 100,
  //    dropOnEmpty: true,
  //    //start:function(event, ui)
  //    //{
  //    //
  //    //},
  //    //update:function(event,ui)//при изменении списка
  //    //{
  //    //    if (!ui.sender){
  //    //        var id = ui.item.attr('data-id');
  //    //
  //    //        var parent_id = ui.item.parent().parent().data('id');
  //    //        if (!parent_id) parent_id = 0;
  //    //
  //    //        var sorts = [];
  //    //
  //    //        ui.item.parent().children().each(function(){
  //    //            var index = $(this).parent().children().index($(this));
  //    //            var id = $(this).data('id');
  //    //            sorts.push([id,index]);
  //    //        });
  //    //
  //    //        sorts = JSON.stringify(sorts);
  //    //
  //    //        // var str_parent = makeParents(ui.item, '');
  //    //        var str_parent = '';
  //    //        var arr = [];
  //    //
  //    //        $.ajax({
  //    //            url: '/admin/<?////=$page_current['uri'];?>/////sortable',
  //    //            dataType: 'text',
  //    //            data:'id='+id+'&parent_id='+parent_id+'&items='+sorts+'&field=<?////=$options['sortable']?>////&parent_field=<?////=$options['parent']?>////',
  //    //            type: 'POST',
  //    //            beforeSend:
  //    //                function(){
  //    //                    $('.load-full-page').fadeTo(0,1);
  //    //                },
  //    //            success:
  //    //                function(data){
  //    //                    $('.load-full-page').fadeOut(0);
  //    //                },
  //    //            error:
  //    //                function(){
  //    //                    item.sortable( "cancel" );
  //    //                    if (ui.sender) ui.sender.sortable('cancel');
  //    //                    //changeLevels(ui.utem);
  //    //                    alert('Ошибка при выполнении операции');
  //    //                    $('.load-full-page').fadeOut(0);
  //    //                },
  //    //        });
  //    //    }
  //    //}
  //});
  <?php } ?>

  <?php if(isset( $options['activate'] )) { ?>

  $( 'button[data-action="activate"]' ).click( function ( e ) {
    e.preventDefault();
    if ( confirm( 'Подтвердите активацию' ) ) {
      var activeId = [];
      var activeElements = $( 'input[name="action"]:checked' );
      $.each( activeElements, function ( index, value ) {
        activeId.push( $( value ).data( 'id' ) );
      } )
      $.ajax( {
        url: '/admin/<?=$page_current['uri'];?>/activate',
        dataType: 'json',
        data: {
          id: activeId
          , active: 1,
          field: "<?=$options['activate']?>"
        },
        type: 'POST',
        beforeSend:
          function () {
            $( '.load-full-page' ).fadeTo( 0, 1 );
          },
        success:
          function ( data ) {
            if ( data.saved == '1' ) {
              window.location.reload();
            }
            else alert( 'Ошибка удаления' );
            $( '.load-full-page' ).fadeOut( 0 );
          },
        error:
          function ( data ) {
            alert( 'Ошибка при выполнении операции' );
            $( '.load-full-page' ).fadeOut( 0 );
          },
      } );
    }
    return false;
  } );

  $( 'button[data-action="deactivate"]' ).click( function ( e ) {
    e.preventDefault();
    if ( confirm( 'Подтвердите деактивацию' ) ) {
      var activeId = [];
      var activeElements = $( 'input[name="action"]:checked' );
      $.each( activeElements, function ( index, value ) {
        activeId.push( $( value ).data( 'id' ) );
      } )
      $.ajax( {
        url: '/admin/<?=$page_current['uri'];?>/activate',
        dataType: 'json',
        data: {
          id: activeId
          , active: 0,
          field: "<?=$options['activate']?>"
        },
        type: 'POST',
        beforeSend:
          function () {
            $( '.load-full-page' ).fadeTo( 0, 1 );
          },
        success:
          function ( data ) {
            if ( data.saved == '1' ) {
              window.location.reload();
            }
            else alert( 'Ошибка удаления' );
            $( '.load-full-page' ).fadeOut( 0 );
          },
        error:
          function ( data ) {
            alert( 'Ошибка при выполнении операции' );
            $( '.load-full-page' ).fadeOut( 0 );
          },
      } );
    }
    return false;
  } );

  $( '.change_active' ).click( function () {
    var id = $( this ).data( 'id' );
    var active = $( this ).attr( 'data-active' );
    $.ajax( {
      url: '/admin/<?=$page_current['uri'];?>/activate',
      dataType: 'json',
      data: 'id=' + id + '&active=' + active + '&field=<?=$options['activate']?>',
      type: 'POST',
      beforeSend:
        function () {
          $( '.load-full-page' ).fadeTo( 0, 1 );
        },
      success:
        function ( data ) {
          if ( data.saved == '1' ) {
            location.reload();
          }
          else alert( data.text );
          $( '.load-full-page' ).fadeOut( 0 );
        },
      error:
        function ( data ) {
          alert( 'Ошибка запроса!' );
          $( '.load-full-page' ).fadeOut( 0 );
        },
    } );

  } );
  <?php } ?>

  <?php if(isset( $options['sortable'] ) && !$tree) { ?>

  // Sortable.create(document.getElementById('tablesortable'), {
  //   animation: 150,
  //   handle: ".sort",
  //   draggable: ".sortable_table",
  //   ghostClass: 'ghost',
  //   onUpdate: function (evt/**Event*/) {
  //     // var parent_id = $(evt.item).parent().parent().data('id');
  //     // if (!parent_id) parent_id = 0;
  //     // var id = $(evt.item).attr('data-id');
  //     // var items = [];
  //     // $('#'+$($(evt.item).parent()).attr('id')+' > li').each(function(index, li){
  //     //   items.push([$(li).data('id'), index])
  //     // });
  //     // sendSort(id, parent_id, items);
  //   },
  // })
  $( '.sortable-table' ).sortable( {
    items: 'tr:not(:first-child, :nth-child(2))',
    handle: '.sort',
    helper: 'clone',
    update: function ( event, ui ) {
      var i = 1;
      var arr = [];
      $( 'table tr:not(:first-child, :nth-child(2))' ).each( function ( index, value ) {
        // var id = $(this).children('td:first-child').children('a').attr('data-id');
        var id = $( '.sort', $( this ) ).data( 'id' );
        arr.push( [
          parseInt( id ),
          index
        ] );
        i++;
      } );
      items = JSON.stringify( arr );
      $.ajax( {
        url: '/admin/<?=$page_current['uri'];?>/sortable',
        dataType: 'text',
        data: 'items=' + items + '&field=<?=$options['sortable']?>',
        type: 'POST',
        beforeSend:
          function () {
            $( '.load-full-page' ).fadeTo( 0, 1 );
          },
        success:
          function ( data ) {
            $( '.load-full-page' ).fadeOut( 0 );
          },
        error:
          function ( data ) {

            alert( 'Ошибка при выполнении операции' );
            $( '.load-full-page' ).fadeOut( 0 );
          },
      } );
    }
  } );
  <?php } ?>

  $( 'table td:last-child a:first-child' ).mouseenter( function ( event ) {
    $( this ).removeClass( 'btn-default' );
    $( this ).addClass( 'btn-warning' );
  } );
  $( 'table td:last-child a:first-child' ).mouseleave( function ( event ) {

    $( this ).removeClass( 'btn-warning' );
    $( this ).addClass( 'btn-default' );
  } );
  $( 'table td:last-child a:not(:first-child):last-child' ).mouseenter( function ( event ) {
    $( this ).removeClass( 'btn-default' );
    $( this ).addClass( 'btn-danger' );
  } );
  $( 'table td:last-child a:not(:first-child):last-child' ).mouseleave( function ( event ) {
    $( this ).removeClass( 'btn-danger' );
    $( this ).addClass( 'btn-default' );
  } );
  $( document ).ready( function () {

    $( 'button[data-action="delete"]' ).click( function ( e ) {
      e.preventDefault();
      if ( confirm( 'Подтвердите удаление' ) ) {
        var deleteId = [];
        var activeElements = $( 'input[name="action"]:checked' );
        $.each( activeElements, function ( index, value ) {
          deleteId.push( $( value ).data( 'id' ) );
        } )
        $.ajax( {
          url: '/admin/<?=$page_current['uri'];?>/delete',
          dataType: 'json',
          data: {
            id: deleteId
          },
          type: 'POST',
          beforeSend:
            function () {
              $( '.load-full-page' ).fadeTo( 0, 1 );
            },
          success:
            function ( data ) {
              if ( data.deleted == '1' ) {
                window.location.reload();
              }
              else alert( 'Ошибка удаления' );
              $( '.load-full-page' ).fadeOut( 0 );
            },
          error:
            function ( data ) {
              alert( 'Ошибка при выполнении операции' );
              $( '.load-full-page' ).fadeOut( 0 );
            },
        } );
      }
      return false;
    } );
    $( '.delete' ).on( 'click', function () {
      var id = $( this ).data( 'id' );
      var $item = $( this );
      if ( confirm( 'Подтвердите удаление' ) ) {
        $.ajax( {
          url: '/admin/<?=$page_current['uri'];?>/delete',
          dataType: 'json',
          data: 'id=' + id,
          type: 'POST',
          beforeSend:
            function () {
              $( '.load-full-page' ).fadeTo( 0, 1 );
            },
          success:
            function ( data ) {
              if ( data.deleted == '1' ) $item.parent().parent().remove();
              else alert( 'Ошибка удаления' );
              $( '.load-full-page' ).fadeOut( 0 );
            },
          error:
            function ( data ) {
              alert( 'Ошибка при выполнении операции' );
              $( '.load-full-page' ).fadeOut( 0 );
            },
        } );
      }
    } );

  } );
</script>
<style>
  .ghost {
    opacity: .5;
    background: #C8EBFB;
  }

</style>