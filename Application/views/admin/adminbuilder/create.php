<?php
use \Application\Helpers\AppHelper;
$fieldTabs = [];
ob_start();
foreach ( $fields as $field => $options ) {
  if(isset($options['whereVisible'])){
    if(!isset($options['whereVisible']['field'])) $options['whereVisible']['field'] = $field;
    if(!AppHelper::compare($options['whereVisible']['operation'], $options['whereVisible']['value'], $item[$options['whereVisible']['field']])) continue;
  }
  if ( $options['edited'] && !isset( $options['tab'] ) ){
    $this->block( 'adminbuilder/layouts/' . $options['type'], array(
      'options' => $options,
      'field'   => $field,
      'item'    => $item,
      'errors'  => $errors
    ) );
  }
  if($options['edited'] && isset( $options['tab'] )){
    $fieldTabs[$options['tab']][] = $this->block( 'adminbuilder/layouts/' . $options['type'], array(
      'options' => $options,
      'field'   => $field,
      'item'    => $item,
      'errors'  => $errors
    ), true );
  }
  if(isset($mergeFields[$field])){
    foreach ( $mergeFields[$field] as $mergeField => $mergeOptions ) {
      if(isset($mergeOptions['whereVisible'])){
        if(!isset($mergeOptions['whereVisible']['field'])) $mergeOptions['whereVisible']['field'] = $mergeField;
        if(!AppHelper::compare($mergeOptions['whereVisible']['operation'], $mergeOptions['whereVisible']['value'], $item[$mergeOptions['whereVisible']['field']])) continue;
      }
      if ( $mergeOptions['edited'] && !isset( $mergeOptions['tab'] ) ){
        $this->block( 'adminbuilder/layouts/' . $mergeOptions['type'], array(
          'options' => $mergeOptions,
          'field'   => $mergeField,
          'item'    => $item,
          'errors'  => $errors
        ) );
      }
      if($mergeOptions['edited'] && isset( $mergeOptions['tab'] )){
        $fieldTabs[$mergeOptions['tab']][] = $this->block( 'adminbuilder/layouts/' . $mergeOptions['type'], array(
          'options' => $mergeOptions,
          'field'   => $mergeField,
          'item'    => $item,
          'errors'  => $errors
        ), true );
      }
    }
  }
}
$home = ob_get_flush();
ob_clean();
?>

<div class="col-xs-12">
<h1 class="col-xs-12 col-md-8 ">Создание новой записи</h1>
</div>
<div class="col-xs-12 clr"></div>
<div class="col-xs-12 col-md-8">

  <script src="/media/js/content.js"></script>
  <link href="/media/css_admin/content.css" rel="stylesheet">
  <?php if ( ( $result && $result !== null ) || isset( $_GET['success'] ) ): ?>
    <p class="alert alert-success"><i class="fa fa-check"></i> <?= $successMessage ?></p>
  <?php elseif ( $result === false || isset( $_GET['error'] ) ): ?>
    <p class="alert alert-danger"><i class="fa fa-times"></i> <?= $errorMessage ?></p>
  <?php endif; ?>
  <?php
  $content = '';
  if ( count( $tabs ) > 0 ):?>
    <!-- Nav tabs -->
    <ul class="nav nav-tabs builder_tab" role="tablist">
      <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab"
                                                data-toggle="tab">Главная</a></li>
      <?php foreach ( $tabs as $tab ) :
        $uniqID = uniqid( 'id' );
        $content .= '<div id="' . ( isset( $tab['id'] ) ? $tab['id'] : $uniqID )
          . '"  role="tabpanel" class="tab-pane">' . $tab['content'] . '</div>';
        ?>
        <li role="presentation"><a href="#<?= ( isset( $tab['id'] ) ? $tab['id'] : $uniqID ) ?>" aria-controls="profile"
                                   role="tab" data-toggle="tab"><?= $tab['name'] ?></a></li>
      <?php endforeach; ?>
      <?php foreach ( $fieldTabs as $name => $fieldTab ) :
        $uniqID = uniqid( 'id' );
        $content .= '<div id="' . ( $uniqID )
          . '"  role="tabpanel" class="tab-pane">' . implode('', $fieldTab) . '</div>';
        ?>
        <li role="presentation"><a href="#<?= ( $uniqID ) ?>" aria-controls="profile"
                                   role="tab" data-toggle="tab"><?= $name ?></a></li>

      <?php endforeach; ?>
    </ul>
  <?php endif; ?>
    <div class="col-xs-12 clr"></div>
  <form method="POST" name="formCreate" class="formCreate" action="#" enctype="multipart/form-data">
    <div id="files"></div>
    <div class="tab-content">
      <div id="home" role="tabpanel" class="tab-pane active">
        <?= $home ?>
        <div class="col-xs-12 clr"></div>
      </div>
      <?= $content ?>
    </div>
    <button type="submit" class="btn btn-success btn-sm" name="submit"><i class="fa fa-floppy-o" aria-hidden="true"></i>
      Сохранить
    </button>
    <a href="/admin/<?=$page_current['uri']?>" class="btn btn-danger btn-sm"><i
        class="fa fa-times"></i> Закрыть</a>
  </form>
<script>
  $( '.builder_tab li' ).click( function ( e ) {
    window.location.href = window.location.pathname + '#' + $( this ).index();
  } );
  var url = window.location.href.split( '#' );
  if ( url[ 1 ] != undefined && url[ 1 ] != '' ) {
    var index_tab = url[ 1 ];
    var activeElement = $( '.builder_tab li:eq("' + index_tab + '")' );
    $( '.builder_tab li' ).removeClass( 'active' );
    $( '.tab-pane' ).removeClass( 'active' );
    activeElement.addClass( 'active' );
    $( $( 'a', activeElement ).attr( 'href' ) ).addClass( 'active' );
  }

    <?php if(count($events) > 0) :?>
    <?php foreach($events['change'] as $mainField => $mergeFields): ?>
    <?php foreach( $mergeFields as $class => $field ) : ?>
    $('*[name="<?=$mainField?>"]').on('change', function(){
        var fields = <?=json_encode($field)?>;
        var value = $(this).val();
        if(value === ''){
            $.each(fields, function(index, val){
                $('*[name="'+val.fieldName+'"]').val('');
            });
            return false;
        }
        $.ajax({
                   url: '/admin/<?=$page_current['uri'];?>/getMergeData',
                   data:'value='+value+'&field=<?=$fields[$mainField]['id_key']?>&class=<?=$class?>',
                   dataType: 'json',         //указываем какой тип будем принимать
                   type: 'POST',               //указываем каким методом будем отправлять
                   success:
                       function(data){
                           $.each(fields, function(index, val){
                               $('*[name="'+val.fieldName+'"]').val(data[val.field]);
                           });
                       },
                   error:
                       function(data){
                           console.log('Ошибка при выполнении операции');
                       }
               });
    });
    <?php endforeach; ?>
    <?php endforeach; ?>
    <?php endif; ?>

<?= $model->getClientValidation() ?>
</script>
