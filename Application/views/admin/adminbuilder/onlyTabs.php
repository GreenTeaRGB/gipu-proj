<div class="col-xs-12">
  <h1 class="col-xs-12 col-md-8 ">Запись <?= $page_current['name']; ?> #<?= $id ?></h1>
</div>
<div class="col-xs-12 clr"></div>
<div class="col-xs-12 col-md-8">



  <script src="/media/js/content.js"></script>
  <link href="/media/css_admin/content.css" rel="stylesheet">
  <?php
  $content = '';
  if ( count( $tabs ) > 0 || count($fieldTabs) > 0) :?>
    <!-- Nav tabs -->
    <ul class="nav nav-tabs builder_tab" role="tablist">
      <?php
      $first = true;
      foreach ( $tabs as $tab ) :
        $uniqID = uniqid( 'id' );
        $content .= '<div id="' . ( isset( $tab['id'] ) ? $tab['id'] : $uniqID )
          . '"  role="tabpanel" class="tab-pane '.($first?'active':'').'">' . $tab['content'] . '</div>';
        ?>
        <li role="presentation" class="<?=($first?'active':'')?>"><a href="#<?= ( isset( $tab['id'] ) ? $tab['id'] : $uniqID ) ?>" aria-controls="profile"
                                   role="tab" data-toggle="tab"><?= $tab['name'] ?></a></li>
      <?php $first = false; endforeach; ?>
    </ul>
  <?php endif; ?>
  <div class="col-xs-12 clr"></div>
  <form method="POST" name="formCreate" class="formCreate" action="#" enctype="multipart/form-data">
    <div id="files"></div>
    <div class="tab-content">
      <?= $content ?>
    </div>
    <button type="submit" class="btn btn-success btn-sm" name="submit"><i class="fa fa-floppy-o" aria-hidden="true"></i>
      Сохранить
    </button>
    <a href="/admin/<?=$page_current['uri']?>" class="btn btn-danger btn-sm"><i
        class="fa fa-times"></i> Закрыть</a>
  </form>
</div>
<script>
    $( document ).ready( function () {

        // $('.builder_tab li a').each(function(){
        //   if(window.location.href.indexOf($(this).attr('href')) > 0){
        //     $('.builder_tab li').removeClass('active');
        //     $('.tab-pane').removeClass('active');
        //     $(this).parent().addClass('active');
        //     $($(this).attr('href')).addClass('active');
        //   }
        // });

        $( '.builder_tab li' ).click( function ( e ) {
            window.location.href = window.location.pathname + '#' + $( this ).index();
        } );
        var url = window.location.href.split( '#' );
        if ( url[ 1 ] != undefined && url[ 1 ] != '' ) {
            var index_tab = url[ 1 ];
            var activeElement = $( '.builder_tab li:eq("' + index_tab + '")' );
            $( '.builder_tab li' ).removeClass( 'active' );
            $( '.tab-pane' ).removeClass( 'active' );
            activeElement.addClass( 'active' );
            $( $( 'a', activeElement ).attr( 'href' ) ).addClass( 'active' );
        }

        // var url = window.location.href.split('?');
        // window.history.replaceState({}, document.title, url[0]);

        $( '.delete-file' ).on( 'click', function () {
            if ( confirm( 'Действительно удалить файл?' ) ) {
                var $em = $( this );
                var file = $em.data( 'file' );
                var field = $em.data( 'field' );
                var id = "<?=$item['id'];?>";
                $.ajax( {
                    url: '/admin/<?=$page_current['uri'];?>/deleteFile',
                    data: 'file-id=' + id + '&file=' + file + '&field=' + field,
                    dataType: 'json',         //указываем какой тип будем принимать
                    type: 'POST',               //указываем каким методом будем отправлять
                    success:
                        function ( data ) {
                            $em.parent().remove();
                        },
                    error:
                        function ( data ) {
                            alert( 'Ошибка при выполнении операции' );
                        }
                } );
            }
        } );

      <?php if(count( $events ) > 0) :?>
      <?php foreach($events['change'] as $mainField => $mergeFields): ?>
      <?php foreach( $mergeFields as $class => $field ) : ?>
        $( '*[name="<?=$mainField?>"]' ).on( 'change', function () {
            var fields = <?=json_encode( $field )?>;
            var value = $( this ).val();
            if ( value === '' ) {
                $.each( fields, function ( index, val ) {
                    $( '*[name="' + val.fieldName + '"]' ).val( '' );
                } );
                return false;
            }
            $.ajax( {
                url: '/admin/<?=$page_current['uri'];?>/getMergeData',
                data: 'value=' + value + '&field=<?=$fields[$mainField]['id_key']?>&class=<?=$class?>',
                dataType: 'json',         //указываем какой тип будем принимать
                type: 'POST',               //указываем каким методом будем отправлять
                success:
                    function ( data ) {
                        $.each( fields, function ( index, val ) {
                            var input = $( '*[name="' + val.fieldName + '"]' );
                            input.parent().find('.builder-files').remove();
                            if(input.attr('type') == 'file'){
                                if(data[ val.field ] == '') return;
                                var html = '<div class="builder-files">\n' +
                                    '              <div class="image-builder single-image">\n' +
                                    '              <img src="'+data[ val.field ]+'" class="img-thumbnail">\n' +
                                    '              <a class="btn btn-xs btn-danger delete-file" data-field="'+val.fieldName+'" ' +
                                    'data-file="'+data[ val.field ]+'"><i class="fa fa-trash-o"></i></a>\n' +
                                    '              <input type="hidden" name="'+val.fieldName+'" value="">\n' +
                                    '            </div>\n' +
                                    '         </div>';
                                input.after(html);
                                return
                            };
                            input.val( data[ val.field ] );
                        } );
                    },
                error:
                    function ( data ) {
                        console.log( 'Ошибка при выполнении операции' );
                    }
            } );
        } );
      <?php endforeach; ?>
      <?php endforeach; ?>
      <?php endif; ?>

    } );
</script>

