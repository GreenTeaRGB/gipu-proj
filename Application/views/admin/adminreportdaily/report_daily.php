<?php
	$sum = 0;
	$count_orders = 0;
?>

<div>
	<div>
		<div class="title">
			<h1 class="colx-xs-12 col-md-12">Дневной отчет</h1>
		</div>
		<?php if (count($report_daily) > 0): ?>
			<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
				<?php foreach ($report_daily as $key => $product): ?>
				<div class="panel panel-default">
					<div class="panel-heading" role="tab" id="heading-<?= $key; ?>">
						<h5 class="panel-title">
							<a role="button" data-id="<? echo $product['main_id']; ?>" data-toggle="collapse" data-parent="#accordion" href="#collapse-<? echo $key; ?>" aria-expanded="false" aria-controls="collapse-<?echo $key; ?>">
								<span class="rep-id"><strong>ID: </strong><? echo $product['main_id']; ?></span>
								<span class="rep-count"><strong>Кол-во продуктов: </strong><? echo $product['count_products']; ?></span>
								<span class="rep-total"><strong>Сумма заказа: </strong><? echo $product['total']; ?></span>
							</a>
						</h5>
					</div>
					<div id="collapse-<?echo $key; ?>" class="panel-collapse collapse " role="tabpanel" aria-labelledby="heading-<?echo $key; ?>">
						<div class="panel-body">
						</div>
					</div>
				</div>
			
			<?php
				$sum = $sum + $product['total'];
				$count_orders = $count_orders + $product['count_products'];
			?>
			<?php endforeach; ?>
			</div>
			<div>
				<div><strong>Общее кол-во заказов: </strong><? echo $count_orders; ?></div>
			</div>
			<div>
				<div><strong>Ощая сумма: </strong><? echo $sum; ?></div>
			</div>
		<?php else: ?>
			<p>
				<strong>У Вас нет заказов</strong>
			</p>
		<?php endif; ?>
	</div>
</div>
