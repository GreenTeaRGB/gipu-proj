<h1 class="colx-xs-12 col-md-12 ">Сообщения поддержки</h1>
<div class="col-xs-12 clr"></div>
<!-- MESSAGES-PAGE -->
<template id="page-messages">
  <div class="cabinet__container__block_long messages" @keyup.esc="closeDialogue">
    <div class="dialogues col-md-3">
      <h2>Диалоги</h2>
      <ul>
        <li v-for="dialog in dialogs">
          <router-link :to="'/' + dialog.userId" class="human">
            <div class="avatar">
              <img :src="dialog.avatar" alt="">
            </div>
            <h3>{{dialog.name}}<span class="online" v-if="dialog.online"></span></h3>
            <p>{{dialog.message}}</p>
          </router-link>
        </li>
      </ul>
      <div class="message-field message-field_long">
        <h4>{{newDialogue.message}}</h4>
        <input @keyup.enter="findUser" v-model="newDialogue.id" id="new-dialogue" type="number" placeholder="id32134890" class="new-dialogue">
        <button @click="findUser" class="message-button icon-message find-contact"></button>
      </div>
    </div>
    <div class="dialogue col-md-9">
      <router-view></router-view>
      <template v-if="!currentDialogue">
        <div class="img"></div>
        <h2>Выберите существующий диалог, либо <label for="new-dialogue">создайте новый</label></h2>
      </template>
    </div>
  </div>
</template>

<!-- PAGE-DIALOGUE -->
<template id="dialogue-window">
  <div @keyup.esc="closeDialogue" data-slideout-ignore>
    <div class="heading">
      <div class="human">
        <div class="avatar">
          <img :src="dialogue.avatar" alt="">
        </div>
        <h3>{{dialogue.name}}</h3>
        <p>{{dialogue.lastAppearance}}</p>
        <div class="online" v-if="dialogue.userOnline"></div>
      </div>
    </div>
    <ul>
      <li v-for="(message, index) in dialogue.messages" :id=" index == (dialogue.messages.length - 1) ? 'lastMessage' : '' ">
        <div class="message" :class="message.incoming ? 'friend' : 'you'">
          <div class="avatar">
            <!-- v-if="dialogue.messages[index - 1].incoming != message.incoming" -->
            <img :src="message.avatar" alt="">
          </div>
          <p class="message__text">{{message.text}}<span>{{ message.incoming ? '' : getMessageStatus(message.status)}}</span></p>
        </div>
      </li>
    </ul>
    <div class="message-field">
      <textarea @keyup.enter="sendMessage" v-model="message.text" placeholder="Введите текст сообщения..."></textarea>
      <button @click="sendMessage" class="message-button textarea-button icon-message"></button>
    </div>
  </div>
</template>

<!-- MESSAGE-ALERT -->
<template id="incoming-message">
  <router-link @click="close" :to="'/messages/' + message.id" v-if="show" class="incoming-message">
    <div class="avatar">
      <img :src="message.avatar" alt="avatar">
    </div>
    <h3>{{message.name}}<span class="online"></span></h3>
    <p>{{message.text}}</p>
    <span @click.prevent="close" class="icon-cancel close"></span>
  </router-link>
</template>
<main class="container-fluid cabinet__wrap">
  <div class="cabinet__bg"></div>
  <div class="container cabinet" id="app">
    <section id="panel" class="col-lg-8 col-md-8 col-xs-8 cabinet__container" :class="{ 'cabinet__container_transparent': transparentPage }">
      <div class="cabinet__wrap_in">
<!--        <alerts :alerts="alerts" v-if="showAlerts"></alerts>-->
<!--        <transition name="fade">-->
<!--          <div class="cabinet__loading" v-show="loading"></div>-->
<!--        </transition>-->
<!--        <transition name="modal">-->
<!--          <modal-window v-if="modalShow"></modal-window>-->
<!--        </transition>-->
        <router-view></router-view>
      </div>
    </section>
<!--    <incoming-message ref="message"></incoming-message>-->
<!--    <transition name="modal">-->
<!--      <tooltip ref="tooltip"></tooltip>-->
<!--    </transition>-->
  </div>
</main>
<link rel="stylesheet" href="/media/css/default/cabinets.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.bundle.js"></script>
<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
<script src="https://unpkg.com/vue-router/dist/vue-router.js"></script>
<script src="/media/js/admin/messages.js"></script>
<style>
  @font-face{font-family:'icomoon';src:url("/media/css/default/fonts/icomoon.eot");
    src:url("/media/css/default/fonts/icomoon.eot#iefix") format("embedded-opentype"),
    url("/media/css/default/fonts/icomoon.ttf") format("truetype"),
    url("/media/css/default/fonts/icomoon.woff") format("woff"),
    url("/media/css/default/fonts/icomoon.svg#icomoon") format("svg");font-weight:normal;font-style:normal}
  [class^="icon-"],[class*=" icon-"]
  {font-family:'icomoon' !important;speak:none;font-style:normal;font-weight:normal;font-variant:normal;text-transform:none;line-height:1;-webkit-font-smoothing:antialiased;-moz-osx-font-smoothing:grayscale}.icon-like:before{content:"\e900"}.icon-stats:before{content:"\e901"}.icon-basket:before{content:"\e902"}.icon-arrow-right:before{content:"\e903";color:#4f463d}.icon-right-side:before{content:"\e904";color:#4f463d}.icon-cancel:before{content:"\e905";color:#4f463d}.icon-delete:before{content:"\e906";color:#4f463d}.icon-smile:before{content:"\e907";color:#4f463d}.icon-check:before{content:"\e908";color:#4f463d}.icon-edit:before{content:"\e909";color:#4f463d}.icon-search:before{content:"\e90a"}.icon-message:before{content:"\e90b";color:#4f463d}.icon-left-side:before{content:"\e90c";color:#4f463d}.icon-column:before{content:"\e90d";color:#4f463d}.icon-plates:before{content:"\e90e";color:#4f463d}.icon-delivery:before{content:"\e90f";color:#4f463d}.icon-gps:before{content:"\e910";color:#4f463d}.icon-settings:before{content:"\e911"}.icon-star:before{content:"\e912";color:#ccc}.icon-ruble:after{content:"\e914";font-weight:bold;font-size:12px;margin-left:1px}.icon-wallet:before{content:"\e915"}

  .cabinet__container {
   margin-top: 0px;
  }
  .avatar img {
    width: 100%;
    max-height: 53px;
    height: 100%;
    transform: translateX(-50%);
    position: relative;
    left: 50%;
  }

</style>