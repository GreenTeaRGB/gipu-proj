<?php
  $csrf = sha1(uniqid());
  \Application\Classes\Session::store(['_csrf'=>$csrf]);
?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <title><?= $title_page; ?></title>
    <meta charset="utf-8">
    <meta name="_csrf" content="<?=$csrf?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="/media/css_admin/selectTree.css" rel="stylesheet">
    <link rel="stylesheet" href="/media/css_admin/bootstrap/bootstrap.css" type="text/css" media="all">
    <link rel="stylesheet" href="/media/css_admin/bootstrap/bootstrap-theme.css" type="text/css" media="all">
    <link rel="stylesheet" href="/media/css_admin/style.css" type="text/css" media="all">
    <link rel="stylesheet" href="/media/css_admin/font-awesome.css">
	
	<link rel="stylesheet" href="/media/css_admin/report_daily.css">
	
    <link rel="stylesheet" href="/media/css/default/colorPicker.css">
    <link href="/media/css_admin/offcanvas.css" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="/media/js/admin/bootstrap.min.js"></script>
    <script src="/media/js/default/validation.js"></script>
    <script src="/media/js/admin/formValidatorAdmin.js"></script>
    <script src="/media/js/default/colorPicker.js"></script>
    <script src="/media/js/admin/PoolFields.js"></script>
  <link rel="stylesheet" href="/media/js/editor/summernote.css">
 <script src="/media/js/editor/summernote.js"></script>
    <script>
        function appendNotice(type, text) {
            $('.notice-container').append('<div class="price-save-message"><i class="fa fa-' + type + '"></i> <span>' + text + '</span></div>').children(':last').hide().fadeTo(500, 1);
            setTimeout(function () {
                $('.notice-container > div:first-child').fadeOut(200).remove()
            }, 3000);

        }
    </script>
</head>
<body>
<div class="notice-container"></div>

<div class="container-fluid header">
    <div class="container">
        <div class="row">
            <div class="col-xs-8 col-md-3">
                <div class="logo">
                    <span>EusCMS</span>
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                            aria-expanded="false">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <p>Управление сайтом</p>
                </div>
            </div>
            <div class="col-xs-2 exit"><a class="btn btn-success active btn-sm pull-left" target="_blank" href="/">Перейти на сайт</a></div>
            <div class="col-xs-2 pull-right exit">
                <a class="btn btn-default active btn-sm pull-right" href="/user/logout"><i class="fa fa-power-off"
                                                                                           aria-hidden="true"></i> Выйти</a>
            </div>
        </div>
    </div>
</div>
<!-- Button trigger modal -->



<div class="container main-cont">
    <div class="row">
      <?php $this->block( 'layouts/menu_admin' ); ?>
        <div class="col-xs-12 col-sm-9 no-padding main-content">
            <ul class="breadcrumb">
              <?= isset($breads) ? $breads : ''; ?>
            </ul>
          <?= $content ?>
        </div>
    </div>
</div>


</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script src="/media/js/bootstrap.min.js"></script>

<script>

    // $('.color-active').popover();
    // $('.btn-operation').button();

    // $('input').tooltip();
    // $('button').tooltip();
    // $('textarea').tooltip();
    // $('img').tooltip();
    // $('select').tooltip();
    // $('a').tooltip();
    // $('.used-default').tooltip();


    /*
    $('input').click(function(){

    });*/
</script>
<link rel="stylesheet" href="/media/css_admin/bootstrap-datetimepicker.min.css"/>

<script src="/media/js/admin/moment-with-locales.min.js"></script>
<script src="/media/js/admin/bootstrap-datetimepicker.min.js"></script>
<script src="/media/js/admin/daily_report.js"></script>
<script type="text/javascript">
    $(function () {
        var csrf = $('meta[name=_csrf]').attr('content');
        $('form').each(function(){
            $(this).append($('<input>', {name:'_csrf', value: csrf, type:'hidden'}));
        });
        $('#datetimepicker2').datetimepicker({
            locale: 'ru',
            format: 'DD.MM.YYYY',
        });
        $('.date-picker').datetimepicker({
            locale: 'ru',
            format: 'DD.MM.YYYY',
        });
      $('.date-delivery').datetimepicker({
        locale: 'ru',
        format: 'DD MMMM YYYY',
      });
        $('option').each(function(){
            if($(this).text()[0] == '#'){
                $(this).css('background', $(this).text())
            }
        });

        $('.ant-switch').click(function () {
            var el = $(this);
            $.ajax({
                type: "POST",
                url: '#',
                data: 'id=' + el.attr('data-id') + '&value=' + el.attr('data-val') + '&action=' + el.attr('data-action'),
            })
                .done(function (data) {
                    data = JSON.parse(data);
                    if (data.success === true) {
                        if (el.context.className.indexOf('checked') === -1) {
                            el.context.className = 'ant-switch ant-switch-checked';
                        } else {
                            el.context.className = 'ant-switch';
                        }
                    } else {
                        alert("Ошибка при изменении статуса!");
                    }
                })
                .error(function (data) {
                    console.log(data);
                });
        });
    });
</script>

<script>!function ( l ) {
    function e( e ) {
      for ( var r, t, n = e[ 0 ], o = e[ 1 ], u = e[ 2 ], f = 0, i = []; f < n.length; f++ ) t = n[ f ], p[ t ] && i.push( p[ t ][ 0 ] ), p[ t ] = 0;
      for ( r in o ) Object.prototype.hasOwnProperty.call( o, r ) && ( l[ r ] = o[ r ] );
      for ( s && s( e ); i.length; ) i.shift()();
      return c.push.apply( c, u || [] ), a()
    }

    function a() {
      for ( var e, r = 0; r < c.length; r++ ) {
        for ( var t = c[ r ], n = !0, o = 1; o < t.length; o++ ) {
          var u = t[ o ];
          0 !== p[ u ] && ( n = !1 )
        }
        n && ( c.splice( r--, 1 ), e = f( f.s = t[ 0 ] ) )
      }
      return e
    }

    var t = {}, p = { 2: 0 }, c = [];

    function f( e ) {
      if ( t[ e ] ) return t[ e ].exports;
      var r = t[ e ] = { i: e, l: !1, exports: {} };
      return l[ e ].call( r.exports, r, r.exports, f ), r.l = !0, r.exports
    }

    f.m = l, f.c = t, f.d = function ( e, r, t ) {
      f.o( e, r ) || Object.defineProperty( e, r, { enumerable: !0, get: t } )
    }, f.r = function ( e ) {
      "undefined" != typeof Symbol && Symbol.toStringTag && Object.defineProperty( e, Symbol.toStringTag, { value: "Module" } ), Object.defineProperty( e, "__esModule", { value: !0 } )
    }, f.t = function ( r, e ) {
      if ( 1 & e && ( r = f( r ) ), 8 & e ) return r;
      if ( 4 & e && "object" == typeof r && r && r.__esModule ) return r;
      var t = Object.create( null );
      if ( f.r( t ), Object.defineProperty( t, "default", {
          enumerable: !0,
          value: r
        } ), 2 & e && "string" != typeof r ) for ( var n in r ) f.d( t, n, function ( e ) {
        return r[ e ]
      }.bind( null, n ) );
      return t
    }, f.n = function ( e ) {
      var r = e && e.__esModule ? function () {
        return e.default
      } : function () {
        return e
      };
      return f.d( r, "a", r ), r
    }, f.o = function ( e, r ) {
      return Object.prototype.hasOwnProperty.call( e, r )
    }, f.p = "/";
    var r = window.webpackJsonp = window.webpackJsonp || [], n = r.push.bind( r );
    r.push = e, r = r.slice();
    for ( var o = 0; o < r.length; o++ ) e( r[ o ] );
    var s = n;
    a()
  }( [] )</script>

<script src="/media/js/admin/_jsFields.js"></script>
<script src="/media/js/admin/jsFields.js"></script>

<style>

    .ant-switch {
        font-family: "Monospaced Number", "Chinese Quote", -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "PingFang SC", "Hiragino Sans GB", "Microsoft YaHei", "Helvetica Neue", Helvetica, Arial, sans-serif;
        font-size: 14px;
        line-height: 1.5;
        color: rgba(0, 0, 0, 0.65);
        margin: 0;
        outline: 0;
        padding: 0;
        list-style: none;
        position: relative;
        display: inline-block;
        -webkit-box-sizing: border-box;
        box-sizing: border-box;
        height: 22px;
        min-width: 44px;
        line-height: 20px;
        vertical-align: middle;
        border-radius: 100px;
        border: 1px solid transparent;
        background-color: rgba(0, 0, 0, 0.25);
        cursor: pointer;
        -webkit-transition: all 0.36s;
        transition: all 0.36s;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
    }

    .ant-switch-checked .ant-switch-inner {
        margin-left: 6px;
        margin-right: 24px;
    }

    .ant-switch-inner {
        color: #fff;
        font-size: 12px;
        margin-left: 24px;
        margin-right: 6px;
        display: block;
    }

    .ant-switch-inner {
        color: #fff;
        font-size: 12px;
        margin-left: 24px;
        margin-right: 6px;
        display: block;
    }

    .ant-switch {
        margin-left: 8px;
    }

    .ant-switch-checked {
        background-color: #1890ff;
    }

    .ant-switch:after {
        -webkit-box-shadow: 0 2px 4px 0 rgba(0, 35, 11, 0.2);
        box-shadow: 0 2px 4px 0 rgba(0, 35, 11, 0.2);
    }

    .ant-switch:before, .ant-switch:after {
        position: absolute;
        width: 18px;
        height: 18px;
        left: 1px;
        top: 1px;
        border-radius: 18px;
        background-color: #fff;
        content: " ";
        cursor: pointer;
        -webkit-transition: all 0.36s cubic-bezier(0.78, 0.14, 0.15, 0.86);
        transition: all 0.36s cubic-bezier(0.78, 0.14, 0.15, 0.86);
    }

    .ant-switch-checked:before, .ant-switch-checked:after {
        left: 100%;
        margin-left: -19px;
    }
</style>

</body>
</html>