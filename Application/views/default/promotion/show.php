<div class="promotion_wrap">
	<div class="title">
		<h1><?= $promotion['name']; ?></h1>
	</div>
	<div class="promotion_content">
		<div class="promotion_description">
			<span>
				Описание акции
			</span>
			<?= $promotion['description']; ?>
		</div>
		<?php if ( $promotion['percent'] ): ?>
			<div class="promotion_percent">
				<span>
					Скидка
				</span>
				<span>
					-<?= $promotion['percent']; ?>%
				</span>
			</div>
		<?php endif; ?>
		<div class="interval">
			<div class="promotion_start">
				<span>Начало действия</span>
				<?= $promotion['start']; ?>
			</div>
			<div class="promotion_finish">
				<span>
					Окончание действия
				</span>
				<?= $promotion['finish']; ?>
			</div>
		</div>
		
		<?php if ( isset($promotion['related_products']) && count($promotion['related_products']) > 0 ): ?>
			<?php $relatedProducts = $promotion['related_products']; ?>
			<div class="related_products_wrap">
				<div class="title">
					<h3>
						Товары по акции
					</h3>
				</div>
				<?php foreach ($relatedProducts as $product): ?>
					<div class="product_item">
						<a href="/catalog/product/<?= $product['alias']; ?>"><?= $product['name']; ?></a>
					</div>
				<?php endforeach; ?>
			</div>
		<?php endif; ?>
	</div>
</div>