<div class="promotion_wrap">
	<div class="title">
		<h1>Список акций</h1>
	</div>
	<?php if ( count($promotions) > 0 ): ?>
		<div class="promotions_list">
			<?php foreach ($promotions as $promotion): ?>
				<div class="promotion_item">
					<a href="/promotions/<?= $promotion['alias']; ?>"><?= $promotion['name']; ?></a>
				</div>
			<?php endforeach; ?>
		</div>
	<?php endif; ?>
</div>