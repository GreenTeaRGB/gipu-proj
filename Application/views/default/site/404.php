<section class="contaner-fluid">
    <div class="container">

        <div class="error-404">
            <p class="page-path">
                <a href="/">Главная</a>
                / 404
            </p>
            <div class="col-lg-5">
                <div class="error-404__text">
						<span>
							404
						</span>
                    <span class="no-found">
							К сожалению данной страницы не существует !
						</span>
                    <button class="btn btn_red ">
                        Перейти на главную страницу
                        <i class="icon-arrow-right"></i>
                    </button>
                </div>
            </div>
            <div class="col-lg-7 hidden">
                <div class="images">
                    <div class="bg2">

                    </div>
                    <div class="bg3">

                    </div>
                </div>
            </div>

        </div>

    </div>
</section>