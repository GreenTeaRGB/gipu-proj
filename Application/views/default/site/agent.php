<script src="/media/js/default/jquery.scrollify.js"></script>
<div class="promo-landing-page">
  <nav class="l-navigation">
    <ul class="list">
      <li class="current"></li>
    </ul>
  </nav>
  <section class="promo-landing-header ">
    <div class="landing-header-bg top-left"></div>
    <div class="landing-header-bg top-right"></div>
    <div class="landing-header-bg woman"></div>
    <div class="landing-header-bg woman_icons"></div>
    <div class="landing-header-bg stars_top-right"></div>
    <div class="landing-header-bg stars_top-left"></div>
    <div class="landing-header-bg stars_1"></div>
    <div class="container">
      <div class="row">
        <div class="col-sm-6">
          <h1>
            Пожалуй, самый легкий способ начать свой бизнес!
          </h1>
          <p>
            Gipu - инновационная платформа, которая позволит вам открыть полноценный интернет-магазин с минимальными
            вложениями и без организации сложных схем доставки или работы с поставщиками.
          </p>
          <a href="#!" class="btn btn_red"> Начать сейчас </a>
        </div>
        <div class="know-more_cont">
          <a href="#2" class="know-more">
            <img src="../build/img/promo-landing/mouse.png" alt="">
            Узнать больше
          </a>
        </div>
      </div>
    </div>
  </section>
  <section class="store-creating scrollify" id="creating">
    <div class="store-creating-bg"></div>
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="screen-number-cont">
            <span class="screen-number first"> 1 </span>
          </div>
          <h2>
            Создание
            интернет-магазина
          </h2>
        </div>
        <div class="col-sm-4">
          <h3>
            Самостоятельно
          </h3>
          <div class="independently">
            <div class="resource-left">
              <span class="count"> 150 тыс. руб. </span>
              <span class="spent"> денег потрачено </span>
            </div>
            <div class="resource-right">
              <span class="count"> 2 мес. </span>
              <span class="spent"> времени потрачено </span>
            </div>
          </div>
          <p>
            Подробнее об этапе создания интернет магазина самостоятельно. Именно в описании стоит указывать примерные
            финансовые затраты.
          </p>
        </div>
        <div class="col-sm-4">
          <!-- <img src="build/img/promo-landing/creating-image.png" alt=""> -->
          <div class="landing-images-container">
            <div class="store-creating-image"></div>
            <div class="gradient-bg"></div>
          </div>

        </div>
        <div class="col-sm-4">
          <h3>
            С помощью Gipu
          </h3>
          <div class="gipu">
            <div class="resource-left">
              <span class="count"> 0 руб. </span>
              <span class="spent"> денег потрачено </span>
            </div>
            <div class="resource-right">
              <span class="count"> 15 минут </span>
              <span class="spent"> времени потрачено </span>
            </div>
          </div>
          <p>
            Вместо трудоемкого процесса создания сайта, вы можете сосредоточиться на продажах товаров, не отвлекаясь на
            дополнительные трудности. gipu организовывает полную синхронизацию базы товаров вашего сайта с основной базой, и
            самостоятельно добавляет систему оплаты к вашему сайту.
          </p>
        </div>
      </div>
    </div>
  </section>
  <section class="suppilers-and-goods scrollify">
    <div class="suppilers-and-goods-bg"></div>
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="screen-number-cont">
            <span class="screen-number"> 2 </span>
          </div>
          <h2>
            Привлечение поставщиков и закупка товаров
          </h2>
        </div>
        <div class="col-sm-4">
          <h3>
            Самостоятельно
          </h3>
          <div class="independently">
            <div class="resource-left">
              <span class="count"> 450 тыс. руб. </span>
              <span class="spent"> денег потрачено </span>
            </div>
            <div class="resource-right">
              <span class="count"> 4 мес. </span>
              <span class="spent"> времени потрачено </span>
            </div>
          </div>
          <p>
            В зависимости от поставщика, он может давать разные условия для работы. Чаще всего вам придется закупать товар у
            поставщика, либо договариваться с ним, нам более выгодные для вас условия, в ущерб закупочным ценам. За счет
            малого оборота, у вас не будет конкурентных цен от поставщика.
          </p>
        </div>
        <div class="col-sm-4">
          <!-- <img src="build/img/promo-landing/suppilers-and-goods-image.png" alt="">
           -->
          <div class="landing-images-container">
            <div class="suppilers-and-goods-image"></div>
            <div class="suppilers-and-goods-image_2"></div>
            <div class="gradient-bg"></div>
          </div>
        </div>
        <div class="col-sm-4">
          <h3>
            С помощью Gipu
          </h3>
          <div class="gipu">
            <div class="resource-left">
              <span class="count"> 0 руб. </span>
              <span class="spent"> денег потрачено </span>
            </div>
            <div class="resource-right">
              <span class="count"> 15 минут </span>
              <span class="spent">
								времени потрачено
							</span>
            </div>
          </div>
          <p>
            У нас на сайте вы можете без каких либо дополнительных сложностей выбрать подходящее по тематике вашего магазина
            товары по оптовым ценам.
          </p>
        </div>
      </div>
    </div>
  </section>
  <section class="workers scrollify">
    <div class="workers-bg"></div>
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="screen-number-cont">
            <span class="screen-number"> 3 </span>
          </div>
          <h2>
            Найм штата сотрудников, организация бух учета,
            доставки товара и колл-центра
          </h2>
        </div>
        <div class="col-sm-4">
          <h3>
            Самостоятельно
          </h3>
          <div class="independently">
            <div class="resource-left">
              <span class="count"> 600 тыс. руб. </span>
              <span class="spent"> денег потрачено </span>
            </div>
            <div class="resource-right">
              <span class="count"> 7 мес. </span>
              <span class="spent"> времени потрачено </span>
            </div>
          </div>
          <p>
            Подробнее об этапе создания интернет магазина самостоятельно. Именно в описании стоит указывать примерные
            финансовые затраты.
          </p>
        </div>
        <div class="col-sm-4">
          <!-- <img src="build/img/promo-landing/workers-image.png" alt=""> -->
          <div class="landing-images-container">
            <div class="workers-image"></div>
            <div class="gradient-bg"></div>
          </div>
        </div>
        <div class="col-sm-4">
          <h3>
            С помощью Gipu
          </h3>
          <div class="gipu">
            <div class="resource-left">
              <span class="count"> 0 руб. </span>
              <span class="spent"> денег потрачено </span>
            </div>
            <div class="resource-right">
              <span class="count"> 15 минут </span>
              <span class="spent"> времени потрачено </span>
            </div>
          </div>
          <p>
            Gipu берет на себя все трудности, с которыми вы можете столкнуться на данном этапе. Мы организовываем колл-центр,
            который отвечает на все звонки ваших клиентов, осуществляем как доставку товаров до двери, так и пункты
            самовывоза
            товаров, полное бухгалтерское и юридическое сопровождение.
          </p>
        </div>
      </div>
    </div>
  </section>
  <section class="last-step scrollify">
    <div class="last-step-bg"></div>
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="screen-number-cont">
            <span class="screen-number"> 4 </span>
          </div>
          <h2>
            Конечный этап
          </h2>
        </div>
        <div class="col-sm-4">
          <h3>
            Самостоятельно
          </h3>
          <div class="independently">
            <div class="resource-left">
              <span class="count"> 600 тыс. руб. </span>
              <span class="spent"> денег потрачено </span>
            </div>
            <div class="resource-right">
              <span class="count"> 7 мес. </span>
              <span class="spent">
								времени потрачено
							</span>
            </div>
          </div>
          <p>
            Только с этого момента ваш сайт начинает работать и, возможно, приносить вам прибыль.
          </p>
        </div>
        <div class="col-sm-4">
          <!-- <img src="build/img/promo-landing/last-step-image.png" alt=""> -->
          <div class="landing-images-container">
            <div class="last-step-image"></div>
            <!-- <div class="workers-image_2"></div> -->
            <div class="gradient-bg"></div>
          </div>
        </div>
        <div class="col-sm-4">
          <h3>
            С помощью Gipu
          </h3>
          <div class="gipu">
            <div class="resource-left">
              <span class="count"> 15 тыс. руб. </span>
              <span class="spent"> денег потрачено </span>
            </div>
            <div class="resource-right">
              <span class="count"> 15 минут </span>
              <span class="spent"> времени потрачено </span>
            </div>
          </div>
          <p>
            С сервисом gipu, к данному моменту, вы работаете уже 7 месяцев. За данный период времени по статистике, вы
            заработали 1 рубль и прочее и прочее
          </p>
        </div>
      </div>
    </div>
  </section>
  <section class="passive-income scrollify">
    <div class="background"></div>
    <div class="container">
      <div class="row">
        <div class="slider-buttons">
          <button class="slider-button next-slide icon-right-side"></button>
          <button class="slider-button prew-slide icon-left-side"></button>
        </div>
        <div class="col-md-12 col-lg-10 slider-cont">
          <div class="col-xs-12 headers">
            <div class="push-md-3 col-md-8 col-lg-6">
              <h2>
                А ещё Gipu - это отличная
                система пасивного дохода
              </h2>
              <p>
                Партнерская программа представляет собой инструмент для индивидуального развития собственной торговой сети без
                ограничений
              </p>
            </div>
          </div>
          <div class="korpus">
            <div class="navigation">
              <input type="radio" name="tab" id="vkl1" />
              <label for="vkl1" class="current">1. Станьте агентом</label>
              <input type="radio" name="tab" id="vkl2" />
              <label for="vkl2">2. Получайте процент с продаж</label>
              <input type="radio" name="tab" id="vkl3" />
              <label for="vkl3">3. Увеличьте доход</label>
              <input type="radio" name="tab" id="vkl4" />
              <label for="vkl4">4. Создайте новую сеть</label>
            </div>
            <div class="block open">
              <div class="col-sm-6">
                <div class="slide-image slide-image_1"></div>
              </div>
              <div class="col-md-6">
                <p class="slide-text">
                  Каждый человек пользуясь нашим сервисом имеет возможность зарабатывать не только на продажах товаров, но и на
                  развитии собственной торговой сети!
                </p>
                <p class="slide-text">
                  Вы получаете вознаграждения за привлечение новых пользователей для нашего сервиса, тем самым формируя свою
                  торговую сеть. Повышаете свое «звание» с которым вы получаете возможность иметь пассивный доход ( % от каждой
                  сделки ) со всей своей реферальной сети!
                </p>
              </div>
            </div>
            <div class="block">
              <div class="col-sm-6">
                <div class="slide-image slide-image_2"></div>
              </div>
              <div class="col-sm-6">
                <p class="slide-text">
                  2222222222222222222222222222222222
                </p>
              </div>
            </div>
            <div class="block">
              <div class="col-sm-6">
                <div class="slide-image slide-image_3"></div>
              </div>
              <div class="col-sm-6">
                <p class="slide-text">
                  333333333333333333333333333333

                </p>
              </div>
            </div>
            <div class="block">
              <div class="col-sm-6">
                <div class="slide-image slide-image_4"></div>
              </div>
              <div class="col-sm-6">
                <p class="slide-text">
                  444444444444444444444

                </p>
              </div>

            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section class="how-it-work">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <h2>
            Как это работает
          </h2>
        </div>
        <div class="col-sm-6 col-md-4">
          <div class="how-it-work__step">
            <div class="step__image lazy" data-src="build/img/promo-landing/step1-image.png">
              <span class="step__number"> 1 </span>
            </div>
            <p class="step__text">
              Создайте свой магазин, использовав конструктор или готовые шаблоны.
            </p>
          </div>
        </div>
        <div class="col-sm-6 col-md-4">
          <div class="how-it-work__step">
            <div class="step__image lazy" data-src="build/img/promo-landing/step2-image.png">
              <span class="step__number"> 2 </span>
            </div>
            <p class="step__text">
              Выберите категорию, чтобы товары сами загрузились в ваш магазин.
            </p>
          </div>
        </div>
        <div class="col-sm-6 col-md-4">
          <div class="how-it-work__step">
            <div class="step__image lazy" data-src="build/img/promo-landing/step3-image.png">
              <span class="step__number"> 3 </span>
            </div>
            <p class="step__text">
              Привлеките покупателя в свой готовый магазин.
            </p>
          </div>
        </div>
        <div class="col-sm-6 col-md-4">
          <div class="how-it-work__step">
            <div class="step__image lazy" data-src="build/img/promo-landing/step4-image.png">
              <span class="step__number"> 4 </span>
            </div>
            <p class="step__text">
              Он выбирает понравившийся товар и оформляет заказ.
              Оплата заказа производится на стороне нашего сервиса, мы сразу получаем ее.
            </p>
          </div>
        </div>
        <div class="col-sm-6 col-md-4">
          <div class="how-it-work__step">
            <div class="step__image lazy" data-src="build/img/promo-landing/step5-image.png">
              <span class="step__number"> 5 </span>
            </div>
            <p class="step__text">
              Мы берем товар с нашего склада и доставляем его покупателю. Вам не нужно прикладывать никаких усилий по
              орагинизации доставки.
            </p>
          </div>
        </div>
        <div class="col-sm-6 col-md-4">
          <div class="how-it-work__step">
            <div class="step__image lazy" data-src="build/img/promo-landing/step6-image.png">
              <span class="step__number"> 6 </span>
            </div>
            <p class="step__text">
              Вы получаете вознаграждение: до 15% стоимости каждого купленного в вашем магазине товара.
            </p>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section class="you-get">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <h2>
            Что вы получаете?
          </h2>
        </div>
        <div class="col-sm-4">
          <div class="you-have you-have_1 you-have_active">
            <span class="lazy" data-src="build/img/promo-landing/constructor.png"></span>
            <h4>
              Удобный конструктор
            </h4>
            <p>
              Супер удобный конструктор, позволяющий создать современный интернет магазин, отражающий вашу уникальную идею
            </p>

          </div>
          <div class="you-have you-have_2">
            <span class="lazy" data-src="build/img/promo-landing/choosing-products.png"></span>
            <h4>
              Огромный выбор товаров
            </h4>
            <p>
              Возможность выбора любой категории продуктов для загрузки в магазин с уже подобранным классным ассортиментом
            </p>
          </div>
          <div class="you-have you-have_3">
            <span class="lazy" data-src="build/img/promo-landing/turnkey-sales.png"></span>
            <h4>
              Продажи «под ключ»
            </h4>
            <p>
              Полную реализацию процесса продажи-доставки покупетелю на стороне сервиса
            </p>
          </div>
        </div>
        <div class="col-sm-8 you-have_inner">
          <img src="build/img/promo-landing/UI.jpg" alt="">
        </div>
      </div>
    </div>
  </section>
  <section class="promo-landing-footer">
    <div class="landing-footer-bg"></div>
    <div class="landing-footer-bg_2"></div>
    <div class="landing-footer-bg_3"></div>
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <h2>
            Начните сегодня
          </h2>
          <p>
            Создаёте свой магазин на Gipu и начните продажи через 5 минут!
          </p>
          <a href="#!" class="btn btn_red">
            Открыть магазин
          </a>
        </div>
      </div>
    </div>
  </section>
</div>
