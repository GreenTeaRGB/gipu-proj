<section class="container offers">
  <div class="corousel corousel_big corousel_infinity">
    <?php foreach ( $slider as $slid ) : ?>
      <div class="corousel_big__cell lazy-at-once" style="background-color: #eaddce"
           data-src="<?= $slid['image'] ?>">
        <i class="f-green mb"><?= $slid['text_cursive'] ?></i>
        <p class="f-light"><?= $slid['text'] ?></p>
        <b class="f-black d_b"><?= $slid['text_bold'] ?></b>
        <a href="<?= $slid['link'] ?>" class="btn btn_red">Подробнее <i class="icon-arrow-right"></i></a>
      </div>
    <?php endforeach; ?>
  </div>

  <div class="corousel corousel_small corousel_static">
    <?php foreach ( $infoSlider as $info ) :
      list( $r, $g, $b, $a ) = explode( ':', $info['color'] );
      ?>
      <a href="<?= $info['link'] ?>" class="corousel_small__cell no-shadow lazy-at-once"
         style="background-color: rgba(<?= $r . ',' . $g . ',' . $b . ',' . $a ?>)" data-src="<?= $info['image_bg'] ?>">
        <span class="f-light color_white"><?= $info['text'] ?></span>
        <img width="50px" src="<?= $info['image'] ?>" alt="">
      </a>
    <?php endforeach; ?>
  </div>
</section>
<section class="container showcase">
  <h2>Популярные товары</h2>
  <div class="container">
    <?php foreach ( $popular as $product ):
      $this->block( 'layouts/product', [ 'product' => $product, 'class' => 'col-lg-2 col-sm-3 col-xs-6 product' ] );
      ?>

      <!--          <div class="col-lg-2 col-sm-3 col-xs-6 product">-->
      <!--              <div class="product__options">-->
      <!--                  <a class="add_to_wishlist --><?php //echo( ( isset( $product['wishlist'] ) && $product['wishlist'] )
//                      ? 'added'
//                      : '' );
      ?><!--" data-id="--><? //= $product['id'];
      ?><!--"><i class="icon-like"></i></a>-->
      <!--                  <a class="add_to_compare" data-id="--><? //= $product['id'];
      ?><!--"><i class="icon-stats"></i></a>-->
      <!--              </div>-->
      <!--              <a href="/catalog/product/--><? //= $product['alias']
      ?><!--">-->
      <!--                  <div class="lazy product__photo" data-src="--><? //= isset( $product['images'][0] )
//                      ? $product['images'][0]
//                      : ''
      ?><!--"></div>-->
      <!--                  <i class="sub-text">--><? //= $product['category_name']
      ?><!--</i>-->
      <!--                  <p>--><? //= $product['name']
      ?><!--</p>-->
      <!--                  <b class="price ruble">--><? //= $product['price']
      ?><!--</b>-->
      <!--              </a>-->
      <!--          </div>-->
    <?php endforeach; ?>
  </div>
  <?= $this->block( 'layouts/brands' ) ?>
  <?php if ( count( $services ) > 0 ): ?>
    <h2>Популярные услуги</h2>
    <div class="corousel_small_circle corousel_static services">
      <?php foreach ( $services as $service ) : ?>
        <div class="service"
             style="background-image: url(<?= isset( $service['images'][0] ) ? $service['images'][0] : '' ?>)">
          <a href="/catalog/product/<?= $service['alias'] ?>">
            <p><?= $service['name'] ?> <span class="arrow arrow_small">
						<svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 129 129"
                 enable-background="new 0 0 129 129"><g><path
                  d="m40.4,121.3c-0.8,0.8-1.8,1.2-2.9,1.2s-2.1-0.4-2.9-1.2c-1.6-1.6-1.6-4.2 0-5.8l51-51-51-51c-1.6-1.6-1.6-4.2 0-5.8 1.6-1.6 4.2-1.6 5.8,0l53.9,53.9c1.6,1.6 1.6,4.2 0,5.8l-53.9,53.9z" /></g></svg>
					</span></p>
          </a>
        </div>
      <?php endforeach; ?>
    </div>
  <?php endif; ?>
  </div>
</section>
<section class="container news">
  <h2>Новости</h2>
  <div>
    <?php
    $first = true;
    foreach ( $newsCategories as $newsCategory ) : ?>
      <a href="#"
         class="red-stroke news-index <?= $first
           ? 'red-stroke_current'
           : '' ?>"
         data-id="<?= $newsCategory['id'] ?>"><?= $newsCategory['name'] ?></a>
      <?php $first = false; endforeach; ?>
  </div>
  <div class="col-md-12" id="content-news">
    <?php foreach ( $news as $new ) : ?>
      <div class="col-md-6 col-sm-12 news__item news__item_date">
        <img data-src="<?= $new['image'] ?>" alt="" class="lazy">
        <i class="date-time"><?= $new['created_at'] ?></i>
        <a href="/news/show/<?= $new['alias'] ?>">
          <span class="sub-name"><?= $new['name'] ?></span>
          <p class="desc-text"><?= $new['short_description'] ?></p>
        </a>
      </div>
    <?php endforeach; ?>
  </div>
  <a href="/news/<?= $firstCategory['alias'] ?>" id="full_news" class="btn btn_white center-block">Остальные новости <i
      class="icon-arrow-right"></i></a>
</section>
<?php
if ( count( $newsCategories ) > 1 ) { ?>
  <script>
    $( document ).ready( function () {
      var news = new News( {
        news: <?=json_encode( [ $firstCategory['id'] => $news ] )?>,
        categories: <?=json_encode( $newsCategories )?>
      } );
    } );
  </script>
<?php } ?>