<?php
	\Application\Classes\Session::remove('last_msg_id');
?>
<div class="dashboard_wrap">
	<?php $this->block('user/' . $user['type_code'] . '/header_title'); ?>
	<?php $this->block('user/' . $user['type_code'] . '/' . $user['type_code'] . '_header'); ?>
	
	<div class="loader"><span></span></div>
	<div class="dialog_wrap">
		<div class="left">
			<div class="title">
				<h1>Диалоги</h1>
			</div>
			<div class="dialogs_list"></div>
			<div class="search_user">
				<span>Начать диалог с пользователем</span>
				<div class="s_message"></div>
				<label for="user_id">id:</label>
				<input type="text" id="user_id" placeholder="32134890">
				<a id="search_user">Искать</a>
			</div>
			<div class="search_results"></div>
		</div>
		<div class="right">
			<div class="panel"></div>
			<div class="message_history"></div>
			<div class="send_block">
				<form action="/dialog/send-message" method="post" enctype="multipart/form-data">
<!--					<div>-->
<!--						<span>Прикрепить</span>-->
<!--						<input type="file" name="attachment">-->
<!--					</div>-->
					<div>
						<textarea name="text" id="dialog_message"></textarea>
						<a id="send" data-id="<?php echo $user['id']; ?>" data-unique="<?= $uniqueId = \Application\Helpers\AppHelper::generateRandomString(); ?>">Отправить</a>
					</div>
				</form>
			</div>
		</div>
	</div>
	<div class="new_message_wrap"></div>
</div>
<script>
	var Dialog = function () {
		var self = this;
		self.dialogs = [];
		self.currentDialogId = '';
		self.sendedMsgId = '';
		
		var currentUser = {
			id: "<?= $user['id']; ?>",
			name: "<?= $user['name']; ?>",
			avatar: ("<?= $user['avatar']; ?>") ? "<?= $user['avatar']; ?>" : '/images/no_photo.png'
		};
		
		self.init = function () {
			self.loadDialogs();
			self.addEventListeners();
		};
		
		self.loadDialogs = function () {
			var loader = $('.loader');
			$.ajax({
				url: '/dialog/load-dialogs',
				data: {
					is_ajax: true
				},
				dataType: "json",
				type: 'post',
				beforeSend: function () {
					loader.show();
				}
			}).done(function (response) {
				loader.hide();
				if (response && response.hasOwnProperty('success') && response.success) {
					var dialogs = response.dialogs;
					
					for (var key in dialogs) {
						if (dialogs.hasOwnProperty(key)) {
							var dialog = dialogs[key];
							if (dialog.hasOwnProperty('messages') && dialog.messages.length > 0) {
								var countUnreadMsgs = 0;
								for (var msgKey in dialog.messages) {
									if (dialog.messages.hasOwnProperty(msgKey)) {
										var message = dialog.messages[msgKey];
										if (message.user_id != currentUser.id && message.status == 0) {
											countUnreadMsgs++;
										}
									}
								}
								dialog.count_unread_msgs = countUnreadMsgs;
							}
						}
					}
					
					self.dialogs = dialogs;
				}
				
				self.showDialogs(self.dialogs);
				setTimeout(function () {
					self.updateDialogs();
				}, 3000);
			}).fail(function (response) {
				loader.hide();
				
				var message = $('<div />', {
						class: 'message message_error',
						text: response.message
					}
				);
				$('.message').append(message);
				
				setTimeout(function () {
					message.remove()
				}, 3000);
			});
		};
		
		self.updateUnreadMessages = function (dialogId, countUnreadMsgs) {
			var dialogsView = $('.dialogs_list .dialog_user');
			dialogsView.each(function (index, elem) {
				var element = $(elem);
				if (element.data('dialog_id') == dialogId) {
					if (countUnreadMsgs >= 1) {
						element.find('.count_unread_messages').text(countUnreadMsgs);
						
						for (var dialogKey in self.dialogs) {
							if (self.dialogs.hasOwnProperty(dialogKey)) {
								if (self.dialogs[dialogKey].id == dialogId) {
									self.dialogs[dialogKey].count_unread_msgs = countUnreadMsgs;
								}
							}
						}
						
					} else {
						element.find('.count_unread_messages').text('');
						
						for (var dialogKey in self.dialogs) {
							if (self.dialogs.hasOwnProperty(dialogKey)) {
								if (self.dialogs[dialogKey].id == dialogId) {
									self.dialogs[dialogKey].count_unread_msgs = 0;
								}
							}
						}
					}
				}
			});
		};
		
		self.showDialogs = function (dialogs) {
			var chatContainer = $('.message_history');
			var dialogContainer = $('.dialogs_list');
			if (dialogs && dialogs.length > 0) {
				dialogContainer.empty();
				for (var key in dialogs) {
					if (dialogs.hasOwnProperty(key)) {
						var dialog = dialogs[key];
						dialogContainer.append(
							$('<a />', {
								class: 'dialog_user'
							}).attr({
								'data-uid': dialog.companion.id,
								'data-dialog_id': dialog.id
							}).append(
								$('<img />', {
									class: 'user_avatar',
									src: (dialog.companion.avatar) ? dialog.companion.avatar : '/images/no_photo.png'
								})
							).append(
								$('<div />', {
									class: 'user_name',
									text: dialog.companion.name
								})
							).append(
								$('<span />', {
									class: 'count_unread_messages',
									text: dialog.hasOwnProperty('count_unread_msgs') && dialog.count_unread_msgs ? dialog.count_unread_msgs : ''
								})
							)
						);
					}
				}
			} else {
				dialogContainer.append(
					$('<div />', {
						class: 'no_items',
						text: 'У Вас пока нет диалогов'
					})
				);
			}
		};
		
		self.addEventListeners = function () {
			$('.dialog_wrap').on('click', '#search_user', function () {
				var that = $(this);
				var searchInput = $('#user_id');
				var userId = searchInput.val();
				
				var message;
				
				if (userId) {
					$.ajax({
						url: '/dialog/search-user',
						data: {
							user_id: userId,
							is_ajax: true
						},
						dataType: "json",
						type: 'post',
						beforeSend: function () {
							that.text('Поиск...');
							that.prop("disabled", true);
						}
					}).done(function (response) {
						that.text('Искать');
						that.prop("disabled", false);
						var resultContainer = $('.search_results');
						resultContainer.empty();
						
						if (response && response.hasOwnProperty('success') && response.success) {
							$('<div />', {
								class: 'found_wrap'
							}).append(
								$('<a />', {
									class: 'dialog_user'
								})
									.attr('data-uid', response.searched_user.id)
									.append(
										$('<img />', {
											class: 'user_avatar',
											src: (response.searched_user.avatar) ? response.searched_user.avatar : '/images/no_photo.png'
										})
									)
									.append(
										$('<div />', {
											class: 'user_name',
											text: response.searched_user.name
										})
									)
							).appendTo(resultContainer);
						} else {
							message = $('<div />', {
									class: 'message message_error',
									text: response.message
								}
							);
							$('.s_message').empty();
							$('.s_message').append(message);
							
							setTimeout(function () {
								message.remove();
							}, 3000);
						}
						
						searchInput.val('');
					});
				} else {
					searchInput.val('');
					message = $('<div />', {
							class: 'message message_error',
							text: 'Введите Id пользователя'
						}
					);
					$('.s_message').empty();
					$('.s_message').append(message);
					
					setTimeout(function () {
						message.remove();
					}, 3000);
				}
				
				return false;
			});
			
			$('.search_results, .dialogs_list').on('click', '.dialog_user', function () {
				var that = $(this);
				var uid = that.data('uid');
				var dialogId;
				
				self.currentDialogId = '';
				
				if (that.data('dialog_id')) {
					dialogId = that.data('dialog_id');
					self.currentDialogId = dialogId;
				}
				
				var block = that.clone();
				var panel = $('.panel');
				var messageHistory = $('.message_history');
				var sendBlock = $('.send_block');
				
				if (that.closest('.search_results')) {
					$('.search_results').empty();
				}
				
				self.cleanDialog(panel);
				
				panel.show();
				
				var dialogWith = $('<div />', {
					class: 'dialog_with'
				}).attr('data-uid', uid);
				
				if (dialogId) {
					dialogWith.attr('data-dialog_id', dialogId);
				}
				
				dialogWith.append(block.children());
				dialogWith.appendTo(panel);
				
				messageHistory.empty();
				messageHistory.show();
				
				sendBlock.show();
				
				panel.append(
					$('<div />', {
						class: 'close_dialog'
					}).append(
						$('<a />', {
							text: 'Закрыть'
						})
					)
				);
			});
			
			$('.dialog_wrap').on('click', '#send', function () {
				var that = $(this);
				var message = $('#dialog_message');
				var userName = currentUser.name;
				var userId = $('.dialog_with').data('uid');
				var dialogId = $('.dialog_with').data('dialog_id');
				
				if (message.val() != '') {
					var data = {
						is_ajax: true,
						receiver_id: userId,
						message: message.val()
					};
					
					if (dialogId) {
						data.dialog_id = dialogId;
					}
					
					var messageObj = {
						text: message.val(),
						user_id: currentUser.id
					};
					
					var generatedMessage = self.generateMessage(messageObj, true);
					self.scrollToLastMsg();
					
					$.ajax({
						url: '/dialog/send-message',
						data: data,
						dataType: "json",
						type: 'post',
						beforeSend: function () {
							that.prop("disabled", true);
						}
					}).done(function (response) {
						that.prop("disabled", false);
						var chatContainer = $('.message_history');
						if (response && response.hasOwnProperty('success') && response.success) {
							
							chatContainer.find('.no_items').remove();
							message.val('');
							if (response.hasOwnProperty('dialog_id') && response.dialog_id) {
								$('.dialog_with').attr('data-dialog_id', response.dialog_id);
								self.setNewDialogToDialogList({}, userId);
							}
							
							if (!response.hasOwnProperty('message_id') && !response.message_id) {
								generatedMessage.append(
									$('<span />', {
										class: 'message message_error',
										text: 'Ошибка отправки сообщения'
									})
								);
							} else {
								self.sendedMsgId = response.message_id;
							}
						} else {
							chatContainer.append($('<div />, {text: response.message}'));
						}
					}).fail(function (response) {
						that.prop("disabled", false);
						
						var message = $('<div />', {
								class: 'message message_error',
								text: response.message
							}
						);
						$('.message').append(message);
						
						setTimeout(function () {
							message.remove()
						}, 3000);
					});
				}
				
				return false;
			});
			
			$('.panel').on('click', '.close_dialog a', function () {
				var that = $(this);
				self.cleanDialog(that.closest('.panel'));
				
				return false;
			});
			
			$('.dialogs_list').on('click', '.dialog_user', function () {
				var that = $(this);
				var dialogId = that.data('dialog_id');
				
				var dialogs = self.dialogs;
				var messages;
				for (var key in dialogs) {
					if (dialogs.hasOwnProperty(key)) {
						if (dialogs[key].id == dialogId) {
							messages = dialogs[key].messages;
							break;
						} else {
							continue;
						}
					}
				}
				
				if (messages && messages.length > 0) {
					for (var key in messages) {
						if (messages.hasOwnProperty(key)) {
							self.generateMessage(messages[key]);
						}
					}
					self.scrollToLastMsg(function () {
						var messages = $('.message_history .d_message');
						var messagesForRead = [];
						
						messages.each(function (index, elem) {
							if ($(elem).isOnDialogViewport()) {
								messagesForRead.push($(elem));
							}
						});
						self.readMessage(messagesForRead);
					});
				}
				
				return false;
			});
			
			
			$('.dialog_wrap').on('mouseenter', '.d_message', function () {
				var that = $(this);
				
				self.readMessage([that]);
				
				return false;
			});
		};
		
		self.generateMessage = function (message, returnMessage = false) {
			var chatContainer = $('.message_history');
			chatContainer.find('.no_items').remove();
			
			var receiverImg = $('.dialog_with img.user_avatar').clone();
			var senderImgSrc = currentUser.avatar;
			
			var receiverId = $('.dialog_with').data('uid');
			
			var imgSrc;
			var userName;
			if (message.user_id != receiverId) {
				imgSrc = senderImgSrc;
				userName = currentUser.name;
			} else {
				imgSrc = receiverImg.attr('src');
				userName = $('.dialog_with .user_name').text();
			}
			
			var messageElement = $('<div />', {
				class: 'd_message'
			}).attr({
				'data-mid': message.id,
				'data-did': message.dialog_id
			}).append(
				$('<img />', {
					class: 'user_avatar',
					src: imgSrc
				})
			).append(
				$('<span />', {
					text: userName
				})
			).append(
				$('<p />', {
					html: message.text
				})
			);
			
			if (message.user_id != currentUser.id && message.status == 0) {
				messageElement.addClass('unread');
			}
			
			chatContainer.append(messageElement);
			
			if (returnMessage) {
				return messageElement;
			}
		};
		
		self.cleanDialog = function (panel) {
			if (panel && panel.length > 0) {
				panel.empty().hide();
				$('.message_history').empty().hide();
				$('.send_block').hide();
				
				$( document ).ajaxStop(function() {
					$(".loading").hide();
				});
			}
		};
		
		self.setNewDialogToDialogList = function (dialog = {}, receiver_id = '') {
			var noDialogsElement = $('.dialogs_list .no_items');
			var dialogWith = $('.dialog_with').clone();
			
			var dialogId = dialogWith.data('dialog_id');
			dialogId = (dialogId !== undefined) ? dialogId : dialog.id;
			
			var uid = (receiver_id && receiver_id !== '') ? receiver_id : dialogWith.data('uid');
			
			if (noDialogsElement && noDialogsElement.length > 0) {
				noDialogsElement.remove();
				
				var newDialogItem = $('<a />', {
					class: 'dialog_user'
				}).attr({
					'data-uid': uid,
					'data-dialog_id': dialogId
				}).append();
				
				$('.dialogs_list').prepend(newDialogItem);
			} else {
				var dialogList = $('.dialog_user');
				if (dialogList && dialogList.length > 0) {
					var exists = false;
					
					dialogList.each(function (index, elem) {
						if ($(elem).data('dialog_id') == dialogId) {
							exists = true;
						}
					});
					
					if (!exists) {
						var newDialogItem = $('<a />', {
							class: 'dialog_user'
						}).attr({
							'data-uid': uid,
							'data-dialog_id': dialogId
						}).append(
							$('<img />', {
								class: 'user_avatar',
								src: (dialog.companion.avatar) ? dialog.companion.avatar : '/images/no_photo.png'
							})
						).append(
							$('<div />', {
								class: 'user_name',
								text: dialog.companion.name
							})
						).append(
							$('<span />', {
								class: 'count_unread_messages',
								text: dialog.count_unread_msgs
							})
						);
						
						$('.dialogs_list').prepend(newDialogItem);
					} else {
						var countUnreadMsgs;
						for (var dialogKey in self.dialogs) {
							if (self.dialogs.hasOwnProperty(dialogKey)) {
								if (self.dialogs[dialogKey].id == dialogId) {
									countUnreadMsgs = (self.dialogs[dialogKey].count_unread_msgs) ? parseInt(self.dialogs[dialogKey].count_unread_msgs)++ : 0;
									self.updateUnreadMessages(dialog.id, countUnreadMsgs);
									break;
								}
							}
						}
						
					}
				}
			}
		};
		
		self.scrollToLastMsg = function (callback = false) {
			var last = document.querySelector('.message_history').lastElementChild;
			last.scrollIntoView();
			
			if (callback && typeof callback === "function") {
				callback();
			}
		};
		
		self.updateDialogs = function () {
			$.ajax({
				url: '/dialog/load-dialogs',
				data: {
					is_ajax: true
				},
				dataType : "json",
				type: 'post'
			}).done(function (response) {
				if (response.hasOwnProperty('success') && response.success && response.hasOwnProperty('dialogs') && response.dialogs ) {
					var dialogs = response.dialogs;
					for (var key in dialogs) {
						if (dialogs.hasOwnProperty(key)) {
							var dialog = dialogs[key];
							var dialogExists = false;
							
							var oldDialogs = self.dialogs;
							for (var oldKey in oldDialogs) {
								if (oldDialogs.hasOwnProperty(oldKey)) {
									var oldDialog = self.dialogs[oldKey];
									
									var countUnreadMsgs = oldDialog.count_unread_msgs;
									
									if (dialog.id == oldDialog.id) {
										dialogExists = true;
										
										if (dialog.hasOwnProperty('messages') && dialog.messages) {
											for (var messageKey in dialog.messages) {
												if (dialog.messages.hasOwnProperty(messageKey)) {
													var message = dialog.messages[messageKey];
													var messageExists = false;
													
													for (var index in self.dialogs[oldKey].messages) {
														if (self.dialogs[oldKey].messages.hasOwnProperty(index)) {
															if (self.dialogs[oldKey].messages[index].id == message.id) {
																messageExists = true;
															}
														}
													}
													
													if (message.user_id == currentUser.id) {
														if (self.sendedMsgId != '' && self.sendedMsgId == message.id) {
															self.sendedMsgId = '';
														} else {
															self.generateMessage(message);
															self.scrollToLastMsg();
														}
														if (!messageExists) {
															self.dialogs[oldKey].messages.push(message);
														}
													} else {
														self.dialogs[oldKey].messages.push(message);
														var receiverId = $('.dialog_with').data('uid');
														
														if (receiverId && dialog.companion.id == receiverId) {
															var messageObj = self.generateMessage(message, true);
															self.scrollToLastMsg(function () {
																var messagesForRead = [messageObj];
																if (messageObj.isOnDialogViewport()) {
																	self.readMessage(messagesForRead);
																}
															});
														} else {
															countUnreadMsgs++;
															self.dialogs[oldKey].count_unread_msgs = countUnreadMsgs;
															self.updateUnreadMessages(dialog.id, countUnreadMsgs);
														}
													}
												}
											}
										}
									} else {
										continue;
									}
								}
							}
							
							if (!dialogExists) {
								var receiver_id = (dialog.sender_id == currentUser.id) ? dialog.receiver_id : dialog.sender_id;
								dialog.count_unread_msgs = 1;
								self.setNewDialogToDialogList(dialog, receiver_id);
								
								self.dialogs.push(dialog);
							}
							
							if (dialog.messages && dialog.messages.length > 0) {
								for (var mesKey in dialog.messages) {
									if (dialog.messages.hasOwnProperty(mesKey)) {
										var message = dialog.messages[mesKey];
										if (message.user_id != currentUser.id) {
											self.newMessageAlert(dialog.companion);
										}
									}
								}
							}
						}
					}
				}
				
				setTimeout(function () {
					self.updateDialogs();
				}, 3000)
			}).fail(function(response) {
				self.updateDialogs();
			});
			
		};
		
		self.newMessageAlert = function (user) {
			var alertContainer = $('.new_message_wrap');
			alertContainer.empty();
			
			alertContainer.css('opacity', 0);
			
			var userAvatar = (user.avatar) ? userAvatar : '/images/no_photo.png';
			
			alertContainer.append(
				$('<div />', {
					class: 'alert_msg'
				}).append(
					$('<img />', {
						class: 'user_avatar',
						src: userAvatar
					})
				)
					.append(
						$('<div />', {
							class: 'user_name',
							text: user.name
						})
					)
					.append(
						$('<span />', {
							text: 'Новое сообщение'
						})
					)
			).animate({opacity: 1}, 500);
			
			setTimeout(function () {
				alertContainer.animate({opacity: 0}, 500);
				setTimeout(function () {
					alertContainer.empty();
				}, 500)
			}, 4000);
			
			return true;
		};
		
		self.updateMessageAfterRead = function (messageId) {
			var dialogId = $('.dialog_with').data('dialog_id');
			
			if (dialogId) {
				var dialogs = self.dialogs;
				for (var key in dialogs) {
					if (dialogs.hasOwnProperty(key)) {
						var dialog = dialogs[key];
						if (dialog.id == dialogId) {
							
							var messages = dialog.messages;
							if (messages && messages.length > 0) {
								for (var messageKey in messages) {
									if (messages.hasOwnProperty(messageKey)) {
										var message = messages[messageKey];
										
										if (message.id == messageId) {
											self.dialogs[key].messages[messageKey].status = 1;
										} else {
											continue;
										}
									}
								}
							}
						} else {
							continue;
						}
					}
				}
			}
		};
		
		self.readMessage = function (messages) {
			if (messages && messages.length > 0) {
				var messageIds = [];
				for (var key in messages) {
					if (messages.hasOwnProperty(key)) {
						var message = messages[key];
						if (message.hasClass('unread')) {
							message.animate({backgroundColor: '#fff'}, 200);
							message.removeClass('unread');
							messageIds.push(message.data('mid'));
							
							var dialogId = message.data('did');
							var dialogUnreadCounter = $('.dialog_user[data-dialog_id="' + dialogId + '"]').find('.count_unread_messages').text();
							
							self.updateUnreadMessages(dialogId, --dialogUnreadCounter);
						}
					}
				}
				
				if (messageIds && messageIds.length > 0) {
					$.ajax({
						url: '/dialog/read-message',
						data: {
							message_ids: messageIds,
							is_ajax: true
						},
						dataType : "json",
						type: 'post'
					}).done(function (response) {
						if (response.hasOwnProperty('success') && response.success) {
							for (var idKey in messageIds) {
								if (messageIds.hasOwnProperty(idKey)) {
									self.updateMessageAfterRead(messageIds[idKey]);
								}
							}
						}
					})
					.fail(function (response) {
						var message = $('<div />', {
								class: 'message message_error',
								text: response.message
							}
						);
						$('.message').append(message);
						
						setTimeout(function () {
							message.remove()
						}, 3000);
					});
				}
			}
			
			return false;
		};
		
		self.init();
	};
	
	$(document).ready(function () {
		new Dialog();
	});
</script>