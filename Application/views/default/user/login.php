<?php $this->breadcrumbs[] = 'Логин'; ?>
<section class="contaner-fluid registration login">
	<div class="registration__bg login__bg"></div>
		<div class="container">
          <?= $this->block( 'layouts/breadcrumbs' ); ?>
			<div class="registration__wrap login__wrap">
				<div class="registration__wrap login__wrap">
					<h1>Вход</h1>
					<?=$form?>
				</div>
			</div>
		</div>
</section>
<!---->
<!--<!doctype html>-->
<!--<html>-->
<!--<head>-->
<!--	<meta charset="utf-8">-->
<!--	<meta http-equiv="X-UA-Compatible" content="IE=edge">-->
<!--	<title>Default project</title>-->
<!--	<meta name="description" content="">-->
<!--	<link rel="icon" type="image/x-icon" href="favicon.ico">-->
<!--	-->
<!--	<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,900&amp;subset=cyrillic" rel="stylesheet">-->
<!--	<link href="https://fonts.googleapis.com/css?family=Marck+Script&amp;subset=cyrillic" rel="stylesheet">-->
<!--	-->
<!--	<meta name="viewport" content="width=device-width">-->
<!--	-->
<!--	<link rel="stylesheet" href="build/css/main.css">-->
<!--</head>-->
<!--<body>-->
<!--<header class="container-fluid header">-->
<!--	<div class="container">-->
<!--		<span class="col-xs-2 menu-button">-->
<!--			<button class="hamburger hamburger--collapse" id="menu-button" type="button">-->
<!--				<span class="hamburger-box">-->
<!--					<span class="hamburger-inner"></span>-->
<!--				</span>-->
<!--			</button>-->
<!--		</span>-->
<!--		<a href="/" class="col-md-2 col-sm-6 col-xs-5 logo">-->
<!--		-->
<!--		</a>-->
<!--		<nav class="col-lg-7 col-md-8 header__navigation">-->
<!--			<ul>-->
<!--				<li><a href="/for_agents.html">Агентам</a></li>-->
<!--				<li><a href="/for_providers.html">Поставщикам</a></li>-->
<!--				<li><a href="/catalog.html">Каталог услуг</a></li>-->
<!--				<li><a href="#!">Барахолка</a></li>-->
<!--				<li><a href="/about.html">О компании</a></li>-->
<!--				<li><a href="/faq.html">FAQ</a></li>-->
<!--			</ul>-->
<!--		</nav>-->
<!--		<div class="col-lg-3 col-md-2 col-sm-3 col-xs-5 header__menu__wrap">-->
<!--			<ul class="header__menu">-->
<!--				<li><a href="#!"><i class="icon icon_small icon-stats"></i></a></li>-->
<!--				<li><a href="#!"><i class="icon icon_small icon-like"></i></a></li>-->
<!--				<li>-->
<!--					<a href="#!">-->
<!--						<i class="icon icon-basket">-->
<!--							<span class="indicator">4</span>-->
<!--						</i>-->
<!--						<span class="accent-text ">Товара на: <span class="bold-text ruble">2 900</span></span>-->
<!--					</a>-->
<!--				</li>-->
<!--			</ul>-->
<!--		</div>-->
<!--	</div>-->
<!--</header>-->
<!--<nav class="container-fluid categories" id="categories">-->
<!--	<div class="header__mobile mobile-menu mobile-menu_current" id="mobile-menu">-->
<!--		<ul class="categories__list">-->
<!--			<h3>Меню</h3>-->
<!--			<li><i></i><a href="&categories-1">Категории товаров</a></li>-->
<!--			<li><i></i><a href="/catalog.html">Каталог услуг</a></li>-->
<!--			<li><i></i><a href="/advertisements.html">Барахолка</a></li>-->
<!--			<li><i></i><a href="/for_agents.html">Агентам</a></li>-->
<!--			<li><i></i><a href="/for_providers.html">Поставщикам</a></li>-->
<!--			<li><i></i><a href="/about.html">О компании</a></li>-->
<!--			<li><i></i><a href="/faq.html">FAQ</a></li>-->
<!--			<li><i></i><a href="/login.html">Вход</a></li>-->
<!--		</ul>-->
<!--	</div>-->
<!--	<div class="container mobile-menu" id="categories-1">-->
<!--		<ul class="categories__list">-->
<!--			<h3><a href="&mobile-menu"></a>Категории</h3>-->
<!--			<li>-->
<!--				<i></i>-->
<!--				<a href="#!">Электроника</a>-->
<!--				<ul>-->
<!--					<li data-link="/catalog.html" data-img="../svg/r_arrow_2.svg">Мобильные телефоны</li>-->
<!--					<li data-link="/catalog.html" data-img="../svg/r_arrow_2.svg">Умные часы и браслеты</li>-->
<!--					<li data-link="/catalog.html" data-img="../svg/r_arrow_2.svg">Телевизоры</li>-->
<!--					<li data-link="/catalog.html" data-img="../svg/r_arrow_2.svg">Фото- и видеокамеры</li>-->
<!--					<li data-link="/catalog.html" data-img="../svg/r_arrow_2.svg">Аудио- и видеотехника</li>-->
<!--					<li data-link="/catalog.html" data-img="../svg/r_arrow_2.svg">Наушники</li>-->
<!--					<li data-link="/catalog.html" data-img="../svg/r_arrow_2.svg">Игры</li>-->
<!--					<li data-link="/catalog.html" data-img="../svg/r_arrow_2.svg">GPS-навигаторы</li>-->
<!--				</ul>-->
<!--			</li>-->
<!--			<li>-->
<!--				<i></i>-->
<!--				<a href="#!">Компьютеры</a>-->
<!--				<ul>-->
<!--					<li data-link="/catalog.html" data-img="../svg/r_arrow_2.svg">Ноутбуки</li>-->
<!--					<li data-link="/catalog.html" data-img="../svg/r_arrow_2.svg">Планшеты</li>-->
<!--					<li data-link="/catalog.html" data-img="../svg/r_arrow_2.svg">Компьютеры</li>-->
<!--					<li data-link="/catalog.html" data-img="../svg/r_arrow_2.svg">Комплектующие</li>-->
<!--					<li data-link="/catalog.html" data-img="../svg/r_arrow_2.svg">Мониторы</li>-->
<!--					<li data-link="/catalog.html" data-img="../svg/r_arrow_2.svg">Принтеры</li>-->
<!--					<li data-link="/catalog.html" data-img="../svg/r_arrow_2.svg">Сетевое оборудование</li>-->
<!--					<li data-link="/catalog.html" data-img="../svg/r_arrow_2.svg">Аксесуары</li>-->
<!--				</ul>-->
<!--			</li>-->
<!--			<li>-->
<!--				<i></i>-->
<!--				<a href="#!">Бытовая техника</a>-->
<!--				<ul>-->
<!--					<li data-link="/catalog.html" data-img="../svg/r_arrow_2.svg">Крупная техника для кухни</li>-->
<!--					<li data-link="/catalog.html" data-img="../svg/r_arrow_2.svg">Приготовление напитков</li>-->
<!--					<li data-link="/catalog.html" data-img="../svg/r_arrow_2.svg">Приготовление блюд</li>-->
<!--					<li data-link="/catalog.html" data-img="../svg/r_arrow_2.svg">Техника для дома</li>-->
<!--					<li data-link="/catalog.html" data-img="../svg/r_arrow_2.svg">Климатическая техника</li>-->
<!--					<li data-link="/catalog.html" data-img="../svg/r_arrow_2.svg">Встраиваемая техника</li>-->
<!--					<li data-link="/catalog.html" data-img="../svg/r_arrow_2.svg">Техника для красоты</li>-->
<!--				</ul>-->
<!--			</li>-->
<!--			<li>-->
<!--				<i></i>-->
<!--				<a href="#!">Детские товары</a>-->
<!--				<ul>-->
<!--					<li data-link="/catalog.html" data-img="../svg/r_arrow_2.svg">Для мам и малышей</li>-->
<!--					<li data-link="/catalog.html" data-img="../svg/r_arrow_2.svg">Подгузники</li>-->
<!--					<li data-link="/catalog.html" data-img="../svg/r_arrow_2.svg">Детское питание</li>-->
<!--					<li data-link="/catalog.html" data-img="../svg/r_arrow_2.svg">Прогулки и путешествия</li>-->
<!--					<li data-link="/catalog.html" data-img="../svg/r_arrow_2.svg">Мебель</li>-->
<!--					<li data-link="/catalog.html" data-img="../svg/r_arrow_2.svg">Игрушки</li>-->
<!--					<li data-link="/catalog.html" data-img="../svg/r_arrow_2.svg">Хобби и творчество</li>-->
<!--					<li data-link="/catalog.html" data-img="../svg/r_arrow_2.svg">Развитие и обучение</li>-->
<!--					<li data-link="/catalog.html" data-img="../svg/r_arrow_2.svg">Детский спорт</li>-->
<!--				</ul>-->
<!--			</li>-->
<!--			<li>-->
<!--				<i></i>-->
<!--				<a href="#!">Зоотовары</a>-->
<!--				<ul>-->
<!--					<li data-link="/catalog.html" data-img="../svg/r_arrow_2.svg">Корма для кошек</li>-->
<!--					<li data-link="/catalog.html" data-img="../svg/r_arrow_2.svg">Корма для собак</li>-->
<!--					<li data-link="/catalog.html" data-img="../svg/r_arrow_2.svg">Товары для кошек</li>-->
<!--					<li data-link="/catalog.html" data-img="../svg/r_arrow_2.svg">Товары для собак</li>-->
<!--					<li data-link="/catalog.html" data-img="../svg/r_arrow_2.svg">Товары для птиц</li>-->
<!--					<li data-link="/catalog.html" data-img="../svg/r_arrow_2.svg">Товары для грызунов</li>-->
<!--					<li data-link="/catalog.html" data-img="../svg/r_arrow_2.svg">Товары для рыб</li>-->
<!--				</ul>-->
<!--			</li>-->
<!--			<li>-->
<!--				<i></i>-->
<!--				<a href="#!">Дом, дача, ремонт</a>-->
<!--				<ul>-->
<!--					<li data-link="/catalog.html" data-img="../svg/r_arrow_2.svg">Всё для дачи</li>-->
<!--					<li data-link="/catalog.html" data-img="../svg/r_arrow_2.svg">Садовая техника</li>-->
<!--					<li data-link="/catalog.html" data-img="../svg/r_arrow_2.svg">Отдых и пикник</li>-->
<!--					<li data-link="/catalog.html" data-img="../svg/r_arrow_2.svg">Всё для ремонта</li>-->
<!--					<li data-link="/catalog.html" data-img="../svg/r_arrow_2.svg">Мебель</li>-->
<!--					<li data-link="/catalog.html" data-img="../svg/r_arrow_2.svg">Посуда и кухня</li>-->
<!--					<li data-link="/catalog.html" data-img="../svg/r_arrow_2.svg">Хозтовары</li>-->
<!--					<li data-link="/catalog.html" data-img="../svg/r_arrow_2.svg">Бытовая химия</li>-->
<!--					<li data-link="/catalog.html" data-img="../svg/r_arrow_2.svg">Освещение</li>-->
<!--				</ul>-->
<!--			</li>-->
<!--			<li>-->
<!--				<i></i><a href="#!">Одежда и обувь</a></li>-->
<!--			<li>-->
<!--				<i></i><a href="#!">Красота и здоровье</a></li>-->
<!--			<li>-->
<!--				<i></i><a href="#!">Авто</a></li>-->
<!--			<li>-->
<!--				<i></i><a href="#!">Еще <img src="build/img/icon_more.png" alt=""></a></li>-->
<!--		</ul>-->
<!--	</div>-->
<!--	<div class="container-fluid categories__wrap mobile-menu" id="category-list">-->
<!--		<div class="container">-->
<!--			<h3><a href="&mobile-menu"></a></h3>-->
<!--			<ul class="categories__category" id="category-list-inner">-->
<!--			-->
<!--			</ul>-->
<!--		</div>-->
<!--	</div>-->
<!--</nav>-->
<!--<nav class="container-fluid search">-->
<!--	<form class="container search__input">-->
<!--		<input type="text" name="s_value" class="input_search" placeholder="Поиск товаров по сайту">-->
<!--		<span class="select"><select name="category">-->
<!--			<option value="-1">выберите раздел</option>-->
<!--			<option value="elect">электроника</option>-->
<!--		</select></span>-->
<!--		<button type="submit"><i class="icon-search"></i></button>-->
<!--	</form>-->
<!--</nav>-->
<!---->
<!--<section class="contaner-fluid registration login">-->
<!--	<div class="registration__bg login__bg"></div>-->
<!--	<div class="container">-->
<!--		<p class="page-path"><a href="#!">Главная</a> / Вход</p>-->
<!--		<div class="registration__wrap login__wrap">-->
<!--			<h1>Вход</h1>-->
<!--			<form class="registration__form login__form">-->
<!--				<label for="ooo">-->
<!--					<p>Логин</p>-->
<!--					<input required type="text" name="login" id="login" placeholder="Телефон или E-mail">-->
<!--				</label>-->
<!--				<label for="pass"><p>Пароль</p><input type="password" name="password" id="pass" minlength="6" placeholder="********"></label>-->
<!--				<button type="submit" class="center-block btn btn_red btn_long">Вход</button>-->
<!--				<p class="forgot"><a href="forgot_password.html">Забыли пароль?</a></p>-->
<!--			</form>-->
<!--		</div>-->
<!--	</div>-->
<!--</section>-->
<!---->
<!--<script></script>-->
<!---->
<!--<footer class="container-fluid footer">-->
<!--	<div class="container">-->
<!--		<div class="col-md-3 col-sm-5 about">-->
<!--			<img src="build/img/logo_lighten.png" alt="gipu.ru">-->
<!--			<p class="desc-text desc-text_small">Аренда дизельного полноприводного автомобиля на 5 суток обошлась нам в 27 тысяч рублей. Бронировали за 4 месяца до путешествия на местном сайте.</p>-->
<!--			<p class="desc-text desc-text_small mt">-->
<!--				© 2018  ООО «Gipuru»-->
<!--			</p>-->
<!--		</div>-->
<!--		<div class="col-md-7 col-sm-12 hide-xs">-->
<!--			<ul class="list">-->
<!--				<h3>Покупателям</h3>-->
<!--				<li><a href="#!">Как выбрать товар</a></li>-->
<!--				<li><a href="#!">Обратная связь</a></li>-->
<!--				<li><a href="#!">Помощь по сервису</a></li>-->
<!--				<li><a href="#!">Оставить отзыв</a></li>-->
<!--			</ul>-->
<!--			<ul class="list">-->
<!--				<h3>Агентам</h3>-->
<!--				<li><a href="/login.html">Войти в личный кабинет</a></li>-->
<!--				<li><a href="/registration_agent.html">Заказать магазин</a></li>-->
<!--				<li><a href="#!">Популярные вопросы</a></li>-->
<!--				<li><a href="#!">Заказать обратный звонок</a></li>-->
<!--			</ul>-->
<!--			<ul class="list">-->
<!--				<h3>Поставщикам</h3>-->
<!--				<li><a href="/login.html">Войти в личый кабинет</a></li>-->
<!--				<li><a href="/registration_provider.html">Подключиться к нам</a></li>-->
<!--				<li><a href="#!">Популярные вопросы</a></li>-->
<!--				<li><a href="#!">Заказать обратный звонок</a></li>-->
<!--			</ul>-->
<!--		</div>-->
<!--		<div class="col-sm-5 col-xs-6 d_n about_second">-->
<!--			<img src="build/img/logo_lighten.png" alt="gipu.ru">-->
<!--			<p class="desc-text desc-text_small">Аренда дизельного полноприводного автомобиля на 5 суток обошлась нам в 27 тысяч рублей. Бронировали за 4 месяца до путешествия на местном сайте.</p>-->
<!--			<p class="desc-text desc-text_small mt">-->
<!--				© 2018  ООО «Gipuru»-->
<!--			</p>-->
<!--		</div>-->
<!--		<div class="col-md-2 col-sm-6 col-xs-6 footer__contacts">-->
<!--			<p>Контакнтый центр</p>-->
<!--			<a href="tel:+78142332211">+7(814)-233-22-11</a>-->
<!--			<p>Служба поддержки</p>-->
<!--			<a href="tel:+78142332211">+7(814)-233-22-11</a>-->
<!--		</div>-->
<!--	</div>-->
<!--</footer>-->
<!---->
<!--<script-->
<!--		src="https://code.jquery.com/jquery-3.3.1.min.js"-->
<!--		integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="-->
<!--		crossorigin="anonymous"></script>-->
<!--<script src="https://unpkg.com/flickity@2/dist/flickity.pkgd.min.js"></script>-->
<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>-->
<!--<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery.lazy/1.7.9/jquery.lazy.min.js"></script>-->
<!--<script src="build/js/scripts.js"></script>-->
<!--<script src="build/js/main.js"></script>-->
<!--</body>-->
<!--</html>-->