<div class="dashboard_wrap">
	<?php $this->block('user/' . $user['type_code'] . '/header_title'); ?>
	<?php $this->block('user/' . $user['type_code'] . '/' . $user['type_code'] . '_header'); ?>
	
	<div class="title">
		<h1>
			Статистика заказов
		</h1>
	</div>
	<div class="seller_orders">
		<?php if ( count($orders) > 0 ): ?>
			<?php foreach ($orders as $order): ?>
				<div class="order_item">
					<div class="title">
						<span>Заказ</span>
						<span>№<?= $order['id']; ?></span>
					</div>
					<div>
						<span>Сумма заказа</span>
						<span><?php echo($order['total']); ?></span>
						<span class="currency">₽</span>
					</div>
					<div>
						<span>Дата заказа</span>
						<span><?= date("d.m.Y", $order['created_at']); ?></span>
					</div>
					<div>
						<span>Время заказа</span>
						<span><?= date("H:i", $order['created_at']); ?></span>
					</div>
					<div class="button_group">
						<a href="/user/order-statistic/<?= $order['id']; ?>">Подробнее</a>
					</div>
				</div>
			<?php endforeach; ?>
		<?php else: ?>
			<p>
				Заказов нет
			</p>
		<?php endif; ?>
	</div>
</div>