<div class="dashboard_wrap">
  <?php $this->block('user/' . $user['type_code'] . '/header_title'); ?>
  <?php $this->block('user/' . $user['type_code'] . '/' . $user['type_code'] . '_header'); ?>

  <div class="<?= $user['type_code']; ?>_balance">
    <div class="title">
      <h1>Баланс</h1>
    </div>
    <div class="amount_wrap">
      <span>Ваш баланс</span>
      <span><?= $user['balance']; ?></span>
    </div>
  </div>
 <?=$form?>
</div>
