<div class="dashboard_wrap">
	<?php $this->block('user/supplier/header_title'); ?>
	<?php $this->block('user/supplier/supplier_header'); ?>
	
	<div class="supplier_warehouses">
		<div class="button_group">
			<a href="/user/add-warehouse" class="button">Добавить склад</a>
		</div>
		<div class="title">
			<h1>Список складов</h1>
		</div>
		<?php if ( count($warehouses) > 0 ): ?>
			<?php foreach ($warehouses as $warehouse):?>
				<div class="warehouse_item">
					<a href="/user/edit-warehouse/<?= $warehouse['id']; ?>">Редактировать</a>
					<a href="/user/show-warehouse/<?= $warehouse['id']; ?>">Войти</a>
					<div>Имя склада<span><?= $warehouse['name']; ?></span></div>
					<div>Статус<span><?= \Application\Models\WarehouseStatus::getStatusById($warehouse['status_id']); ?></span></div>
					<div>Адрес<span><?= $warehouse['address']; ?></span></div>
				</div>
			<?php endforeach; ?>
		<?php else: ?>
			<p>
				У Вас нет складов
			</p>
		<?php endif; ?>
	</div>
</div>