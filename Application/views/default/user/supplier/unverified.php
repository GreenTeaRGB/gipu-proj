<div class="dashboard_wrap">
	<?php $this->block('user/supplier/header_title'); ?>
	
	<div class="supplier_info">
		<div class="subtitle">
			<h2>Статус : <?= \Application\Models\UserStatus::getStatusById($user['status_id']); ?></h2>
		</div>
		<?php if ( $user['status_id'] == \Application\Helpers\DataHelper::USER_REJECTED_STATUS
			|| $user['status_id'] == \Application\Helpers\DataHelper::DEFAULT_USER_STATUS
			|| $user['status_id'] == \Application\Helpers\DataHelper::USER_REQUIRES_EDITING ): ?>
			<div>
				<p class="note">
					Для получения полного доступа, заполните форму верификации
				</p>
			</div>
			<div>
				<div class="subtitle">
					<h3>Форма верификации</h3>
				</div>
				<form action="/user/verify-supplier" method="post" enctype="multipart/form-data">
					
					<div class="form_field juristic_documents">
						<div>
							<label for="juristic_documents">Фото документов на регистрацию юр.лица</label>
							<input type="file" name="juristic_documents[]" id="juristic_documents">
						</div>
						<h3>Данные юр.лица</h3>
						<?php if ( count($juristic_types) > 0 ): ?>
							<div>
								<label for="juristic_type_id">Тип юр.лица</label>
								<select name="juristic_type_id" id="juristic_type_id" required>
									<?php foreach ($juristic_types as $juristic_type): ?>
										<option value="<?= $juristic_type['id']?>">
											<?= $juristic_type['type']?>
										</option>
									<?php endforeach; ?>
								</select>
							</div>
						<?php endif; ?>
						<div>
							<label for="inn">ИНН</label>
							<input type="text" name="inn" id="inn" required>
						</div>
						<div>
							<label for="kpp">КПП</label>
							<input type="text" name="kpp" id="kpp" required>
						</div>
						<div>
							<label for="checking_account">Р/С</label>
							<input type="text" name="checking_account" id="checking_account" required>
						</div>
						<div>
							<label for="bik">БИК</label>
							<input type="text" name="bik" id="bik" required>
						</div>
						<div>
							<label for="ks">К/С</label>
							<input type="text" name="ks" id="ks" required>
						</div>
					</div>
					
					<div class="button_group">
						<input type="submit" value="Отправить">
					</div>
				</form>
			</div>
		<?php else: ?>
			<p>
				Ваши документы проверяются
			</p>
		<?php endif; ?>
	</div>
</div>