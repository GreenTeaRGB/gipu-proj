<div class="dashboard_wrap">
	<?php $this->block('user/supplier/header_title'); ?>
	<?php $this->block('user/supplier/supplier_header'); ?>
	
	<div class="supplier_promotions">
		<div class="button_group">
			<a href="/user/add-promotion" class="button">Добавить акцию</a>
		</div>
		
		<div class="title">
			<h1>Акции</h1>
		</div>
		<?php if ( count($promotions) > 0 ): ?>
			<div class="promotion_list">
				<?php foreach ($promotions as $promotion): ?>
					<div class="promotion_item">
						<div>
							<a href="/user/edit-promotion/<?= $promotion['id']; ?>">Редактировать</a>
							<a href="/user/remove-promotion/<?= $promotion['id']; ?>">Удалить</a>
						</div>
						<div>
							<?= $promotion['name']; ?>
						</div>
					</div>
				<?php endforeach; ?>
			</div>
		<?php else: ?>
			<p>
				У Вас нет акций
			</p>
		<?php endif; ?>
	</div>
</div>