<div class="dashboard_wrap">
	<?php $this->block('user/supplier/header_title'); ?>
	<?php $this->block('user/supplier/supplier_header'); ?>
	
	<div class="title">
		<h1>Склад - <?= $warehouse['name']; ?></h1>
	</div>
	<div class="warehouse">
		<div>Статус<span><?= \Application\Models\WarehouseStatus::getStatusById($warehouse['status_id']); ?></span></div>
		<div>Адрес<span><?= $warehouse['address']; ?></span></div>
	</div>
	
	<div class="button_group">
		<a href="/user/warehouse-list">Назад</a>
		<a href="/product/create">Создать продукт</a>
	</div>
	
	<div class="title">
		<h3>Список продуктов данного склада</h3>
	</div>
	<?php if ( count($products) > 0 ): ?>
	<div class="products_list">
		<?php foreach ($products as $product): ?>
			<div class="product_item">
				<a href="/product/edit/<?= $product['id']; ?>">Редактировать</a>
				<a href="/product/delete/<?= $product['id']; ?>">Удалить</a>
				<div>
					<span>Название<span><?= $product['name']; ?></span></span>
				</div>
				<div>
					<span>Статус<span><?= \Application\Models\ProductStatus::getProductStatusTypeById($product['status_id']); ?></span></span>
				</div>
				<div>
					<span>Дата создания<span> <?= $product['created_at'] ?></span></span>
				</div>
				<div>
					<span>Дата редактирования<span> <?= $product['updated_at'] ?></span></span>
				</div>
			</div>
		<?php endforeach; ?>
	</div>
	<?php else: ?>
		<div>
			Нет сохраненных продуктов
		</div>
	<?php endif; ?>
</div>