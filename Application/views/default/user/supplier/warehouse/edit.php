<div class="dashboard_wrap">
	<?php $this->block('user/supplier/header_title'); ?>
	<?php $this->block('user/supplier/supplier_header'); ?>
	
	<div class="warehouse_form">
		<form action="/user/save-warehouse<?php echo ((isset($warehouse['id'])) ? '/' . $warehouse['id'] : ''); ?>" method="post">
			<div class="form_field">
				<label for="name">Название склада</label>
				<input type="text" id="name"
					   name="name"
					   value="<?php echo ((isset($warehouse['name'])) ? $warehouse['name'] : ''); ?>"
					   required>
			</div>
			<?php if ( count($warehouse_statuses) > 0 ): ?>
			
			<div class="form_field">
				<label for="status_id">Статус склада</label>
				<select name="status_id" id="status_id">
					<?php foreach ($warehouse_statuses as $status): ?>
						<option value="<?= $status['id']; ?>"
							<?php echo((isset($warehouse['status_id']) && $status['id'] == $warehouse['status_id']) ? 'selected': ''); ?>>
							<?= $status['type']; ?>
						</option>
					<?php endforeach; ?>
				</select>
			</div>
			<?php endif; ?>
			<div class="form_field">
				<label for="address">Физический адрес склада</label>
				<textarea name="address" id="address" required><?php echo ((isset($warehouse['address'])) ? $warehouse['address'] : ''); ?></textarea>
			</div>
			
			<div class="button_group">
				<a href="/user/warehouse-list">Назад</a>
				<input type="submit" class="button" value="Сохранить">
			</div>
		</form>
	</div>
</div>