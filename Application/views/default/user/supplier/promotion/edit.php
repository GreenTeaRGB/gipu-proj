<div class="dashboard_wrap">
	<?php $this->block('user/supplier/header_title'); ?>
	<?php $this->block('user/supplier/supplier_header'); ?>
	
	<div class="promotion_form">
		<form action="/user/save-promotion<?php echo ((isset($promotion['id'])) ? '/' . $promotion['id'] : ''); ?>" method="post">
			<div class="form_field">
				<label for="name">Название акции</label>
				<input type="text" id="name"
				       name="name"
				       value="<?php echo ((isset($promotion['name'])) ? $promotion['name'] : ''); ?>"
				       required>
			</div>
			<div class="form_field">
				<label for="alias">Ссылка акции</label>
				<input type="text" id="alias"
					   name="alias"
					   value="<?php echo ((isset($promotion['alias'])) ? $promotion['alias'] : ''); ?>"
					   required>
				<p class="note">
					только латинские буквы, цифры, _-
				</p>
			</div>
			<div class="form_field">
				<label for="description">Описание акции</label>
				<textarea name="description" id="description" required><?php echo ((isset($promotion['description'])) ? $promotion['description'] : ''); ?></textarea>
			</div>
			<div class="form_field">
				<label for="percent">Процент скидки (если есть)</label>
				<input type="text" id="percent"
				       name="percent"
				       value="<?php echo ((isset($promotion['percent'])) ? $promotion['percent'] : ''); ?>">
			</div>
			
			<div class="form_field">
				<label for="start">Начало акции</label>
				<input type="text" id="start"
				       name="start"
				       value="<?php echo ((isset($promotion['start'])) ? $promotion['start'] : ''); ?>">
				<!--<input type="hidden" name="start" required>-->
			</div>
			<div class="form_field">
				<label for="finish">Конец акции</label>
				<input type="text" id="finish"
				       name="finish"
				       value="<?php echo ((isset($promotion['finish'])) ? $promotion['finish'] : ''); ?>">
				<!--<input type="hidden" name="finish" required>-->
			</div>
			
			<div class="form_field">
				<label for="is_active">Активна</label>
				<input type="hidden" name="is_active" value="0">
				<input type="checkbox" id="is_active"
				       name="is_active"
				       value="1"
						<?php echo ((isset($promotion['is_active']) && $promotion['is_active']) ? 'checked' : ''); ?>
				>
			</div>
			
			<?php if ( $related_products && count($related_products) > 0 ): ?>
				<div class="form_field">
					<label for="related_products">Связанные продукты</label>
					<select name="related_products[]" id="related_products" multiple size="10">
						<?php foreach ($related_products as $product): ?>
							<?php if ( isset($promotion['related_products']) && count($promotion['related_products']) > 0 ): ?>
								<?php foreach ($promotion['related_products'] as $selected_product): ?>
									<option value="<?= $product['id']; ?>" <?php echo (($selected_product['id'] == $product['id']) ? 'selected' : ''); ?>><?= $product['name']; ?></option>
								<?php endforeach; ?>
							<?else: ?>
								<option value="<?= $product['id']; ?>"><?= $product['name']; ?></option>
							<?php endif; ?>
						<?php endforeach; ?>
					</select>
				</div>
			<?php endif; ?>
			
			<div class="button_group">
				<a href="/user/promotions">Назад</a>
				<input type="submit" class="button" value="Сохранить">
			</div>
		</form>
	</div>
</div>
<script>
	$(function() {
		$('#start, #finish').datepicker({
			dateFormat : "dd.mm.yy",
			monthNames :
				['Январь','Февраль','Март','Апрель','Май','Июнь','Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'],
			dayNamesMin : ['Вс','Пн','Вт','Ср','Чт','Пт','Сб'],
			changeMonth: true,
			changeYear: true,
			yearRange:  '2018:2040'
		});
	});
</script>