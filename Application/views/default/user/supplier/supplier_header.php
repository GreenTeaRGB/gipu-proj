<div class="supplier_menu">
	<ul>
		<li>
			<a href="/user/dashboard">Регистрационные данные</a>
		</li>
		<li>
			<a href="/user/balance">Баланс</a>
		</li>
		<li>
			<a href="/user/warehouse-list">Склады</a>
		</li>
		<li>
			<a href="/user/orders-statistic">Статистика заказов</a>
		</li>
		<li>
			<a href="/user/promotions">Акции</a>
		</li>
		<li>
			<a href="/user/chat">Личные сообщения</a>
		</li>
		<!--
		<li>
			<a href="/user/referral-list">Список партнеров(рефералов)</a>
		</li>-->

	</ul>
</div>
<!--<div id="header"></div>-->
<!--<template id="l-menu">-->
<!--    <nav class="col-lg-2 col-md-2 l-menu">-->
<!--        <p class="page-path"><a href="/cabinet_provider.html">Личный кабинет</a> / {{pageName}}</p>-->
<!--        <h1>{{pageName}}</h1>-->
<!--        <ul class="l-menu__items">-->
<!--            <li v-for="(el, index) in lMenuItems">-->
<!--                <router-link :to="el.link" active-class="l-menu__items_current">{{el.name}}</router-link>-->
<!--                <ul v-if="el.sub">-->
<!--                    <li v-for="subel in el.sub">-->
<!--                        <router-link :to="subel.link" exact-active-class="l-menu__items_current">{{subel.name}}</router-link>-->
<!--                    </li>-->
<!--                </ul>-->
<!--            </li>-->
<!--        </ul>-->
<!--    </nav>-->
<!--</template> -->