<link rel="stylesheet" href="/media/css/default/cabinets.css">

	<?php $this->block('user/supplier/header_title'); ?>
	<?php $this->block('user/supplier/supplier_header'); ?>
<div id="header"></div>
<template id="l-menu">
    <nav class="col-lg-2 col-md-2 l-menu">
        <p class="page-path"><a href="/cabinet_provider.html">Личный кабинет</a> / {{pageName}}</p>
        <h1>{{pageName}}</h1>
        <ul class="l-menu__items">
            <li v-for="(el, index) in lMenuItems">
                <router-link :to="el.link" active-class="l-menu__items_current">{{el.name}}</router-link>
                <ul v-if="el.sub">
                    <li v-for="subel in el.sub">
                        <router-link :to="subel.link" exact-active-class="l-menu__items_current">{{subel.name}}</router-link>
                    </li>
                </ul>
            </li>
            <li>
                <a href="#!" @click="exit">Выйти</a>
            </li>
        </ul>
    </nav>
</template>

<!-- ALERTS -->
<template id="alerts">
    <div :class="['cabinet__alerts', { cabinet__alerts_isOpen: !hide }]" v-if="alerts[0]">
        <span class="hide" @click="hide = !hide">{{getText()}}<i class="icon-right-side"></i></span>
        <div class="list">
            <alert v-for="(alert, index) in alerts" :data="alert" :index="index"></alert>
        </div>
    </div>
</template>

<!-- ALERT -->
<template id="alert">
    <router-link v-if="show" @click="hide" :to="data.link" :class="['alert', getClass(data.type)]">
        <h3>{{data.header}}</h3>
        <p>{{data.message}}</p>
        <span class="icon-cancel alert__close" @click.prevent="hide"></span>
    </router-link>
</template>

<!-- DEFAULT-BLOCK -->
<template id="infoBlock">
    <div class="cabinet__container__block">
        <h3>{{blockName}}</h3>
        <data-list :data="data"></data-list>
        <router-link :to="link" class="btn_open-page" v-if="link">Редактировать<i class="icon-arrow-right"></i></router-link>
    </div>
</template>

<!-- TABLE-BLOCK-LONG -->
<template id="tableBlockLong">
    <div class="cabinet__container__block_long">
        <h3>{{blockName}}</h3>
        <form class="search__input">
            <input type="text" name="s_value" class="input_search" placeholder="Поиск">
            <button type="submit"><i class="icon-search"></i></button>
        </form>
        <div class="table">
            <table data-slideout-ignore>
                <thead>
                <tr>
                    <th v-for="header in data.headers">{{header}}</th>
                </tr>
                </thead>
                <tbody>
                <tr v-for="row in data.rows">
                    <td v-for="val in row">{{val}}</td>
                </tr>
                </tbody>
            </table>
        </div>
        <div class="load-more center-block" v-if="!hideButtons">
            <router-link :to="{name: 'История продаж'}" class="btn btn_white">Загрузить ещё</router-link>
        </div>
    </div>
</template>

<!-- COLUMN-CHART-BLOCK -->
<template id="columnChartBlock">
    <div class="cabinet__container__block">
        <h3>{{blockName}}</h3>
        <canvas :id="chartName" class="chart" data-slideout-ignore></canvas>
    </div>
</template>

<!-- LINE-CHART-BLOCK -->
<template id="lineChartBlock">
    <div>
        <canvas :id="chartName" class="chart" height="300" data-slideout-ignore></canvas>
    </div>
</template>

<!-- DATA-LIST -->
<template id="dataList">
    <ul class="list_data">
        <li v-for="(val, key) in data">
            <span>{{key}}:</span>
            <p>{{val}}</p>
        </li>
    </ul>
</template>

<!-- MODAL-WINDOW -->
<template id="modalWindow">
    <transition name="modal">
        <div class="modal__wrap" @click.self="$router.go(-1)">
            <div class="modal">
                <span class="icon-cancel cancel" @click="$router.go(-1)"></span>
                <component v-bind:is="template" :data="data"></component>
            </div>
        </div>
    </transition>
</template>

<!-- MODAL-ИСТОРИЯ-ПРОДАЖ -->
<template id="modalTable">
    <div>
        <table-block-long block-name="История продаж" :data="data" :hide-buttons="true"></table-block-long>
        <pagination :data="data.pagination"></pagination>
    </div>
</template>

<!-- MODAL-ЛИЧНЫЕ-ДАННЫЕ -->
<template id="modalPersonal">
    <div class="modal_personal">
        <form action="" ref="personalForm">
            <h3>Личные данные</h3>
            <ul class="list_data">
                <li v-for="el in data">
                    <span>{{el.text}}</span>
                    <input v-if="el.editable" :id="el.id" :name="el.name" type="text" :value="el.val" :prev="el.val" :name="el.name">
                    <p v-else>{{el.val}}</p>
                </li>
            </ul>
            <i>*в целях безопасности вся юридическая информация меняется только по запросу в службу поддержки</i>
            <button @click.prevent="saveChanges" type="submit" class="btn btn_red">Сохранить изменения</button>
        </form>
    </div>
</template>

<!-- PAGINATION -->
<template id="pagination">
    <ul class="center-block catalog__pages pagination">
        <li v-for="(elem, index) in pages" @click="setPage(index + 1)" :class="{'catalog__pages_current': index + 1 == currentPage}">{{index+1}}</li>
        <li v-if="lastPage > pages" @click="setPage(lastPage)" class="last-page" :class="{'catalog__pages_current': lastPage == currentPage}">{{lastPage}}</li>
    </ul>
</template>

<!-- 404-page -->
<template id="pageNotFound">
    <div class="cabinet__container__block_long cabinet_404">
        <h1>Ошибка 404.<br>Нет такой страницы :(</h1>
    </div>
</template>
<template id="page-main">
    <div class="cabinet__page">
        <info-block block-name="Личные данные" :link="{name: 'Личные данные'}" :data="personalData"></info-block>
        <column-chart-block block-name="Популярные товары" chart-name="chart-popular-products" link="/some" :chart-data="statisticsData"></column-chart-block>
        <table-block-long block-name="История продаж" :data="tableData" :hide-buttons="false"></table-block-long>
        <router-view></router-view>
    </div>
</template>
<template id="page-warehouses">
    <div class="cabinet__page">
        <div class="cabinet__container__block_long">
            <div class="warehouses__stock" v-for="(el, index) in data" :class="{'warehouses__stock_locked': el.status != 1}">
                <router-link class="link" :to="'/warehouses/stock/' + el.id">
                    <h3>{{el.label}}</h3>
                    <i>{{el.addr}}</i>
                </router-link>
                <div class="settings__menu">
                    <button @click="openSettings" class="icon-settings settings"></button>
                    <input :value="el.label" maxlength="18" type="text" class="label">
                    <input :value="el.addr" maxlength="30" type="text" class="addr">
                    <div class="buttons">
                        <button @click="deleteStock(el.id, el.label, index)" class="icon-cancel"></button>
                        <button @click="saveStockSettigns(el, $event)" class="icon-check"></button>
                    </div>
                </div>
            </div>
            <a class="warehouses__stock warehouses__stock_add" :class="{'warehouses__stock_add_open': adding }">
                <span @click="adding = true">Добавить склад</span>
                <div class="add">
                    <input type="text" placeholder="Название склада" maxlength="18" v-model="warehouseName">
                    <input type="text" placeholder="Адрес склада" maxlength="30" v-model="warehouseAddr">
                    <button @click="createWarehous"><i class="icon-check"></i></button>
                </div>
            </a>
        </div>
    </div>
</template>
<template id="page-stock">
    <div class="stock" data-slideout-ignore>
        <div class="stock__filters">
            <form>
                <!-- здесь выводятся только те категории, товары в которых есть у поставщика -->
                <span>Категория: </span>
                <i class="select"><select name="category" v-model="currentCategory">
                        <option value="all">Все категории</option>
                        <option v-for="cat in categories" :value="cat.main">{{cat.main}}</option>
                    </select></i>
                <span>Подкатегория: </span>
                <i class="select"><select name="category" v-model="currentSubCategory">
                        <option value="all">Не выбрано</option>
                        <option v-for="cat in subCategories" :value="cat">{{cat}}</option>
                    </select></i>
                <input type="checkbox" id="showDeleted"><label for="showDeleted">Показывать удаленный товар</label>
                <!-- <router-link class="add-product" to="product/add/">Добавить товар...</router-link> -->
            </form>
            <div class="stock__headers">
                <div class="col-md-5 col-xs-6">
                    <b>Сортировать по: </b>
                    <span v-for="el in headers.left" @click="sortBy(el.val)" :class="[{ 'current': el.val == sortedBy.by }, { 'current_reverse': sortedBy.reverse }]">{{el.text}}</span>
                </div>
                <div class="col-md-4 col-xs-3">
                    <span v-for="el in headers.middle" @click="sortBy(el.val)" :class="[{ 'current': el.val == sortedBy.by }, { 'current_reverse': sortedBy.reverse }]">{{el.text}}</span>
                </div>
                <div class="col-xs-3">
                    <span v-for="el in headers.right" @click="sortBy(el.val)" :class="[{ 'current': el.val == sortedBy.by }, { 'current_reverse': sortedBy.reverse }]">{{el.text}}</span>
                </div>
            </div>
        </div>
        <div class="stock__products">
            <ul data-slideout-ignore class="fixed-scroll">
                <li class="product" v-for="(product, index) in tableData.rows">
                    <router-link :to="'/warehouses/product/' + product.id">
                        <div class="col-md-1 col-sm-1 col-xs-1 product__img">
                            <img :src="product.img" alt="фото">
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-4">
                            <b>id{{product.id}}</b>
                            <h4>{{product.name}}</h4>
                            <i>{{product.category}}</i>
                            <b>арт.{{product.article}}</b>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-4">
                            <div class="product__center">
                                <p>Размер / остаток на складе:</p>
                                <u v-for="el in product.sizes">
                                    {{el.size}}/{{el.count}}
                                </u>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-3">
                            <div class="product__center">
                                <p>Статус товара: {{product.status}}</p>
                                <p>Стоимость: {{product.price}}</p>
                            </div>
                        </div>
                    </router-link>
                    <span class="product__button icon-cancel" @click="deleteItem($event, product.id, product.index, product.name)"></span>
                </li>
            </ul>
        </div>
    </div>
</template>
<template id="page-product">
    <div class="container-fluid merchandise">
        <h1>{{product.name}} <span class="merchandise__delete" @click="deleteProduct">Удалить товар</span> <div><i>арт.{{product.article}}</i> <i>id{{product.id}}</i></div></h1>
        <div class="row">
            <div class="col-md-6 col-sm-7 col-xs-12 page-product__content photos">
                <ul class="photos__list slider-photos-list">
                    <li v-for="photo in product.photos.preview"><img alt="img" class="lazy" :src="photo"></li>
                </ul>
                <div class="slider-photos-photo photos__photo">
                    <ul>
                        <li v-for="photo in product.photos.big"><img alt="img" class="lazy-onload" :src="photo"></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-6 col-sm-5 col-xs-12 merchandise__settings">
                <div class="col-md-4 col-sm-5 col-xs-5 price" @click="editPrice">
                    <p>Цена:</p>
                    <span class="ruble">{{product.price - product.discount}}
						<span class="ruble discount" v-if="product.discount > 0">{{product.price}}</span>
					</span>
                    <i class="icon-edit edit"></i>
                </div>
                <div class="col-md-8 col-sm-7 col-xs-6 r-price">
                    <p>Самая низкая цена: <span class="ruble">{{product.prices.lower}}</span></p>
                    <p>Самая высокая цена: <span class="ruble">{{product.prices.highest}}</span></p>
                </div>
            </div>
            <div class="col-md-6 col-sm-5 col-xs-12 sizes">
				<span v-for="(size, index) in product.sizes" class="size" :data-index="index">
					<p :class="{size__count_warn: size.count == 0}">{{size.size}}</p>
					<input type="text" v-model="size.count" class="size__count only-number" @change="sizeChanged" maxlength="4">
					<i>шт.</i>
					<div class="size__btns">
						<button @click="editCount(size, false)">-</button>
						<button @click="editCount(size, true)">+</button>
					</div>
				</span>
                <span class="sizes__add" @click="subMenu = true" :class="{'sizes__add_sub': subMenu}">
					<i>+</i>
					<div>
						<input type="text" placeholder="XXL" v-model="addSizeD.name" maxlength="6">
						<input class="only-number" type="text" v-model="addSizeD.count" placeholder="Кол-во" maxlength="4">
						<button @click="addSize">Добавить</button>
					</div>
				</span>
                <button v-if="edited" @click="saveChanges" class="btn btn_red">Сохранить изменения</button>
            </div>
            <div class="col-md-6 col-sm-5 col-xs-12 discount">
                <p>Текущая скидка:</p>
                <input type="text" id="discount" :placeholder="product.discount +' руб.'" class="only-number">
                <button class="btn btn_red" @click="setDiscount">Указать скидку</button>
            </div>
            <div class="col-md-6 col-sm-5 col-xs-12 merchandise__buttons">
                <a href="../product_page.html" class="btn btn_white" target="_blank">Перейти на страницу товара</a>
                <button class="btn btn_white">Указать об ошибке в товаре</button>
            </div>
        </div>
        <div class="row page-product states">
            <h1>Статистика продаж</h1>
            <div class="chart">
                <line-chart-block chart-name="productChart" :data="product.chart"></line-chart-block>
            </div>
            <h1>Отзывы о вашем товаре</h1>
            <div class="container-fluid reviews">
                <div v-for="review in product.reviews" class="col-md-9 review">
                    <div class="review__photo">
                        <img :src="review.user.avatar" alt="photo" class="lazy">
                        <span class="mark">{{review.mark}}</span>
                    </div>
                    <div class="review__body">
                        <p class="name">{{review.user.name}}</p>
                        <i class="mark">{{review.short}} <i class="date">{{review.date}}</i></i>
                        <dl>
                            <dt>Достоинста</dt>
                            <dd>{{review.good}}</dd>
                            <dt>Недостатки:</dt>
                            <dd>{{review.bad}}</dd>
                            <dt>Комментарии:</dt>
                            <dd>{{review.comment}}</dd>
                        </dl>
                        <i class="experience">Опыт использования: {{review.experience}}</i>
                        <button class="btn_review btn_review_green btn_review_current"><i class="icon-check"></i>{{review.pluses}}</button>
                        <button class="btn_review btn_review_red btn_review_current" data-id="123132"><i class="icon-cancel"></i>{{review.minuses}}</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</template>
<template id="page-productAdd">
    <div class="cabinet__container__block_long page-product-add">
        <nav class="container-fluid search">
            <form class="search__input" v-on:submit.prevent="trySearch">
                <input type="text" v-model="search.value" @input="trySearch(true)" @change="trySearch" name="s_value" class="input_search" placeholder="Попробуйте поискать ваш товар по названию, артикулу, либо номеру штрихкода">
                <span class="select"><select name="category">
					<option value="-1">выберите раздел</option>
					<option value="elect">электроника</option>
				</select></span>
                <button type="submit"><i class="icon-search"></i></button>
            </form>
        </nav>
        <div class="no-result" v-if="noResult">
            <h2><i class="icon-smile"></i>Поиск не дал результатов</h2>
            <router-link class="btn btn_red center-block" :to="{ name: 'Добавление товара' }">Добавить товар</router-link>
        </div>
        <div class="search__result" v-if="data[0] != undefined">
            <h2>Выберите подходящий товар</h2>
            <ul class="stock__products">
                <li v-for="product in data" class="product">
                    <a target="_blank" href="/product_page.html">
                        <div class="col-md-1 col-sm-1 col-xs-1 product__img">
                            <img :src="product.img" alt="фото">
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-4">
                            <b>{{product.id}}</b>
                            <h4>{{product.name}}</h4>
                            <i>{{product.category}}</i>
                            <b>{{product.article}}</b>
                        </div>
                    </a>
                    <div class="warehouse">
                        <button @click.capture="openSelect" class="accept">
                            <i class="icon-check"></i>
                        </button>
                        <span class="select">
							<select v-model="currentWarehouse" name="CurrentWarehouse" required>
								<option value="" disabled selected>Выберите склад</option>
								<option v-for="war in warehouses" :value="war.id">{{war.name}}</option>
							</select>
						</span>
                        <button @click="addCurrent(product.id)" class="btn">Разместить на выбраном складе</button>
                    </div>
                </li>
            </ul>
            <h2>Или</h2>
            <router-link :to="{name: 'Добавление нового товара'}" class="btn btn_red">Добавьте недостающий товар на сайт</router-link>
        </div>
        <router-view></router-view>
    </div>
</template>

<template id="modalProductCreate">
    <transition name="modal">
        <div class="modal__wrap" @click.self="$router.go(-1)">
            <div class="modal modal_create-product">
                <span class="icon-cancel cancel" @click="$router.go(-1)"></span>
                <h1>Добавление товара</h1>
                <form action="">
                    <div class="col-md-12 category">
							<span class="select">
								<select name="warehouse" v-model="userData.currentWarehouse" required>
									<option value="" disabled selected>выберите склад размещения</option>
									<option v-for="war in warehouses" :value="war.id">{{war.name}}</option>
								</select>
							</span>
                        <span class="select">
								<select name="category" @change="getSubCategories" v-model="userData.currentCategory" required>
									<option value="" disabled selected>выберите категорию</option>
									<option v-for="cat in categories.categories" :value="cat.val">{{cat.name}}</option>
								</select>
							</span>
                        <span class="select" v-if="userData.currentCategory != ''">
								<select name="subcategory" @change="getCharacteristics" v-model="userData.currentSubCategory" required>
									<option value="" disabled selected>выберите категорию</option>
									<option v-for="cat in categories.subCategories" :value="cat.val">{{cat.name}}</option>
								</select>
							</span>
                    </div>
                    <div v-if="userData.currentSubCategory != ''" class="col-md-6 col-sm-7 col-xs-12 page-product__content photos">
                        <ul class="photos__list slider-photos-list">
                            <li v-for="photo in product.photos">
                                <img alt="img" :src="photo" class="lazy-onload">
                            </li>
                            <input name="photos" type="file" ref="productPhotos" :files="product.photos" @change="previewPhotos" id="photosInp" accept="image/*" multiple required>
                            <label for="photosInp">Загрузить фото</label>
                        </ul>
                        <div class="slider-photos-photo photos__photo">
                            <ul>
                                <li v-for="photo in product.photos">
                                    <img alt="img" :src="photo" class="lazy-onload">
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div v-if="userData.currentSubCategory != ''" class="col-md-6 description">
                        <p>Название товара:</p>
                        <input name="product-name" type="text" placeholder="Кросовки Nike Air 98" class="name" required>
                        <div class="row">
                            <div class="col-md-6">
                                <p>Артикул товара (при наличии):</p>
                                <input name="article" type="text" @changed="tryFindProduct" v-model="userData.article" placeholder="арт.891212">
                            </div>
                            <div class="col-md-6">
                                <p>Номер штрихкода:</p>
                                <input name="barcode" type="number" @changed="tryFindProduct" v-model="userData.barcode" placeholder="4 665301 555659" required>
                            </div>
                        </div>
                        <div>
                            <p>Оригинальный ли товар:</p>
                            <input type="radio" name="is_original" v-model="userData.isOriginal" value="true" id="original" required><label for="original">Оригинальный</label>
                            <input type="radio" name="is_original" v-model="userData.isOriginal" value="false" id="replika" required><label for="replika">Реплика (не оригинальный)</label>
                            <div v-if="userData.isOriginal == 'true'">
                                <label for="certificates"><p>Прикрепите сертификаты, подтверждающие подлинность товара</p></label>
                                <input id="certificates" name="certificates" type="file" accept="image/*" multiple required>
                            </div>
                        </div>
                        <div class="row characteristics">
                            <div class="col-md-6" v-for="char in product.characteristics">
                                <p>{{char}}:</p>
                                <input name="char" type="text">
                            </div>
                            <div class="col-md-6">
                                <p>Выберите цвет товара:</p>
                                <span class="select">
										<select name="color" required>
											<option value="" disabled selected>Выберите цвет товара</option>
											<option value="red">Красный</option>
											<option value="red">Черный</option>
											<option value="red">Белый</option>
										</select>
									</span>
                            </div>
                        </div>
                        <div class="row merchandise" v-if="userData.currentCategory == 'odejda-i-obuv'">
                            <p>Выберите размеры, которые у вас есть в наличии:</p>
                            <div class="col-md-6 col-sm-5 col-xs-12 sizes">
									<span v-for="(size, index) in product.sizes" class="size" :data-index="index">
										<p :class="{size__count_warn: size.count == 0}">{{size.size}}</p>
										<input type="text" v-model="size.count" class="size__count only-number" maxlength="4">
										<i>шт.</i>
										<div class="size__btns">
											<button @click.prevent="editCount(size, false)">-</button>
											<button @click.prevent="editCount(size, true)">+</button>
										</div>
									</span>
                                <span class="sizes__add" @click="subMenu = true" :class="{'sizes__add_sub': subMenu}">
										<i>+</i>
										<div>
											<input type="text" placeholder="XXL" v-model="addSizeD.name" maxlength="6">
											<input class="only-number" type="text" v-model="addSizeD.count" placeholder="Кол-во" maxlength="4">
											<button @click.prevent="addSize">Добавить</button>
										</div>
									</span>
                            </div>
                        </div>
                        <p>Комментарий:</p>
                        <textarea name="comment" id="" rows="5" placeholder="Все товары перед публикацией проходят проверку модератором. По желанию вы можете оставить комментарий к товару, который увидит модератор."></textarea>
                        <input class="btn btn_red" type="submit" value="Продолжить">
                    </div>
                </form>
            </div>
        </div>
    </transition>
</template>

<template id="modalStockSettings">
    <transition name="modal">
        <div class="modal__wrap" @click.self="$router.go(-1)">
            <div class="modal modal_small">
                <span class="icon-cancel cancel" @click="$router.go(-1)"></span>
                <p>Название склада:</p>
                <input type="text" value="data.name">
                <p>Адрес склада:</p>
                <input type="text" value="data.addr">

            </div>
        </div>
    </transition>
</template>

<main class="container-fluid cabinet__wrap">
    <div class="cabinet__bg"></div>
    <div class="container cabinet" id="app">
        <l-menu-item id="menu" :page-link="pageRoute" :l-menu-items="lMenuItems" :page-name="pageName"></l-menu-item>
        <section id="panel" class="col-lg-10 col-md-10 col-xs-12 container cabinet__container" :class="{ 'cabinet__container_transparent': transparentPage }">
            <div class="cabinet__wrap_in">
                <alerts :alerts="alerts" v-if="showAlerts"></alerts>
                <transition name="fade">
                    <div class="cabinet__loading" v-show="loading"></div>
                </transition>
                <transition name="modal">
                    <modal-window v-if="modalShow"></modal-window>
                </transition>
                <router-view></router-view>
            </div>
        </section>
    </div>
</main>
<!--	<div class="supplier_info">-->
<!--		<div>-->
<!--			<a href="/user/edit">Редактировать</a>-->
<!--		</div>-->
<!--		<div class="form_field">-->
<!--			<label for="user_name">Имя</label>-->
<!--			<span id="user_name">-->
<!--				--><?//= $user['name'] . ' ' . $user['last_name']; ?>
<!--			</span>-->
<!--		</div>-->
<!--		<div class="form_field">-->
<!--			<label for="phone">Телефон</label>-->
<!--			<span id="phone">--><?//= $user['phone']; ?><!--</span>-->
<!--		</div>-->
<!--		<div class="form_field">-->
<!--			<label for="email">Email</label>-->
<!--			<span id="email">--><?//= $user['email']; ?><!--</span>-->
<!--		</div>-->
<!--	</div>-->
<!--	--><?php //if ( count($juristic_data) > 0 ): ?>
<!--		<div class="additional_info">-->
<!--			<div class="title">-->
<!--				<h3>Дополнительная информация</h3>-->
<!--			</div>-->
<!--			<div>-->
<!--				<div>-->
<!--					<label for="organization_name">Наименование юр.лица</label>-->
<!--					<span id="organization_name">--><?//= $juristic_data['organization_name']; ?><!--</span>-->
<!--				</div>-->
<!--				<div>-->
<!--					<label for="juristic_address">Юр.адрес</label>-->
<!--					<span id="juristic_address">--><?//= $juristic_data['juristic_address']; ?><!--</span>-->
<!--				</div>-->
<!--				<div>-->
<!--					<label for="inn">ИНН</label>-->
<!--					<span id="inn">--><?//= $juristic_data['inn']; ?><!--</span>-->
<!--				</div>-->
<!--				<div>-->
<!--					<label for="kpp">КПП</label>-->
<!--					<span id="kpp">--><?//= $juristic_data['kpp']; ?><!--</span>-->
<!--				</div>-->
<!--				<div>-->
<!--					<label for="checking_account">Р/С</label>-->
<!--					<span id="checking_account">--><?//= $juristic_data['checking_account']; ?><!--</span>-->
<!--				</div>-->
<!--				<div>-->
<!--					<label for="bik">БИК</label>-->
<!--					<span id="bik">--><?//= $juristic_data['bik']; ?><!--</span>-->
<!--				</div>-->
<!--				<div>-->
<!--					<label for="ks">К/С</label>-->
<!--					<span id="ks">--><?//= $juristic_data['ks']; ?><!--</span>-->
<!--				</div>-->
<!--			</div>-->
<!--		</div>-->
<!--	--><?php //endif; ?>

<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.bundle.js"></script>
<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
<script src="https://unpkg.com/vue-router/dist/vue-router.js"></script>
<script src="/media/js/default/cabinets.js"></script>