<div class="dashboard_wrap">
	<?php $this->block('user/' . $user['type_code'] . '/header_title'); ?>
	<?php $this->block('user/' . $user['type_code'] . '/' . $user['type_code'] . '_header'); ?>
	<div class="order_preview_wrap">
		<?php if ( count($order) > 0 ): ?>
			<div class="order_item">
				<div class="order_title">
					<h1>Детали заказа</h1>
				</div>
				<?php $products = $order['order_additional']; ?>
				<?php if ( count($products) > 0 ): ?>
					<div class="order_products">
						<?php foreach ($products as $product):
                          $product['images'] = unserialize($product['images']);
                          ?>
							<div class="order_product">
								<?php if ( $product['images'] && count($product['images']) > 0 ):  ?>
									<?php foreach ($product['images'] as $image): ?>
										<div class="prod_image">
											<img src="<?= $image; ?>" alt="<?= $product['name']; ?>">
										</div>
									<?php endforeach; ?>
								<?php else: ?>
									<div class="prod_image">
										<img src="/images/no_photo.png" alt="<?= $product['name']; ?>">
									</div>
								<?php endif; ?>
								<div class="product_info">
									<div><?= $product['category']; ?></div>
									<div><?= $product['name']; ?></div>
									<div>
										<span><?= $product['product_count']; ?></span>
										<span>шт. х</span>
										<span><?= \Application\Helpers\AppHelper::calculatePriceWithCommission($product['price'], $product['commission']); ?></span>
										<span class="currency">₽</span>
									</div>
								</div>
							</div>
						<?php endforeach; ?>
						<div>
							<span>Сумма по товарам</span>
							<span><?= $order['total']; ?></span>
						</div>
					</div>
				<?php else: ?>
					<div class="no_items">
						Продуктов связанных с заказом не найдено
					</div>
				<?php endif; ?>
				
				<?php if ( isset($order['order_address']) && count($order['order_address']) > 0 ): ?>
					<?php $order_address = $order['order_address']; ?>
					<div class="order_address">
						<div class="title">
							<h3>
								Данные получателя
							</h3>
						</div>
						<div>
							<span><?= $order_address['delivery_last_name']; ?></span>
							<span><?= $order_address['delivery_name']; ?></span>
							<span><?= $order_address['patronymic']; ?></span>
						</div>
						<div>
							<span><?= $order_address['country']; ?></span>
							<span><?= $order_address['city']; ?></span>
							<?php if ( $order_address['region'] ): ?>
								<span><?= $order_address['region']; ?></span>
							<?php endif; ?>
						</div>
						<div>
							<span><?= $order_address['street']; ?></span>
							<span><?= $order_address['house_number']; ?></span>
							<?php if ( $order_address['apartment'] ): ?>
								<span><?= $order_address['apartment']; ?></span>
							<?php endif; ?>
						</div>
						<div>
							<span><?= $order_address['post_index']; ?></span>
						</div>
						<div>
							<span><?= $order_address['phone']; ?></span>
						</div>
					</div>
				<?php else: ?>
					<div class="no_address">
						Адреса связанного с ордером не найдено
					</div>
				<?php endif; ?>
			</div>
		<?php endif; ?>
	</div>
</div>