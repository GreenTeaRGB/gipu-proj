<div class="dashboard_wrap">
	<?php $this->block('user/' . $user['type_code'] . '/header_title'); ?>
	<?php $this->block('user/' . $user['type_code'] . '/' . $user['type_code'] . '_header'); ?>
	
	<div class="<?= $user['type_code']; ?>_balance">
		<div class="title">
			<h1>Баланс</h1>
		</div>
		<div class="amount_wrap">
			<span>Ваш баланс</span>
			<span><?= $user['balance']; ?></span>
		</div>
	</div>
    <?php if($user['balance'] > 0): ?>
    <div>
        <a href="/user/balance/output">Подать заявку на вывод стредств</a>
    </div>
    <?php endif; ?>
    <div>
        <h3>Зпросы на вывод</h3>
        <?php if(count($outputRequests) > 0): ?>
      <?php foreach( $outputRequests as $outputRequest ) : ?>
          <ul>
              <li><strong>Дата создания: </strong><?=$outputRequest['created_at']?></li>
              <li><strong>Дата обновления: </strong><?=$outputRequest['updated_at']?></li>
              <li><strong>Сумма: </strong><?=$outputRequest['sum']?></li>
              <li><strong>Статут: </strong><?=$outputRequest['status_name']?></li>
          </ul>
      <?php endforeach; ?>
        <?php else: ?>
            У вас не запросов на вывод
        <?php endif; ?>
    </div>
</div>
