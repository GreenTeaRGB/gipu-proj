<div class="dashboard_wrap">
	<?php $this->block('user/seller/header_title'); ?>
	<?php $this->block('user/seller/seller_header'); ?>
	
	<div class="seller_suppliers">
		<div class="title">
			<h1>Список привлеченных поставщиков</h1>
		</div>
		<?php if ( count($suppliers) > 0 ): ?>
			<?php foreach ($suppliers as $supplier): ?>
				<p><?= $supplier['name'] . $supplier['last_name']; ?></p>
			<?php endforeach; ?>
		<?php else: ?>
			<p>
				У Вас нет привлеченных поставщиков
			</p>
		<?php endif; ?>
	</div>
</div>