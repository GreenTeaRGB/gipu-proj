<div class="dashboard_wrap">
	<?php $this->block('user/seller/header_title'); ?>
	<?php $this->block('user/seller/seller_header'); ?>
	
	<div class="button_group">
		<a href="/user/add-card" class="button">Добавить расчетную карту</a>
	</div>
	<div class="card_list">
		<div class="title">
			<h1>Список расчетных карт</h1>
		</div>
		<?php if ( $cards && count($cards) > 0 ): ?>
			<?php foreach ($cards as $card): ?>
				<div class="card_item">
					<div>
						<a href="/user/edit-card/<?= $card['id']; ?>">Редактировать карту</a>
						<a href="/user/remove-card/<?= $card['id']; ?>">Удалить карту</a>
					</div>
					<div>
						<?= $card['description']; ?>
					</div>
				</div>
			<?php endforeach; ?>
		<?php else: ?>
			<p>
				У Вас нет сохраненных расчетных карт
			</p>
		<?php endif; ?>
	</div>
</div>