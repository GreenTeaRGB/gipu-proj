<div class="dashboard_wrap">
	<?php $this->block('user/seller/header_title'); ?>
	
	<?php $this->block('user/seller/seller_header'); ?>
	
	<div class="seller_sites">
		<div class="title">
			<h1>Сайты продавца</h1>
		</div>
		<div class="button_group">
			<a href="/user/create-site" class="button">Создать сайт</a>
		</div>
		<?php if ( count($sites) > 0 ): ?>
			<?php foreach ($sites as $site): ?>
				<a href="/user/seller/site/<?=$site['id']?>"><?= $site['name']; ?></a>
			<?php endforeach; ?>
		<?php else: ?>
			<p>
				У Вас нет сайтов
			</p>
		<?php endif; ?>
	</div>
</div>