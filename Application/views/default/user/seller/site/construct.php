<link rel="stylesheet" href="/media/css/default/constructor-font.css">
<?php echo serialize( [
  'header' => 1,
  'blocks' => [
    [ 'type' => 'slider', 'variation' => 1, 'options' => [] ]
  ],
  'footer' => 1
] ); ?>

<div class="container-fluid constructor" id="constructor-page">
  <div class="col-md-8 col-xs-12 constructor__window">
    <div class="mode">
      <span class="constructor-display current" title="Переключить окно в режим компьютера"></span>
      <span class="constructor-tablet" title="Переключить окно в режим планшета"></span>
      <span class="constructor-mobile" title="Переключить окно в режим телефона"></span>
    </div>
    <div class="constructor__wrap" id="wrap">
      <div class="modal-edit" id="modal-edit" ctype="">
        <div class="bg close-modal-edit" id="modal-bg"></div>
        <div class="window">
          <header><span class="constructor-cancel close-modal-edit"></span></header>
          <div class="edit_text">
            <div class="func">
              <button title="Сделать выделенный текст жирным"><i class="constructor-text-bold"></i>
              </button>
              <button title="Сделать выделенный текст подчеркнутым"><i
                  class="constructor-text-underline"></i></button>
              <button title="Сделать выделенный текст наклоненным"><i class="constructor-text-italic"></i>
              </button>
              <button title="Сделать выделенный текст ссылкой"><i class="constructor-text-link"></i>
              </button>
            </div>
            <textarea id="ta-edit-text" class="ta" placeholder="Введите текст блока здесь..."></textarea>
            <button class="btn btn_red">Сохранить</button>
          </div>
          <div class="edit_list">
            <ul class="list">
<!--              <li><input type="text" value="Осень 2018" placeholder="Имя ссылки"-->
<!--                         title="Текст ссылки, который пользователи видят на сайте">-->
<!--                <input type="text"-->
<!--                       value="/action-2018"-->
<!--                       placeholder="Ссылка"-->
<!--                       title='Укажите адрес, на который пользователь переходит при клике по ссылке. Внутренние ссылки на ваш сайт следует указывать в формате: "/ссылка-на-страницу"'>-->
<!--              </li>-->
<!--              <li><input type="text" value="Одежда" placeholder="Имя ссылки"-->
<!--                         title="Текст ссылки, который пользователи видят на сайте">-->
<!--                <input type="text"-->
<!--                       value="/cloth"-->
<!--                       placeholder="Ссылка"-->
<!--                       title='Укажите адрес, на который пользователь переходит при клике по ссылке. Внутренние ссылки на ваш сайт следует указывать в формате: "/ссылка-на-страницу"'>-->
<!--              </li>-->
<!--              <li><input type="text" value="Обувь" placeholder="Имя ссылки"-->
<!--                         title="Текст ссылки, который пользователи видят на сайте">-->
<!--                <input type="text"-->
<!--                       value="/boots"-->
<!--                       placeholder="Ссылка"-->
<!--                       title='Укажите адрес, на который пользователь переходит при клике по ссылке. Внутренние ссылки на ваш сайт следует указывать в формате: "/ссылка-на-страницу"'>-->
<!--              </li>-->
<!--              <li><input type="text" value="Аксессуары" placeholder="Имя ссылки"-->
<!--                         title="Текст ссылки, который пользователи видят на сайте">-->
<!--                <input type="text"-->
<!--                       value="/acessories"-->
<!--                       placeholder="Ссылка"-->
<!--                       title='Укажите адрес, на который пользователь переходит при клике по ссылке. Внутренние ссылки на ваш сайт следует указывать в формате: "/ссылка-на-страницу"'>-->
<!--              </li>-->
<!--              <li><input type="text" value="Бренды" placeholder="Имя ссылки"-->
<!--                         title="Текст ссылки, который пользователи видят на сайте">-->
<!--                <input type="text"-->
<!--                       value="/brands"-->
<!--                       placeholder="Ссылка"-->
<!--                       title='Укажите адрес, на который пользователь переходит при клике по ссылке. Внутренние ссылки на ваш сайт следует указывать в формате: "/ссылка-на-страницу"'>-->
<!--              </li>-->
              <!-- здесь отображается максимальное кол-во полей. Если пользователь оставляет поле пустым - ссылка на конечном сайте не рендерится -->
              <button class="btn btn_red">Сохранить</button>
            </ul>
          </div>
          <div class="edit_slider">
            <ul class="list">
              <li class="slide">
                <div class="image">
                  <img src="https://a.lmcdn.ru/bs2/1/81/hp_w_14260_eg.jpg" alt="">
                  <button title="Выбрать изображения из списка существующих"><i
                      class="constructor-edit-images"></i></button>
                  <button title="Загрузить своё изображение"><i class="constructor-upload"></i>
                  </button>
                </div>
                <div class="ctrl">
                  <input type="text" value="Купить со скидкой" placeholder="Текст кнопки"
                         title="Текст кнопки на изображении слайдера">
                  <span class="select" title="Расположение кнопки на изображении слайдера"><select>
										<option value="left-top">Лево верх</option>
										<option value="left">Лево центр</option>
										<option value="left-bottom">Лево низ</option>
										<option value="center-top">Середина верх</option>
										<option value="center">Середина центр</option>
										<option value="center-bottom">Середина низ</option>
										<option value="right-top">Право верх</option>
										<option value="right">Право центр</option>
										<option value="right-bottom">Право низ</option>
									</select></span>
                </div>
              </li>
              <li class="slide">
                <div class="image">
                  <img src="https://a.lmcdn.ru/bs2/2/52/_1__.jpg" alt="">
                  <button title="Выбрать изображения из списка существующих"><i
                      class="constructor-edit-images"></i></button>
                  <button title="Загрузить своё изображение"><i class="constructor-upload"></i>
                  </button>
                </div>
                <div class="ctrl">
                  <input type="text" value="Подробнее" placeholder="Текст кнопки"
                         title="Текст кнопки на изображении слайдера">
                  <span class="select" title="Расположение кнопки на изображении слайдера"><select>
										<option value="left-top">Лево верх</option>
										<option value="left">Лево центр</option>
										<option value="left-bottom">Лево низ</option>
										<option value="center-top">Середина верх</option>
										<option value="center">Середина центр</option>
										<option value="center-bottom">Середина низ</option>
										<option value="right-top">Право верх</option>
										<option value="right">Право центр</option>
										<option value="right-bottom">Право низ</option>
									</select></span>
                </div>
              </li>
            </ul>
          </div>
        </div>
      </div>
      <div class="blocks" id="blocks">
        <?php foreach ( $config['config']['blocks'] as $conf ) {
          $dir =  FODLER.'template_sites/blocks/';
          switch ($conf['type']){
            case 'slider': $dir .='slider/'; break;
            case 'nav': $dir .= 'nav/'; break;
            default:
              continue;
          }
//          if(!file_exists($dir))
        } ?>
      </div>
    </div>
    <div class="col-md-4 col-xs-12 constructor__menu">
      <div class="main" id="cons-main">
        <div class="navigation">
          <h3>Выберите блок для добавления</h3>
        </div>
        <div class="templates" id="templates">
          <div class="lazy template" data-src="/build/constructor-blocks/build/img/blocks/header.png">
            <img src="/build/constructor-blocks/build/img/blocks/header.png" alt="шапка">
            <span class="bg">Шапка сайта</span>
          </div>
          <div class="lazy-onload template"
               data-src="/build/constructor-blocks/build/img/blocks/navigation.png">
            <img src="/build/constructor-blocks/build/img/blocks/navigation.png" alt="навигация">
            <span class="bg">Навигационное меню</span>
          </div>
          <div class="lazy-onload template"
               data-src="/build/constructor-blocks/build/img/blocks/corousel.png">
            <img src="/build/constructor-blocks/build/img/blocks/corousel.png" alt="карусель">
            <span class="bg">Карусели</span>
          </div>
          <div class="lazy-onload template" data-src="/build/constructor-blocks/build/img/blocks/galery.png">
            <img src="/build/constructor-blocks/build/img/blocks/galery.png" alt="карусель">
            <span class="bg">Галереи</span>
          </div>
          <div class="lazy-onload template"
               data-src="/build/constructor-blocks/build/img/blocks/products.png">
            <img src="/build/constructor-blocks/build/img/blocks/products.png" alt="карусель">
            <span class="bg">Витрины</span>
          </div>
          <div class="lazy-onload template" data-src="/build/constructor-blocks/build/img/blocks/text.png">
            <img src="/build/constructor-blocks/build/img/blocks/text.png" alt="карусель">
            <span class="bg">Текстовые блоки</span>
          </div>
          <div class="lazy-onload template" data-src="/build/constructor-blocks/build/img/blocks/footer.png">
            <img src="/build/constructor-blocks/build/img/blocks/footer.png" alt="карусель">
            <span class="bg">Подвалы</span>
          </div>
        </div>
      </div>
      <div class="list templates-list" id="cons-list">
        <div class="navigation">
          <button class="back" id="to-main"><i class="icon-left-side"></i></button>
          <h3>Выберите шаблон</h3>
        </div>
        <div class="templates" id="templates-2">
          <div class="lazy-onload template"
               data-src="/build/constructor-blocks/build/img/preview/header_1.png">
            <img src="/build/constructor-blocks/build/img/preview/header_1.png" alt="шапка">
            <span class="bg">Вариант 1</span>
          </div>
          <div class="lazy-onload template"
               data-src="/build/constructor-blocks/build/img/preview/navigation_1.png">
            <img src="/build/constructor-blocks/build/img/preview/navigation_1.png" alt="навигация">
            <span class="bg">Вариант 2</span>
          </div>
          <div class="lazy-onload template"
               data-src="/build/constructor-blocks/build/img/preview/slider_1.png">
            <img src="/build/constructor-blocks/build/img/preview/slider_1.png" alt="карусель">
            <span class="bg">Вариант 3</span>
          </div>
          <div class="lazy-onload template"
               data-src="/build/constructor-blocks/build/img/preview/slider_2.png">
            <img src="/build/constructor-blocks/build/img/preview/slider_2.png" alt="карусель">
            <span class="bg">Вариант 4</span>
          </div>
          <div class="lazy-onload template"
               data-src="/build/constructor-blocks/build/img/preview/galery_1.png">
            <img src="/build/constructor-blocks/build/img/preview/galery_1.png" alt="карусель">
            <span class="bg">Вариант 5</span>
          </div>
          <div class="lazy-onload template"
               data-src="/build/constructor-blocks/build/img/preview/galery_2.png">
            <img src="/build/constructor-blocks/build/img/preview/galery_2.png" alt="карусель">
            <span class="bg">Вариант 6</span>
          </div>
        </div>
      </div>
      <div class="list palettes-list" id="palettes-list">
        <div class="navigation">
          <button class="close-palettes back" id="close-palettes"><i class="constructor-cancel"></i></button>
          <h3>Выберите цветовую схему</h3>
        </div>
        <div class="templates palettes" id="palettes">
          <div class="lazy-onload template"
               data-src="/build/constructor-blocks/build/img/palettes/scheme_1.png">
            <img src="/build/constructor-blocks/build/img/palettes/scheme_1.png" alt="шапка">
            <span class="bg">Цветовая схема 1</span>
          </div>
          <div class="lazy-onload template"
               data-src="/build/constructor-blocks/build/img/palettes/scheme_2.png">
            <img src="/build/constructor-blocks/build/img/palettes/scheme_2.png" alt="навигация">
            <span class="bg">Цветовая схема 2</span>
          </div>
        </div>
        <div class="navigation__buttons">
          <button class="btn" id="apply-palette"><i class="constructor-ok"></i>Применить</button>
          <button class="close-palettes btn"><i class="constructor-cancel"></i>Отменить</button>
        </div>
      </div>
      <div class="b_menu">
        <button class="button" title="Отменить последнее действие"><i class="constructor-undo"></i></button>
        <button class="button" title="Вернуть последнее действие"><i class="constructor-redo"></i></button>
        <button class="button button_save" title="Сохранить изменения и перейти в личный кабинет">Сохранить
                                                                                                  изменения<i
            class="constructor-save"></i></button>
      </div>
      <span class="constructor-fullscreen change-view" id="change-view"
            title="Перейти в полноэкранный режим просмотра"></span>
      <span class="constructor-colors color-pallete" id="change-palette"
            title="Изменить цветовую палитру сайта"></span>
    </div>
  </div>
  <script
    src="https://code.jquery.com/jquery-3.3.1.min.js"
    integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
    crossorigin="anonymous"></script>
  <script src="https://unpkg.com/flickity@2/dist/flickity.pkgd.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>
  <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery.lazy/1.7.9/jquery.lazy.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"
          integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU="
          crossorigin="anonymous"></script>
  <script src="/media/js/default/constructor.site.js"></script>