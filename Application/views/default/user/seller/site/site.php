<div>
  <a href="/user/sites-list">Вернуться к списку сайтов</a>
  <a href="/user/seller/site/edit/<?= $shop[ 'id' ] ?>">Редактировать</a>
</div>
<div>
  <p><strong>Дата создания: </strong><?= $shop[ 'created_at' ] ?></p>
  <p><strong>Дата дата изменения: </strong><?= $shop[ 'updated_at' ] ?></p>
  <p><strong>Название сайта: </strong><?= $shop[ 'name' ] ?></p>
  <p><strong>Адрес сайта: </strong><?= $shop[ 'url' ] ?></p>
  <p><strong>Статус: </strong><?= $shop[ 'status_name' ] ?></p>
  <p><strong>Тариф: </strong><?= $shop[ 'rate_name' ] ?></p>
  <p><strong>Категории: </strong><br>
		<?php foreach( $shop[ 'categories' ] as $category ) {
			echo $category[ 'name' ] . '<br>';
		} ?></p>
  <p><strong>Оплачен: </strong><?= $shop[ 'paid' ] == 1 ? 'Оплачен' : 'Не оплачен' ?></p>
</div>
<div><a href="<?= $shop[ 'url' ] ?>" target="_blank">Перейти на сайт</a></div>
<div><a href="/user/seller/site/construct/<?= $shop[ 'id' ] ?>" target="_blank">Конструктор сайта</a></div>

