<div class="dashboard_wrap">
	<?php $this->block('user/seller/header_title'); ?>
	<?php $this->block('user/seller/seller_header'); ?>
	
	<div class="seller_referrals">
		<div class="title">
			<h1>Список партнеров(рефералов)</h1>
		</div>
		<?php if ( count($referrals) > 0 ): ?>
			<?php foreach ($referrals as $referral): ?>
				<p><?= $referral['name'] .' '. $referral['last_name']; ?></p>
			<?php endforeach; ?>
		<?php else: ?>
			<p>
				У Вас нет партнеров(рефералов)
			</p>
		<?php endif; ?>
	</div>
</div>