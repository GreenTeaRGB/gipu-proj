<div class="dashboard_wrap">
	<?php $this->block('user/seller/header_title'); ?>
	<?php $this->block('user/seller/seller_header'); ?>
	
	<div class="card_form">
		<form action="/user/save-card<?php echo ((isset($card['id'])) ? '/' . $card['id'] : ''); ?>" method="post">
			<div class="form_field">
				<label for="country">Название карты</label>
				<input type="text" id="description"
				       name="description"
				       value="<?php echo ((isset($card['description'])) ? $card['description'] : ''); ?>"
				       required>
			</div>
			<div class="form_field">
				<label for="card_num">Номер карты</label>
				<input type="text" id="card_num"
				       name="card_num"
				       value="<?php echo ((isset($card['card_num'])) ? $card['card_num'] : ''); ?>"
				       required>
			</div>
			<div class="form_field">
				<label for="month">Месяц</label>
				<input type="text" id="month"
				       name="month"
				       value="<?php echo ((isset($card['month'])) ? $card['month'] : ''); ?>"
				       required>
				
				<label for="year">Год</label>
				<input type="text" id="year"
				       name="year"
				       value="<?php echo ((isset($card['year'])) ? $card['year'] : ''); ?>"
				       required>
			</div>
			<div class="form_field">
				<label for="name">Имя</label>
				<input type="text" id="name"
				       name="name"
				       value="<?php echo ((isset($card['name'])) ? $card['name'] : ''); ?>"
				       required>
				
				<label for="last_name">Фамилия</label>
				<input type="text" id="last_name"
				       name="last_name"
				       value="<?php echo ((isset($card['last_name'])) ? $card['last_name'] : ''); ?>"
				       required>
			</div>
			
			<div class="button_group">
				<a href="/user/pay-cards">Назад</a>
				<input type="submit" class="button" value="Сохранить">
			</div>
		</form>
	</div>
</div>