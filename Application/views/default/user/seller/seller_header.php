<div class="seller_menu">
	<ul>
		<li>
			<a href="/user/dashboard">Регистрационные данные</a>
		</li>
		<li>
			<a href="/user/balance">Баланс</a>
		</li>
		<?php if ( isset($user['seller_type']) && $user['seller_type'] == \Application\Helpers\DataHelper::SELLER_PERSON_TYPE_ID ): ?>
			<li>
				<a href="/user/pay-cards">Платежные карты</a>
			</li>
		<?php endif; ?>
		<li>
			<a href="/user/sites-list">Список сайтов</a>
		</li>
		<li>
			<a href="/user/orders-statistic">Статистика заказов</a>
		</li>
		<li>
			<a href="/user/referral-list">Список партнеров(рефералов)</a>
		</li>
		<li>
			<a href="/user/supplier-list">Список привлеченных поставщиков</a>
		</li>
		<li>
			<a href="/user/chat">Личные сообщения</a>
		</li>
	</ul>
</div>