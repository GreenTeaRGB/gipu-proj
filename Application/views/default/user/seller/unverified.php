<div class="dashboard_wrap">
	<?php $this->block('user/seller/header_title'); ?>
	
	<div class="seller_info">
		<div class="subtitle">
			<h2>Статус : <?= \Application\Models\UserStatus::getStatusById($user['status_id']); ?></h2>
		</div>
		<?php if ( $user['status_id'] == \Application\Helpers\DataHelper::USER_REJECTED_STATUS
			|| $user['status_id'] == \Application\Helpers\DataHelper::DEFAULT_USER_STATUS
			|| $user['status_id'] == \Application\Helpers\DataHelper::USER_REQUIRES_EDITING ): ?>
			<div>
				<p class="note">
					Для получения полного доступа, заполните форму верификации
				</p>
			</div>
			<div>
				<div class="subtitle">
					<h3>Форма верификации</h3>
				</div>
				<form action="/user/verify-seller" method="post" enctype="multipart/form-data">
					<div class="form_field">
						<label for="seller_type">Тип продавца</label>
						<select name="seller_type" id="seller_type">
							<?php foreach ($seller_types as $seller_type): ?>
								<option value="<?= $seller_type['id']; ?>"><?= $seller_type['seller_type']; ?></option>
							<?php endforeach; ?>
						</select>
					</div>
					
					<div class="form_field passport_data">
						<div>
							<label for="passport_data">Фото паспорта</label>
							<input type="file" name="passport_data[]" id="passport_data">
<!--							<input type="file" name="passport_data[]">-->
<!--							<input type="file" name="passport_data[]">-->
						</div>
						<h3>Паспортные данные</h3>
						<div>
							<label for="passport_name">Имя</label>
							<input type="text" name="passport_name" id="passport_name" required>
						</div>
						<div>
							<label for="passport_last_name">Фамилия</label>
							<input type="text" name="passport_last_name" id="passport_last_name" required>
						</div>
						<div>
							<label for="passport_num">Номер паспорта</label>
							<input type="text" name="passport_num" id="passport_num" required>
						</div>
					</div>
					
					<div class="form_field juristic_documents">
						<div>
							<label for="juristic_documents">Фото документов на регистрацию юр.лица</label>
							<input type="file" name="juristic_documents[]" id="juristic_documents">
						</div>
						<h3>Данные юр.лица</h3>
						<?php if ( count($juristic_types) > 0 ): ?>
						<div>
							<label for="juristic_type_id">Тип юр.лица</label>
							<select name="juristic_type_id" id="juristic_type_id" required>
								<?php foreach ($juristic_types as $juristic_type): ?>
									<option value="<?= $juristic_type['id']?>">
										<?= $juristic_type['type']?>
									</option>
								<?php endforeach; ?>
							</select>
						</div>
						<?php endif; ?>
						<div>
							<label for="organization_name">Наименование юр.лица</label>
							<input type="text" name="organization_name" id="organization_name" required>
						</div>
						<div>
							<label for="juristic_address">Юр.адрес</label>
							<textarea type="text" name="juristic_address" id="juristic_address" required></textarea>
						</div>
						<div>
							<label for="inn">ИНН</label>
							<input type="text" name="inn" id="inn" required>
						</div>
						<div>
							<label for="kpp">КПП</label>
							<input type="text" name="kpp" id="kpp" required>
						</div>
						<div>
							<label for="checking_account">Р/С</label>
							<input type="text" name="checking_account" id="checking_account" required>
						</div>
						<div>
							<label for="bik">БИК</label>
							<input type="text" name="bik" id="bik" required>
						</div>
						<div>
							<label for="ks">К/С</label>
							<input type="text" name="ks" id="ks" required>
						</div>
					</div>
					
					<div class="button_group">
						<input type="submit" value="Отправить">
					</div>
				</form>
			</div>
		<?php else: ?>
			<p>
				Ваши документы проверяются
			</p>
		<?php endif; ?>
	</div>
</div>

<script>
	var sellerType = $('#seller_type');
	var defaultSellerType = "<?= \Application\Helpers\DataHelper::DEFAULT_SELLER_TYPE; ?>";
	var juristicDocuments = $('.juristic_documents');
	var passportData = $('.passport_data');
	
	var toggleFields = function () {
		if ( sellerType.val() == defaultSellerType ) {
			juristicDocuments.hide();
			$('form input:hidden, form select:hidden, form textarea:hidden').attr("disabled", true);
			
			passportData.find('input:hidden').attr("disabled", false);
			passportData.show()
		}
		else {
			$('form input:hidden, form select:hidden, form textarea:hidden').attr("disabled", false);
			juristicDocuments.show();
			
			passportData.hide();
			passportData.find('input:hidden').attr("disabled", true);
		}
	};
	
	$(document).ready(function () {
		toggleFields();
		
		sellerType.on('change', function () {
			toggleFields();
		});
	});
</script>