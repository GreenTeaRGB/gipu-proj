<link rel="stylesheet" href="/media/css/default/cabinets.css">
<template id="construct"></template>
<template id="l-menu">
    <nav class="col-lg-2 col-md-2 l-menu">
        <p class="page-path"><router-link :to="{name: 'Главная'}">Личный кабинет</router-link> / {{pageName}}</p>
        <h1>{{pageName}}</h1>
        <ul class="l-menu__items">
            <li v-for="(el, index) in lMenuItems">
                <router-link :to="el.link" @click.native="redirect(el.link, index)" active-class="l-menu__items_current">{{el.name}}<span v-if="el.count > 0">{{el.count}}</span></router-link>
                <ul v-if="el.sub">
                    <li v-for="subel in el.sub">
                        <router-link :to="subel.link" exact-active-class="l-menu__items_current">{{subel.name}}</router-link>
                    </li>
                </ul>
            </li>
            <li>
                <a href="#!" @click="exit">Выйти</a>
            </li>
        </ul>
    </nav>
</template>

<!-- ALERTS -->
<template id="alerts">
    <div :class="['cabinet__alerts', { cabinet__alerts_isOpen: !hide }]" v-if="alerts[0]">
        <span class="hide" @click="hide = !hide">{{getText()}}<i class="icon-right-side"></i></span>
        <div class="list">
            <alert v-for="(alert, index) in alerts" :data="alert" :index="index"></alert>
        </div>
    </div>
</template>

<!-- ALERT -->
<template id="alert">
    <router-link v-if="show" @click="hide" :to="data.link" :class="['alert', getClass(data.type)]">
        <h3>{{data.header}}</h3>
        <p>{{data.message}}</p>
        <span class="icon-cancel alert__close" @click.prevent="hide"></span>
    </router-link>
</template>

<!-- DEFAULT-BLOCK -->
<template id="infoBlock">
    <div class="cabinet__container__block">
        <h3>{{blockName}}</h3>
        <data-list :data="data"></data-list>
        <router-link :to="link" class="btn_open-page" v-if="link">Настройки<i class="icon-arrow-right"></i></router-link>
    </div>
</template>

<!-- TABLE-BLOCK-LONG -->
<template id="tableBlockLong">
    <div class="cabinet__container__block_long">
        <h3>{{blockName}}</h3>
        <form class="search__input">
            <span>от: </span>
            <input type="date" name="min-date" v-model="currentDateMin" :max="currentDateMax" min="2018-08-21">
            <span>до: </span>
            <input type="date" name="max-date" v-model="currentDateMax" :max="today" :min="currentDateMin">
            <button type="submit"><i class="icon-search"></i></button>
        </form>
        <div class="table">
            <table data-slideout-ignore>
                <thead>
                <tr>
                    <th v-for="header in data.headers">{{header}}</th>
                </tr>
                </thead>
                <tbody>
                <tr v-for="row in data.rows">
                    <td v-for="val in row">{{val}}</td>
                </tr>
                </tbody>
            </table>
        </div>
        <div class="load-more center-block" v-if="!hideButtons">
            <router-link :to="link || {name: 'История продаж'}" class="btn btn_white center-block">Загрузить ещё</router-link>
        </div>
    </div>
</template>

<!-- COLUMN-CHART-BLOCK -->
<template id="columnChartBlock">
    <div class="cabinet__container__block">
        <h3>{{blockName}}</h3>
        <canvas :id="chartName" class="chart" data-slideout-ignore></canvas>
    </div>
</template>

<!-- LINE-CHART-BLOCK -->
<template id="lineChartBlock">
    <div>
        <canvas :id="chartName" class="chart" height="160" data-slideout-ignore></canvas>
    </div>
</template>

<!-- DATA-LIST -->
<template id="dataList">
    <ul class="list_data">
        <li v-for="(val, key) in data">
            <span>{{key}}:</span>
            <p>{{val}}</p>
        </li>
    </ul>
</template>

<!-- MODAL-WINDOW -->
<template id="modalWindow">
    <transition name="modal">
        <div class="modal__wrap" @click.self="$router.go(-1)" data-slideout-ignore>
            <div class="modal modal_new" :class="{'modal_new_small' : isSmall}">
                <div class="modal__header">
                    <h5>{{blockName}}</h5>
                    <span class="icon-cancel cancel" @click="$router.go(-1)"></span>
                </div>
                <component v-bind:is="template" :data="data"></component>
            </div>
        </div>
    </transition>
</template>

<!-- MODAL-ИСТОРИЯ-ПРОДАЖ -->
<template id="modalTable">
    <div>
        <table-block-long block-name="" :data="data" :hide-buttons="true"></table-block-long>
        <pagination :data="data.pagination"></pagination>
    </div>
</template>
<!-- MODAL-ВЫВОД-СРЕДСТВ -->
<template id="modal-withdraw">
    <div class="modal_withdraw">
        <form id="#withdraw" action="">
            <div class="row">
                <div class="col-xs-6">
                    <p>Сумма для вывода, руб</p>
                    <input v-model="user.currentSum" :max="user.maxSum" :disabled="user.waitCode" type="number" min="100" placeholder="10000" class="only-number" required>
                    <span v-if="user.currentSum > user.maxSum" class="error">Максимальная сумма {{user.maxSum}}руб.</span>
                    <template v-if="user.waitCode">
                        <p>Код подтверждения из смс</p>
                        <input v-model="user.smsCode" @input="errors.smsCode = false" type="text" placeholder="*****" required>
                        <span v-if="errors.smsCode" class="error">Неверный код из смс</span>
                    </template>
                </div>
                <div v-if="user.type == 1" class="col-xs-6">
                    <p>Расчетный счет</p>
                    <span class="account">{{user.account}}</span>
                </div>
                <div v-if="user.type == 0" class="col-xs-6">
                    <p>Выберите карту</p>
                    <label for="card" class="select">
                        <select :disabled="user.waitCode" id="card" name="currentCard" v-model="user.currentCard" required>
                            <option value="" disabled selected>Выберите карту</option>
                            <option v-for="card in user.cards" :value="card.id">{{card.val}}</option>
                        </select>
                    </label>
                </div>
            </div>
            <input type="submit" @click.prevent="sendRequest" class="center-block btn btn_red" :value=" user.waitCode ? 'Отправить заявку на вывод средств' : 'Получить код подтверждения' ">
        </form>
        <i>Комиссия за вывод составляет {{ user.type == 0 ? '43.1%' : '3%' }} <span @click='$root.showTooltip($event, `<div class="tooltip_long"><div class="col-md-6"><span>Для физических лиц:</span><ul class="list"><li>подоходный налог - 13%</li><li>взносы в пенсионный фонд - 22%</li><li>взнос по ФСС - 3%</li><li>взнос по ФФОМС - 5.1%</li><li>комиссия за транзакцию - 3%</li></ul></div><div class="col-md-6"><span>Для юридических лиц:</span><ul class="list"><li>комиссия за транзакцию - 3%</li></ul></div></div>`)' class="btn_info"></span></i>
    </div>
</template>
<!-- MODAL-ПЕРЕВОД-АГЕНТУ -->
<template id="modal-transfer">
    <div class="modal_withdraw">
        <form id="#withdraw" action="">
            <div class="row">
                <div class="col-md-6">
                    <p>Сумма для вывода, руб</p>
                    <input v-model="user.currentSum" :max="user.maxSum" :disabled="user.waitCode" type="number" min="100" placeholder="10000" class="only-number" required>
                    <span v-if="user.currentSum > user.maxSum" class="error">Максимальная сумма {{user.maxSum}}руб.</span>
                    <template v-if="user.waitCode">
                        <p>Код подтверждения из смс</p>
                        <input v-model="user.smsCode" @input="errors.smsCode = false" type="text" placeholder="*****" required>
                        <span v-if="errors.smsCode" class="error">Неверный код из смс</span>
                    </template>
                </div>
                <div class="col-md-6">
                    <p>id агента</p>
                    <input v-model="user.agentId" @change="checkUserExist" :disabled="user.waitCode" min="3" type="number" placeholder="123456" class="only-number" required>
                    <span v-if="!errors.userExist" class="error">Пользователь с таким id не найден</span>
                </div>
            </div>
            <input type="submit" @click.prevent="sendRequest" class="center-block btn btn_red" :value=" user.waitCode ? 'Отправить заявку на вывод средств' : 'Получить код подтверждения' ">
        </form>
        <i>Комиссия за перевод составляет 0,5%</a></i>
    </div>
</template>
<template id="modal-cards">
    <div class="modal_cards">
        <ul class="list_data">
            <li v-for="(card, index) in cards">
                {{card.val}}
                <span @click="deleteCard(card.id, index)" class="icon-cancel"></span>
            </li>
            <li ref="newCard" class="new">
                <span @click="openNew" class="plus">+</span>
                <div class="inner">
                    <input @keyup="errors.cardNumber = false" @keyup.enter="addNew" @keyup.esc="closeNew" minlength="16" maxlength="16" ref="cardNumber" type="text" placeholder="Номер карты" class="only-number">
                    <span @click="addNew">+</span>
                </div>
            </li>
            <i v-if="errors.cardNumber" class="error">Неправильный номер карты</i>
        </ul>
    </div>
</template>

<!-- PAGINATION -->
<template id="pagination">
    <ul class="center-block catalog__pages pagination">
        <li v-for="(elem, index) in pages" @click="setPage(index + 1)" :class="{'catalog__pages_current': index + 1 == currentPage}">{{index+1}}</li>
        <li v-if="lastPage > pages" @click="setPage(lastPage)" class="last-page" :class="{'catalog__pages_current': lastPage == currentPage}">{{lastPage}}</li>
    </ul>
</template>

<!-- 404-page -->
<template id="pageNotFound">
    <div class="cabinet__container__block_long cabinet_404">
        <h1>Ошибка 404.<br>Нет такой страницы :(</h1>
    </div>
</template>

<!-- TOOLTIP -->
<template id="template-tooltip">
    <transition name="modal">
        <div @keyup.esc="visible = false" @click="visible = false" v-if="visible" class="tooltip__wrap">
            <div :style="{ left: position.left +'px', top: position.top +'px' }" v-html="message" class="tooltip">

            </div>
        </div>
    </transition>
</template>
<!-- MAIN-PAGE -->
<template id="page-main">
    <div class="cabinet__page">
        <info-block block-name="Личные данные" :link="{name: 'Настройки'}" :data="personalData"></info-block>
        <div class="cabinet__container__block">
            <h3>Доход за последние 7 дней</h3>
            <line-chart-block block-name="Популярные товары" chart-name="chart-popular-products" link="/some" :data="chart"></line-chart-block>
        </div>
        <table-block-long block-name="История продаж" :data="tableData" :hide-buttons="false"></table-block-long>
        <router-view></router-view>
    </div>
</template>

<!-- SETTINGS-PAGE -->
<template id="settings">
    <div class="cabinet__container__block_long settings">
        <form action="">
            <ul class="list_data">
                <li v-for="field in fields.default">
                    <span>{{field.name}}:</span>
                    <input v-if="field.editable" :type="field.type" :id="field.id" minlength="5" :value="field.val" class="dynamic-width">
                    <label v-if="field.editable" :for="field.id" class="icon-edit"></label>
                    <p v-if="!field.editable">{{field.val}}</p>
                </li>
                <li>
                    <span>Форма юридической ответственности:</span>
                    <label for="juristic_type" class="select">
                        <select name="juristic_type" id="juristic_type" v-model="juristicType">
                            <option value="1">Физическое лицо</option>
                            <option value="2">ИП</option>
                        </select>
                    </label>
                </li>
                <template v-if="juristicType == 2">
                    <li v-for="field in fields.juristic">
                        <span>{{field.name}}:</span>
                        <input v-if="field.type == 'file'" type="file" :name="field.dbName" :id="field.id">
                        <template v-if="field.type != 'file'">
                            <input v-if="field.editable" :type="field.type" :id="field.id" minlength="5" :value="field.val" class="dynamic-width">
                            <label v-if="field.editable" :for="field.id" class="icon-edit"></label>
                            <p v-if="!field.editable">{{field.val}}</p>
                        </template>
                    </li>
                </template>
            </ul>
            <input @click="saveChanges" type="button" value="Сохранить" class="btn btn_red center-block">
        </form>
    </div>
</template>

<!-- MAGAZINE-PAGE -->
<template id="page-magazine">
    <div class="cabinet__container__block_long magazine">
        <h2>{{magazine.name}}</h2>
        <h3>Общая информация</h3>
        <div class="settings">
            <ul class="list_data">
                <li>
                    <span>Название сайта:</span>
                    <input @change="saveName" v-model="magazine.name" type="text" id="site-name" minlength="5" class="dynamic-width">
                    <label for="site-name" class="icon-edit"></label>
                </li>
                <li v-for="param in magazine.params">
                    <span>{{param.name}}:</span>
                    <p>{{param.val}}</p>
                </li>
            </ul>
        </div>

        <div class="col-md-6">
            <h4>Количество продаж на сайте</h4>
            <line-chart-block block-name="Популярные товары" chart-name="chart-popular-products" link="/some" :data="charts.popular"></line-chart-block>
        </div>
        <div class="col-md-6">
            <h4>Посещаемость сайта</h4>
            <line-chart-block block-name="Посещаемость за день" chart-name="chart-visitors" link="/some" :data="charts.visits"></line-chart-block>
        </div>
        <div class="center-block">
            <a :href="'/user/seller/site/construct/'+$route.params.id" class="btn btn_red">Конструктор сайта</a>
            <a href="#" target="_blank" class="btn btn_red">Перейти на сайт</a>
            <router-link :to="'/magazines/'+ $route.params.id +'/management'" class="btn btn_red">Управление товарами</router-link>
        </div>
    </div>
</template>

<!-- MESSAGES-PAGE -->
<template id="page-messages">
    <div class="cabinet__container__block_long messages" @keyup.esc="closeDialogue">
        <div class="dialogues col-md-3">
            <h2>Диалоги</h2>
            <ul>
                <li v-for="dialog in dialogs">
                    <router-link :to="'/messages/' + dialog.userId" class="human">
                        <div class="avatar">
                            <img :src="dialog.avatar" alt="">
                        </div>
                        <h3>{{dialog.name}}<span class="online" v-if="dialog.online"></span></h3>
                        <p>{{dialog.message}}</p>
                    </router-link>
                </li>
            </ul>
            <div class="message-field message-field_long">
                <h4>{{newDialogue.message}}</h4>
                <input @keyup.enter="findUser"
                       v-model="newDialogue.id"
                       id="new-dialogue"
                       type="number"
                       placeholder="32134890"
                       class="new-dialogue">
                <button @click="findUser" class="message-button icon-message find-contact"></button>
            </div>
        </div>
        <div class="dialogue col-md-9">
            <router-view></router-view>
            <template v-if="!currentDialogue">
                <div class="img"></div>
                <h2>Выберите существующий диалог, либо <label for="new-dialogue">создайте новый</label></h2>
            </template>
        </div>
    </div>
</template>

<!-- PAGE-DIALOGUE -->
<template id="dialogue-window">
    <div @keyup.esc="closeDialogue" data-slideout-ignore>
        <div class="heading">
            <div class="human">
                <div class="avatar">
                    <img :src="dialogue.avatar" alt="">
                </div>
                <h3>{{dialogue.name}}</h3>
                <p>{{dialogue.lastAppearance}}</p>
                <div class="online" v-if="dialogue.userOnline"></div>
            </div>
        </div>
        <ul>
            <li v-for="(message, index) in dialogue.messages" :id=" index == (dialogue.messages.length - 1) ? 'lastMessage' : '' ">
                <div class="message" :class="message.incoming ? 'friend' : 'you'">
                    <div class="avatar">
                        <!-- v-if="dialogue.messages[index - 1].incoming != message.incoming" -->
                        <img :src="message.avatar" alt="">
                    </div>
                    <p class="message__text">{{message.text}}<span>{{ message.incoming ? '' : getMessageStatus(message.status)}}</span></p>
                </div>
            </li>
        </ul>
        <div class="message-field">
            <textarea @keyup.enter="sendMessage" v-model="message.text" placeholder="Введите текст сообщения..."></textarea>
            <button @click="sendMessage" class="message-button textarea-button icon-message"></button>
        </div>
    </div>
</template>

<!-- MESSAGE-ALERT -->
<template id="incoming-message">
    <router-link @click="close" :to="'/messages/' + message.id" v-if="show" class="incoming-message">
        <div class="avatar">
            <img :src="message.avatar" alt="avatar">
        </div>
        <h3>{{message.name}}<span class="online"></span></h3>
        <p>{{message.text}}</p>
        <span @click.prevent="close" class="icon-cancel close"></span>
    </router-link>
</template>

<!-- PARTNERS-PAGE -->
<template id="page-partners">
    <div class="page-partners">
        <div class="cabinet__container__block page-partners__info">
            <div>
                <h3>Приглашенные партнеры</h3>
                <span>{{statistics.partners.total}}<i>всего</i></span>
                <span>{{statistics.partners.mount}}<i>в этом месяце</i></span>
                <span>{{statistics.partners.magazines}}<i>магазинов</i></span>
            </div>
            <div>
                <h3>Доходы по партнерской программе</h3>
                <span class="long">{{statistics.income.total}}<i>всего</i></span>
                <span class="long">{{statistics.income.mount}}<i>в этом месяце</i></span>
            </div>
        </div>
        <div class="cabinet__container__block page-partners__info">
            <h3>Доходы по партнерской программе</h3>
            <canvas id="lineChart" class="chart" height="100" data-slideout-ignore></canvas>
        </div>
        <div class="cabinet__container__block_long">
            <ul class="list">
                <li v-for="user in users" class="user">
                    <div>
                        <img :src="user.avatar">
                        <span>{{user.percent}}</span>
                        <p>{{user.name}}</p>
                        <i>id{{user.id}}</i>
                        <a v-for="magazine in user.magazines" :href="magazine" target="_blank">{{magazine}}</a>
                        <i>Подключен {{user.regDate}}</i>
                    </div>
                    <i @click="showSubs" v-if="user.subs" class="more">Показать {{user.subs.length}} подключенных агента</i>
                    <ul class="list sub">
                        <li v-for="suser in user.subs" class="user user_sub">
                            <img :src="suser.avatar">
                            <span>{{suser.percent}}</span>
                            <p>{{suser.name}}</p>
                            <i>id{{suser.id}}</i>
                            <a v-for="magazine in suser.magazines" :href="magazine" target="_blank">{{magazine}}</a>
                            <i>Подключен {{suser.regDate}}</i>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</template>

<!-- WALLET-PAGE -->
<template id="page-wallet">
    <div class="page-wallet">
        <div class="cabinet__container__block_long page-wallet__info">
            <div class="col-md-5 page-wallet__info">
                <h3>Баланс
                    <span class="ruble">{{info.balance}}</span>
                </h3>
                <p>Доход за последнюю неделю<span class="ruble">{{info.week}}</span></p>
            </div>
            <div class="col-md-7 page-wallet__btns">
                <router-link :to="{ name: 'Вывод средств' }" class="btn btn_red">Вывести средства на карту</router-link>
              <span v-if="userType == 1">
                <router-link :to="{ name: 'Мои карты' }" class="btn btn_white">Мои карты</router-link>
                </span>
                <router-link :to="{ name: 'Перевод средств' }" class="btn btn_white">Перевести агенту</router-link>
            </div>
        </div>
        <table-block-long block-name="История платежей" link="/wallet/history" :data="tableData" :hide-buttons="false"></table-block-long>
        <router-view></router-view>
    </div>
</template>

<!-- MANAGAMENT-PAGE -->
<template id="magazine-management">
    <div class="stock" data-slideout-ignore>
        <div class="stock__filters">
            <form>
                <span>Категория: </span>
                <i class="select"><select name="category" v-model="currentCategory">
                        <option value="all">Все категории</option>
                        <option v-for="cat in categories" :value="cat.main">{{cat.main}}</option>
                    </select></i>
                <span>Подкатегория: </span>
                <i class="select"><select name="category" v-model="currentSubCategory">
                        <option value="all">Не выбрано</option>
                        <option v-for="cat in subCategories" :value="cat">{{cat}}</option>
                    </select></i>
                <input type="checkbox" id="showDeleted"><label for="showDeleted">Показывать скрытый товар</label>
                <!-- <router-link to="daw" class="add">Добавить товар в магазин</router-link> -->
            </form>
            <div class="stock__headers">
                <div class="col-md-5 col-xs-6">
                    <b>Сортировать по: </b>
                    <span v-for="el in headers.left" @click="sortBy(el.val)" :class="[{ 'current': el.val == sortedBy.by }, { 'current_reverse': sortedBy.reverse }]">{{el.text}}</span>
                </div>
                <div class="col-md-4 col-xs-3">
                    <span v-for="el in headers.middle" @click="sortBy(el.val)" :class="[{ 'current': el.val == sortedBy.by }, { 'current_reverse': sortedBy.reverse }]">{{el.text}}</span>
                </div>
                <div class="col-xs-3">
                    <span v-for="el in headers.right" @click="sortBy(el.val)" :class="[{ 'current': el.val == sortedBy.by }, { 'current_reverse': sortedBy.reverse }]">{{el.text}}</span>
                </div>
            </div>
        </div>
        <div class="stock__products">
            <ul data-slideout-ignore class="fixed-scroll">
                <li class="product" v-for="(product, index) in tableData.rows">
                    <router-link :to="'/magazines/'+ $route.params.id +'/product/' + product.id">
                        <div class="col-md-1 col-sm-1 col-xs-1 product__img">
                            <img :src="product.img" alt="фото">
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-4">
                            <b>id{{product.id}}</b>
                            <h4>{{product.name}}</h4>
                            <i>{{product.category}}</i>
                            <b>арт.{{product.article}}</b>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-4">
                            <div class="product__center">
                                <p>Оптовая цена: {{product.prices.opt}}</p>
                                <p>Рекомендованная цена: {{product.prices.recomend}}</p>
                                <b>Статус товара: {{getStatus(product.status)}}</b>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-3" @click.prevent>
                            <div class="product__right">
                                <p>Ваша стоимость:</p>
                                <input @change="setPrice($event, product)" v-model="product.prices.current" type="text" class="only-number number">
                            </div>
                        </div>
                    </router-link>
                    <!-- @click="deleteItem($event, product.id, product.index, product.name)" -->
                    <span v-if="product.status != 1" class="product__button icon-cancel" @click="deleteItem(product)"></span>
                    <span v-else class="product__button icon-check" @click="restoreItem(product)"></span>
                </li>
            </ul>
        </div>
    </div>
</template>

<!-- PRODUCT-PAGE -->
<template id="page-product">
    <div class="container-fluid merchandise">
        <h1>{{product.name}} <span class="merchandise__delete" @click="deleteProduct">Удалить товар</span> <div><i>арт.{{product.article}}</i> <i>id{{product.id}}</i></div></h1>
        <div class="row">
            <div class="col-md-6 col-sm-7 col-xs-12 page-product__content photos">
                <ul class="photos__list slider-photos-list">
                    <li v-for="photo in product.photos.preview"><img alt="img" class="lazy" :src="photo"></li>
                </ul>
                <div class="slider-photos-photo photos__photo">
                    <ul>
                        <li v-for="photo in product.photos.big"><img alt="img" class="lazy-onload" :src="photo"></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-6 col-sm-5 col-xs-12 merchandise__settings">
                <div class="col-md-4 col-sm-5 col-xs-5 price" @click="editPrice">
                    <p>Цена:</p>
                    <span class="ruble">{{product.price - product.discount}}
						<span class="ruble discount" v-if="product.discount > 0">{{product.price}}</span>
					</span>
                    <i class="icon-edit edit"></i>
                </div>
                <div class="col-md-8 col-sm-7 col-xs-6 r-price">
                    <p>Оптовая цена: <span class="ruble">{{product.prices.opt}}</span></p>
                    <p>Рекомендованная цена: <span class="ruble">{{product.prices.recomend}}</span></p>
                </div>
            </div>
            <div class="col-md-6 col-sm-5 col-xs-12 discount">
                <p>Текущая скидка:</p>
                <input @keyup.enter="setDiscount" type="text" id="discount" :placeholder="product.discount +' руб.'" class="only-number">
                <button class="btn btn_red" @click="setDiscount">Указать скидку</button>
            </div>
            <div class="col-md-6 col-sm-5 col-xs-12 merchandise__buttons">
                <a :href="product.link" class="btn btn_white" target="_blank">Перейти на страницу товара</a>
                <button class="btn btn_white">Указать об ошибке в товаре</button>
            </div>
        </div>
        <div class="row page-product states">
            <h1>Статистика продаж</h1>
            <div class="chart">
                <line-chart-block chart-name="productChart" :data="product.chart"></line-chart-block>
            </div>
            <h1>Отзывы об этом товаре</h1>
            <div class="container-fluid reviews">
                <div v-for="review in product.reviews" class="col-md-9 review">
                    <div class="review__photo">
                        <img :src="review.user.avatar" alt="photo" class="lazy">
                        <span class="mark">{{review.mark}}</span>
                    </div>
                    <div class="review__body">
                        <p class="name">{{review.user.name}}</p>
                        <i class="mark">{{review.short}} <i class="date">{{review.date}}</i></i>
                        <dl>
                            <dt>Достоинста</dt>
                            <dd>{{review.good}}</dd>
                            <dt>Недостатки:</dt>
                            <dd>{{review.bad}}</dd>
                            <dt>Комментарии:</dt>
                            <dd>{{review.comment}}</dd>
                        </dl>
                        <i class="experience">Опыт использования: {{review.experience}}</i>
                        <button class="btn_review btn_review_green btn_review_current"><i class="icon-check"></i>{{review.pluses}}</button>
                        <button class="btn_review btn_review_red btn_review_current" data-id="123132"><i class="icon-cancel"></i>{{review.minuses}}</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</template>

<main class="container-fluid cabinet__wrap">
    <div class="cabinet__bg"></div>
    <div class="container cabinet" id="app">
        <l-menu-item id="menu" :page-link="pageRoute" :l-menu-items="lMenuItems" :page-name="pageName"></l-menu-item>
        <section id="panel" class="col-lg-10 col-md-10 col-xs-12 container cabinet__container" :class="{ 'cabinet__container_transparent': transparentPage }">
            <div class="cabinet__wrap_in">
                <alerts :alerts="alerts" v-if="showAlerts"></alerts>
                <transition name="fade">
                    <div class="cabinet__loading" v-show="loading"></div>
                </transition>
                <transition name="modal">
                    <modal-window v-if="modalShow"></modal-window>
                </transition>
                <router-view></router-view>
            </div>
        </section>
        <incoming-message ref="message"></incoming-message>
        <transition name="modal">
            <tooltip ref="tooltip"></tooltip>
        </transition>
    </div>
</main>

<!--<div class="dashboard_wrap">-->
<!--	--><?php //$this->block('user/seller/header_title'); ?>
<!--	--><?php //$this->block('user/seller/seller_header'); ?>
<!--	-->
<!--	<div class="seller_info">-->
<!--		<div class="title">-->
<!--			<h3>Основная информация</h3>-->
<!--		</div>-->
<!--		<div>-->
<!--			<a href="/user/edit">Редактировать</a>-->
<!--		</div>-->
<!--		<div class="form_field">-->
<!--			<label for="user_name">Имя</label>-->
<!--			<span id="user_name">-->
<!--			--><?//= $user['name'] . ' ' . $user['last_name']; ?>
<!--		</span>-->
<!--		</div>-->
<!--		<div class="form_field">-->
<!--			<label for="phone">Телефон</label>-->
<!--			<span id="phone">--><?//= $user['phone']; ?><!--</span>-->
<!--		</div>-->
<!--		<div class="form_field">-->
<!--			<label for="email">Email</label>-->
<!--			<span id="email">--><?//= $user['email']; ?><!--</span>-->
<!--		</div>-->
<!--	</div>-->
<!--	--><?php //if ( count($juristic_data) > 0 ): ?>
<!--		<div class="additional_info">-->
<!--			<div class="title">-->
<!--				<h3>Дополнительная информация</h3>-->
<!--			</div>-->
<!--			<div>-->
<!--				<div>-->
<!--					<label for="organization_name">Наименование юр.лица</label>-->
<!--					<span id="organization_name">--><?//= $juristic_data['organization_name']; ?><!--</span>-->
<!--				</div>-->
<!--				<div>-->
<!--					<label for="juristic_address">Юр.адрес</label>-->
<!--					<span id="juristic_address">--><?//= $juristic_data['juristic_address']; ?><!--</span>-->
<!--				</div>-->
<!--				<div>-->
<!--					<label for="inn">ИНН</label>-->
<!--					<span id="inn">--><?//= $juristic_data['inn']; ?><!--</span>-->
<!--				</div>-->
<!--				<div>-->
<!--					<label for="kpp">КПП</label>-->
<!--					<span id="kpp">--><?//= $juristic_data['kpp']; ?><!--</span>-->
<!--				</div>-->
<!--				<div>-->
<!--					<label for="checking_account">Р/С</label>-->
<!--					<span id="checking_account">--><?//= $juristic_data['checking_account']; ?><!--</span>-->
<!--				</div>-->
<!--				<div>-->
<!--					<label for="bik">БИК</label>-->
<!--					<span id="bik">--><?//= $juristic_data['bik']; ?><!--</span>-->
<!--				</div>-->
<!--				<div>-->
<!--					<label for="ks">К/С</label>-->
<!--					<span id="ks">--><?//= $juristic_data['ks']; ?><!--</span>-->
<!--				</div>-->
<!--			</div>-->
<!--		</div>-->
<!--	--><?php //endif; ?>
<!--</div>-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.bundle.js"></script>
<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
<script src="https://unpkg.com/vue-router/dist/vue-router.js"></script>
<!--<script src="/media/js/default/cabinets.js"></script>-->
<script src="/media/js/default/cabinet-agent.js"></script>