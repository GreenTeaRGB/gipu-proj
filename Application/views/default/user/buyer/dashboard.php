<div class="dashboard_wrap">
	<?php $this->block('user/buyer/buyer_header'); ?>
	
	<div class="buyer_info">
		<div>
			<a href="/user/edit">Редактировать</a>
		</div>
		<div class="form_field">
			<label for="user_name">Имя</label>
			<span id="user_name">
				<?= $user['name'] . ' ' . $user['last_name']; ?>
			</span>
		</div>
		<div class="form_field">
			<label for="phone">Телефон</label>
			<span id="phone"><?= $user['phone']; ?></span>
		</div>
		<div class="form_field">
			<label for="email">Email</label>
			<span id="email"><?= $user['email']; ?></span>
		</div>
		<div class="form_field">
			<label for="phone">Получать уведомления по телефону</label>
			<span id="phone"><? echo ($user['notify_by_phone'] ? '<i class="fas fa-check"></i>' : '<i class="fas fa-times"></i>'); ?></span>
		</div>
		<div class="form_field">
			<label for="email">Получать уведомления по email</label>
			<span id="email"><? echo ($user['notify_by_email'] ? '<i class="fas fa-check"></i>' : '<i class="fas fa-times"></i>'); ?></span>
		</div>
	</div>
</div>