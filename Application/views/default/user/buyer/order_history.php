<div class="dashboard_wrap">
	<?php $this->block('user/buyer/buyer_header'); ?>
	
	<div class="order_history_wrap">
		<div class="title">
			<h1>История заказов</h1>
		</div>
		<div class="order_history">
			<?php if ( count($orders) > 0 ): ?>
				<?php foreach ($orders as $order): ?>
					<div class="order_item">
						<div class="title">
							<span>Заказ</span>
							<span>№<?= $order['id']; ?></span>
						</div>
						<div>
							<span>Сумма заказа</span>
							<span>
								<?php echo($order['total'] + $order['delivery_price'] - $order['discount']); ?>
							</span>
							<span class="currency">₽</span>
						</div>
						<div>
							<span>
								Дата заказа
							</span>
							<span>
								<?= date("d.m.Y", $order['created_at']); ?>
							</span>
						</div>
						<div>
							<span>
								Время заказа
							</span>
							<span>
								<?= date("H:i", $order['created_at']); ?>
							</span>
						</div>
						<div class="button_group">
							<a href="/user/order-preview/<?= $order['id']; ?>">Подробнее</a>
						</div>
					</div>
				<?php endforeach; ?>
			<?php else: ?>
				<p>
					У Вас нет заказов
				</p>
			<?php endif; ?>
		</div>
	</div>
</div>