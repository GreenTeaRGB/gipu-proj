<?php $this->block('user/buyer/header_title'); ?>
<div class="buyer_menu">
	<ul>
		<li>
			<a href="/user/dashboard">Регистрационные данные</a>
		</li>
		<li>
			<a href="/user/order-history">История заказов</a>
		</li>
		<li>
			<a href="/user/balance">Баланс и бонусы</a>
		</li>
		<li>
			<a href="/user/addresses">Адреса доставки</a>
		</li>
	</ul>
</div>