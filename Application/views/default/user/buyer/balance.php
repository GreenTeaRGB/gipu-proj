<div class="dashboard_wrap">
	<?php $this->block('user/buyer/buyer_header'); ?>
	
	<div class="buyer_balance">
		<div class="title">
			<h1>Баланс и бонусы</h1>
		</div>
		<div class="amount_wrap">
			<span>Ваш баланс</span>
			<span><?= $user['balance']; ?></span>
		</div>
		<hr>
		<div>
			<h4>Бонусы</h4>
			<?php if ( count($bonuses) > 0 ): ?>
				<p class="note">У Вас есть следующие бонусы</p>
				<?php foreach ($bonuses as $bonus): ?>
					<div class="bonus_item">
						<?= $bonus['title']; ?>
					</div>
				<?php endforeach; ?>
			<?php else: ?>
				<p>
					У Вас нет бонусов
				</p>
			<?php endif; ?>
		</div>
	</div>
</div>
