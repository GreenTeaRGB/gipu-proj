<div class="dashboard_wrap">
	<?php $this->block('user/buyer/buyer_header'); ?>
	
	<div class="button_group">
		<a href="/user/add-address" class="button">Добавить адрес</a>
	</div>
	<div class="addresses_list">
		<div class="title">
			<h1>Список адресов доставки</h1>
		</div>
		<?php if ( $addresses && count($addresses) > 0 ): ?>
			<?php foreach ($addresses as $address): ?>
				<div class="address_item">
					<div>
						<a href="/user/edit-address/<?= $address['id']; ?>">Редактировать адрес</a>
						<a href="/user/remove-address/<?= $address['id']; ?>">Удалить адрес</a>
					</div>
					<div>
						<span>Имя</span><span><?= $address['delivery_name']; ?></span>
					</div>
					<div>
						<span>Фамилия</span><span><?= $address['delivery_last_name']; ?></span>
					</div>
					<div>
						<span>Отчество</span><span><?= $address['patronymic']; ?></span>
					</div>
					<div>
						<span>Страна</span><span><?= $address['country']; ?></span>
					</div>
					<?php if ( $address['region'] ): ?>
						<div>
							<span>Область</span><span><?= $address['region']; ?></span>
						</div>
					<?php endif; ?>
					<div>
						<span>Город</span><span><?= $address['city']; ?></span>
					</div>
					<div>
						<span>Улица</span><span><?= $address['street']; ?></span>
					</div>
					<div>
						<span>Номер дома</span><span><?= $address['house_number']; ?></span>
					</div>
					<?php if ( $address['apartment'] ): ?>
						<div>
							<span>Номер квартиры</span><span><?= $address['apartment']; ?></span>
						</div>
					<?php endif; ?>
					<div>
						<span>Почтовый индекс</span><span><?= $address['post_index']; ?></span>
					</div>
					<div>
						<span>Телефон</span><span><?= $address['phone']; ?></span>
					</div>
				</div>
			<?php endforeach; ?>
		<?php else: ?>
			<p>
				У Вас нет сохраненных адресов
			</p>
		<?php endif; ?>
	</div>
</div>