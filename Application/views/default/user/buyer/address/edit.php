<div class="dashboard_wrap">
	<?php $this->block('user/buyer/buyer_header'); ?>
	
	<div class="address_form">
		<form action="/user/save-address<?php echo((isset($address['id'])) ? '/' . $address['id'] : ''); ?>"
			  method="post">
			<div class="form_field">
				<label for="delivery_name">Имя</label>
				<input type="text" id="delivery_name"
					   name="delivery_name"
					   value="<?php echo((isset($address['delivery_name'])) ? $address['delivery_name'] : ''); ?>"
					   required>
			</div>
			
			<div class="form_field">
				<label for="delivery_last_name">Фамилия</label>
				<input type="text" id="delivery_last_name"
					   name="delivery_last_name"
					   value="<?php echo((isset($address['delivery_last_name'])) ? $address['delivery_last_name'] : ''); ?>"
					   required>
			</div>
			
			<div class="form_field">
				<label for="patronymic">Отчество</label>
				<input type="text" id="patronymic"
					   name="patronymic"
					   value="<?php echo((isset($address['patronymic'])) ? $address['patronymic'] : ''); ?>"
					   required>
			</div>
			
			<div class="form_field">
				<label for="country">Страна</label>
				<input type="text" id="country"
					   name="country"
					   value="<?php echo((isset($address['country'])) ? $address['country'] : ''); ?>"
					   required>
			</div>
			<div class="form_field">
				<label for="region">Область</label>
				<input type="text" id="region"
					   name="region"
					   value="<?php echo((isset($address['region'])) ? $address['region'] : ''); ?>">
			</div>
			
			<div class="form_field">
				<label for="city">Город</label>
				<input type="text" id="city"
					   name="city"
					   value="<?php echo((isset($address['city'])) ? $address['city'] : ''); ?>"
					   required>
			</div>
			
			<div class="form_field">
				<label for="street">Улица</label>
				<input type="text" id="street"
					   name="street"
					   value="<?php echo((isset($address['street'])) ? $address['street'] : ''); ?>"
					   required>
			</div>
			
			<div class="form_field">
				<label for="house_number">Номер дома</label>
				<input type="text" id="house_number"
					   name="house_number"
					   value="<?php echo((isset($address['house_number'])) ? $address['house_number'] : ''); ?>"
					   required>
			</div>
			<div class="form_field">
				<label for="apartment">Номер квартиры</label>
				<input type="text" id="apartment"
					   name="apartment"
					   value="<?php echo((isset($address['apartment'])) ? $address['apartment'] : ''); ?>">
			</div>
			
			<div class="form_field">
				<label for="post_index">Почтовый индекс</label>
				<input type="text" id="post_index"
					   name="post_index"
					   value="<?php echo((isset($address['post_index'])) ? $address['post_index'] : ''); ?>" required>
			</div>
			
			<div class="form_field">
				<label for="phone">Телефон</label>
				<input type="text" id="phone"
					   name="phone"
					   value="<?php echo((isset($address['phone'])) ? $address['phone'] : ''); ?>" required>
			</div>
			
			<div class="button_group">
				<a href="/user/addresses">Назад</a>
				<input type="submit" class="button" value="Сохранить">
			</div>
		</form>
	</div>
</div>