<div class="dashboard_wrap">
	<?php $this->block('user/buyer/buyer_header'); ?>
	<div class="buyer_info">
		
		<form action="/user/save" method="post">
			<div class="form_field">
				<label for="name">Имя</label>
				<input type="text" id="name" name="name" value="<?= $user['name']; ?>" required>
			</div>
			<div class="form_field">
				<label for="name">Фамилия</label>
				<input type="text" id="last_name" name="last_name" value="<?= $user['last_name']; ?>" required>
			</div>
			<div class="form_field">
				<label for="phone">Телефон</label>
				<input type="text" id="phone" name="phone" value="<?= $user['phone']; ?>" required>
			</div>
			<div class="form_field">
				<label for="email">Email</label>
				<input type="text" id="email" name="email" value="<?= $user['email']; ?>" required>
			</div>
			<div class="form_field">
				<label for="notify_by_phone">Получать уведомления по телефону</label>
				<input type="hidden" name="notify_by_phone" value="0" />
				<input type="checkbox" id="notify_by_phone"
					   name="notify_by_phone" value="1"
					<?php echo ($user['notify_by_phone'] ? 'checked' : ''); ?>>
			</div>
			<div class="form_field">
				<label for="notify_by_email">Получать уведомления по email</label>
				<input type="hidden" name="notify_by_email" value="0" />
				<input type="checkbox" id="notify_by_email"
					   name="notify_by_email" value="1"
					<?php echo ($user['notify_by_email'] ? 'checked' : ''); ?>>
			</div>
			
			<div class="button_group">
				<a href="/user/dashboard">Назад</a>
				<input type="submit" value="Сохранить">
			</div>
		</form>
	</div>
</div>