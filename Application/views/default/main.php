<?php
  use Application\Helpers\AppHelper;
  use Application\Helpers\UserHelper;
  use Application\Models\Category;
  $cartData = AppHelper::getCartItemsCountWithTotal();
?>
<!DOCTYPE html>
<html lang="ru">
<head>
   <?= $this->block('layouts/head')?>
</head>
<body>
<?= $this->block( 'layouts/menu', [ 'cartData' => $cartData ] ); ?>
<?php $user = UserHelper::getUser(); ?>
<?php //if( $user && $user['id'] ): ?>
<!--    <div class="wish_list">-->
<!--        <a href="/wish-list">Избранное</a>-->
<!--    </div>-->
<?php //endif; ?>
<!--<div class="messages">-->
<!--  --><?//= AppHelper::showMessages() ?>
<!--</div>-->
<?= $content ?>
<?= $this->block( 'layouts/footer_scripts' ); ?>
<?= $this->block( 'layouts/footer' ); ?>
<div class="loader">
    <div class="loader-inner box1"></div>
    <div class="loader-inner box2"></div>
    <div class="loader-inner box3"></div>
</div>
<div class="bg_loader">
</div>
</body>
</html>
<?php AppHelper::eraseMessages(); ?>

