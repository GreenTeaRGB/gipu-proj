<?php
	$orderDelivery = \Application\Classes\Session::getByKey('order_delivery');
?>
<div class="dashboard_wrap">
	<div class="address_form">
		<form id="delivery_address">
			<div class="form_field">
				<label for="delivery_name">Имя</label>
				<input type="text" id="delivery_name"
					   name="delivery_name"
					   value="<?php echo((isset($orderDelivery['delivery_name'])) ? $orderDelivery['delivery_name'] : ''); ?>"
					   required>
			</div>
			
			<div class="form_field">
				<label for="delivery_last_name">Фамилия</label>
				<input type="text" id="delivery_last_name"
					   name="delivery_last_name"
					   value="<?php echo((isset($orderDelivery['delivery_last_name'])) ? $orderDelivery['delivery_last_name'] : ''); ?>"
					   required>
			</div>
			
			<div class="form_field">
				<label for="patronymic">Отчество</label>
				<input type="text" id="patronymic"
					   name="patronymic"
					   value="<?php echo((isset($orderDelivery['patronymic'])) ? $orderDelivery['patronymic'] : ''); ?>"
					   required>
			</div>
			
			<div class="form_field">
				<label for="country">Страна</label>
				<input type="text" id="country"
					   name="country"
					   value="<?php echo((isset($orderDelivery['country'])) ? $orderDelivery['country'] : ''); ?>"
					   required>
			</div>
			<div class="form_field">
				<label for="region">Область</label>
				<input type="text" id="region"
					   name="region"
					   value="<?php echo((isset($orderDelivery['region'])) ? $orderDelivery['region'] : ''); ?>">
			</div>
			<div class="form_field">
				<label for="city">Город</label>
				<input type="text" id="city"
					   name="city"
					   value="<?php echo((isset($orderDelivery['city'])) ? $orderDelivery['city'] : ''); ?>"
					   required>
			</div>
			<div class="form_field">
				<label for="street">Улица</label>
				<input type="text" id="street"
					   name="street"
					   value="<?php echo((isset($orderDelivery['street'])) ? $orderDelivery['street'] : ''); ?>"
					   required>
			</div>
			<div class="form_field">
				<label for="house_number">Номер дома</label>
				<input type="text" id="house_number"
					   name="house_number"
					   value="<?php echo((isset($orderDelivery['house_number'])) ? $orderDelivery['house_number'] : ''); ?>"
					   required>
			</div>
			<div class="form_field">
				<label for="apartment">Номер квартиры</label>
				<input type="text" id="apartment"
					   name="apartment"
					   value="<?php echo((isset($orderDelivery['apartment'])) ? $orderDelivery['apartment'] : ''); ?>">
			</div>
			<div class="form_field">
				<label for="post_index">Почтовый индекс</label>
				<input type="text" id="post_index"
					   name="post_index"
					   value="<?php echo((isset($orderDelivery['post_index'])) ? $orderDelivery['post_index'] : ''); ?>" required>
			</div>
			<div class="form_field">
				<label for="phone">Телефон</label>
				<input type="text" id="phone"
					   name="phone"
					   value="<?php echo((isset($orderDelivery['phone'])) ? $orderDelivery['phone'] : ''); ?>" required>
			</div>
		</form>
	</div>
</div>