<?php

use Application\Helpers\DataHelper;
use Application\Helpers\AppHelper;
$total = 0;
$data = \Application\Classes\Session::getByKey('user_data_cart');
$name = '';
$email = '';
$phone = '';
$address = '';
$comment = '';
if($data){
  $name = $data['name'];
  $email = $data['email'];
  $phone = $data['phone'];
  $address = $data['address'];
  $comment = $data['comment'];
}

?>
<?php
ob_start();
foreach ( $cartProducts as $cartProduct ) :
  $total += ( $cartProduct['product']['price'] * $cartProduct['count'] );
  ?>
  <div class="product">
    <img src="<?= $cartProduct['product']['images'][0] ?>" alt="">
    <i class="sub-text"><?= $cartProduct['category'] ?></i>
    <p>
      <?= $cartProduct['product']['name'] ?>
    </p>
    <span>
									<?= $cartProduct['count'] ?> шт. х <b class="price_ruble"><?= $cartProduct['product']['price'] ?></b> <b>₽</b>
								</span>
  </div>
<?php
endforeach;
$products = ob_get_contents();
ob_clean();
?>
<section class="contaner-fluid">
  <div class="container ordering">
    <div class="col-md-9 custom">
      <form action="/checkout/place-order" method="POST">

        <div class="ordering-cont">
          <h1>
            Оформление заказа
          </h1>
          <div class="col-md-6">
            <div class="personality">
              <div class="messages" style="color: #ff0000!important;">
                <?= AppHelper::showMessages() ?>
              </div>
              <h2>
                Личные данные
              </h2>
              <div class="client">
							<span>
								Представтесь
							</span>
                <input type="text" name="name" value="<?= $name ?>">
              </div>
              <div class="tel">
							<span>
								Телефон
							</span>
                <input type="tel" name="phone" placeholder="+7 (____) ____ - __ - __"
                       value="<?= $phone ?>">
              </div>
              <div class="email">
							<span>
								Почта
							</span>
                <input type="email" name="email" value="<?= $email ?>">
              </div>

            </div>
            <div class="delivery">
              <h2>
                Доставка
              </h2>
              <div class="adress">
							<span>
								Адрес доставки
							</span>
                <input type="text" name="address" value="<?=$address?>">
              </div>
              <div class="comment">
							<span>
								Комментарий
							</span>
                <input type="text" name="comment" value="<?=$comment?>">
              </div>

            </div>
            <div class="payment col-md-8">
              <?php if ( $payments ): ?>
                <h2>
                  Способ оплаты
                </h2>
              <?php foreach ( $payments as $payment ) :
                  if($payment['agent_only'] == 1 &&  $user['type'] != \Application\Helpers\DataHelper::DEFAULT_SELLER_TYPE_ID)
                    continue;
                  ?>
                  <div>
                    <input type="radio" id="id<?=$payment['id']?>" name="payment" value="<?=$payment['id']?>">
                    <label for="id<?=$payment['id']?>"><?=$payment['type']?></label>
                  </div>
              <?php endforeach; ?>
              <?php endif; ?>
              <div class="total">
                <div class="delivery-price">
                  Доставка:
                  <span>
										<?= DataHelper::DEFAULT_DELIVERY_PRICE ?>
									</span>
                </div>
                <?php if($discount > 0): ?>
                <div class="discount-price">
                  Скидка:
                  <span>
										<?= $discount ?>
									</span>
                </div>
                <?php endif; ?>
                <div class="total-price">
                  Итого:
                  <b>
                    <?= $total + DataHelper::DEFAULT_DELIVERY_PRICE - $discount ?>
                  </b>
                  <i>
                    ₽
                  </i>
                </div>
              </div>
              <button class="btn">
                Оформить заказ
              </button>
            </div>
          </div>
          <div class="col-md-6">
            <div class="products-cont">
              <h2>
                Ваша корзина
              </h2>
              <div class="products">
                <?= $products ?>
              </div>
            </div>
          </div>
        </div>
      </form>
    </div>
  </div>
</section>