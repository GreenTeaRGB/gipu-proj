<?php
$this->breadcrumbs[] = 'Избранное';
?>
<section class="contaner-fluid">
  <div class="container">

    <div class="favorites">
      <?=$this->block('layouts/breadcrumbs')?>
      <h1>
        Избранное
      </h1>
      <div class="sorting">
        <div class="catalog__menu_right">
          <a href="#!" class="btn_sort btn_sort_current"><i class="icon-plates"></i></a>
          <a href="#!" class="btn_sort"><i class="icon-column"></i></a>
        </div>
      </div>
      <div class="products">
        <?php foreach ( $products as $product ) {
          $this->block('layouts/product', ['product' => $product, 'class' => 'col-lg-2 col-sm-3 col-xs-6 product', 'inCart' => true]);
        } ?>
<!--        <div class="col-lg-2 col-sm-3 col-xs-6 product">-->
<!--          <div class="product__options">-->
<!--            <a href="#!"><i class="icon-like"></i></a>-->
<!--            <a href="#!"><i class="icon-stats"></i></a>-->
<!--          </div>-->
<!--          <a href="#!">-->
<!--            <div class="lazy product__photo" style="background-image: url(&quot;build/img/products/headphones.jpg&quot;);"></div>-->
<!--            <i class="sub-text">Гаджеты</i>-->
<!--            <p>Смартфон Samsung Galaxy S9+ 64GB</p>-->
<!--            <b class="price ruble">2900</b>-->
<!--            <button class="btn btn_red">-->
<!--              В корзину-->
<!--            </button>-->
<!--          </a>-->
<!--        </div>-->
<!--        <div class="col-lg-2 col-sm-3 col-xs-6 product">-->
<!--          <div class="product__options">-->
<!--            <a href="#!"><i class="icon-like"></i></a>-->
<!--            <a href="#!"><i class="icon-stats"></i></a>-->
<!--          </div>-->
<!--          <a href="#!">-->
<!--            <div class="lazy product__photo" style="background-image: url(&quot;build/img/products/smartphone.jpg&quot;);"></div>-->
<!--            <i class="sub-text">Гаджеты</i>-->
<!--            <p>Смартфон Samsung Galaxy S9+ 64GB</p>-->
<!--            <b class="price ruble">2900</b>-->
<!--            <button class="btn btn_red">-->
<!--              В корзину-->
<!--            </button>-->
<!--          </a>-->
<!--        </div>-->
<!--        <div class="col-lg-2 col-sm-3 col-xs-6 product">-->
<!--          <div class="product__options">-->
<!--            <a href="#!"><i class="icon-like"></i></a>-->
<!--            <a href="#!"><i class="icon-stats"></i></a>-->
<!--          </div>-->
<!--          <a href="#!">-->
<!--            <div class="lazy product__photo" style="background-image: url(&quot;build/img/products/iron.jpg&quot;);"></div>-->
<!--            <i class="sub-text">Гаджеты</i>-->
<!--            <p>Смартфон Samsung Galaxy S9+ 64GB</p>-->
<!--            <b class="price ruble">2900</b>-->
<!--            <button class="btn btn_red">-->
<!--              В корзину-->
<!--            </button>-->
<!--          </a>-->
<!--        </div>-->
<!--        <div class="col-lg-2 col-sm-3 col-xs-6 product">-->
<!--          <div class="product__options">-->
<!--            <a href="#!"><i class="icon-like"></i></a>-->
<!--            <a href="#!"><i class="icon-stats"></i></a>-->
<!--          </div>-->
<!--          <a href="#!">-->
<!--            <div class="lazy product__photo" style="background-image: url(&quot;build/img/products/router.jpg&quot;);"></div>-->
<!--            <i class="sub-text">Гаджеты</i>-->
<!--            <p>Смартфон Samsung Galaxy S9+ 64GB</p>-->
<!--            <b class="price ruble">2900</b>-->
<!--            <button class="btn btn_red">-->
<!--              В корзину-->
<!--            </button>-->
<!--          </a>-->
<!--        </div>-->
<!--        <div class="col-lg-2 col-sm-3 col-xs-6 product">-->
<!--          <div class="product__options">-->
<!--            <a href="#!"><i class="icon-like"></i></a>-->
<!--            <a href="#!"><i class="icon-stats"></i></a>-->
<!--          </div>-->
<!--          <a href="#!">-->
<!--            <div class="lazy product__photo" style="background-image: url(&quot;build/img/products/smartphone_2.jpg&quot;);"></div>-->
<!--            <i class="sub-text">Гаджеты</i>-->
<!--            <p>Смартфон Samsung Galaxy S9+ 64GB</p>-->
<!--            <b class="price ruble">2900</b>-->
<!--            <button class="btn btn_red">-->
<!--              В корзину-->
<!--            </button>-->
<!--          </a>-->
<!--        </div>-->
<!--        <div class="col-lg-2 col-sm-3 col-xs-6 product">-->
<!--          <div class="product__options">-->
<!--            <a href="#!"><i class="icon-like"></i></a>-->
<!--            <a href="#!"><i class="icon-stats"></i></a>-->
<!--          </div>-->
<!--          <a href="#!">-->
<!--            <div class="lazy product__photo" style="background-image: url(&quot;build/img/products/vacuum.jpg&quot;);"></div>-->
<!--            <i class="sub-text">Гаджеты</i>-->
<!--            <p>Смартфон Samsung Galaxy S9+ 64GB</p>-->
<!--            <b class="price ruble">2900</b>-->
<!--            <button class="btn btn_red">-->
<!--              В корзину-->
<!--            </button>-->
<!--          </a>-->
<!--        </div>-->
<!--        <div class="col-lg-2 col-sm-3 col-xs-6 product">-->
<!--          <div class="product__options">-->
<!--            <a href="#!"><i class="icon-like"></i></a>-->
<!--            <a href="#!"><i class="icon-stats"></i></a>-->
<!--          </div>-->
<!--          <a href="#!">-->
<!--            <div class="lazy product__photo" style="background-image: url(&quot;build/img/products/camera.jpg&quot;);"></div>-->
<!--            <i class="sub-text">Гаджеты</i>-->
<!--            <p>Смартфон Samsung Galaxy S9+ 64GB</p>-->
<!--            <b class="price ruble">2900</b>-->
<!--            <button class="btn btn_red">-->
<!--              В корзину-->
<!--            </button>-->
<!--          </a>-->
<!--        </div>-->
<!--        <div class="col-lg-2 col-sm-3 col-xs-6 product">-->
<!--          <div class="product__options">-->
<!--            <a href="#!"><i class="icon-like"></i></a>-->
<!--            <a href="#!"><i class="icon-stats"></i></a>-->
<!--          </div>-->
<!--          <a href="#!">-->
<!--            <div class="lazy product__photo" style="background-image: url(&quot;build/img/products/vacuum_2.jpg&quot;);"></div>-->
<!--            <i class="sub-text">Гаджеты</i>-->
<!--            <p>Смартфон Samsung Galaxy S9+ 64GB</p>-->
<!--            <b class="price ruble">2900</b>-->
<!--            <button class="btn btn_red">-->
<!--              В корзину-->
<!--            </button>-->
<!--          </a>-->
<!--        </div>-->
<!--        <div class="col-lg-2 col-sm-3 col-xs-6 product">-->
<!--          <div class="product__options">-->
<!--            <a href="#!"><i class="icon-like"></i></a>-->
<!--            <a href="#!"><i class="icon-stats"></i></a>-->
<!--          </div>-->
<!--          <a href="#!">-->
<!--            <div class="lazy product__photo" style="background-image: url(&quot;build/img/products/vacuum_3.jpg&quot;);"></div>-->
<!--            <i class="sub-text">Гаджеты</i>-->
<!--            <p>Смартфон Samsung Galaxy S9+ 64GB</p>-->
<!--            <b class="price ruble">2900</b>-->
<!--            <button class="btn btn_red">-->
<!--              В корзину-->
<!--            </button>-->
<!--          </a>-->
<!--        </div>-->
<!--        <div class="col-lg-2 col-sm-3 col-xs-6 product">-->
<!--          <div class="product__options">-->
<!--            <a href="#!"><i class="icon-like"></i></a>-->
<!--            <a href="#!"><i class="icon-stats"></i></a>-->
<!--          </div>-->
<!--          <a href="#!">-->
<!--            <div class="lazy product__photo" style="background-image: url(&quot;build/img/products/tv.jpg&quot;);"></div>-->
<!--            <i class="sub-text">Гаджеты</i>-->
<!--            <p>Смартфон Samsung Galaxy S9+ 64GB</p>-->
<!--            <b class="price ruble">2900</b>-->
<!--            <button class="btn btn_red">-->
<!--              В корзину-->
<!--            </button>-->
<!--          </a>-->
<!--        </div>-->
      </div>


    </div>

  </div>
</section>

<!--<div class="wishlist_wrap">-->
<!--	--><?php //if ( count($items) > 0 ): ?>
<!--		--><?php //foreach ($items as $item): ?>
<!--			--><?php //$product = $item['product']; ?>
<!--			<div class="wishlist_item">-->
<!--				<a href="/catalog/product/--><?//= $product['alias']; ?><!--">--><?//= $product['name']; ?><!--</a>-->
<!--				<div class="button_group">-->
<!--					<div>-->
<!--						<input type="number" class="product_count"-->
<!--							   min="1"-->
<!--							--><?php //echo($product['availability'] > 0 ? 'max="' . $product['availability'] . '"' : ''); ?>
<!--							   value="1"-->
<!--						>-->
<!--					</div>-->
<!--					--><?php //if ( $product['price'] ): ?>
<!--						--><?php //$priceWithCommission = \Application\Helpers\AppHelper::calculatePriceWithCommission($product['price'], $product['commission']); ?>
<!--						<div class="product_price">-->
<!--						<span>-->
<!--							--><?//= $priceWithCommission; ?>
<!--						</span>-->
<!--							<span class="currency">₽</span>-->
<!--						</div>-->
<!--					--><?php //endif; ?>
<!--					<div>-->
<!--						<a href="#" class="add_to_cart" data-id="--><?//= $product['id']; ?><!--">Добавить в корзину</a>-->
<!--					</div>-->
<!--				</div>-->
<!--				<div>-->
<!--					<a class="add_to_wishlist added" data-id="--><?//= $product['id']?><!--">Избранное</a>-->
<!--				</div>-->
<!--			</div>-->
<!--		--><?php //endforeach; ?>
<!--	--><?php //else: ?>
<!--		<div class="no_item">-->
<!--			Нет продуктов-->
<!--		</div>-->
<!--	--><?php //endif; ?>
<!--</div>-->