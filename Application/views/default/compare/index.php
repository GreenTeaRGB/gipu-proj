<?php
$this->breadcrumbs[] = 'Сравнение товаров';
?>

<section class="contaner-fluid">
  <div class="container">
    <?php if ( isset( $no_items ) && $no_items ): ?>
      <div class="no_items">
        Нет товаров для сравнения
      </div>
    <?php else: ?>
      <?php
      $displayProperties = [];
      ob_start();
      foreach ( $items as $index => $item ) :
        $warehouse = array_shift( $item['warehouses'] );
        ?>
        <td>
          <div class="position-relative item">
            <div class="product__options">
              <a href="#!" class="remove_from_compare" data-product_id="<?= $item['id'] ?>"
                 data-category_name="<?= $default_category ?>"><i class="icon-cancel"></i></a>
            </div>
            <a href="/catalog/product/<?= $item['alias'] ?>">
              <div class="lazy product__photo"
                   style="background-image: url(<?= $item['images'][0] ?>);"></div>
              <i class="sub-text"><?= $default_category ?></i>
              <p><?= $item['name'] ?></p>
              <b class="price ruble"><?= $item['full_price'] ?></b>
              <button class="btn btn_red add_to_cart" data-warehouse="<?= $warehouse['id'] ?>"
                      data-id="<?= $item['id'] ?>">
                <span>В корзину</span>
              </button>
            </a>
          </div>
        </td>
        <?php


        foreach ( $properties as $property ) {
          $prop = [];
          foreach ( $item["property_values"] as $property_value ) {
            if ( $property_value['property_id'] == $property['id'] ) {
              if ( $property['type'] == 'L' ) {
                $prop[] = $property['property_list'][$property_value['value_enum']]['value'];
              } else {
                $prop[] = $property_value['value'];
              }
            }
          }
          if ( count( $prop ) > 0 ) {
            $displayProperties[$property['property_group_name']][$property['name']][] = implode( ', ', $prop );
          }
        }
      endforeach;
      $products = ob_get_contents();
      ob_end_clean();
      ?>
      <div class="compare">
        <?= $this->block( 'layouts/breadcrumbs' ); ?>
        <h1>
          Сравнение товаров
        </h1>
        <div class="table-cont">
          <table class="compare_table compare_title">
            <tr class="first_line">
              <td>
                <div class="select-cont">
                  <span class="select"></span>
                  <select id="select_part" class="section">
                    <?php foreach ( $categories as $category ): ?>
                      <option value="<?= $category; ?>" <?= ( ( $default_category && $default_category == $category )
                        ? 'selected'
                        : '' ); ?>><?= $category; ?></option>
                    <?php endforeach; ?>
                  </select>
                </div>
                <a href="/compare/remove/">
                  <i class="icon-delete"></i>
                  <span>
									Очистить сравнение
								</span>
                </a>
              </td>

              <?= $products ?>
            </tr>
            <?php foreach ( $displayProperties as $groupName => $displayProperty ) :ob_start(); ?>
              <?php $countTd = 0;
              foreach ( $displayProperty as $propertyName => $propertyValues ):
                if ( $countTd == 0 ) {
                  $countTd = count( $propertyValues );
                } ?>
                <tr>
                  <td><?= $propertyName ?></td>
                  <?php
                  $yellow = [];
                  while ( $value = array_shift( $propertyValues ) ):
                    $isYellow = in_array( $value, $propertyValues );
                    if ( $isYellow ) {
                      $yellow[] = $value;
                    }
                    ?>
                    <td <?= $isYellow || in_array( $value, $yellow )
                      ? ' class="yellow"'
                      : '' ?>><?= $value ?></td>
                  <?php endwhile; ?>
                </tr>
              <?php endforeach;
              $propOutput = ob_get_contents();
              ob_end_clean(); ?>
              <tr class="grey">
                <td><?= $groupName ?></td><?= str_repeat( '<td></td>', $countTd ) ?></tr>
              <?= $propOutput ?>
            <?php endforeach; ?>
            <tr>
              <td></td>
              <?= $products ?>
            </tr>
          </table>
        </div>
      </div>
    <?php endif; ?>
  </div>
</section>
<!---->
<!--<div class="compare_wrap">-->
<!--    <div class="title">-->
<!--        <h1>Сравнение товаров</h1>-->
<!--    </div>-->
<!--  --><?php //if( isset( $no_items ) && $no_items ): ?>
<!--      <div class="no_items">-->
<!--          Нет товаров для сравнения-->
<!--      </div>-->
<!--  --><?php //else: ?>
<!--      <div class="loader"><span></span></div>-->
<!--      <table class="compare_table">-->
<!--          <tr>-->
<!--              <td>-->
<!--                  <table class="compare_title">-->
<!--                      <tr>-->
<!--                          <td class="compare_header">-->
<!--                              <div>-->
<!--                                  <select id="select_part">-->
<!--                                    --><?php //foreach( $categories as $category ): ?>
<!--                                        <option value="--><? //= $category; ?><!--" --><? //= ( ( $default_category && $default_category == $category )
//                                            ? 'selected'
//                                            : '' ); ?><!--><? //= $category; ?><!--</option>-->
<!--                                    --><?php //endforeach; ?>
<!--                                  </select>-->
<!--                              </div>-->
<!--                              <div>-->
<!--                                  <a href="/compare/remove/" class="remove_category_btn">Очистить сравнение</a>-->
<!--                              </div>-->
<!--                          </td>-->
<!--                      </tr>-->

<script>
  var categorySelect = $( '#select_part' );
  var compareTableTitle = $( '.compare_title' );

  // function getCategoryProperties(categoryName, items) {
  //     if (categoryName) {
  //         var msgContainer = $('.message');
  //         $.ajax({
  //                    type: "POST",
  //                    url: "/compare/get-properties",
  //                    dataType: "json",
  //                    data: {
  //                        category_name: categoryName,
  //                        is_ajax: true
  //                    },
  //                    beforeSend: function () {
  //                        categorySelect.prop("disabled", true);
  //                    }
  //                }).done(function (response) {
  //             categorySelect.prop("disabled", false);
  //
  //             if (response.hasOwnProperty('success') && response.success && response.hasOwnProperty('properties')) {
  //                 var properties = response.properties;
  //                 if (properties && !$.isEmptyObject(properties)) {
  //                     for (var propGroup in properties) {
  //                         if (properties.hasOwnProperty(propGroup)) {
  //                             var propertyGroupElement = $('<tr />', {
  //                                 class: 'property_group',
  //                                 text: propGroup
  //                             });
  //                             compareTableTitle.append(propertyGroupElement);
  //
  //                             var propertiesByGroup = properties[propGroup];
  //                             if (propertiesByGroup && propertiesByGroup.length > 0) {
  //                                 for (var propKey in propertiesByGroup) {
  //                                     if (propertiesByGroup.hasOwnProperty(propKey) && propertiesByGroup[propKey].main != 1) {
  //                                         var property = propertiesByGroup[propKey];
  //                                         // console.log(property);
  //                                         var propertyElement = $('<tr />', {
  //                                             class: 'property',
  //                                             text: property.name
  //                                         });
  //
  //                                         compareTableTitle.append(propertyElement);
  //                                     }
  //                                 }
  //                             }
  //                         }
  //                     }
  //                 }
  //                 if (items && items.length > 0) {
  //                     buildItemsForCompare(items, properties);
  //                 }
  //             }
  //         }).fail(function (response) {
  //             categorySelect.prop("disabled", false);
  //
  //             var message = $('<div />', {
  //                                 class: 'message message_error',
  //                                 text: response.message
  //                             }
  //             );
  //             msgContainer.append(message);
  //
  //             setTimeout(function () {
  //                 message.remove()
  //             }, 3000);
  //         });
  //     }
  //
  //     return false;
  // }

  //                           function buildItemsForCompare(items, sharedProperties) {
  //                               if (items && items.length > 0) {
  //                                   var wrapColumn = $('<td />', {
  //                                       class: 'compare_item_wrap'
  //                                   });
  //
  //                                   var itemsTable = $('<table />', {
  //                                       class: 'items_for_compare'
  //                                   });
  //
  //                                   var titleRow = $('<tr />', {
  //                                       class: 'title_row compare_header'
  //                                   });
  //                                   console.log(items);
  //                                   // for (var key in items) {
  //                                   //     if (items.hasOwnProperty(key)) {
  //                                   //         var item = items[key];
  //                                   //
  //                                   //         var titleColumn = $('<td />');
  //                                   //         // <div class="position-relative item">
  //                                   //         //     <div class="product__options">
  //                                   //         //         <a href="#!"><i class="icon-cancel"></i></a>
  //                                   //         //     </div>
  //                                   //         //     <a href="#!">
  //                                   //         //         <div class="lazy product__photo" style="background-image: url(&quot;build/img/products/smartphone_2.jpg&quot;);"></div>
  //                                   //         //         <i class="sub-text">Телефоны</i>
  //                                   //         //         <p>Смартфон Samsung Galaxy S9+ 64GB</p>
  //                                   //         //         <b class="price ruble">2900</b>
  //                                   //         //         <button class="btn btn_red">
  //                                   //         //             <span> В корзину</span>
  //                                   //         //         </button>
  //                                   //         //     </a>
  //                                   //         // </div>
  //                                   //         var mainDiv = $('<div>', { class: 'position-relative item'});
  //                                   //         var divForLinkCancel = $('<div>', {class: 'product__options'});
  //                                   //         var cancelLink = $('<a>', { href: '#',
  //                                   //                                     class: 'remove_from_compare',
  //                                   //                                     'data-product_id': item.id,
  //                                   //                                     'data-category_name': categorySelect.val(),
  //                                   //                                     html: '<i class="icon-cancel"></i>'
  //                                   //                                 });
  //                                   //         mainDiv.append(divForLinkCancel.append(cancelLink));
  //                                   //
  //                                   //         var mainLink = $('<a>', {href: item.alias });
  //                                   //
  //                                   //
  //                                   //         var itemImageSrc = (item.images && item.images.hasOwnProperty('0') && item.images[0])
  //                                   //             ? item.images[0]
  //                                   //             : '/images/no_photo.png';
  //                                   //         var imgDiv = $('<div>', {style: 'background-image: url('+itemImageSrc+');'});
  //                                   //         var iCategory = $('<i>', { class: 'sub-text', text:categorySelect.val()});
  //                                   //         var nameProduct = $('<p>', { text: item.name});
  //                                   //         var price = $('<b>', {class: 'price ruble', text: item.full_price });
  //                                   //         var button = $('<button>', {class: 'btn btn_red', html: $('<span>', {text: 'В корзину'})});
  //                                   //         mainLink.append(imgDiv);
  //                                   //         mainLink.append(iCategory);
  //                                   //         mainLink.append(nameProduct);
  //                                   //         mainLink.append(price);
  //                                   //         mainLink.append(button);
  //                                   //         // titleColumn.append(
  //                                   //         //     $('<img />', {
  //                                   //         //         class: 'product_image',
  //                                   //         //         src: itemImageSrc
  //                                   //         //     })
  //                                   //         // ).append(
  //                                   //         //     $('<div />', {
  //                                   //         //         class: 'item_name',
  //                                   //         //         text: item.name
  //                                   //         //     })
  //                                   //         // ).append(
  //                                   //         //     $('<div />', {
  //                                   //         //         class: 'item_price',
  //                                   //         //         text: item.full_price
  //                                   //         //     }).append(
  //                                   //         //         $('<span />', {
  //                                   //         //             class: 'currency',
  //                                   //         //             text: '₽'
  //                                   //         //         })
  //                                   //         //     )
  //                                   //         // ).append(
  //                                   //         //     $('<a />', {
  //                                   //         //         class: 'add_to_cart',
  //                                   //         //         text: 'В корзину'
  //                                   //         //     }).attr({
  //                                   //         //                 'data-id': item.id
  //                                   //         //             })
  //                                   //         // ).append(
  //                                   //         //     $('<a />', {
  //                                   //         //         class: 'remove_from_compare',
  //                                   //         //         text: 'X'
  //                                   //         //     }).attr({
  //                                   //         //                 'data-product_id': item.id,
  //                                   //         //                 'data-category_name': categorySelect.val()
  //                                   //         //             })
  //                                   //         // );
  //                                   //         $('.first_line').append(titleColumn.append(mainDiv.append(mainLink)));
  //                                   //         // titleRow.append(titleColumn);
  //                                   //         // itemsTable.append(titleRow);
  //                                   //     }
  //                                   // }
  //
  //                                   if (sharedProperties && !$.isEmptyObject(sharedProperties)) {
  //                                       for (var propertyGroup in sharedProperties) {
  //
  //                                           if (sharedProperties.hasOwnProperty(propertyGroup)) {
  //                                               var propertiesByGroup = sharedProperties[propertyGroup];
  //                                               var row = $('<tr />', {
  //                                                   class: 'property_group'
  //                                               });
  // //
  // //Названия столбцов
  // //
  //                                               for (var key1 in items) {
  //
  //                                                   if (items.hasOwnProperty(key1)) {
  //                                                       row.append(
  //                                                           $('<td />', {
  //                                                               class: 'item_value property',
  //                                                               html: '&nbsp;'
  //                                                           })
  //                                                       );
  //                                                   }
  //                                               }
  //
  //                                               itemsTable.append(row);
  //                                               if (propertiesByGroup.length > 0) {
  //                                                   for (var pvKey in propertiesByGroup) {
  //
  //                                                       if (propertiesByGroup.hasOwnProperty(pvKey) && propertiesByGroup[pvKey].main != 1) {
  //                                                           var property = propertiesByGroup[pvKey];
  //                                                           // console.log(property);
  //                                                           var propertyRow = $('<tr />');
  //                                                           // console.log(item);
  //                                                           for (var key in items) {
  //                                                               if (items.hasOwnProperty(key)) {
  //                                                                   var item = items[key];
  //                                                                   if (item.hasOwnProperty('property_values') && Object.keys(item.property_values).length > 0) {
  //                                                                       var tdValues = "";
  //                                                                       var enumEl = false;
  //                                                                       for (var pvKey in item.property_values) {
  //                                                                           if (item.property_values.hasOwnProperty(pvKey)) {
  //                                                                               var propertyValue = item.property_values[pvKey];
  //
  //                                                                               // console.log('prop_valuer - '+ propertyValue.property_id);
  //                                                                               // console.log('prop_id' + property.id);
  //
  //                                                                               if (propertyValue.property_id == property.id) {
  //                                                                                   // console.log('prop_id -- ' +property.id);
  //                                                                                   // console.log(propertyValue.value_enum);
  //                                                                                   if (propertyValue.value_enum != 0) {
  //                                                                                       // alert('df');
  //                                                                                       enumEl = true;
  //                                                                                       var enumId = propertyValue.value_enum;
  //                                                                                       var propertyList = property.hasOwnProperty('property_list')
  //                                                                                           ? property.property_list
  //                                                                                           : [];
  //                                                                                       if (Object.keys(propertyList).length > 0) {
  //                                                                                           for (var propListKey in propertyList) {
  //                                                                                               if (propertyList.hasOwnProperty(propListKey)) {
  //                                                                                                   var enumObj = propertyList[propListKey];
  //                                                                                                   if (enumId == enumObj.id) {
  //                                                                                                       tdValues += enumObj.value + ', ';
  //                                                                                                   }
  //                                                                                               }
  //                                                                                           }
  //                                                                                       }
  //                                                                                   } else {
  //
  //                                                                                       tdValues = propertyValue.value;
  //                                                                                   }
  //                                                                                   var itemColumn = $('<td />', {
  //                                                                                       class: 'item_value property',
  //                                                                                       text: tdValues
  //                                                                                   });
  //
  //                                                                                   // if (!enumEl) {
  //                                                                                   propertyRow.append(itemColumn);
  //
  //                                                                                   itemsTable.append(propertyRow);
  //                                                                                   // }
  //                                                                               }
  //                                                                           }
  //                                                                       }
  //
  //                                                                       if (enumEl) {
  //                                                                           if (tdValues) {
  //                                                                               tdValues = tdValues.substring(0, tdValues.length - 2);
  //                                                                           }
  //                                                                           var itemColumn = $('<td />', {
  //                                                                               class: 'item_value',
  //                                                                               text: tdValues
  //                                                                           });
  //
  //                                                                           propertyRow.append(itemColumn);
  //
  //                                                                           itemsTable.append(propertyRow);
  //                                                                       }
  //                                                                   }
  //                                                               }
  //                                                           }
  //                                                       }
  //                                                   }
  //                                               }
  //                                           }
  //                                       }
  //                                   }
  //
  //                                   wrapColumn.append(itemsTable);
  //                                   compareTableTitle.parent().parent().append(wrapColumn);
  //                               }
  //
  //                               return false;
  //                           }

  $( document ).ready( function () {
    var removeCategoryBtn = $( '.remove_category_btn' );
    removeCategoryBtn.attr( 'href', removeCategoryBtn.attr( 'href' ) + $( '#select_part' ).val() );

    categorySelect.on( 'change', function () {
      var that = $( this );
      var url = '/compare/' + that.val();

      removeCategoryBtn.attr( 'href', removeCategoryBtn.attr( 'href' ) + that.val() );

      location.replace( url );
    } );

    var categoryName = categorySelect.val();

    //var items = <?//= isset($items) ? json_encode( $items ) : '[]'; ?>//;
    //getCategoryProperties(categoryName, items);

    $( '.compare_table' ).on( 'click', '.remove_from_compare', function () {
      var that = $( this );
      var productId = that.data( 'product_id' );
      var categoryName = that.data( 'category_name' );

      $.ajax( {
        type: "POST",
        url: "/compare/remove-item",
        dataType: "json",
        data: {
          product_id: productId,
          category_name: categoryName,
          is_ajax: true
        }
      } ).done( function ( response ) {
        if ( response.hasOwnProperty( 'success' ) && response.success ) {
          if ( response.hasOwnProperty( 'category_removed' ) && response.category_removed ) {
            location.replace( '/compare' );
          } else {
            location.reload();
          }
        }
      } ).fail( function ( response ) {
        var message = $( '<div />', {
            class: 'message message_error',
            text: response.message
          }
        );
        msgContainer.append( message );

        setTimeout( function () {
          message.remove()
        }, 3000 );
      } );

      return false;
    } );
  } );
</script>
<!---->
<!--                  </table>-->
<!--              </td>-->
<!--          </tr>-->
<!--      </table>-->
<!--  --><?php //endif; ?>
<!--</div>-->