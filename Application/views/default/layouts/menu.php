<?php
  $countWishlist = \Application\Models\Wishlist::getCountForUser();
  $wishlist = \Application\Classes\Session::getByKey('wishlist');
  if($wishlist)
    $countWishlist += count($wishlist);
 $сompare = \Application\Classes\Session::getByKey('compare');

 $countCompare = 0;
  if($сompare)
 foreach( $сompare as $item ) $countCompare += count($item);
?>
<header class="container-fluid header">
    <div class="container">
		<span class="col-xs-2 menu-button">
			<button class="hamburger hamburger--collapse" id="menu-button" type="button">
				<span class="hamburger-box">
					<span class="hamburger-inner"></span>
				</span>
			</button>
		</span>
        <a href="/" class="col-md-2 col-sm-6 col-xs-5 logo">

        </a>
        <nav class="col-lg-7 col-md-8 header__navigation">
            <ul>
                <li><a href="/for_agents">Агентам</a></li>
                <li><a href="/for_providers.html">Поставщикам</a></li>
                <li><a href="/catalog">Каталог услуг</a></li>
                <li><a href="#!">Барахолка</a></li>
                <li><a href="/contacts">О компании</a></li>
                <li><a href="/questions">FAQ</a></li>
            </ul>
        </nav>
        <div class="col-lg-3 col-md-2 col-sm-3 col-xs-5 header__menu__wrap">
            <ul class="header__menu">
                <li><a href="/compare">
                        <i class="icon icon_small icon-stats">
                            <span class="indicator"><?= $countCompare ?></span>
                        </i>
                    </a>
                </li>
                <li><a href="/wish-list">
                        <i class="icon icon_small icon-like">
                              <span class="indicator"><?= $countWishlist ?></span>
                        </i>
                    </a>
                </li>
                <li>
                    <a href="/cart">
                        <i class="icon icon-basket">
                            <span class="indicator"><?= $cartData['count'] ?></span>
                        </i>
                      <?php if( $cartData['count'] > 0 ): ?>
                          <span class="accent-text ">Товара на: <span
                                      class="bold-text ruble"><?= $cartData['total']; ?></span></span>
                      <?php endif; ?>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</header>
<?php
if(self::getModule() !== 'cabinet'){
  $this->block('layouts/top_categories');
  $this->block('layouts/search');
}
?>

<!---->
<!--<ul>-->
<!--    <li><a href="/">Главная</a></li>-->
<!--    <li><a href="/catalog">Каталог</a></li>-->
<!--    <li><a href="/news">Новости</a></li>-->
<!--    <li><a href="/user/register">Регистрация</a></li>-->
<!--    <li><a href="/user/login">Вход</a></li>-->
<!--    <li><a href="/user/dashboard">Личный кабинет</a></li>-->
<!--    <li><a href="/questions">FAQ</a></li>-->
<!--    <li><a href="/compare">Сравнение</a></li>-->
<!--</ul>-->