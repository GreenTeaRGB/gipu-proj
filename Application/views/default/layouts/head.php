<?php
  $csrf = sha1(uniqid());
//    $_SESSION['_csrf'][] = $csrf;
?>
<title><?= isset($this->title)?$this->title:'' ?></title>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="_csrf" content="<?=$csrf?>">
<meta name="keywords" content="<?= isset($this->keywords)?$this->keywords:'' ?>">
<meta name="description" content="<?= isset($this->description)?$this->description:'' ?>">
<link rel="icon" type="image/x-icon" href="favicon.ico">
<link rel="stylesheet" href="/media/css/default/style.css">
<link rel="stylesheet" href="/media/css/default/question.css">
<link rel="stylesheet" href="/media/css/default/colorPicker.css">
<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,900&amp;subset=cyrillic" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Marck+Script&amp;subset=cyrillic" rel="stylesheet">
<link rel="stylesheet" href="/media/css/default/main.css">
<script src="https://code.jquery.com/jquery-3.3.1.js"
        integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
        crossorigin="anonymous"></script>

<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery.lazy/1.7.9/jquery.lazy.min.js"></script>
<script src="https://unpkg.com/flickity@2/dist/flickity.pkgd.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>

<script src="/media/js/default/formValidator.js"></script>
<script src="/media/js/default/validation.js"></script>
<script src="/media/js/default/news.js"></script>
<script src="/media/js/default/helper.js"></script>
<script src="/media/js/default/script.js"></script>
<script src="/media/js/default/mainScripts.js"></script>

