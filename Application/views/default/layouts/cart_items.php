<?php
	$cartProducts = \Application\Classes\Session::getByKey('cart');
	$defaultDeliveryPrice = \Application\Helpers\DataHelper::DEFAULT_DELIVERY_PRICE;
	$discount = \Application\Classes\Session::getByKey('discount');
	
//	echo '<pre>';
//	var_dump($cartProducts);
//	die();
?>
<div class="cart_items_wrap">
	<?php if ( $cartProducts && count($cartProducts) > 0 ): ?>
		<?php foreach ($cartProducts as $cartProduct): ?>
			<div class="cart_item">
				<div>

					<?= $cartProduct['product']['name']; ?>
				</div>
				<div>
					<span>
						Количество
					</span>
					<span>
						<?= $cartProduct['count']; ?>
					</span>
				</div>
				<div>
					<span>
						Стоимость
					</span>
					<span>
						<?= $cartProduct['product']['full_price']; ?>
					</span>
					<span class="currency">₽</span>
				</div>
			</div>
		<?php endforeach; ?>
		<div class="delivery">
			<span>Доставка</span>
			<span><?= $defaultDeliveryPrice; ?></span>
			<span class="currency">₽</span>
		</div>
		<?php if ( $discount && count($discount) > 0 ): ?>
			<div class="promo">
				<span>Промокод</span>
				<span>-<?= $discount['discount']; ?></span>
				<span class="currency">₽</span>
			</div>
		<?php endif; ?>
		<div class="cart_total"></div>
	<?php else: ?>
		<div>
			В корзине нет товаров
		</div>
	<?php endif; ?>
</div>
<script>
	var cartProducts = <?= json_encode($cartProducts); ?>;
	var deliveryPrice = <?= $defaultDeliveryPrice; ?>;
	var discount = <?= ($discount && count($discount) > 0) ? json_encode($discount) : json_encode([]); ?>;
	var CartItems = function (products, deliveryPrice = 0 , discount = {}) {
		var self = this;
		this.items = products;
		this.discount = discount;
		this.deliveryPrice = deliveryPrice;
		
		var totalWrap = $('.cart_total');
		
		this.calculateItems = function (items) {
			var sum = 0;
			for (var key in items) {
				if ( items.hasOwnProperty(key) ) {
					var item = items[key];
					
					sum += parseFloat(item.subtotal);
				}
			}
			
			if ( this.deliveryPrice > 0 ) {
				sum += parseInt(this.deliveryPrice);
			}
			
			if ( !jQuery.isEmptyObject(this.discount) ) {
				sum -= parseInt(this.discount.discount);
			}
			
			return sum.toFixed(2);
		};
		
		this.generateGrandTotal = function (total) {
			var wrap = $('<div />');
			
			var title = $('<span />', {
				class: 'total_title',
				text: 'К оплате'
			});
			
			var total = $('<span />', {
				class: 'total',
				text: total
			}).append(
				$('<span />', {
					class: 'cart_item_currency',
					text: '₽'
				})
			);
			
			wrap.append(title);
			wrap.append(total);
			totalWrap.append(wrap);
		};
		
		this.init = function () {
			if ( this.items && this.items.length > 0 ) {
				var grandTotal = this.calculateItems(this.items);
				
				this.generateGrandTotal(grandTotal);
			}
			else {
				window.location.replace("/cart");
			}
		};
		
		this.init();
	};
	new CartItems(cartProducts, deliveryPrice, discount);
</script>