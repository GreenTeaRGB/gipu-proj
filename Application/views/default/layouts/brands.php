<?php
  $brands = \Application\Models\Brands::findSort( [ 'active' => 1 ], [ 'sort' => 'asc' ] );
?>
<h2>Популярные бренды</h2>
<div class="corousel_small_circle corousel_static brands">
  <?php foreach( $brands as $brand ): ?>
    <a href="<?= $brand['link'] ?>" class="brand">
      <img src="<?= $brand['image'] ?>" alt="apple">
    </a>
  <?php endforeach; ?>
</div>