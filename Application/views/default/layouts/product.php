<div class="<?= isset( $class ) ? $class : 'col-sm-3 col-xs-6' ?> product">
  <div class="product__options">
    <a href="#"
       class="add_to_wishlist <?php echo( ( isset( $product['wishlist'] ) && $product['wishlist'] )
         ? 'wishlist_active'
         : '' ); ?>" data-id="<?= $product['id']; ?>"><i class="icon-like"></i></a>
    <a href="#" class="add_to_compare <?php echo( ( isset( $product['compare'] ) && $product['compare'] )
      ? 'compare_active'
      : '' ); ?>" data-id="<?= $product['id']; ?>"><i
        class="icon-stats"></i></a>
  </div>
  <a href="/catalog/product/<?= $product['alias']; ?>">
    <div class="lazy product__photo" data-src="<?= isset( $product['images'][0] )
      ? $product['images'][0]
      : '' ?>"></div>
    <i class="sub-text"><?= $product['category_name'] ?></i>
    <p><?= $product['name'] ?></p>
    <?php if ( $product['price'] ): ?>
      <?php $priceWithCommission = \Application\Helpers\AppHelper::calculatePriceWithCommission( $product['price'], $product['commission'] ); ?>
      <b class="price ruble"><?= $priceWithCommission; ?></b>
    <?php endif; ?>
    <?php if(isset($inCart) && $inCart): ?>
      <button class="btn btn_red add_to_cart" data-id="<?= $product['id'] ?>">
        В корзину
      </button>
    <?php endif; ?>
  </a>
</div>