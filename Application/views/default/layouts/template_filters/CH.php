<span class="rolled"><?= $property['name'] ?></span>
<ul class="list list_single limit-height limit-height_with-btn">
  <?php foreach( $property['property_list'] as $element ) : ?>
      <li><input type="checkbox" id="<?= $property['code'].'_'.$element['id'] ?>"
                 name="properties[<?= $property['id'] ?>][]"
            <?= isset( $_GET['properties'][$property['id']]) && in_array( $element['id'], $_GET['properties'][$property['id']])
                ? 'checked="checked"'
                : '' ?> value="<?=$element['id']?>">
          <label for="<?= $property['code'].'_'.$element['id'] ?>"><?= $element['value'] ?></label>
      </li>
  <?php endforeach; ?>
</ul>