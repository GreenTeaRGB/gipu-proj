<span class="rolled"><?= $property['name'] ?></span>
<ul class="list list_inline list_colors limit-height limit-height_with-btn" data-max="8"">
<?php foreach( $property['property_list'] as $element ) : ?>
  <li style="background: url(<?=$element['file']?>) no-repeat center center;  background-size: cover;">
    <input type="checkbox" id="<?= $property['code'].'_'.$element['id'] ?>"
           name="properties[<?= $property['id'] ?>][]"
        <?= isset( $_GET['properties'][$property['id']]) && in_array( $element['id'], $_GET['properties'][$property['id']])
            ? 'checked="checked"'
            : '' ?> value="<?=$element['id']?>">
    <label for="<?= $property['code'].'_'.$element['id'] ?>"></label>
  </li>
<?php endforeach; ?>
</ul>