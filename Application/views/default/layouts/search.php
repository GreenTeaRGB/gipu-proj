<?php $categories = \Application\Models\Category::getFullList(); ?>
<nav class="container-fluid search">
    <form class="container search__input" action="/search">
        <input type="text" name="q" class="input_search" value="<?= isset( $_GET['q'] )
            ? $_GET['q']
            : "" ?>" placeholder="Поиск товаров по сайту">
        <span class="select"><select name="category">
			<option value="-1">выберите раздел</option>
            <?php foreach( $categories as $category ) : ?>
                <option value="<?= $category['id'] ?>" <?= isset( $_GET['category'] ) &&  $_GET['category'] == $category['id']
                    ? "selected"
                    : "" ?>><?= $category['name'] ?></option>
            <?php endforeach; ?>
		</select></span>
        <button type="submit"><i class="icon-search"></i></button>
    </form>
</nav>