<?php
  use \Application\Classes\Session;
  use \Application\Models\Category;
  use \Application\Models\Product;
  $categories = Category::getFullList();
  $products = Session::getByKey('viewed_products');
  $products = Product::find(['id' => array_column($products, 'id')]);
  if( $products && count( $products ) > 0 ):
?>
<h2>Просмотренное</h2>
<div class="corousel corousel_small_circle corousel_static corousel_viewed">
    <?php foreach( $products as $product ):
      $product['category_name'] = $categories[$product['category_id']]['name'];
      ?>
      <?=$this->block('layouts/product', ['product' => $product])?>
    <?php
    endforeach;
   endif; ?>
</div>