
<section class="contaner-fluid thanks">
  <?php if ($show_thanks): ?>
    <div class="thanks-top">
        <div class="container">
            <div class="col-lg-5 col-md-8 custom">
              <div class="thanks__text">
                <span>
                        Заказ №<?= $order['id']; ?>
                </span>
                  <h1>
                      Спасибо! Заказ оформлен
                  </h1>
                  <p>
                      Заказ будет доставлен с <?=date('d')?> по <?=\Application\Helpers\DataHelper::redate('d M',strtotime("+2 day"))?> по адресу: <?= $order['address'] ?>. Как только заказ будет на месте, вам придёт смс.
                  </p>
                  <a href="/catalog" class="btn">Продолжить покупки</a>
              </div>
            </div>
        </div>
    </div>
  <?php endif; ?>
    <div class="thanks-bottom">
        <div class="container">
            <h2>
                Детали заказа
            </h2>
            <div class="col-lg-7 custom">
                <div class="col-md-6">
                  <?php 

                  if ( count($products) > 0 ): ?>
                    <div class="products">
                      <?php foreach ($products as $product): ?>

                          <div class="product">
                              <img src="<?=$product['images'][0]?>" alt="<?= $product['name']; ?>">
                              <i class="sub-text"><?= $product['category']; ?></i>
                              <p>
                                <?= $product['name']; ?>
                              </p>
                              <span>
									<?= $product['order_count']; ?> шт. х <?= \Application\Helpers\AppHelper::calculatePriceWithCommission($product['price'], $product['commission']); ?> <b>₽</b>
							</span>
                          </div>
                      <?php endforeach; ?>
                    </div>

                    <?php else: ?>
                    <div class="no_items">
                        Продуктов связанных с заказом не найдено
                    </div>
                  <?php endif; ?>
                </div>
                <div class="col-md-6">
                    <div class="details">

                        <h4>
                            Стоимость доставки
                        </h4>
                        <span class="give-point"><?= $order['delivery_price']; ?> ₽</span>
                        <span class="adress">
                            <?= $order['address'] ?>
                        </span>
<!--                        <a href="#" class="more">-->
<!--                            Подробнее-->
<!--                        </a>-->
                        <h4>
                            Данные получателя
                        </h4>
                        <span class="name">
                           <?= $order['name'] ?>
                        </span>

<!--                        <a href="mailto:m.soloviv@yandex.ru" class="email">m.soloviv@yandex.ru</a>-->
                        <a href="tel:<?= $order['phone']; ?>" class="phone"><?= $order['phone']; ?></a>
                        <span class="name">
                             <?= $order['comment'] ?>
                        </span>
                        <h4>
                            Способ оплаты
                        </h4>
                        <span class="payment">
                            <?=$order['payment_name']?>
                        </span>
<!---->
<!--                        <div class="order_delivery">-->
<!--                            <div class="title">-->
<!--                                <h3>Стоимость доставки</h3>-->
<!--                                <span class="give-point">--><?//= $order['delivery_price']; ?><!-- ₽</span>-->
<!--                                <p class="note">В черте города</p>-->
<!--                            </div>-->
<!--                        </div>-->
<!---->
<!--                      --><?php //if ( $order_address && count($order_address) > 0 ): ?>
<!--                          <div class="order_address">-->
<!--                              <div class="title">-->
<!--                                  <h3>-->
<!--                                      Данные получателя-->
<!--                                  </h3>-->
<!--                              </div>-->
<!--                              <div>-->
<!--                                  <span>--><?//= $order_address['delivery_last_name']; ?><!--</span>-->
<!--                                  <span>--><?//= $order_address['delivery_name']; ?><!--</span>-->
<!--                                  <span>--><?//= $order_address['patronymic']; ?><!--</span>-->
<!--                              </div>-->
<!--                              <div>-->
<!--                                  <span>--><?//= $order_address['country']; ?><!--</span>-->
<!--                                  <span>--><?//= $order_address['city']; ?><!--</span>-->
<!--                                --><?php //if ( $order_address['region'] ): ?>
<!--                                    <span>--><?//= $order_address['region']; ?><!--</span>-->
<!--                                --><?php //endif; ?>
<!--                              </div>-->
<!--                              <div>-->
<!--                                  <span>--><?//= $order_address['street']; ?><!--</span>-->
<!--                                  <span>--><?//= $order_address['house_number']; ?><!--</span>-->
<!--                                --><?php //if ( $order_address['apartment'] ): ?>
<!--                                    <span>--><?//= $order_address['apartment']; ?><!--</span>-->
<!--                                --><?php //endif; ?>
<!--                              </div>-->
<!--                              <div>-->
<!--                                  <span>--><?//= $order_address['post_index']; ?><!--</span>-->
<!--                              </div>-->
<!--                              <div>-->
<!--                                  <span>--><?//= $order_address['phone']; ?><!--</span>-->
<!--                              </div>-->
<!--                          </div>-->
<!--                      --><?php //else: ?>
<!--                          <div class="no_address">-->
<!--                              Адреса связанного с ордером не найдено-->
<!--                          </div>-->
<!--                      --><?php //endif; ?>



<!--                        <h4>-->
<!--                            Самовывоз-->
<!--                        </h4>-->
<!--                        <span class="give-point">-->
<!--							Пункт выдачи посылок Boxberry-->
<!--						</span>-->
<!--                        <span class="adress">-->
<!--							Санкт-Петербург, Большевиков, д. 10А-->
<!--						</span>-->
<!--                        <a href="#" class="more">-->
<!--                            Подробнее-->
<!--                        </a>-->
<!--                        <h4>-->
<!--                            Данные получателя-->
<!--                        </h4>-->
<!--                        <span class="name">-->
<!--							Соловьёв Михаил-->
<!--						</span>-->
<!--                        <a href="mailto:m.soloviv@yandex.ru" class="email">m.soloviv@yandex.ru</a>-->
<!--                        <a href="tel:+7 911 085-46-23" class="phone">+7 911 085-46-23</a>-->
<!--                        <h4>-->
<!--                            Способ оплаты-->
<!--                        </h4>-->
<!--                        <span class="payment">-->
<!--							Наличными-->
<!--						</span>-->

                    </div>
                </div>

            </div>
        </div>
    </div>
</section>




<!--<div class="order_item">-->
<!--	--><?php //if ($show_thanks): ?>
<!--		<div class="thanks">-->
<!--			<div>-->
<!--				Заказ №--><?//= $order['id']; ?>
<!--				--><?php //$relatedOrders = trim(trim($order_ids, $order['id']), ','); ?>
<!--				--><?php //if ( $relatedOrders ): ?>
<!--					<span>Связанные заказы (--><?php //echo($relatedOrders); ?><!--)</span>-->
<!--				--><?php //endif; ?>
<!--			</div>-->
<!--			<div>-->
<!--				Спасибо! Заказ оформлен-->
<!--			</div>-->
<!--			<div>-->
<!--				<span class="note">-->
<!--					Как только заказ будет на месте, Вам придёт смс.-->
<!--				</span>-->
<!--			</div>-->
<!--			<div>-->
<!--				<a href="/catalog">Продолжить покупки</a>-->
<!--			</div>-->
<!--		</div>-->
<!--	--><?php //endif; ?>
<!--	--><?php //\Application\Classes\Session::remove('show_thanks'); ?>
<!--	<div class="order_title">-->
<!--		<h1>Детали заказа</h1>-->
<!--	</div>-->
<!--	--><?php //if ( count($products) > 0 ): ?>
<!--		<div class="order_products">-->
<!--			--><?php //foreach ($products as $product): ?>
<!--				<div class="order_product">-->
<!--					--><?php //if ( $product['images'] && count($product['images']) > 0 ): ?>
<!--						--><?php //foreach ($images = $product['images'] as $image): ?>
<!--							<div class="prod_image">-->
<!--								<img src="--><?//= $image; ?><!--" alt="--><?//= $product['name']; ?><!--">-->
<!--							</div>-->
<!--						--><?php //endforeach; ?>
<!--					--><?php //else: ?>
<!--						<div class="prod_image">-->
<!--							<img src="/images/no_photo.png" alt="--><?//= $product['name']; ?><!--">-->
<!--						</div>-->
<!--					--><?php //endif; ?>
<!--					<div class="product_info">-->
<!--						<div>-->
<!--							--><?//= $product['category']; ?>
<!--						</div>-->
<!--						<div>-->
<!--							--><?//= $product['name']; ?>
<!--						</div>-->
<!--						<div>-->
<!--							--><?//= $product['order_count']; ?>
<!--							<span>шт. х</span>-->
<!--							<span>-->
<!--								--><?//= \Application\Helpers\AppHelper::calculatePriceWithCommission($product['price'], $product['commission']); ?>
<!--							</span>-->
<!--							<span class="currency">₽</span>-->
<!--						</div>-->
<!--					</div>-->
<!--				</div>-->
<!--			--><?php //endforeach; ?>
<!--		</div>-->
<!--	--><?php //else: ?>
<!--		<div class="no_items">-->
<!--			Продуктов связанных с заказом не найдено-->
<!--		</div>-->
<!--	--><?php //endif; ?>
<!--	-->
<!--	<div class="order_delivery">-->
<!--		<div class="title">-->
<!--			<h3>Стоимость доставки</h3>-->
<!--			<span>--><?//= $order['delivery_price']; ?><!--</span>-->
<!--			<span class="currency">₽</span>-->
<!--			<p class="note">В черте города</p>-->
<!--		</div>-->
<!--	</div>-->
<!--	-->
<!--	--><?php //if ( $order_address && count($order_address) > 0 ): ?>
<!--		<div class="order_address">-->
<!--			<div class="title">-->
<!--				<h3>-->
<!--					Данные получателя-->
<!--				</h3>-->
<!--			</div>-->
<!--			<div>-->
<!--				<span>--><?//= $order_address['delivery_last_name']; ?><!--</span>-->
<!--				<span>--><?//= $order_address['delivery_name']; ?><!--</span>-->
<!--				<span>--><?//= $order_address['patronymic']; ?><!--</span>-->
<!--			</div>-->
<!--			<div>-->
<!--				<span>--><?//= $order_address['country']; ?><!--</span>-->
<!--				<span>--><?//= $order_address['city']; ?><!--</span>-->
<!--				--><?php //if ( $order_address['region'] ): ?>
<!--					<span>--><?//= $order_address['region']; ?><!--</span>-->
<!--				--><?php //endif; ?>
<!--			</div>-->
<!--			<div>-->
<!--				<span>--><?//= $order_address['street']; ?><!--</span>-->
<!--				<span>--><?//= $order_address['house_number']; ?><!--</span>-->
<!--				--><?php //if ( $order_address['apartment'] ): ?>
<!--					<span>--><?//= $order_address['apartment']; ?><!--</span>-->
<!--				--><?php //endif; ?>
<!--			</div>-->
<!--			<div>-->
<!--				<span>--><?//= $order_address['post_index']; ?><!--</span>-->
<!--			</div>-->
<!--			<div>-->
<!--				<span>--><?//= $order_address['phone']; ?><!--</span>-->
<!--			</div>-->
<!--		</div>-->
<!--	--><?php //else: ?>
<!--		<div class="no_address">-->
<!--			Адреса связанного с ордером не найдено-->
<!--		</div>-->
<!--	--><?php //endif; ?>
<!--</div>-->