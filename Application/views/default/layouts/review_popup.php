<?php $model = new \Application\Models\Review(); ?>
<div class="review_popup">
	<div class="review_messages"></div>
	<form action="/review/add" method="post" class="review_form">
		<div class="form_field">
			<div>Оценка</div>
			<label for="rating1">1</label>
            <input type="radio" name="rating" id="rating1" value="1">

			<label for="rating2">2</label>
            <input type="radio" name="rating" id="rating2" value="2">

			<label for="rating3">3</label>
            <input type="radio" name="rating" id="rating3" value="3">

			<label for="rating4">4</label>
            <input type="radio" name="rating" id="rating4" value="4">

			<label for="rating5">5</label>
            <input type="radio" name="rating" id="rating5" value="5">

		</div>
		<div class="form_field">
			<label>Достоинства</label>
			<textarea name="accomplishments" required></textarea>
		</div>
		<div class="form_field">
			<label>Недостатки</label>
			<textarea name="limitations" required></textarea>
		</div>
		<div class="form_field">
			<label>Рекомендации</label>
			<textarea name="recommendations" required></textarea>
		</div>
		<div class="form_field">
			<label>Текст отзыва</label>
			<textarea name="comment" required></textarea>
		</div>
		<div class="button_group">
			<input type="submit" value="Отправить" id="send_review" data-id="<?= $product['id']; ?>">
		</div>
		<input type="hidden" name="_csrf" value="<?= $model->getCSRF(); ?>">
	</form>
</div>