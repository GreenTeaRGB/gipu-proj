<div class="question_item">
	<div class="question_name faq-list__item faq-list__item_marker-image can-open">
		<span><?= $question['name']; ?></span>
	</div>
	<?php if (isset($question['text']) && $question['text']): ?>
		<div class="question_text faq-list-last__item">
			<?= $question['text']; ?>
		</div>
	<?php endif; ?>
	<?php if ( isset($question['children']) && count($question['children']) > 0 ): ?>
		<?php foreach ($question['children'] as $key => $child): ?>
			
			<?php $this->block('layouts/question', ['question' => $child]); ?>
		<?php endforeach; ?>
	<?php endif; ?>
</div>