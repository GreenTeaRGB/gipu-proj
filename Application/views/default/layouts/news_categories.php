<?php
	$newsCategories = \Application\Models\NewsCategory::find();
?>
<?php if ( count($newsCategories) > 0 ): ?>
	<div class="news_categories">
		<ul>
			<?php foreach ($newsCategories as $newsCategory): ?>
				<li>
					<a href="/news/<?= $newsCategory['alias']; ?>"><?= $newsCategory['name']; ?></a>
				</li>
			<?php endforeach; ?>
		</ul>
	</div>
<?php endif; ?>