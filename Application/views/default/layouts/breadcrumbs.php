<?php if(isset($this->breadcrumbs) && count($this->breadcrumbs) > 0) : ?>
  <p class="page-path"><a href="/">Главная</a>
  <?php foreach( $this->breadcrumbs as $breadcrumb ) : ?>
    <?php if(is_array($breadcrumb) && isset($breadcrumb['label']) && isset($breadcrumb['url'])): ?>
      / <a href="<?=$breadcrumb['url']?>"><?=$breadcrumb['label']?></a>
      <?php elseif(is_string($breadcrumb)) :?>
        / <?= $breadcrumb ?>
      <?php endif;?>
  <?php endforeach; ?>
  </p>
<?php endif; ?>