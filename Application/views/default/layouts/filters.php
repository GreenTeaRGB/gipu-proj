<span class="rolled">Цена, руб.</span>
<div class="value-slider">
  <div class="vals">
    <i><input type="text" name="price[min]" data-val="<?=(int)$min?>" data-prev="<?=isset($_GET['price']['min'])?(int)$_GET['price']['min']:(int)$min?>" data-prev=" " class="only-number min" maxlength="8"></i>
    <i><input type="text" name="price[max]" data-val="<?=(int)$max?>" data-some="<?=isset($_GET['price']['max'])?(int)$_GET['price']['max']:(int)$max?>" data-some=" " class="only-number max" maxlength="8"></i>
  </div>
  <span class="bg-low">
    <span class="reg reg-1"></span>
    <span class="reg reg-2"></span>
    <span class="bg"></span>
  </span>
</div>
<input type="checkbox" id="onsale" name="onsale"><label for="onsale">В продаже</label>
<?php foreach( $properties as $property ):?>
    <?=$this->block('layouts/template_filters/'.$property['template_filter'], ['property'=>$property])?>
<?php endforeach; ?>
<div>
  <input type="submit" value="Применить" class="btn btn_red">
  <a href="<?=$_SERVER['REDIRECT_URL']?>" class="red-stroke btn_clear-form">Очистить</a>
</div>