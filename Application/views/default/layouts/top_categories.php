<?php $categories = \Application\Models\Category::findTree();?>
<nav class="container-fluid categories" id="categories">
  <div class="header__mobile mobile-menu mobile-menu_current" id="mobile-menu">
    <ul class="categories__list">
      <h3>Меню</h3>
      <li><i></i><a href="&categories-1">Категории товаров</a></li>
      <li><i></i><a href="/catalog.html">Каталог услуг</a></li>
      <li><i></i><a href="/advertisements.html">Барахолка</a></li>
      <li><i></i><a href="/for_agents.html">Агентам</a></li>
      <li><i></i><a href="/for_providers.html">Поставщикам</a></li>
      <li><i></i><a href="/about.html">О компании</a></li>
      <li><i></i><a href="/faq.html">FAQ</a></li>
      <li><i></i><a href="/login.html">Вход</a></li>
    </ul>
  </div>
  <div class="container mobile-menu" id="categories-1">
    <ul class="categories__list">
      <h3><a href="&mobile-menu"></a>Категории</h3>
      <?php foreach( $categories[0] as $category ) : ?>
        <li>
          <i></i>
          <a href="/category/<?= $category['alias'] ?>"><?= $category['name'] ?></a>
          <?php if( isset( $categories[$category['id']] ) ) : ?>
            <ul>
              <?php foreach( $categories[$category['id']] as $parent ):  ?>
                <li data-link="/category/<?= $parent['alias'] ?>"
                    data-img="<?= $parent['category_image'] ?>"><?= $parent['name'] ?></li>
              <?php endforeach; ?>
            </ul>
          <?php endif; ?>
        </li>
      <?php endforeach; ?>
    </ul>
  </div>
  <div class="container-fluid categories__wrap mobile-menu" id="category-list">
    <div class="container">
      <h3><a href="&mobile-menu"></a></h3>
      <ul class="categories__category" id="category-list-inner">

      </ul>
    </div>
  </div>
</nav>