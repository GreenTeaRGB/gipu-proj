<?php
use Application\Models\Contact;
$contact = Contact::get();
?>
<footer class="container-fluid footer">
  <div class="container">
    <div class="col-md-3 col-sm-5 about">
      <img src="/media/images/logo_lighten.png" alt="gipu.ru">
      <p class="desc-text desc-text_small">Аренда дизельного полноприводного автомобиля на 5 суток обошлась нам в 27 тысяч рублей. Бронировали за 4 месяца до путешествия на местном сайте.</p>
      <p class="desc-text desc-text_small mt">
        © 2018  ООО «Gipuru»
      </p>
    </div>
    <div class="col-md-7 col-sm-12 hide-xs">
      <ul class="list">
        <h3>Покупателям</h3>
        <li><a href="#!">Как выбрать товар</a></li>
        <li><a href="#!">Обратная связь</a></li>
        <li><a href="#!">Помощь по сервису</a></li>
        <li><a href="#!">Оставить отзыв</a></li>
      </ul>
      <ul class="list">
        <h3>Агентам</h3>
        <li><a href="/user/dashboard">Войти в личный кабинет</a></li>
        <li><a href="/registration_agent.html">Заказать магазин</a></li>
        <li><a href="#!">Популярные вопросы</a></li>
        <li><a href="#!">Заказать обратный звонок</a></li>
      </ul>
      <ul class="list">
        <h3>Поставщикам</h3>
        <li><a href="/user/dashboard">Войти в личый кабинет</a></li>
        <li><a href="/user/register">Подключиться к нам</a></li>
        <li><a href="#!">Популярные вопросы</a></li>
        <li><a href="#!">Заказать обратный звонок</a></li>
      </ul>
    </div>
    <div class="col-sm-5 col-xs-6 d_n about_second">
      <img src="build/img/logo_lighten.png" alt="gipu.ru">
      <p class="desc-text desc-text_small">Аренда дизельного полноприводного автомобиля на 5 суток обошлась нам в 27 тысяч рублей. Бронировали за 4 месяца до путешествия на местном сайте.</p>
      <p class="desc-text desc-text_small mt">
        © <?=date('Y')?>  ООО «Gipuru»
      </p>
    </div>
    <div class="col-md-2 col-sm-6 col-xs-6 footer__contacts">
      <p>Контакнтый центр</p>
      <a href="tel:<?=$contact['phone']?>"><?=$contact['phone']?></a>
      <p>Служба поддержки</p>
      <a href="tel:<?=$contact['support_phone']?>"><?=$contact['support_phone']?></a>
    </div>
  </div>
</footer>