<?php
$this->breadcrumbs[] = ['label' => 'Новости', 'url' => '/news'];
$this->breadcrumbs[] = $news_item['name'];
?>
<section class="contaner-fluid">
  <div class="container news-page">
    <?= $this->block( 'layouts/breadcrumbs' ) ?>
    <h1>	<?= $news_item['name']; ?></h1>
    <p class="desc-text">
      <img class="lazy" src="<?= $news_item['image']; ?>" alt="<?= $news_item['name']; ?>">
      <?= $news_item['description']; ?>
    </p>
    <p class="date-time"><?= $news_item['created_at']; ?></p>
  </div>
</section>
