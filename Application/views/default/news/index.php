<?php
$this->breadcrumbs[] = 'Новости';
?>
<section class="contaner-fluid">
    <div class="container">
        <div class="news">
          <?= $this->block( 'layouts/breadcrumbs' ) ?>
            <h1>
                Новости
            </h1>
            <div>
              <?php foreach( $news_categories as $news_category ) : ?>
                  <a href="/news/<?= $news_category['alias'] ?>"
                     class="red-stroke <?= strpos($_SERVER['REQUEST_URI'], '/news/'.$news_category['alias']) !== false
                         ? "red-stroke_current"
                         : '' ?>red-stroke_current"><?= $news_category['name'] ?></a>
              <?php endforeach; ?>
            </div>
          <?php foreach( $news as $newsItem ): ?>
              <div class="news__item news__item_date">
                  <img alt="news_1" class="lazy" src="<?= $newsItem['image']; ?>" style="">
                  <i class="date-time"><?= $newsItem['created_at']; ?></i>
                  <a href="/news/show/<?= $newsItem['alias']; ?>">
                      <span class="sub-name"><?= $newsItem['name']; ?></span>
                      <p class="desc-text"> <?= $newsItem['short_description']; ?> </p>
                  </a>
              </div>
          <?php endforeach; ?>
          <?= $pagination; ?>
        </div>

    </div>
</section>