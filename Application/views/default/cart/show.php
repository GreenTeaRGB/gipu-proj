<?php
$this->breadcrumbs[] = 'Корзина';
?>
<section class="container basket" id="basket-page">
    <div>
      <?=$this->block('layouts/breadcrumbs')?>
        <h1>Ваша корзина</h1>
        <div class="col-lg-8 col-md-7">
            <?php if($products && count($products) > 0): ?>
            <div class="wrap">
                <ul class="list basket__list" ref="basketList">
                    <?php foreach( $products as $product ) :?>
                        <li class="basket__product">
                            <div class="col-md-6">
                                <a href="/catalog/product/<?=$product['product']['alias']?>" class="link" data-id="<?=$product['product']['id']?>">
                                    <img src="<?=$product['product']['images'][0]?>" class="img" alt="img">
                                    <div class="inner">
                                        <span class="sub-text category"><?=$product['category']?></span>
                                        <p class="name"><?=$product['product']['name']?></p>
                                    </div>
                                </a>
                            </div>
                            <div class="col-md-6">
                                <b class="ruble price"><?=$product['product']['full_price']?></b>
                                <div class="number">
                                    <button>&#8211;</button>
                                    <input disabled type="number" min="1" max="5000" value="<?=$product['count']?>">
                                    <button>+</button>
                                </div>
                                <b class="ruble price-total"><?=($product['product']['full_price'] * $product['count'])?></b>
                                <button class="delete"><i class="icon-delete"></i></button>
                            </div>
                        </li>
                    <?php endforeach; ?>
                </ul>
                <!-- Прошу не кидаться в меня камнями за вьюшку здесь. Господин дизайнер нарисовал такое чудо, воплощая которое обычными средствами js, я умру прямо на рабочем месте. -->
                <!-- Принцип работы следующий: в верхний ul из php Вы помещаете все данные по аналогии с примером, а вьюшка, в свою очередь, посредством своей магии переносит все данные в нижний ul, после чего все работает, как и задумал дизайнер. -->
                <!-- Большая просьба не удалять нижний ul, иначе ничего работать не будет -->
                <ul class="list basket__list">
                    <li v-for="(product, index) in products" class="basket__product">
                        <div class="col-lg-6 col-xs-12 prices">
                            <a :href="product.link">
                                <img :src="product.img" alt="img">
                                <div class="inner">
                                    <span class="sub-text">{{product.category}}</span>
                                    <p>{{product.name}}</p>
                                </div>
                            </a>
                        </div>
                        <div class="col-lg-6 col-xs-12 prices">
                            <b class="ruble price">{{product.price}}</b>
                            <div class="number">
                                <button @click="minMax(product, -1)">&#8211;</button>
                                <input disabled v-model.number="product.count" type="number" min="1" max="5000" value="1">
                                <button @click="minMax(product, +1)">+</button>
                            </div>
                            <b class="ruble price-total">{{product.count * product.price}}</b>
                            <button @click="del(index)" class="delete"><i class="icon-delete"></i></button>
                        </div>
                    </li>
                </ul>
                <!-- VUE END -->
                <div v-if="lastDeleted != null" class="basket__deleted">
                    <span>Вы удалили <a :href="lastDeleted.link" target="_blank">{{lastDeleted.name}}</a></span><button @click="undelete" class="btn btn_white">Отменить</button>
                </div>
            </div>
            <?php else: ?>
                Корзина пуста
            <?php endif; ?>
            <h2 class="to-hide">С этим товаром так же покупают</h2>
            <div class="col-md-12 to-hide">
                <div class="corousel corousel_small_circle corousel_static corousel_viewed">
                    <div class="col-sm-3 col-xs-6 product">
                        <div class="product__options">
                            <a href="#!"><i class="icon-like"></i></a>
                            <a href="#!"><i class="icon-stats"></i></a>
                        </div>
                        <a href="#!">
                            <div class="lazy product__photo" data-src="build/img/products/clothes/product_2.png"></div>
                            <i class="sub-text">Гаджеты</i>
                            <p>Смартфон Samsung Galaxy S9+ 64GB</p>
                            <b class="price ruble">2900</b>
                        </a>
                    </div>
                    <div class="col-sm-3 col-xs-6 product">
                        <div class="product__options">
                            <a href="#!"><i class="icon-like"></i></a>
                            <a href="#!"><i class="icon-stats"></i></a>
                        </div>
                        <a href="#!">
                            <div class="lazy product__photo" data-src="build/img/products/clothes/product_3.png"></div>
                            <i class="sub-text">Гаджеты</i>
                            <p>Смартфон Samsung Galaxy S9+ 64GB</p>
                            <b class="price ruble">2900</b>
                        </a>
                    </div>
                    <div class="col-sm-3 col-xs-6 product">
                        <div class="product__options">
                            <a href="#!"><i class="icon-like"></i></a>
                            <a href="#!"><i class="icon-stats"></i></a>
                        </div>
                        <a href="#!">
                            <div class="lazy product__photo" data-src="build/img/products/clothes/product_4.png"></div>
                            <i class="sub-text">Гаджеты</i>
                            <p>Смартфон Samsung Galaxy S9+ 64GB</p>
                            <b class="price ruble">2900</b>
                        </a>
                    </div>
                    <div class="col-sm-3 col-xs-6 product">
                        <div class="product__options">
                            <a href="#!"><i class="icon-like"></i></a>
                            <a href="#!"><i class="icon-stats"></i></a>
                        </div>
                        <a href="#!">
                            <div class="lazy product__photo" data-src="build/img/products/clothes/product_5.png"></div>
                            <i class="sub-text">Гаджеты</i>
                            <p>Смартфон Samsung Galaxy S9+ 64GB</p>
                            <b class="price ruble">2900</b>
                        </a>
                    </div>
                    <div class="col-sm-3 col-xs-6 product">
                        <div class="product__options">
                            <a href="#!"><i class="icon-like"></i></a>
                            <a href="#!"><i class="icon-stats"></i></a>
                        </div>
                        <a href="#!">
                            <div class="lazy product__photo" data-src="build/img/products/clothes/product_6.png"></div>
                            <i class="sub-text">Гаджеты</i>
                            <p>Смартфон Samsung Galaxy S9+ 64GB</p>
                            <b class="price ruble">2900</b>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-5 col-sm-8 col-xs-12 basket__valid_wrap">
            <?php if($products && count($products) > 0): ?>
              <div class="basket__valid">
                <div class="row gift">
                  <div class="col-xs-3">
                    <span class="gift__img"></span>
                  </div>
                  <div class="col-xs-9 gift__text">
                    <p>Вводите <b>промокоды</b> для получения <i>специальных бонусов</i> к вашему заказу</p>
                  </div>
                </div>
                <p>У Вас есть промокод? <span v-if="badCode">Промокод не обнаружен</span></p>
                <input type="text" placeholder="xxx-xxx-xxx-xxx" class="promo_code" ref='promocode'>
                <span @click="checkCode">Активировать</span>
                <div class="total">
                  <p>Товары: ({{products.length}}): <span class="ruble">{{totalPrice}}</span></p>
                  <p>Доставка: <span class="ruble"><?=\Application\Helpers\DataHelper::DEFAULT_DELIVERY_PRICE?></span></p>
                  <p v-if="discount > 0">Скидка: <span class="ruble">{{discount}}</span></p>
                  <b>Итого: <span class="ruble">{{ totalPrice + <?=\Application\Helpers\DataHelper::DEFAULT_DELIVERY_PRICE?> - discount }}</span></b>
                </div>
                <button class="button checkout" onclick="location.href='/checkout'; return false;">Оформить заказ <i class="icon-arrow-right"></i></button>
              </div>
            <?php endif; ?>
        </div>
    </div>
</section>
<script src="https://unpkg.com/flickity@2/dist/flickity.pkgd.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery.lazy/1.7.9/jquery.lazy.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
