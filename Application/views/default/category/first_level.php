<?php
  $this->breadcrumbs = $breadcrumbs;
?>
<section class="contaner-fluid">
    <div class="container">

        <div class="catalog_level1">
           <?=$this->block('layouts/breadcrumbs')?>
            <h1>
                <?=$category['name']?>
            </h1>
            <div class="col-lg-2 col-md-3 col-sm-4 catalog__filters" id="sort-menu">
                <div class="form">

                    <?php foreach( $category['child'] as $cat ) :?>
                        <span class="title"><?=$cat['name']?></span>
                      <?php if(isset($cat['child'])): ?>
                        <ul class="list list_filter limit-height">
                            <?php foreach( $cat['child'] as $child ): ?>
                              <li><a href="/category/<?= $child['alias'] ?>"><?=$child['name']?></a></li>
                            <?php endforeach; ?>
                        </ul>
                      <?php endif; ?>
                    <?php endforeach; ?>
                </div>
            </div>

            <div class="col-lg-10 col-md-9 col-sm-8">
                <?php if(isset($mainVisibleCategoryID[0])): 
                  $cat = $categories[$mainVisibleCategoryID[0]];
                  ?>
                <div class="col-lg-4 col-md-6">
                    <a href="/category/<?=$cat['alias']?>">
                        <div class="top-product swimsuit">
                            <img src="<?=$cat['category_image']->thumb(300, 345)?>" alt="">
                            <h3>
                                <?=$cat['name']?>
                            </h3>
                        </div>
                    </a>
                </div>
                <?php endif; ?>
                <?php if(isset($mainVisibleCategoryID[1]) && isset($mainVisibleCategoryID[2])):
                  $catFirst = $categories[$mainVisibleCategoryID[1]];
                  $catSecond = $categories[$mainVisibleCategoryID[2]];
                  ?>
                <div class="col-lg-4 col-md-6">
                    <a href="/category/<?=$catFirst['alias']?>">
                        <div class="top-product shoes">
                            <img src="<?=$catFirst['category_image']->thumb(200, 100)?>" alt="">
                            <h3>
                              <?=$catFirst['name']?>
                            </h3>
                        </div>
                    </a>
                    <a href="/category/<?=$catSecond['alias']?>">
                        <div class="top-product swimsuit">
                            <img src="<?=$catSecond['category_image']->thumb(200, 100)?>" alt="">
                            <h3>
                              <?=$catSecond['name']?>
                            </h3>
                        </div>
                    </a>
                </div>
                <?php endif; ?>
                <?php if(isset($mainVisibleCategoryID[3])):
                  $cat = $categories[$mainVisibleCategoryID[3]];
                  ?>
                <div class="col-lg-4 col-md-12">
                    <a href="/category/<?=$cat['alias']?>">
                        <div class="top-product dress">
                            <img src="<?=$cat['category_image']->thumb(300, 345)?>" alt="">
                            <h3>
                              <?=$cat['name']?>
                            </h3>
                        </div>
                    </a>
                </div>
                <?php endif; ?>
                  <?php
                    foreach( $mainVisibleCategoryIDProduct as $id ) :?>
                      <div class="col-lg-12">
                          <h2>
                              <?=$categories[$id]['name']?>
                          </h2>
                          <div class="corousel corousel_small_circle corousel_static corousel_viewed">
                              <?php
                                if(isset($products[$categories[$id]['id']]))
                                  foreach( $products[$categories[$id]['id']] as $product ) :
                                ?>
                                    <?php $this->block('layouts/product', ['product' => $product]); ?>
                               <?php endforeach; ?>

                          </div>
                      </div>
                  <?php endforeach; ?>

          
<div class="row"><div class="col-md-12"><?=$this->block('layouts/brands')?></div></div>
            </div>

        </div>
    </div>
</section>