<section class="contaner-fluid search-results">
  <div class="search-results-top">
    <div class="container">
      <?php if(count($products) == 0): ?>
      <div class="col-lg-8 col-md-8 custom">
        <div class="search-results__text">
          <i class="icon icon-smile"></i>
          <h1>
            По вашему запросу ничего не найдено
          </h1>
          <p>
            Попробуйте сократить запрос или задать его по-другому. Убедитесь, что название бренда и модели написано
            правильно.
          </p>
          <a href="/catalog" class="btn">
            Перейти в католог
          </a>
          <button class="btn">
            Написать нам
          </button>
        </div>
      </div>
        <div class="search-results-bottom">
          <div class="container">
            <?php if(count($recomended) > 0): ?>
              <h2>
              Возможно, вас заинтересует
            </h2>
            <div class="col-lg-12 custom">
              <div>
                <?php foreach( $recomended as $recomend ) : ?>
                  <?=$this->block('layouts/product', ['product' => $recomend, 'class' => 'col-lg-2 col-sm-3 col-xs-6'])?>
                <?php endforeach; ?>
              </div>
            </div>
              <?php endif; ?>
          </div>
        </div>
      <?php else: ?>
        <?php foreach( $products as $product ) :?>
      <div class="col-lg-12 custom">
        <div>
          <?=$this->block('layouts/product', ['product' => $product, 'class' => 'col-lg-2 col-sm-3 col-xs-6'])?>
        </div>
      </div>
        <?php endforeach; ?>
      <?php endif; ?>
    </div> <?=$pagination?>

  </div>

  </div>
</section>