<?php
  $this->breadcrumbs[] = 'Каталог';
//  $rootCategories = array_shift($categories);
//	echo '<pre>';
//	var_dump($categories);
//	die();
?>

<section class="contaner-fluid">
    <div class="container">
        <div class="catalog_level1">
            <?=$this->block('layouts/breadcrumbs')?>
            <h1>
                Каталог
            </h1>
            <div class="col-lg-2 col-md-3 col-sm-4 catalog__filters" id="sort-menu">
                <div class="form">
                    <?php foreach($categories  as $category ): ?>
							<span class="title"><?=$category['name']?></span>
							<ul class="list list_filter limit-height">
								<?php
								  if(isset($categories['id'])): ?>
									<?php foreach( $categories['id'] as $parentCategory ) : ?>
										<li><a href="/category/<?= $parentCategory['alias'] ?>"><?=$parentCategory['name']?></a></li>
									<?php endforeach; ?>
								<?php endif; ?>
							</ul>
                    <?php endforeach; ?>
                </div>
            </div>
			
            <div class="col-lg-10 col-md-9 col-sm-8">
                <div class="col-lg-4 col-md-6">
                    <a href="#!">
                        <div class="top-product swimsuit">
                            <img src="/build/img/products/clothes/swimsuit.jpg" alt="">
                            <h3>
                                Купальники
                            </h3>
                        </div>
                    </a>

                </div>
                <div class="col-lg-4 col-md-6">
                    <a href="#!">
                        <div class="top-product shoes">
                            <img src="/build/img/products/clothes/shoes.jpg" alt="">
                            <h3>
                                Кроссовки
                            </h3>
                        </div>
                    </a>
                    <a href="#!">
                        <div class="top-product">
                            <img src="/build/img/products/clothes/belt.jpg" alt="">
                            <h3>
                                Ремни
                            </h3>
                        </div>
                    </a>
                </div>
                <div class="col-lg-4 col-md-12">
                    <a href="#!">
                        <div class="top-product dress">
                            <img src="/build/img/products/clothes/dress.jpg" alt="">
                            <h3>
                                Платья
                            </h3>
                        </div>
                    </a>
                </div>
	
	            <?php foreach($categories  as $category ): ?>
				<div class="col-lg-12">
					<h2><?=$category['name']?></h2>
					<div class="corousel corousel_small_circle corousel_static corousel_viewed">
			            <?php
				            if(isset($category['id'])): ?>
					            <?php foreach( $categories as $parentCategory ) : ?>
									<?php if ($parentCategory['parent_id'] == $category['id']):?>
										<div class="col-sm-3 col-xs-6 product">
											<div class="product__options">
												<a href="#!"><i class="icon-like"></i></a>
												<a href="#!"><i class="icon-stats"></i></a>
											</div>
											<a href="/category/<?=$parentCategory['alias']?>">
												<div class="lazy product__photo" data-src="<?=$parentCategory['category_image']?>"></div>
												<i class="sub-text"><?=$category['name']?></i>
												<p><?=$parentCategory['name']?></p>
											</a>
										</div>
										<?php endif; ?>
					            <?php endforeach; ?>
				            <?php endif; ?>
					</div>
				</div>
	            <?php endforeach; ?>

				<div class="row"><div class="col-md-12"><?=$this->block('layouts/brands')?></div></div>

            </div>
        </div>

    </div>
</section>


<!--<div class="categories_wrap">-->
<!--	Список категорий-->
<!--	--><?php //if (count($categories) > 0): ?>
<!--		--><?php //foreach ($categories as $category): ?>
<!--		<div class="category_item">-->
<!--			<a href="/category/--><?//= $category['alias']; ?><!--">-->
<!--				--><?//= $category['name']; ?>
<!--			</a>-->
<!--		</div>-->
<!--		--><?php //endforeach; ?>
<!--	--><?php //else: ?>
<!--		<div>-->
<!--			Нет категорий-->
<!--		</div>-->
<!--	--><?php //endif; ?>
<!--</div>-->