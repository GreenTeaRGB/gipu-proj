<?php
    use \Application\Classes\Session;
  $sorts = Session::getByKey('sort');
  $count = Session::getByKey('count');
  $this->setSeo( $category );
  $this->breadcrumbs = $breadcrumbs;
//  $this->breadcrumbs[] = [ 'label' => 'Каталог', 'url' => '/catalog' ];
//  $this->breadcrumbs[] = $category['name'];
?>
<section class="container catalog">
    <div class="col-lg-2 col-md-3 col-sm-4 catalog__filters" id="sort-menu">
        <form action="">
            <span><?= $category['name'] ?></span>
            <ul class="list list_filter limit-height">
              <?php foreach( $category['child'] as $parent ): ?>
                  <li><a href="/category/<?= $parent['alias'] ?>" class="<?=($_SERVER['REQUEST_URI'] == '/category/'.$parent['alias'])?'color_accent':''?>"><?= $parent['name'] ?></a></li>
              <?php endforeach; ?>
            </ul>
           <?=$this->block('layouts/filters', ['properties'=>$filter_properties, 'min'=>$min, 'max'=>$max])?>
        </form>
    </div>
    <div class="col-lg-10 col-md-9 col-sm-8">
      <?= $this->block( 'layouts/breadcrumbs' ); ?>
        <h1>Каталог</h1>
        <div class="catalog__window">
            <div class="catalog__menu">
                <div class="d_ib">
                    <p>Сортировка:
                        <!-- При сортировке по ворзрастанию, элементу .red-stroke_current добавляется класс  red-stroke_current_ascending-->
                        <a href="#" data-sort="price" class="red-stroke sort
                        <?= $sorts && $sorts['by'] ? 'red-stroke_current_ascending' : ''?>
                        <?=$sorts && $sorts['order'] == 'price' ? 'red-stroke_current' : ''?>">Цена</a>
                        <a href="#" data-sort="rate" class="red-stroke sort
                        <?= $sorts && $sorts['by'] ? 'red-stroke_current_ascending' : ''?>
                        <?=$sorts && $sorts['order'] == 'rate' ? 'red-stroke_current' : ''?>">Рейтинг</a>
                        <a href="#" data-sort="popular" class="red-stroke sort
                        <?= $sorts && $sorts['by'] ? 'red-stroke_current_ascending' : ''?>
                        <?=$sorts && $sorts['order'] == 'popular' ? 'red-stroke_current' : ''?>">Популярность</a>
                    </p>
                    <a href="#!" class="btn_sort" id="btn-sort">
                        <svg xmlns="http://www.w3.org/2000/svg" version="1.1" id="Layer_1" x="0px" y="0px"
                             viewBox="0 0 511.999 511.999" style="enable-background:new 0 0 511.999 511.999;"
                             xml:space="preserve" width="512" height="512"><g>
                                <g>
                                    <g>
                                        <path d="M510.078,35.509c-3.388-7.304-10.709-11.977-18.761-11.977H20.682c-8.051,0-15.372,4.672-18.761,11.977    s-2.23,15.911,2.969,22.06l183.364,216.828v146.324c0,7.833,4.426,14.995,11.433,18.499l94.127,47.063    c2.919,1.46,6.088,2.183,9.249,2.183c3.782,0,7.552-1.036,10.874-3.089c6.097-3.769,9.809-10.426,9.809-17.594V274.397    L507.11,57.569C512.309,51.42,513.466,42.813,510.078,35.509z M287.27,253.469c-3.157,3.734-4.889,8.466-4.889,13.355V434.32    l-52.763-26.381V266.825c0-4.89-1.733-9.621-4.89-13.355L65.259,64.896h381.482L287.27,253.469z"></path>
                                    </g>
                                </g>
                            </g></svg>
                    </a>
                </div>
                <div class="catalog__menu_right">
                    <a href="#!" class="btn_sort btn_sort_current"><i class="icon-plates"></i></a>
                    <a href="#!" class="btn_sort"><i class="icon-column"></i></a>
                    <p>Кол. на странице:
                        <a href="#" data-count="30" class="count red-stroke <?=$count && $count == 30 ? 'red-stroke_current' : ''?>">30</a>
                        <a href="#" data-count="60" class="count red-stroke <?=$count && $count == 60 ? 'red-stroke_current' : ''?>">60</a>
                        <a href="#" data-count="90" class="count red-stroke <?=$count && $count == 90 ? 'red-stroke_current' : ''?>">90</a>
                    </p>
                </div>
            </div>
            <div class="catalog__products">
              <?php if( count( $products ) > 0 ):
                foreach( $products as $product ):?>
                      <?=$this->block('layouts/product', ['product' => $product])?>
                <?php
                endforeach;

              else: ?>
                <div>
                    В данной категории нет товаров
                </div>
              <?php endif; ?>
            </div>
            <?=$pagination->get()?>
        </div>
        <?=$this->block('layouts/brands')?>
        <?=$this->block('layouts/viewed')?>

    </div>
</section>

<!--<div class="category_wrap">-->
<!--    <div class="title">-->
<!--        <h1>--><?//= $category['name']; ?><!--</h1>-->
<!--    </div>-->
<!---->
<!--    <h3>Список товаров в категории</h3>-->
<!---->
<!--  --><?php //if( count( $products ) > 0 ): ?>
<!--    --><?php //foreach( $products as $product ): ?>
<!--          <div class="product_item">-->
<!--              <div class="button_group">-->
<!--                  <a class="add_to_wishlist --><?php //echo( ( isset( $product['wishlist'] ) && $product['wishlist'] )
//                      ? 'added'
//                      : '' ); ?><!--" data-id="--><?//= $product['id']; ?><!--">Избранное</a>-->
<!--                  <a class="add_to_compare" data-id="--><?//= $product['id']; ?><!--">Сравнить</a>-->
<!--              </div>-->
<!--              <a href="/catalog/product/--><?//= $product['alias']; ?><!--">--><?//= $product['name']; ?><!--</a>-->
<!--              <div class="button_group">-->
<!--                  <div>-->
<!--                      <input type="number" class="product_count"-->
<!--                             min="1"-->
<!--                          --><?php //echo( $product['availability'] > 0
//                              ? 'max="'.$product['availability'].'"'
//                              : '' ); ?>
<!--                             value="1"-->
<!--                      >-->
<!--                  </div>-->
<!--                --><?php //if( $product['price'] ): ?>
<!--                  --><?php //$priceWithCommission = \Application\Helpers\AppHelper::calculatePriceWithCommission( $product['price'], $product['commission'] ); ?>
<!--                    <div class="product_price">-->
<!--							<span>-->
<!--								--><?//= $priceWithCommission; ?>
<!--							</span>-->
<!--                        <span class="currency">₽</span>-->
<!--                    </div>-->
<!--                --><?php //endif; ?>
<!--                  <div>-->
<!--                      <a href="#" class="add_to_cart" data-id="--><?//= $product['id']; ?><!--">Добавить в корзину</a>-->
<!--                  </div>-->
<!--              </div>-->
<!--          </div>-->
<!--    --><?php //endforeach; ?>
<!--  --><?php //else: ?>
<!--      <div>-->
<!--          В данной категории нет товаров-->
<!--      </div>-->
<!--  --><?php //endif; ?>
<!--</div>-->