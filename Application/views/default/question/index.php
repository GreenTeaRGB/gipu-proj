<section class="contaner-fluid">
	<div class="container faq questions_wrap">
		<p class="page-path">
			<a href="#!">Главная</a>
			/ Часто задаваемые вопросы
		</p>
		<h1>
			Часто задаваемые вопросы
		</h1>
		<div class="col-lg-8">
			<form id="add-question" class="search__input search">
				<input type="text" name="s_value" id="q_search" placeholder="Поиск" class="input_search">
				<button type="submit"><i class="icon-search search_btn"></i></button>
			</form>
			<?php if ( $questions && count($questions) > 0 ): ?>
				<div class="questions_container faq-list">
					<?php foreach ($questions as $question): ?>
						<?php $this->block('layouts/question', ['question' => $question]); ?>
					<?php endforeach; ?>
				</div>
			<?php else: ?>
				<div class="no_items">
					Нет вопросов
				</div>
			<?php endif; ?>
		</div>
		<div class="col-lg-4">
			<form class="add_question question-form">
				<h3 class="question-form-heading">
					Задать вопрос в службу поддержки
				</h3>
				<span class="select first">
					<select name="question_type" id="question-type" class="question-select question-select_first" required>
						<option value="0" selected="selected" class="default-option">
							Вид вопроса
						</option>
						<?php if(is_array($questions_type) && count($questions_type) > 0):?>
							<?php foreach ($questions_type as $type): ?>
								<option value="<?= $type['id'];?>">
									<?= $type['name'];?>
								</option>
							<?php endforeach; ?>
						<?php endif;?>
					</select>
				</span>
				
				<span class="select second">
					<select name="question_category" id="question-category" class="question-select" required>
						<option value="0" selected="selected" class="default-option">
							Категория вопроса
						</option>
						<?php if(is_array($questions_category) && count($questions_category) > 0):?>
							<?php foreach ($questions_category as $category): ?>
								<option value="<?= $category['id'];?>">
									<?= $category['name'];?>
								</option>
							<?php endforeach; ?>
						<?php endif;?>
					</select>
				</span>
				<textarea name="question" id="question-text" class="question-text" placeholder="Вопрос"></textarea>
				<div class="form_field">
					<input type="email" name="email" placeholder="Ваш email" required>
				</div>
				<input type="checkbox" id="accept" name="allow" value="1" class="accept-check" required>
				<label for="accept" class="accept-label">
					Согласен с правилами
				</label>
					<input type="submit" class="btn btn_red" value="Задать вопрос">
			</form>
			<div class="rules">
				<h4 class="rules__heading">
					Правила и порядок публикации вопроса
				</h4>
				<p class="rules_text">
					Доставка товаров в другие регионы осуществляется по согласованию с менеджерами интернет магазина по тарифам транспортной компании-партнера (Деловые Линии). Информацию о доставке в конкретный регион вы можете уточнить по телефону: 8(812) 244-65-65. Плюс калькулятор доставки на сайте. Доставка товара до терминала транспортной службы осуществляется бесплатно (в пределах КАД)
				</p>
			</div>
		</div>
</section>

<script src="https://cdnjs.cloudflare.com/ajax/libs/mark.js/8.11.1/jquery.mark.js"></script>
<script>
	$(document).ready(function () {
		$('.questions_wrap').on('click', '.question_item', function () {
		
			var that = $(this);
			
			if($(this).parent().is('.questions_container')){
				$(this).find('span').removeClass('red-stroke');
			}
			if(!$(this).parent().is('.questions_container') && $(this).parent().parent().is('.questions_container'))
			{
				$(this).find('span').toggleClass('red-stroke');
			}
			
			that.toggleClass('_open');
			that.find('>.question_text').slideToggle();
			that.find('>.question_item').each(function (index, elem) {
				$(elem).slideToggle();
			});
			return false;
		});
		
		$('.questions_wrap').on('click', '.question_text', function (event) {
			event.stopPropagation();
			return false;
		});
		
		var questionsBlock = $('.questions_container').clone();
		
		$('.search_btn').on('click', function () {
			var searchInput = $('#q_search');
			if (searchInput.val() != '') {
				$('.questions_container').html(questionsBlock.html());
				$('.questions_container').unmark().mark(searchInput.val());
				
				var highlighted = $('.questions_container').find('mark[data-markjs="true"]');
				highlighted.each(function (index, elem) {
					var element = $(elem);
					openParents(element);
				});
			}
			
			return false;
		});
		
		var openParents = function (elementObject, addOpenedClass = false) {
			
			if (elementObject.find('>.question_text').length > 0) {
				elementObject.find('>.question_text').show();
				elementObject.addClass('_open');
				addOpenedClass = true;
			}
			
			var parent = elementObject.parent().closest('.question_item');
			if (addOpenedClass) {
				parent.addClass('_open');
			}
			if (parent.length > 0) {
				parent.show();
				openParents(parent, addOpenedClass);
			} else {
				elementObject.find('>.question_item').each(function (index, elem) {
					$(elem).show();
				});
			}
		}
	});
</script>