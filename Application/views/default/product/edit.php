<?php

  use Application\Classes\Session;

?>
<div class="dashboard_wrap">
    <div class="messages">
      <?php
        $errors = Session::getByKey( 'error_form' );
        Session::remove( 'error_form' );
        if( $errors && isset( $errors['Product'] ) ) {
          foreach( $errors['Product'] as $field => $error ) {
            echo '<p>'.$field.' - '.$error[0].'</p>';
          }
        }
      ?>
    </div>
  <?php $this->block( 'user/supplier/header_title' ); ?>
  <?php $this->block( 'user/supplier/supplier_header' ); ?>

    <div class="product_form">
        <form action="/product/save<?php echo( ( isset( $product['id'] ) )
            ? '/'.$product['id']
            : '' ); ?>" method="post" enctype="multipart/form-data">
            <div class="form_field">
                <label for="name">Название продукта</label>
                <input type="text" id="name"
                       name="name"
                       value="<?php echo( ( isset( $product['name'] ) )
                           ? $product['name']
                           : '' ); ?>"
                       required>
            </div>

            <div class="form_field">
                <label for="alias">Ссылка продукта</label>
                <input type="text" id="alias"
                       name="alias"
                       value="<?php echo( ( isset( $product['alias'] ) )
                           ? $product['alias']
                           : '' ); ?>"
                       required>
            </div>

            <div class="form_field">
                <label for="meta_title">Мета заголовок</label>
                <input type="text" id="meta_title"
                       name="meta_title"
                       value="<?php echo( ( isset( $product['meta_title'] ) )
                           ? $product['meta_title']
                           : '' ); ?>"
                       required>
            </div>

            <div class="form_field">
                <label for="meta_description">Мета описание</label>
                <input type="text" id="meta_description"
                       name="meta_description"
                       value="<?php echo( ( isset( $product['meta_description'] ) )
                           ? $product['meta_description']
                           : '' ); ?>">
            </div>

            <div class="form_field">
                <label for="meta_keywords">Мета слова</label>
                <input type="text" id="meta_keywords"
                       name="meta_keywords"
                       value="<?php echo( ( isset( $product['meta_keywords'] ) )
                           ? $product['meta_keywords']
                           : '' ); ?>">
            </div>

          <?php if( count( $categories ) > 0 ): ?>
              <div class="form_field">
                  <label for="category_id">Категория</label>
                  <select name="category_id" id="category_id">
                    <?php foreach( $categories as $category ): ?>
                        <option value="<?= $category['id']; ?>"
                            <?php echo( ( isset( $category['id'] ) && isset( $product['category_id'] ) && $category['id'] == $product['category_id'] )
                                ? 'selected'
                                : '' ); ?>>
                          <?= $category['name']; ?>
                        </option>
                    <?php endforeach; ?>
                  </select>
              </div>
              <div class="form_field props_wrap">
                  <div class="title">
                      <h3>Характеристики</h3>
                  </div>
                  <div class="properties"></div>
              </div>
          <?php endif; ?>

            <div class="form_field">
                <label for="sku">Артикул</label>
                <input type="text" name="sku" id="sku" required value="<?php echo( ( isset( $product['sku'] ) )
                    ? $product['sku']
                    : '' ); ?>">
            </div>

            <div class="form_field">
                <label for="price">Цена</label>
                <input type="text" name="price" id="price" required value="<?php echo( ( isset( $product['price'] ) )
                    ? $product['price']
                    : '' ); ?>">
            </div>

            <div class="form_field">
                <label for="images">Изображения для продукта</label>
                <div id="images">
                    <input type="file" name="Product[images][]" multiple="multiple">
                </div>
              <?php if( isset( $product['images'] ) && count( $product['images'] ) > 0 ): ?>
                  <div class="product_images_preview">
                    <?php foreach( $product['images'] as $image ): ?>
                        <div>
                            <img style="max-width: 100px;" src="<?= $image; ?>" alt="">
                        </div>
                    <?php endforeach; ?>
                  </div>
              <?php endif; ?>
            </div>

            <div class="form_field">
                <label for="description">Описание товара</label>
                <textarea name="description" id="description" required><?php echo( ( isset( $product['description'] ) )
                      ? $product['description']
                      : '' ); ?></textarea>
            </div>

            <div class="form_field">
                <label for="original">Оригинал</label>
                <input type="radio" name="original" id="original"
                       value="1" <?php echo( ( isset( $product['original'] ) && $product['original'] )
                    ? 'checked'
                    : '' ); ?>>

                <label for="copy">Реплика</label>
                <input type="radio" name="original" id="copy"
                       value="0" <?php echo( ( isset( $product['original'] ) && $product['original'] )
                    ? ''
                    : 'checked' ); ?>>
            </div>

            <div class="form_field certificates_wrap" style="display: none;">
                <label for="certificate">Сертификаты</label>
                <div id="certificate">
                    <input type="file" name="Product[certificate][]" multiple="multiple" required>
                </div>
              <?php if( isset( $product['certificate'] ) && $product['certificate'] && count( $product['certificate'] ) > 0 ): ?>
                  <div class="product_certificates_preview">
                    <?php foreach( $product['certificate'] as $certificate ): ?>
                        <div>
                            <img width="100px" src="<?= $certificate; ?>" alt="">
                        </div>
                    <?php endforeach; ?>
                  </div>
              <?php endif; ?>
            </div>

            <div class="form_field">
                <label for="availability">Доступен в общем каталоге</label>
                <input type="checkbox" name="availability" id="availability"
                       value="1" <?php echo( ( isset( $product['availability'] ) && $product['availability'] )
                    ? 'checked'
                    : '' ); ?>>
            </div>

            <div class="form_field">
                <label for="commission">Комиссия продукта</label>
                <input type="text" id="commission"
                       name="commission"
                       value="<?php echo( ( isset( $product['commission'] ) )
                           ? $product['commission']
                           : '' ); ?>">
            </div>

            <div class="button_group">
              <?php if( isset( $product['warehouse_id'] ) && $product['warehouse_id'] ): ?>
                  <a href="/user/show-warehouse/<?= $product['warehouse_id']; ?>">Назад</a>
              <?php else: ?>
                  <a href="/user/warehouse-list">Назад</a>
              <?php endif; ?>
                <input type="submit" class="button" value="Сохранить">
            </div>
        </form>
    </div>
</div>

<script>
    var original = $('input[name="original"]');
    var certificates = $('.certificates_wrap');
    var propertiesWrap = $('.properties');

    var propertyValues = <?= ( isset( $product ) && isset( $product['property_values'] ) )
        ? json_encode( $product['property_values'] )
        : '[]'; ?>;
    
    var toggleFields = function () {
        for (var i = 0, length = original.length; i < length; i++) {
            if (original[i].checked) {
                if (original[i].value == 1) {
                    certificates.find('input:hidden').attr("disabled", false);
                    certificates.show();
                }
                else {
                    certificates.hide();
                    certificates.find('input:hidden').attr("disabled", true);
                }

                break;
            }
        }
    };
	
    var generateProperty = function (property) {
        if (property && !jQuery.isEmptyObject(property)) {
            var propertyElement = $('<div />', {
                class: 'property_item'
            });
            
            var content = $('<div />', {
                class: 'property_name',
                text: property.name
            });
            if (property.type == 'L') {
                if (property.hasOwnProperty('property_list') /*&& property.property_list.length > 0*/) {
                    for (var listKey in property.property_list) {
                        if (property.property_list.hasOwnProperty(listKey)) {
                            var enumItem = property.property_list[listKey];
                            var firstChar = enumItem.description.substring(0, 1);
                            if (firstChar == '#') {
                                content.append(
                                    $('<a />', {
                                        class: 'prop_list_item'
                                    }).attr({
                                                'data-prop_id': property.id,
                                                'data-enum': enumItem.id
                                            })
                                      .css("background-color", enumItem.description)
                                      .append(
                                          $('<span />', {
                                              class: 'ok_block'
                                          })
                                      ).append(
                                        $('<input />', {
                                            type: 'hidden',
                                            name: 'property_enum[' + property.id + '][]',
                                        }).attr({
                                                    'disabled': true,
                                                    'data-enum': enumItem.id,
                                                }).val(enumItem.id)
											.prop("required", (property.required == 1 ? true : false))
                                    )
									
                                );
                            } else {
                                content.append(
                                    $('<a />', {
                                        class: 'prop_list_item',
                                        text: enumItem.value
                                    }).attr({
                                                'data-enum': enumItem.id
                                            }).append(
                                        $('<span />', {
                                            class: 'ok_block'
                                        })
                                    ).append(
                                        $('<input />', {
                                            type: 'hidden',
                                            name: 'property_enum[' + property.id + '][]'
                                        }).attr({
                                                    'disabled': true
                                                }).val(enumItem.id)
											.prop("required", (property.required == 1 ? true : false))
                                    )
                                );
                            }

                            for (var pvKey in propertyValues) {
                                if (propertyValues.hasOwnProperty(pvKey)) {
                                    var pv = propertyValues[pvKey];

                                    if (pv.property_id == property.id && pv.value_enum == enumItem.id) {
                                        var prop = content.find('.prop_list_item[data-enum="' + pv.value_enum + '"]').addClass('active');
                                        prop.find('input').attr({ 'disabled': false });
                                    }
                                }
                            }
                        }
                    }
                }
            } else if (property.type == 'F') {
                content.append($('<input>', {
                    type: 'file',
                    class: 'prop_input_value',
                    name: 'PropertyValue[' + property.id + '][value]'+(property.multi == 1 ? '[]':''),
                }));
                content.append(
                    $('<span />', {
                        text: 'Описание'
                    }).append(
                        $('<textarea />', {
                            class: 'prop_textarea_value',
                            name: 'property_value[' + property.id + '][description]'+(property.multi == 1 ? '[]':'')
                        })
                    )
                );
            } else {
                content.append(
                    $('<span />', {
                        text: 'Значение'
                    }).append(
                        $(property.type == 'T'
                              ? '<textarea>'
                              : '<input />', {
                              class: 'prop_input_value',
                              name: 'property_value[' + property.id + '][value]'+(property.multi == 1 ? '[]':''),
                          })
                    )
                );
                if (property.description_field == 1)
                    content.append(
                        $('<span />', {
                            text: 'Описание'
                        }).append(
                            $('<textarea />', {
                                class: 'prop_textarea_value',
                                name: 'property_value[' + property.id + '][description]'+(property.multi == 1 ? '[]':'')
                            })
                        )
                    );

                for (var pvKey in propertyValues) {
                    if (propertyValues.hasOwnProperty(pvKey)) {
                        var pv = propertyValues[pvKey];
                        if (pv.property_id == property.id) {
                            var input = content.find('.prop_input_value').val(pv.value);
                            input.attr('disabled', false);
                            var textarea = content.find('.prop_textarea_value').text(pv.description);
                            textarea.attr('disabled', false);
                        }
                    }
                }
            }
	
            propertyElement.append(content);
			
            if(property.multi == 1){
				content.append(
					$('<input />', {
						type: 'button',
						class: 'button-more',
					}).val("Еще"));
			}

			propertyElement.append(content);
			
            return propertyElement;
        }
        
        
    };

    var getPropertiesByCategoryId = function (categoryId) {
    	var that = $('#category_id');
        var msgContainer = $('.message');

        $.ajax({
                   type: "POST",
                   url: "/product/property-list",
                   dataType: "json",
                   data: {
                       category_id: categoryId,
                       is_ajax: true
                   },
                   beforeSend: function () {
                       that.prop("disabled", true);
                   }
               }).done(function (response) {
            that.prop("disabled", false);

            if (response.hasOwnProperty('success') && response.success && response.hasOwnProperty('properties')) {
                var properties = response.properties;
                propertiesWrap.empty();
                $('.props_wrap').hide();

                for (var propKey in properties) {
                    if (properties.hasOwnProperty(propKey)) {
                    $('.props_wrap').show();
                    var property = properties[propKey];
                    var propertyElement = generateProperty(property);
                    propertiesWrap.append(propertyElement);
                    }
                }
            }
            else {
                var message = $('<div />', {
                                    class: 'message message_error',
                                    text: response.message
                                }
                );

                msgContainer.append(message);

                setTimeout(function () {
                    message.remove();
                }, 3000);
            }
        }).fail(function (response) {
            that.prop("disabled", false);

            var message = $('<div />', {
                                class: 'message message_error',
                                text: response.message
                            }
            );
            msgContainer.append(message);

            setTimeout(function () {
                message.remove()
            }, 3000);
        });
    };

    $(document).ready(function () {
        toggleFields();

        var categoryId = $('#category_id').val();
        getPropertiesByCategoryId(categoryId);
	
        original.on('change', function () {
            toggleFields();
        });

        $('#category_id').on('change', function () {
            var that = $(this);

            var categoryId = that.val();
            getPropertiesByCategoryId(categoryId);

            return false;
        });


        $('.props_wrap').on('click', '.prop_list_item', function () {
            var that = $(this);

            if (that.hasClass('active')) {
                that.removeClass('active');
                that.find('input').attr('disabled', true);
            } else {
                that.addClass('active');
                that.find('input').attr('disabled', false);
            }
        });
        
        $('.props_wrap').on('click', '.button-more', function () {
        	var clon = $(this).parent().parent().clone();
        	var main = $(this).parent().parent();
        	$(main).after(clon);
        	$(main).find('.button-more:last').remove();
		})
        
    });
</script>