<?php


	namespace Application\Validators;

	use \Application\Classes\Application;

	class EmailValidator extends \Application\Validators\Validator
	{

		public $pattern = '/^[a-zA-Z0-9!#$%&\'*+\/=?^_`{|}~-]+(?:\.[a-zA-Z0-9!#$%&\'*+\/=?^_`{|}~-]+)*@(?:[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?$/';

		public $fullPattern = '/^[^@]*<[a-zA-Z0-9!#$%&\'*+\\/=?^_`{|}~-]+(?:\.[a-zA-Z0-9!#$%&\'*+\\/=?^_`{|}~-]+)*@(?:[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?>$/';

		public $allowName = false;

		public $checkDNS = false;

		public $enableIDN = false;

		public function init()
		{
			if( $this->enableIDN && !function_exists( 'idn_to_ascii' ) ) {
				throw new \Exception( 'In order to use IDN validation intl extension must be installed and enabled.' );
			}
			if( $this->message === null ) {
				$this->message = 'Значение «{attribute}» не является правильным email ';
			}
		}


		protected function validateValue( $value )
		{
			if( !is_string( $value ) || strlen( $value ) >= 320 ) {
				$valid = false;
			}
			elseif( !preg_match( '/^(.*<?)(.*)@(.*?)(>?)$/', $value, $matches ) ) {
				$valid = false;
			}
			else {
				$domain = $matches[ 3 ];
				if( $this->enableIDN ) {
					$value = $matches[ 1 ] . idn_to_ascii( $matches[ 2 ] ) . '@' . idn_to_ascii( $domain ) . $matches[ 4 ];
				}
				$valid = preg_match( $this->pattern, $value ) || $this->allowName && preg_match( $this->fullPattern, $value );
				if( $valid && $this->checkDNS ) {
					$valid = checkdnsrr( $domain, 'MX' ) || checkdnsrr( $domain, 'A' );
				}
			}
			return $valid ? null : [ $this->message, [] ];
		}

		public function clientValidateAttribute( $model, $attribute, $view )
		{
			$options = [
				'validator'   => 'email',
				'pattern'     => $this->pattern,
				'fullPattern' => $this->fullPattern,
				'allowName'   => $this->allowName,
				'message'     => Application::format( $this->message, [
					'attribute' => $model->getAttributeLabel( $attribute ),
				] ),
				'enableIDN'   => (bool) $this->enableIDN,
			];
			if( $this->skipOnEmpty ) {
				$options[ 'skipOnEmpty' ] = 1;
			}

			if( $this->whenClient != null ) {
				$options[ 'whenClient' ] = $this->whenClient;
			}
//        ValidationAsset::register($view);
//        if ($this->enableIDN) {
//            PunycodeAsset::register($view);
//        }

//        return 'validation.email(value, messages, ' . json_encode($options) . ');';
			return $options;
		}
	}
