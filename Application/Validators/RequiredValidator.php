<?php

	namespace Application\Validators;

	use \Application\Classes\Application;

	class RequiredValidator extends \Application\Validators\Validator
	{

		public $skipOnEmpty = false;

		public $requiredValue;

		public $strict = false;

		public $message;

		public function init()
		{
			if( $this->message === null ) {
				$this->message = $this->requiredValue === null ? 'Поле {attribute} обязательно для заполнения' : 'Значение должно быть равно';
			}
		}

		protected function validateValue( $value )
		{
			if( $this->requiredValue === null ) {
				if( $this->strict && $value !== null || !$this->strict && !$this->isEmpty( is_string( $value ) ? trim( $value ) : $value ) ) {
					return null;
				}
			}
			elseif( !$this->strict && $value == $this->requiredValue || $this->strict && $value === $this->requiredValue ) {
				return null;
			}
			if( $this->requiredValue === null ) {
				return [ $this->message, [] ];
			}
			else {
				return [
					$this->message,
					[
						'requiredValue' => $this->requiredValue,
					]
				];
			}
		}

		public function clientValidateAttribute( $model, $attribute, $view )
		{
			$options = [];
			if( $this->requiredValue !== null ) {
				$options[ 'message' ]       = Application::format( $this->message, [
					'requiredValue' => $this->requiredValue,
				] );
				$options[ 'requiredValue' ] = $this->requiredValue;
			}
			else {
				$options[ 'message' ] = $this->message;
			}
			if( $this->strict ) {
				$options[ 'strict' ] = 1;
			}

			$options[ 'message' ] = Application::format( $options[ 'message' ], [
				'attribute' => $model->getAttributeLabel( $attribute ),
			] );
			if( $this->whenClient != null ) {
				$options[ 'whenClient' ] = $this->whenClient;
			}
			$options[ 'validator' ] = 'required';
//        ValidationAsset::register($view);

//        return 'validation.required(value, messages, ' . json_encode($options, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE) . ');';
			return $options;
		}
	}
