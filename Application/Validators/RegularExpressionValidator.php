<?php

	namespace Application\Validators;
  use \Application\Classes\Application;
	class RegularExpressionValidator extends \Application\Validators\Validator
	{

		public $pattern;

		public $not = false;

		public function init()
		{
			if( $this->pattern === null ) {
				throw new \Exception( 'The "pattern" property must be set.' );
			}
			if( $this->message === null ) {
				$this->message = 'Неверный формат значения';
			}
		}


		protected function validateValue( $value )
		{
			$valid = !is_array( $value ) && ( !$this->not && preg_match( $this->pattern, $value ) || $this->not && !preg_match( $this->pattern, $value ) );

			return $valid ? null : [ $this->message, [] ];
		}

		public function clientValidateAttribute( $model, $attribute, $view )
		{
			$pattern     = $this->pattern;
			$pattern     = preg_replace( '/\\\\x\{?([0-9a-fA-F]+)\}?/', '\u$1', $pattern );
			$deliminator = substr( $pattern, 0, 1 );
			$pos         = strrpos( $pattern, $deliminator, 1 );
			$flag        = substr( $pattern, $pos + 1 );
			if( $deliminator !== '/' ) {
				$pattern = '/' . str_replace( '/', '\\/', substr( $pattern, 1, $pos - 1 ) ) . '/';
			}
			else {
				$pattern = substr( $pattern, 0, $pos + 1 );
			}
			if( !empty( $flag ) ) {
				$pattern .= preg_replace( '/[^igm]/', '', $flag );
			}

			$options = [
				'validator' => 'regularExpression',
				'pattern'   => $pattern,
				'not'       => $this->not,
				'message'   => Application::format( $this->message, [
					'attribute' => $model->getAttributeLabel( $attribute ),
				] ),
			];
			if( $this->skipOnEmpty ) {
				$options[ 'skipOnEmpty' ] = 1;
			}
			if( $this->whenClient != null ) {
				$options[ 'whenClient' ] = $this->whenClient;
			}

//        ValidationAsset::register($view);

//        return 'validation.regularExpression(value, messages, ' . json_encode($options) . ');';
			return $options;
		}
	}
