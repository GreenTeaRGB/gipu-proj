<?php

	namespace Application\Validators;

	use \Application\Classes\Application;

	class BooleanValidator extends \Application\Validators\Validator
	{

		public $trueValue = '1';

		public $falseValue = '0';

		public $strict = false;

		public function init()
		{
			if( $this->message === null ) {
				$this->message = '{attribute} must be either "{true}" or "{false}".';
			}
		}

		protected function validateValue( $value )
		{
			$valid = !$this->strict && ( $value == $this->trueValue || $value == $this->falseValue ) || $this->strict && ( $value === $this->trueValue || $value === $this->falseValue );
			if( !$valid ) {
				return [
					$this->message,
					[
						'true'  => $this->trueValue,
						'false' => $this->falseValue,
					]
				];
			}
			else {
				return null;
			}
		}

		public function clientValidateAttribute( $model, $attribute, $view )
		{
			$options = [
				'validator'  => 'boolean',
				'trueValue'  => $this->trueValue,
				'falseValue' => $this->falseValue,
				'message'    => Application::format( $this->message, [
					'attribute' => $model->getAttributeLabel( $attribute ),
					'true'      => $this->trueValue,
					'false'     => $this->falseValue,
				] )
			];

			if( $this->skipOnEmpty ) {
				$options[ 'skipOnEmpty' ] = 1;
			}
			if( $this->strict ) {
				$options[ 'strict' ] = 1;
			}
			if( $this->whenClient != null ) {
				$options[ 'whenClient' ] = $this->whenClient;
			}
//        return 'validation.boolean(value, messages, ' . json_encode($options, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE) . ');';
			return $options;
		}
	}
