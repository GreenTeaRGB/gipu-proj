<?php

	namespace Application\Validators;

	use \Application\Classes\Application;

	class EachValidator extends \Application\Validators\Validator
	{

		public $rule;

		public $allowMessageFromRule = true;

		private $_validator;

		public $min = 1;

		public $max = 1;

		public function init()
		{
			if( $this->message === null ) {
				$this->message = 'Неверный формат значения {attribute}';
			}
			if( $this->min !== null && $this->tooSmall == null ) {
				$this->tooSmall = 'Элементов {attribute} должно быть больше {min} элементов';
			}
			if( $this->max !== null && $this->tooBig === null ) {
				$this->tooBig = 'Значение {attribute} должно быть не больше {max} элементов';
			}
		}

		private function getValidator( $model = null )
		{
			if( $this->_validator === null ) {
				$this->_validator = $this->createEmbeddedValidator( $model );
			}
			return $this->_validator;
		}

		private function createEmbeddedValidator( $model )
		{
			$rule = $this->rule;
			if( $rule instanceof Validator ) {
				return $rule;
			}
			elseif( is_array( $rule ) && isset( $rule[ 0 ] ) ) {
				if( !is_object( $model ) ) {
					throw new \InvalidArgumentException( "Модель не является объектом" );
				}
				return Validator::createValidator( $rule[ 0 ], $model, $this->attributes, array_slice( $rule, 1 ) );
			}
			else {
				throw new InvalidConfigException( 'Invalid validation rule: a rule must be an array specifying validator type.' );
			}
		}

		public function validateAttribute( $model, $attribute )
		{
			$value = $model->$attribute;
			$count = count( $value );
			if( $count > $this->max ) {
				$this->addError( $model, $attribute, $this->tooBig, [ 'attribute' => $attribute, 'max' => $this->max ] );
				return;
			}
			if( $count < $this->min ) {
				$this->addError( $model, $attribute, $this->tooSmall, [ 'attribute' => $attribute, 'min' => $this->min ] );
				return;
			}
			$validator = $this->getValidator( $model );
			if( $validator instanceof FilterValidator && is_array( $value ) ) {
				$filteredValue = [];
				foreach( $value as $k => $v ) {
					if( !$validator->skipOnArray || !is_array( $v ) ) {
						$filteredValue[ $k ] = call_user_func( $validator->filter, $v );
					}
				}
				$model->$attribute = $filteredValue;
			}
			else {
				$this->getValidator( $model );
				parent::validateAttribute( $model, $attribute );
			}
		}

		protected function validateValue( $value )
		{
			if( !is_array( $value ) ) {
				return [ $this->message, [] ];
			}
			$validator = $this->getValidator();
			foreach( $value as $v ) {
				$result = $validator->validateValue( $v );
				if( $result !== null ) {
					return $this->allowMessageFromRule ? $result : [ $this->message, [] ];
				}
			}

			return null;
		}

		public function clientValidateAttribute( $model, $attribute, $view )
		{
			$label = $model->getAttributeLabel( $attribute );

			$options = [
				'validator' => 'each',
				'rule'      => array_merge( $this->rule, [ 'message' => $this->message ] ),
				'message'   => Application::format( $this->message, [
					'attribute' => $label,
				] ),
			];

			if( $this->min !== null ) {
				$options[ 'min' ]      = is_string( $this->min ) ? (float) $this->min : $this->min;
				$options[ 'tooSmall' ] = Application::format( $this->tooSmall, [
					'attribute' => $label,
					'min'       => $this->min,
				] );
			}
			if( $this->max !== null ) {
				$options[ 'max' ]    = is_string( $this->max ) ? (float) $this->max : $this->max;
				$options[ 'tooBig' ] = Application::format( $this->tooBig, [
					'attribute' => $label,
					'max'       => $this->max,
				] );
			}
			if( $this->skipOnEmpty ) {
				$options[ 'skipOnEmpty' ] = 1;
			}
			if( $this->whenClient != null ) {
				$options[ 'whenClient' ] = $this->whenClient;
			}
			//        ValidationAsset::register($view);

			//        return 'validation.number(value, messages, ' . json_encode($options) . ');';
			return $options;
		}
	}
