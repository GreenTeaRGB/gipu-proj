<?php


	namespace Application\Validators;


	class FilterValidator extends \Application\Validators\Validator
	{

		public $filter;

		public $skipOnArray = false;

		public $skipOnEmpty = false;

		public function init()
		{
			if( $this->filter === null ) {
				throw new \Exception( 'The "filter" property must be set.' );
			}
		}

		public function validateAttribute( $model, $attribute )
		{
			$value = $model->$attribute;
			if( !$this->skipOnArray || !is_array( $value ) ) {
				$model->$attribute = call_user_func( $this->filter, $value );
			}
		}

		public function clientValidateAttribute( $model, $attribute, $view )
		{
			if( $this->filter !== 'trim' ) {
				return null;
			}

			$options = [ 'validator' => 'filter' ];
			if( $this->skipOnEmpty ) {
				$options[ 'skipOnEmpty' ] = 1;
			}
			if( $this->whenClient != null ) {
				$options[ 'whenClient' ] = $this->whenClient;
			}
//        ValidationAsset::register($view);

//        return 'value = validation.trim($form, attribute, ' . json_encode($options, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE) . ');';
			return $options;
		}
	}
