<?php

	namespace Application\Validators;

	/**
	 * Class UrlValidator
	 * @package Application\Validators
	 */
	class UrlValidator extends \Application\Validators\Validator
	{

		/**
		 * @var string
		 */
//		public $pattern = '/^([a-z][a-z0-9+.-]*):(?:\\/\\/((?:(?=((?:[a-z0-9-._~!$&\'()*+,;=:]|%[0-9A-F]{2})*))(\\3)@)?(?=(\\[[0-9A-F:.]{2,}\\]|(?:[a-z0-9-._~!$&\'()*+,;=]|%[0-9A-F]{2})*))\\5(?::(?=(\\d*))\\6)?)(\\/(?=((?:[a-z0-9-._~!$&\'()*+,;=:@\\/]|%[0-9A-F]{2})*))\\8)?|(\\/?(?!\\/)(?=((?:[a-z0-9-._~!$&\'()*+,;=:@\\/]|%[0-9A-F]{2})*))\\10)?)(?:\\?(?=((?:[a-z0-9-._~!$&\'()*+,;=:@\\/?]|%[0-9A-F]{2})*))\\11)?(?:#(?=((?:[a-z0-9-._~!$&\'()*+,;=:@\\/?]|%[0-9A-F]{2})*))\\12)?$/i';
		public $pattern = '/^{schemes}:\/\/(([A-Z0-9][A-Z0-9_-]*)(\.[A-Z0-9][A-Z0-9_-]*)+)/i';

		/**
		 * @var array
		 */
		public $validSchemes = [ 'http', 'https' ];

		/**
		 * @var
		 */
		public $defaultScheme;

		/**
		 * @var bool
		 */
		public $enableIDN = false;


		/**
		 * @inheritdoc
		 */
		public function init()
		{
			if( $this->enableIDN && !function_exists( 'idn_to_ascii' ) ) {
				throw new InvalidConfigException( 'In order to use IDN validation intl extension must be installed and enabled.' );
			}
			if( $this->message === null ) {
				$this->message = '{attribute} is not a valid URL.';
			}
		}

		/**
		 * @inheritdoc
		 */
		public function validateAttribute( $model, $attribute )
		{
			$value  = $model->$attribute;
			$result = $this->validateValue( $value );
			if( !empty( $result ) ) {
				$this->addError( $model, $attribute, $result[ 0 ], $result[ 1 ] );
			}
			elseif( $this->defaultScheme !== null && strpos( $value, '://' ) === false ) {
				$model->$attribute = $this->defaultScheme . '://' . $value;
			}
		}

		/**
		 * @inheritdoc
		 */
		protected function validateValue( $value )
		{
			// make sure the length is limited to avoid DOS attacks
			if( is_string( $value ) && strlen( $value ) < 2000 ) {
				if( $this->defaultScheme !== null && strpos( $value, '://' ) === false ) {
					$value = $this->defaultScheme . '://' . $value;
				}

				if( strpos( $this->pattern, '{schemes}' ) !== false ) {
					$pattern = str_replace( '{schemes}', '(' . implode( '|', $this->validSchemes ) . ')', $this->pattern );
				}
				else {
					$pattern = $this->pattern;
				}

				if( $this->enableIDN ) {
					$value = preg_replace_callback( '/:\/\/([^\/]+)/', function( $matches )
					{
						return '://' . idn_to_ascii( $matches[ 1 ] );
					}, $value );
				}

				if( preg_match( $pattern, $value ) ) {
					return null;
				}
			}

			return [ $this->message, [] ];
		}

		/**
		 * @inheritdoc
		 */
		public function clientValidateAttribute( $model, $attribute, $view )
		{
			if( strpos( $this->pattern, '{schemes}' ) !== false ) {
				$pattern = str_replace( '{schemes}', '(' . implode( '|', $this->validSchemes ) . ')', $this->pattern );
			}
			else {
				$pattern = $this->pattern;
			}

			$options = [
				'pattern'   => $pattern,
				'message'   => \Application\Classes\Application::format( $this->message, [
					'attribute' => $model->getAttributeLabel( $attribute )
				] ),
				'enableIDN' => (bool) $this->enableIDN,
				'validator' => 'url'
			];
			if( $this->skipOnEmpty ) {
				$options[ 'skipOnEmpty' ] = 1;
			}
			if( $this->defaultScheme !== null ) {
				$options[ 'defaultScheme' ] = $this->defaultScheme;
			}
			if( $this->whenClient != null ) {
				$options[ 'whenClient' ] = $this->whenClient;
			}

			return $options;
		}
	}
