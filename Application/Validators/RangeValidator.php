<?php

	namespace Application\Validators;

	use \Application\Classes\Application;

	class RangeValidator extends \Application\Validators\Validator
	{

		public $range;

		public $strict = false;

		public $not = false;

		public $allowArray = false;

		public function init()
		{
			if( !is_array( $this->range ) ) {
				throw new \Exception( 'The "range" property must be set.' );
			}
			if( $this->message === null ) {
				$this->message = 'Неверный формат значения';
			}
		}


		protected function validateValue( $value )
		{
			if( !$this->allowArray && is_array( $value ) ) {
				return [ $this->message, [] ];
			}

			$in = true;

			foreach( ( is_array( $value ) ? $value : [ $value ] ) as $v ) {
				if( !in_array( $v, $this->range, $this->strict ) ) {
					$in = false;
					break;
				}
			}

			return $this->not !== $in ? null : [ $this->message, [] ];
		}

		public function clientValidateAttribute( $model, $attribute, $view )
		{
			$range = [];
			foreach( $this->range as $value ) {
				$range[] = (string) $value;
			}
			$options = [
				'validator' => 'range',
				'range'     => $range,
				'not'       => $this->not,
				'message'   => Application::format( $this->message, [
					'attribute' => $model->getAttributeLabel( $attribute ),
				] ),
			];
			if( $this->skipOnEmpty ) {
				$options[ 'skipOnEmpty' ] = 1;
			}
			if( $this->allowArray ) {
				$options[ 'allowArray' ] = 1;
			}

			if( $this->whenClient != null ) {
				$options[ 'whenClient' ] = $this->whenClient;
			}
//        ValidationAsset::register($view);

//        return 'validation.range(value, messages, ' . json_encode($options, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE) . ');';
			return $options;
		}
	}
