<?php

	namespace Application\Validators;

	use \Application\Classes\Application;

	class CaptchaValidator extends \Application\Validators\Validator
	{

		public $skipOnEmpty = false;

		public function init()
		{
			if( $this->message === null ) {
				$this->message = 'The verification code is incorrect.';
			}
		}

		protected function validateValue( $value )
		{
			$valid = isset( $_SESSION[ 'randomnr2' ] ) && md5( $value ) === $_SESSION[ 'randomnr2' ];
			return $valid ? null : [ $this->message, [] ];
		}


		public function clientValidateAttribute( $object, $attribute, $view )
		{

		}
	}
