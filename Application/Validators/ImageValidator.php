<?php

	namespace Application\Validators;

	use \Application\Helpers\UploadedFile;
	use \Application\Classes\Application;

	class ImageValidator extends \Application\Validators\FileValidator
	{
		public $notImage;

		public $minWidth;

		public $maxWidth;

		public $minHeight;

		public $maxHeight;

		public $underWidth;

		public $overWidth;

		public $underHeight;

		public $overHeight;

		public function init()
		{
			parent::init();
			if( $this->notImage === null ) {
				$this->notImage = 'Файл не является изображением.';
			}
			if( $this->underWidth === null ) {
				$this->underWidth = 'Файл слишком маленький. Ширина должна быть более ' . $this->minWidth . ' пикселей'; //$this->underWidth
			}
			if( $this->underHeight === null ) {
				$this->underHeight = 'Файл слишком маленький. Высота должна быть более ' . $this->minHeight; //$this->underHeight
			}
			if( $this->overWidth === null ) {
				$this->overWidth = 'Файл слишком большой. Ширина не должна превышать ' . $this->maxWidth . ' пикселей'; //$this->overWidth
			}
			if( $this->overHeight === null ) {
				$this->overHeight = 'Файл слишком большой. Высота не должна превышать ' . $this->maxHeight . ' пикселей'; //$this->overHeight
			}
		}


		protected function validateValue( $file )
		{
			$result = parent::validateValue( $file );
			return empty( $result ) ? $this->validateImage( $file ) : $result;
		}

		protected function validateImage( $image )
		{
			if( false === ( $imageInfo = getimagesize( $image->tempName ) ) ) {
				return [ $this->notImage, [ 'file' => $image->name ] ];
			}

			list( $width, $height ) = $imageInfo;

			if( $width == 0 || $height == 0 ) {
				return [ $this->notImage, [ 'file' => $image->name ] ];
			}

			if( $this->minWidth !== null && $width < $this->minWidth ) {
				return [ $this->underWidth, [ 'file' => $image->name, 'limit' => $this->minWidth ] ];
			}

			if( $this->minHeight !== null && $height < $this->minHeight ) {
				return [ $this->underHeight, [ 'file' => $image->name, 'limit' => $this->minHeight ] ];
			}

			if( $this->maxWidth !== null && $width > $this->maxWidth ) {
				return [ $this->overWidth, [ 'file' => $image->name, 'limit' => $this->maxWidth ] ];
			}

			if( $this->maxHeight !== null && $height > $this->maxHeight ) {
				return [ $this->overHeight, [ 'file' => $image->name, 'limit' => $this->maxHeight ] ];
			}

			return null;
		}

		public function clientValidateAttribute( $model, $attribute, $view )
		{
//        ValidationAsset::register($view);
			$options                = $this->getClientOptions( $model, $attribute );
			$options[ 'validator' ] = 'image';
//        return 'validation.image(attribute, messages, ' . json_encode($options, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE) . ', deferred);';
			return $options;
		}

		protected function getClientOptions( $model, $attribute )
		{
			$options = parent::getClientOptions( $model, $attribute );
			$label   = $model->getAttributeLabel( $attribute );

			if( $this->notImage !== null ) {
				$options[ 'notImage' ] = Application::format( $this->notImage, [
					'attribute' => $label
				] );
			}

			if( $this->minWidth !== null ) {
				$options[ 'minWidth' ]   = $this->minWidth;
				$options[ 'underWidth' ] = Application::format( $this->underWidth, [
					'attribute' => $label,
					'limit'     => $this->minWidth
				] );
			}

			if( $this->maxWidth !== null ) {
				$options[ 'maxWidth' ]  = $this->maxWidth;
				$options[ 'overWidth' ] = Application::format( $this->overWidth, [
					'attribute' => $label,
					'limit'     => $this->maxWidth
				] );
			}

			if( $this->minHeight !== null ) {
				$options[ 'minHeight' ]   = $this->minHeight;
				$options[ 'underHeight' ] = Application::format( $this->underHeight, [
					'attribute' => $label,
					'limit'     => $this->minHeight
				] );
			}

			if( $this->maxHeight !== null ) {
				$options[ 'maxHeight' ]  = $this->maxHeight;
				$options[ 'overHeight' ] = Application::format( $this->overHeight, [
					'attribute' => $label,
					'limit'     => $this->maxHeight
				] );
			}
			if( $this->whenClient != null ) {
				$options[ 'whenClient' ] = $this->whenClient;
			}

			return $options;
		}
	}
