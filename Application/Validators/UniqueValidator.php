<?php

	namespace Application\Validators;

	class UniqueValidator extends \Application\Validators\Validator
	{

		public $targetClass;

		public $targetAttribute;

		public $filter;

		public function init()
		{
			if( $this->message === null ) {
				$this->message = 'Элемент не уникален';
			}
		}

		public function validateAttribute( $model, $attribute )
		{
			$targetClass     = $this->targetClass === null ? get_class( $model ) : $this->targetClass;
			$targetAttribute = $this->targetAttribute === null ? $attribute : $this->targetAttribute;
			if( is_array( $targetAttribute ) ) {
				$params = [];
				foreach( $targetAttribute as $k => $v ) {
					$params[ $v ] = is_integer( $k ) ? $model->$v : $model->$k;
				}
			}
			else {
				$params = [ $targetAttribute => $model->$attribute ];
			}

			foreach( $params as $value ) {
				if( is_array( $value ) ) {
					$this->addError( $model, $attribute, '{attribute} is invalid.' );
					return;
				}
			}
			if(isset($model->id)){
			  $params['!id'] = $model->id;
      }
			$query = new $targetClass();

			$query->where( $params );

			if( $this->filter instanceof \Closure ) {
				call_user_func( $this->filter, $query );
			}
			elseif( $this->filter !== null ) {
				$query->andWhere( $this->filter );
			}

			if( $query->exists() ) {
				$this->addError( $model, $attribute, $this->message );
			}
		}
	}
