<?php

	namespace Application\Validators;

	use \Application\Classes\Application;

	class StringValidator extends \Application\Validators\Validator
	{

		public $length;

		public $max;

		public $min;

		public $message;

		public $tooShort;

		public $tooLong;

		public $notEqual;

		public $encoding;

		public function init()
		{
			if( is_array( $this->length ) ) {
				if( isset( $this->length[ 0 ] ) ) {
					$this->min = $this->length[ 0 ];
				}
				if( isset( $this->length[ 1 ] ) ) {
					$this->max = $this->length[ 1 ];
				}
				$this->length = null;
			}
			if( $this->encoding === null ) {
				$this->encoding = 'utf-8';
			}
			if( $this->message === null ) {
				$this->message = 'Значение должно быть строкой.';
			}
			if( $this->min !== null && $this->tooShort === null ) {
				$this->tooShort = 'Значение должно содержать минимум ' . $this->min . ' символов';
			}
			if( $this->max !== null && $this->tooLong === null ) {
				$this->tooLong = 'Значение должно содержать максимум ' . $this->min . ' символов';
			}
			if( $this->length !== null && $this->notEqual === null ) {
				$this->notEqual = 'Значение должно содержать ' . $this->min . ' символов';
			}
		}


		public function validateAttribute( $model, $attribute )
		{
			$value = $model->$attribute;

			if( !is_string( $value ) ) {
				$this->addError( $model, $attribute, $this->message );

				return;
			}

			$length = mb_strlen( $value, $this->encoding );

			if( $this->min !== null && $length < $this->min ) {
				$this->addError( $model, $attribute, $this->tooShort, [ 'min' => $this->min ] );
			}
			if( $this->max !== null && $length > $this->max ) {
				$this->addError( $model, $attribute, $this->tooLong, [ 'max' => $this->max ] );
			}
			if( $this->length !== null && $length !== $this->length ) {
				$this->addError( $model, $attribute, $this->notEqual, [ 'length' => $this->length ] );
			}
		}

		/**
		 * @inheritdoc
		 */
		protected function validateValue( $value )
		{
			if( !is_string( $value ) ) {
				return [ $this->message, [] ];
			}

			$length = mb_strlen( $value, $this->encoding );

			if( $this->min !== null && $length < $this->min ) {
				return [ $this->tooShort, [ 'min' => $this->min ] ];
			}
			if( $this->max !== null && $length > $this->max ) {
				return [ $this->tooLong, [ 'max' => $this->max ] ];
			}
			if( $this->length !== null && $length !== $this->length ) {
				return [ $this->notEqual, [ 'length' => $this->length ] ];
			}

			return null;
		}

		/**
		 * @inheritdoc
		 */
		public function clientValidateAttribute( $model, $attribute, $view )
		{
			$label = $model->getAttributeLabel( $attribute );

			$options = [
				'validator' => 'string',
				'message'   => Application::format( $this->message, [
					'attribute' => $label,
				] ),
			];

			if( $this->min !== null ) {
				$options[ 'min' ]      = $this->min;
				$options[ 'tooShort' ] = Application::format( $this->tooShort, [
					'attribute' => $label,
					'min'       => $this->min,
				] );
			}
			if( $this->max !== null ) {
				$options[ 'max' ]     = $this->max;
				$options[ 'tooLong' ] = Application::format( $this->tooLong, [
					'attribute' => $label,
					'max'       => $this->max,
				] );
			}
			if( $this->length !== null ) {
				$options[ 'is' ]       = $this->length;
				$options[ 'notEqual' ] = Application::format( $this->notEqual, [
					'attribute' => $label,
					'length'    => $this->length,
				] );
			}
			if( $this->skipOnEmpty ) {
				$options[ 'skipOnEmpty' ] = 1;
			}

			if( $this->whenClient != null ) {
				$options[ 'whenClient' ] = $this->whenClient;
			}
//        ValidationAsset::register($view); messages,

//        return 'validation.string(value,' . json_encode($options, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE) . ');';
//        return  json_encode($options, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
			return $options;
		}
	}
