<?php

	namespace Application\Validators;

	use \Application\Classes\Application;

	class NumberValidator extends \Application\Validators\Validator
	{

		public $integerOnly = false;

		public $max;

		public $min;

		public $bigOnAttribute;

		public $smallOnAttribute;

		public $tooBig;

		public $tooSmall;

		public $integerPattern = '/^\s*[+-]?\d+\s*$/';

		public $numberPattern = '/^\s*[-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?\s*$/';

		public function init()
		{
			if( $this->message === null ) {
				$this->message = $this->integerOnly ? 'Значение {attribute} должно быть целым числом.' : 'Значение {attribute} должно быть числом.';
			}
			if( $this->min !== null && $this->tooSmall === null ) {
				$this->tooSmall = 'Значение {attribute} должно быть не меньше ' . $this->min;
			}
			if( $this->max !== null && $this->tooBig === null ) {
				$this->tooBig = 'Значение {attribute} должно быть не больше ' . $this->max;
			}
			if( $this->smallOnAttribute !== null && $this->tooSmall === null ) {
				$this->tooSmall = 'Значение {attribute} должно быть не меньше ' . $this->smallOnAttribute;
			}
			if( $this->bigOnAttribute !== null && $this->tooBig === null ) {
				$this->tooBig = 'Значение {attribute} должно быть не больше ' . $this->bigOnAttribute;
			}
		}

		public function validateAttribute( $model, $attribute )
		{
			$value = $model->$attribute;
			if( is_array( $value ) ) {
				$this->addError( $model, $attribute, $this->message );
				return;
			}
			$pattern = $this->integerOnly ? $this->integerPattern : $this->numberPattern;

			if( !preg_match( $pattern, "$value" ) && $value != '' ) {
				$this->addError( $model, $attribute, $this->message );
			}
			if( $this->min !== null && $value < $this->min ) {
				$this->addError( $model, $attribute, $this->tooSmall, [ 'min' => $this->min ] );
			}
			if( $this->max !== null && $value > $this->max ) {
				$this->addError( $model, $attribute, $this->tooBig, [ 'max' => $this->max ] );
			}
			if( $this->smallOnAttribute !== null && $value > $model->{$this->smallOnAttribute}) {
				$this->addError( $model, $attribute, $this->tooSmall, [ 'min' => $this->smallOnAttribute ] );
			}
			if( $this->bigOnAttribute !== null && $value < $model->{$this->bigOnAttribute}) {
				$this->addError( $model, $attribute, $this->tooBig, [ 'max' => $this->bigOnAttribute ] );
			}
		}

		protected function validateValue( $value )
		{
			if( is_array( $value ) ) {
				return [ '{attribute} is invalid.', [] ];
			}
			$pattern = $this->integerOnly ? $this->integerPattern : $this->numberPattern;
			if( !preg_match( $pattern, "$value" ) ) {
				return [ $this->message, [] ];
			}
			elseif( $this->min !== null && $value < $this->min ) {
				return [ $this->tooSmall, [ 'min' => $this->min ] ];
			}
			elseif( $this->max !== null && $value > $this->max ) {
				return [ $this->tooBig, [ 'max' => $this->max ] ];
			}
			elseif( $this->smallOnAttribute !== null && $value < $this->smallOnAttribute ) {
				return [ $this->tooSmall, [ 'min' => $this->smallOnAttribute ] ];
			}
			elseif( $this->bigOnAttribute !== null && $value > $this->bigOnAttribute ) {
				return [ $this->tooBig, [ 'max' => $this->bigOnAttribute ] ];
			}
			else {
				return null;
			}
		}

		public function clientValidateAttribute( $model, $attribute, $view )
		{
			$label = $model->getAttributeLabel( $attribute );

			$options = [
				'validator' => 'number',
				'pattern'   => $this->integerOnly ? $this->integerPattern : $this->numberPattern,
				'message'   => Application::format( $this->message, [
					'attribute' => $label,
				] ),
			];

			if( $this->min !== null ) {
				$options[ 'min' ]      = is_string( $this->min ) ? (float) $this->min : $this->min;
				$options[ 'tooSmall' ] = Application::format( $this->tooSmall, [
					'attribute' => $label,
					'min'       => $this->min,
				] );
			}
			if( $this->max !== null ) {
				$options[ 'max' ]    = is_string( $this->max ) ? (float) $this->max : $this->max;
				$options[ 'tooBig' ] = Application::format( $this->tooBig, [
					'attribute' => $label,
					'max'       => $this->max,
				] );
			}
			if( $this->bigOnAttribute !== null ) {
				$options[ 'bigOnAttribute' ]    = $this->bigOnAttribute;
				$options[ 'tooBig' ] = Application::format( $this->tooBig, [
					'attribute' => $label,
					'bigOnAttribute'       => $this->bigOnAttribute,
				] );
			}
			if( $this->smallOnAttribute !== null ) {
				$options[ 'smallOnAttribute' ]    =  $this->smallOnAttribute;
				$options[ 'tooBig' ] = Application::format( $this->tooSmall, [
					'attribute' => $label,
					'smallOnAttribute'       => $this->smallOnAttribute,
				] );
			}
			if( $this->skipOnEmpty ) {
				$options[ 'skipOnEmpty' ] = 1;
			}

			if( $this->whenClient != null ) {
				$options[ 'whenClient' ] = $this->whenClient;
			}

//        ValidationAsset::register($view);

//        return 'validation.number(value, messages, ' . json_encode($options) . ');';
			return $options;
		}
	}
