<?php

	namespace Application\Validators;

	use \Application\Classes\Application;

	class CompareValidator extends \Application\Validators\Validator
	{

		public $compareAttribute;

		public $compareValue;

		public $type = 'string';

		public $operator = '==';

		public $message;

		public function init()
		{
			if( $this->message === null ) {
				switch( $this->operator ) {
					case '==':
						$this->message = 'Пароли не совпадают.';
						break;
					case '===':
						$this->message = 'Пароли не совпадают.';
						break;
					case '!=':
						$this->message = "Значение «{attribute}» не должно быть равно «{compareValue}»";
						break;
					case '!==':
						$this->message = 'Значение «{attribute}» не должно быть равно «{compareValue}».';
						break;
					case '>':
						$this->message = 'Значение «{attribute}» должно быть больше значения «{compareValue}».';
						break;
					case '>=':
						$this->message = 'Значение «{attribute}» должно быть больше или равно значения «{compareValue}».';
						break;
					case '<':
						$this->message = 'Значение «{attribute}» должно быть меньше значения «{compareValue}».';
						break;
					case '<=':
						$this->message = 'Значение «{attribute}» должно быть меньше или равно значения «{compareValue}».';
						break;
					default:
						throw new \Exception( "Unknown operator: {$this->operator}" );
				}
			}
		}

		public function validateAttribute( $model, $attribute )
		{

			$value = $model->$attribute;
			if( is_array( $value ) ) {
				$this->addError( $model, $attribute, '{attribute} is invalid.' );

				return;
			}
			if( $this->compareValue !== null ) {
				$compareLabel = $compareValue = $this->compareValue;
			}
			else {
				$compareAttribute = $this->compareAttribute === null ? $attribute . '_repeat' : $this->compareAttribute;
				$compareValue     = $model->$compareAttribute;
				$compareLabel     = $model->getAttributeLabel( $compareAttribute );
			}

			if( !$this->compareValues( $this->operator, $this->type, $value, $compareValue ) ) {
				$this->addError( $model, $attribute, $this->message, [
					'compareAttribute' => $compareLabel,
					'compareValue'     => $compareValue,
				] );
			}
		}

		protected function validateValue( $value )
		{
			if( $this->compareValue === null ) {
				throw new InvalidConfigException( 'CompareValidator::compareValue must be set.' );
			}
			if( !$this->compareValues( $this->operator, $this->type, $value, $this->compareValue ) ) {
				return [
					$this->message,
					[
						'compareAttribute' => $this->compareValue,
						'compareValue'     => $this->compareValue,
					]
				];
			}
			else {
				return null;
			}
		}

		protected function compareValues( $operator, $type, $value, $compareValue )
		{
			if( $type === 'number' ) {
				$value        = (float) $value;
				$compareValue = (float) $compareValue;
			}
			else {
				$value        = (string) $value;
				$compareValue = (string) $compareValue;
			}
			switch( $operator ) {
				case '==':
					return $value == $compareValue;
				case '===':
					return $value === $compareValue;
				case '!=':
					return $value != $compareValue;
				case '!==':
					return $value !== $compareValue;
				case '>':
					return $value > $compareValue;
				case '>=':
					return $value >= $compareValue;
				case '<':
					return $value < $compareValue;
				case '<=':
					return $value <= $compareValue;
				default:
					return false;
			}
		}

		public function clientValidateAttribute( $model, $attribute, $view )
		{
			$options = [
				'validator' => 'compare',
				'operator'  => $this->operator,
				'type'      => $this->type,
			];

			if( $this->compareValue !== null ) {
				$options[ 'compareValue' ] = $this->compareValue;
				$compareValue              = $this->compareValue;
			}
			else {
				$compareAttribute              = $this->compareAttribute === null ? $attribute . '_repeat' : $this->compareAttribute;
				$compareValue                  = $model->getAttributeLabel( $compareAttribute );
				$options[ 'compareAttribute' ] = $compareAttribute;
			}

			if( $this->skipOnEmpty ) {
				$options[ 'skipOnEmpty' ] = 1;
			}

			$options[ 'message' ] = Application::format( $this->message, [
				'attribute'        => $model->getAttributeLabel( $attribute ),
				'compareAttribute' => $compareValue,
				'compareValue'     => $compareValue,
			] );
			if( $this->whenClient != null ) {
				$options[ 'whenClient' ] = $this->whenClient;
			}
//        ValidationAsset::register($view);

//        return 'validation.compare(value, messages, ' . json_encode($options, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE) . ');';
			return $options;
		}
	}
