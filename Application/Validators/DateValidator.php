<?php

	namespace Application\Validators;

	use DateTime;
	use IntlDateFormatter;


	class DateValidator extends \Application\Validators\Validator
	{

		public $format;

		public $locale;

		public $timeZone;

		public $timestampAttribute;

		public $timestampAttributeFormat;

		public $timestampAttributeTimeZone = 'UTC';

		public $max;

		public $min;

		public $tooBig;

		public $tooSmall;

		public $maxString;

		public $minString;

		private $_dateFormats
			= [
				'short'  => 3,
				'medium' => 2,
				'long'   => 1,
				'full'   => 0,
			];

		public function init()
		{
			if( $this->message === null ) {
				$this->message = 'The format of {attribute} is invalid.';
			}
			if( $this->format === null ) {
				$this->format = 'd.m.Y';
			}
			if( $this->locale === null ) {
				$this->locale = 'ru';
			}
			if( $this->timeZone === null ) {
				$this->timeZone = 'Europe/Moscow';
			}
			if( $this->min !== null && $this->tooSmall === null ) {
				$this->tooSmall = '{attribute} must be no less than {min}.';
			}
			if( $this->max !== null && $this->tooBig === null ) {
				$this->tooBig = '{attribute} must be no greater than {max}.';
			}
			if( $this->maxString === null ) {
				$this->maxString = (string) $this->max;
			}
			if( $this->minString === null ) {
				$this->minString = (string) $this->min;
			}
			if( $this->max !== null && is_string( $this->max ) ) {
				$timestamp = $this->parseDateValue( $this->max );
				if( $timestamp === false ) {
					throw new \Exception( "Invalid max date value: {$this->max}" );
				}
				$this->max = $timestamp;
			}
			if( $this->min !== null && is_string( $this->min ) ) {
				$timestamp = $this->parseDateValue( $this->min );
				if( $timestamp === false ) {
					throw new \Exception( "Invalid min date value: {$this->min}" );
				}
				$this->min = $timestamp;
			}
		}


		public function validateAttribute( $model, $attribute )
		{
			$value     = $model->$attribute;
			$timestamp = $this->parseDateValue( $value );
			if( $timestamp === false ) {
				$this->addError( $model, $attribute, $this->message, [] );
			}
			elseif( $this->min !== null && $timestamp < $this->min ) {
				$this->addError( $model, $attribute, $this->tooSmall, [ 'min' => $this->minString ] );
			}
			elseif( $this->max !== null && $timestamp > $this->max ) {
				$this->addError( $model, $attribute, $this->tooBig, [ 'max' => $this->maxString ] );
			}
			elseif( $this->timestampAttribute !== null ) {
				if( $this->timestampAttributeFormat === null ) {
					$model->{$this->timestampAttribute} = $timestamp;
				}
				else {
					$model->{$this->timestampAttribute} = $this->formatTimestamp( $timestamp, $this->timestampAttributeFormat );
				}
			}
		}

		protected function validateValue( $value )
		{
			$timestamp = $this->parseDateValue( $value );
			if( $timestamp === false ) {
				return [ $this->message, [] ];
			}
			elseif( $this->min !== null && $timestamp < $this->min ) {
				return [ $this->tooSmall, [ 'min' => $this->minString ] ];
			}
			elseif( $this->max !== null && $timestamp > $this->max ) {
				return [ $this->tooBig, [ 'max' => $this->maxString ] ];
			}
			else {
				return null;
			}
		}

		protected function parseDateValue( $value )
		{
			if( is_array( $value ) ) {
				return false;
			}
			$format = $this->format;
			if( strncmp( $this->format, 'php:', 4 ) === 0 ) {
				$format = substr( $format, 4 );
			}
			else {
				if( extension_loaded( 'intl' ) ) {
					return $this->parseDateValueIntl( $value, $format );
				}
			}
			return $this->parseDateValuePHP( $value, $format );
		}

		private function parseDateValueIntl( $value, $format )
		{
			if( isset( $this->_dateFormats[ $format ] ) ) {
				$formatter = new IntlDateFormatter( $this->locale, $this->_dateFormats[ $format ], IntlDateFormatter::NONE, 'UTC' );
			}
			else {
				$hasTimeInfo = ( strpbrk( $format, 'ahHkKmsSA' ) !== false );
				$formatter   = new IntlDateFormatter( $this->locale, IntlDateFormatter::NONE, IntlDateFormatter::NONE, $hasTimeInfo ? $this->timeZone : 'UTC', null, $format );
			}
			$formatter->setLenient( false );
			$parsePos   = 0;
			$parsedDate = @$formatter->parse( $value, $parsePos );
			if( $parsedDate === false || $parsePos !== mb_strlen( $value, 'UTF-8' ) ) {
				return false;
			}

			return $parsedDate;
		}

		private function parseDateValuePHP( $value, $format )
		{
			$hasTimeInfo = ( strpbrk( $format, 'HhGgis' ) !== false );

			$date   = DateTime::createFromFormat( $format, $value, new \DateTimeZone( $hasTimeInfo ? $this->timeZone : 'UTC' ) );
			$errors = DateTime::getLastErrors();
			if( $date === false || $errors[ 'error_count' ] || $errors[ 'warning_count' ] ) {
				return false;
			}

			if( !$hasTimeInfo ) {
				$date->setTime( 0, 0, 0 );
			}
			return $date->getTimestamp();
		}

		private function formatTimestamp( $timestamp, $format )
		{
			if( strncmp( $format, 'php:', 4 ) === 0 ) {
				$format = substr( $format, 4 );
			}
			else {
				$format = FormatConverter::convertDateIcuToPhp( $format, 'date' );
			}

			$date = new DateTime();
			$date->setTimestamp( $timestamp );
			$date->setTimezone( new \DateTimeZone( $this->timestampAttributeTimeZone ) );
			return $date->format( $format );
		}
	}
