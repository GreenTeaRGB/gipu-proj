<?php

	namespace Application\Validators;


	class ExistValidator extends \Application\Validators\Validator
	{

		public $targetClass;

		public $targetAttribute;

		public $filter;

		public $allowArray = false;

		public function init()
		{
			if( $this->message === null ) {
				$this->message = 'Значение «{attribute}» не существует';
			}
		}

		public function validateAttribute( $model, $attribute )
		{
			$targetAttribute = $this->targetAttribute === null ? $attribute : $this->targetAttribute;
			if( is_array( $targetAttribute ) ) {
				if( $this->allowArray ) {
					throw new \Exception( 'The "targetAttribute" property must be configured as a string.' );
				}
				$params = [];

				foreach( $targetAttribute as $k => $v ) {
					if( is_string( $v ) ) {
						$params[ $v ] = $model->$v;
					}
					elseif( is_array( $targetAttribute[$k] ) ) {
						foreach( $v as $val ) {
							$params['or'][ $val ] = $model->$k;
						}
					}
				}
			}
			else {
				$params = [ $targetAttribute => $model->$attribute ];
			}
//			if( !$this->allowArray ) {
//				foreach( $params as $value ) {
//					if( is_array( $value ) ) {
//						$this->addError( $model, $attribute, '{attribute} is invalid.' );
//						return;
//					}
//				}
//			}

			$targetClass = $this->targetClass === null ? get_class( $model ) : $this->targetClass;
			$query       = $this->createQuery( $targetClass, $params );
			if( is_array( $model->$attribute ) ) {
				if( $query->count() != count( $model->$attribute ) ) {
					$this->addError( $model, $attribute, $this->message );
				}
			}
			elseif( !$query->exists() ) {
				$this->addError( $model, $attribute, $this->message );
			}
		}

		protected function validateValue( $value )
		{

			if( $this->targetClass === null ) {
				throw new \Exception( 'The "targetClass" property must be set.' );
			}
			if( !is_string( $this->targetAttribute ) ) {
				throw new \Exception( 'The "targetAttribute" property must be configured as a string.' );
			}

			$query = $this->createQuery( $this->targetClass, [ $this->targetAttribute => $value ] );

			if( is_array( $value ) ) {
				if( !$this->allowArray ) {
					return [ $this->message, [] ];
				}
				return $query->count() == count( $value ) ? null : [ $this->message, [] ];
			}
			else {
				return $query->exists() ? null : [ $this->message, [] ];
			}
		}

		protected function createQuery( $targetClass, $condition )
		{
			$query = new $targetClass();
			if( isset( $this->type ) && $this->type == 'or' && isset($condition['or']) ) {
			    $or = $condition['or'];
			    unset($condition['or']);
                $query->where( $condition );
				$query->andOrWhere( $or );
			}
			else {
				$query->where( $condition );
			}

			if( $this->filter instanceof \Closure ) {
				call_user_func( $this->filter, $query );
			}
			elseif( $this->filter !== null ) {
				$query->andWhere( $this->filter );
			}
			return $query;
		}
	}
