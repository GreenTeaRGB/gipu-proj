<?php

namespace Application\Validators;

use \Application\Classes\Application;
use \Application\Helpers\UploadedFile;

class Validator
{

  public static $builtInValidators = [
    'boolean'  => 'Application\Validators\BooleanValidator',
    'captcha'  => 'Application\Validators\CaptchaValidator',
    'compare'  => 'Application\Validators\CompareValidator',
    'date'     => 'Application\Validators\DateValidator',
    'default'  => 'Application\Validators\DefaultValueValidator',
    'double'   => 'Application\Validators\NumberValidator',
    'each'     => 'Application\Validators\EachValidator',
    'email'    => 'Application\Validators\EmailValidator',
    'exist'    => 'Application\Validators\ExistValidator',
    'file'     => 'Application\Validators\FileValidator',
    'filter'   => 'Application\Validators\FilterValidator',
    'image'    => 'Application\Validators\ImageValidator',
    'in'       => 'Application\Validators\RangeValidator',
    'integer'  => [
      'class'       => 'Application\Validators\NumberValidator',
      'integerOnly' => true,
    ],
    'match'    => 'Application\Validators\RegularExpressionValidator',
    'link'     => [
      'class'   => 'Application\Validators\RegularExpressionValidator',
      'pattern' => '/(^[0-9a-zA-Z-_]+$)/',
      'message' => 'Допустимые символы: 0-9 a-z A-Z - _'
    ],
    'number'   => 'Application\Validators\NumberValidator',
    'required' => 'Application\Validators\RequiredValidator',
    'safe'     => 'Application\Validators\SafeValidator',
    'string'   => 'Application\Validators\StringValidator',
    'trim'     => [
      'class'       => 'Application\Validators\FilterValidator',
      'filter'      => 'trim',
      'skipOnArray' => true,
    ],
    'unique'   => 'Application\Validators\UniqueValidator',
    'url'      => 'Application\Validators\UrlValidator',
  ];

  public $attributes = [];

  public $message;

  public $on = [];

  public $except = [];

  public $skipOnError = true;

  public $skipOnEmpty = true;

  public $enableClientValidation = true;

  public $isEmpty;

  public $when;

  public $whenClient;

  public static function createValidator( $type, $model, $attributes, $params = [] )
  {
    $params['attributes'] = $attributes;
    if ( $type instanceof \Closure || $model->hasMethod( $type ) ) {
      $params['class']  = __NAMESPACE__ . '\InlineValidator';
      $params['method'] = $type;
    } else {
      if ( isset( static::$builtInValidators[$type] ) ) {
        $type = static::$builtInValidators[$type];
      }
      if ( is_array( $type ) ) {
        $params = array_merge( $type, $params );
      } else {
        $params['class'] = $type;
      }
    }
    $class = new $params['class']();
    foreach ( $params as $field => $param ) {
      if ( ( $field == 'on' || $field == 'except' ) && !is_array( $param ) ) {
        $param = (array)$param;
      }

      $class->$field = $param;
    }
    if ( method_exists( $class, 'init' ) )
      $class->init();

    return $class;
  }

  public function __construct()
  {
    $this->attributes = (array)$this->attributes;
    $this->on         = (array)$this->on;
    $this->except     = (array)$this->except;
  }

  public function validateAttributes( $model, $attributes = null )
  {
    if ( is_array( $attributes ) ) {
      $attributes = array_intersect( $this->attributes, $attributes );
    } else {
      $attributes = $this->attributes;
    }
    foreach ( $attributes as $attribute ) {
      if ( $this instanceof \Application\Validators\FileValidator ) {
        if ( ( isset( $model->getFields()[$attribute] ) && $model->getFields()[$attribute]['multiple'] ) /*|| $this->maxFiles > 1*/ ) {
          $model->$attribute = UploadedFile::getInstances( $model, $attribute );
        } else {
          $model->$attribute = UploadedFile::getInstance( $model, $attribute );
        }
      }
      if ( !isset( $model->$attribute ) ) {
        $model->$attribute = '';
      }
      $skip = $this->skipOnError && $model->hasErrors( $attribute )
        || $this->skipOnEmpty && $this->isEmpty( $model->$attribute );
      if ( !$skip ) {
        if ( $this->when === null || call_user_func( $this->when, $model, $attribute ) ) {
          $this->validateAttribute( $model, $attribute );
        }
      }
    }
  }

  public function validateAttribute( $model, $attribute )
  {
    $result = $this->validateValue( $model->$attribute );
    if ( !empty( $result ) ) {
      $this->addError( $model, $attribute, $result[0], $result[1] );
    }
  }

  public function validate( $value, &$error = null )
  {
    $result = $this->validateValue( $value );
    if ( empty( $result ) ) {
      return true;
    }

    list( $message, $params ) = $result;
    $params['attribute'] = 'the input value';
    $params['value']     = is_array( $value ) ? 'array()' : $value;
    $error               = Application::format( $message, $params );
    return false;
  }

  protected function validateValue( $value )
  {
    throw new \Exception( get_class( $this ) . ' does not support validateValue().' );
  }

  public function clientValidateAttribute( $model, $attribute, $view )
  {
    return null;
  }

  public function isActive( $scenario )
  {
    return !in_array( $scenario, $this->except, true ) && ( empty( $this->on ) || in_array( $scenario, $this->on, true ) );
  }

  public function addError( $model, $attribute, $message, $params = [] )
  {
    $value               = $model->$attribute;
    $params['attribute'] = $model->getAttributeLabel( $attribute );
    $params['value']     = is_array( $value ) ? 'array()' : $value;
    $model->addError( $attribute, Application::format( $message, $params ) );
  }

  public function isEmpty( $value )
  {
    if ( $this->isEmpty !== null ) {
      return call_user_func( $this->isEmpty, $value );
    } else {
      return $value === null || $value === [] || $value === '';
    }
  }


}
