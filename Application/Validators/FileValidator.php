<?php

	namespace Application\Validators;

	use \Application\Classes\Application;
	use \Application\Helpers\UploadedFile;
	use \Application\Helpers\FileUploadHelper as FileHelper;

	class FileValidator extends \Application\Validators\Validator
	{

		public $extensions = [ 'png', 'jpg', 'jpeg' ];

		public $checkExtensionByMimeType = true;

		public $mimeTypes;

		public $minSize;

		public $maxSize;

		public $maxFiles = 1;

		public $message;

		public $uploadRequired;

		public $tooBig;

		public $tooSmall;

		public $tooMany;

		public $wrongExtension;

		public $wrongMimeType;


		public function init()
		{
			if( $this->message === null ) {
				$this->message = 'Загрузка файла не удалась.';
			}
			if( $this->uploadRequired === null ) {
				$this->uploadRequired = 'Загрузите файл.';
			}
			if( $this->tooMany === null ) {
				$this->tooMany = 'Вы не можете загружать более ' . $this->maxFiles . ' файлов';
			}
			if( $this->wrongExtension === null ) {
				$this->wrongExtension = 'Загрузка запрещенного файла';
			}
			if( $this->tooBig === null ) {
				$this->tooBig = 'Файл слишком большой. Размер не должен превышать ' . $this->maxSize . ' байт';
			}
			if( $this->tooSmall === null ) {
				$this->tooSmall = 'Файл слишком маленький. Размер должен быть более ' . $this->minSize . ' байт';
			}
			if( !is_array( $this->extensions ) ) {
				$this->extensions = preg_split( '/[\s,]+/', strtolower( $this->extensions ), -1, PREG_SPLIT_NO_EMPTY );
			}
			else {
				$this->extensions = array_map( 'strtolower', $this->extensions );
			}
			if( $this->wrongMimeType === null ) {
				$this->wrongMimeType = 'Загрузка файла с запрещенными MIME-типами';
			}
			if( !is_array( $this->mimeTypes ) ) {
				$this->mimeTypes = preg_split( '/[\s,]+/', strtolower( $this->mimeTypes ), -1, PREG_SPLIT_NO_EMPTY );
			}
			else {
				$this->mimeTypes = array_map( 'strtolower', $this->mimeTypes );
			}
		}

		public function validateAttribute( $model, $attribute )
		{

			if( $this->maxFiles > 1 ) {
				$files = $model->$attribute;

				if( !is_array( $files ) ) {
					$this->addError( $model, $attribute, $this->uploadRequired );
					return;
				}
				foreach( $files as $i => $file ) {
					if( !$file instanceof UploadedFile || $file->error == UPLOAD_ERR_NO_FILE ) {
						unset( $files[ $i ] );
					}
				}

				$model->$attribute = array_values( $files );
				if( empty( $files ) ) {
					$this->addError( $model, $attribute, $this->uploadRequired );
				}
				if( count( $files ) > $this->maxFiles ) {
					$this->addError( $model, $attribute, $this->tooMany, [ 'limit' => $this->maxFiles ] );
				}
				else {
					foreach( $files as $file ) {
						$result = $this->validateValue( $file );
						if( !empty( $result ) ) {
							$this->addError( $model, $attribute, $result[ 0 ], $result[ 1 ] );
						}
					}
				}
			}
			else {
				$result = $this->validateValue( $model->$attribute );
				if( !empty( $result ) ) {
					$this->addError( $model, $attribute, $result[ 0 ], $result[ 1 ] );
				}
			}
		}

		protected function validateValue( $file )
		{
			if( !$file instanceof UploadedFile || $file->error == UPLOAD_ERR_NO_FILE ) {
				return [ $this->uploadRequired, [] ];
			}
			switch( $file->error ) {
				case UPLOAD_ERR_OK:
					if( $this->maxSize !== null && $file->size > $this->maxSize ) {
						return [ $this->tooBig, [ 'file' => $file->name, 'limit' => $this->getSizeLimit() ] ];
					}
					elseif( $this->minSize !== null && $file->size < $this->minSize ) {
						return [ $this->tooSmall, [ 'file' => $file->name, 'limit' => $this->minSize ] ];
					}
					elseif( !empty( $this->extensions ) && !$this->validateExtension( $file ) ) {
						return [
							$this->wrongExtension,
							[ 'file' => $file->name, 'extensions' => implode( ', ', $this->extensions ) ]
						];
					}
					elseif( !empty( $this->mimeTypes ) && !in_array( FileHelper::getMimeType( $file->tempName ), $this->mimeTypes, false ) ) {
						return [
							$this->wrongMimeType,
							[ 'file' => $file->name, 'mimeTypes' => implode( ', ', $this->mimeTypes ) ]
						];
					}
					else {
						return null;
					}
				case UPLOAD_ERR_INI_SIZE:
				case UPLOAD_ERR_FORM_SIZE:
					return [ $this->tooBig, [ 'file' => $file->name, 'limit' => $this->getSizeLimit() ] ];
				case UPLOAD_ERR_PARTIAL:
					throw new \Exception( 'File was only partially uploaded: ' . $file->name, __METHOD__ );
					break;
				case UPLOAD_ERR_NO_TMP_DIR:
					throw new \Exception( 'Missing the temporary folder to store the uploaded file: ' . $file->name, __METHOD__ );
					break;
				case UPLOAD_ERR_CANT_WRITE:
					throw new \Exception( 'Failed to write the uploaded file to disk: ' . $file->name, __METHOD__ );
					break;
				case UPLOAD_ERR_EXTENSION:
					throw new \Exception( 'File upload was stopped by some PHP extension: ' . $file->name, __METHOD__ );
					break;
				default:
					break;
			}

			return [ $this->message, [] ];
		}

		public function getSizeLimit()
		{
			$limit = $this->sizeToBytes( ini_get( 'upload_max_filesize' ) );
			if( $this->maxSize !== null && $limit > 0 && $this->maxSize < $limit ) {
				$limit = $this->maxSize;
			}
			if( isset( $_POST[ 'MAX_FILE_SIZE' ] ) && $_POST[ 'MAX_FILE_SIZE' ] > 0 && $_POST[ 'MAX_FILE_SIZE' ] < $limit ) {
				$limit = (int) $_POST[ 'MAX_FILE_SIZE' ];
			}

			return $limit;
		}

		public function isEmpty( $value, $trim = false )
		{
			$value = is_array( $value ) ? reset( $value ) : $value;
			return !( $value instanceof UploadedFile ) || $value->error == UPLOAD_ERR_NO_FILE;
		}

		private function sizeToBytes( $sizeStr )
		{
			switch( substr( $sizeStr, -1 ) ) {
				case 'M':
				case 'm':
					return (int) $sizeStr * 1048576;
				case 'K':
				case 'k':
					return (int) $sizeStr * 1024;
				case 'G':
				case 'g':
					return (int) $sizeStr * 1073741824;
				default:
					return (int) $sizeStr;
			}
		}

		protected function validateExtension( $file )
		{
			$extension = mb_strtolower( $file->getExtension(), 'utf-8' );

			if( $this->checkExtensionByMimeType ) {

				$mimeType = FileHelper::getMimeType( $file->tempName, null, false );
				if( $mimeType === null ) {
					return false;
				}

				$extensionsByMimeType = FileHelper::getExtensionsByMimeType( $mimeType );

				if( !in_array( $extension, $extensionsByMimeType, true ) ) {
					return false;
				}
			}

			if( !in_array( $extension, $this->extensions, true ) ) {
				return false;
			}

			return true;
		}

		public function clientValidateAttribute( $model, $attribute, $view )
		{
//        ValidationAsset::register($view);
			$options                = $this->getClientOptions( $model, $attribute );
			$options[ 'validator' ] = 'file';
//        return 'validation.file(attribute, messages, ' . json_encode($options, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE) . ');';
			return $options;
		}

		protected function getClientOptions( $model, $attribute )
		{
			$label = $model->getAttributeLabel( $attribute );

			if( $this->message !== null ) {
				$options[ 'message' ] = Application::format( $this->message, [
					'attribute' => $label,
				] );
			}

			$options[ 'skipOnEmpty' ] = $this->skipOnEmpty;

			if( !$this->skipOnEmpty ) {
				$options[ 'uploadRequired' ] = Application::format( $this->uploadRequired, [
					'attribute' => $label,
				] );
			}

			if( $this->mimeTypes !== null ) {
				$options[ 'mimeTypes' ]     = $this->mimeTypes;
				$options[ 'wrongMimeType' ] = Application::format( $this->wrongMimeType, [
					'attribute' => $label,
					'mimeTypes' => join( ', ', $this->mimeTypes ),
				] );
			}

			if( $this->extensions !== null ) {
				$options[ 'extensions' ]     = $this->extensions;
				$options[ 'wrongExtension' ] = Application::format( $this->wrongExtension, [
					'attribute'  => $label,
					'extensions' => join( ', ', $this->extensions ),
				] );
			}

			if( $this->minSize !== null ) {
				$options[ 'minSize' ]  = $this->minSize;
				$options[ 'tooSmall' ] = Application::format( $this->tooSmall, [
					'attribute' => $label,
					'limit'     => $this->minSize,
				] );
			}

			if( $this->maxSize !== null ) {
				$options[ 'maxSize' ] = $this->maxSize;
				$options[ 'tooBig' ]  = Application::format( $this->tooBig, [
					'attribute' => $label,
					'limit'     => $this->maxSize,
				] );
			}

			if( $this->maxFiles !== null ) {
				$options[ 'maxFiles' ] = $this->maxFiles;
				$options[ 'tooMany' ]  = Application::format( $this->tooMany, [
					'attribute' => $label,
					'limit'     => $this->maxFiles,
				] );
			}
			if( $this->whenClient != null ) {
				$options[ 'whenClient' ] = $this->whenClient;
			}

			return $options;
		}
	}
