<?php
return [
	'admin' => 'admin/index',
	'admin/user' => 'adminUser/index',
  'admin/messages'=> 'adminMessages/index',
  'admin/product/getContentPropertiesByCategoryId/([0-9]+)' => 'adminProduct/getContentPropertiesByCategoryId/$1',
  'admin/product/getContentPropertiesByCategoryId/([0-9]+)/([0-9]+)' => 'adminProduct/getContentPropertiesByCategoryId/$1/$2',
  'admin/remittance' => 'adminRemittance/index',
  'admin/getCrosFile' => 'admin/getCrosFile',
];
?>