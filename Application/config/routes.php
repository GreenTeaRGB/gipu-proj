<?php

$admin_routes   = require_once dirname( __FILE__ ) . '/admin_routes.php';
$builder_routes = require_once dirname( __FILE__ ) . '/BuilderRouters.php';
$routes         = [
  'user/login'        => 'user/showLogin',
  'user/auth'         => 'user/authentication',
  'user/registration' => 'user/registration',
  'user/register'     => 'user/showRegister',
  'user/edit'         => 'user/edit',
  'user/save'         => 'user/save',
  'user/logout'       => 'user/logout',
  'user/dashboard'    => 'user/index',


  /* cabinet actions */

  'cabinet/getDashboardData'                        => 'cabinet/getDashboardData',
  'cabinet/updateData'                              => 'cabinet/updateData',
  'cabinet/getWallet'                               => 'cabinet/getWallet',
  'cabinet/getWallet/page-([0-9]+)'                 => 'cabinet/getWallet/$1',
  'cabinet/getCards'                                => 'cabinet/getCards',
  'cabinet/createWarehouse'                         => 'cabinet/createWarehouse',
  'cabinet/deleteWarehouse/([0-9]+)'                => 'cabinet/deleteWarehouse/$1',
  'cabinet/updateWarehouse/([0-9]+)'                => 'cabinet/updateWarehouse/$1',
  'cabinet/getDataWarehouse/([0-9]+)'               => 'cabinet/getDataWarehouse/$1',
  'cabinet/getDataWarehouse/([0-9]+)/page-([0-9]+)' => 'cabinet/getDataWarehouse/$1/$2',
  'cabinet/addCard'                                 => 'cabinet/addCard',
  'cabinet/deleteCard/([0-9]+)'                     => 'cabinet/deleteCard/$1',
  'cabinet/checkUserExist/([0-9]+)'                 => 'cabinet/checkUserExist/$1',
  'cabinet/checkSmsCode/([0-9]+)'                   => 'cabinet/checkSmsCode/$1',
  'cabinet/sendSmsCode'                             => 'cabinet/sendSmsCode',
  'cabinet/transfer'                                => 'cabinet/transfer',
  'cabinet/requestOutput'                           => 'cabinet/requestOutput',
  'cabinet/referral-list'                           => 'cabinet/referralList',
  'cabinet/updateShop/([0-9]+)'                     => 'cabinet/updateShop/$1',
  'cabinet/getDataOrdersProduct/([0-9]+)'           => 'cabinet/getDataOrdersProduct/$1',
  'cabinet/getCategoryAndProductByShopId/([0-9]+)'  => 'cabinet/getCategoryAndProductByShopId/$1',
  'cabinet/updatePriceProduct/([0-9]+)'             => 'cabinet/updatePriceProduct/$1',
  'cabinet/changeActiveProduct/([0-9]+)'            => 'cabinet/changeActiveProduct/$1',
  'cabinet/getProduct/([0-9]+)'                     => 'cabinet/getProduct/$1',
  'cabinet/updateDiscount/([0-9]+)'                 => 'cabinet/updateDiscount/$1',
  'cabinet/deleteProduct/([0-9]+)'                  => 'cabinet/deleteProduct/$1',
  'cabinet/getOrderProducts/page-([0-9]+)'          => 'cabinet/getOrderProducts/$1',

  /* dialog actions */

  'user/chat'             => 'dialog/dialogs',
  'dialog/search-user'    => 'dialog/searchUser',
  'dialog/send-message'   => 'dialog/sendMessage',
  'dialog/load-dialog'    => 'dialog/loadDialog',
  'dialog/load-dialogs'   => 'dialog/loadDialogs',
  'dialog/update-dialog'  => 'dialog/updateDialog',
  'dialog/update-dialogs' => 'dialog/updateDialogs',
  'dialog/read-message'   => 'dialog/readMessage',


  'user/order-history'          => 'user/orderHistory',
  'user/order-preview/([0-9]+)' => 'user/orderPreview/$1',
  'user/balance'                => 'user/balance',
  'user/balance/output'         => 'user/outputBalance',
  'user/balance/output/save'    => 'user/saveOutputBalance',

  /* delivery addresses for buyers */

  'user/addresses'               => 'user/addresses',
  'user/add-address'             => 'user/addAddress',
  'user/save-address'            => 'user/saveAddress',
  'user/save-address/([0-9]+)'   => 'user/saveAddress/$1',
  'user/edit-address/([0-9]+)'   => 'user/editAddress/$1',
  'user/remove-address/([0-9]+)' => 'user/removeAddress/$1',


  /* wish-list actions */

  'wish-list'        => 'wishlist/index',
  'wish-list/toggle' => 'wishlist/toggle',
  //'wish-list/remove' => 'wishlist/remove',

  /* seller actions */

  'user/verify-seller'                  => 'user/verifySeller',
  'user/sites-list'                     => 'user/sitesList',
  'user/seller/site/([0-9]+)'           => 'user/site/$1',
  'user/seller/site/construct/([0-9]+)' => 'user/construct/$1',
  'user/seller/site/edit/([0-9]+)'      => 'user/siteEdit/$1',

  'user/seller/site/configs/([0-9a-zA-Z -]+)' => 'user/configsConstruct/$1',

  'user/seller/site/templates/([0-9a-zA-Z -]+)/([0-9a-zA-Z -]+)' => 'user/templatesConstruct/$1/$2',

  'user/seller/site/paid-site/([0-9]+)' => 'user/paidSite/$1',
  'user/seller/site/paid/([0-9]+)'      => 'user/paid/$1',
  'user/referral-list'                  => 'user/referralList',
  'user/supplier-list'                  => 'user/supplierList',
  'user/create-site/([0-9]+)'           => 'user/createSite/$1',
  'user/create-site'                    => 'user/createSite',

  /* pay cards for seller */

  'user/pay-cards'            => 'user/cards',
  'user/add-card'             => 'user/addCard',
  'user/save-card'            => 'user/saveCard',
  'user/save-card/([0-9]+)'   => 'user/saveCard/$1',
  'user/edit-card/([0-9]+)'   => 'user/editCard/$1',
  'user/remove-card/([0-9]+)' => 'user/removeCard/$1',

  'user/orders-statistic'         => 'user/ordersStatistic',
  'user/order-statistic/([0-9]+)' => 'user/orderStatistic/$1',

  /* supplier actions */

  'user/verify-supplier' => 'user/verifySupplier',

  'user/promotions'                => 'user/promotions',
  'user/add-promotion'             => 'user/addPromotion',
  'user/save-promotion'            => 'user/savePromotion',
  'user/save-promotion/([0-9]+)'   => 'user/savePromotion/$1',
  'user/edit-promotion/([0-9]+)'   => 'user/editPromotion/$1',
  'user/remove-promotion/([0-9]+)' => 'user/removePromotion/$1',

  'promotions'                  => 'promotion/index',
  'promotions/([0-9a-zA-Z -]+)' => 'promotion/show/$1',

  /* supplier warehouse actions */

  'user/warehouse-list'          => 'user/warehouseList',
  'user/add-warehouse'           => 'user/addWarehouse',
  'user/show-warehouse/([0-9]+)' => 'user/showWarehouse/$1',
  'user/save-warehouse'          => 'user/saveWarehouse',
  'user/save-warehouse/([0-9]+)' => 'user/saveWarehouse/$1',
  'user/edit-warehouse/([0-9]+)' => 'user/editWarehouse/$1',

  /* category actions */

  'catalog'                                  => 'category/list',
  'catalog/product/([0-9a-zA-Z -]+)'         => 'category/product/$1',
  'category/sort'                            => 'category/sort',
  'category/([0-9a-zA-Z -_]+)/page-([0-9]+)' => 'category/show/$1/$2',
  'category/([0-9a-zA-Z -_]+)'               => 'category/show/$1',
  'search/page-([0-9]+)'                     => 'category/search/$1',
  'search'                                   => 'category/search',

  /* product actions */

  'product/create'        => 'product/create',
  'product/edit/([0-9]+)' => 'product/edit/$1',
  'product/save'          => 'product/save',
  'product/save/([0-9]+)' => 'product/save/$1',
  'product/property-list' => 'product/getPropertiesByCategoryId',

  /* cart actions */

  'cart'                      => 'cart/index',
  'cart/add'                  => 'cart/add',
  'cart/save'                 => 'cart/save',
  'cart/remove'               => 'cart/remove',
  'cart/add-discount'         => 'cart/addDiscount',
  'cart/remove-discount'      => 'cart/removeDiscount',
  'cart/cart-data'            => 'cart/getCartData',
  'cart/lastDeleted/([0-9]+)' => 'cart/lastDeleted/$1',
  'cart/addDeleted'           => 'cart/addDeleted',

  /* checkout actions */

  'checkout'                    => 'checkout/index',
  'checkout/save-orderdelivery' => 'checkout/saveOrderDelivery',
  'checkout/place-order'        => 'checkout/placeOrder',

  /* news actions */

  'news/getByCategory/([0-9]+)'          => 'news/getByCategory/$1',
  'news/show/([0-9a-zA-Z -_]+)'          => 'news/show/$1',
  'news'                                 => 'news/index',
  'news/([0-9a-zA-Z -_]+)/page-([0-9]+)' => 'news/index/$1/$2',
  'news/([0-9a-zA-Z -_]+)'               => 'news/index/$1',

  /* compare actions */

  'compare'                          => 'compare/index',
  'compare/add'                      => 'compare/add',
  'compare/remove/([0-9a-zA-Z -_]+)' => 'compare/remove/$1',
  'compare/remove-item'              => 'compare/removeItem',
  'compare/load-items'               => 'compare/loadItemsForCompare',
  'compare/get-properties'           => 'compare/getCategoryProperties',
  'compare/([0-9a-zA-Z -_]+)'        => 'compare/index/$1',

  /* review actions */

  'review/add' => 'review/add',

  /* like|dislike action */

  'like/toggle' => 'like/toggle',

  /* questions action */

  'questions'     => 'question/index',
  'questions/add' => 'question/addQuestion',
  'contacts'      => 'contact/index',
  'for_agents' => 'site/agent',
  'error-404'     => 'errors/404',
  //	'([a-zA-Z0-9_-]+)' => 'site/page/$1',
  ''              => 'site/index',
];

return array_merge( $routes, $admin_routes, $builder_routes );
