<?php
	if( isset( $errors ) && isset( $errors[$class][ $attribute ] ) && is_array( $errors[$class][ $attribute ] ) ) {
		foreach( $errors[$class][ $attribute ] as $error ) { ?>
            <em class="invalid"><?= $error ?></em>
		<?php }
	}
?>

