<?php
	$attrs = ' ';
	if(isset($settings['attr']) && is_array($settings['attr'])){
		foreach($settings['attr'] as $attr => $value){
			$attrs .= $attr.'="'.$value.'" ';
		}
	}
?>
<div>
	<label for="<?=$class.'['.$name.']'?>">
		<?=isset($settings['label'])?$settings['label']:$name?>
	</label>
	<input type="hidden" name="<?=$class.'['.$name.']'?>" id="<?=$class.'['.$name.']'?>" <?=$attrs?> value="0">
	<input type="checkbox" name="<?=$class.'['.$name.']'?>" id="<?=$class.'['.$name.']'?>" <?=$attrs?> value="1">
	<?php require 'error.php'; ?>
</div>