<?php
	$attrs = ' ';
	if(isset($settings['attr']) && is_array($settings['attr'])){
		foreach($settings['attr'] as $attr => $value){
			$attrs .= $attr.'="'.$value.'" ';
		}
	}
$unic = uniqid();
	$id = $class.'_'.$attribute.'_'.$unic;
?>
<div>
  <img src="/images/captcha.php" id="<?=$id?>" style="width: 100px;height: 38px;"/>
  <a id="refresh-captcha"><i class="fa fa-refresh" aria-hidden="true"></i>refresh</a>
  <input type="text" name="<?=$class.'['.$attribute.']'?>" <?=$attrs?> />
	<?php require 'error.php'; ?>
</div>
<script>
    document.querySelector('#refresh-captcha').addEventListener('click', function(e){
      document.querySelector('#<?=$id?>').setAttribute('src', '/images/captcha.php');
    });
</script>