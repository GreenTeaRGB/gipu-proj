<?php
$attrs = ' ';

if(isset($settings['attr']) && is_array($settings['attr'])){
  foreach($settings['attr'] as $attr => $value){
    $attrs .= $attr.'="'.$value.'" ';
  }
}
$id = strtolower($class).'-'.$attribute;
?>

  <label for="<?=$id?>">
    <p><?=isset($settings['label'])?$settings['label']:$class.'['.$attribute.']'?></p>
      <input type="text" name="<?=$class.'['.$attribute.']'?>" id="<?=$id?>" <?=$attrs?> value="<?=isset($this->builder->getModel()->$attribute) ? $this->builder->getModel()->$attribute : '' ?>" >
    <?php require 'error.php'; ?>
  </label>


