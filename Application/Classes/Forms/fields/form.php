<?php
  $formIdentifer = uniqid();
  $modalId       = uniqid();
  $attrs         = ' ';
  foreach( $this->builder->getAttrForm() as $attr => $value )
    $attrs .= $attr.'="'.$value.'" ';
  $modalSettings = $this->builder->getAttrModal();
  if( $modal ){ ?>
    <!-- Button trigger modal -->
    <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#modal<?= $modalId ?>">
      <?= isset( $modalSettings['button'] )
          ? $modalSettings['button']
          : 'click' ?>
    </button>

<!-- Modal -->
<div class="modal fade" id="modal<?= $modalId ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"
                    id="myModalLabel"><?= isset( $modalSettings['title'] )
                      ? $modalSettings['title']
                      : 'title' ?></h4>
            </div>
            <div class="modal-body">
              <?php } ?>
                <form data-form-name="form<?= $formIdentifer ?>" <?= $attrs ?> >
                  <?= $merge ?>
                  <?php
                    $attrs = ' ';
                    if( isset( $settings['submit']['attr'] ) && is_array( $settings['submit']['attr'] ) ) {
                      foreach( $settings['submit']['attr'] as $attr => $value ) {
                        $attrs .= $attr.'="'.$value.'" ';
                      }
                    }
                    if( !$modal ) {
                      ?>
                        <div>
                            <input type="submit" name="submit_<?= $this->builder->getModel()->getBuilderFormName() ?>"
                                   id="submit_<?= $this->builder->getModel()->getBuilderFormName() ?>"
                                <?php if( isset( $this->fields['submit']['attr'] ) ) {
                                  foreach( $this->fields['submit']['attr'] as $f => $v ) {
                                    echo $f.'="'.$v.'"';
                                  }
                                } ?>
                                   value="<?= isset( $this->fields['submit']['label'] )
                                       ? $this->fields['submit']['label']
                                       : 'Отправить' ?>">
                        </div>
                    <?php } ?>
                  <?php foreach( $this->builder->getLinks() as $link ) {
                    $attr = '';
                    if( isset( $link['attr'] ) )
                      foreach( $link['attr'] as $at => $el )
                        $attr .= $at.'="'.$el.'"';
                    echo '<a href="'.$link['url'].'" '.$attr.'>'.$link['name'].'</a>';
                  } ?>
                </form>
              <?php if( $modal ){ ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default"
                        data-dismiss="modal"><?= isset( $modalSettings['button_close'] )
                      ? $modalSettings['button_close']
                      : 'Закрыть' ?></button>
                <div>
                    <input type="submit" name="submit_<?= $this->builder->getModel()->getBuilderFormName() ?>"
                           id="submit_<?= $this->builder->getModel()->getBuilderFormName() ?>"
                        <?php if( isset( $this->fields['submit']['attr'] ) )
                          foreach( $this->fields['submit']['attr'] as $f => $v )
                            echo $f.'="'.$v.'"';
                        ?>
                           value="<?= isset( $modalSettings['button_save'] )
                               ? $modalSettings['button_save']
                               : 'Отправить' ?>">
                </div>
            </div>
        </div>
    </div>
</div>
<?php } ?>
<script>
  <?php
  $class_name = $this->builder->getModel()->formName();
  foreach( $this->builder->getModel()->getActiveValidators() as $activeValidator ) {
    if( $activeValidator->enableClientValidation ) {
      foreach( $activeValidator->attributes as $attribute ) {
        if( !is_null( $activeValidator->clientValidateAttribute( $this->builder->getModel(), $attribute, false ) ) ) {
          $attribute_form = $class_name.'['.$attribute.']';
          $option         = $activeValidator->clientValidateAttribute( $this->builder->getModel(), $attribute, false );
          if( $activeValidator instanceof \Application\Validators\EachValidator )
            $attribute_form .= '[]';
          $option['attribute'] = $attribute_form;
          if( isset( $option['compareAttribute'] ) )
            $option['compareAttribute'] = $class_name.'['.$option['compareAttribute'].']';
          $validation[$attribute_form][] = $option;
          if( $activeValidator instanceof \Application\Validators\CompareValidator ) {
            $option                                              = $activeValidator->clientValidateAttribute( $this->builder->getModel(), $attribute, false );
            $option['compareAttribute']                          = $attribute_form;
            $option['attribute']                                 = $class_name.'['.$attribute.'_repeat]';
            $validation[$class_name.'['.$attribute.'_repeat]'][] = $option;
          }
        }
      }
    }
  }
  foreach( $this->builder->getMergeModel() as $model ) {
    foreach( $model->getModel()->getActiveValidators() as $activeValidator ) {
      if( $activeValidator->enableClientValidation ) {
        foreach( $activeValidator->attributes as $attribute ) {
          if( !is_null( $activeValidator->clientValidateAttribute( $this->builder->getModel(), $attribute, false ) ) ) {
            $attribute_form = $class_name.'['.$attribute.']';
            if( $activeValidator instanceof \Application\Validators\EachValidator )
              $attribute_form .= '[]';
            $option                             = $activeValidator->clientValidateAttribute( $this->builder->getModel(), $attribute, false );
            $option['attribute']                = $attribute_form;
            $validation[$attribute_form][]      = $option;
            $validation[$attribute_form.'[]'][] = $option;
            if( $activeValidator instanceof \Application\Validators\CompareValidator ) {
              $option                                              = $activeValidator->clientValidateAttribute( $this->builder->getModel(), $attribute, false );
              $option['compareAttribute']                          = $attribute_form;
              $option['attribute']                                 = $class_name.'['.$attribute.'_repeat]';
              $validation[$class_name.'['.$attribute.'_repeat]'][] = $option;
            }
          }
        }
      }
    }
  }
  ?>
  $("form[data-form-name='form<?=$formIdentifer?>']").formValidator(<?=json_encode( $validation, JSON_UNESCAPED_UNICODE | JSON_HEX_QUOT | JSON_HEX_TAG | JSON_HEX_AMP | JSON_HEX_APOS )?>);


  <?php  if($ajax){  ?>
  $('form[data-form-name="form<?=$formIdentifer?>"]').submit(function (e) {
      e.preventDefault();
      var data = new FormData($('form[data-form-name="form<?=$formIdentifer?>"]')[0]);
      var options = <?=json_encode( $validation )?> ;
      data.append('submit_<?=$this->builder->getModel()->getBuilderFormName()?>', true);
      messages = [];
      var valid = true;
      for (var inputName of data.entries()) {
          var option = options[inputName[0]];
          if (option != undefined) {
              option.forEach(function (val) {
                  validation.init(inputName[1], val);
              });
          }
          if (validation.messages.length > 0) {
              valid = false;
              var element = $('form[data-form-name="form<?=$formIdentifer?>"] *[name="' + inputName[0] + '"]');
              $('em.invalid', element.parent()).remove();
              element.addClass('invalid');
              $(element.parent()).append('<em class="invalid">' + validation.messages[0] + '</em>');

          }else{
              var element = $('form[data-form-name="form<?=$formIdentifer?>"] *[name="' + inputName[0] + '"]');
              element.removeClass('invalid').addClass('valid');
          }
          validation.messages = [];

      }
      if (!valid) return false;
      $.ajax({
          type: "POST",
          processData: false,
          contentType: false,
          url: '<?=$this->builder->getAttrForm()['action']?> ',
          data: data,
          dataType: 'json',
          beforeSend: function () {

          },
          success: function (data) {
              if (data.success && !data.error) {
                  if (data.message)
                      alert(data.message);
                  if(data.redirect){
                      window.location.href = data.redirect;
                  }
                  $('form[data-form-name="form<?=$formIdentifer?>"]')[0].reset();
                  $('em.invalid').remove();
                <?php if($modal){ ?>
                  $('#modal<?= $modalId ?>').fadeOut(400);
                <?php } ?>
              } else {
                  if (data.error) {
                      $('em.invalid').remove();
                      $.each(data.error, function (attribute, message) {
                          $('form[data-form-name="form<?=$formIdentifer?>"] *[name="<?=$this->builder->getModel()->formName()?>[' + attribute + ']"]').addClass('invalid')
                              .parent()
                              .append('<em class="invalid">' + message + '</em>');
                      });
                  }
              }
          },
          error: function (data) {
              console.log('err');
              console.log(data);
          }
      });

      return false;

  });
  <?php  } ?>
</script>