<?php
	$attrs = ' ';

	if(isset($settings['attr']) && is_array($settings['attr'])){
		foreach($settings['attr'] as $attr => $value){
			$attrs .= $attr.'="'.$value.'" ';
		}
	}
	$id = strtolower($class).'-'.$attribute;
?>

	<label for="<?=$id?>">
		<p><?=isset($settings['label'])?$settings['label']:$class.'['.$attribute.']'?></p>
        <input type="password" name="<?=$class.'['.$attribute.']'?>" id="<?=$id?>" <?=$attrs?> >
      <?php require 'error.php'; ?>
	</label>


