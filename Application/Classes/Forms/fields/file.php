<?php
	$attrs = ' ';
	if(isset($settings['attr']) && is_array($settings['attr'])){
		foreach($settings['attr'] as $attr => $value){
			$attrs .= $attr.'="'.$value.'" ';
		}
	}
	$multi = isset($settings['multi']) && $settings['multi']?true:false;
?>
<div>
	<label for="<?=$class.'['.$attribute.']'?>">
		<?=isset($settings['label'])?$settings['label']:$attribute?>
	</label>
  <input type="hidden" name="<?=$class.'['.$attribute.']'?><?=$multi?'[]':''?>" <?=$attrs?> value="" >
	<input type="file" name="<?=$class.'['.$attribute.']'?><?=$multi?'[]':''?>" id="<?=$class.'['.$attribute.']'?>" <?=$attrs?> value="" <?=$multi?'multiple':''?>>
	<?php require 'error.php'; ?>
</div>