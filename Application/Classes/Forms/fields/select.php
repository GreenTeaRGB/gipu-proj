
<?php
	$attrs = ' ';
	if(isset($settings['attr']) && is_array($settings['attr'])){
		foreach($settings['attr'] as $attr => $value){
			$attrs .= $attr.'="'.$value.'" ';
		}
	}

?>

	<label for="<?=$class.'['.$attribute.']'?>">
		<p><?=isset($settings['label'])?$settings['label']:$class.'['.$attribute.']'?></p>
		<select name="<?=$class.'['.$attribute.']'?>" id="<?=$class.'['.$attribute.']'?>" <?=$attrs?> >
			<?php if(isset($settings['values']) && isset($settings['id']) && isset($settings['name'])){
				foreach($settings['values'] as $item) { ?>
					<option value="<?=$item[$settings['id']]?>" <?=isset($this->builder->getModel()->$attribute)
					&& $item[$settings['id']] == $this->builder->getModel()->$attribute ? 'selected' : ''?>><?=$item[$settings['name']]?></option>
				<?php	}
			} ?>
		</select>
	</label>
	<?php require 'error.php'; ?>
