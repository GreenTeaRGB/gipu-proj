
<?php
	$attrs = ' ';
	if(isset($settings['attr']) && is_array($settings['attr'])){
		foreach($settings['attr'] as $attr => $value){
			$attrs .= $attr.'="'.$value.'" ';
		}
	}
?>
<div>
	<label for="<?=$class.'['.$attribute.']'?>">
		<?=isset($settings['label'])?$settings['label']:$class.'['.$attribute.']'?>
	</label>
	<select name="<?=$class.'['.$attribute.']'?>[]" id="<?=$class.'['.$attribute.']'?>" <?=$attrs?> multiple>
		<?php if(isset($settings['values']) && isset($settings['id']) && isset($settings['name'])){
			foreach($settings['values'] as $item) { ?>
				<option value="<?=$item[$settings['id']]?>" <?=isset($this->builder->getModel()->$attribute) && in_array($item[$settings['id']], $this->builder->getModel()->$attribute) ? 'selected' : ''?>><?=$item[$settings['name']]?></option>
			<?php	}
		} ?>
	</select>
	<?php require 'error.php'; ?>
</div>