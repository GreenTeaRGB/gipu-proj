<?php
  $attrs = ' ';

  if( isset( $settings['attr'] ) && is_array( $settings['attr'] ) ) {
    foreach( $settings['attr'] as $attr => $value ) {
      $attrs .= $attr.'="'.$value.'" ';
    }
  }
  $id = strtolower( $class ).'-'.$attribute;
?>

<label for="<?= $id ?>">
    <p><?= isset( $settings['label'] )
          ? $settings['label']
          : $class.'['.$attribute.']' ?></p>
    <input type="tel" pattern="[\+]\d{1}\s[\(]\d{3}[\)]\s\d{3}[\-]\d{2}[\-]\d{2}" minlength="12"
           name="<?= $class.'['.$attribute.']' ?>" id="<?= $id ?>" <?= $attrs ?>
           value="<?= isset( $this->builder->getModel()->$attribute )
               ? $this->builder->getModel()->$attribute
               : '' ?>">
  <?php require 'error.php'; ?>
</label>


