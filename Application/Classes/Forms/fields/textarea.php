<?php
	$attrs = ' ';
	if(isset($settings['attr']) && is_array($settings['attr'])){
		foreach($settings['attr'] as $attr => $value){
			$attrs .= $attr.'="'.$value.'" ';
		}
	}
?>

	<label for="<?=$class.'['.$attribute.']'?>">
		<p><?=isset($settings['label'])?$settings['label']:$class.'['.$attribute.']'?></p>
        <textarea name="<?=$class.'['.$attribute.']'?>" id="<?=$class.'['.$attribute.']'?>" <?=$attrs?> ><?=isset($settings['default']) && !isset($this->builder->getModel->$attribute)?$settings['default']:''?><?=isset($this->builder->getModel()->$attribute) ? $this->builder->getModel()->$attribute : '' ?>
  </textarea>
      <?php require 'error.php'; ?>
	</label>