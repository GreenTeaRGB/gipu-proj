<?php

  namespace Application\Classes\Forms;

  use Application\Classes\Forms\FormBuilderHtml;
  use Application\Classes\Forms\Interfaces\FormBuilderInterface;

  /**
   * Class FormBuilder
   * @package Application\Classes\Forms
   */
  class FormBuilder
  {

    /**
     * @var bool
     */
    private $ajax = false;
    /**
     * @var bool
     */
    private $client_validate = false;
    /**
     * @var \Application\Classes\Forms\Interfaces\FormBuilderInterface
     */
    private $model;
    /**
     * @var array
     */
    private $mergeForms = [];
    /**
     * @var string
     */
    private $modelName = '';

    /**
     * @var array
     */
    private $fields = [];

    /**
     * @var string
     */
    private $successMesssage = 'Успешно сохранено';

    /**
     * @var string
     */
    private $errorMessage = 'Ошибка при сохраненинии';

    /**
     * @var bool
     */
    private $isModal = false;

    /**
     * @var array
     */
    private $attr_form = [];

    /**
     * @var array
     */
    private $attr_modal = [];

    /**
     * @var array
     */
    private $mergeModel = [];

    /**
     * @var array
     */
    private $links = [];

    /**
     * FormBuilder constructor.
     * @param \Application\Classes\Forms\Interfaces\FormBuilderInterface $model
     */
    public function __construct( FormBuilderInterface $model )
    {
      $this->model     = $model;
      $className       = new \ReflectionClass( $model );
      $this->modelName = $className->getShortName();
      $this->attr_form = [ 'method' => 'POST', 'enctype' => 'multipart/form-data', 'action' => '' ];
    }

    /**
     * @return bool
     */
    public function isAjax():bool
    {
      $headers = getallheaders();
      if( isset( $headers['X-Requested-With'] ) && $headers['X-Requested-With'] == 'XMLHttpRequest' )
        return true;
      else
        return false;
    }

    /**
     * @return $this
     */
    public function ajax()
    {
      $this->ajax = true;
      return $this;
    }

    /**
     * @return bool
     */
    public function formAjax()
    {
      return $this->ajax;
    }

    /**
     * @return bool
     */
    public function isSubmit():bool
    {
      if( isset( $_POST['submit_'.$this->model->getBuilderFormName()] ) )
        return true;
      else
        return false;
    }


    /**
     * @return mixed
     */
    public function createForm()
    {
      $html = new FormBuilderHtml( $this );
      return $html->getForm();
    }

    /**
     * @return array
     */
    public function getFields():array
    {
      return $this->fields;
    }


    /**
     * @param \Application\Classes\Forms\FormBuilder $form
     * @throws \Exception
     */
    public function merge( FormBuilder $form )
    {
      if( $this->modelName == $form->getModelName() )
        throw new \Exception( 'Объеденение одинаковых моделей '.$this->modelName );
      $this->mergeForms[$form->getModelName()] = new FormBuilderHtml( $form );
      $this->mergeModel[]                      = $form;

      return $this;
    }

    /**
     * @return array
     */
    public function getMergeModel():array
    {
      return $this->mergeModel;
    }

    /**
     * @return array
     */
    public function getAttrForm():array
    {
      return $this->attr_form;
    }

    /**
     * @param array $attr_form
     */
    public function setAttrForm( array $attr_form )
    {
      $this->attr_form = array_merge( $this->attr_form, $attr_form );
      return $this;
    }

    /**
     * @return array
     */
    public function getAttrModal():array
    {
      return $this->attr_modal;
    }

    /**
     * @param array $attr_modal
     */
    public function setAttrModal( array $attr_modal )
    {
      $this->attr_modal = $attr_modal;
      return $this;
    }


    /**
     * @param $action
     * @return $this
     */
    public function action( $action )
    {
      $this->attr_form['action'] = $action;
      return $this;
    }

    /**
     * @return \Application\Classes\Forms\Interfaces\FormBuilderInterface
     */
    public function getModel()
    {
      return $this->model;
    }

    /**
     * @return array
     */
    public function getMergeForms():array
    {
      return $this->mergeForms;
    }

    /**
     * @return string
     */
    public function getModelName():string
    {
      return $this->modelName;
    }

    /**
     * @param bool $is_ajax_check
     * @return $this
     */
    public function setIsAjaxCheck( bool $is_ajax_check )
    {
      $this->is_ajax_check = $is_ajax_check;
      return $this;
    }

    /**
     * @return bool
     */
    public function isClientValidate():bool
    {
      return $this->client_validate;
    }

    /**
     * @param bool $client_validate
     */
    public function setClientValidate( bool $client_validate ):void
    {
      $this->client_validate = $client_validate;
    }

    /**
     * @return $this
     */
    public function modal()
    {
      $this->isModal = true;
      return $this;
    }

    /**
     * @return bool
     */
    public function isModal():bool
    {
      return $this->isModal;
    }

    /**
     * @return string
     */
    public function getSuccessMesssage():string
    {
      return $this->successMesssage;
    }

    /**
     * @param string $successMesssage
     */
    public function setSuccessMesssage( string $successMesssage ):void
    {
      $this->successMesssage = $successMesssage;
    }

    /**
     * @return string
     */
    public function getErrorMessage():string
    {
      return $this->errorMessage;
    }

    /**
     * @param string $errorMessage
     */
    public function setErrorMessage( string $errorMessage ):void
    {
      $this->errorMessage = $errorMessage;
    }


    /**
     * @return array
     */
    public function getLinks():array
    {
      return $this->links;
    }

    /**
     * @param array $links
     */
    public function setLinks( array $links )
    {
      $this->links = $links;
      return $this;
    }


  }

  ?>