<?php
namespace Application\Classes\Forms;
class FormBuilderHtml
{

	private $builder;
	private $html_fields = '';
	private $fields = [];
	const PATH_TEMPLATE = FOLDER . 'Application/Classes/Forms/fields/';

	public function __construct(FormBuilder $builder)
	{
		$this->builder = $builder;
	}

	private function generate()
	{
		$model =  $this->builder->getModel();
		$this->html_fields = '<div class="' . $this->builder->getModelName() . '_wrap">';
		$this->fields =$model->getFieldsFormBuilder();
		foreach($this->fields as $name => $settings) {
			$this->html_fields .= $this->field( $this->builder->getModelName() , $settings, $name, $model->getErrors() );
			if(isset($settings['confirm'])){
				if(isset($settings['label'])){
					$settings['label'] = 'Повторите '.strtolower($settings['label']);
				}
				$this->html_fields .= $this->field( $this->builder->getModelName(), $settings, $name.'_repeat', $model->getErrors() );
			}
		}
		$this->html_fields .= '</div>';
		return $this;
	}

	public function getForm()
	{
		$merge = $this->getHtmlFields();
		foreach($this->builder->getMergeForms() as $mergeForm) {
			$merge .= $mergeForm->getHtmlFields();
		}
		$modal = $this->builder->isModal();
		$ajax = $this->builder->formAjax();
		$template = self::PATH_TEMPLATE . 'form.php';
		if(file_exists($template)) {
			ob_start();
			require $template;
			$content = ob_get_contents();
			ob_end_clean();
			\Application\Classes\Session::remove('error_form');
			return $content;
		}
	}

	/**
	 * @return string
	 */
	public function getHtmlFields(): string
	{
		$this->generate();
		return $this->html_fields;
	}
	
	private function field(string $class, array $settings, string $attribute, array $errors)
	{
		$template = self::PATH_TEMPLATE . $settings['type'] . '.php';
		$settings['label'] = $this->builder->getModel()->getAttributeLabel($attribute);
		if(file_exists($template)) {
			$errors = \Application\Classes\Session::getByKey('error_form');
			ob_start();
			require $template;
			$content = ob_get_contents();
			ob_end_clean();
			return $content;
		}
	}
}
?>