<?php

	namespace Application\Classes\Forms\Interfaces;
	interface FormBuilderInterface
	{
		public function rules() : array ;
		public function formFields(array $data = []);
		public function attributeLabels() : array ;
	}

	?>