<?php

	namespace Application\Classes;

	use \Application\Helpers\UserHelper;
	use \Application\Classes\Template;

  /**
   * Class ControllerAdminBase
   * @package Application\Classes
   */
  Abstract Class ControllerAdminBase extends ControllerBase
	{
    /**
     * @var
     */
    protected $actionModel;
    /**
     * @var string
     */
    private $layouts = 'adminMain';

    /**
     * ControllerAdminBase constructor.
     * @throws \ReflectionException
     */
    public function __construct()
		{
		  $ref = new \ReflectionClass($this);
      $class = str_replace(['Admin', 'Controller'], '', $ref->getShortName());
		  $modelClass = '\Application\Models\\'.$class;
		  if(class_exists($modelClass))
		    $this->actionModel = new $modelClass();
			$this->template = new Template( $this->layouts, get_class( $this ), 'admin' );
			self::setModule('admin');
			if( !UserHelper::isAdmin() ) {
				$this->redirect( '/user/login' );
			}
			$user        = \Application\Helpers\UserHelper::getUser();
			$innerRoutes = array( '/admin', '/admin/getCrosFile' );
			$pages       = \Application\Models\AdminPage::getListByUserGroup( $user[ 'user_group_id' ], false );
			$access      = false;
			foreach( $pages as $page ) {
				if( strpos( $_SERVER[ 'REQUEST_URI' ], $page[ 'uri' ] ) !== false || in_array( $_SERVER[ 'REQUEST_URI' ], $innerRoutes ) )
					$access = true;
			}

			if( !$access )
				die( 'Access denied' );
		}

    /**
     * @param int $page
     * @return bool
     */
    public function actionIndex( $page = 1 )
    {
      $builder = new AdminBuilder( $this->actionModel, [] );
      $builder->index( $page );
      return true;
    }

    /**
     * @return bool
     */
    public function actionCreate()
    {
      $builder = new AdminBuilder( $this->actionModel );
      $builder->create();
      return true;
    }

    /**
     * @param $id
     * @return bool
     */
    public function actionEdit( $id )
    {
      $builder = new AdminBuilder( $this->actionModel );
      $builder->edit( $id );
      return true;
    }

    /**
     * @return bool
     */
    public function actionActivate()
    {
      $builder = new AdminBuilder( $this->actionModel );
      $builder->active();
      return true;
    }

    /**
     * @return bool
     */
    public function actionSortable()
    {
      $builder = new AdminBuilder( $this->actionModel );
      $builder->sort();
      return true;
    }

    /**
     * @return bool
     */
    public function actionDelete()
    {
      $builder = new AdminBuilder( $this->actionModel );
      $builder->delete();
      return true;
    }

    /**
     * @return bool
     */
    public function actionModal()
    {
      $builder = new AdminBuilder( $this->actionModel );
      $builder->modal();
      return true;
    }

    /**
     * @param $id
     * @return bool
     */
    public function actionView( $id){
      $builder = new AdminBuilder( $this->actionModel );
      $builder->view( $id );
      return true;
    }


    /**
     * @param $view
     * @param array $params
     * @param bool $content
     * @return bool|string
     */
    public function render( $view, $params = [], $content = false )
		{
			foreach( $params as $key => $param ) {
				$this->template->vars( $key, $param );
			}
			return $this->template->view( $view, $content );

		}
	}
