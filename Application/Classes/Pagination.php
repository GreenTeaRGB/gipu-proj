<?php

namespace Application\Classes;

class Pagination
{

   
    private $max = 10;

    
    private $index = 'page';

   
    private $current_page;

   
    private $total;

   
    private $limit;

    
    public function __construct($total, $currentPage, $limit, $index)
    {
        if (strpos($_SERVER['REQUEST_URI'], 'admin/products')) $this->max = 5;
            
        $this->total = $total;

       
        $this->limit = $limit;

        
        $this->index = $index;

       
        $this->amount = $this->amount();
        
        
        $this->setCurrentPage($currentPage);
    }

   
    public function get()
    {
      if($this->amount <= 1) return '';
        $links = null;
        $limits = $this->limits();
        
        $html = '<ul class="center-block catalog__pages pagination">';
        
        for ($page = $limits[0]; $page <= $limits[1]; $page++) {

            if ($page == $this->current_page) {
                $links .= '<li class="catalog__pages_current"><a href="#">' . $page . '</a></li>';
            } else {
               
                $links .= $this->generateHtml($page);
            }
        }

        
        if (!is_null($links)) {
            
            if ($this->current_page > 1)
            
                $links = $this->generateHtml(1, '<img src="/media/svg/r_arrow_2.svg" class="rotate" alt="r_arrow">') . $links;

           
            if ($this->current_page < $this->amount)
            
                $links .= $this->generateHtml($this->amount, '<img src="/media/svg/r_arrow_2.svg" alt="r_arrow">');
        }

        $html .= $links . '</ul>';

       
        return $html;
    }

   
    private function generateHtml($page, $text = null)
    {
        $page = intval($page);
        if (!$text)
        
            $text = $page;
        $q_str = '';
        if (strpos($_SERVER['REQUEST_URI'], '?') === false) $link = $_SERVER['REQUEST_URI'];
        else
        {
            $items = explode('?', $_SERVER['REQUEST_URI']);
            $link = $items[0];
            $q_str = '?' . $items[1];
        }
       
        $currentURI = rtrim($link, '/') . '/';
         
        $currentURI = preg_replace('~/page-[0-9]+~', '', $currentURI);

        return
                '<li><a href="' . $currentURI . $this->index . $page . $q_str . '">' . $text . '</a></li>';
    }

   
    private function limits()
    {
       
        $left = $this->current_page - round($this->max / 2);
        
        
        $start = $left > 0 ? $left : 1;

        
        if ($start + $this->max <= $this->amount) {
        
            $end = $start > 1 ? $start + $this->max : $this->max;
        } else {
           
            $end = $this->amount;

           
            $start = $this->amount - $this->max > 0 ? $this->amount - $this->max : 1;
        }

       
        return
                array($start, $end);
    }

    
    private function setCurrentPage($currentPage)
    {
        
        $this->current_page = $currentPage;

       
        if ($this->current_page > 0) {
           
            if ($this->current_page > $this->amount)
            
                $this->current_page = $this->amount;
        } else
        
            $this->current_page = 1;
    }

   
    private function amount()
    {
       
        return ceil($this->total / $this->limit);
    }

}
