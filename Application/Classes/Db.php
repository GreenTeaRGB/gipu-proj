<?php
namespace Application\Classes;
class Db
{

	private static $instance = null;

	public static function getInstance()
	{
		if (null === self::$instance)
		{
			self::$instance = new self();
		}
		return self::$instance;
	}

	private function __clone() {}
	private function __construct() {}

	public function getConnection()
	{
		// Получаем параметры подключения из файла
		$paramsPath = ROOT . '/config/db_params.php';
		$params = include($paramsPath);

		// Устанавливаем соединение
		$dsn = "mysql:host={$params['host']};dbname={$params['dbname']}";
		$db = new \PDO($dsn, $params['user'], $params['password']);

		// Задаем кодировку
		$db->exec("set names utf8");
			$db->exec("set sql_mode='NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';");

		return $db;
	}



}