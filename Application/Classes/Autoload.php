<?php
spl_autoload_register('Autoloader::autoload');

class Autoloader
{
	const debug = false;
	
	public function __construct()
	{
	
	}
	
	public static function autoload($file)
	{
		$file = str_replace('\\', '/', $file);
		$path = $_SERVER['DOCUMENT_ROOT'];
		$filepath = $path . '/' . $file . '.php';
		if (file_exists($filepath)) {
			require_once($filepath);
		} else {
			$flag = true;
			if (Autoloader::debug) return Autoloader::recursive_autoload($file, $path, $flag);
		}
	}
	
	public static function recursive_autoload($file, $path, &$flag)
	{
		$res = false;
		if (false !== ($handle = opendir($path)) && $flag) {
			while (false !== ($dir = readdir($handle)) && $flag) {
				if (strpos($dir, '.') === false) {
					$path2 = $path . '/' . $dir;
					$filepath = $path2 . '/' . $file . '.php';
					if (file_exists($filepath)) {
//				
						if (Autoloader::debug) Autoloader::StPutFile((' Нашли файл в ' . $filepath));
						return $filepath;
					}
					$res = Autoloader::recursive_autoload($file, $path2, $flag);
				}
			}
			closedir($handle);
		}
		return $res;
	}

	private static function StPutFile($data)
	{
	}
}


