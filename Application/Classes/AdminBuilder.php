<?php

namespace Application\Classes;

use Application\Helpers\Image;
use Application\Helpers\UploadedFile;
use Application\Helpers\UserHelper;
use Application\Models\AdminPage;
use Application\Validators\ImageValidator;
use function array_column;
use function array_flip;
use function array_keys;
use function array_merge;
use function count;
use function header;
use function is_array;
use function serialize;
use function sort;
use function strpos;
use function var_dump;

/**
 * Class AdminBuilder
 * @package Application\Classes
 */
class AdminBuilder extends ObjectMain
{
  /**
   * @var
   */
  private $model;
  /**
   * @var string
   */
  private $layouts = 'adminMain';
  /**
   * @var array
   */
  private $white_ext;
  /**
   * @var
   */
  private $fields;
  /**
   * @var
   */
  private $user;
  /**
   * @var
   */
  private $item;
  /**
   * @var
   */
  private $items;
  /**
   * @var array
   */
  private $merge = [];
  /**
   * @var array
   */
  private $mergeFields = [];
  /**
   * @var array
   */
  private $tabs;
  /**
   * @var int
   */
  private $lim = 15;
  /**
   * @var bool
   */
  public $tree = false;
  /**
   * @var null
   */
  private $result = null;
  /**
   * @var array
   */
  private $options;
  /**
   * @var array
   */
  private $errors = [];
  /**
   * @var string
   */
  private $successMessage = 'Успешно сохранено!';
  /**
   * @var string
   */
  private $errorMessage = 'Ошибка сохранения!';
  /**
   * @var array
   */
  private $events = [];
  /**
   * @var array
   */
  private $detailTables = [];
  /**
   * @var
   */
  private $view;
  /**
   * @var
   */
  private $page_current;
  /**
   * @var array
   */
  private $where = [];
  /**
   * @var array
   */
  private $like = [];
  /**
   * @var array
   */
  private $orWhere = [];

  /**
   * AdminBuilder constructor.
   * @param       $model
   * @param array $tabs
   * @param array $options
   */
  public function __construct( $model, $tabs = [], $options = [] )
  {
    $this->model    = $model;
    $this->tabs     = $tabs;
    $this->options  = $options;
    $this->user     = UserHelper::getUser();
    $this->template = new Template( $this->layouts, get_class( $this ), 'admin' );
    $this->template->vars( 'user', $this->user );
    $this->white_ext = array( 'jpg', 'jpeg', 'gif', 'png', 'doc', 'docx', 'xls', 'xlsx', 'pdf' );
    $this->fields    = $this->model->getFields();
  }

  /**
   * @param int $page
   * @return bool
   */
  public function index( $page = 1 )
  {
    $uri = str_replace( '/admin/', '', $_SERVER['REDIRECT_URL'] );
    $uri = preg_replace( '~/page-[0-9]+~', '', $uri );

    $this->page_current = AdminPage::findOne( [ 'uri' => $uri ] );

    if ( isset( $_GET['filter'] ) ) {
      foreach ( $_GET['filter'] as $field => $value ) {
        if ( strpos( $field, '[' ) !== false && strpos( $field, ']' ) === false ) {
          $field .= ']';
        }
        if ( $value == '' )
          continue;
        if ( $this->fields[$field]['type'] == 'date' ) {
          if ( isset( $value['from'] ) && $value['from'] !== '' ) {
            $this->where ['>=' . $field] = strtotime( $value['from'] );
          }
          if ( isset( $value['to'] ) && $value['to'] !== '' ) {
            $this->where ['<=' . $field] = strtotime( $value['to'] );
          }
        } else {
          if ( is_array( $value ) ) {
            sort( $value );
            $value = self::serialize( $value );
          }
          list( $del, $value ) = $this->model->getDelimiter( $value );
          if ( $del != '=' )
            $this->where[$del . $field] = $value;
          else
            $this->like[$field] = $value;
        }
      }
//      if ( count( $this->where  ) > 0 ) {
//        $this->model->where( $this->where );
//      }
//      if ( count( $like ) > 0 )
//        $this->model->like( $this->like, 'AND' );
    }
    //для количества
    if ( count( $this->where ) > 0 )
      $this->model->where( $this->where );
    if ( count( $this->orWhere ) > 0 )
      $this->model->orWhere( $this->orWhere );
    if ( count( $this->like ) > 0 )
      $this->model->like( $this->like, 'AND' );
    $count = $this->model->count();

    if ( $this->tree ) {
      if ( isset( $this->options['sortable'] ) && !isset( $_GET['sort'] ) ) {
        $this->model->order( [ $this->options['sortable'] => 'ASC' ] );
      }
      $this->items = $this->model->fetchAll();
      $this->template->vars( 'treeHtml', $this->tree( $this->items ) );
    } else {


      if ( isset( $this->options['sortable'] ) && !isset( $_GET['sort'] ) || isset( $_GET['sort'] ) && $_GET['by'] ) {
        if ( isset( $this->options['sortable'] ) && !isset( $_GET['sort'] ) ) {
          $this->model->order( [ $this->options['sortable'] => 'ASC' ] );
        }
        if ( isset( $_GET['sort'] ) && $_GET['by'] ) {
          $this->model->order( [ $_GET['sort'] => $_GET['by'] ] );
        }
      } else {
        $this->model->order( [ 'id' => 'DESC' ] );
      }


      $pagination  = new Pagination( $count, $page, $this->lim, 'page-' );
      $count_pages = ceil( $count / $this->lim );
      $start       = ( $page - 1 ) * $this->lim;
      // для элементов
      if ( count( $this->where ) > 0 )
        $this->model->where( $this->where );
      if ( count( $this->orWhere ) > 0 )
        $this->model->orWhere( $this->orWhere );
      if ( count( $this->like ) > 0 )
        $this->model->like( $this->like, 'AND' );
      $this->items = $this->model->limit( $start, $this->lim )->fetchAll();
    }
    $this->prepareAll();
    $this->breadcrumbs ['Главная']                   = '/';
    $this->breadcrumbs ['Админпанель']               = '/admin';
    $this->breadcrumbs [$this->page_current['name']] = '#';
//
//    $this->template->vars( 'title_page', $page_current['name'] );
//    $this->template->vars( 'page_current', $page_current );
//    $this->template->vars( 'items', $this->items );
//    $this->template->vars( 'tree', $this->tree );
//    $this->template->vars( 'tabs', $this->tabs );
//    $this->template->vars( 'options', $this->options );
//    $this->sortFields($this->fields);
//    $this->template->vars( 'fields', $this->fields );
    $this->template->vars(
      'pagination', isset( $count_pages ) && $count_pages > 1
      ? $pagination->get()
      : ''
    );
//    $this->template->vars( 'breads', $this->getBreadcrumbs() );
//    $this->template->view( 'index' );
    if ( $this->view == null ) {
      $this->setView( 'index' );
      $this->render();
    }
    return true;
  }

  /**
   * @param int  $id
   * @param bool $type
   * @return bool
   */
  public function create( $id = 0, $type = false )
  {
    $errors       = $this->getErrors();
    $this->errors = $errors
      ? array_merge( $this->errors, $errors )
      : $this->errors;
    $errorsMerge  = $this->getErrorsMerge();
    $this->errors = $errorsMerge
      ? array_merge( $this->errors, $errorsMerge )
      : $this->errors;
    if ( $type == 'parent' ) {
      $_POST['parent_id'] = $id;
    }
    if ( !$this->errors && $this->result == null && isset( $_POST['submit'] ) )
      $this->result = $this->insert( $id, $type );

    $this->item = $this->getFieldValues();

    if ( $type == 'copy' ) {
      $this->item = $this->model->where( [ 'id' => $id ] )->fetchOne();
    }
    $uri = str_replace( '/create', '', str_replace( '/admin/', '', $_SERVER['REQUEST_URI'] ) );
    $uri = preg_replace( '~/page-[0-9]+~', '', $uri );

    $page_current                              = AdminPage::findOne( [ 'uri' => $uri ] );
    $this->page_current                        = $page_current;
    if($this->result && !$this->errors){
      $this->backRedirect();
    }
    $this->breadcrumbs ['Главная']             = '/';
    $this->breadcrumbs ['Админпанель']         = '/admin';
    $this->breadcrumbs [$page_current['name']] = '/admin/' . $page_current['uri'];

    foreach ( $this->fields as $field => $option ) {
      if ( $option['type'] == 'mselect' && isset( $this->model->getTypes()[$field] ) && !isset( $option['merge'] ) )
        if ( isset( $option['merge'] ) ) {
          $this->item[$field] = [];
          $mergeClass         = new $option['merge']['class']();
          $mergeData          = $mergeClass::find( [ $option['merge']['field'] => $this->item[$option['merge']['item']] ] );
          if ( $mergeData )
            foreach ( $mergeData as $item_merge ) {
              $this->item[$field][] = $item_merge[$option['merge']['class_field']];
            }
        }
    }


    $this->prepare();
    $this->breadcrumbs ['Создать новую запись ' . $page_current['name']] = '#';
    //      $this->template->vars( 'breads', $this->getBreadcrumbs() );
    //      $this->template->vars( 'title_page', $page_current['name'] );
    //      $this->template->vars( 'fields', $this->fields );
    //      $this->template->vars( 'item', $this->item );
    //      $this->template->vars( 'url', $this->model->formName() );
    //      $this->template->vars( 'errors', $this->errors );
    //      $this->template->vars( 'result', $this->result );
    //      $this->template->vars( 'tabs', $this->getDetailTables() );
    //      $this->template->vars( 'page_current', $page_current );
    //      $this->template->vars( 'successMessage', $this->successMessage );
    //      $this->template->vars( 'errorMessage', $this->errorMessage );
    //      $this->template->vars( 'events', $this->events );
    if ( $this->view == null ) {
      $this->setView( 'create' );
      $this->render();
    }
    return true;
  }

  public function view( $id )
  {
    $this->setView( 'view' );
    $this->edit( $id );
    return $this->render();
  }

  /**
   * @param $id
   * @return bool
   */
  public function edit( $id )
  {
    $this->model->id = $id;
    $errors          = $this->getErrors();
    $this->errors    = $errors
      ? array_merge( $this->errors, $errors )
      : $this->errors;
    $mergeErrors     = $this->getErrorsMerge();
    $this->errors    = $mergeErrors
      ? array_merge( $this->errors, $mergeErrors )
      : $this->errors;
    if ( !$this->errors && $this->result == null && isset( $_POST['submit'] ) )
      $this->result = $this->update( $id );

    $uri = str_replace( '/admin/', '', $_SERVER['REQUEST_URI'] );
    $uri = preg_replace( '~/edit/[0-9]+~', '', $uri );
    $uri = preg_replace( '~/view/[0-9]+~', '', $uri );

    $page_current                              = AdminPage::findOne( [ 'uri' => $uri ] );
    $this->page_current                        = $page_current;
    if($this->result && !$this->errors){
      $this->backRedirect();
    }
    $this->breadcrumbs ['Главная']             = '/';
    $this->breadcrumbs ['Админпанель']         = '/admin';
    $this->breadcrumbs [$page_current['name']] = '/admin/' . $page_current['uri'];

    $this->breadcrumbs ['Запись ' . $page_current['name'] . ' #' . $id] = '#';

    $item = $this->model->where( [ 'id' => $id ] )->fetchOne();

    foreach ( $this->fields as $field => $option ) {
      if ( $option['type'] == 'mselect' && isset( $this->model->getTypes()[$field] ) && !isset( $option['merge'] ) )
        $item[$field] = ( $item[$field] );
      if ( isset( $option['merge'] ) ) {
        $item[$field] = [];
        $mergeClass   = new $option['merge']['class']();
        $mergeData    = $mergeClass::find( [ $option['merge']['field'] => $item[$option['merge']['item']] ] );
        if ( $mergeData )
          foreach ( $mergeData as $item_merge ) {
            $item[$field][] = $item_merge[$option['merge']['class_field']];
          }
      }
    }

    $this->item = $item;

    if ( !$item )
      die( 'Элемент не найден' );
    $this->prepare();
    //      $this->template->vars( 'item', $this->item );
    //      $this->template->vars( 'title_page', $this->page_current['name'] );
    //      $this->template->vars( 'page_current', $this->page_current );
    //      $this->template->vars( 'errors', $this->errors );
    //      $this->template->vars( 'url', $this->model->formName() );
    //      $this->template->vars( 'breads', $this->getBreadcrumbs() );
    //      $this->template->vars( 'result', $this->result );
    //      $this->template->vars( 'tabs', $this->getDetailTables() );
    //      $this->template->vars( 'successMessage', $this->successMessage );
    //      $this->template->vars( 'errorMessage', $this->errorMessage );
    //      $this->template->vars( 'fields', $this->fields );
    //      $this->template->vars( 'events', $this->events );
    //      $this->template->view('edit');
    if ( $this->view == null ) {
      $this->setView( 'edit' );
      $this->render();
    }
    return true;
  }

  /**
   *
   */
  public function sort()
  {
    if ( isset( $_POST['items'] ) && isset( $_POST['field'] ) ) {
      if ( isset( $_POST['parent_id'] ) && isset( $_POST['id'] ) )
        $this->model->setFields( [ $_POST['parent_field'] => $_POST['parent_id'] ] )->where( [ 'id' => $_POST['id'] ] )
                    ->update();

      $items = json_decode( $_POST['items'] );
      foreach ( $items as $item ) {
        list( $id, $value ) = $item;
        $this->model->setFields( [ $_POST['field'] => $value ] )->where( [ 'id' => $id ] )->update();
      }
    }
  }

  /**
   * @return bool
   */
  public function active()
  {
    if ( isset( $_POST['id'] ) && isset( $_POST['active'] ) && isset( $_POST['field'] ) ) {
      if ( $this->model->setFields( [ $_POST['field'] => $_POST['active'] ] )->where( [ 'id' => $_POST['id'] ] )
                       ->update() ) {
        echo json_encode( [ 'saved' => 1 ] );
      } else {
        echo json_encode( [ 'text' => 'Ошибка сохранения' ] );
      }
    }
    return true;
  }

  /**
   *
   */
  public function modal()
  {
    if ( isset( $_POST['search'] ) && isset( $_POST['field'] ) ) {
      $settings = $this->model->getFields()[$_POST['field']];
      $class    = new $settings['class']();
      $like     = [];
      foreach ( $settings['fields'] as $setting ) {
        $like[$setting] = $_POST['search'];
      }
      $page  = isset( $_POST['page'] )
        ? $_POST['page']
        : 1;
      $start = ( $page - 1 ) * $this->lim;
      if ( isset( $settings['where'] ) ) {
        $class->where( $settings['where'] );
      }
      $count = $class->like( $like )->count();
      if ( $count <= $this->lim )
        $start = 0;
      if ( isset( $settings['where'] ) ) {
        $class->where( $settings['where'] );
      }
      $result = $class->like( $like )->limit( $start, $this->lim )->fetchAll();
      echo json_encode( [ 'elements' => $result, 'count' => $count ] );
    }
  }

  /**
   * @param int  $id
   * @param bool $type
   * @return bool
   */
  public function insert( $id = 0, $type = false )
  {
    if ( isset( $_POST['submit'] ) ) {
      $this->getFields();
      //				foreach ($this->fields as $field => $options){
      //					if(isset($options['only']) && $options['only'] == 'one' && isset($_POST[$field]) && $_POST[$field] == 1){
      //						$this->model->setFields([$field =>0])->update();
      //					}
      //				}
      $insertsID = [];
      foreach ( $this->merge as $model ) {
        if ( $_POST[$model['field']] !== '' )
          $model['model']->setFields( $_POST[$model['className']] )
                         ->where( [ $this->fields[$model['field']]['id_key'] => $_POST[$model['field']] ] )->update();
        else {
          $inID                             = $model['model']->setFields( $_POST[$model['className']] )->insert();
          $insertsID[$model['className']][] = $inID;
          $_POST[$model['field']]           = $inID;
        }
      }

      $res = $this->model->setFields( $_POST )->insert();
      if ( !$res ) {
        foreach ( $this->merge as $model ) {
          $model['model']->setFields( $_POST[$model['className']] )->insert();
          $model['model']->where( [ 'id' => $insertsID[$model['className']] ] )->delete();
        }
      }
      foreach ( $_POST as $key => $value )
        if ( $key != 'submit' )
          unset( $_POST[$key] );
      $this->result = $res;
      return $res;
    }
    return false;
  }

  /**
   * @param $id
   * @return bool
   */
  public function update( $id )
  {
    $result = false;
    if ( isset( $_POST['submit'] ) ) {
      //				foreach ($this->fields as $field => $options) {
      //					if ( isset($options['only']) && $options['only'] == 'one' && isset($_POST[$field]) && $_POST[$field] == 1 ) {
      //						$this->model->setFields([$field => 0])->update();
      //					}
      //				}
      $this->getFields( $id );

      foreach ( $this->merge as $model ) {
        
        if ( isset($_POST[$model['field']]) && $_POST[$model['field']] !== '' ){
          $model['model']->setFields( $_POST[$model['className']] )
                         ->where( [ $this->fields[$model['field']]['id_key'] => $_POST[$model['field']] ] )->update();
        }elseif(isset($this->model->{$model['field']}) && $this->model->{$model['field']} != '' ) {
          $model['model']->setFields( $_POST[$model['className']] )
                         ->where( [ $this->fields[$model['field']]['id_key'] => $this->model->{$model['field']} ] )->update();
        }
        else
          $_POST[$model['field']] = $model['model']->setFields( $_POST[$model['className']] )->insert();
      }
      $result       = $this->model->setFields( $_POST )->where( [ 'id' => $id ] )->update();
      $this->result = $result;
    }

    return $result;
  }

  /**
   * @param        $model
   * @param string $field
   * @throws \ReflectionException
   */
  public function merge( $model, $field = '' )
  {
    $join      = $field;
    $className = $model->formName();
    if ( $join == '' ) {
      $fields = preg_split( '/(?<=[a-z])(?=[A-Z])/u', $className );
      foreach ( $fields as $item ) {
        $join .= strtolower( $item ) . '_';
      }
      $join = $join . 'id';
    }
    $this->merge[] = [ 'model' => $model, 'field' => $join, 'className' => $className ];

  }

  /**
   *
   */
  public function getMergeData()
  {

    if ( isset( $_POST['class'] ) && isset( $_POST['value'] ) && isset( $_POST['field'] ) ) {
      $mergeClasses = array_column( $this->merge, 'className' );
      if ( !in_array( $_POST['class'], $mergeClasses ) )
        die;
      $class = '\\Application\\Models\\' . $_POST['class'];
      $data  = $class::findOne( [ $_POST['field'] => $_POST['value'] ] );
      if ( $data )
        $response = json_encode( $data );
      else
        $response = [];

      die( $response );
    }
  }

  /**
   *
   */
  private function prepare()
  {
    foreach ( $this->merge as $model ) {
      $fields    = $model['model']->getFields();
      $modelName = $model['model']->formName();
      $preName   = $model['model']->attributeLabels();
      $preName   = isset( $preName[$modelName] ) ? $modelName : '';
      $id        = $this->item[$model['field']];
      $data      = $model['model']->where( [ 'id' => $id ] )->fetchOne();
      foreach ( $fields as $field => $option ) {
        $this->events['change'][$model['field']][$modelName][] = [
          'field'     => $field,
          'fieldName' => $modelName . '[' . $field . ']'
        ];
        $option['label'] = $preName != ''
          ? $preName . ' ( ' . $option['label'] . ' )' : $option['label'];

        $this->mergeFields[$model['field']][$modelName . '[' . $field . ']'] = $option;
        $this->item[$modelName . '[' . $field . ']']                         = $data
          ? $data[$field]
          : '';
      }
      $this->sortFields( $this->mergeFields[$model['field']] );
    }
  }

  /**
   *
   */
  private function prepareAll()
  {
    foreach ( $this->merge as $model ) {
      $fields    = $model['model']->getFields();
      $modelName = $model['model']->formName();
      $id        = array_column( $this->items, $model['field'] );
      $data      = $model['model']->where( [ 'id' => $id ] )->fetchAll();
      foreach ( $fields as $field => $option ) {
        $this->fields[$modelName . '[' . $field . ']'] = $option;
      }
      foreach ( $fields as $field => $options ) {
        $mokeFields[$modelName . '[' . $field . ']'] = '';
      }
      foreach ( $this->items as $id => $item ) {
        $mergeElements = [];
        if ( isset( $data[$item[$model['field']]] ) ) {
          foreach ( $data[$item[$model['field']]] as $dataField => $datum )
            $mergeElements[$modelName . '[' . $dataField . ']'] = $datum;
        } else {
          $mergeElements = $mokeFields;
        }
        $this->items[$id] = array_merge( $this->items[$id], $mergeElements );
      }
    }
  }

  /**
   * @return bool
   */
  private function getErrorsMerge()
  {
    $errors = false;
    if ( isset( $_POST['submit'] ) ) {
      foreach ( $this->merge as $merge ) {
//          foreach( $merge['model']->getFields() as $field_name => $options ) {
//            if( ( isset( $_POST[$merge['className']][$field_name] ) || isset( $_FILES[$merge['className']][$field_name] ) ) && $options['required'] ) {
//              if( $options['type'] == 'file' ) {
//                if( !is_uploaded_file( $_FILES[$merge['className']][$field_name]['tmp_name'] ) )
//                  $errors[$merge['className'].'['.$field_name.']'] = '';
//              } else if( empty( $_POST[$merge['className']][$field_name] ) )
//                $errors[$merge['className'].'['.$field_name.']'] = '';
//            }
//          }
        if ( $merge['model']->load( $_POST ) && !$merge['model']->validate() ) {
          foreach ( $merge['model']->getErrors() as $field => $error ) {
            $errors[$merge['className'] . '[' . $field . ']'] = $error[0];
          }
        }
      }
    }
    return $errors;
  }

  /**
   * @return bool
   */
  public function delete()
  {
    if ( isset( $_POST['id'] ) ) {
//        $id   = intval( $_POST['id'] );
//        $item = $this->model->where( [ 'id' => $id ] )->fetchOne();
//
//        foreach( $this->fields as $field_name => $options ) {
//          if( $options['type'] == 'file' ) {
//            if( $options['multiple'] ) {
//              $files = ( $item[$field_name] );
//              if( is_array( $files ) )
//                foreach( $files as $file )
//                  @unlink( FOLDER.$file );
//            } else @unlink( FOLDER.$item[$field_name] );
//          }
//        }

      $res = $this->model->where( [ 'id' => $_POST['id'] ] )->delete()
        ? '1'
        : '0';
      echo json_encode( array( 'deleted' => $res ) );
      return true;
    }

    return false;
  }

  /**
   * @return mixed
   */
  public function getResult()
  {
    return $this->result;
  }

  /**
   * @return bool
   */
  public function deleteFile()
  {
    if ( isset( $_POST['file-id'] ) && isset( $_POST['field'] ) && isset( $_POST['file'] ) ) {

      if ( !isset( $this->fields[$_POST['field']] ) )
        return false;

      $file  = $_POST['file'];
      $field = $this->fields[$_POST['field']];
      $id    = intval( $_POST['file-id'] );

      $item = $this->model->where( [ 'id' => $id ] )->fetchOne();
      if ( !$item )
        return false;

      if ( $field['type'] == 'file' || $field['type'] == 'afile' ) {
        if ( $field['multiple'] == false && $file == $item[$_POST['field']] ) {
          @unlink( FOLDER . $file );
          $this->model->setFields( array( $_POST['field'] => '' ) )->where( [ 'id' => $id ] )->update();
        }

        if ( $field['multiple'] ) {
          $files = $item[$_POST['field']];
          if ( !is_array( $files ) )
            return false;
          $key = array_search( $file, $files );
          if ( $key === false )
            return false;
          @unlink( FOLDER . $file );
          unset( $files[$key] );
          $new_files = ( $files );
          $this->model->setFields( array( $_POST['field'] => $new_files ) )->where( [ 'id' => $id ] )->update();
        }
      }

      echo json_encode( array() );
      return true;
    }
    return false;
  }

  /**
   * @return string
   */
  private function getBreadcrumbs()
  {
    $content = '';

    $count = count( $this->breadcrumbs );
    if ( $count == 1 )
      return '';
    $i = 1;
    foreach ( $this->breadcrumbs as $title => $url ) {
      $i++;

      $url = $url;

      if ( $i <= $count )
        $content .= '<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="' . $url . '" itemprop="url"><span itemprop="title">' . $title . '</span></a></li>';
      else
        $content .= '<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><span itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><span itemprop="title">' . $title . '</span></span></li>';
    }

    $content .= '';
    return $content;
  }

  /**
   * @return bool
   */
  private function getErrors()
  {
    $errors = false;
    if ( isset( $_POST['submit'] ) ) {
//        foreach( $this->fields as $field_name => $options ) {
//          if( ( isset( $_POST[$field_name] ) || isset( $_FILES[$field_name] ) ) && $options['required'] ) {
//            if( $options['type'] == 'file' ) {
//              if( !is_uploaded_file( $_FILES[$field_name]['tmp_name'] ) )
//                $errors[$field_name] = '';
//            } else if( empty( $_POST[$field_name] ) )
//              $errors[$field_name] = '';
//          }
//        }
      if ( $this->model->load( $_POST, '' ) && !$this->model->validate() ) {
        foreach ( $this->model->getErrors() as $field => $error ) {
          $errors[$field] = $error[0];
        }
      }
    }
    return $errors;
  }

  /**
   * @param array $item_in
   * @return mixed
   */
  private function getFieldValues( $item_in = array() )
  {
    foreach ( $this->fields as $field_name => $field ) {
      if ( count( $item_in ) > 0 )
        $item[$field_name] = isset( $_POST[$field_name] )
          ? $_POST[$field_name]
          : $item_in[$field_name];
      else $item[$field_name] = isset( $_POST[$field_name] ) && $field['type'] !== 'file' && $field['type'] !== 'content'
        ? $_POST[$field_name]
        : false;
      if ( $field['type'] == 'content' ) {
        $content         = new Content();
        $item['content'] = $content->init( $_POST, $_FILES )->getContent();
      }

    }

    return $item;
  }

  /**
   * @param bool $id
   * @return $this
   */
  private function getFields( $id = false )
  {
    if ( $id )
      $item = $this->model->where( [ 'id' => $id ] )->fetchOne();

    foreach ( $this->fields as $field_name => $field ) {
      if ( $field['type'] == 'content' ) {
        $content          = new Content();
        $_POST['content'] = $content->init( $_POST, $_FILES )->getContent();
      }

      if ( $field['type'] == 'checkbox' ) {
        $_POST[$field_name] = isset( $_POST[$field_name] )
          ? 1
          : 0;
      }

      if ( $field['type'] == 'mselect' || $field['type'] == 'amselect' ) {
        if ( !isset( $_POST[$field_name] ) && isset( $field['default'] ) )
          $_POST[$field_name] = $field['default'];
      }

      if ( $field['type'] == 'file' || $field['type'] == 'afile' ) {
        if ( isset( $_FILES[$field_name] ) && $_FILES[$field_name]['name'] != '' ) {
          $_POST[$field_name] = $field['multiple']
            ? $this->uploadFiles( $_FILES[$field_name], $field['file-type'] )
            : $this->uploadFile( $_FILES[$field_name]['name'], $_FILES[$field_name]['tmp_name'], $field['file-type'] );
        }
        if ( isset( $item ) ) {
          if ( $field['multiple'] ) {
            $arr1 = isset( $item[$field_name] ) && $item[$field_name] !== false
              ? $item[$field_name]
              : [];
            if ( isset( $_POST[$field_name] ) ) {
              $arr2 = is_array( ( $_POST[$field_name] ) )
                ? ( $_POST[$field_name] )
                : array();
            } else {
              $arr2 = [];
            }
            $arr3 = [];
            if ( isset( $_POST['copy_' . $field_name] ) && is_array( $_POST['copy_' . $field_name] ) ) {
              foreach ( $_POST['copy_' . $field_name] as $index => $item ) {
                $info = pathinfo( $item );
                if ( !in_array( $info['extension'], [ 'png', 'jpg', 'jpeg' ] ) ) continue; //Быстрая проверка
                $dest = '/images/' . md5( uniqid( $index ) ) . '.' . $info['extension'];
                if ( copy( $item, FOLDER . $dest ) ) {
                  $uploadFile = UploadedFile::load( 'copy_' . $field_name, [
                    'name'     => FOLDER . $dest,
                    'tempName' => FOLDER . $dest,
                    'type'     => filetype( FOLDER . $dest ),
                    'size'     => filesize( FOLDER . $dest ),
                    'error'    => 0,
                  ] );
                  $validator  = new ImageValidator();
                  if ( $validator->validate( $uploadFile ) ) { // Детальная проверка
                    $arr3[] = $dest;
                  } else {
                    @unlink( FOLDER . $dest );
                  }
                }
              }
            }
            $_POST[$field_name] = ( array_merge( $arr1, $arr2, $arr3 ) );
          } else {
            if ( isset( $_POST[$field_name] ) )
              @unlink( FOLDER . $item[$field_name] );
            else
              $_POST[$field_name] = $item[$field_name];
          }
        }
      }
    }
    return $this;
  }

  /**
   * @param $files
   * @param $type
   * @return string
   */
  private function uploadFiles( $files, $type )
  {
    $file = array();

    foreach ( $files['tmp_name'] as $key => $tmp_name ) {
      $filename = $this->uploadFile( $files['name'][$key], $tmp_name, $type );
      if ( $filename )
        $file[] = $filename;
    }

    $files_out = ( $file );

    return $files_out;
  }

  /**
   * @param $name
   * @param $tmp_name
   * @param $type
   * @return bool|string
   */
  private function uploadFile( $name, $tmp_name, $type )
  {
    $filename = false;

    if ( is_uploaded_file( $tmp_name ) ) {
      $ext      = $this->getExt( $name, $type );
      $filename = '/images/' . md5( $tmp_name ) . '.' . $ext;
      move_uploaded_file( $tmp_name, FOLDER . $filename );
    }
    return $filename;
  }

  /**
   * @param $name
   * @param $type
   * @return mixed|string
   */
  private function getExt( $name, $type )
  {
    $file    = explode( '.', $name );
    $fileext = array_pop( $file );
    return $fileext;
    if ( !in_array( strtolower( $fileext ), $this->white_ext ) )
      die( 'Загрузка запрещенного файла' );

    switch ( $type ) {
      case 'image/*':
        $ext = 'jpg';
        break;
      case '*':
        $ext = $fileext;
        break;
    }
    return $ext;
  }

  /**
   * @param     $data
   * @param int $level
   * @param int $pid
   * @return string
   * @throws \ReflectionException
   */
  public function tree( $data, $level = 1, $pid = 0 )
  {

    $html      = '';
    $class     = new \ReflectionClass( $this->model );
    $className = $class->getShortName();
    $url       = strtolower( $className );

    if ( $level === 1 ) {
      $html = <<<HTML
		<ul class="categories sortables" id="categories" data-level="$level">
HTML;
    } else {
      $uniq       = uniqid( 'id' );
      $category_p = $this->model->where( [ 'id' => $pid ] )->fetchOne();
      $html       = <<<HTML
					<ul class="subs" id="{$uniq}" data-level="$level">
HTML;
      $text       = 'Добавить в ' . $category_p['name'];
    }
    if ( $level != 1 )
      $html .= <<<HTML
				<li class="create-category" data-level="$level" >
					<a href="/admin/$url/create/parent/$pid" class="btn btn-primary btn-xs">
						<i class="fa fa-plus"></i>
						$text
					</a>
				</li>
HTML;
    foreach ( $data as $el ) {
      if ( $el['active'] == 1 ) {
        $color  = 'btn-success';
        $active = 0;
        $title  = 'Включить категорию';
      } else {
        $color  = 'btn-default';
        $active = 1;
        $title  = 'Выключить категорию';
      }
      if ( $el[$this->options['parent']] == $pid ) {
        $html .= <<<HTML
				<li class="sort-item" data-id="{$el['id']}" data-level="$level">
         <div data-id="{$el['id']}" class="panel panel-default" style="background:#f9f9f9">
             <a class="btn $color btn-xs change_active tree_btn" data-active="$active" data-id="{$el['id']}" title="$title" data-toggle="tooltip" data-placement="right">
             	<i class="fa fa-power-off" aria-hidden="true"></i>
             </a>
						<a class="btn btn-xs btn-default sort tree_btn">
							<i class="fa fa-bars"></i>
						</a>
            <a class="show-next btn btn-xs tree_btn btn-primary" data-type="show">+</a>
							<span class="sort">{$el['name']}</span>
            <a class="pull-right btn btn-xs btn-danger tree_btn delete" data-id="{$el['id']}" class="red"  title="Удалить">
            	<i class="fa fa-trash-o red" aria-hidden="true"></i>
            </a>
            <a href="/admin/$url/edit/{$el['id']}" class="pull-right tree_btn edit btn btn-xs btn-warning"  title="Редактировать">
            	<i class="fa fa-pencil-square-o" aria-hidden="true"></i>
            </a> 
				 </div>
HTML;
        $level++;
        $html  .= $this->tree( $data, $level, $el['id'] );
        $html  .= '</li>';
        $level = 1;
      }
    }
    $html .= '</ul>';
    return $html;
  }

  /**
   * @param array $errors
   */
  public function setErrors( array $errors )
  {
    $this->errors = array_merge( $this->errors, $errors );
  }

  /**
   * @param string $successMessage
   */
  public function setSuccessMessage( string $successMessage )
  {
    $this->successMessage = $successMessage;
  }

  /**
   * @param bool $attribute
   * @return bool
   */
  public function hasErrors( $attribute = false )
  {
    if ( !$attribute ) {
      return count( $this->errors ) > 0
        ? true
        : false;
    } elseif ( !isset( $this->errors[$attribute] ) ) {
      return false;
    }
    return true;
  }

  /**
   * @param string $errorMessage
   */
  public function setErrorMessage( string $errorMessage )
  {
    $this->errorMessage = $errorMessage;
  }

  /**
   * @param bool $result
   */
  public function setResult( bool $result )
  {
    $this->result = $result;
  }

  /**
   * @return array
   */
  public function getDetailTables(): array
  {
    return $this->detailTables;
  }

  /**
   * @param array $detailTables
   * <p>
   * In array<br>
   * name - Название таба required<br>
   * id - Ид таба если надо<br>
   * content required - html <br>
   * </p>
   */
  public function setDetailTables( array $detailTables )
  {
    $this->detailTables[] = $detailTables;
  }

  /**
   *
   */
  public function render()
  {

    switch ( $this->view ) {
      case 'edit':
      case 'create':
        $this->sortFields( $this->fields, 'dsort' );
        break;
      case 'index':
        $this->sortFields( $this->fields );
        break;
    }

    $this->template->vars( 'item', $this->item );
    $this->template->vars( 'title_page', $this->page_current['name'] );
    $this->template->vars( 'page_current', $this->page_current );
    $this->template->vars( 'errors', $this->errors );
    $this->template->vars( 'url', $this->model->formName() );
    $this->template->vars( 'breads', $this->getBreadcrumbs() );
    $this->template->vars( 'model', $this->model );
    $this->template->vars( 'builder', $this );
    $this->template->vars( 'items', $this->items );
    $this->template->vars( 'tree', $this->tree );
    $this->template->vars( 'options', $this->options );
    $this->template->vars( 'mergeFields', $this->mergeFields );
    $this->template->vars( 'result', $this->errors ? false : $this->result );
    $this->template->vars( 'tabs', $this->getDetailTables() );
    $this->template->vars( 'successMessage', $this->successMessage );
    $this->template->vars( 'errorMessage', $this->errorMessage );
    $this->template->vars( 'fields', $this->fields );
    $this->template->vars( 'events', $this->events );
    $this->template->view( $this->view );
  }

  public function renderTabs( $id )
  {
    $uri                                       = str_replace( '/admin/', '', $_SERVER['REQUEST_URI'] );
    $uri                                       = preg_replace( '~/edit/[0-9]+~', '', $uri );
    $page_current                              = AdminPage::findOne( [ 'uri' => $uri ] );
    $this->page_current                        = $page_current;
    $this->breadcrumbs ['Главная']             = '/';
    $this->breadcrumbs ['Админпанель']         = '/admin';
    $this->breadcrumbs [$page_current['name']] = '/admin/' . $page_current['uri'];
    $this->template->vars( 'title_page', $this->page_current['name'] );
    $this->template->vars( 'page_current', $this->page_current );
    $this->template->vars( 'id', $id );
    $this->breadcrumbs ['Запись ' . $page_current['name'] . ' #' . $id] = '#';
    $this->template->vars( 'breads', $this->getBreadcrumbs() );
    $this->template->vars( 'tabs', $this->getDetailTables() );
    $this->template->view( 'onlyTabs' );
  }

  /**
   * @return mixed
   */
  public function getView()
  {
    return $this->view;
  }

  /**
   * @param mixed $view
   */
  public function setView( $view )
  {
    $this->view = $view;
  }

  /**
   * @return mixed
   */
  public function getItem()
  {
    return $this->item;
  }

  /**
   * @return array
   */
  public function getMergeModel()
  {
    $mergeClasses = array_column( $this->merge, 'className' );
    $result       = [];
    foreach ( $mergeClasses as $mergeClass ) {
      $class    = '\\Application\Models\\' . $mergeClass;
      $result[] = new $class();
    }
    return $result;
  }

  /**
   * @return array
   */
  public function getWhere(): array
  {
    return $this->where;
  }

  /**
   * @param array $where
   */
  public function setWhere( array $where )
  {
    $this->where = $where;
  }

  /**
   * @return array
   */
  public function getLike(): array
  {
    return $this->like;
  }

  /**
   * @param array $like
   */
  public function setLike( array $like )
  {
    $this->like = $like;
  }

  /**
   * @param $fields
   */
  private function sortFields( &$fields, $sortField = 'sort' )
  {
    if ( $sortField == 'sort' ) {
      uasort( $fields, function ( $a, $b ) {
        if ( !isset( $a['sort'] ) || !isset( $b['sort'] ) ) return 0;
        return ( $a['sort'] - $b['sort'] );
      } );
    } elseif ( $sortField == 'dsort' ) {
      uasort( $fields, function ( $a, $b ) {
        if ( !isset( $a['dsort'] ) || !isset( $b['dsort'] ) ) return 0;
        return ( $a['dsort'] - $b['dsort'] );
      } );
    }
  }

  /**
   * @return mixed
   */
  public function getItems()
  {
    return $this->items;
  }

  /**
   * @param mixed $items
   */
  public function setItems( $items )
  {
    $this->items = $items;
  }

  /**
   * @return array
   */
  public function getOrWhere(): array
  {
    return $this->orWhere;
  }

  /**
   * @param array $orWhere
   */
  public function setOrWhere( array $orWhere )
  {
    $this->orWhere = $orWhere;
  }

  private function backRedirect()
  {
    $url = $this->page_current['uri'];
    header("Location: /admin/$url");
  }
}