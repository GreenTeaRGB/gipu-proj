<?php
namespace Application\Classes;

/**
 * Class Application
 * @package Application\Classes
 */
class Application
{


  /**
   *
   */
  public static function run()
	{
		$_POST = self::getRequest($_POST);
		$_GET = self::getRequest($_GET);
		$_REQUEST = self::getRequest($_REQUEST);
	}

  /**
   * @param      $data
   * @param bool $quotes
   * @return array
   */
  public static function getRequest( $data, $quotes = true)
	{
		$array = array();
		foreach ($data as $key => $value) {
			if( !is_array($value) ){
				if($key == 'text' || $key == 'items'){
					$array[$key] = $value;
					continue;
				}
				$array[$key] = $quotes ? htmlspecialchars($value, ENT_QUOTES) :  htmlspecialchars($value);
			}
			else
			{
				$array[$key] = self::getRequest($value);
			}
		}
		return $array;
	}

  /**
   * @param $message
   * @param $params
   * @return string
   */
  public static function format( $message, $params){
			$p = [];
			foreach ($params as $name => $value) {
				$p['{' . $name . '}'] = $value;
			}

			return strtr($message, $p);
	}
}