<?php

  namespace Application\Classes;
  use function method_exists;
  use function str_replace;
  use function var_dump;

  /**
   * Class MainModel
   * @package Application\Classes
   */

  Abstract class MainModel extends ObjectMain
  {

    /**
     * @var
     */
    protected $table;
    /**
     * @var
     */
    protected $db;
    /**
     * @var
     */
    private $result;
    /**
     * @var
     */
    private $sql;
    /**
     * @var string
     */
    private $selectFields = '*';
    /**
     * @var array
     */
    private $fields = array();
    /**
     * @var array
     */
    private $allFields = [];
    /**
     * @var array
     */
    private $allFieldsParams = [];
    /**
     * @var null
     */
    private $types = null;
    /**
     * @var null
     */
    private $where = null;
    /**
     * @var array
     */
    private $params = array();
    /**
     * @var null
     */
    private $like = null;
    /**
     * @var array
     */
    private $likeParams = array();
    /**
     * @var array
     */
    private $likeOrder = array();
    /**
     * @var null
     */
    private $order = null;
    /**
     * @var null
     */
    private $model = null;
    /**
     * @var null
     */
    private $group = null;
    /**
     * @var null
     */
    private $limit = null;
    /**
     * @var array
     */
    private $getters = [];
    /**
     * @var array
     */
    private $setters = [];
    /**
     * @var array
     */
    private $limitParams = array();
    /**
     * @var array
     */
    private $whereParams = array();
    /**
     * @var null
     */
    private static $instance = null;
    /**
     * @var
     */
    private $lastState;

    /**
     * MainModel constructor.
     * @param $table
     * @param $typesFields
     * @param $model
     */
    public function __construct( $table, $typesFields, $model )
    {
      $this->table = $table;
      $this->types = $typesFields;
      $this->model = $model;
      $this->db    = Db::getInstance()->getConnection();

      foreach( $typesFields as $field => $value ) {
        $words  = explode( '_', $field );
        $method = '';
        foreach( $words as $word ) {
          $method .= ucfirst( $word );
        }
        if( method_exists( $this->model, 'get'.$method ) ) {
          $this->getters[$field] = 'get'.$method;
        }
        if( method_exists( $this->model, 'set'.$method ) ) {
          $this->setters[$field] = 'set'.$method;
        }
      }
    }

    /**
     * @return mixed
     */
    abstract public function getTypes();

    /**
     * @param $sql
     * @return $this
     */
    public function sql( $sql )
    {
      $this->sql = $sql;
      return $this;
    }

    /**
     * @param $str
     * @return $this
     */
    public function selectFields( $str )
    {
      if( is_array( $str ) ) {
        $this->selectFields = implode( ',', $str );
      } else {
        $this->selectFields = $str;
      }
      return $this;
    }

    /**
     * @param $str
     * @return $this
     */
    public function group( $str )
    {
      $this->group = $str;
      return $this;
    }

    /**
     * @param array $str
     * @return $this
     */
    public function order( $str = array() )
    {
      $this->order = ' ORDER BY ';
      if( is_array( $str ) > 0 ) {
        foreach( $str as $field => $order ) {
          $this->order .= $field.' '.$order.',';
        }
      }
      $this->order = substr( $this->order, 0, -1 );
      if( !is_array( $str ) )
        $this->order = $str;
      return $this;
    }

    /**
     * @param bool $start
     * @param bool $end
     * @return $this
     */
    public function limit( $start = false, $end = false )
    {
      if( $start !== false ) {
        $this->limit                      = ' LIMIT :limit_start';
        $this->limitParams['limit_start'] = $start;
        $this->types["limit_start"]       = 'int';
      } else {
        return $this;
      }
      if( $end !== false ) {
        $this->limit                    .= ', :limit_end';
        $this->limitParams['limit_end'] = $end;
        $this->types["limit_end"]       = 'int';
      }
      return $this;
    }

    /**
     * @param bool $str
     * @return $this
     */
    public function where( $str = false )
    {
      if( is_array( $str ) ) {
        $this->where = 'WHERE (';
        foreach( $str as $field => $value ) {
          if( is_array( $value ) ) {
            if( strpos( $field, '__filter' ) !== false && $this->where != 'WHERE (' )
              $this->where .= ' AND ';
            $this->where .= '(';
            $count       = 0;
            foreach( $value as $field_item => $value_item ) {
              $tmpField = $field;
              list( $del, $tmpField ) = $this->getDelimiter( $tmpField );
              if( strpos( $tmpField, '__filter' ) !== false ) {
                $paramFields = $tmpField.$field_item;
                $tmpField    = $field_item;
                $type        = $tmpField;
                if( $this->where != 'WHERE (' && $this->where != 'WHERE ((' && $count != 0 )
                  $this->where .= ' OR ';

              } else {
                $paramFields = $tmpField;
                if( $this->where != 'WHERE (' && $this->where != 'WHERE ((' )
                  $this->where .= ' OR ';
                $type = $count;
              }
              $this->where             .= "`{$tmpField}`$del:{$paramFields}_{$field_item}";
              $key                     = $paramFields.'_'.$type;
              $this->whereParams[$key] = $value_item;
              $this->types[$key]       = $this->types[$tmpField];
              $count++;
            }
            $this->where .= ')';
          } else {
            if( $this->where != 'WHERE (' )
              $this->where .= ' AND ';
            list( $del, $field ) = $this->getDelimiter( $field );
            $this->where               .= "`{$field}`$del:$field";
            $this->whereParams[$field] = $value;
          }
        }
        $this->where .= ' ) ';
      } else $this->where = $str;

      return $this;
    }

    /**
     * @param $field
     * @return array
     */
    public function getDelimiter( $field )
    {
      $field = htmlspecialchars_decode($field);
      $delimiter = $field[0];
      switch( $delimiter ) {
        case '>' :
          $del   = '>';
          $field = substr( $field, 1 );
          if( $field[0] == '=' ) {
            $del   .= '=';
            $field = substr( $field, 1 );
          }
          break;
        case '<' :
          $del   = '<';
          $field = substr( $field, 1 );
          if( $field[0] == '=' ) {
            $del   .= '=';
            $field = substr( $field, 1 );
          }
          break;
        case '!' :
          $del   = '<>';
          $field = substr( $field, 1 );
          break;
        default:
          $del = '=';
      }
      return [ $del, $field ];
    }

    /**
     * @param array $where
     */
    public function andWhere( array $where )
    {
      $this->createWhere( $where, 'AND' );
      //			foreach( $where as $field => $value ) {
      //				$unic = uniqid();
      //				if( $this->where == '' ) {
      //					$this->where .= ' WHERE ';
      //				}
      //				else {
      //					$this->where .= ' AND ';
      //				}
      //				list( $del, $field ) = $this->getDelimiter( $field );
      //				$this->where                         .= "`{$field}`$del:$field$unic";
      //				$this->whereParams[ $field . $unic ] = $value;
      //				$this->types[ $field . $unic ]       = 'str';
      //			}
      return $this;
    }

    /**
     * @param $where
     * @param $type
     */
    private function createWhere( $where, $type )
    {
      if( $this->where == '' ) {
        $this->where .= ' WHERE (';
      } else {
        $this->where = str_replace('WHERE (', 'WHERE ((', $this->where);
        $this->where .= $type.' (';
      }
      $index = 0;
      foreach( $where as $field => $value ) {
        if( $index != 0 )
          $this->where .= ' AND ';
        $unic = uniqid();
        list( $del, $field ) = $this->getDelimiter( $field );
        if(is_array($value)){
          $value =  'IN("'.implode('","', $value).'")';
          $del = ' ';
        }

        $this->where                     .= "`{$field}`$del:$field$unic";
        $this->whereParams[$field.$unic] = $value;
        $this->types[$field.$unic]       = 'str';
        $index++;
      }
      $this->where .= ' ))';
    }

    /**
     * @param array $where
     */
    public function orWhere( array $where )
    {
      $this->createWhere( $where, 'OR' );
      return $this;
    }

    /**
     * @param $where
     */
    public function andOrWhere( $where )
    {
      if( $this->where == '' ) {
        $this->where .= ' (';
      } else {
        $this->where .= ' AND (';
      }
      $index = 0;
      foreach( $where as $field => $value ) {
        if( $index != 0 )
          $this->where .= ' OR ';
        $unic = uniqid();
        list( $del, $field ) = $this->getDelimiter( $field );
        $this->where                     .= "`{$field}`$del:$field$unic";
        $this->whereParams[$field.$unic] = $value;
        $this->types[$field.$unic]       = 'str';
        $index++;
      }
      $this->where .= ' )';
    }

    /**
     * @param array $likeData
     * @param string $delimer
     * @return $this
     */
    public function like( $likeData = array(), $delimer = 'OR' )
    {
      if( $this->where == '' )
        $this->like = 'WHERE ';
      $count    = count( $likeData );
      $iterator = 0;
      if( is_array( $likeData ) ) {
        if( $count > 1 && $this->where == '' )
          $this->like = ' WHERE (';
        if( $count > 1 && $this->where != '' )
          $this->like = ' AND (';
        if( $count == 1 && $this->where != '' )
          $this->like = ' AND ';
        foreach( $likeData as $field => $value ) {
          if( $iterator == 0 )
            $this->like .= "`{$field}` LIKE :like_{$field} ";
          else
            $this->like .= "$delimer `{$field}` LIKE :like_{$field} ";
          $this->likeParams["like_".$field] = '%'.$value.'%';
          $this->types["like_".$field]      = 'str';
          $iterator++;
        }
      }
      if( $count > 1 )
        $this->like .= ') ';
      if( !is_array( $likeData ) )
        $this->like = $likeData;
      return $this;
    }

    /**
     * @param      $field
     * @param      $value
     * @param bool $order
     * @return mixed
     */
    public function getByField( $field, $value, $order = false )
    {
      $model = $this->where( array( $field => $value ) );
      if( $order )
        $model->order( " ORDER BY $order" );
      return $model->fetchOne();
    }

    /**
     * @return \Core\Classes\MainModel
     */
    private function select($clear = true)
    {
      $this->sql = "SELECT {$this->selectFields} FROM `{$this->table}` ".$this->where.$this->like.$this->group.$this->order.$this->limit;
      return $this->exec($clear);
    }

    /**
     * @return \Core\Classes\MainModel
     */
    public function delete()
    {
      $deleted = true;
      if( method_exists( $this->model, 'beforeDelete' ) ) {
        $deleted = $this->model->beforeDelete( $this );
      }

      if( !$deleted ) {
        return false;
      }
      $this->sql = "DELETE FROM `{$this->table}` ".$this->where;
      $return    = $this->exec();
      if( method_exists( $this->model, 'afterDelete' ) ) {
        $this->model->afterDelete( $this );
      }
      return $return;
    }

    /**
     * @return bool|\Core\Classes\MainModel
     */
    public function update()
    {
      $set = '';
      if( isset( $this->fields['id'] ) )
        unset( $this->fields['id'] );
      if( count( $this->fields ) > 0 ) {
        if( method_exists( $this->model, 'beforeUpdate' ) ) {
          $this->fields = $this->model->beforeUpdate( $this->fields );
        }
        foreach( $this->fields as $field => $value ) {
          if( $set != '' )
            $set .= ', ';
          $set .= "`{$field}`=:$field";
        }
        $this->sql = "UPDATE `{$this->table}` SET $set ".$this->where.$this->like;
        $result    = $this->exec();
        if( method_exists( $this->model, 'afterUpdate' ) ) {
          $this->model->afterUpdate( $result );
        }
        return $result;
      } else {
        return false;
      }

    }

    /**
     * @param $arFields
     * @return $this
     */
    public function setFields( $arFields )
    {
      if( count( $arFields ) > 0 ) {
        foreach( $arFields as $field => $arField ) {
          if( !isset( $this->types[$field] ) ) {
            unset( $arFields[$field] );
          }
        }
        foreach( $this->setters as $setterField => $setterMethod ) {
          if( isset( $arFields[$setterField] ) )
            $arFields[$setterField] = $this->model->$setterMethod( $arFields[$setterField] );
        }
      }
      $this->fields = $arFields;

      return $this;
    }

    /**
     * @param array $arFields
     * @return $this
     */
    public function setAllFields( $arFields )
    {
      foreach( $arFields as $index => $element ) {
        if( is_array( $element ) ) {
          foreach( $element as $field => $value ) {
            if( !isset( $this->types[$field] ) ) {
              unset( $arFields[$index][$field] );
            }
          }
        }
      }
      $this->allFields = $arFields;
      return $this;
    }

    /**
     * @return bool
     */
    public function insert()
    {
      if( isset( $this->fields['id'] ) )
        unset( $this->fields['id'] );
      $fields = '';
      $values = '';
      if( method_exists( $this->model, 'beforeInsert' ) ) {
        $this->fields = $this->model->beforeInsert( $this->fields );
      }
      foreach( $this->fields as $key => $field ) {
        if( $values != '' && $fields != '' ) {
          $fields .= ' , ';
          $values .= ' , ';
        }
        $fields .= "`{$key}`";
        $values .= ":$key";
      }

      $this->sql = "INSERT INTO `{$this->table}`({$fields}) VALUES($values)";

      if( $insert = $this->exec() ) {
        if( method_exists( $this->model, 'afterInsert' ) ) {
          $this->model->afterInsert( $insert->getInsertId() );
        }
        return $insert->getInsertId();
      } else
        return false;
    }

    /**
     * @return bool
     */
    public function insertAll()
    {
      $insertFields = '';
      $values       = '';
      foreach( $this->allFields as $index => $fields ) {
        if( method_exists( $this->model, 'beforeInsert' ) ) {
          $fields = $this->model->beforeInsert( $fields );
        }
        $values .= '(';
        foreach( $fields as $field => $value ) {
          if( $index == 0 ) {
            $insertFields .= "`{$field}`,";
          }
          $values                                   .= ":all$index$field,";
          $this->allFieldsParams["all$index$field"] = $value;
          $this->types["all$index$field"]           = is_numeric( $value )
              ? 'int'
              : 'str';
        }
        $values = substr( $values, 0, -1 );
        $values .= '),';
      }
      $insertFields = substr( $insertFields, 0, -1 );
      $this->sql    = "INSERT INTO `{$this->table}`({$insertFields}) VALUES ".substr( $values, 0, -1 );
      if( $insert = $this->exec() ) {
        if( method_exists( $this->model, 'afterInsert' ) ) {
          $this->model->afterInsert( $insert->getInsertId() );
        }
        return $insert->getInsertId();
      } else
        return false;
    }

    /**
     * @return $this
     */
    public function exec($clear = true)
    {
      $this->result = $this->db->prepare( $this->sql );

      if( is_array( $this->params ) ) {
        foreach( $this->params as $key => $field ) {
          if(is_array($field)){
            throw new \Exception("Массив в params ". var_export($field, true) ." (возможно проблему решит добавление в модель метода set Пример: protected function setImages(\$element)){ return serialize(\$element); } ".$this->model->formName());
          }
          $this->result->bindValue(
              ":$key", $field, $this->types[$key] == 'int'
              ? \PDO::PARAM_INT
              : \PDO::PARAM_STR
          );
        }
      }

      if( is_array( $this->fields ) ) {
        foreach( $this->fields as $key => $field ) {
          if(is_array($field)){
            throw new \Exception("Массив в fields ". var_export($field, true) ." (возможно проблему решит добавление в модель метода set Пример: protected function setImages(\$element)){ return serialize(\$element); } ".$this->model->formName());
          }
          $this->result->bindValue(
              ":$key", $field, $this->types[$key] == 'int'
              ? \PDO::PARAM_INT
              : \PDO::PARAM_STR
          );
        }
      }
      if( is_array( $this->whereParams ) ) {
        foreach( $this->whereParams as $key => $field ) {
          if(is_array($field)){
            throw new \Exception("Массив в whereParams ". var_export($field, true) ." (возможно проблему решит добавление в модель метода set Пример: protected function setImages(\$element)){ return serialize(\$element); } ".$this->model->formName());
          }
          $this->result->bindValue(
              ":$key", $field, $this->types[$key] == 'int'
              ? \PDO::PARAM_INT
              : \PDO::PARAM_STR
          );
        }
      }
      if( is_array( $this->allFieldsParams ) ) {
        foreach( $this->allFieldsParams as $key => $field ) {
          if(is_array($field)){
            throw new \Exception("Массив в allFieldsParams ". var_export($field, true) ." (возможно проблему решит добавление в модель метода set Пример: protected function setImages(\$element)){ return serialize(\$element); } ".$this->model->formName());
          }
          $this->result->bindValue(
              ":$key", $field, $this->types[$key] == 'int'
              ? \PDO::PARAM_INT
              : \PDO::PARAM_STR
          );
        }
      }
      if( is_array( $this->likeParams ) ) {
        foreach( $this->likeParams as $key => $field ) {
          if(is_array($field)){
            throw new \Exception("Массив в likeParams ". var_export($field, true) ." (возможно проблему решит добавление в модель метода set Пример: protected function setImages(\$element)){ return serialize(\$element); } ".$this->model->formName());
          }
          $this->result->bindValue(
              ":$key", $field, $this->types[$key] == 'int'
              ? \PDO::PARAM_INT
              : \PDO::PARAM_STR
          );
        }
      }
      if( is_array( $this->limitParams ) ) {
        foreach( $this->limitParams as $key => $field ) {
          if(is_array($field)){
            throw new \Exception("Массив в limitParams ". var_export($field, true) ." (возможно проблему решит добавление в модель метода set Пример: protected function setImages(\$element)){ return serialize(\$element); } ".$this->model->formName());
          }
          $this->result->bindValue( ":$key", $field, \PDO::PARAM_INT );
        }
      }
      $this->res = $this->result->execute();
//    echo '<pre>';
//    $this->result->debugDumpParams();
//    echo '</pre>';
      $this->lastState = clone $this;
      if($clear){
        $this->clear();
      }

      return $this;
    }

    /**
     * @return mixed
     */
    public function getLastState()
    {
      return $this->lastState;
    }

    /**
     * @return array
     */
    public function fetchAll($clear = true)
    {
      $array = array();
      if( !$this->sql )
        $this->select($clear);
      else
        $this->exec($clear);

      while( $data = $this->result->fetch( \PDO::FETCH_ASSOC ) ) {
        foreach( $this->getters as $getterField => $getterMethod ) {
          if( isset( $data[$getterField] ) ) {
            $data[$getterField] = $this->model->$getterMethod( $data[$getterField] );
          }
        }
        if(method_exists($this->model, 'onFetchAll')){
          $data = $this->model->onFetchAll($data);
        }
        if(isset($data['id']))
            $array[$data['id']] = $data;
        else
            $array[] = $data;
      }
      if( method_exists( $this->model, 'afterFetchAll' ) ) {
        $array = $this->model->afterFetchAll( $array );
      }
      return $array;
    }

    /**
     * @return mixed
     */
    public function count($clear = true)
    {
      $this->selectFields( ' COUNT(*) as `count`' );
      $this->select($clear);
      $data = $this->result->fetch( \PDO::FETCH_ASSOC );
      return $data['count'];
    }

    /**
     * @return bool
     */
    public function exists()
    {
      return $this->count()  > 0
          ? true
          : false;
    }

    /**
     * @return mixed
     */
    public function fetchOne($clear = true)
    {
      if( !$this->sql )
        $this->select($clear);
      else
        $this->exec($clear);
      $data = $this->result->fetch( \PDO::FETCH_ASSOC );
      foreach( $this->getters as $getterField => $getterMethod ) {
        if( isset( $data[$getterField] ) )
          $data[$getterField] = $this->model->$getterMethod( $data[$getterField] );
      }
      if( method_exists( $this->model, 'afterFetchOne' ) ) {
        $data = $this->model->afterFetchOne( $data );
      }
      return $data;
    }

    /**
     * @return mixed
     */
    public function getInsertId()
    {
      return $this->db->lastInsertId();
    }

    /**
     * @return array
     */
    public function getWhereParams()
    {
      return $this->whereParams;
    }

    /**
     * @return mixed
     */
    public function getResult()
    {
      $this->exec();
      return $this->result;
    }

    /**
     * @return $this
     */
    public function clear()
    {
      $this->sql             = '';
      $this->selectFields    = '*';
      $this->fields          = array();
      $this->allFields       = array();
      $this->allFieldsParams = array();
      $this->where           = null;
      $this->whereParams     = null;
      $this->params          = array();
      $this->like            = null;
      $this->likeParams      = array();
      $this->likeOrder       = array();
      $this->order           = null;
      $this->group           = null;
      $this->limit           = null;
      $this->limitParams     = array();
      return $this;
    }

    /**
     * @return int
     */
    public function getUnixTime()
    {
      $date     = new \DateTime( null );
      $unixTime = $date->getTimestamp();
      /*+ $date->getOffset();*/

      return $unixTime;
    }

  }
