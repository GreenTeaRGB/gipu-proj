<?php
	
	namespace Application\Classes;
	
	use Application\Helpers\AppHelper;
  use Application\Models\User;

  class Session
	{
		
		public static function setSessionForUser($user, $rewrite = false)
		{
			if ( $rewrite || !isset($_SESSION['user']) || !$_SESSION['user'] ) {
				if ( !is_array($user) ) {
					$user = User::findOne(['id' => $user]);
				}
				if(isset($user['password'])) unset($user['password']);
				self::store([
					'user' => $user
				]);
				
				return true;
			}
		}
		
		public static function UpdateUserSessionByKey($key, $value)
		{
			if ( isset($_SESSION['user']) ) {
				$_SESSION['user'][$key] = $value;
			}
			
			return true;
		}
		
		public static function store($key, $value = '')
		{
			if ( is_string($key) && $value && is_string($value) ) {
				$_SESSION[$key] = $value;
			} else {
				if ( is_array($key) && count($key) > 0 ) {
					$sessionArray = $key;

					if ( AppHelper::isAssoc($sessionArray) ) {
						foreach ($sessionArray as $sesKey => $sesValue) {
							$_SESSION[$sesKey] = $sesValue;
						}
					} else {
						foreach ($sessionArray as $sesItem) {
							foreach ($sesItem as $sesKey => $sesValue) {
								$_SESSION[$sesKey] = $sesValue;
							}
						}
					}
				}
			}
			return true;
		}

    public static function remove($key)
    {
      if ( is_string($key) && isset($_SESSION[$key]) ) {
        unset($_SESSION[$key]);
      } else {
        if ( is_array($key) && count($key) > 0 ) {
          $keysArr = $key;
          if ( AppHelper::isAssoc($keysArr) ) {
            foreach ($keysArr as $sesKey) {
              if ( isset($_SESSION[$key]) ) {
                unset($_SESSION[$sesKey]);
              }
            }
          } else {
            foreach ($keysArr as $keyItem) {
              foreach ($keyItem as $item) {
                if ( isset($_SESSION[$key]) ) {
                  unset($_SESSION[$item]);
                }
              }
            }
          }
        }
      }
      return true;
    }

    public static function getByKey($key)
    {
      if ( isset($_SESSION[$key]) ) {
        return $_SESSION[$key];
      }
      return false;
    }

		public static function logout()
		{
			self::remove('user');
			self::remove('addresses');
			
			return true;
		}

		public static function setViewedProduct($product) {
      $products = Session::getByKey( 'viewed_products' );
      if( $products == false ) {
        Session::store( [ 'viewed_products' => [ $product ] ] );
      } elseif( !in_array( $product['id'], array_column($products, 'id') ) ) {
        $count = count( $products );
        if( $count > 20 ) {
          $products = array_slice( $products, 1, $count );
        }
        array_push( $products, $product );
        Session::store( [ 'viewed_products' => $products ] );
      }
    }
		

	}