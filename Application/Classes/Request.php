<?php

  namespace Application\Classes;


  /**
   * Class Request
   * @package Application\Classes
   */
  class Request
  {

    /**
     * @var
     */
    private $post;
    /**
     * @var
     */
    private $get;
    /**
     * @var
     */
    private $files;
    /**
     * @var
     */
    private $request;
    /**
     * @var
     */
    private $server;

    /**
     * Request constructor.
     */
    public function __construct()
    {
      $this->post    = $_POST;
      $this->get     = $_GET;
      $this->files   = $_FILES;
      $this->request = $_REQUEST;
      $this->server  = $_SERVER;
    }

    /**
     * @param string $field
     * @return bool
     */
    public function post( string $field = '' )
    {
      if( $field !== '' ) {
        if( isset( $this->post[$field] ) )
          return $this->post[$field];
        else
          return false;
      }
      return $this->post;
    }

    /**
     * @param string $field
     * @return bool
     */
    public function get( string $field = '' )
    {
      if( $field !== '' ) {
        if( isset( $this->get[$field] ) )
          return $this->get[$field];
        else
          return false;
      }
      return $this->get;
    }

    /**
     * @param string $field
     * @return bool
     */
    public function files( string $field = '' )
    {
      if( $field !== '' ) {
        if( isset( $this->files[$field] ) )
          return $this->files[$field];
        else
          return false;
      }
      return $this->files;
    }

    /**
     * @param string $field
     * @return bool
     */
    public function request( string $field = '' )
    {
      if( $field !== '' ) {
        if( isset( $this->request[$field] ) )
          return $this->request[$field];
        else
          return false;
      }
      return $this->request;
    }

    /**
     * @param string $field
     * @return bool
     */
    public function server( string $field = '' )
    {
      if( $field !== '' ) {
        if( isset( $this->server[$field] ) )
          return $this->server[$field];
        else
          return false;
      }
      return $this->server;
    }

  }