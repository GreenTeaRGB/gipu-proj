<?php

namespace Application\Classes;

use Application\Classes\Forms\FormBuilder;
use Application\Classes\Forms\FormBuildertml;
use Application\Validators\Validator;
use ArrayObject;
use function count;
use ReflectionClass;

/**
 * Class Model
 * @package Application\Classes
 */
abstract class Model extends \Application\Classes\MainModel /*implements IteratorAggregate, ArrayAccess*/
{
  /**
   *
   */
  const SCENARIO_DEFAULT = 'default';
  /**
   * @var
   */
  private $_errors;
  /**
   * @var
   */
  private $_validators;
  /**
   * @var array
   */
  private $fieldsFormBuilder = [];
  /**
   * @var string
   */
  private $builderFormName = '';
  /**
   * @var
   */
  private $_attributes;
  /**
   * @var string
   */
  private $_scenario = self::SCENARIO_DEFAULT;
  /**
   * @var bool
   */
  private static $checkCsrf = false;
  /**
   * @var bool
   */
  private static $_csrf = false;
  /**
   * @var array
   */
  private $deletedItems = [];

  private $saveFiles = [];

  /**
   * @return array
   */
  public function rules()
  {
    return [];
  }

  /**
   * @return array
   */
  public function scenarios()
  {
    $scenarios = [ self::SCENARIO_DEFAULT => [] ];
    foreach ( $this->getValidators() as $validator ) {
      foreach ( $validator->on as $scenario ) {
        $scenarios[$scenario] = [];
      }
      foreach ( $validator->except as $scenario ) {
        $scenarios[$scenario] = [];
      }
    }
    $names = array_keys( $scenarios );
    foreach ( $this->getValidators() as $validator ) {
      if ( empty( $validator->on ) && empty( $validator->except ) ) {
        foreach ( $names as $name ) {
          foreach ( $validator->attributes as $attribute ) {
            $scenarios[$name][$attribute] = true;
          }
        }
      } elseif ( empty( $validator->on ) ) {
        foreach ( $names as $name ) {
          if ( !in_array( $name, $validator->except, true ) ) {
            foreach ( $validator->attributes as $attribute ) {
              $scenarios[$name][$attribute] = true;
            }
          }
        }
      } else {
        foreach ( $validator->on as $name ) {
          foreach ( $validator->attributes as $attribute ) {
            $scenarios[$name][$attribute] = true;
          }
        }
      }
    }
    foreach ( $scenarios as $scenario => $attributes ) {
      if ( !empty( $attributes ) ) {
        $scenarios[$scenario] = array_keys( $attributes );
      }
    }
    return $scenarios;
  }

  /**
   * @return string
   * @throws \ReflectionException
   */
  public function formName()
  {
    $reflector = new ReflectionClass( $this );
    if($reflector->isAnonymous()){
      return $reflector->getParentClass()->getShortName();
    }
    return $reflector->getShortName();
  }

  /**
   * @return array
   * @throws \ReflectionException
   */
  public function attributes()
  {
    $class = new ReflectionClass( $this );
    $names = [];
    foreach ( $class->getProperties( \ReflectionProperty::IS_PUBLIC ) as $property ) {
      if ( !$property->isStatic() ) {
        $names[] = $property->getName();
      }
    }
    return $names;
  }

  /**
   * @return array
   */
  public function attributeLabels()
  {
    return [$this->formName() => $this->formName()];
  }

  /**
   * @return array
   */
  public function attributeHints()
  {
    return [];
  }

  /**
   * @param $name
   * @return bool
   */
  public function hasMethod( $name )
  {
    if ( method_exists( $this, $name ) ) {
      return true;
    }
    return false;
  }

  /**
   * @param null $attributeNames
   * @param bool $clearErrors
   * @return bool
   * @throws \Exception
   */
  public function validate( $attributeNames = null, $clearErrors = true )
  {

    if ( $clearErrors ) {
      $this->clearErrors();
    }

    if ( !$this->beforeValidate() ) {
      return false;
    }
    $scenarios = $this->scenarios();
    $scenario  = $this->getScenario();

    if ( !isset( $scenarios[$scenario] ) ) {
      throw new \Exception( "Не известный сценарий: $scenario" );
    }
    if ( $attributeNames === null ) {
      $attributeNames = $this->activeAttributes();
    }
    foreach ( $this->getActiveValidators() as $validator ) {
      $validator->validateAttributes( $this, $attributeNames );
    }
    $this->afterValidate();
    return !$this->hasErrors();
  }

  /**
   * @return bool
   * @throws \ReflectionException
   */
  public function afterValidate()
  {
    if ( isset( $_SESSION['_csrf'] ) && is_array( $_SESSION['_csrf'] ) )
      $_SESSION['_csrf'] = array_filter( $_SESSION['_csrf'], function ( $csrf ) {
        return isset( $_POST['_csrf'] ) && $csrf !== $_POST['_csrf'];
      } );
    $errors = Session::getByKey( 'error_form' );
    if ( $errors ) {
      $errors[$this->formName()] = $this->getErrors();
    } else {
      Session::store( [ 'error_form' => [ $this->formName() => $this->getErrors() ] ] );
    }
    return true;
  }

  /**
   * @return bool
   */
  public function beforeValidate()
  {

    if ( self::$checkCsrf === true )
      return true;
    if ( $this->checkCSRF() && self::$checkCsrf === false ) {
      self::$checkCsrf = true;
    } else {
      self::$checkCsrf = false;
      $this->addError( 'csrf', 'Не правильный токен' );
    }
    return self::$checkCsrf;
  }

  /**
   * @return bool
   */
  public function checkCSRF()
  {
    return true; // isset( $_POST['_csrf'] ) && isset($_SESSION['_csrf']) && in_array($_POST['_csrf'], $_SESSION['_csrf']);
  }

  /**
   * @return string
   */
  public function getCSRF()
  {
//      if( self::$_csrf !== false )
//        return self::$_csrf;
    self::$_csrf = md5( uniqid() );
    unset( $_SESSION['_csrf'] );
    $_SESSION['_csrf'][] = self::$_csrf;
    return self::$_csrf;
  }

  /**
   * @return ArrayObject
   * @throws \Exception
   */
  public function getValidators()
  {
    if ( $this->_validators === null ) {
      $this->_validators = $this->createValidators();
    }
    return $this->_validators;
  }

  /**
   * @param null $attribute
   * @return array
   * @throws \Exception
   */
  public function getActiveValidators( $attribute = null )
  {
    $validators = [];
    $scenario   = $this->getScenario();
    foreach ( $this->getValidators() as $validator ) {
      if ( $validator->isActive( $scenario ) && ( $attribute === null || in_array( $attribute, $validator->attributes, true ) ) ) {
        $validators[] = $validator;
      }
    }
    return $validators;
  }

  /**
   * @return ArrayObject
   * @throws \Exception
   */
  public function createValidators()
  {
    $validators = new ArrayObject;
    foreach ( $this->rules() as $rule ) {
      if ( $rule instanceof Validator ) {
        $validators->append( $rule );
      } elseif ( is_array( $rule ) && isset( $rule[0], $rule[1] ) ) { // attributes, validator type
        $validator = Validator::createValidator( $rule[1], $this, (array)$rule[0], array_slice( $rule, 2 ) );
        $validators->append( $validator );
      } else {
        throw new \Exception( 'Invalid validation rule: a rule must specify both attribute names and validator type.' );
      }
    }
    return $validators;
  }

  /**
   *
   */
  public function clearValidators()
  {
    $this->_validators = null;
  }

  /**
   * @param $attribute
   * @return bool
   * @throws \Exception
   */
  public function isAttributeRequired( $attribute )
  {
    foreach ( $this->getActiveValidators( $attribute ) as $validator ) {
      if ( $validator instanceof RequiredValidator && $validator->when === null ) {
        return true;
      }
    }
    return false;
  }

  /**
   * @param $attribute
   * @return bool
   */
  public function isAttributeSafe( $attribute )
  {
    return in_array( $attribute, $this->safeAttributes(), true );
  }

  /**
   * @param $attribute
   * @return bool
   */
  public function isAttributeActive( $attribute )
  {
    return in_array( $attribute, $this->activeAttributes(), true );
  }

  /**
   * @param $attribute
   * @return mixed|string
   */
  public function getAttributeLabel( $attribute )
  {
    $labels = $this->attributeLabels();
    return isset( $labels[$attribute] )
      ? $labels[$attribute]
      : $this->generateAttributeLabel( $attribute );
  }

  /**
   * @param $attribute
   * @return mixed|string
   */
  public function getAttributeHint( $attribute )
  {
    $hints = $this->attributeHints();
    return isset( $hints[$attribute] )
      ? $hints[$attribute]
      : '';
  }

  /**
   * @param null $attribute
   * @return bool
   */
  public function hasErrors( $attribute = null )
  {
    return $attribute === null
      ? !empty( $this->_errors )
      : isset( $this->_errors[$attribute] );
  }

  /**
   * @param null $attribute
   * @return array
   */
  public function getErrors( $attribute = null )
  {
    if ( $attribute === null ) {
      return $this->_errors === null
        ? []
        : $this->_errors;
    } else {
      return isset( $this->_errors[$attribute] )
        ? $this->_errors[$attribute]
        : [];
    }
  }

  /**
   * @return array
   */
  public function getFirstErrors()
  {
    if ( empty( $this->_errors ) ) {
      return [];
    } else {
      $errors = [];
      foreach ( $this->_errors as $name => $es ) {
        if ( !empty( $es ) ) {
          $errors[$name] = reset( $es );
        }
      }
      return $errors;
    }
  }

  /**
   * @param $attribute
   * @return mixed|null
   */
  public function getFirstError( $attribute )
  {
    return isset( $this->_errors[$attribute] )
      ? reset( $this->_errors[$attribute] )
      : null;
  }

  /**
   * @param        $attribute
   * @param string $error
   * @throws \ReflectionException
   */
  public function addError( $attribute, $error = '' )
  {
    $_SESSION['error_form'][$this->formName()][$attribute][] = $error;
    $this->_errors[$attribute][]                             = $error;
  }

  /**
   * @param array $items
   * @throws \ReflectionException
   */
  public function addErrors( array $items )
  {
    foreach ( $items as $attribute => $errors ) {
      if ( is_array( $errors ) ) {
        foreach ( $errors as $error ) {
          $this->addError( $attribute, $error );
        }
      } else {
        $this->addError( $attribute, $errors );
      }
    }
  }

  /**
   * @param null $attribute
   * @throws \ReflectionException
   */
  public function clearErrors( $attribute = null )
  {
    if ( $attribute === null ) {
      $this->_errors = [];
      unset( $_SESSION['error_form'][$this->formName()] );
    } else {
      unset( $_SESSION['error_form'][$this->formName()][$attribute] );
      unset( $this->_errors[$attribute] );
    }
  }

  /**
   * @param $name
   * @return string
   */
  public function generateAttributeLabel( $name )
  {
    $label = trim(
      strtolower(
        str_replace(
          [
            '-',
            '_',
            '.'
          ], ' ', preg_replace( '/(?<![A-Z])[A-Z]/', ' \0', $name )
        )
      )
    );

    return $label;
  }

  /**
   * @param null  $names
   * @param array $except
   * @return array
   * @throws \ReflectionException
   */
  public function getAttributes( $names = null, $except = [] )
  {
    $values = [];
    if ( $names === null ) {
      $names = $this->attributes();
    }
    foreach ( $names as $name ) {
      $values[$name] = $this->$name;
    }
    foreach ( $except as $name ) {
      unset( $values[$name] );
    }

    return $values;
  }

  /**
   * @param      $values
   * @param bool $safeOnly
   */
  public function setAttributes( $values, $safeOnly = true )
  {
    if ( is_array( $values ) ) {
      //				$attributes = array_flip( $safeOnly ? $this->safeAttributes() : $this->attributes() );
      foreach ( $values as $name => $value ) {
        //					if( isset( $attributes[ $name ] ) ) {
        $this->_attributes[$name] = $value;
        $this->$name              = $value;
        //					}
        //					elseif( $safeOnly ) {
        //						$this->onUnsafeAttribute( $name, $value );
        //					}
      }
    }
  }

  /**
   * @param $name
   * @param $value
   */
  public function onUnsafeAttribute( $name, $value )
  {

  }

  /**
   * @return string
   */
  public function getScenario()
  {
    return $this->_scenario;
  }

  /**
   * @param $value
   */
  public function setScenario( $value )
  {
    $this->_scenario = $value;
  }

  /**
   * @return array
   */
  public function safeAttributes()
  {
    $scenario  = $this->getScenario();
    $scenarios = $this->scenarios();
    if ( !isset( $scenarios[$scenario] ) ) {
      return [];
    }
    $attributes = [];
    foreach ( $scenarios[$scenario] as $attribute ) {
      if ( $attribute[0] !== '!' ) {
        $attributes[] = $attribute;
      }
    }

    return $attributes;
  }

  /**
   * @return array|mixed
   */
  public function activeAttributes()
  {
    $scenario  = $this->getScenario();
    $scenarios = $this->scenarios();
    if ( !isset( $scenarios[$scenario] ) ) {
      return [];
    }
    $attributes = $scenarios[$scenario];
    foreach ( $attributes as $i => $attribute ) {
      if ( $attribute[0] === '!' ) {
        $attributes[$i] = substr( $attribute, 1 );
      }
    }

    return $attributes;
  }

  /**
   * @param      $data
   * @param null $formName
   * @return bool
   * @throws \ReflectionException
   */
  public function load( $data, $formName = null )
  {


    $scope = $formName === null
      ? $this->formName()
      : $formName;
    if ( $scope === '' && !empty( $data ) ) {
      $this->setAttributes( $data );
      return true;
    } elseif ( isset( $data[$scope] ) ) {
      $this->setAttributes( $data[$scope] );
      return true;
    } else {
      return false;
    }
  }

  /**
   * @param      $models
   * @param      $data
   * @param null $formName
   * @return bool
   * @throws \ReflectionException
   */
  public static function loadMultiple( $models, $data, $formName = null )
  {
    //			if( $formName === null ) {
    //				/* @var $first Model */
    //				$first = reset( $models );
    //
    //				if( $first === false ) {
    //					return false;
    //				}
    //				$formName = $first->formName();
    //			}

    $success = false;
    foreach ( $models as $i => $model ) {
      /* @var $model Model */
      if ( $formName == '' ) {
        if ( !empty( $data[$i] ) ) {
          $model->load( $data[$i], '' );
          $success = true;
        }
      } elseif ( !empty(
      $data[$formName === false
        ? $model->formName()
        : $formName][$i]
      ) ) {
        $model->load(
          $data[$formName === false
            ? $model->formName()
            : $formName][$i], ''
        );
        $success = true;
      }
    }
    return $success;
  }

  /**
   * @param      $models
   * @param null $attributeNames
   * @return bool
   * @throws \Exception
   */
  public static function validateMultiple( $models, $attributeNames = null )
  {
    $valid = true;
    /* @var $model Model */
    foreach ( $models as $model ) {
      $valid = $model->validate( $attributeNames ) && $valid;
    }
    return $valid;
  }

  /**
   * @param array  $activeFields
   * @param string $form
   * @return FormBuilder
   * @throws \Exception
   * @throws \ReflectionException
   */
  public function form( $activeFields = [], $form = 'formFields' )
  {

    $scenario  = $this->getScenario();
    $scenarios = $this->scenarios();
    if ( !isset( $scenarios[$scenario] ) ) {
      throw new \Exception( "Не известный сценарий: $scenario" );
    }
    if ( !( $this instanceof \Application\Classes\Forms\Interfaces\FormBuilderInterface ) )
      throw new \Exception( "Модель {$this->formName()} не реализует interface FormBuilderInterface" );
    $this->builderFormName = $form;
    $fields                = [];
    if ( $form == 'formFields' ) {
      $form_fields = $this->$form( (array)$activeFields );
    } else {
      $form_fields = $this->$form();
    }
    foreach ( $form_fields as $field_name => $form_field ) {
      if ( in_array( $field_name, $scenarios[$scenario] ) || $field_name == 'submit' ) {
        $fields[$field_name] = $form_field;
      }
    }
    $this->fieldsFormBuilder = $fields;
    return new FormBuilder( $this );
  }


  private function loadFilesAttribute( $elements = [] )
  {
    foreach ( $this->getFields() as $attribute => $settings ) {
      if ( $settings['type'] == 'file' ) {
        if ( !isset( $this->$attribute ) || ( isset( $this->$attribute ) && $this->$attribute == '' ) ) continue;
        $filesDelete = array_column( $elements, $attribute );
        self::removeFiles( $filesDelete );
        $dir = '/images/' . $this->formName();
        if ( !file_exists( FOLDER . $dir ) )
          @mkdir( FOLDER . $dir );
        if ( is_array( $this->$attribute ) ) {
          foreach ( $this->$attribute as $file ) {
            if ( $file instanceof \Application\Helpers\UploadedFile ) {
              if ( $file->saveAs( FOLDER . $dir . '/' . $file->getHashName() ) ) {
                $this->_attributes[$attribute][] = $dir . '/' . $file->getHashName();
                $this->saveFiles[]               = $dir . '/' . $file->getHashName();
              }
            }
          }
          $this->_attributes[$attribute] = serialize( $this->_attributes[$attribute] );
        } else {
          if ( $this->$attribute instanceof \Application\Helpers\UploadedFile ) {
            if ( $this->$attribute->saveAs( FOLDER . $dir . '/' . $this->$attribute->getHashName() ) ) {
              $this->_attributes[$attribute] = $dir . '/' . $this->$attribute->getHashName();
              $this->saveFiles[]             = $dir . '/' . $this->$attribute->getHashName();
            }
          }
        }
      }
    }
  }

  /**
   * @return bool
   * @throws \ReflectionException
   */
  public function created()
  {
    $this->loadFilesAttribute();
    if ( $id = $this->setFields( $this->_attributes )->insert() ) {
      return $id;
    }
    self::removeFiles( $this->saveFiles );
    return false;
  }

  public function updated( $where )
  {
    if ( is_numeric( $where ) ) {
      $data = $this->where( [ 'id' => $where ] )->fetchAll();
      $this->loadFilesAttribute( $data );
      $this->where( [ 'id' => $where ] );
    } else {
      $data = $this->where( $where )->fetchAll();
      $this->loadFilesAttribute( $data );
      $this->where( $where );
    }
    if ( $this->setFields( $this->_attributes )->update() ) {
      return true;
    }
    self::removeFiles( $this->saveFiles );
    return false;
  }

  /**
   * @param array $filter
   * @return mixed
   */
  public static function find( $filter = array() )
  {
    $model = new static();
    if ( count( $filter ) > 0 )
      $model->where( $filter );
    return $model->fetchAll();
  }

  /**
   * @param array $filter
   * @param array $sort
   * @return mixed
   */
  public static function findSort( $filter = array(), $sort = [] )
  {
    $model = new static();
    if ( count( $filter ) > 0 )
      $model->where( $filter );
    if ( count( $sort ) > 0 )
      $model->order( $sort );
    return $model->fetchAll();
  }

  /**
   * @param $where
   * @return mixed
   */
  public static function findOne( $where )
  {
    $model = new static();
    if ( !is_numeric( $where ) )
      $model->where( $where );
    else
      $model->where( [ 'id' => $where ] );
    return $model->fetchOne();
  }

  /**
   * @param       $count
   * @param array $filter
   * @param array $order
   * @return mixed
   */
  public static function findLast( $count, $filter = [], $order = [ 'id' => 'desc' ] )
  {
    $model = new static();
    if ( count( $filter ) > 0 )
      $model->where( $filter );
    return $model->order( $order )->limit( $count )->fetchAll();
  }

  /**
   * @param $data
   * @return mixed
   */
  public static function create( $data )
  {
    $model = new static();
    return $model->setFields( $data )->insert();
  }

  public static function getCount($where = []){
    $model = new static();
    if(count($where) > 0)
      $model->where($where);
    return $model->count();
  }

  /**
   * @param $id
   * @return mixed
   */
  public static function deleteById( $id )
  {
    $model = new static();
    return $model->where( [ 'id' => $id ] )->delete();
  }

  /**
   * @param array $filter
   * @return mixed
   */
  public static function deleteFilter( array $filter )
  {
    $model = new static();
    $done  = $model->where( $filter )->delete();
    return $done;
  }

  /**
   * @param $id
   * @param $data
   * @return mixed
   */
  public static function updateById( $id, $data )
  {
    $model = new static();
    return $model->setFields( $data )->where( [ 'id' => $id ] )->update();
  }

  /**
   * @param $where
   * @param $data
   * @return mixed
   */
  public static function updateByFilter( $where, $data )
  {
    $model = new static();
    return $model->setFields( $data )->where( $where )->update();
  }

  /**
   * @return array
   * @throws \ReflectionException
   */
  public function fields()
  {
    $fields = $this->attributes();

    return array_combine( $fields, $fields );
  }

  //		public function getIterator()
  //		{
  //			$attributes = $this->getAttributes();
  //			return new ArrayIterator( $attributes );
  //		}

  /**
   * @return array
   */
  public function getFieldsFormBuilder(): array
  {
    return $this->fieldsFormBuilder;
  }

  /**
   * @return string
   */
  public function getBuilderFormName(): string
  {
    return $this->builderFormName;
  }

  //
  //		public function offsetExists( $offset )
  //		{
  //			return $this->$offset !== null;
  //		}
  //
  //		public function offsetGet( $offset )
  //		{
  //			return $this->$offset;
  //		}
  //
  //		public function offsetSet( $offset, $item )
  //		{
  //			$this->$offset = $item;
  //		}
  //
  //		public function offsetUnset( $offset )
  //		{
  //			$this->$offset = null;
  //		}

  /**
   * @return array
   */
  public function getDeletedItems(): array
  {
    return $this->deletedItems;
  }

  /**
   * @param array $deletedItems
   */
  public function setDeletedItems( array $deletedItems )
  {
    $this->deletedItems = $deletedItems;
  }

  /**
   * @param $model
   * @return bool
   */
  protected function beforeDelete( $model ): bool
  {
    $this->setDeletedItems( $model->fetchAll( false ) );
    return true;
  }

  protected function afterDelete( $model )
  {
    if ( count( $this->getDeletedItems() ) > 0 ) {
      return true;
    } else {
      return false;
    }
  }

  public static function removeFiles( $files )
  {
    if ( is_array( $files ) ) {
      $deleted = true;
      foreach ( $files as $file ) {
        if ( is_array( $file ) ) {
          $deleted = self::removeFiles( $file );
        } else {
          if ( strpos( $file, '/images/' ) !== false ) {
            $folder = FOLDER . '/images/thumbs/' . md5( $file );
            if ( file_exists( $folder ) ) {
              self::delTree( $folder );
            }
            if ( file_exists( FOLDER . $file ) ) {
              $deleted = @unlink( FOLDER . $file ) && $deleted;
            }
          }
        }
      }
      return $deleted;
    } elseif ( is_string( $files ) ) {
      if ( strpos( $files, '/images/' ) !== false ) {
        $folder = FOLDER . '/images/thumbs/' . md5( $files );
        if ( file_exists( $folder ) ) {
          self::delTree( $folder );
        }
        if ( file_exists( FOLDER . $files ) ) {
          return @unlink( FOLDER . $files );
        }
      }
    } else {
      return false;
    }
  }

  public static function delTree( $dir )
  {
    $files = array_diff( scandir( $dir ), array( '.', '..' ) );
    foreach ( $files as $file ) {
      ( is_dir( "$dir/$file" ) ) ? delTree( "$dir/$file" ) : unlink( "$dir/$file" );
    }
    return rmdir( $dir );
  }

  public function getClientValidation( array $models = [] )
  {
    $formIdentifer = uniqid( 'id' );
    $class_name    = $this->formName();
    foreach ( $this->getActiveValidators() as $activeValidator ) {
      if ( $activeValidator->enableClientValidation ) {
        foreach ( $activeValidator->attributes as $attribute ) {
          if ( !is_null( $activeValidator->clientValidateAttribute( $this, $attribute, false ) ) ) {
            $attribute_form = $attribute;
            $option         = $activeValidator->clientValidateAttribute( $this, $attribute, false );
            if ( $activeValidator instanceof \Application\Validators\EachValidator )
              $attribute_form .= '[]';
            $option['attribute'] = $attribute_form;
            if ( isset( $option['compareAttribute'] ) )
              $option['compareAttribute'] = $option['compareAttribute'];
            $validation[$attribute_form][]        = $option;
            $validation[$attribute_form . '[]'][] = $option;
            if ( $activeValidator instanceof \Application\Validators\CompareValidator ) {
              $option                               = $activeValidator->clientValidateAttribute( $this, $attribute, false );
              $option['compareAttribute']           = $attribute_form;
              $option['attribute']                  = $attribute . '_repeat';
              $validation[$attribute . '_repeat'][] = $option;
            }
          }
        }
      }
    }

    foreach ( $models as $model ) {
      foreach ( $model->getActiveValidators() as $activeValidator ) {
        if ( $activeValidator->enableClientValidation ) {
          foreach ( $activeValidator->attributes as $attribute ) {
            if ( !is_null( $activeValidator->clientValidateAttribute( $model, $attribute, false ) ) ) {
              $attribute_form = $model->formName() . '[' . $attribute . ']';
              if ( $activeValidator instanceof \Application\Validators\EachValidator )
                $attribute_form .= '[]';
              $option                               = $activeValidator->clientValidateAttribute( $model, $attribute, false );
              $option['attribute']                  = $attribute_form;
              $validation[$attribute_form][]        = $option;
              $validation[$attribute_form . '[]'][] = $option;
              if ( $activeValidator instanceof \Application\Validators\CompareValidator ) {
                $option                                                    = $activeValidator->clientValidateAttribute( $model, $attribute, false );
                $option['compareAttribute']                                = $attribute_form;
                $option['attribute']                                       = $class_name . '[' . $attribute . '_repeat]';
                $validation[$class_name . '[' . $attribute . '_repeat]'][] = $option;
              }
            }
          }
        }
      }
    }
    //
    return '$("form[data-form-name=\'form' . $formIdentifer . '\']").formValidatorAdmin(' . json_encode( $validation, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE | JSON_HEX_QUOT | JSON_HEX_TAG | JSON_HEX_AMP | JSON_HEX_APOS ) . ');';

  }

}
