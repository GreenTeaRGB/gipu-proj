<?php
	
	namespace Application\Classes;
	Abstract Class ControllerBase extends ObjectMain
	{
		protected $template;
		private $layouts = 'main';
		public $breadcrumbs = array('Главная' => '/');
		
		public $vars = array();
		
		function __construct()
		{
			$this->template = new Template($this->layouts, get_class($this));
			self::setModule(get_class($this));
		}
		
		function getBreadcrumbs()
		{
			$content = '';
			$count = count($this->breadcrumbs);
			if ($count == 1)
				return '';
			$i = 1;
			foreach ($this->breadcrumbs as $title => $url) {
				$i++;

//				$url =  $url;
				if ($i <= $count)
					$content .= '<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a href="' . $url . '" itemprop="url"><span itemprop="title">' . $title . '</span></a></li>';
				else
					$content .= '<li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><span itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><span itemprop="title">' . $title . '</span></span></li>';
			}
			$content .= '';
			return $content;
		}
		
		public function replace($str, $data)
		{
			foreach ($data as $key => $value) {
				$str = str_replace($key, $value, $str);
			}
			return $str;
		}
		
		abstract function actionIndex();
		
		public function render($view, $params = [])
		{
			foreach ($params as $key => $param) {
				$this->template->vars($key, $param);
			}
			$this->template->view($view);
			
			return true;
		}
		
		public function setSeo($seo)
		{
			$this->template->setSeo($seo);
		}
		
		public function redirect($url, $statusCode = 303)
		{
			$url = '/' . ltrim($url, '/');
			
			header('Location: ' . $url, true, $statusCode);
			exit();
		}

		public function block($templateName, $vars = false){
			return $this->template->block($templateName, $vars);
		}

		public function getConfig($path){
			return $this->template->getConfig($path);
		}

		public function error404(){
      return $this->template->error404();
    }


    protected function login()
    {
      $this->redirect( '/user/login' );
    }

    protected function dashboard()
    {
      $this->redirect( '/user/dashboard' );
    }

    public function json($type, $message = '', $data = []){
      $res = ['success' => $type, 'message' => $message];
      $res = array_merge($res, $data);
      die(json_encode($res));
    }

	}