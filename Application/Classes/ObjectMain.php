<?php

namespace Application\Classes;
use function time;

/**
 * Class ObjectMain
 * @package Application\Classes
 */
abstract class ObjectMain
{

  /**
   * @var string
   */
  private static $module = '';

  /**
   * @return string
   */
  public static function getModule(): string
  {
    return self::$module;
  }

  /**
   * @param string $module
   */
  public static function setModule( string $module )
  {
    self::$module = $module;
  }

  /**
   * @param $element
   * @return string
   */
  public static function serialize( $element ): string
  {
    return serialize( array_filter( array_unique( (array)$element ), function ( $value ) {
      return $value != '';
    } ) );
  }

  /**
   * @param $element
   * @return mixed
   */
  public static function unserialize( $element ): array
  {
    if ( preg_match('/^a\:\d\:\{/', $element) ) {
      return unserialize( $element );
    }
    return [];
  }

  /**
   * @param $element
   * @return string
   */
  public static function json_encode( $element ): string
  {
    return json_encode( $element );
  }

  /**
   * @param $element
   * @return mixed
   */
  public static function json_decode( $element ): array
  {
    return (array)json_decode( $element, true );
  }

  /**
   * @param $element
   * @return false|int
   */
  public static function strtotime( $element )
  {
    return strtotime( $element );
  }

  /**
   * @param $element
   * @return false|string
   */
  public static function dateFormat( $element, $format = 'd.m.Y' )
  {
    if($element == 0) return 0;
    return date( $format , $element );
  }

}

?>