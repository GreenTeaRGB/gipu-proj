<?php

  namespace Application\Classes;

  /**
   * Class Router
   * @package Application\Classes
   */
  class Router
  {
    /**
     * @var mixed
     */
    private $routes;

    /**
     * Router constructor.
     */
    public function __construct()
    {
      $routesPath   = ROOT.'/config/routes.php';
      $this->routes = include( $routesPath );
    }

    /**
     * @return string
     */
    private function getURI()
    {
      if( !empty( $_SERVER['REQUEST_URI'] ) ) {
        return trim( $_SERVER['REQUEST_URI'], '/' );
      }
    }

    /**
     *
     */
    public function run()
    {
      $uri = explode( '?', $this->getURI() );
      $uri = $uri[0];

      foreach( $this->routes as $uriPattern => $path ) {

        if( preg_match( "~^$uriPattern$~", $uri ) ) {
          $internalRoute = preg_replace( "~^$uriPattern$~", $path, $uri );

          $segments = explode( '/', $internalRoute );

          $controllerName = array_shift( $segments ).'Controller';
          $controllerName = ucfirst( $controllerName );

          $actionName = 'action'.ucfirst( array_shift( $segments ) );

          $parameters = $segments;

          /*$controllerFile = ROOT . '/Controllers/' . $controllerName . '.php';

          if(file_exists($controllerFile)) {
            require_once ( $controllerFile );
          }
          else {
//										General::get404();
            return;
          }*/
          $controllerName   = '\Application\Controllers\\'.$controllerName;
          $controllerObject = new $controllerName;
          $k                = method_exists( $controllerObject, $actionName );
          if( $k == false ) {
            throw new \Exception("Роут не найден $controllerName - <b>$actionName</b> (это временная заглушка)");
            //                   General::get404();
            return;
          }

          $result = call_user_func_array( array( $controllerObject, $actionName ), $parameters );

          if( $result != null ) {
            break;
          }
        }
      }
    }

  }
