<?php

  namespace Application\Classes;

  use function var_dump;

  Class Template extends ObjectMain
  {
    private $template;
    private $folder;
    private $controller;
    private $layouts;
    public  $module;
    private $vars = array();

    public function __construct( $layouts, $controllerName, $template = 'default' )
    {

      $this->folder     = ROOT.'views/'.$template.'/';
      $this->layouts    = $layouts;
      $controllerName   = explode( '\\', $controllerName );
      $controllerName   = array_pop( $controllerName );
      $arr              = explode( 'Controller', $controllerName );
      $this->controller = strtolower( $arr[0] );
      $this->module     = $this->controller;
      //$this->vars('user_guest', User::isGuest());
      //$this->vars('currencies', Currency::getList());
	
    }

    public function vars( $varname, $value )
    {
      if( isset( $this->vars[$varname] ) == true ) {
//        trigger_error( 'Unable to set var `'.$varname.'`. Already set, and overwrite not allowed.', E_USER_NOTICE );
        return false;
      }
      $this->vars[$varname] = $value;
      return true;
    }

    public function setSeo( $data )
    {
      $this->title = isset( $data['meta_title'] )
          ? $data['meta_title']
          : '';

      $this->keywords = isset( $data['meta_keywords'] )
          ? $data['meta_keywords']
          : '';

      $this->description = isset( $data['meta_description'] )
          ? $data['meta_description']
          : '';

      $this->h1 = isset( $data['meta_h1'] )
          ? $data['meta_h1']
          : '';
      return true;
    }

    public function view( $name , $contentOnly = false)
    {
      $pathLayout  = $this->folder.$this->layouts.'.php';
      $contentPage = $this->folder.$this->controller.'/'.$name.'.php';
      if( file_exists( $pathLayout ) == false ) {
        trigger_error( 'Layout `'.$this->layouts.'` does not exist.', E_USER_NOTICE );
        return false;
      }
      if( file_exists( $contentPage ) == false ) {
        trigger_error( 'Template `'.$name.'` does not exist.', E_USER_NOTICE );
        return false;
      }
      extract( $this->vars );
      ob_start();
      include $contentPage;
      $content = ob_get_contents();
      ob_end_clean();
      if($contentOnly)
        return $content;
      include( $pathLayout );
    }

    public function block( $templateName, $vars = false, $content = false )
    {
      if( is_array( $vars ) ) {
        $this->vars = array_merge( $this->vars, $vars );
      }

      extract( $this->vars );
      $file = $this->folder.$templateName.'.php';
      if( file_exists( $file ) ) {
        if(!$content)
        {
          require( $file );
        }
        else{
          ob_start();
          require( $file );
          $content = ob_get_contents();
          ob_end_clean();
          return $content;
        }
      }
    }

    public function error404()
    {
      $this->controller = 'site';
      header( "HTTP/1.1 404 Not Found" );
      $this->view( '404' );
      die;
    }

    public function getConfig( $path )
    {
      $file = FOLDER.$path.'.php';
      if( !file_exists( $file ) )
        return false;
      return require $file;

    }

  }