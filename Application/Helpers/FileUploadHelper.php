<?php

namespace Application\Helpers;

/**
 * Class FileUploadHelper
 * @package Application\Helpers
 */
class FileUploadHelper
{
  /**
   * @var array
   */
  private static $white_ext = [ 'jpg', 'jpeg', 'gif', 'png', 'doc', 'docx', 'xls', 'xlsx', 'pdf' ];
  /**
   * @var
   */
  private static $_mimeTypes;

  /**
   * @param      $files
   * @param bool $path
   * @return array
   */
  public static function uploadFiles( $files, $path = false )
  {
    if ( $path && !file_exists( $path ) ) {
      mkdir( $path );
    }
    $data = [];
    foreach ( $files as $field => $dataFile ) {
      if ( is_array( $dataFile['tmp_name'] ) ) {
        foreach ( $dataFile['tmp_name'] as $key => $value ) {
          $filename = self::uploadFile( $dataFile['name'][$key], $dataFile['tmp_name'][$key], $dataFile['type'][$key], $path );
          if ( $filename ) {
            $data[$field][] = $filename;
          }
        }
      } else {
        $data[$field][] = self::uploadFile( $dataFile['name'], $dataFile['tmp_name'], $dataFile['type'], $path );
      }
    }
    return $data;
  }

  /**
   * @param $name
   * @param $tmp_name
   * @param $type
   * @param $path
   * @return bool|string
   */
  private static function uploadFile( $name, $tmp_name, $type, $path )
  {
    $filename = false;
    if ( is_uploaded_file( $tmp_name ) ) {
      $ext = self::getExt( $name, $type );
      if ( !$ext ) return false;
      $filename = $path !== false ? $path . '/' . md5( $tmp_name . uniqid() ) . '.' . $ext
        : '/images/' . md5( $tmp_name . uniqid() ) . '.' . $ext;
      move_uploaded_file( $tmp_name, FOLDER . $filename );
    }
    return $filename;
  }

  /**
   * @param $name
   * @param $type
   * @return bool|mixed
   */
  private static function getExt( $name, $type )
  {
    $file    = explode( '.', $name );
    $fileext = array_pop( $file );
    if ( !in_array( strtolower( $fileext ), self::$white_ext ) ) {
      return false;
    }
//			switch($type) {
//				case 'image/*':
//					$ext = 'jpg';
//					break;
//				case '*':
//					$ext = $fileext;
//					break;
//			}
    return $fileext;
  }

  /**
   * @param      $file
   * @param null $magicFile
   * @param bool $checkExtension
   * @return mixed|null
   * @throws \Exception
   */
  public static function getMimeType( $file, $magicFile = null, $checkExtension = true )
  {
    if ( $magicFile !== null ) {
      $magicFile = include( $magicFile );
    }
    if ( !extension_loaded( 'fileinfo' ) ) {
      if ( $checkExtension ) {
        return static::getMimeTypeByExtension( $file, $magicFile );
      } else {
        throw new \Exception( 'The fileinfo PHP extension is not installed.' );
      }
    }
    $info = finfo_open( FILEINFO_MIME_TYPE, $magicFile );
    if ( $info ) {
      $result = finfo_file( $info, $file );
      finfo_close( $info );

      if ( $result !== false ) {
        return $result;
      }
    }

    return $checkExtension ? static::getMimeTypeByExtension( $file, $magicFile ) : null;
  }

  /**
   * @param      $file
   * @param null $magicFile
   * @return null
   */
  public static function getMimeTypeByExtension( $file, $magicFile = null )
  {
    $mimeTypes = static::loadMimeTypes( $magicFile );

    if ( ( $ext = pathinfo( $file, PATHINFO_EXTENSION ) ) !== '' ) {
      $ext = strtolower( $ext );
      if ( isset( $mimeTypes[$ext] ) ) {
        return $mimeTypes[$ext];
      }
    }

    return null;
  }

  /**
   * @param      $mimeType
   * @param null $magicFile
   * @return array
   */
  public static function getExtensionsByMimeType( $mimeType, $magicFile = null )
  {
    $mimeTypes = static::loadMimeTypes( $magicFile );
    return array_keys( $mimeTypes, mb_strtolower( $mimeType, 'utf-8' ), true );
  }

  /**
   * @param $magicFile
   * @return mixed
   */
  public static function loadMimeTypes( $magicFile )
  {
    if ( $magicFile == null )
      return require FOLDER . 'Application/Validators/mimeTypes.php';
    else
      return require FOLDER . $magicFile;
  }


}

?>