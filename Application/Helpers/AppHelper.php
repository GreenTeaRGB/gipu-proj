<?php

namespace Application\Helpers;

use \Application\Classes\Session;

class AppHelper
{
  public static function isAssoc( array $arr )
  {
    if ( [] === $arr ) return false;
    return array_keys( $arr ) !== range( 0, count( $arr ) - 1 );
  }

  public static function calculatePriceWithCommission( $price, $commission )
  {
    $result = 0;
    if ( $price ) {
      $commission = ( $commission && $commission > 0 ) ? ( 1 + ( $commission / 100 ) ) : 1;

      $result = $price * $commission;
      $result = ceil( $result * 100 ) / 100;
    }

    return $result;
  }

  public static function buildTree( $items, $parentId = 0 )
  {
    $result = [];

    if ( $items && count( $items ) > 1 ) {
      foreach ( $items as $item ) {
        if ( $item['parent_id'] == $parentId ) {
          $item['children'] = self::buildTree( $items, $item['id'] );
          $result[]         = $item;
        }
      }
      return $result;
    } else {
      return $items;
    }
  }

  public static function getCartItemsCountWithTotal()
  {
    $cartData = Session::getByKey( 'cart' );
    $count    = 0;
    $total    = 0;
    if ( $cartData )
      foreach ( $cartData as $cartItem ) {
        $count += $cartItem['count'];
        $total += self::calculatePriceWithCommission( $cartItem['product']['price'], $cartItem['product']['commission'] ) * $cartItem['count'];
      }

    $discount = Session::getByKey( 'discount' );
//			echo '<pre>';
//			var_dump($discount, $total);
//			die;
    if ( $discount ) {
      $total -= $discount['discount'];
    }

    $data = [
      'count' => $count,
      'total' => number_format( (float)$total, 2, '.', '' )
    ];

    return $data;
  }

  public static function showMessages()
  {
    $messages = Session::getByKey( 'message' );
    $html     = '';

    if ( $messages ) {
      if ( !self::isAssoc( $messages ) ) {
        if ( $messages && count( $messages ) > 0 ) {
          foreach ( $messages as $message ) {
            if ( isset( $message['type'] ) && $message['text'] ) {
              $html .= '<div style="color:red;" class="message message_' . $message['type'] . '">' . $message['text'] . '</div>';
            }
          }

          return $html;
        }
      } else {
        if ( isset( $messages['type'] ) && $messages['text'] ) {
          $html .= '<div style="color:red;" class="message message_' . $messages['type'] . '">' . $messages['text'] . '</div>';
        }
        return $html;
      }
    }

    return false;
  }

  public static function eraseMessages()
  {
    Session::remove( 'message' );
    return true;
  }

  public static function setMessage( $type = '', $text )
  {
    $message = [
      'type' => $type,
      'text' => $text
    ];

    Session::store(
      [
        'message' => $message
      ]
    );
  }

  public static function generateRandomString( $length = 10 )
  {
    $characters       = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen( $characters );
    $randomString     = '';
    for ( $i = 0; $i < $length; $i++ ) {
      $randomString .= $characters[rand( 0, $charactersLength - 1 )];
    }
    return $randomString;
  }

  public static function compare( $operation, $value, $compareValue )
  {
    switch ( $operation ) {
      case '>' :
        return $value > $compareValue;
      case '>=':
        return $value >= $compareValue;
      case '<' :
        return $value < $compareValue;
      case '<=' :
        return $value <= $compareValue;
      case '!' :
        return $value != $compareValue;
      default:
        return $value == $compareValue;
    }
  }

}