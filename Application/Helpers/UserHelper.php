<?php

	namespace Application\Helpers;

	use \Application\Models\User;
	use \Application\Models\UserType;
	use \Application\Classes\Session;
	use \Application\Helpers\DataHelper;

	class UserHelper
	{

		private static $user;

		public static function getUser()
		{
			$user = Session::getByKey('user');
			
			if( $user && isset($user['id']) ) {
				$id = $user['id'];
				if( self::$user === null ) {
					self::$user = User::getById((int)$id);
					if ( self::$user === false ) {
						Session::remove('user');
					}
				}
				return self::$user;
			}
			Session::remove('user');
			return false;
		}

		public static function isAdmin()
		{
			if( self::getUser() && (self::$user['type_code'] == 'admin' || self::$user['type_code'] == 'moderator' )) {
				return true;
			}
			return false;
		}
		
		public static function getUserTypes()
		{
			$userTypeModel = new UserType();
			$userTypes = $userTypeModel->fetchAll();
			return  array_slice($userTypes, 2);
		}

		public static function userIsVerified()
		{
			if( self::getUser() && (self::$user['status_id'] == DataHelper::USER_VERIFIED_STATUS )) {
				return true;
			}
			return false;
		}

		public static function hashPassword($password){
		  return md5(DataHelper::SALT. $password);
    }
	}

	?>