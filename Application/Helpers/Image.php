<?php

  namespace Application\Helpers;

  class Image
  {
    private $filename;

    public function __construct( $filename )
    {
      $this->filename = $filename;
    }

    public function thumb( $width = null, $height = null, $crop = true )
    {
      if( $this->filename && file_exists( ( $filename = FOLDER.$this->filename ) ) ) {
        $info         = pathinfo( $filename );
        $thumbName    = $info['filename'].'-'.md5( filemtime( $filename ).(int) $width.(int) $height.(int) $crop ).'.'.$info['extension'];
        $thumbFile    = FOLDER.'/images/thumbs/'.$thumbName;
        $thumbWebFile = '/images/thumbs/'.$thumbName;
        if( file_exists( $thumbFile ) ) {
          return $thumbWebFile;
        } elseif( $this->copyResizedImage( $filename, $thumbFile, $width, $height, $crop ) ) {
          return $thumbWebFile;
        }
      }
      return '';
    }

    public function copy( $width = null, $height = null, $crop = true ){
      if( $this->filename ) {
        $info         = pathinfo( $this->filename );
        $thumbName    = $info['filename'].'-'.md5(  $this->filename .(int) $width.(int) $height.(int) $crop ).'.'.$info['extension'];
        $thumbFile    = FOLDER.'/images/thumbs/'.$thumbName;
        $thumbWebFile = '/images/thumbs/'.$thumbName;
        if( file_exists( $thumbFile ) ) {
          return $thumbWebFile;
        } elseif( $this->copyResizedImage( $this->filename, $thumbFile, $width, $height, $crop ) ) {
          return $thumbWebFile;
        }
      }
      return '';
    }

    public function copyResizedImage( $inputFile, $outputFile, $width, $height = null, $crop = true )
    {
      if( extension_loaded( 'gd' ) ) {
        $image = new GD( $inputFile );
        if( $height ) {
          if( $width && $crop ) {
            $image->cropThumbnail( $width, $height );
          } else {
            $image->resize( $width, $height );
          }
        } else {
          $image->resize( $width );
        }
        return $image->save( $outputFile );
      } elseif( extension_loaded( 'imagick' ) ) {
        $image = new \Imagick( $inputFile );

        if( $height && !$crop ) {
          $image->resizeImage( $width, $height, \Imagick::FILTER_LANCZOS, 1, true );
        } else {
          $image->resizeImage( $width, null, \Imagick::FILTER_LANCZOS, 1 );
        }

        if( $height && $crop ) {
          $image->cropThumbnailImage( $width, $height );
        }

        return $image->writeImage( $outputFile );
      } else {
        throw new HttpException( 500, 'Please install GD or Imagick extension' );
      }
    }

    public function __toString()
    {
      return $this->filename;
    }
  }