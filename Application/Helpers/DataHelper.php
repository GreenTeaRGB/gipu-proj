<?php

namespace Application\Helpers;

class DataHelper
{
  const SALT = 'RHjb3WpRAI2raktKfhva';

  const DEFAULT_RESTRICTION_ID = 1;

  const DEFAULT_BUYER_TYPE_ID    = 3;
  const DEFAULT_SELLER_TYPE_ID   = 4;
  const DEFAULT_SUPPLIER_TYPE_ID = 5;

  const SELLER_PERSON_TYPE_ID       = 1;
  const SELLER_ORGANISATION_TYPE_ID = 2;

  const DEFAULT_USER_STATUS     = 1;
  const USER_WAITING_FOR_REVIEW = 2;
  const USER_REQUIRES_EDITING   = 3;
  const USER_REJECTED_STATUS    = 4;
  const USER_VERIFIED_STATUS    = 5;

  const DEFAULT_SELLER_TYPE = 1;

  const DEFAULT_STATUS_WAREHOUSE = 1;

  const DEFAULT_PRODUCT_STATUS_ID = 1;
  const PRODUCT_STATUS_ALLOWED    = 4;

  const PROMOTION_ACTIVE = 1;

  const DEFAULT_PAY_TYPE_ID = 1;

  const DEFAULT_ORDER_STATUS_ID = 1;
  const DEFAULT_ORDER_PARENT_ID = 0;

  const DEFAULT_DELIVERY_STATUS_ID = 1;

  const DEFAULT_DELIVERY_PRICE = 300;

  const DEFAULT_ORDER_INDEX = 0;

  const REVIEW_STATUS_ACTIVE = 1;

  const DEFAULT_ACTIVE = 1;

  const DEFAULT_DIALOG_MESSAGE_STATUS = 1;
  const DIALOG_MESSAGE_STATUS_READ    = 2;
  const DEFAULT_LOAD_MESSAGE_COUNT    = 50;

  const SHOP_STATUS_MOD          = 1;
  const SHOP_STATUS_NEED_CORRECT = 1;
  const SHOP_STATUS_ACTIVE       = 3;
  const SHOP_STATUS_NOT_ACTIVE   = 4;
  const SHOP_STATUS_DELETE       = 5;

  const DEFAULT_REFERRAL_LEVEL = 1;

  const SUPPORT_ID = 1;

  const PROPERTY_LIST_TYPE = 'L';

  const DEFAULT_REQUEST_OUTPUT_STATUS = 1;

  const TYPE_TRANSACTION         = [
    1 => [ 'id' => 1, 'name' => 'Перевод денежных средст между агентами' ],
    2 => [ 'id' => 2, 'name' => 'warning' ],
    3 => [ 'id' => 3, 'name' => 'error' ],
  ];
  const TYPE_TRANSACTION_INFO    = 1;
  const TYPE_TRANSACTION_WARNING = 2;
  const TYPE_TRANSACTION_ERROR   = 3;


  const REQUEST_OUTPUT_STATUS = [
    1 => [ 'id' => 1, 'name' => 'Принята' ],
    2 => [ 'id' => 2, 'name' => 'Обработана' ],
    3 => [ 'id' => 3, 'name' => 'Отменена' ]
  ];

  const STATUS_SUPPORT_QUESTION = [
    1 => [ 'id' => 1, 'name' => 'Без ответа' ],
    2 => [ 'id' => 2, 'name' => 'Ответ получен' ],
  ];

  const REGIONS = [
    1 => [
      'id'     => 1,
      'name'   => 'Москва',
      'active' => 1
    ],
    2 => [
      'id'     => 2,
      'name'   => 'Санкт-Петербург',
      'active' => 1
    ]
  ];

  const CITIES = [
    1 => [
      'id'     => 1,
      'name'   => 'Москва',
      'region' => 1,
      'active' => 1
    ],
    2 => [
      'id'     => 2,
      'name'   => 'СПб',
      'region' => 2,
      'active' => 1
    ]
  ];

  // Проценты при регистрации
  const DEPTH_ONE_PERCENT = 25.0 / 100;
  const DEPTH_TWO_PERCENT = 15.0 / 100;

  const PERCENT_REG_RANK_ONE   = 1.0 / 100;
  const PERCENT_REG_RANK_TWO   = 2.5 / 100;
  const PERCENT_REG_RANK_THREE = 4.5 / 100;
  const PERCENT_REG_RANK_FOUR  = 7.0 / 100;
  const PERCENT_REG_RANK_FIVE  = 10.0 / 100;

  // Проценты при покупке
  const RANK_ONE   = 5.0 / 100;
  const RANK_TWO   = 10.0 / 100;
  const RANK_THREE = 15.0 / 100;
  const RANK_FOUR  = 20.0 / 100;
  const RANK_FIVE  = 30.0 / 100;

  public static function getPercentRegByRank( $rank )
  {
    switch ( $rank ) {
      case 1 :
        return self::PERCENT_REG_RANK_ONE;
      case 2 :
        return self::PERCENT_REG_RANK_TWO;
      case 3 :
        return self::PERCENT_REG_RANK_THREE;
      case 4 :
        return self::PERCENT_REG_RANK_FOUR;
      case 5 :
        return self::PERCENT_REG_RANK_FIVE;
      default:
        return 0;
    }
  }

  public static function getPercentByRank( $rank )
  {
    switch ( $rank ) {
      case 1:
        return self::RANK_ONE;
      case 2:
        return self::RANK_TWO;
      case 3:
        return self::RANK_THREE;
      case 4:
        return self::RANK_FOUR;
      case 5:
        return self::RANK_FIVE;
      default:
        return 0;
    }
  }

  public static function getRegions()
  {
    return self::REGIONS;
  }

  public static function getCities()
  {
    return self::CITIES;
  }

  public static function getCityNameById( $cityId )
  {
    foreach ( self::CITIES as $city ) {
      if ( $city['id'] == $cityId ) {
        return $city['name'];
      }
    }
  }

  public static function getSummaryByRating( $rating )
  {
    $output = '';
    switch ( $rating ) {
      case 1:
        $output = 'Ужасная модель';
        break;
      case 2:
        $output = 'Плохая модель';
        break;
      case 3:
        $output = 'Модель с недостатками';
        break;
      case 4:
        $output = 'Хорошая модель';
        break;
      case 5:
        $output = 'Отличная модель';
        break;
      default:
        $output = 'Нет оценки';
    }

    return $output;
  }

  public static function translit( $str )
  {
    $str      = mb_strtolower( $str, 'UTF-8' );
    $translit = array(
      "А"  => "a",
      "Б"  => "b",
      "В"  => "v",
      "Г"  => "g",
      "Д"  => "d",
      "Е"  => "e",
      "Ж"  => "j",
      "З"  => "z",
      "И"  => "i",
      "Й"  => "y",
      "К"  => "k",
      "Л"  => "l",
      "М"  => "m",
      "Н"  => "n",
      "О"  => "o",
      "П"  => "p",
      "Р"  => "r",
      "С"  => "s",
      "Т"  => "t",
      "У"  => "u",
      "Ф"  => "f",
      "Х"  => "h",
      "Ц"  => "ts",
      "Ч"  => "ch",
      "Ш"  => "sh",
      "Щ"  => "sch",
      "Ъ"  => "",
      "Ы"  => "i",
      "Ь"  => "",
      "Э"  => "e",
      "Ю"  => "yu",
      "Я"  => "ya",
      "а"  => "a",
      "б"  => "b",
      "в"  => "v",
      "г"  => "g",
      "д"  => "d",
      "е"  => "e",
      "ж"  => "j",
      "з"  => "z",
      "и"  => "i",
      "й"  => "y",
      "к"  => "k",
      "л"  => "l",
      "м"  => "m",
      "н"  => "n",
      "о"  => "o",
      "п"  => "p",
      "р"  => "r",
      "с"  => "s",
      "т"  => "t",
      "у"  => "u",
      "ф"  => "f",
      "х"  => "h",
      "ц"  => "ts",
      "ч"  => "ch",
      "ш"  => "sh",
      "щ"  => "sch",
      "ъ"  => "y",
      "ы"  => "i",
      "ь"  => "",
      "э"  => "e",
      "ю"  => "yu",
      "я"  => "ya",
      " "  => "-",
      "'"  => "",
      '"'  => "",
      '.'  => '',
      '№'  => 'no',
      "\\" => "-",
      "/"  => "-",
      ","  => "-",
      "("  => '',
      ')'  => '',
      '?'  => '',
      '!'  => '',
      '«'  => '',
      '»'  => ''
    );
    $str_new  = strtr( $str, $translit );
    $str_new  = str_replace( [ '–', '--', '---' ], '-', $str_new );
    return $str_new;
  }

  public static function redate( $param, $time = 0 )
  {
    if ( intval( $time ) == 0 )
      $time = time();

    $MonthNames = array(
      "Января",
      "Февраля",
      "Марта",
      "Апреля",
      "Мая",
      "Июня",
      "Июля",
      "Августа",
      "Сентября",
      "Октября",
      "Ноября",
      "Декабря"
    );

    if ( strpos( $param, 'M' ) === false )
      return date( $param, $time );
    else
      return date( str_replace( 'M', $MonthNames[date( 'n', $time ) - 1], $param ), $time );
  }


}