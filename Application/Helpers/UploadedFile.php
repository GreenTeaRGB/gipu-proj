<?php

  namespace Application\Helpers;


  class UploadedFile
  {

    public $name;

    public $tempName;

    public $type;

    public $size;

    public $error;

    private $hashName = null;

    private static $_files;

    private $modelName = false;

    public function __construct( $config = [] )
    {
      foreach( $config as $field => $value ) {
        $this->$field = $value;
      }
    }

    public static function getInstance( $model, $attribute )
    {
      $name = new \ReflectionClass( $model );
      return static::getInstanceByName( $name->getShortName(), $attribute );
    }

    public static function getInstances( $model, $attribute )
    {
      $name = new \ReflectionClass( $model );
      return static::getInstancesByName( $name->getShortName(), $attribute );
    }

    public static function getInstanceByName( $name, $attribute )
    {
      $files = self::loadFiles();
      if(isset( $files[$name.'['.$attribute.']'] )){
        return $files[$name.'['.$attribute.']'];
      }
      if(isset( $files[$attribute] )){
        return $files[$attribute];
      }
      return null;
      
    }

    public static function getInstancesByName( $name, $attribute )
    {
      $files = self::loadFiles();
      if( isset( $files[$name] ) ) {
        return [ $files[$name] ];
      } elseif( isset( $files[$attribute] ) ) {
        return $files[$attribute];
      }
      $results = [];
      foreach( $files as $key => $file ) {
        if( strpos( $key, "{$name}[{$attribute}" ) === 0 || strpos( $key, "{$attribute}[" ) === 0 ) {
          $results[] = $file;
        }
      }
      return $results;
    }

    public static function reset()
    {
      self::$_files = null;
    }

    public function saveAs( $file, $deleteTempFile = true )
    {
      if( $this->error == UPLOAD_ERR_OK ) {
        if( $deleteTempFile ) {
          return move_uploaded_file( $this->tempName, $file );
        } elseif( is_uploaded_file( $this->tempName ) ) {
          return copy( $this->tempName, $file );
        }
      }
      return false;
    }

    public function save( $model )
    {
      $name = new \ReflectionClass( $model );
      if( !file_exists( FOLDER.'/images/'.strtolower( $name->getShortName() ) ) )
        @mkdir( FOLDER.'/images/'.strtolower( $name->getShortName() ) );
      $path = '/images/'.strtolower( $name->getShortName() ).'/'.$this->getHashName();
      if( $this->saveAs( FOLDER.$path ) )
        return $path;
      else
        return false;
    }

    public function getBaseName()
    {
      return pathinfo( $this->name, PATHINFO_FILENAME );
    }

    public function getExtension()
    {
      return strtolower( pathinfo( $this->name, PATHINFO_EXTENSION ) );
    }

    public function getHashName()
    {

      if( $this->hashName === null )
        $this->hashName = md5( $this->getBaseName().uniqid() ).'.'.$this->getExtension();
      return $this->hashName;
    }

    public function getHasError()
    {
      return $this->error != UPLOAD_ERR_OK;
    }

    private static function loadFiles()
    {
      if( self::$_files === null ) {
        self::$_files = [];
        if( isset( $_FILES ) && is_array( $_FILES ) ) {
          foreach( $_FILES as $class => $info ) {
            self::loadFilesRecursive( $class, $info['name'], $info['tmp_name'], $info['type'], $info['size'], $info['error'] );
          }
        }
      }
      return self::$_files;
    }

    public static function load( $field, $fileData )
    {
      if( isset( self::$_files[$field] ) )
        return self::$_files[$field];
      if( !isset( $fileData['name'] )
          || !isset( $fileData['tempName'] )
          || !isset( $fileData['type'] )
          || !isset( $fileData['size'] )
          || !isset( $fileData['error'] ) ){
        return false;
      }else{
        self::$_files[$field] = new static(
            [
                'name'     => $fileData['name'],
                'tempName' => $fileData['tempName'],
                'type'     => $fileData['type'],
                'size'     => $fileData['size'],
                'error'    => $fileData['error'],
            ]
        );
      }
      return self::$_files[$field];
    }

    private static function loadFilesRecursive( $key, $names, $tempNames, $types, $sizes, $errors )
    {
      if( is_array( $names ) ) {
        foreach( $names as $i => $name ) {
          self::loadFilesRecursive( $key.'['.$i.']', $name, $tempNames[$i], $types[$i], $sizes[$i], $errors[$i] );
        }
      } elseif( $errors !== UPLOAD_ERR_NO_FILE ) {
        self::$_files[$key] = new static(
            [
                'name'     => $names,
                'tempName' => $tempNames,
                'type'     => $types,
                'size'     => $sizes,
                'error'    => $errors,
            ]
        );
      }
    }
  }
