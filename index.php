<?php
ini_set('display_errors',1);
error_reporting(E_ALL);
session_start();
date_default_timezone_set('Europe/Moscow');
define('SECRET', '');
define('FOLDER', __DIR__ .'/');
define('ROOT', FOLDER.'/Application/');

require_once($_SERVER['DOCUMENT_ROOT'] . '/Application/Classes/Autoload.php');

\Application\Classes\Application::run();

$router = new \Application\Classes\Router();
$router->run();