$(document).ready(function () {
    $('.add_question').submit(function (e) {
        e.preventDefault();
        var that = $(this);

        var msgContainer = $('.messages');

        // console.log(that.data('question_type'));
        $.ajax({
                   type: "POST",
                   url: "/questions/add",
                   dataType: "json",
                   data: that.serialize(),
               }).done(function (data) {
            that.prop("disabled", false);
            window.notification('Ваш вопрос: ', data);
            var count = parseInt($('.icon.icon_small.icon-like .indicator').text());
            if(data.success && !data.delete){
                $('.icon.icon_small.icon-like .indicator').text(count+1);
            }else if(data.success && data.delete){
                if(count >= 1) count = count-1;
                $('.icon.icon_small.icon-like .indicator').text(count);
            }
            var message = '';
            if (data.hasOwnProperty('success') && data.success) {
                message = $('<div />', {
                                class: 'message message_success',
                                text: data.message
                            }
                );

                // location.reload();
            }
            else {
                message = $('<div />', {
                                class: 'message message_error',
                                text: data.message
                            }
                );
            }

            msgContainer.append(message);

            setTimeout(function () {
                message.remove();
            }, 3000);
        }).fail(function (data) {
            that.prop("disabled", false);

            var message = $('<div />', {
                                class: 'message message_error',
                                text: 'Ошибка отправки вопроса'
                            }
            );
            msgContainer.append(message);

            setTimeout(function () {
                message.remove()
            }, 3000);
        });

        return false;
    });

    $('.add_to_cart').on('click', function (e) {
        e.preventDefault();
        var that = $(this);
        var msgContainer = $('.messages');
        var productId = that.data('id');
        var warehouse_id = that.data('warehouse');
        var form = new FormData($('#product')[0]);
        form.append('product_id', productId);
        form.append('count', count);
        form.append('warehouse_id', warehouse_id);
        form.append('is_ajax', true);
        // return false;
        var count = that.closest('.button_group').find('.product_count').val();


        if (!count) {
            count = 1;
        }

        var defaultErrMsg = 'Ошибка добавления товара';

        $.ajax({
                   type: "POST",
                   url: "/cart/add",
                   dataType: "json",
                   processData: false,
                   contentType: false,
                   data: form,
                   // data: {
                   //     product_id: productId,
                   //     warehouse_id : warehouse_id,
                   //     count: count,
                   //     is_ajax: true
                   // },
                   beforeSend: function () {
                       that.text('Добавляется...');
                       that.prop("disabled", true);
                   }
               }).done(function (data) {
            that.html('В корзине');
            that.prop("disabled", false);

            var count = parseInt($('.icon.icon-basket .indicator').text());
            if(data.success){
              $('.icon.icon-basket .indicator').text(count+1);
            }
            // location.reload();

            // var message = '';
            // if (data.hasOwnProperty('success') && data.success) {
            //     window.notification('Добавление товара  в корзину', data.message);
            //     // message = $('<div />', {
            //     //                 class: 'message message_success',
            //     //                 text: data.message
            //     //             }
            //     // );
            // }
            // else {
            //     window.notification('Добавление товара  в корзину', data.message, 'error');
            //     // message = $('<div />', {
            //     //                 class: 'message message_error',
            //     //                 text: defaultErrMsg
            //     //             }
            //     // );
            // }

            // msgContainer.append(message);

            // setTimeout(function () {
            //     message.remove();
            // }, 3000);
        }).fail(function (data) {
            // that.text('Добавить в корзину');
            that.prop("disabled", false);
            if(data.message !== undefined)
            window.notification('Добавление товара  в корзину', data.message, 'error');
            // var message = $('<div />', {
            //                     class: 'message message_error',
            //                     text: defaultErrMsg
            //                 }
            // );
            // msgContainer.append(message);

            // setTimeout(function () {
            //     message.remove()
            // }, 3000);
        });

        return false;
    });

    $('.add_to_wishlist').on('click', function (e) {
        e.preventDefault();
        var that = $(this);
        var productId = that.data('id');
        var msgContainer = $('.messages');

        $.ajax({
                   type: "POST",
                   url: "/wish-list/toggle",
                   dataType: "json",
                   data: {
                       product_id: productId,
                       is_ajax: true
                   },
                   beforeSend: function () {
                       // that.text('Добавляется...');
                       that.prop("disabled", true);
                   }
               }).done(function (data) {
            // that.text('Добавить в избранное');
            that.prop("disabled", false);
            window.notification('Избранное', data.message, data.success ? 'success' : "error");
            var count = parseInt($('.icon.icon_small.icon-like .indicator').text());
            if(data.success && !data.delete){
              if($(that).hasClass('detail')){
                $('i', that).addClass('wishlist_active');
              }else{
                $(that).addClass('wishlist_active');
              }
                $('.icon.icon_small.icon-like .indicator').text(count+1);
            }else if(data.success && data.delete){
              if($(that).hasClass('detail')){
                $('i', that).removeClass('wishlist_active');
              }else{
                $(that).removeClass('wishlist_active');
              }
                if(count >= 1) count = count-1;
                $('.icon.icon_small.icon-like .indicator').text(count);
            }
            var message = '';
            if (data.hasOwnProperty('success') && data.success) {
                message = $('<div />', {
                                class: 'message message_success',
                                text: data.message
                            }
                );

                // location.reload();
            }
            else {
                message = $('<div />', {
                                class: 'message message_error',
                                text: data.message
                            }
                );
            }

            msgContainer.append(message);

            setTimeout(function () {
                message.remove();
            }, 3000);
        }).fail(function (data) {
            // that.text('Добавить в избранное');
            that.prop("disabled", false);

            var message = $('<div />', {
                                class: 'message message_error',
                                text: 'Ошибка добавления в список избранных'
                            }
            );
            msgContainer.append(message);

            setTimeout(function () {
                message.remove()
            }, 3000);
        });

        return false;
    });

    $('.add_to_compare').on('click', function (e) {
        e.preventDefault();
        var that = $(this);
        var productId = that.data('id');
        var msgContainer = $('.messages');
        var redirect = false;
        if($(this).hasClass('move')) redirect = true;
        $.ajax({
                   type: "POST",
                   url: "/compare/add",
                   dataType: "json",
                   data: {
                       product_id: productId,
                       is_ajax: true
                   },
                   beforeSend: function () {
                       // that.text('Добавляется...');
                       that.prop("disabled", true);
                   }
               }).done(function (data) {
            // that.text('Сравнить');
            that.prop("disabled", false);
            window.notification('Сравнение товаров', data.message, data.success ? 'success' : "error");
            var count = parseInt($('.icon.icon_small.icon-stats .indicator').text());
            if(data.success && !data.delete){
                $('.icon.icon_small.icon-stats .indicator').text(count+1);
                if($(that).hasClass('detail')){
                  $('i', that).addClass('compare_active');
                }else{
                  $(that).addClass('compare_active');
                }
            }else if(data.success && data.delete) {
              $('.icon.icon_small.icon-stats .indicator').text(count-1);
              if($(that).hasClass('detail')){
                $('i', that).removeClass('compare_active');
              }else{
                $(that).removeClass('compare_active');
              }
            }
            // if(redirect) window.location.href = '/compare';
            var message = '';
            if (data.hasOwnProperty('success') && data.success) {
                message = $('<div />', {
                                class: 'message message_success',
                                text: data.message
                            }
                );
            }
            else {
                message = $('<div />', {
                                class: 'message message_error',
                                text: data.message
                            }
                );
            }

            msgContainer.append(message);

            setTimeout(function () {
                message.remove();
            }, 3000);
        }).fail(function (data) {
            // that.text('Сравнить');
            that.prop("disabled", false);

            var message = $('<div />', {
                                class: 'message message_error',
                                text: data.message
                            }
            );
            msgContainer.append(message);

            setTimeout(function () {
                message.remove()
            }, 3000);
        });
    });

    $('.remove_compare_item').on('click', function () {
        // url - compare/remove-item
        // send categoryName too
    });

    $('a[href*="#"]:not(.carousel-control-next, .carousel-control-prev)').on('click', function (e) {
        var id = $(this).attr('href');
        try {
            $([document.documentElement, document.body]).animate({
                                                                     scrollTop: $(id).offset().top
                                                                 }, 1000);
        } catch (e) {

        }
    });

    $('.sort').on('click', function(e){
        e.preventDefault();

        $.ajax({
                   type: "POST",
                   url: "/category/sort",
                   dataType: "json",
                   data: {
                       sort: $(this).data('sort')
                   },
                   async: false,
                   beforeSend: function () {

                   }
               }).done(function (data) {
            window.location.reload()
        }).fail(function (data) {

        });

        return false;
    });
    $('.count').on('click', function(e){
        e.preventDefault();

        $.ajax({
                   type: "POST",
                   url: "/category/sort",
                   dataType: "json",
                   data: {
                       count: $(this).data('count')
                   },
                   async: false,
                   beforeSend: function () {

                   }
               }).done(function (data) {
            window.location.reload()
        }).fail(function (data) {

        });

        return false;
    });

    var csrf = $('meta[name=_csrf]').attr('content');
    $('form').each(function(){
        $(this).append($('<input>', {name:'_csrf', value: csrf, type:'hidden'}));
    });


});



// var Cart = function () {
//     // var self = this;
//     // this.items = [];
//     // this.itemsCount = 0;
//     // this.itemContainer = $('.cart_products');
//     //
//     // var msgContainer = $('.messages');
//     //
//     // var checkoutBtn = $('.checkout');
//     // var grandTotalElement = null;
//     // var cartIntermediateSum = null;
//     // this._intermediateTotal = 0;
//     // this._grandTotal = 0;
//     // this.promo = {};
//     //
//     // this._removed = [];
//     //
//     // this.delivery = {
//     //     note: 'В черте города',
//     //     price: 250
//     // };
//     //
//     // this.calculateItemsCount = function (items) {
//     //     if (items && items.length > 0) {
//     //         var count = 0;
//     //         for (var key in items) {
//     //             if (items.hasOwnProperty(key)) {
//     //                 count += parseInt(items[key].count);
//     //             }
//     //         }
//     //
//     //         return count;
//     //     }
//     //     else {
//     //         return false;
//     //     }
//     // }
//     //
//     // this.recalculateItemsCount = function () {
//     //     var itemsCount = self.calculateItemsCount(self.items);
//     //     $('.cart_intermediate_count').text(itemsCount);
//     // }
//     //
//     // this.checkItems = function () {
//     //     if (this.items && this.items.length > 0) {
//     //         checkoutBtn.show();
//     //
//     //         return true;
//     //     }
//     //     else {
//     //         checkoutBtn.hide();
//     //
//     //         return false;
//     //     }
//     // }
//     //
//     // this.changeItem = function (productId, paramName, value) {
//     //     var items = this.items;
//     //     for (var key in items) {
//     //         if (items.hasOwnProperty(key)) {
//     //             var item = items[key];
//     //             if (item.product.id == productId) {
//     //                 item[paramName] = value;
//     //                 if(paramName == 'count'){
//     //                     $('#id'+productId+' .price-total').text((item.subtotal*value).toFixed(2))
//     //                 }
//     //             }
//     //
//     //         }
//     //     }
//     // }
//     //
//     // this.calculateSubtotal = function (price, count) {
//     //     return (parseFloat(price) * parseInt(count)).toFixed(2);
//     // }
//     //
//     // this.recalculateSubtotal = function (productId, items) {
//     //     var subtotal = $('.cart_item_subtotal');
//     //     for (var key in items) {
//     //         if (items.hasOwnProperty(key)) {
//     //             if (items[key].product.id == productId) {
//     //                 var item = items[key];
//     //                 var updatedSubtotal = self.calculateSubtotal(item.product.full_price, item.count);
//     //
//     //                 subtotal.each(function (index, elem) {
//     //                     var element = $(elem);
//     //
//     //                     if (element.data('id') == productId) {
//     //                         element.text(updatedSubtotal);
//     //                         self.changeItem(productId, 'subtotal', updatedSubtotal);
//     //                     }
//     //                 });
//     //             }
//     //         }
//     //     }
//     // }
//     //
//     // this.calculateGrandTotal = function (items, deliveryPrice = 0, discount = 0) {
//     //     var sum = 0;
//     //
//     //     for (var key in items) {
//     //         if (items.hasOwnProperty(key)) {
//     //             var item = items[key];
//     //             sum += parseFloat(item.subtotal)*item.count;
//     //         }
//     //     }
//     //
//     //     sum += parseInt(deliveryPrice);
//     //     sum -= parseInt(discount);
//     //     return (sum).toFixed(2);
//     // }
//     //
//     // this.recalculateGrandTotal = function (changeElem, items, deliveryPrice = 0, discount = 0) {
//     //     var sum = this.calculateGrandTotal(items, deliveryPrice, discount);
//     //     changeElem.text(sum);
//     //
//     //     this.recalculateItemsCount();
//     // }
//     //
//     // this.drawTable = function (callback = false) {
//     //     var oldTable = $('.cart_items');
//     //     var oldGrandTotal = $('.cart_grand_total');
//     //     var oldEmptyCart = $('.cart_empty');
//     //
//     //     if (oldTable.length > 0) {
//     //         oldTable.remove();
//     //     }
//     //     if (oldGrandTotal.length > 0) {
//     //         oldGrandTotal.remove();
//     //     }
//     //     if (oldEmptyCart.length > 0) {
//     //         oldEmptyCart.remove();
//     //     }
//     //     if (this.checkItems()) {
//     //         if (callback) {
//     //             callback();
//     //         }
//     //         var ul = $('#basket-list').html('');
//     //         var items = this.items;
//     //         // var table = $('<table />', {
//     //         //     class: 'cart_items'
//     //         // });
//     //         // this.itemContainer.append(table);
//     //         // var tableHead = '<tr><th>Название</th><th>Количество</th><th>Стоимость</th><th>Комиссия</th><th>Общая стоимость</th><th></th></tr>';
//     //         // table.append(tableHead);
//     //         for (var key in items) {
//     //             if (items.hasOwnProperty(key)) {
//     //                 var item = items[key];
//     //                 console.log(item);
//     //                 var li = $('<li>', {class: 'basket__product', id: 'id'+item.product.id})
//     //                 var first_div = $('<div>', { class: 'col-md-6'});
//     //                 var first_a = $('<a>', {href: '/catalog/product/'+item.product.alias});
//     //                 var first_img = $('<img>', {src: item.product.images[0]})
//     //                 var inner_div = $('<div>', { class: 'inner' });
//     //                 var span = $('<span>', { class: 'sub-text', text: item.category });
//     //                 var p = $('<p>', { text: item.product.name});
//     //                 inner_div.append(span).append(p);
//     //                 first_a.append(first_img).append(inner_div);
//     //                 first_div.append(first_a);
//     //
//     //                 var second_div = $('<div>', { class: 'col-md-6'});
//     //                 var b = $('<b>', { class: 'ruble cart_item_price price', text: item.product.price });
//     //                 var number_div = $('<div>', { class: 'number' });
//     //                 var minButton = $('<button>', { html: '&#8211;', class: 'decrement' });
//     //                 var maxButton = $('<button>', { text: '+' , class: 'increment'});
//     //                 var input = $('<input>',
//     //                               { disabled:'disabled',
//     //                                   type: 'number',
//     //                                   min: 1,
//     //                                   max: 5000,
//     //                                   value: item.count,
//     //                                   class: 'item_count',
//     //                                   'data-id': item.product.id
//     //                               }
//     //                 );
//     //                 var total_b = $('<b>', { class: 'ruble price-total', text: (item.product.full_price*item.count) });
//     //                 var button_del = $('<button>',
//     //                                    {
//     //                                        class:'delete cart_elem remove_cart_item',
//     //                                        "data-id": item.product.id,
//     //                                        html: '<i class="icon-delete"></i>'
//     //                                    });
//     //                 second_div.append(b);
//     //                 number_div.append(minButton).append(input).append(maxButton);
//     //                 second_div.append(number_div).append(total_b).append(button_del);
//     //                 li.append(first_div);
//     //                 li.append(second_div);
//     //                 ul.append(li);
//     //
//     //                 // var item = items[key];
//     //                 //
//     //                 // var row = $('<tr />', {
//     //                 //     class: 'cart_item'
//     //                 // });
//     //                 //
//     //                 // var imageSrc = (typeof item.product.images != "undefined")
//     //                 //     ? item.product.images[0]
//     //                 //     : '/images/no_photo.png';
//     //                 //
//     //                 // var prodDetails = $('<td />', {
//     //                 //     class: 'cart_elem'
//     //                 // }).append(
//     //                 //     $('<img />', {
//     //                 //         class: 'cart_elem_img',
//     //                 //         src: imageSrc
//     //                 //     })
//     //                 // ).append(
//     //                 //     $('<div />', {
//     //                 //         class: 'cart_elem_category',
//     //                 //         text: item.category
//     //                 //     })
//     //                 // ).append(
//     //                 //     $('<div />', {
//     //                 //         class: 'cart_elem_name',
//     //                 //         text: item.product.name
//     //                 //     })
//     //                 // );
//     //                 //
//     //                 // prodDetails.appendTo(row);
//     //                 //
//     //                 // var priceCell = $('<td />').append(
//     //                 //     $('<span />', {
//     //                 //         class: 'cart_item_price',
//     //                 //         text: item.product.full_price
//     //                 //     })
//     //                 // ).append(
//     //                 //     $('<span />', {
//     //                 //         class: 'cart_item_currency',
//     //                 //         text: '₽'
//     //                 //     })
//     //                 // );
//     //                 //
//     //                 // priceCell.appendTo(row);
//     //                 //
//     //                 // var counter = $('<td />', {
//     //                 //     class: 'cart_elem counter_wrap'
//     //                 // });
//     //                 // counter.append(
//     //                 //     $('<span />', {
//     //                 //         class: 'decrement',
//     //                 //         text: '-'
//     //                 //     })
//     //                 // );
//     //                 // counter.append(
//     //                 //     $('<input />', {
//     //                 //         value: item.count,
//     //                 //         class: 'item_count'
//     //                 //     }).attr({
//     //                 //         type: 'text', /*number*/
//     //                 //         min: 1,
//     //                 //         'data-id': item.product.id
//     //                 //     })
//     //                 // );
//     //                 // counter.append(
//     //                 //     $('<span />', {
//     //                 //         class: 'increment',
//     //                 //         text: '+'
//     //                 //     })
//     //                 // );
//     //                 // counter.appendTo(row);
//     //                 //
//     //                 var subtotal = self.calculateSubtotal(item.product.full_price, 1);
//     //                 self.changeItem(item.product.id, 'subtotal', subtotal);
//     //                 //
//     //                 // var subtotalWrap = $('<td />').append(
//     //                 //     $('<span />', {
//     //                 //         class: 'cart_item_subtotal',
//     //                 //         text: subtotal
//     //                 //     }).attr("data-id", item.product.id)
//     //                 // ).append(
//     //                 //     $('<span />', {
//     //                 //         class: 'cart_item_currency',
//     //                 //         text: '₽'
//     //                 //     })
//     //                 // );
//     //                 //
//     //                 // subtotalWrap.appendTo(row);
//     //                 //
//     //                 // $('<td />', {
//     //                 //     class: 'cart_elem remove_cart_item',
//     //                 //     text: 'Удалить'
//     //                 // }).attr("data-id", item.product.id).appendTo(row);
//     //                 //
//     //                 // row.appendTo(table);
//     //             }
//     //         }
//     //
//     //         var rowGrandTotal = $('<div />', {
//     //             class: 'cart_grand_total'
//     //         }).append(
//     //             $('<div />', {
//     //                 class: 'additional'
//     //             }).append(
//     //                 $('<div />', {
//     //                     class: 'promo'
//     //                 }).append(
//     //                     $('<p />', {
//     //                         class: 'promo_title',
//     //                         text: 'У Вас есть промокод?'
//     //                     })
//     //                 ).append(
//     //                     $('<div />').append(
//     //                         $('<input />', {
//     //                             class: 'promo_code',
//     //                             type: 'text'
//     //                         })
//     //                     ).append(
//     //                         $('<a />', {
//     //                             class: 'activate_promo',
//     //                             text: 'Активировать'
//     //                         })
//     //                     )
//     //                 )
//     //             )
//     //         ).append(
//     //             $('<div />', {
//     //                 class: 'cart_intermediate'
//     //             }).append(
//     //                 $('<div />', {}).append(
//     //                     $('<span />', {
//     //                         class: 'cart_intermediate_products',
//     //                         text: 'Товары'
//     //                     }).append(
//     //                         $('<span />', {
//     //                             text: '('
//     //                         })
//     //                     )
//     //                       .append(
//     //                           $('<span />', {
//     //                               class: 'cart_intermediate_count',
//     //                               text: self.itemsCount
//     //                           })
//     //                       )
//     //                       .append(
//     //                           $('<span />', {
//     //                               text: ')'
//     //                           })
//     //                       )
//     //                 ).append(
//     //                     $('<span />', {
//     //                         class: 'cart_intermediate_sum',
//     //                         text: self.calculateGrandTotal(self.items)
//     //                     })
//     //                 ).append(
//     //                     $('<span />', {
//     //                         class: 'cart_item_currency',
//     //                         text: '₽'
//     //                     })
//     //                 )
//     //             ).append(
//     //                 $('<div />', {}).append(
//     //                     $('<span />', {
//     //                         class: 'cart_intermediate_delivery',
//     //                         text: 'Доставка'
//     //                     })
//     //                 ).append(
//     //                     $('<span />', {
//     //                         class: 'cart_intermediate_delivery_price',
//     //                         text: self.delivery.price
//     //                     }).append(
//     //                         $('<span />', {
//     //                             class: 'cart_item_currency',
//     //                             text: '₽'
//     //                         })
//     //                     )
//     //                 ).append(
//     //                     $('<span />', {
//     //                         class: 'note',
//     //                         text: self.delivery.note
//     //                     })
//     //                 )
//     //             )
//     //         )
//     //           .append(
//     //               $('<div />', {
//     //                   class: 'grand_total_sum'
//     //               }).append(
//     //                   $('<span />', {
//     //                       class: 'grand_total_title',
//     //                       text: 'К оплате'
//     //                   })
//     //               ).append(
//     //                   $('<span />', {
//     //                       class: 'grand_total',
//     //                       text: self.calculateGrandTotal(self.items, self.delivery.price)
//     //                   })
//     //               ).append(
//     //                   $('<span />', {
//     //                       class: 'cart_item_currency',
//     //                       text: '₽'
//     //                   })
//     //               )
//     //           )
//     //           .append(
//     //               $('<div />', {
//     //                   class: 'button_group'
//     //               }).append(
//     //                   $('<a />', {
//     //                       class: 'button checkout',
//     //                       text: 'Оформить заказ'
//     //                   })
//     //               )
//     //           );
//     //         $('.basket > div').append(rowGrandTotal);
//     //         // table.parent().append(rowGrandTotal);
//     //
//     //         if (!jQuery.isEmptyObject(self.promo)) {
//     //             self.setDiscount($('.activate_promo'), self.promo);
//     //         }
//     //     }
//     //     else {
//     //         this.itemContainer.append(
//     //             $('<div />', {
//     //                   class: 'cart_empty',
//     //                   text: 'Корзина пуста'
//     //               }
//     //             )
//     //         );
//     //     }
//     // }
//     //
//     // this.setDiscount = function (btn, discount) {
//     //     btn.addClass('activated');
//     //     btn.text('Удалить');
//     //
//     //     $('.promo_title').hide();
//     //
//     //     $('.promo_code').hide();
//     //
//     //     btn.parent().prepend(
//     //         $('<span />', {
//     //             class: 'promocode_title',
//     //             text: self.promo.title
//     //         })
//     //     );
//     //
//     //     self.recalculateGrandTotal($('.grand_total'), self.items, self.delivery.price, discount.discount);
//     // }
//     //
//     // this.buildRemovedItem = function (item, appendTo) {
//     //     // <span>Вы удалили <a href="#!" id="deletedItems"></a></span><button class="btn btn_white" id="basketUndelete">Отменить</button>
//     //     return $('<div />', {
//     //         class: 'removed_item'
//     //     }).append(
//     //         $('<span />', {
//     //             class: 'removed_title',
//     //             text: 'Вы удалили'
//     //         })
//     //     ).append(
//     //         $('<span />', {
//     //             class: 'removed_item_name',
//     //             text: item.product.name
//     //         })
//     //     )
//     //       .append(
//     //           $('<button />', {
//     //               class: 'restore_removed btn btn_white',
//     //               text: 'Отменить',
//     //           }).attr("data-id", item.product.id)
//     //             .attr("data-warehouse", item.warehouse_id)
//     //       )
//     //     // .appendTo(appendTo);
//     // }
//     //
//     // this.drawBlockForRemoved = function (items) {
//     //     var oldBlockForRemoved = $('.removed_items');
//     //     console.log(items);
//     //     if (oldBlockForRemoved.length > 0) {
//     //         oldBlockForRemoved.remove();
//     //     }
//     //
//     //     if (items && items.length > 0) {
//     //         var container = self.itemContainer;
//     //         // var itemsWrapper = $('<div />', {
//     //         //     class: 'removed_items'
//     //         // });
//     //         var itemsWrapper = $('.basket__deleted').css('display', 'block');
//     //         for (var key in items) {
//     //             if (items.hasOwnProperty(key)) {
//     //                 itemsWrapper.append(self.buildRemovedItem(items[key], itemsWrapper));
//     //             }
//     //         }
//     //
//     //         // container.append(itemsWrapper);
//     //     }else {
//     //         $('.basket__deleted').css('display', 'none')
//     //     }
//     // }
//     //
//     // this.addEventListeners = function () {
//     //     var msgContainer = $('.messages');
//     //
//     //     $('.cart_products').on('click', '.remove_cart_item', function () {
//     //         var that = $(this);
//     //         var productId = that.data('id');
//     //
//     //         var defaultErrMsg = 'Ошибка удаления товара';
//     //
//     //         $.ajax({
//     //                    type: "POST",
//     //                    url: "/cart/remove",
//     //                    dataType: "json",
//     //                    data: {
//     //                        product_id: productId,
//     //                        is_ajax: true
//     //                    },
//     //                    beforeSend: function () {
//     //                        that.text('Удаляется...');
//     //                        that.prop("disabled", true);
//     //                    }
//     //                }).done(function (data) {
//     //             that.text('Удалить');
//     //             that.prop("disabled", false);
//     //
//     //             if (data.hasOwnProperty('success') && data.success) {
//     //                 var newItems = [];
//     //                 for (var key in self.items) {
//     //                     if (self.items.hasOwnProperty(key)) {
//     //                         if (self.items[key].product.id != productId) {
//     //                             newItems.push(self.items[key]);
//     //                         }
//     //                         else {
//     //                             self._removed.push(self.items[key]);
//     //                         }
//     //                     }
//     //                 }
//     //
//     //                 self.items = newItems;
//     //                 self.drawTable();
//     //                 self.drawBlockForRemoved(self._removed);
//     //             }
//     //             else {
//     //                 var message = $('<div />', {
//     //                                     class: 'message message_error',
//     //                                     text: defaultErrMsg
//     //                                 }
//     //                 );
//     //                 msgContainer.append(message);
//     //
//     //                 setTimeout(function () {
//     //                     message.remove()
//     //                 }, 3000);
//     //             }
//     //         }).fail(function (data) {
//     //             that.text('Удалить');
//     //             that.prop("disabled", false);
//     //
//     //             var message = $('<div />', {
//     //                                 class: 'message message_error',
//     //                                 text: defaultErrMsg
//     //                             }
//     //             );
//     //             self.drawTable();
//     //
//     //             msgContainer.append(message);
//     //
//     //             setTimeout(function () {
//     //                 message.remove()
//     //             }, 3000);
//     //         });
//     //
//     //         return false;
//     //     });
//     //
//     //     $('.cart_products').on('click', '.decrement', function () {
//     //         var that = $(this);
//     //         var counterInput = that.parent().find('.item_count');
//     //         var value = parseInt(counterInput.val());
//     //
//     //         if (value <= 1) {
//     //             counterInput.val(1);
//     //         }
//     //         else {
//     //             counterInput.val(value - 1);
//     //         }
//     //
//     //         var productId = counterInput.data('id');
//     //         self.changeItem(productId, 'count', parseInt(counterInput.val()));
//     //         self.recalculateSubtotal(productId, self.items);
//     //         self.recalculateGrandTotal($('.grand_total'), self.items, self.delivery.price, self.promo.discount);
//     //         self.recalculateGrandTotal($('.cart_intermediate_sum'), self.items);
//     //     });
//     //
//     //     $('.cart_products').on('click', '.increment', function () {
//     //         var that = $(this);
//     //         var counterInput = that.parent().find('.item_count');
//     //         var value = parseInt(counterInput.val());
//     //
//     //         counterInput.val(value + 1);
//     //
//     //         var productId = counterInput.data('id');
//     //         self.changeItem(productId, 'count', parseInt(counterInput.val()));
//     //
//     //         self.recalculateSubtotal(productId, self.items);
//     //         self.recalculateGrandTotal($('.grand_total'), self.items, self.delivery.price, self.promo.discount);
//     //         self.recalculateGrandTotal($('.cart_intermediate_sum'), self.items);
//     //     });
//     //
//     //     $('.cart_products').on('keypress', '.item_count', function (event) {
//     //         event = (event)
//     //             ? event
//     //             : window.event;
//     //         var charCode = (event.which)
//     //             ? event.which
//     //             : event.keyCode;
//     //         if (charCode > 31 && (charCode < 48 || charCode > 57)) {
//     //             return false;
//     //         }
//     //     });
//     //
//     //     $('.cart_products').on('change', '.item_count', function (event) {
//     //         var that = $(this);
//     //         var productId = that.data('id');
//     //
//     //         self.changeItem(productId, 'count', parseInt(that.val()));
//     //         self.recalculateSubtotal(productId, self.items);
//     //
//     //         self.recalculateGrandTotal($('.grand_total'), self.items, self.delivery.price, self.promo.discount);
//     //         self.recalculateGrandTotal($('.cart_intermediate_sum'), self.items);
//     //     });
//     //
//     //     $('.cart_wrap').on('click', '.checkout', function () {
//     //         var that = $(this);
//     //         var defaultErrMsg = 'Ошибка сохранения корзины';
//     //
//     //         $.ajax({
//     //                    type: "POST",
//     //                    url: "/cart/save",
//     //                    dataType: "json",
//     //                    data: {
//     //                        cart_data: self.items,
//     //                        is_ajax: true
//     //                    },
//     //                    beforeSend: function () {
//     //                        that.prop("disabled", true);
//     //                        /** todo show loader */
//     //                    }
//     //                }).done(function (data) {
//     //             if (data.hasOwnProperty('success') && data.success) {
//     //                 window.location.replace("/checkout");
//     //             }
//     //             else {
//     //                 var message = $('<div />', {
//     //                                     class: 'message message_error',
//     //                                     text: defaultErrMsg
//     //                                 }
//     //                 );
//     //                 msgContainer.append(message);
//     //
//     //                 setTimeout(function () {
//     //                     message.remove()
//     //                 }, 3000);
//     //
//     //                 self.drawTable();
//     //             }
//     //         }).fail(function (data) {
//     //             that.prop("disabled", false);
//     //
//     //             var message = $('<div />', {
//     //                                 class: 'message message_error',
//     //                                 text: defaultErrMsg
//     //                             }
//     //             );
//     //             self.drawTable();
//     //
//     //             msgContainer.append(message);
//     //
//     //             setTimeout(function () {
//     //                 message.remove()
//     //             }, 3000);
//     //         });
//     //
//     //         return false;
//     //     });
//     //
//     //     $('.cart_wrap').on('click', '.activate_promo', function () {
//     //         var that = $(this);
//     //         var promoCodeElement = $('.promo_code');
//     //
//     //         if (!that.hasClass('activated')) {
//     //             if (promoCodeElement.val() != '') {
//     //                 $.ajax({
//     //                            type: "POST",
//     //                            url: "/cart/add-discount",
//     //                            dataType: "json",
//     //                            data: {
//     //                                promo_code: promoCodeElement.val(),
//     //                                is_ajax: true
//     //                            },
//     //                            beforeSend: function () {
//     //                                that.prop("disabled", true);
//     //                                /** todo show loader */
//     //                            }
//     //                        }).done(function (data) {
//     //                     that.prop("disabled", false);
//     //                     if (data.hasOwnProperty('success') && data.success) {
//     //                         self.promo = data.discount;
//     //                         self.setDiscount(that, self.promo);
//     //                     }
//     //                     else {
//     //                         var message = $('<div />', {
//     //                                             class: 'message message_error',
//     //                                             text: data.message
//     //                                         }
//     //                         );
//     //                         msgContainer.append(message);
//     //
//     //                         setTimeout(function () {
//     //                             message.remove()
//     //                         }, 3000);
//     //                     }
//     //                 }).fail(function (data) {
//     //                     that.prop("disabled", false);
//     //
//     //                     var message = $('<div />', {
//     //                                         class: 'message message_error',
//     //                                         text: data.message
//     //                                     }
//     //                     );
//     //                     msgContainer.append(message);
//     //
//     //                     setTimeout(function () {
//     //                         message.remove()
//     //                     }, 3000);
//     //                 });
//     //             }
//     //         }
//     //         else {
//     //             that.removeClass('activated');
//     //             that.text('Активировать');
//     //             $('.promo_title').hide();
//     //             $.ajax({
//     //                        type: "POST",
//     //                        url: "/cart/remove-discount",
//     //                        dataType: "json",
//     //                        data: {
//     //                            promo_code: promoCodeElement.val(),
//     //                            is_ajax: true
//     //                        },
//     //                        beforeSend: function () {
//     //                            that.prop("disabled", true);
//     //                            /** todo show loader */
//     //                        }
//     //                    }).done(function (data) {
//     //                 that.prop("disabled", false);
//     //                 if (data.hasOwnProperty('success') && data.success) {
//     //                     $('.promo_title').show();
//     //                     promoCodeElement.show();
//     //                     $('.promocode_title').remove();
//     //                     self.promo = {};
//     //                     self.recalculateGrandTotal($('.grand_total'), self.items, self.delivery.price);
//     //                 }
//     //                 else {
//     //                     $('.promo_title').show();
//     //                     var message = $('<div />', {
//     //                                         class: 'message message_error',
//     //                                         text: data.message
//     //                                     }
//     //                     );
//     //                     msgContainer.append(message);
//     //
//     //                     setTimeout(function () {
//     //                         message.remove()
//     //                     }, 3000);
//     //                 }
//     //             }).fail(function (data) {
//     //                 that.prop("disabled", false);
//     //                 $('.promo_title').show();
//     //
//     //                 var message = $('<div />', {
//     //                                     class: 'message message_error',
//     //                                     text: data.message
//     //                                 }
//     //                 );
//     //                 msgContainer.append(message);
//     //
//     //                 setTimeout(function () {
//     //                     message.remove()
//     //                 }, 3000);
//     //             });
//     //         }
//     //
//     //         return false;
//     //     });
//     //
//     //     $('.cart_wrap').on('click', '.restore_removed', function () {
//     //         var that = $(this);
//     //         var productId = that.data('id');
//     //         var warehouseId = that.data('warehouse');
//     //
//     //         var items = self._removed;
//     //         var currentItem;
//     //
//     //         var newRemovedItems = [];
//     //
//     //         for (var key in items) {
//     //             if (items.hasOwnProperty(key)) {
//     //                 if (items[key].product.id == productId) {
//     //                     currentItem = items[key];
//     //                 }
//     //                 else {
//     //                     newRemovedItems.push(items[key]);
//     //                 }
//     //             }
//     //         }
//     //
//     //         $.ajax({
//     //                    type: "POST",
//     //                    url: "/cart/add",
//     //                    dataType: "json",
//     //                    data: {
//     //                        product_id: productId,
//     //                        warehouse_id: warehouseId,
//     //                        count: currentItem.count,
//     //                        is_ajax: true
//     //                    },
//     //                    beforeSend: function () {
//     //                        that.text('Добавляется...');
//     //                        that.prop("disabled", true);
//     //                    }
//     //                }).done(function (data) {
//     //             that.text('Отменить');
//     //             that.prop("disabled", false);
//     //
//     //             var message = '';
//     //             if (data.hasOwnProperty('success') && data.success) {
//     //                 /*message = $('<div />', {
//     //                         class: 'message message_success',
//     //                         text: 'Удаление отменено'
//     //                     }
//     //                 );
//     //                 */
//     //                 self._removed = newRemovedItems;
//     //                 self.items.push(currentItem);
//     //
//     //                 var restoreBtn = $('.restore_removed');
//     //
//     //                 restoreBtn.each(function (index, elem) {
//     //                     var element = $(elem);
//     //
//     //                     if (element.data('id') == productId) {
//     //                         element.parent().remove();
//     //                     }
//     //                 });
//     //
//     //                 self.drawTable();
//     //                 self.drawBlockForRemoved(self._removed);
//     //                 self.recalculateGrandTotal($('.grand_total'), self.items, self.delivery.price, self.promo.discount);
//     //                 self.recalculateGrandTotal($('.cart_intermediate_sum'), self.items);
//     //             }
//     //             else {
//     //                 message = $('<div />', {
//     //                                 class: 'message message_error',
//     //                                 text: 'Ошибка отмены удаления'
//     //                             }
//     //                 );
//     //             }
//     //
//     //             /*msgContainer.append(message);
//     //
//     //             setTimeout(function () {
//     //                 message.remove()
//     //             }, 3000);*/
//     //
//     //         }).fail(function (data) {
//     //             that.text('Отменить');
//     //             that.prop("disabled", false);
//     //
//     //             var message = $('<div />', {
//     //                                 class: 'message message_error',
//     //                                 text: 'Ошибка отмены удаления'
//     //                             }
//     //             );
//     //             msgContainer.append(message);
//     //
//     //             setTimeout(function () {
//     //                 message.remove()
//     //             }, 3000);
//     //         });
//     //
//     //         return false;
//     //     });
//     // }
//
//     // this.init = function () {
//     //     var loader = $('.loader');
//     //     $.ajax({
//     //                type: "POST",
//     //                url: "/cart/cart-data",
//     //                dataType: "json",
//     //                data: {
//     //                    is_ajax: true
//     //                },
//     //                beforeSend: function () {
//     //                    loader.show();
//     //                }
//     //            }).done(function (data) {
//     //         loader.hide();
//     //
//     //         if (data.hasOwnProperty('success') && data.success) {
//     //             self.items = data.products;
//     //             self.promo = data.discount;
//     //         }
//     //
//     //         self.addEventListeners();
//     //         self.drawTable(function () {
//     //             self.itemsCount = self.calculateItemsCount(self.items);
//     //         });
//     //         grandTotalElement = $('.grand_total');
//     //         cartIntermediateSum = $('.cart_intermediate_sum');
//     //     }).fail(function (data) {
//     //         loader.hide();
//     //
//     //         var message = $('<div />', {
//     //                             class: 'message message_error',
//     //                             text: 'Ошибка загрузки корзины'
//     //                         }
//     //         );
//     //         msgContainer.append(message);
//     //
//     //         setTimeout(function () {
//     //             message.remove()
//     //         }, 3000);
//     //     });
//     // }
//     //
//     // this.init();
// };

$.fn.isOnDialogViewport = function () {

    var dialogWrap = $('.dialog_wrap');

    var viewport = {
        top: dialogWrap.offset().top + 200
    };
    viewport.bottom = viewport.top + dialogWrap.outerHeight() - 100;

    var element = this.offset();
    element.bottom = element.top + this.outerHeight();

    return (!(viewport.bottom < element.top || viewport.top > element.bottom));
};

Array.prototype.unique = function () {
    var a = this.concat();
    for (var i = 0; i < a.length; ++i) {
        for (var j = i + 1; j < a.length; ++j) {
            if (a[i] === a[j])
                a.splice(j--, 1);
        }
    }

    return a;
};

$(document).ready(function () {
    var $fixed_top = $('.fix-top');
    if ($fixed_top.length > 0) {
        $fixed_top = $fixed_top.offset().top;
    }
    $(window).bind('scroll', function () {
        if ($(window).scrollTop() > $fixed_top) {
            $('.fix-top').addClass('fixed-top');
        }
        else {
            $('.fix-top').removeClass('fixed-top');
        }
    });

});
