(function (document, $, window) {

    window.Construct = function (data) {
        this.config = {};
        this.changed = false;
        this.sortActive = false;
        this.positionInsertBlock = false;
        console.log(this);
        this.init();
    }

    window.Construct.prototype.init = function () {
        this.setControls();
        this.initEvent();
    }

    window.Construct.prototype.initEvent = function () {
        var self = this;

        $(".menu-list:not(.block-add-close)").click(function () {
            self.renderTemplates($(this).data('action'));
            self.show(".offcanvas_content");
        });
        $('#hide_content').on('click', function () {
            self.hide(".offcanvas_content");
            $('.btns').remove();
        });

        $('.template_body').on('click', '.controls .block-add', function () {
            self.clearActions();
            self.positionInsertBlock = $(this).parent().parent().data('sort');
            self.show(".offcanvas");
        });

        $('.block-add-close').on('click', function () {
            self.hide(".offcanvas");
            $('.btns').remove();
        });


        $('.template_body').on('click', '.controls .sort', function () {
            self.clearActions();
            switch ($(this).data('sort')) {
                case 'top' :
                    self.moveTop(this);
                    break;
                case 'bottom' :
                    self.moveBottom(this);
                    break;
                default:
                    return false;
            }
        })

        $('.content_body').on('click', '.element', function () {
            $('.element').removeClass('active');
            $(this).addClass('active');
            var code = $(this).data('code');
            self.activeTemplate = self.config[code][$(this).data('index')];
            self.activeTemplate['template'] = code;
            self.addButtons(
                function () {
                    self.insertTemplate(self.getTemplate(code, self.activeTemplate.code));
                    self.clearActions();
                }, function () {
                    self.clearActions();
                });
        });

        $('.settings').on('click', function (e) {
            self.clearActions();
            e.preventDefault();
            self.show(".offcanvas_settings_content");

        });

        $('.close_edit_content').on('click', function (e) {
            e.preventDefault();
            self.clearActions();
            self.hide(".offcanvas_edit_content");
        });
        $('.close_settings_content').on('click', function (e) {
            e.preventDefault();
            self.clearActions();
            self.hide(".offcanvas_settings_content");
        });
        $('.close_design_content').on('click', function (e) {
            e.preventDefault();
            self.clearActions();
            self.hide(".offcanvas_design_content");
        });

        $('.template_body').on('click', '.controls .edit', function () {
            self.clearActions();
            self.show(".offcanvas_edit_content");
            var template = $(this).parent().parent().data('template');
            var code = $(this).parent().parent().data('code');
            var id = $(this).data('id');
            self.edit(self.getConfig(template, code), id);
        })

        $('.template_body').on('click', '.controls .design', function () {
            self.clearActions();
            self.show(".offcanvas_design_content");
            var template = $(this).parent().parent().data('template');
            var code = $(this).parent().parent().data('code');
            var id = $(this).data('id');
            self.design(self.getConfig(template, code), id);
        })

        $('.template_body').on('click', '.controls .delete', function () {
            $($(this).parent().parent()).remove();
            self.sort();
        });
    }

    window.Construct.prototype.design = function (config, id = '') {
        var self = this;
        var root = $('.design_content');
        root.html('');
        var text = config.text.split(',');
        var block = config.block.split(',');
        var background = config.background.split(',');
        if (text !== undefined) {
            $.each(text, function(index, value){
                value = '#'+id+' '+value;
                self.createColorPicker(root, 'color_text', 'Цвет текста', function (data) {
                    if (data !== ''){
                        var val = data.val();
                        var rgba = 'rgba('+val.r+','+val.g+','+val.b+','+((100/(255/val.a))/100)+')';
                        $(value).css('color', rgba);
                    }
                });
                self.createSlider(root, 'text_size', 'Размер шрифта', function (event, ui) {
                    $(value).css('font-size', ui.value + 'px');
                });
                self.createSlider(root, 'line-height', 'Высота линии', function (event, ui) {
                    $(value).css('line-height', ui.value + 'px');
                });

                self.createSlider(root, 'letter-spacing', 'Разрядка текста', function (event, ui) {
                    $(value).css('letter-spacing', ui.value + 'px');
                });

                self.createSelect(root, 'font', 'Шрифт', function (e) {
                    $(value).css('font-family', $(this).val())
                }, '', config.fonts)

                self.createSelect(root, 'align', 'Выравнивание', function (e) {
                    $(value).css('text-align', $(this).val())
                }, '', ['left', 'center', 'right'])
            })
        }


        if (block !== undefined) {
            $.each(block, function(index, value){
                value = '#'+id+' '+value;
                self.createSlider(root, 'margin-top', 'Отступ сверху', function (event, ui) {
                    $(value).css('margin-top', ui.value + 'px');
                });

                self.createSlider(root, 'margin-left', 'Отступ слева', function (event, ui) {
                    $(value).css('margin-left', ui.value + 'px');
                });

                self.createSlider(root, 'margin-right', 'Отступ справа', function (event, ui) {
                    $(value).css('margin-right', ui.value + 'px');
                });

                self.createSlider(root, 'margin-bottom', 'Отступ снизу', function (event, ui) {
                    $(value).css('margin-bottom', ui.value + 'px');
                });

                self.createSlider(root, 'padding-top', 'Поле сверху', function (event, ui) {
                    $(value).css('padding-top', ui.value + 'px');
                });

                self.createSlider(root, 'padding-left', 'Поле слева', function (event, ui) {
                    $(value).css('padding-left', ui.value + 'px');
                });

                self.createSlider(root, 'padding-right', 'Поле справа', function (event, ui) {
                    $(value).css('padding-right', ui.value + 'px');
                });

                self.createSlider(root, 'padding-bottom', 'Поле снизу', function (event, ui) {
                    $(value).css('padding-bottom', ui.value + 'px');
                });
            })
        }

        if (background !== undefined) {
            $.each(background, function(index, value){
                value = '#'+id+' '+value;
                self.createColorPicker(root, 'color_bg', 'Цвет фона', function (data) {
                    var val = data.val();
                    var rgba = 'rgba('+val.r+','+val.g+','+val.b+','+((100/(255/val.a))/100)+')';
                    $(value).css('background', rgba);
                });
            });
        }
    }

    window.Construct.prototype.margins = function(){

    }

    window.Construct.prototype.edit = function (settings, id = '') {
        var self = this;
        var root = $('.edit_content');
        root.html('');
        if (id !== '')
            var section = '#' + id;
        else
            var section = 'section[data-code="' + settings.code + '"]';
        $(section + ' img').each(function () {
            var element = this;
            self.createImage(root, Math.random().toString(36).substr(2, 9), 'Изображение', function (e, img) {
                var input = $(e).get(0);
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        $(element).attr('src', e.target.result);
                        $(img).attr('src', e.target.result).attr('width', '200px');
                    }
                    reader.readAsDataURL(input.files[0]);
                }
            }, $(this).attr('src'));
        });

        $(section + ' a').each(function () {
            var element = this;
            if ($('img', $(element)).length == 0)
                var name = $(element).text();
            else
                var name = false;
            self.createLink(root, '', 'link',
                function (_this, e) {
                    $(element).text(_this.val());
                }, function (_this, e) {
                    $(element).attr('href', _this.val());
                }, name, $(element).attr('href'));
        })

        $(section + ' p').each(function () {
            var element = this;
            self.createText(root, 'text', 'Текст',
                function (_this, e) {
                    $(element).html(_this.val());
                }, $(element).html());
        })
        $(section + ' h1, ' + section + ' h2, '+ section + ' h3, '+ section + ' h4, '+section + ' h5, '+ section + ' h6').each(function () {
            var element = this;
            self.createText(root, 'text', 'Текст',
                function (_this, e) {
                    $(element).html(_this.val());
                }, $(element).html());
        })
        if (settings.blocks != undefined && settings.blocks.length > 0) {
            $.each(settings.blocks, function (index, value) {
                // console.log(index, value);
                // if($(value).length > 0){
                //     $.each($(value), function(i , v){
                //         var id = Math.random().toString(36).substr(3,9);
                //         $(this).attr('id', id);
                //         Sortable.create(id, {
                //             group: {
                //                 name: 'shared',
                //             }
                //         });
                //     });
                // }else{
                //     var id = Math.random().toString(36).substr(3,9);
                // $(value).attr('id', id);
                if(self.sortActive !== false)
                    self.sortActive.destroy();
                try {
                    self.sortActive = Sortable.create($(value)[0], {
                        group: {
                            name: 'shared',
                        }
                    });
                    // Sortable.create(v, {
                    //     group: {
                    //         name: 'shared',
                    //     }
                    // });
                    // $($(value)[0]).sortable('destroy');
                } catch (e) {

                }
                // }
            });
        }


    }

    window.Construct.prototype.addButtons = function (callbackSave, callbackClose) {
        $('.btns').remove();
        var div = $('<div>').addClass('btns');
        var buttonSave = $('<button>').attr('code', 'button').addClass('btn save btn-success buttonAction').text('Сохранить');
        var buttonClose = $('<button>').attr('code', 'button').addClass('btn clear btn-danger buttonAction').text("Отменить");
        div.append(buttonSave);
        div.append(buttonClose);
        $('.content_header').append(div);
        $(buttonSave).on('click', callbackSave);
        $(buttonClose).on('click', callbackClose);
    }

    window.Construct.prototype.renderTemplates = function (code) {
        var configs = this.getConfigs(code);
        if (configs !== undefined) {
            var content = $('<div>').addClass('row');
            $('.content_body').html('');
            $.each(configs, function (index, config) {
                var div = $('<div>').addClass('col-md-5 element').attr('data-index', index).attr('data-code', code);
                if (config.preview_image != undefined) {
                    div.append($('<img>').attr('src', config.preview_image).addClass('responsive'));
                }
                if (config.name != undefined) {
                    div.append($('<p>').html(config.name));
                }
                content.append(div);
            });
            $('.content_body').append(content);
        }
    }


    window.Construct.prototype.setControls = function () {
        var self = this;
        $('.block').each(function () {
            $(this).append(self.controls());
        });
    }

    window.Construct.prototype.controls = function (id = '') {
        var div = $('<div>').addClass('controls');
        div.append($('<div>').attr('data-id', id).addClass('control edit').text('Редактировать'));
        div.append($('<div>').attr('data-id', id).addClass('control design').text('Дизайн'));
        div.append($('<div>').attr('data-id', id).addClass('control block-add').text('Добавить блок'));
        div.append($('<div>').attr('data-id', id).addClass('control sort').attr('data-sort', 'top').text('Выше'));
        div.append($('<div>').attr('data-id', id).addClass('control sort').attr('data-sort', 'bottom').text('Ниже'));
        div.append($('<div>').attr('data-id', id).addClass('control delete').text('Удалить'));
        return div;
    }

    window.Construct.prototype.moveTop = function (_this) {
        var block = $(_this).parent().parent();
        var sort = parseInt(block.attr('data-sort'));
        $('section[data-sort="' + (sort - 1) + '"]').insertAfter(block);
        this.sort();
    }

    window.Construct.prototype.moveBottom = function (_this) {
        var block = $(_this).parent().parent();
        var sort = parseInt(block.attr('data-sort'));
        $('section[data-sort="' + (sort + 1) + '"]').insertBefore(block);
        this.sort();
    }

    window.Construct.prototype.sort = function () {
        $('.block').each(function (index, value) {
            $(value).attr('data-sort', index + 1);
        });
    }

    window.Construct.prototype.insertTemplate = function (html) {
        var self = this;
        if (self.positionInsertBlock !== false) {
            var id = Math.random().toString(36).substr(3, 9);
            var section = $('<section>')
                .attr('id', id)
                .attr('data-sort', 0)
                .addClass('block')
                .attr('data-code', self.activeTemplate.code)
                .attr('data-template', self.activeTemplate.template);
            section.append($('<div>').append(html));
            section.append(self.controls(id));
            var findBlock = $('section[data-sort=' + (self.positionInsertBlock) + ']');
            if (findBlock.length == 0) {
                findBlock = $('.construct .template_body');
                findBlock.append(section);
            }
            else {
                section.insertAfter(findBlock);
            }
            self.sort();
        }
    }

    window.Construct.prototype.createImage = function (root, name, label, callback = false, defaultValue = false) {
        var div = $('<div>').addClass('form-group');
        var label = $('<label>').attr('for', name);
        var input = $('<input>').attr('type', 'file').attr('name', name).attr('id', name).addClass('form-control');
        div.append(label);
        if (defaultValue !== false) {
            var img = $('<img>').attr('src', defaultValue).attr('width', '200px');
            div.append(img);
        }
        div.append(input);
        $(root).append(div);
        if (callback !== false) {
            input.on('change', function () {
                callback($(this), img);
            });
        }
    }

    window.Construct.prototype.createLink = function (root, name, label, callbackName = false, callbackLink = false, defaultValueName = '', defaultValueLink = '') {
        var div = $('<div>').addClass('input-group');
        var label = $('<label>').text(label);
        div.append(label);
        if (defaultValueName !== false)
            var inputName = $('<input>').attr('name', Math.random().toString(36).substr(3, 9)).val(defaultValueName).addClass('form-control');
        var inputLink = $('<input>').attr('name', Math.random().toString(36).substr(3, 9)).val(defaultValueLink).addClass('form-control');
        div.append(inputName).append(inputLink);
        $(root).append(div);
        if (callbackLink !== false) {
            inputLink.on('input', function (e) {
                callbackLink($(this), e);
            })
        }
        if (callbackName !== false && defaultValueName !== false) {
            inputName.on('input', function (e) {
                callbackName($(this), e);
            })
        }
    }

    window.Construct.prototype.createCheckbox = function (root, name, label, callback = false, defaultValue = false) {
        var div = $('<div>').addClass('custom-control custom-checkbox');
        var label = $('<label>').attr('for', name).addClass('custom-control-label').text(label);
        var input = $('<input>')
            .attr('type', 'checkbox')
            .attr('name', name)
            .attr('id', name)
            .addClass('custom-control-input');
        if (defaultValue) input.prop('checked', true);
        if (callback !== false) {
            $(input).on('click', callback);
        }
        $(root).append(div.append(input).append(label));
    }

    window.Construct.prototype.createInput = function (root, name, label, callback = false, defaultValue = '') {
        var div = $('<div>').addClass('form-group');
        var label = $('<label>').attr('for', name);
        div.append(label);
        var input = $('<input>').attr('name', name).attr('id', name).addClass('form-control').attr('value', defaultValue);
        if (callback !== false) {
            $(input).on('input', callback);
        }
        $(root).append(div.append(input));
    }

    window.Construct.prototype.createText = function (root, name, label, callback = false, defaultValue = '') {
        var div = $('<div>').addClass('form-group');
        var label = $('<label>').attr('for', name);
        div.append(label);
        var textarea = $('<textarea>')
            .attr('name', name)
            .attr('id', name)
            .addClass('form-control')
            .text(defaultValue);
        if (callback !== false) {
            $(textarea).on('input', function () {
                callback($(this), textarea)
            });
        }
        $(root).append(div.append(textarea));
    }

    window.Construct.prototype.createSelect = function (root, name, label, callback = false, defaultValue = '', data = []) {
        if (data.length == 0) return false;
        var div = $('<div>').addClass('form-group');
        var label = $('<label>').text(label);
        div.append(label);
        var select = $('<select>').attr('name', name).addClass('form-control');
        $.each(data, function (index, font) {
            var option = $('<option>').attr('value', font).text(font);
            if (defaultValue == font) option.prop('selected', true);
            select.append(option);
        });
        $(root).append($(div).append(select));
        if (callback !== false)
            $(select).on('change', callback);
    }

    window.Construct.prototype.createSlider = function (root, name, label, callback = false, defaultValue = '') {
        var container = $('<div>').addClass(name);
        var slider = $('<div>').addClass('slider');
        root.append($('<label>').text(label));
        var uniq = Math.random().toString(36).substr(2, 9);
        var input = $('<input>').addClass('form-control').attr('id', 'input' + uniq);
        root.append($('<div>').addClass('info').attr('id', uniq).append(input));
        $(slider).slider({
            value: 12,
            min: 0,
            max: 40,
            step: 0.1,
            create: function (event, ui) {
                val = $(slider).slider("value");
                if (defaultValue != '') val = defaultValue;
                $('#input' + uniq).val(val);
            },
            slide: function (event, ui) {
                $('#input' + uniq).val(ui.value);
                if (callback != false) {
                    callback(event, ui);
                }
            }
        });
        $(root).append(slider);
        $('#input' + uniq).on('input', function (e) {
            callback(e, {value: $(this).val()});
        });
    }

    window.Construct.prototype.createColorPicker = function (root, name, label, callback = false, defaultValue = '') {
        var rootDiv = $('<div>').addClass('form-group');
        rootDiv.append($('<label>').attr('for', name).text(label));
        var div = $('<div>').attr('name', name).attr('id', name);
        rootDiv.append(div);
        $(root).append(rootDiv);
        $(div).jPicker(
            {
                window:
                {
                    expandable: true,
                    position:
                        {
                            x: 'screenCenter', // acceptable values "left", "center", "right", "screenCenter", or relative px value
                            y: 'center', // acceptable values "top", "bottom", "center", or relative px value
                        },
                    alphaSupport: true,
                }
            },function(color){
                if(callback !== false){
                    callback(color);
                }
            },
            function(color){
                if(callback !== false){
                    callback(color);
                }
            });
    }

    window.Construct.prototype.createList = function () {

    }

    window.Construct.prototype.createSe = function () {

    }

    window.Construct.prototype.getTemplate = function (template, code) {
        var self = this;
        self.html = null;
        $.ajax({
            url: '/user/seller/site/templates/' + template + '/' + code,
            type: 'POST',
            dataType: 'html',
            async: false,
            success: function (data) {
                self.html = data;
            },
            error: function (data) {
                $('.content_body').text('Шаблоны не найдены');
            }
        });
        return self.html;
    }

    window.Construct.prototype.getConfigs = function (config) {

        var self = this;
        if (self.config[config] == undefined) {
            $.ajax({
                url: '/user/seller/site/configs/' + config,
                type: 'POST',
                dataType: 'json',
                async: false,
                success: function (data) {
                    self.config[config] = data;
                },
                error: function (data) {
                    $('.content_body').text('Шаблоны не найдены');
                }
            });
        }
        return self.config[config]
    }

    window.Construct.prototype.getConfig = function (type, code) {
        var config = this.getConfigs(type);
        var returnConfig = [];
        if (config !== undefined) {
            returnConfig = config.filter(function (n) {
                return n.code === code;
            });
        }
        if (returnConfig.length > 0) {
            return returnConfig[0];
        }
        return false;
    }


    window.Construct.prototype.show = function (element) {
        $(element).show("drop", {direction: "right"}, 300);
    }

    window.Construct.prototype.hide = function (element) {
        $(element).hide("drop", {direction: "right"}, 300);
    }

    window.Construct.prototype.clearActions = function () {
        var self = this;
        self.activeTemplate = {};
        self.positionInsertBlock = false;
        $('.element').removeClass('active');
        $('.btns').remove();
        self.hide(".offcanvas_content");
        self.hide(".offcanvas");
        self.hide(".offcanvas_design_content");
        self.hide(".offcanvas_edit_content");
        self.hide(".offcanvas_settings_content");
    }


})(document, $, window)