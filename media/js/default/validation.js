(function (window, $, document) {

  window.validation = new function (settings) {
    this.messages = [];
    this.attrname = null;
    this.regexNonASCII = /[^\0-\x7E]/;
    this.regexSeparators = /[\x2E\u3002\uFF0E\uFF61]/g;
    this.isEmpty = function (value) {
      return value === null || value === undefined || value == [] || value === '' || value.length === 0;
    }

    this.init = function (value, options, attrname) {
      this.attrname = attrname;
      var res = this[options.validator](value, options);
      if (res != undefined)
        this.addMessage(res, value);
    }

    this.addMessage = function (message, value) {
      this.messages.push(message.replace(/\{value\}/g, value));
    }

    this.boolean = function (value, options) {
      if (options.whenClient) {
        eval(options.whenClient);
        var whenResult = when(value, options);
        if (!whenResult) {
          return;
        }
      }
      if (options.skipOnEmpty && this.isEmpty(value)) {
        return;
      }
      var valid = !options.strict && (value == options.trueValue || value == options.falseValue)
        || options.strict && (value === options.trueValue || value === options.falseValue);

      if (!valid) {
        this.addMessage(options.message, value);
      }
    }

    this.string = function (value, options) {
      if (options.whenClient) {
        eval(options.whenClient);
        var whenResult = when(value, options);
        if (!whenResult) {
          return;
        }
      }
      if (options.skipOnEmpty && this.isEmpty(value)) {
        return;
      }
      if (typeof value !== 'string') {

        this.addMessage(options.message, value);
        // return;
      }
      if (options.min !== undefined && value.length < options.min) {

        this.addMessage(options.tooShort, value);
      }
      if (options.max !== undefined && value.length > options.max) {

        this.addMessage(options.tooLong, value);
      }
      if (options.is !== undefined && value.length != options.is) {

        this.addMessage(options.notEqual, value);
      }
    }

    this.file = function (value, options) {
      var self = this;
      var attribute = '';
      var files = self.getUploadedFiles(attribute, options);
      $.each(files, function (i, file) {
        self.validateFile(file, options);
      });
    }

    this.image = function (value, options) {
      if (options.whenClient) {
        eval(options.whenClient);
        var whenResult = when(value, options);
        if (!whenResult) {
          return;
        }
      }
      var self = this;
      var attribute = '';
      var files = self.getUploadedFiles(attribute, options);
      $.each(files, function (i, file) {
        self.validateFile(file, options);

        if (typeof FileReader === "undefined") {
          return;
        }

        var def = $.Deferred(),
          fr = new FileReader(),
          img = new Image();
        img.onload = function () {
          if (options.minWidth && this.width < options.minWidth) {
            return options.underWidth.replace(/\{file\}/g, file.name);
            self.messages.push(options.underWidth.replace(/\{file\}/g, file.name));
          }

          if (options.maxWidth && this.width > options.maxWidth) {
            self.messages.push(options.overWidth.replace(/\{file\}/g, file.name));
          }

          if (options.minHeight && this.height < options.minHeight) {
            self.messages.push(options.underHeight.replace(/\{file\}/g, file.name));
          }

          if (options.maxHeight && this.height > options.maxHeight) {
            self.messages.push(options.overHeight.replace(/\{file\}/g, file.name));
          }
          def.resolve();
        };

        img.onerror = function () {
          self.messages.push(options.notImage.replace(/\{file\}/g, file.name));
          def.resolve();
        };

        fr.onload = function () {
          img.src = fr.result;
        };

        fr.onerror = function () {
          def.resolve();
        };

        fr.readAsDataURL(file);

        // deferred.push(def);
      });
    }

    this.each = function (attribute, options) {
      if (options.whenClient) {
        eval(options.whenClient);
        var whenResult = when(attribute, options);
        if (!whenResult) {
          return;
        }
      }
      var self = this;
      if (options.max && attribute.length > options.max) {
        this.messages.push(options.tooBig);
        return;
      }
      if (options.min && attribute.length < options.min) {
        this.messages.push(options.tooSmall);
        return;
      }
      $.each(attribute, function (index, val) {
        self[options.rule[0]](val, options.rule);
      });
    }

    this.getUploadedFiles = function (attribute, options) {
      var self = this;
      if (typeof File === "undefined") {
        return [];
      }

      try {
        var files = $('*[name="' + self.attrname + '"]').get(0).files;
      } catch (e) {
        try {
          files = $('*[name="' + self.attrname + '[]"]').get(0).files;
        } catch (ee) {
          return;
        }
      }

      if (files.length === 0) {
        if (!options.skipOnEmpty) {
          this.messages.push(options.uploadRequired);
        }
        return [];
      }

      if (options.maxFiles && options.maxFiles < files.length) {
        this.messages.push(options.tooMany);
        return [];
      }

      return files;
    }

    this.validateFile = function (file, options) {
      if (options.whenClient) {
        eval(options.whenClient);
        var whenResult = when(value, options);
        if (!whenResult) {
          return;
        }
      }
      if (options.extensions && options.extensions.length > 0) {
        var index, ext;

        index = file.name.lastIndexOf('.');

        if (!~index) {
          ext = '';
        } else {
          ext = file.name.substr(index + 1, file.name.length).toLowerCase();
        }

        if (!~options.extensions.indexOf(ext)) {
          this.messages.push(options.wrongExtension.replace(/\{file\}/g, file.name));
        }
      }

      if (options.mimeTypes && options.mimeTypes.length > 0) {
        if (!~options.mimeTypes.indexOf(file.type)) {
          this.messages.push(options.wrongMimeType.replace(/\{file\}/g, file.name));
        }
      }

      if (options.maxSize && options.maxSize < file.size) {
        this.messages.push(options.tooBig.replace(/\{file\}/g, file.name));
      }

      if (options.minSize && options.minSize > file.size) {
        this.messages.push(options.tooSmall.replace(/\{file\}/g, file.name));
      }
    }

    this.number = function (value, options) {
      if (options.skipOnEmpty && this.isEmpty(value)) {
        return;
      }
      if (options.whenClient) {
        eval(options.whenClient);
        var whenResult = when(value, options);
        if (!whenResult) {
          return;
        }
      }
      if (isNaN(value) || typeof value === 'string' && !value.match(options.numberPattern)) {
        this.addMessage(options.message, value);
        return;
      }
      value = parseFloat(value);
      if (options.min !== undefined && value < options.min) {
        this.addMessage(options.tooSmall, value);
      }
      if (options.max !== undefined && value > options.max) {
        this.addMessage(options.tooBig, value);
      }
      if (options.smallOnAttribute !== undefined && value > parseFloat($('*[name="' + options.smallOnAttribute + '"]').val())) {
        this.addMessage(options.tooSmall, value);
      }
      if (options.bigOnAttribute !== undefined && value < parseFloat($('*[name="' + options.bigOnAttribute + '"]').val())) {
        this.addMessage(options.tooBig, value);
      }
    }

    this.range = function (value, options) {
      if (options.skipOnEmpty && this.isEmpty(value)) {
        return;
      }

      if (!options.allowArray && $.isArray(value)) {
        this.addMessage(options.message, value);
        return;
      }

      var inArray = true;

      $.each($.isArray(value)
        ? value
        : [value], function (i, v) {
        if ($.inArray(v, options.range) == -1) {
          inArray = false;
          return false;
        } else {
          return true;
        }
      });

      if (options.not === inArray) {
        this.addMessage(options.message, value);
      }
    }

    this.regularExpression = function (value, options) {
      if (options.skipOnEmpty && this.isEmpty(value)) {
        return;
      }
      if (!options.not && !value.match(new RegExp(options.pattern.toString().replace(/\//g, ""))) || options.not && value.match(new RegExp(options.pattern.toString().replace(/\//g, "")))) {
        this.addMessage(options.message, value);
        return options.message;
      }
    }

    this.mapDomain = function (string, fn) {
      var parts = string.split('@');
      var result = '';
      if (parts.length > 1) {

        result = parts[0] + '@';
        string = parts[1];
      }

      string = string.replace(this.regexSeparators, '\x2E');
      var labels = string.split('.');
      var encoded = this.map(labels, fn).join('.');
      return result + encoded;
    }

    this.map = function (array, fn) {
      var result = [];
      var length = array.length;
      while (length--) {
        result[length] = fn(array[length]);
      }
      return result;
    }

    this.toASCII = function (input) {
      var self = this;
      return self.mapDomain(input, function (string) {
        return self.regexNonASCII.test(string)
          ? 'xn--' + self.encode(string)
          : string;
      });
    }

    this.ucs2decode = function (string) {
      var output = [];
      var counter = 0;
      var length = string.length;
      while (counter < length) {
        var value = string.charCodeAt(counter++);
        if (value >= 0xD800 && value <= 0xDBFF && counter < length) {
          var extra = string.charCodeAt(counter++);
          if ((extra & 0xFC00) == 0xDC00) {
            output.push(((value & 0x3FF) << 10) + (extra & 0x3FF) + 0x10000);
          } else {
            output.push(value);
            counter--;
          }
        } else {
          output.push(value);
        }
      }
      return output;
    }

    this.encode = function (input) {
      var output = [];

      input = this.ucs2decode(input);

      var inputLength = input.length;

      var n = initialN;
      var delta = 0;
      var bias = initialBias;

      for (var currentValue of input) {
        if (currentValue < 0x80) {
          output.push(String.fromCharCode(currentValue));
        }
      }

      var basicLength = output.length;
      var handledCPCount = basicLength;

      if (basicLength) {
        output.push(delimiter);
      }

      while (handledCPCount < inputLength) {

        var m = maxInt;
        for (var currentValue of input) {
          if (currentValue >= n && currentValue < m) {
            m = currentValue;
          }
        }

        var handledCPCountPlusOne = handledCPCount + 1;
        if (m - n > floor((maxInt - delta) / handledCPCountPlusOne)) {
          error('overflow');
        }

        delta += (m - n) * handledCPCountPlusOne;
        n = m;

        for (var currentValue of input) {
          if (currentValue < n && ++delta > maxInt) {
            error('overflow');
          }
          if (currentValue == n) {
            var q = delta;
            for (var k = base; /* no condition */; k += base) {
              var t = k <= bias
                ? tMin
                : (k >= bias + tMax
                  ? tMax
                  : k - bias);
              if (q < t) {
                break;
              }
              var qMinusT = q - t;
              var baseMinusT = base - t;
              output.push(
                String.fromCharCode(digitToBasic(t + qMinusT % baseMinusT, 0))
              );
              q = Math.floor(qMinusT / baseMinusT);
            }

            output.push(String.fromCharCode(digitToBasic(q, 0)));
            bias = adapt(delta, handledCPCountPlusOne, handledCPCount == basicLength);
            delta = 0;
            ++handledCPCount;
          }
        }

        ++delta;
        ++n;

      }
      return output.join('');
    };

    this.email = function (value, options) {
      if (options.whenClient) {
        eval(options.whenClient);
        var whenResult = when(value, options);
        if (!whenResult) {
          return;
        }
      }
      if (options.skipOnEmpty && this.isEmpty(value)) {
        return;
      }
      var regexp = /^(.*<?)(.*)@(.*)(>?)$/;
      var valid = true;

//             if (options.enableIDN) {
      var matches = regexp.exec(value);
      if (matches === null) {
        valid = false;
      } else {
        value = matches[1] + this.toASCII(matches[2]) + '@' + this.toASCII(matches[3]) + matches[4];
      }

      if (!valid || !(value.match(new RegExp(options.pattern.toString().replace(/\//g, ""))) || (options.allowName && value.match(options.fullPattern.toString().replace(/\//g, ""))))) {
        return options.message;
      }
    }

    this.captcha = function (value, options) {
      if (options.skipOnEmpty && this.isEmpty(value)) {
        return;
      }

      var hash = $('body').data(options.hashKey);
      if (hash == null) {
        hash = options.hash;
      } else {
        hash = hash[options.caseSensitive
          ? 0
          : 1];
      }
      var v = options.caseSensitive
        ? value
        : value.toLowerCase();
      for (var i = v.length - 1, h = 0; i >= 0; --i) {
        h += v.charCodeAt(i);
      }
      if (h != hash) {
        this.addMessage(options.message, value);
      }
    }

    this.trim = function (value, options) {
      var $input = $form.find(attribute.input);
      var value = $input.val();
      if (!options.skipOnEmpty || !this.isEmpty(value)) {
        value = $.trim(value);
        $input.val(value);
      }
      return value;
    }

    this.compare = function (value, options) {
      if (options.whenClient) {
        var func = 'var when = ' + options.whenClient;
        eval(func);
        var whenResult = when(value, options);
        if (!whenResult) {
          return;
        }
      }
      var self = this;
      if (options.skipOnEmpty && this.isEmpty(value)) {
        return;
      }

      var compareValue, valid = true;
      if (options.compareAttribute === undefined) {
        compareValue = options.compareValue;
      } else {
        compareValue = $('*[name="' + options.compareAttribute + '"]').val();
      }
      if (options.type === 'number') {
        value = parseFloat(value);
        compareValue = parseFloat(compareValue);
      }

      switch (options.operator) {
        case '==':
          valid = value == compareValue;
          break;
        case '===':
          valid = value === compareValue;
          break;
        case '!=':
          valid = value != compareValue;
          break;
        case '!==':
          valid = value !== compareValue;
          break;
        case '>':
          valid = value > compareValue;
          break;
        case '>=':
          valid = value >= compareValue;
          break;
        case '<':
          valid = value < compareValue;
          break;
        case '<=':
          valid = value <= compareValue;
          break;
        default:
          valid = false;
          break;
      }

      if (!valid) {
        this.addMessage(options.message, value);
      } else {
        $('em.invalid', $('*[name="' + options.compareAttribute + '"]').removeClass('invalid').parent()).remove();
        $('em.invalid', $('*[name="' + options.attribute + '"]').removeClass('invalid').parent()).remove();
      }
    }

    this.required = function (value, options) {
      if (options.whenClient) {
        eval(options.whenClient);
        var whenResult = when(value, options);
        if (!whenResult) {
          return;
        }
      }
      var valid = false;
      if (options.requiredValue === undefined) {
        var isString = typeof value == 'string' || value instanceof String;
        if (options.strict && value !== undefined || !options.strict && !this.isEmpty(isString
            ? $.trim(value)
            : value)) {
          valid = true;
        }
      } else if (!options.strict && value == options.requiredValue || options.strict && value === options.requiredValue) {
        valid = true;
      }

      if (!valid) {
        this.messages.push(options.message);
        return options.message;
      }
    }

    this.url = function (value, options) {
      if (options.whenClient) {
        eval(options.whenClient);
        var whenResult = when(value, options);
        if (!whenResult) {
          return;
        }
      }
      if (options.skipOnEmpty && this.isEmpty(value)) {
        return;
      }

      if (options.defaultScheme && !value.match(/:\/\//)) {
        value = options.defaultScheme + '://' + value;
      }
      var valid = true;

      if (options.enableIDN) {
        var regexp = /^([^:]+):\/\/([^\/]+)(.*)$/,
          matches = regexp.exec(value);
        if (matches === null) {
          valid = false;
        } else {
          value = matches[1] + '://' + punycode.toASCII(matches[2]) + matches[3];
        }
      }
      var pattern = /^(http|https):\/\/(([A-Z0-9][A-Z0-9_-]*)(\.[A-Z0-9][A-Z0-9_-]*)+)/i;
      if (!valid || !value.match(pattern)) {
        this.messages.push(options.message);
        return options.message;
      }
    }

  }


})(window, jQuery, document)