(function(window, $, document){

    window.Data = new function(){
        this.data = false;
    }
    window.Data.prototype.getByUrl = function(url, data = {}, async = false){
        var self = this;
        $.ajax({
                   url: url,
                   type: 'POST',
                   dataType: 'json',
                   async: async,
                   data : data,
                   success: function (data) {
                       self.data = data;
                   },
                   error: function (data) {
                       self.data = false
                   }
               });
        return self.data;
    }

})(window, $, document)