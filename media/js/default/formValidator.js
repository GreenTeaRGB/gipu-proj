(function ($) {
    $.fn.formValidator = function(data){
        console.log(data);
       for (var field in data){
           $('*[name="'+field+'"]').on('blur change', function(){
               valid(data, this);
           });
           $('*[name="'+field+'[]"]').on('blur change', function(){
               valid(data, this);
           });
       }
    }

    function valid(data, self){
        try {
            var value = $(self).val();
        }catch (e){
            return;
        }
        var name = $(self).attr('name');
        console.log(data[name], name);
        data[name].forEach(function(v, i){
            validation.init(value, v, name);
            if(validation.messages.length > 0){
                $(self).addClass('invalid');
                $('em.invalid', $(self).parent()).remove();
                $(self).parent().append('<em class="invalid">'+validation.messages[0]+'</em>');
            }else{
                $(self).removeClass('invalid');
                $('em.invalid', $(self).parent()).remove();
                $('em.invalid', $('*[name="'+$(self).attr('name')+'"]').parent()).remove();
            }

        });
        validation.messages = [];
    }
})($)