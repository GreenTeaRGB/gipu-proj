(function(document, $, window){

    window.News = function(data){
        this.news = data.news
        this.categories = data.categories;
        this.createEvents();
    }

    window.News.prototype.createEvents = function(){
        var self = this;
        $('.news-index').on('click', function(e){
            e.preventDefault();
            $('.news-index').removeClass('red-stroke_current');
            $(this).addClass('red-stroke_current');
            var category_id = $(this).data('id');
            if(self.news[category_id] !== undefined){
                self.setLink(self.categories[category_id].alias);
                self.renderIndexPage(self.news[category_id]);
            }else{
                self.getData(category_id);
                if(self.news[category_id] !== undefined){
                    self.setLink(self.categories[category_id].alias);
                    self.renderIndexPage(self.news[category_id]);
                }
            }
            return false;
        });
    }

    window.News.prototype.setLink = function(alias){
        $('#full_news').attr('href', '/news/'+alias);
    }

    window.News.prototype.renderIndexPage = function(data){
        var content = $('#content-news').html('');
        $.each(data, function(index, value){
            var div = $('<div>').addClass('col-md-6 col-sm-12 news__item news__item_date');
            var img = $('<img>').attr('src', value.image).addClass('lazy');
            var i = $('<i>').addClass('date-time').text(value.created_at);
            var a = $('<a>').attr('href', '/news/show/'+value.alias);
            var span = $('<span>').addClass('sub-name').html(value.name);
            var p = $('<p>').addClass('desc-text').html(value.short_description);
            a.append(span).append(p);
            div.append(img).append(i).append(a);
            content.append(div);
        })
    }

    window.News.prototype.getData = function(category_id){
        var self = this;
        $.ajax({
            url: '/news/getByCategory/'+category_id,
            type: 'POST',
            async: false,
            dataType: 'json',
            success: function (data) {
                self.news[category_id] = data;
            }
        });
    }


})(document, $, window)