(function (window, $, document) {

    window.Helper = new function () {

        this.success = function (message) {
            this.message('success', message);
        }

        this.warning = function (message) {
            this.message('warning', message);
        }

        this.error = function (message) {
           this.message('error', message);
        }

        this.message = function(type, message){
            var span = $('<span>')
                .attr('aria-hidden', 'true')
                .html("&times;");
            var buttom = $('<button>')
                .attr('type', 'button')
                .attr('data-dismiss', 'alert')
                .attr('aria-label', 'Close')
                .addClass('close')
                .append(span);
            var alert = $('<div>')
                .addClass('alert alert-'+type+' top-alert alert-dismissible fade show')
                .text(message)
                .append(buttom);
            $('.messages').append(alert);
            setTimeout(function(){
                alert.toggle('dismiss')
            },10000);
        }

        this.loader = function () {
            if ($('.bg_loader').css('display') == 'block') {
                $('.bg_loader').hide(100);
                $('.loader').hide(100);
            }
            else {
                $('.bg_loader').show(100);
                $('.loader').show(100);
            }
        }
    }

})(window, $, document)