(function ($) {
    $.fn.formValidatorAdmin = function(data){
        console.log(data);
        for (var field in data){
            $('*[name="'+field+'"]').on('blur change', function(){
                valid(data, this);
            });
            $('*[name="'+field+'[]"]').on('blur change', function(){
                valid(data, this);
            });
        }
    }

    function valid(data, self){
        try {
            var value = $(self).val();
        }catch (e){
            return;
        }
        var name = $(self).attr('name');
        console.log(data[name], name);
        data[name].forEach(function(v, i){
            validation.init(value, v, name);
            if(validation.messages.length > 0){
                $(self).parent().addClass('has-error has-feedback').removeClass('has-success');
                $('p.help-block', $(self).parent()).remove();
                $(self).after('<p class="help-block">'+validation.messages[0]+'</p>');
            }else{
                $('p.help-block', $(self).parent()).remove();
                $(self).parent().removeClass('has-error has-feedback').addClass('has-success');
                $('p.help-block', $('*[name="'+$(self).attr('name')+'"]').parent()).remove();
            }

        });
        validation.messages = [];
    }
})($)