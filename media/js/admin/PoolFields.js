(function($,window, document){

  window.PoolFields = new function(){
    this._pool = {};
    this.add = function(key, data){
      if(this._pool[key] == undefined) this._pool[key] = [];
      this._pool[key].push(data);
    }
    this.get = function(key){
      return this._pool[key] == undefined ? [] : this._pool[key];
    }
    this.remove = function(key){
      delete this._pool[key];
    }
  }

})($,window, document)