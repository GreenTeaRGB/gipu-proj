function translite(str){
	var arr={'а':'a', 'б':'b', 'в':'v', 'г':'g', 'д':'d', 'е':'e', 'ж':'g', 'з':'z', 'и':'i', 'й':'y', 'к':'k', 'л':'l', 'м':'m', 'н':'n', 'о':'o', 'п':'p', 'р':'r', 'с':'s', 'т':'t', 'у':'u', 'ф':'f', 'ы':'i', 'э':'e', 'А':'a', 'Б':'b', 'В':'v', 'Г':'g', 'Д':'d', 'Е':'e','Ж':'g', 'З':'z', 'И':'i', 'Й':'y', 'К':'k', 'Л':'l', 'М':'m', 'Н':'n', 'О':'o', 'П':'p', 'Р':'r', 'С':'s', 'Т':'t', 'У':'u', 'Ф':'f', 'Ы':'i', 'Э':'e', 'ё':'yo', 'х':'h', 'ц':'ts', 'ч':'ch', 'ш':'sh', 'щ':'shch', 'ъ':'', 'ь':'', 'ю':'yu', 'я':'ya', 'Ё':'yo', 'Х':'h', 'Ц':'ts', 'Ч':'ch', 'Ш':'sh', 'Щ':'shch', 'Ъ':'', 'Ь':'',
	'Ю':'yu', 'Я':'ya',' ':'-'};
	var replacer=function(a){return arr[a]||a};

	return str.replace(/[А-яьЬъЪёЁ\s]/g,replacer)
}

$(document).ready(function() {
	$('.input-fill').blur(function(){
		var val = $(this).val();
		var link = translite(val.toLowerCase());
		link = link.replace(/[^a-z0-9-]/g,'');
		if ($('.input-uri').val()=='') $('.input-uri').val(link);
		if ($('input[name=h1]').val()=='') $('input[name=h1]').val(val);
		if ($('input[name=title]').val()=='') $('input[name=title]').val(val);
		if ($('input[name=breadcrumb]').val()=='') $('input[name=breadcrumb]').val(val);
		
	});

	$('.input-uri').keydown(function(event) {
 		
 		var codes = [8, 9, 13, 16, 17, 18, 27, 33, 34, 35, 36, 37,38,39,40,45,46,189,109];
 		var letters = ['а','б','в','г','д','е','ё','ж','з','и','й','к','л','м','н','о','п','р','с','т','у','ф','х','ц','ч','ш','щ','ъ','ы','ь','э','ю','я','А','Б','В','Г','Д','Е','Ё','Ж','З','И','Й','К','Л','М','Н','О','П','Р','С','Т','У','Ф','Х','Ц','Ч','Ш','Щ','Ъ','Ы','Ь','Э','Ю','Я'];
 		if ($.inArray(event.keyCode, codes) != '-1' || (event.keyCode >=48 && event.keyCode <=57) || (event.keyCode >=65 && event.keyCode <=90) || (event.keyCode >=96 && event.keyCode <=105)) 
 		{
 			if ($.inArray(event.key, letters) == -1) 
 				{
 					
 					return;
 				}
	 		else
	 		{
                event.preventDefault(); 
                showErrorUri($(this));
	        }
        }
        else 
        {
            event.preventDefault(); 
            showErrorUri($(this));
        }
 		
	});

	$('#options').on('keydown','.number-fill',function(event) {

 		var codes = [8, 9, 13, 16, 17, 18, 27, 33, 34, 35, 36, 37,38,39,40,45,46,189,109];

 		if ($.inArray(event.keyCode, codes) != '-1' || (event.keyCode >=48 && event.keyCode <=57) || (event.keyCode >=96 && event.keyCode <=105)) 
 		{
 			return;
        }
        else 
        {
            event.preventDefault(); 
            showErrorUri($(this));
        }
 		
	});
});

$('.input-uri').keyup(function(){

	var val = $(this).val();
	val = val.toLowerCase();
	$(this).val(val);
	return;
});

function showErrorUri(item)
{
	item.parent().addClass('has-error');
		setTimeout(function() {
			item.parent().removeClass('has-error');
		}, 300);
}