$(document).ready(function () {
	var state = [];
	$(".panel").on('click',function(e){
		var id = $(this).find('a').data("id");
		var body = $(this).find('.panel-body');
		if($.inArray(id, state) !== -1){
			return false;
		}
		state.push(id);
		$.ajax({
			url: '/admin/reportdaily/infoByProductID/'+id,
			type: 'POST',
			async: false,
			dataType: 'json',
			success: function (data) {
				console.log(data);
				var response = "";
				if(!$.isEmptyObject(data)) {
					body.empty();
					response = response + "<div class='card-body'><strong>Покупатель: </strong>" + data[0].u_name + " " + data[0].last_name;
					response = response + "<br><strong>Телефон: </strong>" + data[0].phone;
					response = response + "<br><strong>E-mail: </strong>" + data[0].email +"<br>";
					$.each(data, function (i, elem) {
						response = response + "<br><strong>Наименование товара: </strong>" + elem.p_name;
						response = response + "<br><strong>Цена товара: </strong>" + elem.price;
					})
					response = response + "</div>";
					body.append(response);
				}
			},
			error: function (data) {
				// console.log(data);
			}
		});
	});
	
});