$(document).ready(function() {
	$('.input-price').on('keydown', function(event) {
 		var val = $(this).val();
 		var codes = [8, 9, 13, 16, 17, 18, 27, 33, 34, 35, 36, 37,38,39,40,45,46,109, 188, 190, 110];
 		var letters = ['б','Б','ю','Ю']
 		if ($.inArray(event.keyCode, codes) != '-1' || (event.keyCode >=48 && event.keyCode <=57) || (event.keyCode >=96 && event.keyCode <=105)) 
 		{
 			
 			var count = val.match(/\./g);
 			if (count && count.length > 0 && (event.keyCode == 110 || event.keyCode == 188 ||event.keyCode == 190)) 
 				{
		            event.preventDefault(); 
		            showErrorUri($(this));
		        }
 			else 
			{
				if ($.inArray(event.key, letters) != -1) 
				{
		            event.preventDefault(); 
		            showErrorUri($(this));
		        }
		        else return;
			}
        }
        else 
        {
            event.preventDefault(); 
            showErrorUri($(this));
        }
	});
$('.input-price').keyup(function(){
	var val = $(this).val();
	val = val.replace(',','.');
	$(this).val(val);
	return;
});
});
function showErrorUri(item)
{
	item.parent().addClass('has-error');
		setTimeout(function() {
			item.parent().removeClass('has-error');
		}, 300);
}