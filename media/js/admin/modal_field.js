( function ( window, $, document ) {

  window.ModalClass = function ( settings ) {
    this.count = settings.count;
    // this.fullCount =
      this.fields = settings.fields;
    this.model = settings.model;
    this.field = settings.field;
    this.id = settings.id;
    this.head = settings.head;
    this.url = settings.url;
    this.attributes = settings.attributes;
    this.info = settings.info;
    this.pagination = settings.pagination;
    this.page = 1;
    this.updateState = false;
    this.dataPages = {};
    this.limit = settings.limit;
    this.searchText = '';
    this.elements = settings.elements;
    this.init();
    this.initEvents();
    this.activeButton = null;
    // console.log(this, settings);
  }

  window.ModalClass.prototype.init = function () {
    var self = this;
    var $table = $( '#' + self.id + ' .table' );
    $table.html( '' );
    self.loader( $table );
    $table.html( '<tbody>' );
    var th = $( '<tr>' );
    $.each( self.head, function ( filed, label ) {
      th.append( $( '<th>' ).text( label ) );
    } );
    $table.append( th );
    $.each( self.elements, function ( i, el ) {
      var tr = $( '<tr>' ).attr( 'data-id', el.id );
      var displayName = '';
      $.each( self.fields, function ( index, field ) {
        tr.append( $( '<td>' ).text( el[ field ] ) );
        displayName += el[ field ] + " ";
      } );
      tr.attr( 'data-display-name', displayName );
      $table.append( tr );
    } );
    self.Pagination();
  }

  window.ModalClass.prototype.initEvents = function () {
    var self = this;
    $( '#' + self.id + ' table ' ).on( 'click', 'tr', function () {
      $( '#' + self.id + ' table tr' ).removeClass( 'active' );
      $( this ).addClass( 'active' );
    } );

    $( '.element'+self.id ).on( 'click', '.btn-modal', function () {
      self.activeButton = $(this);
      self.search();
    } );

    $( '#add' + self.id ).on( 'click', function ( e ) {
      e.preventDefault();
      var btn = $( '.element', $( this ).parent() ).first().clone();
      $( 'input', btn ).val( '' );
      $( this ).before( btn );
      return false;
    } );

    $( '#' + self.id ).on( 'click', '.select_id', function () {
      var newID = $( '#' + self.id + ' table tr.active' ).data( 'id' );
      var displayName = $( '#' + self.id + ' table tr.active' ).data( 'display-name' );
      if ( newID === undefined ) return;
      $( '#' + $( this ).data( 'id' ) , self.activeButton.parent()).val( newID );
      $( '#' + $( this ).data( 'id' ) + '_name', self.activeButton.parent() ).val( displayName );
      $( '#' + $( this ).data( 'id' ) , self.activeButton.parent()).trigger( 'change' );
      console.log($( this ).data( 'id' ));
      var link = $( 'a', $( self.activeButton ).parent() );
      var text = '';
      $( '#' + self.id + ' table tr.active td' ).each( function () {
        text += $( this ).text() + ' ';
      } );
      link.attr( 'href', '/admin/' + self.model + '/edit/' + newID );
      link.text( "Подробнее" ); //text
      // if(self.info !== false)
      self.showInfo( $( self.activeButton ).parent(), newID );
    } );

    $( '#' + self.id ).on( 'click', '.search', function () {
      var val = $( 'input', $( this ).parent() ).val();
      if ( val == '' ) {
        self.updateState = true;
      }
      self.searchText = val;
      self.search();
    } );

    $( '#' + self.id ).on( 'input', '.input-modal', function () {
      self.filter( $( this ).val() );
    } );

    $( '#' + self.id + ' .pagination ' ).on( 'click', 'a', function ( e ) {
      e.preventDefault();
      if ( $( this ).parent().hasClass( 'active' ) ) return false;
      var page = parseInt( $( this ).text() );
      if ( page == NaN ) return;
      self.page = page;
      self.search();
      return false;
    } );

    $('.element' + self.id).on('click', '.deleteElement'+ self.id, function(e){
      e.preventDefault();
      var elements = $('.element'+self.id+ ' .element');
      if(elements.length == 1){
        $('input', elements).val('');
        $('ul', elements).remove();
        return false;
      }
      $(this).parent().remove();
      return false;
    })
  }

  window.ModalClass.prototype.filter = function ( value ) {
    var self = this,
      elements = {};
    if ( value !== '' ) {
      for ( var i in self.dataPages ) {
        Object.assign( elements, self.filterObjectProperties( self.dataPages[ i ], function ( data ) {
          var filtered = false;
          $.each( self.fields, function ( index, field ) {
            if ( !filtered )
              filtered = data[ field ].toLowerCase().indexOf( value.toLowerCase() ) !== -1;
          } )
          return filtered;
        } ) );
        if ( Object.keys( elements ).length >= self.limit ) {
          break;
        }
      }
      self.elements = elements;
    } else {
      self.elements = self.dataPages[ self.page ];
    }
    self.updateState = true;
    self.init();
  }

  window.ModalClass.prototype.filterObjectProperties = function ( obj, filtercb ) {
    var ret = {},
      self = this;
    count = 0;
    for ( var p in obj )
      if ( obj.hasOwnProperty( p ) )
        if ( filtercb( obj[ p ] ) ) {
          ret[ p ] = obj[ p ];
          if ( count == self.limit ) {
            return ret;
          }
          count++;
        }
    return ret;
  }

  window.ModalClass.prototype.showInfo = function ( element, id ) {
    $( 'ul', element ).remove();
    var self = this;
    var ul = $( '<ul>' );
    var item = this.elements[ id ];
    if ( item === undefined ) return false;
    $.each( self.info, function ( index, value ) {
      var li = $( '<li>' );
      try {
        var label = self.attributes[ value ];
      } catch ( e ) {
        var label = value;
      }
      var strong = $( '<strong>' ).text( ( label == undefined ? value : label ) + ": " );
      li.append( strong ).append( $( '<span>' ).text( item[ value ] ) );
      ul.append( li );
    } )
    $( element ).append( ul );
  }

  window.ModalClass.prototype.search = function ( cache = true ) {
    var self = this;
    if ( self.dataPages[ self.page ] !== undefined && cache && self.searchText == '' && !self.updateState ) {
      self.elements = self.dataPages[ self.page ];
      self.init();
      return;
    }
    self.updateState = false;
    $.ajax( {
      url: '/admin/' + self.url + '/modal',
      data: { 'field': self.field, 'search': self.searchText, 'page': self.page },
      type: 'POST',
      dataType: 'json',
      beforeSend: function () {
        var $table = $( '#' + self.id + ' .table' );
        $table.html( '' );
        self.loader( $table );
      },
      success: function ( data ) {
        self.elements = data.elements;
        self.dataPages[ self.page ] = data.elements;
        self.count = data.count;
        self.fullCount = data.count;
        self.init();
        self.removeLoader();
      },
      error: function ( err ) {
        self.removeLoader();
      }
    } )
  }

  window.ModalClass.prototype.Pagination = function () {
    var pages = Math.ceil( this.count / this.limit );
    $( '#' + this.id + ' .pagination' ).html( '' );
    if ( pages > 1 ) {
      var ul = $( '<ul>' ).addClass( 'pagination' );
      for ( var i = 1; i <= pages; i++ ) {
        var li = $( '<li>' ).append( $( '<a>' ).text( i ).attr( 'data-page', i ) );
        if ( i == this.page ) {
          li.addClass( 'active' );
        }
        ul.append( li );
      }
      $( '#' + this.id + ' .pagination' ).append( ul );
    }
  }

  window.ModalClass.prototype.loader = function ( append ) {
    $( append ).append( ' <div class="loader">\n'
      + '                  <div class="loader-inner box1"></div>\n'
      + '                  <div class="loader-inner box2"></div>\n'
      + '                  <div class="loader-inner box3"></div>\n'
      + '                 /div>\n' );
  }

  window.ModalClass.prototype.removeLoader = function () {
    $( '.loader' ).remove();
  }

} )( window, $, document )