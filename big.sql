-- phpMyAdmin SQL Dump
-- version 4.6.6deb4
-- https://www.phpmyadmin.net/
--
-- Хост: localhost
-- Время создания: Дек 04 2018 г., 22:04
-- Версия сервера: 10.1.26-MariaDB-0+deb9u1
-- Версия PHP: 7.0.27-0+deb9u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `big`
--

-- --------------------------------------------------------

--
-- Структура таблицы `admin_pages`
--

CREATE TABLE `admin_pages` (
  `id` int(255) NOT NULL,
  `name` varchar(1024) NOT NULL,
  `icon` varchar(1024) NOT NULL,
  `uri` varchar(1024) NOT NULL,
  `sort_order` int(255) NOT NULL,
  `site_access` int(25) NOT NULL,
  `visible` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `admin_pages`
--

INSERT INTO `admin_pages` (`id`, `name`, `icon`, `uri`, `sort_order`, `site_access`, `visible`) VALUES
(1, 'Пользователи', '<i class=\"fa fa-database\" aria-hidden=\"true\"></i>', 'user', 1, 1, 1),
(3, 'Группы пользователей', '<i class=\"fa fa-database\" aria-hidden=\"true\"></i>', 'usergroup', 4000, 1, 1),
(4, 'Управление категориями', '<i class=\"fa fa-database\" aria-hidden=\"true\"></i>', 'category', 4000, 1, 1),
(5, 'Склады', '<i class=\"fa fa-database\" aria-hidden=\"true\"></i>', 'warehouse', 4000, 1, 1),
(6, 'Управление товарами', '<i class=\"fa fa-database\" aria-hidden=\"true\"></i>', 'product', 4000, 1, 1),
(7, 'Новости', '<i class=\"fa fa-database\" aria-hidden=\"true\"></i>', 'news', 4000, 1, 1),
(8, 'Новости', '<i class=\"fa fa-database\" aria-hidden=\"true\"></i>', 'newscategory', 4000, 4, 0),
(11, 'Магазины', '<i class=\"fa fa-database\" aria-hidden=\"true\"></i>', 'shop', 4000, 4, 1),
(12, 'Статусы сайта', '<i class=\"fa fa-database\" aria-hidden=\"true\"></i>', 'shopstatus', 4000, 4, 1),
(13, 'Тарифы', '<i class=\"fa fa-database\" aria-hidden=\"true\"></i>', 'rate', 4000, 4, 1),
(15, 'Отзывы', '<i class=\"fa fa-database\" aria-hidden=\"true\"></i>', 'review', 4000, 4, 1),
(16, 'FAQ', '<i class=\"fa fa-database\" aria-hidden=\"true\"></i>', 'question', 4000, 4, 1),
(17, 'Сообщение поддержки', '<i class=\"fa fa-database\" aria-hidden=\"true\"></i>', 'supportquestion', 4000, 4, 1),
(18, 'Группы свойств', '<i class=\"fa fa-database\" aria-hidden=\"true\"></i>', 'propertygroup', 4000, 4, 1),
(19, 'Свойства каталога', '<i class=\"fa fa-database\" aria-hidden=\"true\"></i>', 'properties', 4000, 4, 1),
(20, 'Свойство enum', '<i class=\"fa fa-database\" aria-hidden=\"true\"></i>', 'propertyenum', 4000, 4, 0),
(21, 'Значение свойства', '<i class=\"fa fa-database\" aria-hidden=\"true\"></i>', 'propertyvalue', 4000, 4, 0),
(22, 'Слайдер', '<i class=\"fa fa-database\" aria-hidden=\"true\"></i>', 'slider', 4000, 4, 1),
(23, 'Бренды', '<i class=\"fa fa-database\" aria-hidden=\"true\"></i>', 'brands', 4000, 4, 1),
(24, 'Инфо слайдер', '<i class=\"fa fa-database\" aria-hidden=\"true\"></i>', 'infoslider', 4000, 4, 1),
(25, 'Выписка по заявкам на вывод', '<i class=\"fa fa-database\" aria-hidden=\"true\"></i>', 'requestoutput', 4000, 4, 1),
(26, 'Перевод средств', '<i class=\"fa fa-database\" aria-hidden=\"true\"></i>', 'remittance', 4000, 4, 1),
(27, 'Переводы между агентами', '<i class=\"fa fa-database\" aria-hidden=\"true\"></i>', 'reporttransaction', 4000, 4, 1),
(28, 'Акции', '<i class=\"fa fa-database\" aria-hidden=\"true\"></i>', 'discount', 4000, 4, 1),
(30, 'Отчеты', '<i class=\"fa fa-database\" aria-hidden=\"true\"></i>', 'reportdaily', 4000, 4, 1),
(31, 'Курьеры', '<i class=\"fa fa-database\" aria-hidden=\"true\"></i>', 'courier', 4000, 4, 1),
(32, 'Категрии пунктов меню', '<i class=\"fa fa-database\" aria-hidden=\"true\"></i>', 'adminpagecategory', 4000, 4, 1),
(33, 'Заказы товаров', '<i class=\"fa fa-database\" aria-hidden=\"true\"></i>', 'order', 4000, 4, 1),
(34, 'Статусы заказа', '<i class=\"fa fa-database\" aria-hidden=\"true\"></i>', 'orderstatus', 4000, 4, 1),
(35, 'Управление регионами', '<i class=\"fa fa-database\" aria-hidden=\"true\"></i>', 'region', 4000, 4, 1),
(36, 'Поставщики', '<i class=\"fa fa-database\" aria-hidden=\"true\"></i>', 'supplier', 1, 1, 1),
(37, 'Покупатели', '<i class=\"fa fa-database\" aria-hidden=\"true\"></i>', 'byer', 1, 1, 1),
(38, 'Агенты', '<i class=\"fa fa-database\" aria-hidden=\"true\"></i>', 'seller', 1, 1, 1),
(39, 'Витрина товаров', '<i class=\"fa fa-database\" aria-hidden=\"true\"></i>', 'shopcase', 4000, 4, 1),
(40, 'Блоки', '<i class=\"fa fa-database\" aria-hidden=\"true\"></i>', 'block', 4000, 4, 1),
(41, 'Шаблоны', '<i class=\"fa fa-database\" aria-hidden=\"true\"></i>', 'template', 4000, 4, 1),
(42, 'Цветовая схема', '<i class=\"fa fa-database\" aria-hidden=\"true\"></i>', 'colorscheme', 4000, 4, 1),
(43, 'Выписки по покупкам', '<i class=\"fa fa-database\" aria-hidden=\"true\"></i>', 'statementorderproduct', 4000, 4, 1),
(44, 'Заказ товара агентом', '<i class=\"fa fa-database\" aria-hidden=\"true\"></i>', 'orderinternal', 4000, 4, 1),
(45, 'Категории вопросов', '<i class=\"fa fa-database\" aria-hidden=\"true\"></i>', 'questioncategory', 4000, 4, 1),
(46, 'Типы вопросов', '<i class=\"fa fa-database\" aria-hidden=\"true\"></i>', 'questiontype', 4000, 4, 1),
(47, 'Контакты', '<i class=\"fa fa-database\" aria-hidden=\"true\"></i>', 'contact', 4000, 4, 1),
(48, 'Внутренний чат', '<i class=\"fa fa-database\" aria-hidden=\"true\"></i>', 'messages', 4000, 4, 1),
(49, 'Изменение цен от вендора', '<i class=\"fa fa-database\" aria-hidden=\"true\"></i>', 'changeprice', 4000, 4, 1),
(50, 'Назначить курьера', '<i class=\"fa fa-database\" aria-hidden=\"true\"></i>', 'appoint', 4000, 4, 1),
(51, 'Тип доставки', '<i class=\"fa fa-database\" aria-hidden=\"true\"></i>', 'deliverytype', 4000, 4, 1),
(52, 'Заявки на вывод средств', '<i class=\"fa fa-database\" aria-hidden=\"true\"></i>', 'noticerequestoutput', 4000, 4, 1),
(53, 'Выписки агентам за подключение', '<i class=\"fa fa-database\" aria-hidden=\"true\"></i>', 'statementagent', 4000, 4, 1),
(54, 'test', '<i class=\"fa fa-database\" aria-hidden=\"true\"></i>', 'test', 4000, 4, 1),
(55, 'news', '<i class=\"fa fa-database\" aria-hidden=\"true\"></i>', 'news', 4000, 4, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `admin_page_category`
--

CREATE TABLE `admin_page_category` (
  `id` int(255) NOT NULL,
  `name` varchar(1024) NOT NULL,
  `sort` int(255) NOT NULL,
  `pages` varchar(1024) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `admin_page_category`
--

INSERT INTO `admin_page_category` (`id`, `name`, `sort`, `pages`) VALUES
(1, 'Пользователи', 2, 'a:3:{i:0;s:2:\"36\";i:1;s:2:\"37\";i:2;s:2:\"38\";}'),
(3, 'Товары', 0, 'a:1:{i:0;s:1:\"6\";}'),
(4, 'Управление контентом', 3, 'a:7:{i:0;s:1:\"7\";i:1;s:2:\"22\";i:2;s:2:\"23\";i:3;s:2:\"24\";i:4;s:2:\"35\";i:5;s:2:\"39\";i:6;s:2:\"47\";}'),
(5, 'Финансы', 5, 'a:3:{i:0;s:2:\"25\";i:1;s:2:\"43\";i:2;s:2:\"53\";}'),
(6, 'Конструктор сайта', 4, 'a:6:{i:0;s:2:\"11\";i:1;s:2:\"12\";i:2;s:2:\"13\";i:3;s:2:\"40\";i:4;s:2:\"41\";i:5;s:2:\"42\";}'),
(8, 'Тех поддержка', 8, 'a:5:{i:0;s:2:\"15\";i:1;s:2:\"16\";i:2;s:2:\"17\";i:3;s:2:\"48\";i:4;s:2:\"33\";}'),
(9, 'Категории', 1, 'a:3:{i:0;s:1:\"4\";i:1;s:2:\"18\";i:2;s:2:\"19\";}'),
(10, 'Администрация сайта', 6, 'a:2:{i:0;s:1:\"1\";i:1;s:2:\"31\";}'),
(11, 'Уведомления', 7, 'a:4:{i:0;s:2:\"44\";i:1;s:2:\"27\";i:2;s:2:\"49\";i:3;s:2:\"52\";}');

-- --------------------------------------------------------

--
-- Структура таблицы `block`
--

CREATE TABLE `block` (
  `id` int(255) NOT NULL,
  `name` varchar(1024) NOT NULL,
  `image` text NOT NULL,
  `required` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `block`
--

INSERT INTO `block` (`id`, `name`, `image`, `required`) VALUES
(1, 'test', '/images/aecafc27e7a129b892238db6500d1e9e.jpg', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `brands`
--

CREATE TABLE `brands` (
  `id` int(255) NOT NULL,
  `active` int(255) NOT NULL DEFAULT '1',
  `sort` int(255) NOT NULL,
  `image` varchar(1024) NOT NULL,
  `link` varchar(1024) NOT NULL,
  `name` varchar(1024) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `brands`
--

INSERT INTO `brands` (`id`, `active`, `sort`, `image`, `link`, `name`) VALUES
(1, 1, 0, '/images/b8c8da5413c9ad556d81ddad96a112f0.jpg', 'test', 'test'),
(2, 1, 5, '/images/3bc4cb055043ebcce2a6b04d743fc409.jpg', 'dsadsa', 'test2'),
(3, 1, 6, '/images/55323579293d7b2c445762763ec7edc7.jpg', 'test231213', 'test3'),
(4, 1, 3, '/images/139c2cb7a189df3013254def2238e23d.jpg', 'test', 'tes'),
(5, 1, 1, '/images/72a401d36b320d339f5b6dc72ca4db14.jpg', 'asdfasdf', 'sadsfdasadf'),
(6, 1, 2, '/images/71e693865cdd5d1f7e4be01cc0c8a6c8.jpg', 'sadsadgassadg', 'sadgsadgasdg'),
(7, 1, 4, '/images/a6c07924b48bd329922e27f099a949e2.jpg', 'sony', 'sony');

-- --------------------------------------------------------

--
-- Структура таблицы `category`
--

CREATE TABLE `category` (
  `id` int(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `created_at` int(100) NOT NULL,
  `updated_at` int(100) NOT NULL,
  `category_icon` varchar(255) NOT NULL,
  `sort` int(11) NOT NULL,
  `parent_id` int(15) NOT NULL,
  `category_image` varchar(255) NOT NULL,
  `alias` varchar(255) NOT NULL,
  `meta_title` varchar(65) NOT NULL,
  `meta_description` varchar(180) NOT NULL,
  `meta_keywords` varchar(180) NOT NULL,
  `commission` int(100) NOT NULL,
  `active` int(1) NOT NULL,
  `balls` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `category`
--

INSERT INTO `category` (`id`, `name`, `created_at`, `updated_at`, `category_icon`, `sort`, `parent_id`, `category_image`, `alias`, `meta_title`, `meta_description`, `meta_keywords`, `commission`, `active`, `balls`) VALUES
(1, 'Электроника', -10800, -10800, '', 0, 0, '/images/aed4703404bc27fc0f075709799729d3.jpg', 'elektronika', 'asds', 'asdasd', 'asdasd', 0, 1, 0),
(2, 'Компьютеры', 0, 0, 'sadf', 1, 0, '/images/6574ed86099db55db270ca280e81243c.jpg', 'kompyuteri', 'kompyuteri', 'kompyuteri', 'kompyuteri', 0, 1, 0),
(3, 'Бытовая техника', 0, 0, '12342134', 2, 0, '', 'bitovaya-tehnika', '123412', '341234', '1234', 12341234, 1, 0),
(4, 'Детские товары', 0, 0, '12342134', 5, 0, '', 'detskie-tovari', '123412', '341234', '1234', 12341234, 1, 0),
(5, 'Зоотовары', 0, 0, '12342134', 8, 0, '', 'zootovari', '123412', '341234', '1234', 12341234, 1, 0),
(6, 'Дом, дача, ремонт', 0, 0, '12342134', 3, 0, '', 'dom-dacha-remont', '123412', '341234', '1234', 12341234, 1, 0),
(7, 'Одежда и обувь', 0, 0, '12342134', 7, 0, '', 'odejda-i-obuv', '123412', '341234', '1234', 12341234, 1, 0),
(8, 'Красота и здоровье', 0, 0, '12342134', 6, 0, '', 'krasota-i-zdorove', '123412', '341234', '1234', 12341234, 1, 0),
(9, 'Авто', 0, 0, '12342134', 4, 0, '', 'avto', '123412', '341234', '1234', 12341234, 1, 0),
(11, 'Мобильные телефоны', -10800, -10800, '12341234', 2, 1, '/images/a316e3db6086d8a1655fe99993cb34de.png', 'mobilnie-telefoni', '', '', '', 0, 1, 0),
(12, 'Умные часы и браслеты', 0, 0, '12341234', 6, 1, '', 'umnie-chasi-i-brasleti', '', '', '', 0, 1, 0),
(13, 'Телевизоры', 0, 0, '12341234', 1, 1, '/images/9adf1311ecd2a5192ff3066287b0dccd.png', 'televizori', '', '', '', 0, 1, 0),
(14, 'Фото- и видеокамеры', 0, 0, '12341234', 3, 1, '/images/b66ac92aa3197ce43e815b75bad5412e.png', 'foto-i-videokameri', '', '', '', 0, 1, 0),
(15, 'Аудио- и видеотехника', 0, 0, '12341234', 4, 1, '/images/a4e7ecc700d4aa257491157ebddee795.png', 'audio-i-videotehnika', '', '', '', 0, 1, 0),
(16, 'Наушники', 0, 0, '12341234', 5, 1, '/images/b55b9efa12a43fd50a1c083e344c19e8.png', 'naushniki', '', '', '', 0, 1, 0),
(17, 'Игры', 0, 0, '12341234', 7, 1, '', 'igri', '', '', '', 0, 1, 0),
(18, 'GPS-навигаторы', 0, 0, '12341234', 8, 1, '', 'gps-navigatori', '', '', '', 0, 1, 0),
(19, 'Ноутбуки', -10800, -10800, '12341234', 3, 2, '/images/71aaadaba00321d3e5ceffaf5e7f715f.png', 'noutbuki', '', '', '', 0, 1, 0),
(20, 'Планшеты', 0, 0, 'https://image.flaticon.com/icons/svg/259/259968.svg', 3, 2, '', 'plansheti', '', '', '', 0, 1, 0),
(22, 'Компьютеры', -10800, -10800, 'https://image.flaticon.com/icons/svg/259/259968.svg', 3, 2, '/images/4c38999edc5d582fa705bf5852f0f8d1.jpg', 'pc', '', '', '', 0, 1, 0),
(23, 'Комплектующие', 0, 0, '12341234', 3, 2, '', 'komplektuyuschie', '', '', '', 0, 1, 0),
(24, 'Мониторы', -10800, -10800, '12341234', 3, 2, '/images/a6d29927e6b93ccfa24782a724bb96fb.jpg', 'monitori', '', '', '', 0, 1, 0),
(25, 'Принтеры', -10800, -10800, '12341234', 3, 2, '/images/ad8ce09bda6e62f1a60b7b9d09057614.jpg', 'printeri', '', '', '', 0, 1, 0),
(26, 'Сетевое оборудование', 0, 0, '12341234', 3, 2, '', 'setevoe-oborudovanie', '', '', '', 0, 1, 0),
(27, 'Аксесуары', 0, 0, '12341234', 3, 2, '', 'aksesuari', '', '', '', 0, 1, 0),
(28, 'Крупная техника для кухни', 0, 0, '12341234', 1, 3, '', 'krupnaya-tehnika-dlya-kuhni', '', '', '', 0, 1, 0),
(29, 'Приготовление напитков', 0, 0, '12341234', 2, 3, '', 'prigotovlenie-napitkov', '', '', '', 0, 1, 0),
(30, 'Приготовление блюд', 0, 0, '12341234', 3, 3, '', 'prigotovlenie-blyud', '', '', '', 0, 1, 0),
(31, 'Техника для дома', 0, 0, '12341234', 4, 3, '', 'tehnika-dlya-doma', '', '', '', 0, 1, 0),
(32, 'Климатическая техника', 0, 0, '12341234', 5, 3, '', 'klimaticheskaya-tehnika', '', '', '', 0, 1, 0),
(33, 'Встраиваемая техника', 0, 0, '12341234', 6, 3, '', 'vstraivaemaya-tehnika', '', '', '', 0, 1, 0),
(34, 'Техника для красоты', 0, 0, '12341234', 3, 3, '', 'tehnika-dlya-krasoti', '', '', '', 0, 1, 0),
(35, 'Для мам и малышей', 0, 0, '12341234', 3, 4, '', 'dlya-mam-i-malishey', '', '', '', 0, 1, 0),
(36, 'Подгузники', 0, 0, '12341234', 3, 4, '', 'podguzniki', '', '', '', 0, 1, 0),
(37, 'Детское питание', 0, 0, '12341234', 3, 4, '', 'detskoe-pitanie', '', '', '', 0, 1, 0),
(38, 'Прогулки и путешествия', 0, 0, '12341234', 3, 4, '', 'progulki-i-puteshestviya', '', '', '', 0, 1, 0),
(40, 'Игрушки', 0, 0, '12341234', 3, 4, '', 'igrushki', '', '', '', 0, 1, 0),
(41, 'Хобби и творчество', 0, 0, '12341234', 3, 4, '', 'hobbi-i-tvorchestvo', '', '', '', 0, 1, 0),
(42, 'Развитие и обучение', 0, 0, '12341234', 3, 4, '', 'razvitie-i-obuchenie', '', '', '', 0, 1, 0),
(43, 'Корма для кошек', 0, 0, '12341234', 3, 5, '', 'korma-dlya-koshek', '', '', '', 0, 1, 0),
(44, 'Корма для собак', 0, 0, '12341234', 3, 5, '', 'korma-dlya-sobak', '', '', '', 0, 1, 0),
(45, 'Товары для собак', 0, 0, '12341234', 3, 5, '', 'tovari-dlya-sobak', '', '', '', 0, 1, 0),
(46, 'Товары для кошек', 0, 0, '12341234', 3, 5, '', 'tovari-dlya-koshek', '', '', '', 0, 1, 0),
(47, 'Товары для птиц', 0, 0, '12341234', 3, 5, '', 'tovari-dlya-ptits', '', '', '', 0, 1, 0),
(48, 'Товары для рыб', -10800, -10800, '12341234', 3, 5, '/images/c48205605c9ad54c7e8243bd406bd00d.jpg', 'tovari-dlya-rib', '', '', '', 0, 1, 0),
(49, 'Товары для грызунов', 0, 0, '12341234', 3, 5, '', 'tovari-dlya-grizunov', '', '', '', 0, 1, 0),
(50, 'Всё для дачи', 0, 0, '12341234', 1, 6, '', 'vsё-dlya-dachi', '', '', '', 0, 1, 0),
(51, 'Садовая техника', 0, 0, '12341234', 2, 6, '', 'sadovaya-tehnika', '', '', '', 0, 1, 0),
(52, 'Отдых и пикник', 0, 0, '12341234', 3, 6, '', 'otdih-i-piknik', '', '', '', 0, 1, 0),
(53, 'Всё для ремонта', -10800, -10800, '12341234', 4, 6, '', 'vse-dlya-remonta', '', '', '', 0, 1, 0),
(55, 'Мебель', 0, 0, '12341234', 5, 6, '', 'mebel', '', '', '', 0, 1, 0),
(56, 'Посуда и кухня', 0, 0, '12341234', 6, 6, '', 'posuda-i-kuhnya', '', '', '', 0, 1, 0),
(57, 'Хозтовары', 0, 0, '12341234', 7, 6, '', 'hoztovari', '', '', '', 0, 1, 0),
(58, 'Бытовая химия', 0, 0, '12341234', 8, 6, '', 'bitovaya-himiya', '', '', '', 0, 1, 0),
(59, 'Освещение', 0, 0, '12341234', 9, 6, '', 'osveschenie', '', '', '', 0, 1, 0),
(60, 'Женщинам', 0, 0, '12341234', 5, 7, '', 'jenschinam', '', '', '', 0, 1, 0),
(61, 'Мужчинам', 0, 0, '12341234', 1, 7, '', 'mujchinam', '', '', '', 0, 1, 0),
(62, 'Девочкам ', 0, 0, '12341234', 2, 7, '', 'devochkam-', '', '', '', 0, 1, 0),
(63, 'Мальчикам', 0, 0, '12341234', 3, 7, '', 'malchikam', '', '', '', 0, 1, 0),
(64, 'Малышам', 0, 0, '12341234', 4, 7, '', 'malisham', '', '', '', 0, 1, 0),
(65, 'Сумки и чемоданы', 0, 0, '12341234', 6, 7, '', 'sumki-i-chemodani', '', '', '', 0, 1, 0),
(66, 'Аксессуары', 0, 0, '12341234', 7, 7, '', 'aksessuari', '', '', '', 0, 1, 0),
(67, 'Аптека', 0, 0, '12341234', 3, 8, '', 'apteka', '', '', '', 0, 1, 0),
(68, 'Макияж', 0, 0, '12341234', 3, 8, '', 'makiyaj', '', '', '', 0, 1, 0),
(69, 'Парфюмерия', 0, 0, '12341234', 3, 8, '', 'parfyumeriya', '', '', '', 0, 1, 0),
(70, 'Уход за лицом', 0, 0, '12341234', 3, 8, '', 'uhod-za-litsom', '', '', '', 0, 1, 0),
(71, 'Уход за волосами', 0, 0, '12341234', 3, 8, '', 'uhod-za-volosami', '', '', '', 0, 1, 0),
(72, 'Уход за телом', 0, 0, '12341234', 3, 8, '', 'uhod-za-telom', '', '', '', 0, 1, 0),
(74, 'Шины', 0, 0, '12341234', 9, 9, '', 'shini', '', '', '', 0, 1, 0),
(75, 'Диски', 0, 0, '12341234', 2, 9, '', 'diski', '', '', '', 0, 1, 0),
(76, 'Видео регистраторы', 0, 0, '12341234', 3, 9, '', 'video-registratori', '', '', '', 0, 1, 0),
(77, 'Аудио и видео', 0, 0, '12341234', 4, 9, '', 'audio-i-video', '', '', '', 0, 1, 0),
(79, 'Аксессуары для авто', 0, 0, '12341234', 5, 9, '', 'aksessuari-dlya-avto', '', '', '', 0, 1, 0),
(80, 'Запчасти', 0, 0, '12341234', 6, 9, '', 'zapchasti', '', '', '', 0, 1, 0),
(81, 'Автохимия', 0, 0, '12341234', 7, 9, '', 'avtohimiya', '', '', '', 0, 1, 0),
(82, 'Противоугонные системы', 0, 0, '12341234', 8, 9, '', 'protivougonnie-sistemi', '', '', '', 0, 1, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `color_scheme`
--

CREATE TABLE `color_scheme` (
  `id` int(255) NOT NULL,
  `name` varchar(1024) NOT NULL,
  `image` text NOT NULL,
  `file` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `color_scheme`
--

INSERT INTO `color_scheme` (`id`, `name`, `image`, `file`) VALUES
(1, 'test', '/images/a17bff09e698d3ac94582a96a207c65d.png', '/images/c4febb40d86464be00956460dacf384c.css');

-- --------------------------------------------------------

--
-- Структура таблицы `contact`
--

CREATE TABLE `contact` (
  `id` int(255) NOT NULL,
  `support_phone` varchar(1024) NOT NULL,
  `phone` varchar(1024) NOT NULL,
  `text` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `contact`
--

INSERT INTO `contact` (`id`, `support_phone`, `phone`, `text`) VALUES
(1, '+7(814)-233-22-11', '+7(814)-233-22-22', '<p class=\"desc-text\">\r\n			Далеко-далеко за словесными горами в стране гласных и согласных живут рыбные тексты. Своего но имеет, домах буквенных одна, заголовок lorem свой дороге журчит коварный, текстов над языкового он продолжил! Пояс, имеет жизни. Всеми, буквенных строчка. Дороге, языком имени рукописи домах собрал ведущими правилами. Сих, выйти всеми. На берегу семь, она использовало обеспечивает рот составитель ему сих, образ большого маленькая ты, толку безорфографичный над напоивший. Это грустный мир, свою продолжил злых вскоре не сбить текст эта вершину своих переписали даль маленький коварных своего знаках семантика. Переулка, диких не коварных пор заголовок запятых, взобравшись грамматики по всей рекламных текстами буквенных повстречался семь залетают одна от всех собрал?\r\n		</p>\r\n		<h2>Условия оплаты</h2>\r\n		<p class=\"desc-text\">\r\n			Далеко-далеко за, словесными горами в стране гласных и согласных живут рыбные тексты. Они которое безорфографичный оксмокс она мир сбить возвращайся? Предупреждал текстами возвращайся большого маленькая не рукописи взобравшись раз несколько сбить подзаголовок журчит, деревни себя домах лучше! Безорфографичный, сих точках вопроса продолжил все буквенных, страну пор силуэт текста пустился которое дороге имени?\r\n		</p>\r\n		<h2>Гарантия доставки и возврата товара</h2>\r\n		<p class=\"desc-text\">\r\n			Далеко-далеко за, словесными горами в стране гласных и согласных живут рыбные тексты. Они которое безорфографичный оксмокс она мир сбить возвращайся? Предупреждал текстами возвращайся большого маленькая не рукописи взобравшись раз несколько сбить подзаголовок журчит, деревни себя домах лучше! Безорфографичный, сих точках вопроса продолжил все буквенных, страну пор силуэт текста пустился которое дороге имени?\r\n		</p>\r\n		<h2>Юридическая информация</h2>\r\n		<ul class=\"list\">\r\n			<li><a href=\"#!\" class=\"download download_pdf\">Договор для агента</a></li>\r\n			<li><a href=\"#!\" class=\"download download_pdf\">Договор для поставщика</a></li>\r\n			<li><a href=\"#!\" class=\"download download_pdf\">Договор для покупателя</a></li>\r\n			<li><a href=\"#!\" class=\"download download_pdf\">Правила публикации товара</a></li>\r\n			<li><a href=\"#!\" class=\"download download_pdf\">Политика конфиденциальности</a></li>\r\n		</ul>');

-- --------------------------------------------------------

--
-- Структура таблицы `courier`
--

CREATE TABLE `courier` (
  `id` int(255) NOT NULL,
  `user_id` int(255) NOT NULL,
  `license_plate` varchar(1024) NOT NULL,
  `passport_photo` text NOT NULL,
  `drive_license_photo` text NOT NULL,
  `count_order` int(11) NOT NULL,
  `car_type` varchar(255) NOT NULL,
  `mark` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `courier`
--

INSERT INTO `courier` (`id`, `user_id`, `license_plate`, `passport_photo`, `drive_license_photo`, `count_order`, `car_type`, `mark`, `status`) VALUES
(1, 106, '12312321safa', '/images/03389f9eef9ba3e02ad8694bf2576fa3.png', '/images/f35e890ccdebc073095ab38e2e404ad7.png', 12, 'test111', 'test', 'test'),
(2, 110, 'test1', '', '', 6, 'test2', 'test3', '');

-- --------------------------------------------------------

--
-- Структура таблицы `delivery_address`
--

CREATE TABLE `delivery_address` (
  `id` int(255) NOT NULL,
  `delivery_name` varchar(255) NOT NULL,
  `delivery_last_name` varchar(255) NOT NULL,
  `patronymic` varchar(255) NOT NULL,
  `country` varchar(255) NOT NULL,
  `region` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `street` varchar(255) NOT NULL,
  `house_number` int(10) NOT NULL,
  `apartment` int(10) NOT NULL,
  `user_id` int(255) NOT NULL,
  `post_index` varchar(15) NOT NULL,
  `phone` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `delivery_address`
--

INSERT INTO `delivery_address` (`id`, `delivery_name`, `delivery_last_name`, `patronymic`, `country`, `region`, `city`, `street`, `house_number`, `apartment`, `user_id`, `post_index`, `phone`) VALUES
(1, 'Иван', 'Иванов', 'Иванович', 'Беларусь', 'Могилев', 'Могилев', 'Первомайская', 11, 22, 21, '212000', '12334421'),
(2, 'Петр', 'Петров', 'Петрович', 'Беларусь', 'Могилев', 'Могилев', 'Пионерская', 12, 0, 21, '212000', '34124123'),
(5, 'Василий', 'Козлов', 'Николаевич', 'test12', 'test12', 'test12', 'test12', 112, 112, 21, '123123', '34122131');

-- --------------------------------------------------------

--
-- Структура таблицы `delivery_status`
--

CREATE TABLE `delivery_status` (
  `id` int(255) NOT NULL,
  `type` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `delivery_status`
--

INSERT INTO `delivery_status` (`id`, `type`) VALUES
(1, 'Ожидает отправки'),
(2, 'Передано курьеру'),
(3, 'Доставлено');

-- --------------------------------------------------------

--
-- Структура таблицы `delivery_type`
--

CREATE TABLE `delivery_type` (
  `id` int(255) NOT NULL,
  `name` varchar(1024) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `delivery_type`
--

INSERT INTO `delivery_type` (`id`, `name`) VALUES
(1, 'test');

-- --------------------------------------------------------

--
-- Структура таблицы `dialog`
--

CREATE TABLE `dialog` (
  `id` int(255) NOT NULL,
  `sender_id` int(255) NOT NULL,
  `receiver_id` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `dialog`
--

INSERT INTO `dialog` (`id`, `sender_id`, `receiver_id`) VALUES
(1, 23, 33),
(24, 23, 34),
(25, 33, 45),
(26, 96, 45),
(33, 33, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `discount`
--

CREATE TABLE `discount` (
  `id` int(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `discount` int(255) NOT NULL,
  `has_expired` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `discount`
--

INSERT INTO `discount` (`id`, `title`, `discount`, `has_expired`) VALUES
(2, 'sdjf5682mwe', 100, 1),
(3, 'sbxczv682mwe', 100, 1),
(4, 'sbxcsdf2mwe', 100, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `info_slider`
--

CREATE TABLE `info_slider` (
  `id` int(255) NOT NULL,
  `active` int(255) NOT NULL DEFAULT '1',
  `sort` int(255) NOT NULL,
  `text` varchar(1024) NOT NULL,
  `image` text NOT NULL,
  `image_bg` text NOT NULL,
  `color` varchar(1024) NOT NULL,
  `link` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `info_slider`
--

INSERT INTO `info_slider` (`id`, `active`, `sort`, `text`, `image`, `image_bg`, `color`, `link`) VALUES
(1, 1, 0, '<i class=\"f-green d_b color_black\">ЙЙУ</i>', '', '/images/3c730b9bf25dd07e97c451b6334fcfe7.jpg', '234:221:206:255', 'dsasdadasdas'),
(2, 1, 0, '<i class=\"f-green d_b color_black\">новинка</i><b class=\"f-black color_white\">Strong <span class=\"d_b fz_24 mt-10 color_white\">будильник</span></b>', '', '/images/gipu/products/watch.png', '176:206:238:255', 'dasddasdas'),
(3, 1, 0, '<span class=\"f-light color_white\">Чайник <b class=\"f-black d_b color_white\">BORK</b></span>', '/images/gipu/icon_wifi.png', '/images/gipu/products/kettle.png', '189:180:244:255', 'dasddasdas'),
(4, 1, 0, '<span class=\"f-light color_white\">Чайник <b class=\"f-black d_b color_white\">BORK</b></span>', '', '/images/gipu/products/chair.png', '159:217:132:255', 'dasddasdas'),
(5, 1, 0, '<i class=\"f-green d_b color_black\">новинка</i><b class=\"f-black op_70\">Xiaomi<br>Redmi</b>', '', '/images/gipu/products/smartphone.png', '234:221:206:255', 'dsasdadasdas'),
(6, 1, 0, '<i class=\"f-green d_b color_black\">новинка</i><b class=\"f-black color_white\">Strong <span class=\"d_b fz_24 mt-10 color_white\">будильник</span></b>', '', '/images/gipu/products/watch.png', '176:206:238:255', 'dasddasdas'),
(7, 1, 0, '<span class=\"f-light color_white\">Чайник <b class=\"f-black d_b color_white\">BORK</b></span>', '/images/gipu/icon_wifi.png', '/images/gipu/products/kettle.png', '189:180:244:255', 'dasddasdas'),
(8, 1, 0, '<span class=\"f-light color_white\">Чайник <b class=\"f-black d_b color_white\">BORK</b></span>', '', '/images/gipu/products/chair.png', '159:217:132:255', 'dasddasdas');

-- --------------------------------------------------------

--
-- Структура таблицы `juristic_data`
--

CREATE TABLE `juristic_data` (
  `id` int(255) NOT NULL,
  `organization_name` varchar(255) NOT NULL,
  `juristic_address` text NOT NULL,
  `inn` varchar(255) NOT NULL,
  `kpp` varchar(255) NOT NULL,
  `checking_account` varchar(255) NOT NULL,
  `bik` varchar(255) NOT NULL,
  `ks` varchar(255) NOT NULL,
  `contact_name` varchar(100) NOT NULL,
  `bank_name` varchar(255) NOT NULL,
  `skan_registration` varchar(255) NOT NULL,
  `skan_passport` varchar(255) NOT NULL,
  `address_bank` varchar(255) NOT NULL,
  `swift` varchar(255) NOT NULL,
  `rs` varchar(255) NOT NULL,
  `ogrn` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `juristic_data`
--

INSERT INTO `juristic_data` (`id`, `organization_name`, `juristic_address`, `inn`, `kpp`, `checking_account`, `bik`, `ks`, `contact_name`, `bank_name`, `skan_registration`, `skan_passport`, `address_bank`, `swift`, `rs`, `ogrn`) VALUES
(35, 'sssssssssssssssss', 'sssssssssssssssss', 'sssssssssssssssss', 'sssssssssssssssss', 'sssssssssssssssss', 'sssssssssssssssss', 'sssssssssssssssss', 'sssssssssssssssss', '', '', '', '', '', '', ''),
(36, 'sssssssssssssssss12312131123aaaaaaaaaaaaa', 'sssssssssssssssssaaaaaaaaaaaaaaa', '123123', '3554', '123321212112', '12332112123', '12332121213', '123123asasss', 'Название банка', '/images/JuristicData/1fc5834ba727e21848e107956d23b593.png', '/images/JuristicData/68468bb476f75e3cbf8ec0fc96cee1ff.png', 'Адрес банка', 'Свифт банка', '3333333', '53325354'),
(37, '', '', '32312312', '0', '', '0', '', '', 'test', '', '', 'test', 'test', '234234', '');

-- --------------------------------------------------------

--
-- Структура таблицы `juristic_type`
--

CREATE TABLE `juristic_type` (
  `id` int(255) NOT NULL,
  `type` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `juristic_type`
--

INSERT INTO `juristic_type` (`id`, `type`) VALUES
(1, 'ИП'),
(2, 'ООО');

-- --------------------------------------------------------

--
-- Структура таблицы `message`
--

CREATE TABLE `message` (
  `id` int(255) NOT NULL,
  `user_id` int(255) NOT NULL,
  `created_at` bigint(255) NOT NULL,
  `read_at` bigint(255) NOT NULL,
  `dialog_id` int(255) NOT NULL,
  `text` text NOT NULL,
  `attachment` varchar(255) NOT NULL,
  `status` int(2) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `message`
--

INSERT INTO `message` (`id`, `user_id`, `created_at`, `read_at`, `dialog_id`, `text`, `attachment`, `status`) VALUES
(1, 23, 1540557384, 1541055234, 1, 'test2', '', 2),
(2, 33, 1540557418, 1541055234, 1, 'test2', '', 2),
(3, 33, 1540557451, 1541055234, 1, '412421', '', 2),
(4, 23, 1540557459, 1541055234, 1, '123123321', '', 2),
(5, 33, 1540557466, 1541055234, 1, '124112', '', 2),
(6, 33, 1540557468, 1541055234, 1, 'test6', '', 2),
(7, 23, 1540990884, 1541055234, 1, '1', '', 2),
(8, 23, 1540990910, 1541055234, 1, '1', '', 2),
(9, 23, 1540991013, 1541055234, 1, '1', '', 2),
(10, 23, 1540991045, 1541055234, 1, 'sdafsadfsdsadf', '', 2),
(11, 23, 1540991178, 1541055234, 1, 'fffffffff', '', 2),
(12, 23, 1540991287, 1541055234, 1, 'adssddsaasd', '', 2),
(13, 23, 1540991874, 1541055234, 1, 'ddddddddddd', '', 2),
(14, 33, 1540992419, 1541055234, 1, '333123123', '', 2),
(15, 33, 1540992430, 1541055234, 1, '32213', '', 2),
(16, 23, 1540992439, 1541055234, 1, 'test message\n', '', 2),
(17, 23, 1540992466, 1541055234, 1, 'sadgdsadgsadg', '', 2),
(18, 23, 1540992573, 1541055234, 1, 'dddddddd', '', 2),
(19, 33, 1540992579, 1541055234, 1, 'sdafasdfasdf', '', 2),
(20, 23, 1540992595, 1541055234, 1, 'asdfsadfasd', '', 2),
(21, 33, 1540992605, 1541055234, 1, 'dddddddddd', '', 2),
(22, 33, 1540992670, 1541055234, 1, 'aaaaaaaaaaa', '', 2),
(23, 23, 1540992895, 1541055234, 1, 'qwerqwer', '', 2),
(24, 23, 1540993019, 1541055234, 1, 'sadas', '', 2),
(25, 23, 1540993505, 1541055234, 1, '111111111111', '', 2),
(26, 33, 1540993561, 1541055234, 1, '32213321321', '', 2),
(27, 33, 1540993617, 1541055234, 1, '321321', '', 2),
(28, 33, 1540993672, 1541055234, 1, 'ывфыыфвыфвыфв', '', 2),
(29, 33, 1540993707, 1541055234, 1, 'выффыв', '', 2),
(30, 33, 1540993765, 1541055234, 1, 'еыфефеыуеф', '', 2),
(31, 33, 1540993848, 1541055234, 1, '23414242112', '', 2),
(32, 33, 1540993890, 1541055234, 1, '3213231123231', '', 2),
(33, 33, 1540994057, 1541055234, 1, 'ыавыывфыавфавыыавф', '', 2),
(34, 33, 1540994078, 1541055234, 1, 'ывпфпывфрыфврывфр', '', 2),
(35, 33, 1540994089, 1541055234, 1, 'пппппппппппппп', '', 2),
(36, 33, 1540994132, 1541055234, 1, '2345423452345', '', 2),
(37, 23, 1540994162, 1541055234, 1, 'sadasadsdsa', '', 2),
(38, 33, 1540994177, 1541055234, 1, 'test', '', 2),
(39, 23, 1540994190, 1541055234, 1, '12312123', '', 2),
(40, 23, 1540994259, 1541055234, 1, '21312', '', 2),
(41, 33, 1540994268, 1541055234, 1, 'sadasd', '', 2),
(42, 23, 1540994279, 1541055234, 1, 'lol', '', 2),
(43, 33, 1540994286, 1541055234, 1, 'lol', '', 2),
(44, 33, 1541054150, 1541055234, 1, '132123', '', 2),
(45, 23, 1541054884, 1541055234, 1, '32434214231', '', 2),
(46, 23, 1541055066, 1541055234, 1, 'sdasdasad', '', 2),
(47, 23, 1541055301, 1541055302, 1, '3123123', '', 2),
(48, 33, 1541055566, 1541055567, 1, 'sdafasdf', '', 2),
(49, 23, 1541055744, 1541056243, 1, '132321', '', 2),
(50, 33, 1541055776, 1541056243, 1, '12342', '', 2),
(51, 23, 1541055789, 1541056243, 1, '123123', '', 2),
(52, 33, 1541055794, 1541056243, 1, '123123', '', 2),
(53, 23, 1541055861, 1541056243, 1, '132123', '', 2),
(54, 33, 1541055868, 1541056243, 1, '321123', '', 2),
(55, 23, 1541055931, 1541056243, 1, '32452345', '', 2),
(56, 23, 1541056148, 1541056243, 1, '32452345', '', 2),
(57, 33, 1541056151, 1541056243, 1, '24352345', '', 2),
(58, 33, 1541056253, 1541056254, 1, '21341234', '', 2),
(59, 23, 1541056261, 1541056262, 1, '12341234', '', 2),
(60, 33, 1541056280, 1541056280, 1, '12341234', '', 2),
(61, 33, 1541056378, 1541056379, 1, '31231231231', '', 2),
(62, 33, 1541056436, 1541056437, 1, '2134', '', 2),
(63, 23, 1541056446, 1541056448, 1, '23452345', '', 2),
(64, 23, 1541056728, 1541056728, 1, '12341234', '', 2),
(65, 33, 1541056740, 1541056741, 1, '12341234', '', 2),
(66, 23, 1541056845, 1541056857, 1, '12341234', '', 2),
(67, 23, 1541056916, 1541056917, 1, '1234123', '', 2),
(68, 33, 1541056923, 1541056924, 1, '23452345', '', 2),
(69, 23, 1541056938, 1541056951, 1, 'sgsdfgsdfg', '', 2),
(70, 33, 1541062749, 1541063910, 1, '123', '', 2),
(71, 33, 1541062876, 1541063910, 1, '44444', '', 2),
(72, 33, 1541062957, 1541063910, 1, '555555', '', 2),
(73, 33, 1541063088, 1541063910, 1, '6666', '', 2),
(74, 33, 1541063863, 1541063910, 1, 'test', '', 2),
(75, 33, 1541063883, 1541063910, 1, '616631631261326213', '', 2),
(76, 33, 1541063907, 1541063910, 1, '11111111', '', 2),
(77, 23, 1541063918, 1541063918, 1, '412421', '', 2),
(78, 33, 1541064015, 1541067078, 1, '63246234', '', 2),
(79, 33, 1541064233, 1541067078, 1, '44444', '', 2),
(80, 33, 1541064335, 1541067078, 1, '123', '', 2),
(81, 33, 1541064365, 1541067078, 1, '44441421421412', '', 2),
(82, 33, 1541064405, 1541067078, 1, 'dsasdgasd', '', 2),
(83, 33, 1541064451, 1541067078, 1, '4112441212', '', 2),
(84, 33, 1541064509, 1541067078, 1, 'ssdasfdasdfasd', '', 2),
(85, 33, 1541064597, 1541067078, 1, 'rrrrrr', '', 2),
(86, 33, 1541064711, 1541067078, 1, '1424124', '', 2),
(87, 33, 1541064781, 1541067078, 1, '34563456', '', 2),
(88, 33, 1541064872, 1541067078, 1, '666666', '', 2),
(89, 33, 1541064937, 1541067078, 1, '11111', '', 2),
(90, 33, 1541064971, 1541067078, 1, '222222', '', 2),
(91, 33, 1541064988, 1541067078, 1, '333333', '', 2),
(92, 33, 1541065033, 1541067078, 1, '44444', '', 2),
(93, 33, 1541065072, 1541067078, 1, '555555', '', 2),
(94, 33, 1541065105, 1541067078, 1, '444444', '', 2),
(95, 33, 1541066042, 1541067078, 1, 'fffffffff', '', 2),
(96, 33, 1541066062, 1541067078, 1, 'wwwwwww', '', 2),
(97, 33, 1541066377, 1541067078, 1, 'ttttttt', '', 2),
(102, 33, 1541072740, 1541075395, 1, '546345', '', 2),
(103, 33, 1541072783, 1541075395, 1, '!!!!!!!!!!!!!!!!!!!!', '', 2),
(104, 23, 1541072812, 0, 24, 'rrrrrrrrrrr', '', 1),
(105, 33, 1541074887, 1541075395, 1, '2343223', '', 2),
(106, 33, 1541074915, 1541075395, 1, 'fffffff', '', 2),
(107, 33, 1541075526, 1541075542, 1, '55555', '', 2),
(108, 33, 1541075567, 1541075575, 1, '44444', '', 2),
(109, 33, 1541075690, 1541076114, 1, '523452345', '', 2),
(110, 33, 1541075861, 1541076114, 1, 'dfsasdf\n', '', 2),
(111, 33, 1541075862, 1541076114, 1, 'asdfasdf\n', '', 2),
(112, 33, 1541075863, 1541076114, 1, 'asdfasdgasdg\n', '', 2),
(113, 33, 1541075864, 1541076114, 1, 'asdgasdgasdgasdg\n', '', 2),
(114, 33, 1541076053, 1541076114, 1, '436435\n', '', 2),
(115, 33, 1541076054, 1541076114, 1, '34563456345\n', '', 2),
(116, 33, 1541076185, 1541076214, 1, 'shdsdfhhsfd\n', '', 2),
(117, 33, 1541076186, 1541076214, 1, 'sdfhhfhdfs\n', '', 2),
(118, 33, 1541076187, 1541076214, 1, 'fhsdsdfhsdf\n', '', 2),
(119, 33, 1541076190, 1541076214, 1, 'sdfhsdfh\n', '', 2),
(120, 33, 1541076374, 1541076402, 1, 'sgdfdsfg\n', '', 2),
(121, 33, 1541076376, 1541076402, 1, 'sdfgsdfg\n', '', 2),
(122, 33, 1541076377, 1541076402, 1, 'sdfgsdfg\n', '', 2),
(123, 33, 1541076381, 1541076402, 1, '1\n', '', 2),
(124, 33, 1541076382, 1541076402, 1, '2\n', '', 2),
(125, 33, 1541076383, 1541076402, 1, '3\n', '', 2),
(126, 23, 1541076408, 1541076411, 1, 'dsfasdf\n', '', 2),
(127, 23, 1541076409, 1541076411, 1, 'asdfasdf\n', '', 2),
(128, 23, 1541076411, 1541076413, 1, 'gagasdgasdg\n', '', 2),
(129, 23, 1541076412, 1541076413, 1, 'asdgsadgsadg\n', '', 2),
(130, 23, 1541076413, 1541076415, 1, 'sagdasdgasdg\n', '', 2),
(131, 23, 1541076424, 1541076498, 1, 'fdfsdfsfddf\n', '', 2),
(132, 23, 1541076425, 1541076498, 1, 'fdfsdfsfddf\nsdfgsdfgsdf\n', '', 2),
(133, 23, 1541076426, 1541076498, 1, 'sdfgsdfgsdfg\n', '', 2),
(134, 23, 1541076427, 1541076498, 1, 'sdfgsdfg\n', '', 2),
(135, 23, 1541076427, 1541076498, 1, '\n', '', 2),
(136, 33, 1541076500, 1541076500, 1, 'fdhfdgh\n', '', 2),
(137, 33, 1541076510, 1541076511, 1, '            ', '', 2),
(138, 33, 1541076514, 1541076515, 1, 'erwyewryerwy\n', '', 2),
(139, 33, 1541076530, 1541076577, 1, 'weryweryw', '', 2),
(140, 33, 1541076547, 1541076577, 1, '1111111111111\n', '', 2),
(141, 33, 1541076647, 0, 25, 'erwtwertwer', '', 1),
(142, 33, 1541076649, 0, 25, 'werywery', '', 1),
(143, 23, 1541158401, 1541158411, 1, 'test', '', 2),
(144, 23, 1541413485, 1541413578, 1, '1', '', 2),
(145, 23, 1541413588, 1541413592, 1, '123\r\n', '', 2),
(146, 33, 1541414840, 1541414891, 1, '1111', '', 2),
(147, 33, 1541414911, 1541414925, 1, '1122', '', 2),
(148, 23, 1541414944, 1541414947, 1, '111', '', 2),
(149, 23, 1541414958, 1541414964, 1, '123', '', 2),
(150, 23, 1541414978, 1542091462, 1, '222', '', 2),
(151, 96, 1543400423, 0, 26, 'test', '', 1),
(152, 33, 1543401343, 1543401347, 33, 'Help\n', '', 2),
(153, 1, 1543401366, 1543401370, 33, 'ok\n', '', 2),
(154, 1, 1543402550, 1543402554, 33, '!!!!!', '', 2),
(155, 33, 1543406180, 1543406185, 33, '++++++\n', '', 2);

-- --------------------------------------------------------

--
-- Структура таблицы `news`
--

CREATE TABLE `news` (
  `id` int(255) NOT NULL,
  `category_id` int(255) NOT NULL,
  `alias` varchar(255) NOT NULL,
  `name` varchar(1024) NOT NULL,
  `short_description` text NOT NULL,
  `description` text NOT NULL,
  `image` text NOT NULL,
  `created_at` int(60) NOT NULL,
  `updated_at` int(60) NOT NULL,
  `active` int(1) NOT NULL DEFAULT '0',
  `sort` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `news`
--

INSERT INTO `news` (`id`, `category_id`, `alias`, `name`, `short_description`, `description`, `image`, `created_at`, `updated_at`, `active`, `sort`) VALUES
(2, 1, 'test2', 'Пиццерия из Бостона добавит в меню блюда, созданные ИИ', 'Искусственный интеллект применяется уже во многих сферах начиная мелкими расчетами и заканчивая разработками в сфере медицины и исследования космоса.', 'Искусственный интеллект применяется уже во многих сферах начиная мелкими расчетами и заканчивая разработками в сфере медицины и исследования космоса. И недавно группа исследователей из Массачусетского технологического института обучила ИИ разрабатывать новые рецепты для приготовления пиццы. И этот ИИ хотят «нанять» на работу в одну из пиццерий Бостона.\r\nА ходе исследований новым трюкам обучали нейросеть с открытым исходным кодом textgnrnn, которая изначально нацелена на генерирование фраз на определенную тематику на основе имеющихся текстов. В данном случае использовались рецепты пиццы из книг и кулинарных блогов, которые были предоставлены «на растерзание» нейросети. Проанализировав информацию, textgnrnn создал собственные уникальные сочетания продуктов и представил новые блюда.\r\nПосле этого проверить получившиеся рецепты доверили поварам пиццерии Crush Pizza. В итоге не все рецепты получились съедобными, а некоторые и вовсе не могли существовать в реальности. Как вам, например, такие ингрииенты, как нарезанный карамельный сыр и повязка из грецкого ореха? Помимо этого, ИИ-повар иногда забывал добавить соусы. Но были и удачные варианты, которые можно, скорее, отнести к кухне в стиле фьюжн (сочетание, например, шпината, персика и клюквы в пицце многим может показаться странным), но они оказались вполне съедобны и вкусны.\r\nЛучше всего у ИИ получилось, по мнению специалистов Crush Pizza, сочетание креветок, джема и итальянских сосисок. Эту пиццу хотят включить в основное меню, как и некоторые другие, слегка доведенные до ума местными поварами. Если вас заинтересовала эта тема – ниже вы можете посмотреть подробный ролик о первой пицце, созданной ИИ.', '/images/2a77b2b1e6517125db6f130e87dd02e8.jpg', 1542661200, 1542661200, 1, 3),
(3, 1, 'test3', 'В прошивку MIUI 10 Xiaomi добавили функцию очистки WhatsApp', 'В прошивку Miui 10 компания Xiaomi внесла изменения, добавив функцию по очистке WhatsApp.', 'Смартфоны китайской компании получили полезную функцию, считают эксперты. Приложение WhatsApp Cleaner встроено в программное обеспечение. Данное ПО выполняет функцию по очистке самого популярного в мире мессенджера от привязанных к нему файлов и позволяет избежать перегруженности встроенной памяти. Видеоролики, фотоснимки, документы, голосовые сообщения, анимация иногда занимают слишком много пространства. Очистка в рамках WhatsApp Cleaner происходит в совместном режиме. Нельзя удалить отдельно видеоролики и анимацию. Стереть нужно всё сразу, оставляя лишь переписку, либо вообще ничего. \r\n\r\nПользователи имеют возможность выбора конкретной категории для удаления (видеосюжеты или фотоснимки). Они исчезают из всех бесед, освобождая место на накопителе. Функция присутствует в смартфонах Xiaomi, их вскоре обновят до самой новой версии прошивки MIUI 10 Global Beta ROM. ', '/images/cd26ebff18a271a974aae490b63b2877.jpg', 1532034000, 1532034000, 1, 4),
(4, 2, 'test4', 'Москву назвали самым эффективным регионом контрактной системы России', 'Москва – один из максимально эффективных регионов контрактной системы страны. Такое заявление прозвучало из уст аудитора Счётной палаты РФ Максима Рохмистрова на сессии «Цифровые государственные закупки» Московского финансового форума.', 'Москва – один из максимально эффективных регионов контрактной системы страны. Такое заявление прозвучало из уст аудитора Счётной палаты РФ Максима Рохмистрова на сессии «Цифровые государственные закупки» Московского финансового форума.\r\n\r\n \r\n\r\n«Есть регионы, которые искренне переживают за происходящее в контрактной системе страны и активно создают новые инструменты, способствующие эффективные инструменты для повышения доступа к закупочным процедурам и развитию конкуренции. Это Москва и Татарстан», -сказал Максим Рохмистров.\r\n\r\nПерспективы рынка закупок аудитор Счётной палаты связывает со смарт-контрактами и считает дальновидными тех, кто начал работу в этом направлении.\r\n\r\n \r\n\r\nВызовом контрактной системе страны цифровизацию назвал руководитель Департамента города Москвы по конкурентной политике Геннадий Дёгтев.\r\n\r\n«Цифровизация сегодня – это вызов контрактной системе, главными направлениями которой являются эффективность, конкуренция и, соответственно, минимизация рисков, для того, чтобы реализовывать потребности города», - сказал Геннадий Дёгтев.\r\n\r\n \r\n\r\nЦифровизация позволяет столице эффективно выстраивать процесс межведомственной работы управления закупками, подчеркнул Геннадий Дёгтев.\r\n\r\n«С одной стороны, это автоматизированное сравнение цифровых показателей, с другой – минимизация нагрузки на профильных специалистов при подготовке и размещении процедуры. Московская Единая автоматизированная информационная система торгов (ЕАИСТ) обеспечивает необходимые стандарты в цифровом формате. Главное контрольное управление в постоянном режиме имеет возможность осуществлять контроль в упреждающем режиме, профилактируя нарушения», - сказал Геннадий Дёгтев.\r\n\r\n \r\n\r\nОсновным приоритетом он назвал стандартизацию закупок. Ранее московские заказчики применяли более 4 тыс. форм контрактов, сегодня в автоматизированном режиме можно осуществлять закупки по 130 комплектам типовой документации.\r\n\r\n \r\n\r\n«Реальные результаты стандартизации уже видны: среднее количество участников закупок на лот выросло с 2,8 участников в 2011 году до 4,9 участников по итогам 2017 года, сократилась доля закупок с единственным участником с 34 процентов в 2011 году до 11 процентов в 2017 году.\r\n\r\nСтандартизация облегчила работу контрактных служб – комплект документации формируется в электронном виде автоматически. Предельные цены позволяют уходить от разных элементов дополнительного сравнения характеристик, запросов коммерческих предложений от поставщиков, которые, на самом деле, не готовы выполнять эту задачу», - сказал Геннадий Дёгтев.\r\n\r\n \r\n\r\nОн считает, что предпринимательское сообщество позитивно воспринимает стандартизацию.\r\n\r\n \r\n\r\n«Благодаря стандартизированным правилам закупочного процесса, растет ответственность бизнеса и его желание участвовать именно в таких закупках», - уточнил Геннадий Дёгтев.\r\n\r\n \r\n\r\nСистемность через цифровизацию позволяет сделать работу на рынке закупок мегаполиса прозрачной и понятной, убеждён спикер.\r\n\r\n \r\n\r\n«Сквозная идеология контрактной системы на сегодняшний день требует, чтобы ты вошел в одной точке, неважно, на портале поставщиков, сайте «Бизнес-навигатор» Корпорации МСП или в ЕИС и мог пройти все интересующие разделы, найти необходимую информацию в 2-3 клика. которые возникают как сопровождение. Это чрезвычайно важно. Именно этот подход используется в столичной системе закупок и делает её максимально удобной и понятной», - сказал Геннадий Дёгтев.\r\n\r\n \r\n\r\nВ качестве примера он привёл портал поставщиков – интернет-платформу, созданную в столице как витрину городского заказа для осуществления закупок малого объёма заказчиками Москвы. Благодаря ей, рынок закупок столицы стал доступен и региональным предпринимателям.\r\n\r\n \r\n\r\n«Более 102 тысяч пользователей из всех регионов страны зарегистрированы на портале поставщиков. Только с начала года портал прирос 22 тысячи предпринимателями. Источниками роста стало проведение котировочных сессий, подключение региональных заказчиков, проведение обучающих мероприятий. Во многом благодаря порталу объём закупок у субъектов малого предпринимательства московскими заказчиками вырос за шесть лет в 10 раз»,- сказал Геннадий Дёгтев.\r\n\r\n \r\n\r\nМосковский финансовый форум — это уникальная площадка для профессиональной дискуссии, посвящённой вопросам финансово-экономической политики России. Миссия форума заключается в выработке решений, способствующих повышению конкурентоспособности российской экономики.\r\n\r\nОрганизаторами форума выступают Министерство финансов Российской Федерации и Правительство Москвы.', '/images/cdfd022aa579e5bd6a3ffc6674b96315.jpg', 1536699600, 1536699600, 1, 5),
(5, 4, 'test5', 'Что такое Lorem Ipsum?', 'Lorem Ipsum - это текст-&quot;рыба&quot;, часто используемый в печати и вэб-дизайне.', 'Lorem Ipsum - это текст-&quot;рыба&quot;, часто используемый в печати и вэб-дизайне. Lorem Ipsum является стандартной &quot;рыбой&quot; для текстов на латинице с начала XVI века. В то время некий безымянный печатник создал большую коллекцию размеров и форм шрифтов, используя Lorem Ipsum для распечатки образцов. Lorem Ipsum не только успешно пережил без заметных изменений пять веков, но и перешагнул в электронный дизайн. Его популяризации в новое время послужили публикация листов Letraset с образцами Lorem Ipsum в 60-х годах и, в более недавнее время, программы электронной вёрстки типа Aldus PageMaker, в шаблонах которых используется Lorem Ipsum.', '/images/ed4ef7197ed85c153d2aeab8550d529c.jpg', 1539291600, 1539291600, 1, 6),
(6, 4, 'nashe-otkritie', 'пора уже', 'мы скоро будем работать', 'Вот подходит к финалу разработка нашего сайта и мы скоро приступим к работе', '/images/e950ab4a1a1ba9c98c54d7bb6a998fcf.jpg', 1543525200, 1543525200, 1, 0),
(7, 2, 'test-news', '&quot;Вы никто и ничто&quot;. Как прибалты становятся бесправными рабами в Евросоюзе', 'Полицейские Латвии и Великобритании расследуют дело банды, торговавшей людьми. Подобные истории случались и раньше. Граждане Латвии, Литвы и Эстонии, отправляющиеся в другие страны Евросоюза на заработки, зачастую оказываются там в совершенно унизительном положении. РИА Новости пообщалось с теми, кто узнал это на собственном опыте.', 'Обещали хороший заработок\r\nНедавно в Латвии задержали 51-летнего гражданина, обвиняемого в торговле людьми в Великобритании и угрозе убийством. В рамках этого же дела осенью прошлого года в английском городе Дерби (графство Девоншир) арестовали пятерых латвийцев, еще четверых — в феврале. В ходе полицейской операции удалось вызволить десять человек. Они вроде бы работали официально — хоть и за очень небольшое вознаграждение. Однако на самом деле не имели доступа к банковским счетам, открытым на их имена. Людей сдавали внаем одному из местных предприятий. Если они требовали свои деньги — запугивали.\r\n\r\nМошенники подбирали жертв в Риге и окрестностях, в городах Тукумсе и Юрмале. Наивным прибалтам обещали в Англии хорошие заработки и подсовывали на подпись испещренные мелким шрифтом бумаги, которые никто не удосуживался детально изучить. &quot;Но они себя потерпевшими признавать не хотят. Это такой ментальный феномен. Они считают: лучше быть обокраденными и обманутыми, одураченными, но не признать, что находились в рабстве&quot;, — поясняет представитель полиции Латвии Армандс Лубартс.\r\n\r\n&quot;Вы никто и ничто. Вы будете плакать&quot;\r\nКогда уроженке Даугавпилса Ольге Раньшевой знакомые предложили поехать на грибную ферму в Ирландию, она долго сомневалась — не хотелось оставлять дочь на попечении бабушки. &quot;И тут как гром среди ясного неба — диагноз &quot;рак&quot; моей маме. И, конечно, я понимаю, что на лечение понадобятся деньги, соглашаюсь на предложение, покупаю билет на самолет. Диагноз позже не подтвердился, но я же этого не могла предугадать&quot;, — вспоминает Ольга в разговоре с РИА Новости.', '/images/f68d681530ae9424b1461e191cb19863.png', 1543525200, 1543525200, 1, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `news_category`
--

CREATE TABLE `news_category` (
  `id` int(255) NOT NULL,
  `name` text NOT NULL,
  `alias` varchar(255) NOT NULL,
  `sort` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `news_category`
--

INSERT INTO `news_category` (`id`, `name`, `alias`, `sort`) VALUES
(1, 'Агентам', 'for_sellers', 0),
(2, 'Поставщикам', 'for_suppliers', 0),
(3, 'Покупателям', 'for_buyers', 0),
(4, 'Новости компании', 'company_news', 0),
(12, 'qwe', '', 2);

-- --------------------------------------------------------

--
-- Структура таблицы `order_address`
--

CREATE TABLE `order_address` (
  `id` int(255) NOT NULL,
  `order_id` int(255) NOT NULL,
  `delivery_name` varchar(255) NOT NULL,
  `delivery_last_name` varchar(255) NOT NULL,
  `patronymic` varchar(255) NOT NULL,
  `country` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `region` varchar(255) NOT NULL,
  `street` varchar(255) NOT NULL,
  `house_number` int(255) NOT NULL,
  `apartment` int(255) NOT NULL,
  `post_index` int(255) NOT NULL,
  `phone` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `order_product`
--

CREATE TABLE `order_product` (
  `id` int(255) NOT NULL,
  `order_id` int(255) NOT NULL,
  `product_id` int(255) NOT NULL,
  `buyer_id` int(255) NOT NULL,
  `count` int(255) NOT NULL,
  `shop_id` int(11) NOT NULL,
  `properties` text NOT NULL,
  `buy_price` decimal(65,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `order_product`
--

INSERT INTO `order_product` (`id`, `order_id`, `product_id`, `buyer_id`, `count`, `shop_id`, `properties`, `buy_price`) VALUES
(207, 240, 43, 96, 1, 0, 'a:5:{i:1;s:4:\"1634\";i:2;s:4:\"1635\";i:4;s:4:\"1637\";i:9;s:4:\"1641\";i:10;s:4:\"1644\";}', '2000.00'),
(208, 240, 29, 96, 1, 0, 'a:5:{i:1;s:4:\"1489\";i:2;s:4:\"1492\";i:4;s:4:\"1493\";i:9;s:4:\"1499\";i:10;s:4:\"1505\";}', '22000.00'),
(209, 241, 29, 0, 1, 0, 'a:5:{i:1;s:4:\"1488\";i:2;s:4:\"1491\";i:4;s:4:\"1493\";i:9;s:4:\"1499\";i:10;s:4:\"1500\";}', '22000.00'),
(210, 242, 321, 0, 1, 0, 'a:2:{i:19;s:4:\"4161\";i:20;s:4:\"4162\";}', '157170.00'),
(211, 243, 28, 0, 1, 0, 'a:0:{}', '20000.00'),
(212, 244, 346, 96, 1, 0, 'a:1:{i:20;s:4:\"3181\";}', '10.00'),
(213, 245, 28, 0, 1, 0, 'a:0:{}', '20000.00'),
(214, 247, 321, 0, 1, 0, 'a:0:{}', '157170.00'),
(215, 248, 28, 0, 1, 0, 'a:0:{}', '20000.00'),
(216, 248, 30, 0, 1, 0, 'a:0:{}', '10990.00'),
(217, 250, 403, 96, 1, 0, 'a:0:{}', '1500.00'),
(218, 251, 402, 96, 1, 0, 'a:0:{}', '23000.00'),
(219, 253, 402, 96, 1, 0, 'a:0:{}', '23000.00'),
(220, 254, 404, 96, 1, 0, 'a:0:{}', '1600.00'),
(221, 255, 402, 96, 1, 0, 'a:0:{}', '23000.00'),
(222, 255, 404, 96, 1, 0, 'a:0:{}', '1600.00');

-- --------------------------------------------------------

--
-- Структура таблицы `order_status`
--

CREATE TABLE `order_status` (
  `id` int(255) NOT NULL,
  `name` varchar(1024) NOT NULL,
  `active` int(255) NOT NULL DEFAULT '1',
  `color` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `order_status`
--

INSERT INTO `order_status` (`id`, `name`, `active`, `color`) VALUES
(1, 'Новый заказ', 1, '0:0:255:255'),
(2, 'Доставляется', 1, '255:255:0:255'),
(3, 'Выполнен', 1, '0:255:0:255'),
(4, 'Отказано', 1, '255:0:0:255');

-- --------------------------------------------------------

--
-- Структура таблицы `pay_card`
--

CREATE TABLE `pay_card` (
  `id` int(255) NOT NULL,
  `user_id` int(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `name` varchar(100) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  `card_num` bigint(40) NOT NULL,
  `month` int(2) NOT NULL,
  `year` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `pay_card`
--

INSERT INTO `pay_card` (`id`, `user_id`, `description`, `name`, `last_name`, `card_num`, `month`, `year`) VALUES
(7, 23, '', '', '', 5555555555555555, 0, 0),
(8, 23, '', '', '', 2222222222222222, 0, 0),
(9, 23, '', '', '', 4344444444444444, 0, 0),
(10, 33, '', '', '', 1111111111111111, 0, 0),
(11, 33, '', '', '', 2222222222222222, 0, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `pay_status`
--

CREATE TABLE `pay_status` (
  `id` int(255) NOT NULL,
  `type` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `pay_status`
--

INSERT INTO `pay_status` (`id`, `type`) VALUES
(1, 'Ожидает обработки'),
(2, 'Обрабатывается'),
(3, 'Завершено'),
(4, 'Отклонено');

-- --------------------------------------------------------

--
-- Структура таблицы `pay_type`
--

CREATE TABLE `pay_type` (
  `id` int(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `agent_only` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `pay_type`
--

INSERT INTO `pay_type` (`id`, `type`, `agent_only`) VALUES
(1, 'Наличными курьеру', 0),
(2, 'Безналичный расчет курьеру', 0),
(3, 'Оплатить со счета на сайте', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `price`
--

CREATE TABLE `price` (
  `id` int(11) NOT NULL,
  `price_agent` decimal(65,2) NOT NULL,
  `discount` decimal(65,2) NOT NULL,
  `active` int(1) NOT NULL,
  `product_id` int(11) NOT NULL,
  `agent_id` int(11) NOT NULL,
  `shop_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `price`
--

INSERT INTO `price` (`id`, `price_agent`, `discount`, `active`, `product_id`, `agent_id`, `shop_id`) VALUES
(3, '1700.00', '100.00', 1, 43, 23, 23),
(5, '20000.00', '1000.00', 1, 29, 23, 23),
(6, '2000000.00', '0.00', 0, 13, 23, 24);

-- --------------------------------------------------------

--
-- Структура таблицы `product`
--

CREATE TABLE `product` (
  `id` int(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `category_id` int(100) NOT NULL,
  `created_at` bigint(100) NOT NULL,
  `updated_at` bigint(100) NOT NULL,
  `status_id` int(2) NOT NULL,
  `sku` varchar(255) NOT NULL,
  `price` decimal(65,2) NOT NULL,
  `images` text NOT NULL,
  `description` text NOT NULL,
  `original` int(1) NOT NULL,
  `certificate` text NOT NULL,
  `owner_id` text NOT NULL,
  `warehouse_id` int(255) NOT NULL,
  `availability` int(1) NOT NULL,
  `sale_id` int(255) NOT NULL,
  `alias` varchar(255) NOT NULL,
  `meta_title` varchar(80) NOT NULL,
  `meta_description` varchar(185) NOT NULL,
  `meta_keywords` varchar(185) NOT NULL,
  `commission` int(15) NOT NULL,
  `type` int(1) NOT NULL DEFAULT '0',
  `purchase_price` decimal(65,2) NOT NULL,
  `agent_price` decimal(65,2) NOT NULL,
  `active` int(1) NOT NULL DEFAULT '1',
  `region_id` text NOT NULL,
  `old_price` decimal(65,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `product`
--

INSERT INTO `product` (`id`, `name`, `category_id`, `created_at`, `updated_at`, `status_id`, `sku`, `price`, `images`, `description`, `original`, `certificate`, `owner_id`, `warehouse_id`, `availability`, `sale_id`, `alias`, `meta_title`, `meta_description`, `meta_keywords`, `commission`, `type`, `purchase_price`, `agent_price`, `active`, `region_id`, `old_price`) VALUES
(13, 'Смартфон Apple iPhone X', 19, -10800, 1543845723, 0, '39571111', '80990.00', 'a:2:{i:0;s:44:\"/images/167c9d1510895090950085106f3365b7.jpg\";i:1;s:44:\"/images/9d09575f13b343c09aea7f05f444bb21.png\";}', 'Флагман от компании Apple 2017 года. Задняя крышка выполнена из стекла, по бокам &quot;хирургическая&quot; нержавеющая сталь. Новый OLED-дисплей получил название Super Retina, его диагональ составляет 5,8 дюйма, разрешение — 2436x1125 точек (458 ppi), реализована поддержка технологий True Tone и 3D Touch. Вместо дактилоскопического датчика теперь используется технология Face ID — система распознавания лица для разблокировки смартфона. Аппарат получил продвинутую поддержку дополненной реальности, а также стереодинамики.', 0, 'a:0:{}', 'a:3:{i:0;s:2:\"47\";i:1;s:2:\"22\";i:2;s:2:\"50\";}', 0, 0, 4, 'apple-iphone-x', 'qwe', 'qwe', 'qwe', 0, 1, '1000.00', '2000.00', 1, 'a:1:{i:0;s:1:\"1\";}', '80990.00'),
(18, 'Пылесос Samsung VC07H8150HQ/GE', 3, 101, 1537275380, 0, '32423', '69990.00', 'a:1:{i:0;s:44:\"/images/d2e51c92ed344eae01b034044c95235b.jpg\";}', 'с контейнером, пылесборник: контейнер, потребление: 700 Вт, регулировка мощности на ручке, шум 79 дБ, турбощетка', 0, 'a:0:{}', 'a:3:{i:0;s:2:\"47\";i:1;s:2:\"22\";i:2;s:2:\"50\";}', 0, 0, 0, 'tews', 'ывфывфвыф', 'выфывфывф', 'ывфывфывф', 0, 0, '0.00', '0.00', 1, '', '69990.00'),
(19, 'Геймпад Sony Dualshock 4 Wireless Controller', 27, 1209, 1542982350, 0, '3213', '3000.00', 'a:1:{i:0;s:44:\"/images/6ed6b044353f92f97e899f618b03357d.jpg\";}', 'для Sony PlayStation 4, 13 кнопок, 2 аналоговых стика, обратная связь, беспроводной', 0, 'a:0:{}', 'a:3:{i:0;s:2:\"47\";i:1;s:2:\"22\";i:2;s:2:\"50\";}', 0, 0, 0, 'adksandk', '', '', '', 0, 0, '0.00', '0.00', 1, '', '3000.00'),
(20, 'Ремонт', 3, 101, 1536825387, 0, 'asdsaddsasda', '1232321213.00', 'a:1:{i:0;s:44:\"/images/91f669250589bad58bfe955904e37a12.jpg\";}', 'asdfasdfsdfadsfdsfafsadsdfa', 1, 'a:0:{}', 'a:3:{i:0;s:2:\"47\";i:1;s:2:\"22\";i:2;s:2:\"50\";}', 1, 0, 0, 'test123123123', 'dsadsa', 'asdsadsad', 'dsadsasdadsa', 2, 1, '0.00', '0.00', 1, '', '1232321213.00'),
(21, 'Экшен-камера GoPro HERO5 Black', 14, 1209, 1542982437, 0, '7543', '75990.00', 'a:1:{i:0;s:44:\"/images/2c1159158f0ffe2e313ea9f311fe47d3.jpg\";}', 'стандартная, 4K, матрица 1/2.3&quot; (видео 12 Мп), Timelapse, GPS', 0, 'a:0:{}', 'a:3:{i:0;s:2:\"47\";i:1;s:2:\"22\";i:2;s:2:\"50\";}', 0, 0, 0, 'asdnsaidsa', '', '', '', 0, 0, '0.00', '0.00', 1, '', '75990.00'),
(26, 'sfdasdfa', 1, 1536745605, 1536745605, 0, 'asdfasdfdf', '3241234.00', 'a:3:{i:0;s:52:\"/images/product/889e3bdfa0137d45c6ed789b5019bfd9.jpg\";i:1;s:52:\"/images/product/301ac0d1c63db067b2f1b3a36938a1c5.jpg\";i:2;s:52:\"/images/product/c73e80d533637c19f4f5dd2239b2eb8a.jpg\";}', 'dsfasdfasdf', 0, '', 'a:3:{i:0;s:2:\"47\";i:1;s:2:\"22\";i:2;s:2:\"50\";}', 1, 0, 0, 'safdsfadsafd', 'sfdsadsafdsfad', 'sdafsdafsfda', 'sadfsadf', 0, 0, '0.00', '0.00', 1, '', '3241234.00'),
(27, 'Bosch GSR 10.8-2-Li', 3, 1209, 1536759860, 0, '6534', '7999.00', 'a:1:{i:0;s:44:\"/images/311ddb7db8cc6575c747cd4f6e446c50.jpg\";}', '', 0, 'a:0:{}', 'a:3:{i:0;s:2:\"47\";i:1;s:2:\"22\";i:2;s:2:\"50\";}', 1, 0, 0, 'dsadkjngf', '', '', '', 0, 0, '0.00', '0.00', 1, '', '7999.00'),
(28, 'Телевизор LG 32LN570V', 13, -10800, 1543932703, 0, '4324', '20000.00', 'a:1:{i:0;s:44:\"/images/0c50d434f17f0cd5dd8250c59452cb88.jpg\";}', '', 0, 'a:0:{}', 'a:2:{i:0;s:2:\"47\";i:1;s:2:\"22\";}', 0, 0, 0, 'adfadsadvcv', '', '', '', 0, 0, '13000.00', '15000.00', 1, '', '20000.00'),
(29, ' Ноутбук HP Pavilion 15-cc008ur (2CP09EA)', 19, -10800, 1543575839, 0, '214324', '22000.00', 'a:1:{i:0;s:44:\"/images/bf2e1d378411974ff3383f40552f81e8.jpg\";}', '', 0, 'a:0:{}', 'a:3:{i:0;s:2:\"47\";i:1;s:2:\"22\";i:2;s:2:\"50\";}', 0, 0, 0, 'notebook', '', '', '', 0, 0, '15000.00', '17000.00', 1, '', '22000.00'),
(30, 'Планшет Huawei Mediapad T2 7.0', 20, -10800, 1543572972, 0, '324324', '10990.00', 'a:1:{i:0;s:44:\"/images/3cad3e4bc6861696a71ee257a4179a1c.jpg\";}', 'Планшет работает в сетях, которые подробно описаны в спецификациях в Интернете. Хорошо проверьте сети, в которых вы хотите. С момента, когда министерство TIC присудило операторам 4G операторам, он назначил Claro, Direct TV и UNE полосу 2500-2600 МГц и Tigo-ETB, Movistar и Avantel полосой 1700 МГц. Для Colombia 4G команда должна указать который поддерживает: LTE 2,500 2600 МГц или 2,5 2,6 ГГц или полосу 7, также известную как азиатская полоса (сеть Claro, Direc TV и UNE) и 1700 МГц или AWS или полоса 4, также известная как американский диапазон (сеть Movistar) , Tigo-ETB и Avantel). ', 0, 'a:0:{}', 'a:3:{i:0;s:2:\"47\";i:1;s:2:\"22\";i:2;s:2:\"50\";}', 0, 0, 0, 'djmvgkfns', '', '', '', 0, 1, '5000.00', '10005.00', 1, '', '10990.00'),
(31, 'Камера Hikvision DS-2CD2742FWD-IS цветная', 77, 1209, 1536759890, 0, '2134', '20000.00', 'a:1:{i:0;s:44:\"/images/25e1dbc9f98365bb8f646a33d9b3df1b.jpg\";}', 'Основные\r\nКонструкция: купольная\r\nТип матрицы: CMOS\r\nДень/ночь: \r\nПрименение: уличная\r\nФизический размер матрицы: 1/3&quot;\r\nСетевой интерфейс: проводной\r\nИК-подсветка: 20 м\r\nАвтономная запись видео: на карту памяти, на сетевой накопитель\r\nПоддержка 3GPP: \r\nИнтерфейсы\r\nМикрофонный вход: \r\nЛинейный аудиовход: \r\nЛинейный аудиовыход: \r\nКомпозитный видеовыход: \r\nPoE: \r\nRS-485: \r\nТревожные входы: 1\r\nРелейные выходы: 1\r\nEthernet: 100 Мбит/с\r\nРазмеры и вес\r\nШирина: 140 мм\r\nВысота: 99.9 мм\r\nВес: 1000 г\r\nДлина: 140 мм', 0, 'a:0:{}', 'a:3:{i:0;s:2:\"47\";i:1;s:2:\"22\";i:2;s:2:\"50\";}', 1, 0, 0, 'bnjdmnvb', '', '', '', 0, 0, '0.00', '0.00', 1, '', '20000.00'),
(33, 'Электронная книга Digma E63S, Dark Gray', 20, 1536760149, 1542982433, 0, '4324', '15000.00', 'a:1:{i:0;s:44:\"/images/0027dbf133be02c1dcd577f4d6b18c8b.jpg\";}', 'Общая информация\r\nДата выхода на рынок: 2017 г.\r\nОсновные\r\nВстроенная камера: \r\nРазмер экрана: 6&quot;\r\nРазрешение экрана: 600 x 800\r\nФлэш-память: 4 ГБ\r\nТип экрана: монохромный\r\nТехнология экрана: E-Ink Carta\r\nМатериал корпуса: пластик\r\nЦвет: черный\r\nИнтерфейсы\r\nАудиовыход: \r\nВидеовыход: \r\n3G-модем: \r\nBluetooth: \r\nWi-Fi: \r\nUSB: \r\nРазмеры и вес\r\nШирина: 163 мм\r\nВес: 160 г\r\nДлина: 116 мм\r\nТолщина: 8.4 мм\r\nПитание\r\nТип аккумулятора: Li-pol\r\nЁмкость: 1 500 мА·ч', 0, 'a:0:{}', 'a:3:{i:0;s:2:\"47\";i:1;s:2:\"22\";i:2;s:2:\"50\";}', 0, 0, 0, 'vbndjsal', '', '', '', 0, 0, '0.00', '0.00', 1, '', '15000.00'),
(34, 'Стиральная машина Indesit IWSB 5085', 3, 1209, 1536760370, 0, '3245', '20990.00', 'a:1:{i:0;s:44:\"/images/886b5f79f9e2b2a0fed8327573f28527.jpg\";}', 'Основные\r\nЗагрузка белья: фронтальная\r\nМаксимальная загрузка: 5 кг\r\nМаксимальная скорость отжима: 800 об/мин\r\nКласс отжима: D\r\nКоличество программ: 16\r\nЦвет люка: белый\r\nТип: автоматическая стиральная машина\r\nЦвет: белый\r\nКласс энергопотребления: A\r\nИсполнение: свободностоящее\r\nПроизводительность и энергоэффективность\r\nКласс стирки: A\r\nРасход электроэнергии за цикл: 0.95 кВт*ч\r\nИнверторная технология: \r\nРасход воды за цикл: 43 л\r\nГодовой расход электроэнергии: 190 кВт·ч/год\r\nУправление и индикация\r\nПользовательские программы: \r\nТаймер отложенного старта: 12 ч\r\nИндикация ошибок: \r\nИндикация: светодиодная\r\nЗвуковой сигнал: \r\nБлокировка от детей: \r\nУправление: электронное\r\nГабариты\r\nГлубина с люком: 41.4 см\r\nШирина: 59.5 см\r\nГлубина: 40 см\r\nВысота: 85 см\r\nВес: 62.5 кг', 0, 'a:0:{}', 'a:3:{i:0;s:2:\"47\";i:1;s:2:\"22\";i:2;s:2:\"50\";}', 1, 0, 0, 'gnndkdkdk', '', '', '', 0, 0, '0.00', '0.00', 1, '', '20990.00'),
(35, 'Утюг Gorenje SIH2200GC', 3, 1536760580, 1536760580, 0, '34324', '3990.00', 'a:1:{i:0;s:44:\"/images/26350ac029f876cee9e7702ce364770b.jpg\";}', 'Основные\r\nЦвет корпуса: белый, серый\r\nБеспроводной утюг: \r\nТип утюга: с пароувлажнением\r\nПодошва: керамика\r\nМаксимальная мощность: 2 200 Вт\r\nРазмеры и вес\r\nВес утюга: 1.1 кг (брутто: 1.4 кг)\r\nШирина: 120 мм (в упаковке: 125 мм)\r\nВысота: 165 мм (в упаковке: 159 мм)\r\nДлина: 305 мм (в упаковке: 313 мм)\r\nКомплектация\r\nГладильная доска: \r\nНасадка для деликатных тканей: \r\nМерный стаканчик: \r\nКонструкция\r\nАвтоматическое сматывание шнура: \r\nСпрей: \r\nДлина сетевого шнура: 2 м\r\nОбъём резервуара для воды: 300 мл\r\nФункциональные особенности\r\nАвтоматический выбор температуры: \r\nАвтоматическое отключение: \r\nСистема защиты от накипи: \r\nДистанционное управление: \r\nПротивокапельная система: \r\nФункция самоочистки: ', 0, 'a:0:{}', 'a:3:{i:0;s:2:\"47\";i:1;s:2:\"22\";i:2;s:2:\"50\";}', 1, 0, 0, 'uvsdad', '', '', '', 0, 0, '0.00', '0.00', 1, '', '3990.00'),
(36, 'Пылесос PANDA X500 black', 3, 1536760772, 1536760772, 0, '4324', '2300.00', 'a:1:{i:0;s:44:\"/images/f59568e8bf3c24b39c496535aadb6a2f.jpg\";}', 'Общая информация\r\nДата выхода на рынок: 2012 г.\r\nОсновные\r\nЦвет корпуса: черный\r\nМощность: 135 Вт\r\nВремя уборки: 1 час 50 минут\r\nКонструкция\r\nУльтрафиолетовая лампа: \r\nОбъём пылесборника: 0.3 л\r\nШум\r\nУровень шума: 50 дБ\r\nГабариты\r\nШирина: 34 см\r\nГлубина: 34 см\r\nВысота: 9 см\r\nФункциональность\r\nПульт ДУ: \r\nПрограммирование графика уборки: \r\nРежим локальной уборки: \r\nНавигация\r\n&quot;Виртуальная стена&quot; в комплекте: 1\r\nАвтоматический возврат на базу: \r\nДатчики перепада высоты: ', 0, 'a:0:{}', 'a:3:{i:0;s:2:\"47\";i:1;s:2:\"22\";i:2;s:2:\"50\";}', 1, 0, 0, 'dnbvjfmnkd', '', '', '', 0, 0, '0.00', '0.00', 1, '', '2300.00'),
(37, 'Стиральная машина Gorenje W65Z03A/S', 0, 1536760948, 1536760948, 0, '3432', '25990.00', 'a:1:{i:0;s:44:\"/images/07722d75aa319bc493bd51bdb574373f.jpg\";}', 'Основные\r\nЦвет люка: черный\r\nТип: автоматическая стиральная машина\r\nЦвет: серый\r\nКласс энергопотребления: A+++\r\nИсполнение: свободностоящее\r\nЗагрузка белья: фронтальная\r\nМаксимальная загрузка: 6 кг\r\nМаксимальная скорость отжима: 1000 об/мин\r\nКласс отжима: C\r\nКоличество программ: 23 (+ создание собственных программ)\r\nПроизводительность и энергоэффективность\r\nКласс стирки: A\r\nРасход электроэнергии за цикл: 0.79 кВт*ч\r\nРасход воды за цикл: 49 л\r\nУправление и индикация\r\nТаймер отложенного старта: 24 ч\r\nИндикация ошибок: \r\nИндикация: цифровая\r\nЗвуковой сигнал: \r\nБлокировка от детей: \r\nУправление: электронное\r\nГабариты\r\nШирина: 60 см\r\nГлубина: 44 см\r\nВысота: 85 см\r\nВес: 61.5 кг\r\nПрограммные функции\r\nПредварительная стирка: \r\nДополнительное полоскание: \r\nСпортивные вещи и обувь: \r\nСамоочистка: \r\nОсобенности конструкции\r\nОбработка паром: \r\nПодсветка барабана: \r\nУровень шума при стирке: 56 дБ\r\nУровень шума при отжиме: 68 дБ\r\nЗащита от протечек: ', 0, 'a:0:{}', 'a:3:{i:0;s:2:\"47\";i:1;s:2:\"22\";i:2;s:2:\"50\";}', 1, 0, 0, 'mfkfasdkd', '', '', '', 0, 0, '0.00', '0.00', 1, '', '25990.00'),
(38, 'Лампа ЭРА NLED-421-3W-BK', 59, 1536761141, 1536761141, 0, '3213', '2500.00', 'a:1:{i:0;s:44:\"/images/f50b8c259978ea4120c5f6d7c0d49c19.jpg\";}', 'Основные\r\nТип: лампа\r\nПитание: аккумулятор (+ от сети)\r\nРасположение: настольное\r\nЦвет плафонов, абажура: черный\r\nЦвет каркаса: черный\r\nПульт дистанционного управления: \r\nДлина: 26 см\r\nШирина: 8 см\r\nВысота: 30 см\r\nМощность лампы: 3 Вт (мощность всей лампы)\r\nЦветовая температура: 3000 K\r\nТип ламп: светодиодные\r\nРегулировка яркости: (2 уровня)\r\nПовышенная влагозащищенность: \r\nДизайн и монтаж\r\nПоворотные лампы: \r\nДатчик движения: \r\nМатериал плафона: пластик\r\nРегулируемая высота: \r\nДизайн: современный\r\nМатериал каркаса: пластик', 0, 'a:0:{}', 'a:3:{i:0;s:2:\"47\";i:1;s:2:\"22\";i:2;s:2:\"50\";}', 1, 0, 0, 'ldgndjknffdsf', '', '', '', 0, 0, '0.00', '0.00', 1, '', '2500.00'),
(39, 'Видеоняня Motorola MBP853 Connect', 4, 1536761282, 1536761282, 0, '3245', '3000.00', 'a:1:{i:0;s:44:\"/images/32bf31fbdaf2af2163627daf4e65a2d8.jpg\";}', 'Технические характеристики\r\nТип: Видеоняня\r\nЧастота передачи: 2.4 ГГц ~ 2,48 ГГц\r\nДатчик изображения: Цветной CMOS\r\nЛинза: f 2,5mm, F 2,8\r\nИнфракрасные светодиоды: 8 штук', 0, 'a:0:{}', 'a:3:{i:0;s:2:\"47\";i:1;s:2:\"22\";i:2;s:2:\"50\";}', 1, 0, 0, 'sadsadvbcxhfg', '', '', '', 0, 0, '0.00', '0.00', 1, '', '3000.00'),
(41, 'Сковорода Tefal Flavour H1157774', 56, 1536761788, 1536761788, 0, '34324', '1800.00', 'a:1:{i:0;s:44:\"/images/2c18926a7531dce338d24652d7cc31ae.jpg\";}', 'Основные\r\nТип: ВОК\r\nАнтипригарное покрытие: фторопласт (Resist Plus)\r\nМатериал: алюминий\r\nРазмер: 26 см\r\nКрышка в комплекте: \r\nОсобенности конструкции\r\nРучка: бакелитовая\r\nСовместимость с конфорками: газ, электро, стеклокерамика\r\nСъемная ручка: \r\nИндикатор нагрева (термоспот): ', 0, 'a:0:{}', 'a:3:{i:0;s:2:\"47\";i:1;s:2:\"22\";i:2;s:2:\"50\";}', 1, 0, 0, 'bmdjkffmdksf', '', '', '', 0, 0, '0.00', '0.00', 1, '', '1800.00'),
(42, 'Беговая дорожка Sundays Fitness T2000D', 8, 1536761975, 1536761975, 0, '34324', '7990.00', 'a:1:{i:0;s:44:\"/images/6e38cb651e746db4a502f71560da4cf5.jpg\";}', 'Основные\r\nТип: электрическая\r\nШирина полотна: 41.5 см\r\nДлина полотна: 120 см\r\nРегулировка угла наклона полотна: ручная бесступенчатая (1 уровень)\r\nМаксимальная масса пользователя: 110 кг\r\nСкорость движения полотна: 0.8 — 12 км/ч\r\nМощность двигателя: 1.5 л.с.\r\nСкладная конструкция: \r\nПрограммные функции\r\nПрограммы тренировки: 15\r\nВозможность создания программ: \r\nОценка Body Fat: \r\nТренировка по времени: \r\nТренировка по расстоянию: \r\nПрограмма постоянного пульса: \r\nИнформационный блок\r\nДисплей: \r\nОтображаемая информация: время тренировки, пройденная дистанция, текущая скорость, расход калорий, пульс, угол наклона\r\nСканирующий режим: \r\nДополнительная информация\r\nСтеппер: \r\nУпоры для отжиманий: \r\nВентилятор: \r\nКлюч безопасности: \r\nДержатель: \r\nКомпенсатор неровности пола: \r\nТип питания: электрическая сеть\r\nВибромассажер: \r\nРазмеры и масса\r\nВысота: 160 см\r\nШирина: 70 см\r\nДлина: 125 см\r\nМасса: 60 кг', 0, 'a:0:{}', 'a:3:{i:0;s:2:\"47\";i:1;s:2:\"22\";i:2;s:2:\"50\";}', 1, 0, 0, 'bnmfgjfnkj', '', '', '', 0, 0, '0.00', '0.00', 1, '', '7990.00'),
(43, 'На этом товаре ведутся разработки не обращайте на него внимание', 19, 1538082000, 1543838783, 0, 'aaaaaaaaaaaaaa', '2000.00', 'a:9:{i:0;s:52:\"/images/product/84f344e15ac198c13a5aa89c27c94b85.jpg\";i:1;s:52:\"/images/product/548f20dde13c4d3ccba34b886147be4d.jpg\";i:2;s:52:\"/images/product/3532dce596cbc83944093b5f786e0995.jpg\";i:3;s:52:\"/images/product/b602a485b20839e4a9dcb7a9db596510.jpg\";i:4;s:52:\"/images/product/7ebcb495290ccb3c39be16cf346564e5.jpg\";i:5;s:52:\"/images/product/cab0839c4a4699c912c4ab6c73bd9e36.jpg\";i:6;s:52:\"/images/product/a9c20ce00683d207209099e5e1c9ab7e.jpg\";i:7;s:52:\"/images/product/f12908a1741eeb36537cd02a5c45c921.jpg\";i:8;s:52:\"/images/product/314dd0fb6e12d661ad215912604368c0.jpg\";}', 'description aaaaaaaaaaaaaa', 0, 'a:0:{}', 'a:3:{i:0;s:2:\"47\";i:1;s:2:\"22\";i:2;s:2:\"50\";}', 0, 11, 0, 'aaaaaaaaaaaaaa', 'aaaaaaaaaaaaaa', 'aaaaaaaaaaaaaa', 'aaaaaaaaaaaaaa', 0, 1, '1000.00', '1500.00', 1, 'a:2:{i:0;s:1:\"1\";i:1;s:1:\"2\";}', '2000.00'),
(73, 'test_product', 20, 1537431555, 1537431592, 0, '', '0.00', 'a:0:{}', '', 0, 'a:0:{}', 'a:3:{i:0;s:2:\"47\";i:1;s:2:\"22\";i:2;s:2:\"50\";}', 0, 0, 0, 'test_product', '', '', '', 0, 0, '0.00', '0.00', 1, '', '0.00'),
(74, 'test', 3, 2409, 1537784582, 0, '', '0.00', 'a:0:{}', '', 0, 'a:0:{}', 'a:3:{i:0;s:2:\"47\";i:1;s:2:\"22\";i:2;s:2:\"50\";}', 0, 6, 4, 'ghfggh', '', '', '', 0, 0, '0.00', '0.00', 1, '', '0.00'),
(86, 'sadsdasad2222222222222222222', 19, 1538946000, 1539003260, 0, 'asdsdaas', '1232.00', 'a:2:{i:0;s:20:\"/images/no_photo.png\";i:1;s:44:\"/images/b77627c6f8e9bed27afcf517daea2940.jpg\";}', 'safsfaasdfsadf', 1, 'a:0:{}', 'a:3:{i:0;s:2:\"47\";i:1;s:2:\"22\";i:2;s:2:\"50\";}', 0, 11, 3, 'fasagsdgdsgdagdsgsdagsasgasasg', 'asdgasdg', 'agdsadg', 'gsadgsadg', 0, 0, '0.00', '0.00', 1, '', '1232.00'),
(87, 'ddddddddddddddddddddddddddddddddddddd', 19, 1539003444, 1539003444, 0, '214324', '123231123.00', 'a:0:{}', 'fdsgdfsgdsfg', 1, 'a:0:{}', 'a:3:{i:0;s:2:\"47\";i:1;s:2:\"22\";i:2;s:2:\"50\";}', 0, 1, 3, 'sssssssssddddddddddddddddddddddddddddd', 'asdfasdfasdf', 'asdfasdf', 'sdfasdfa', 0, 0, '0.00', '0.00', 1, '', '123231123.00'),
(88, 'asfsadfsdasdasdfffdfdssdfsfdfsdsadfsdafsdaf', 19, 1539003755, 1539003755, 0, '212132123', '213123123.00', 'a:1:{i:0;s:44:\"/images/2939e4289770c853194e28d0f9b16a6c.jpg\";}', 'sdasadgg', 1, 'a:1:{i:0;s:44:\"/images/a567578b09ef4c951313013a81bdc8ee.png\";}', 'a:3:{i:0;s:2:\"47\";i:1;s:2:\"22\";i:2;s:2:\"50\";}', 0, 1, 4, 'fasfasdgsdgasadgasdgasdggsadsgdasda', 'sadsadgsadsdga', 'sdgagsdasdga', 'dsgasagdsdaggsda', 1, 0, '0.00', '0.00', 1, '', '213123123.00'),
(89, 'fasdsdfaasdfadsf', 19, 1539003818, 1539003818, 0, 'sadfasda', '213123.00', 'a:1:{i:0;s:44:\"/images/c5b57585e549227e7d92cd0344d25025.jpg\";}', 'sdafasdgsad', 1, 'a:1:{i:0;s:44:\"/images/8f9b5c83764d1e9bb28306f3dc5efce1.jpg\";}', 'a:3:{i:0;s:2:\"47\";i:1;s:2:\"22\";i:2;s:2:\"50\";}', 0, 1, 3, 'asdfgsagsadgsadgsgadsdagsd', 'gasdgsadg', 'sadgsda', 'asdgasdg', 1, 0, '0.00', '0.00', 1, '', '213123.00'),
(90, 'testtetesetset', 19, 1540216053, 1540216053, 0, 'sdfasd', '1500.00', 'a:0:{}', 'afsdfdsdasafdfa', 0, 'a:0:{}', 'a:3:{i:0;s:2:\"47\";i:1;s:2:\"22\";i:2;s:2:\"50\";}', 0, 1, 0, 'asdgggsadgsadgsadggsdasadg', 'sdgasgda', 'gsadsgdasgdagsd', 'asgdasdga', 12, 0, '1000.00', '1200.00', 1, '', '1500.00'),
(91, 'Наушники вакуумные PC&amp;Pro Superbass RX-11', 27, 1540242000, 1540288884, 0, '172t361g11', '300.00', 'a:2:{i:0;s:44:\"/images/12965e686c05844e5500b7cadd9fa918.jpg\";i:1;s:44:\"/images/5ad7aa2a44a605e16039630ff2810235.jpg\";}', '', 0, 'a:0:{}', 'a:3:{i:0;s:2:\"47\";i:1;s:2:\"22\";i:2;s:2:\"50\";}', 0, 0, 0, 'vacuum-11-22', '', '', '', 0, 0, '150.00', '250.00', 1, '', '300.00'),
(321, 'Apple iPhone XS Max 256GB Space Grey', 11, 1, 1, 0, 'MT532RU/A', '157170.00', 'a:2:{i:0;s:114:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=bigimage&id=2FDF2022-2E17-471D-88B0-C872879554BE\";i:1;s:111:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=image&id=2FDF2022-2E17-471D-88B0-C872879554BE\";}', 'bot', 0, 'a:0:{}', '10', 10, 0, 0, 'MT532RUA', 'title', 'desc', 'keyw', 0, 0, '104780.00', '115258.00', 1, '', '157170.00'),
(322, 'Apple iPhone XR 256GB White', 11, 1, 1, 0, 'MRYL2RU/A', '111915.00', 'a:2:{i:0;s:114:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=bigimage&id=7967EDD9-F2EB-45C4-8C85-4970E73F2BD2\";i:1;s:111:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=image&id=7967EDD9-F2EB-45C4-8C85-4970E73F2BD2\";}', 'bot', 0, 'a:0:{}', '10', 10, 0, 0, 'MRYL2RUA', 'title', 'desc', 'keyw', 0, 0, '74610.00', '82071.00', 1, '', '111915.00'),
(323, 'Apple iPhone XR 128GB Coral', 11, 1, 1, 0, 'MRYG2RU/A', '99000.00', 'a:2:{i:0;s:114:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=bigimage&id=107A359B-0D57-481B-B3AC-4056F937667F\";i:1;s:111:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=image&id=107A359B-0D57-481B-B3AC-4056F937667F\";}', 'bot', 0, 'a:0:{}', '10', 10, 0, 0, 'MRYG2RUA', 'title', 'desc', 'keyw', 0, 0, '66000.00', '72600.00', 1, '', '99000.00'),
(324, 'Apple iPhone XS 256GB Space Grey', 11, 1, 1, 0, 'MT9H2RU/A', '144315.00', 'a:2:{i:0;s:114:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=bigimage&id=2FDF2022-2E17-471D-88B0-C872879554BE\";i:1;s:111:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=image&id=2FDF2022-2E17-471D-88B0-C872879554BE\";}', 'bot', 0, 'a:0:{}', '10', 10, 0, 0, 'MT9H2RUA', 'title', 'desc', 'keyw', 0, 0, '96210.00', '105831.00', 1, '', '144315.00'),
(325, 'Apple iPhone XR 64GB Black', 11, 1, 1, 0, 'MRY42RU/A', '93255.00', 'a:2:{i:0;s:114:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=bigimage&id=BAF8CB03-C255-4AD6-A153-AC21E39EF3C1\";i:1;s:111:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=image&id=BAF8CB03-C255-4AD6-A153-AC21E39EF3C1\";}', 'bot', 0, 'a:0:{}', '10', 10, 0, 0, 'MRY42RUA', 'title', 'desc', 'keyw', 0, 0, '62170.00', '68387.00', 1, '', '93255.00'),
(326, 'Apple iPhone XS Max 64GB Gold', 11, 1, 1, 0, 'MT522RU/A', '138600.00', 'a:2:{i:0;s:114:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=bigimage&id=CF918D93-448F-48FF-ABA0-161D728D104C\";i:1;s:111:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=image&id=CF918D93-448F-48FF-ABA0-161D728D104C\";}', 'bot', 0, 'a:0:{}', '10', 10, 0, 0, 'MT522RUA', 'title', 'desc', 'keyw', 0, 0, '92400.00', '101640.00', 1, '', '138600.00'),
(327, 'Apple iPhone XR 128GB Yellow', 11, 1, 1, 0, 'MRYF2RU/A', '99000.00', 'a:2:{i:0;s:114:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=bigimage&id=AA065B12-6950-40E5-8362-F7051A4678D4\";i:1;s:111:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=image&id=AA065B12-6950-40E5-8362-F7051A4678D4\";}', 'bot', 0, 'a:0:{}', '10', 10, 0, 0, 'MRYF2RUA', 'title', 'desc', 'keyw', 0, 0, '66000.00', '72600.00', 1, '', '99000.00'),
(328, 'Apple iPhone XS 64GB Space Grey', 11, 1, 1, 0, 'MT9E2RU/A', '125730.00', 'a:2:{i:0;s:114:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=bigimage&id=2FDF2022-2E17-471D-88B0-C872879554BE\";i:1;s:111:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=image&id=2FDF2022-2E17-471D-88B0-C872879554BE\";}', 'bot', 0, 'a:0:{}', '10', 10, 0, 0, 'MT9E2RUA', 'title', 'desc', 'keyw', 0, 0, '83820.00', '92202.00', 1, '', '125730.00'),
(329, 'Apple iPhone XS 512GB Gold', 11, 1, 1, 0, 'MT9N2RU/A', '168705.00', 'a:2:{i:0;s:114:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=bigimage&id=CF918D93-448F-48FF-ABA0-161D728D104C\";i:1;s:111:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=image&id=CF918D93-448F-48FF-ABA0-161D728D104C\";}', 'bot', 0, 'a:0:{}', '10', 10, 0, 0, 'MT9N2RUA', 'title', 'desc', 'keyw', 0, 0, '112470.00', '123717.00', 1, '', '168705.00'),
(330, 'Apple iPhone XS 64GB Gold', 11, 1, 1, 0, 'MT9G2RU/A', '125730.00', 'a:2:{i:0;s:114:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=bigimage&id=CF918D93-448F-48FF-ABA0-161D728D104C\";i:1;s:111:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=image&id=CF918D93-448F-48FF-ABA0-161D728D104C\";}', 'bot', 0, 'a:0:{}', '10', 10, 0, 0, 'MT9G2RUA', 'title', 'desc', 'keyw', 0, 0, '83820.00', '92202.00', 1, '', '125730.00'),
(331, 'Apple iPhone XS Max 256GB Silver', 11, 1, 1, 0, 'MT542RU/A', '157170.00', 'a:2:{i:0;s:114:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=bigimage&id=B44259B3-88A0-454E-8DC1-E637E31D9754\";i:1;s:111:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=image&id=B44259B3-88A0-454E-8DC1-E637E31D9754\";}', 'bot', 0, 'a:0:{}', '10', 10, 0, 0, 'MT542RUA', 'title', 'desc', 'keyw', 0, 0, '104780.00', '115258.00', 1, '', '157170.00'),
(332, 'Apple iPhone XR 256GB Coral', 11, 1, 1, 0, 'MRYP2RU/A', '111915.00', 'a:2:{i:0;s:114:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=bigimage&id=107A359B-0D57-481B-B3AC-4056F937667F\";i:1;s:111:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=image&id=107A359B-0D57-481B-B3AC-4056F937667F\";}', 'bot', 0, 'a:0:{}', '10', 10, 0, 0, 'MRYP2RUA', 'title', 'desc', 'keyw', 0, 0, '74610.00', '82071.00', 1, '', '111915.00'),
(333, 'Apple iPhone XS Max 64GB Silver', 11, 1, 1, 0, 'MT512RU/A', '138600.00', 'a:2:{i:0;s:114:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=bigimage&id=B44259B3-88A0-454E-8DC1-E637E31D9754\";i:1;s:111:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=image&id=B44259B3-88A0-454E-8DC1-E637E31D9754\";}', 'bot', 0, 'a:0:{}', '10', 10, 0, 0, 'MT512RUA', 'title', 'desc', 'keyw', 0, 0, '92400.00', '101640.00', 1, '', '138600.00'),
(334, 'Apple iPhone XS Max 256GB Gold', 11, 1, 1, 0, 'MT552RU/A', '157170.00', 'a:2:{i:0;s:114:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=bigimage&id=CF918D93-448F-48FF-ABA0-161D728D104C\";i:1;s:111:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=image&id=CF918D93-448F-48FF-ABA0-161D728D104C\";}', 'bot', 0, 'a:0:{}', '10', 10, 0, 0, 'MT552RUA', 'title', 'desc', 'keyw', 0, 0, '104780.00', '115258.00', 1, '', '157170.00'),
(335, 'Apple iPhone XR 256GB Blue', 11, 1, 1, 0, 'MRYQ2RU/A', '111915.00', 'a:2:{i:0;s:114:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=bigimage&id=7B2F2246-883D-4EFC-B6C0-2651AA4B892A\";i:1;s:111:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=image&id=7B2F2246-883D-4EFC-B6C0-2651AA4B892A\";}', 'bot', 0, 'a:0:{}', '10', 10, 0, 0, 'MRYQ2RUA', 'title', 'desc', 'keyw', 0, 0, '74610.00', '82071.00', 1, '', '111915.00'),
(336, 'Apple iPhone XR 128GB Blue', 11, -10800, 1543489100, 0, 'MRYH2RU/A', '74800.00', 'a:2:{i:0;s:114:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=bigimage&id=7B2F2246-883D-4EFC-B6C0-2651AA4B892A\";i:1;s:111:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=image&id=7B2F2246-883D-4EFC-B6C0-2651AA4B892A\";}', 'bot', 0, 'a:0:{}', 'a:0:{}', 0, 0, 0, 'MRYH2RUA', 'title', 'desc', 'keyw', 0, 0, '66000.00', '69800.00', 1, '', '99000.00'),
(337, 'Apple iPhone X 256GB Silver', 11, -10800, 1543488897, 0, 'MQAG2RU/A', '92000.00', 'a:2:{i:0;s:114:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=bigimage&id=01FEAF52-5ECA-4F46-82A9-6E8E5DE308EF\";i:1;s:111:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=image&id=01FEAF52-5ECA-4F46-82A9-6E8E5DE308EF\";}', 'bot', 0, 'a:0:{}', 'a:0:{}', 0, 0, 0, 'MQAG2RUA', 'title', 'desc', 'keyw', 0, 0, '83400.00', '87000.00', 1, '', '125100.00'),
(338, 'Apple iPhone XR 256GB (PRODUCT)RED', 11, -10800, 1543489171, 0, 'MRYM2RU/A', '83400.00', 'a:2:{i:0;s:114:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=bigimage&id=C6BAE96C-4D11-437C-BE67-226FD57595A0\";i:1;s:111:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=image&id=C6BAE96C-4D11-437C-BE67-226FD57595A0\";}', 'bot', 0, 'a:0:{}', 'a:0:{}', 0, 0, 0, 'MRYM2RUA', 'title', 'desc', 'keyw', 0, 0, '74610.00', '78400.00', 1, '', '111915.00'),
(339, 'Apple iPhone XR 64GB Blue', 11, -10800, 1543489009, 0, 'MRYA2RU/A', '71000.00', 'a:2:{i:0;s:114:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=bigimage&id=7B2F2246-883D-4EFC-B6C0-2651AA4B892A\";i:1;s:111:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=image&id=7B2F2246-883D-4EFC-B6C0-2651AA4B892A\";}', 'bot', 0, 'a:0:{}', 'a:0:{}', 0, 0, 0, 'MRYA2RUA', 'title', 'desc', 'keyw', 0, 0, '62170.00', '66000.00', 1, '', '93255.00'),
(340, 'Apple iPhone XS Max 512GB Silver', 11, -10800, 1543399311, 0, 'MT572RU/A', '128000.00', 'a:2:{i:0;s:114:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=bigimage&id=B44259B3-88A0-454E-8DC1-E637E31D9754\";i:1;s:111:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=image&id=B44259B3-88A0-454E-8DC1-E637E31D9754\";}', 'bot', 0, 'a:0:{}', 'a:0:{}', 0, 0, 0, 'MT572RUA', 'title', 'desc', 'keyw', 0, 0, '121020.00', '124000.00', 1, '', '181530.00'),
(341, 'Apple iPhone XR 128GB White', 11, 1, 1, 0, 'MRYD2RU/A', '99000.00', 'a:2:{i:0;s:114:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=bigimage&id=7967EDD9-F2EB-45C4-8C85-4970E73F2BD2\";i:1;s:111:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=image&id=7967EDD9-F2EB-45C4-8C85-4970E73F2BD2\";}', 'bot', 0, 'a:0:{}', '10', 10, 0, 0, 'MRYD2RUA', 'title', 'desc', 'keyw', 0, 0, '66000.00', '72600.00', 1, '', '99000.00'),
(342, 'Apple iPhone XS 64GB Silver', 11, 1, 1, 0, 'MT9F2RU/A', '125730.00', 'a:2:{i:0;s:114:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=bigimage&id=B44259B3-88A0-454E-8DC1-E637E31D9754\";i:1;s:111:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=image&id=B44259B3-88A0-454E-8DC1-E637E31D9754\";}', 'bot', 0, 'a:0:{}', '10', 10, 0, 0, 'MT9F2RUA', 'title', 'desc', 'keyw', 0, 0, '83820.00', '92202.00', 1, '', '125730.00'),
(343, 'Apple iPhone XS 512GB Silver', 11, 1, 1, 0, 'MT9M2RU/A', '168705.00', 'a:2:{i:0;s:114:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=bigimage&id=B44259B3-88A0-454E-8DC1-E637E31D9754\";i:1;s:111:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=image&id=B44259B3-88A0-454E-8DC1-E637E31D9754\";}', 'bot', 0, 'a:0:{}', '10', 10, 0, 0, 'MT9M2RUA', 'title', 'desc', 'keyw', 0, 0, '112470.00', '123717.00', 1, '', '168705.00'),
(344, 'Apple iPhone XS Max 512GB Space Grey', 11, -10800, 1543405860, 0, 'MT562RU/A', '126000.00', 'a:2:{i:0;s:114:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=bigimage&id=2FDF2022-2E17-471D-88B0-C872879554BE\";i:1;s:111:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=image&id=2FDF2022-2E17-471D-88B0-C872879554BE\";}', 'bot', 0, 'a:0:{}', 'a:0:{}', 0, 0, 0, 'MT562RUA', 'title', 'desc', 'keyw', 0, 0, '121020.00', '123000.00', 1, '', '181530.00'),
(345, 'Apple iPhone XR 256GB Black', 11, 1, 1, 0, 'MRYJ2RU/A', '111915.00', 'a:2:{i:0;s:114:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=bigimage&id=BAF8CB03-C255-4AD6-A153-AC21E39EF3C1\";i:1;s:111:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=image&id=BAF8CB03-C255-4AD6-A153-AC21E39EF3C1\";}', 'bot', 0, 'a:0:{}', '10', 10, 0, 0, 'MRYJ2RUA', 'title', 'desc', 'keyw', 0, 0, '74610.00', '82071.00', 1, '', '111915.00'),
(346, 'IRBIS SF02, 1.77\" (128x160), 2xSimCard, Bluetooth, microUSB, MicroSD, Black', 11, 1, 1, 0, 'SF02b', '10.00', 'a:2:{i:0;s:114:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=bigimage&id=EC031EEE-3DDD-463E-965E-39FE20FA7684\";i:1;s:111:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=image&id=EC031EEE-3DDD-463E-965E-39FE20FA7684\";}', 'bot', 0, 'a:0:{}', '10', 10, 0, 0, 'SF02b', 'title', 'desc', 'keyw', 0, 0, '7.00', '7.00', 1, '', '10.00'),
(347, 'IRBIS SP552, 5.5\" (1440x720IPS), MTK6737 4x1,3Ghz (QuadCore), 2048MB, 16GB, cam 5.0MPx+13.0MPx, Wi-Fi, LTE+3G (2xSimCard), Bluetooth, GPS, Android 7.0, MicroSD, jack 3.5,  Space Grey', 11, 1, 1, 0, 'SP552', '150.00', 'a:2:{i:0;s:114:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=bigimage&id=1C753CBD-5DBF-4C78-9951-7B2AC9AC1072\";i:1;s:111:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=image&id=1C753CBD-5DBF-4C78-9951-7B2AC9AC1072\";}', 'bot', 0, 'a:0:{}', '10', 10, 0, 0, 'SP552', 'title', 'desc', 'keyw', 0, 0, '100.00', '110.00', 1, '', '150.00'),
(348, 'IRBIS SF15, 1.77\" (128x160), cam 0,08MPx, 2xSimCard, Bluetooth, microUSB, MicroSD, Black', 11, 1, 1, 0, 'SF15d', '18.00', 'a:2:{i:0;s:114:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=bigimage&id=640DD2D8-EAA2-42A3-930A-3974B6C4E9A6\";i:1;s:111:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=image&id=640DD2D8-EAA2-42A3-930A-3974B6C4E9A6\";}', 'bot', 0, 'a:0:{}', '10', 10, 0, 0, 'SF15d', 'title', 'desc', 'keyw', 0, 0, '12.00', '13.00', 1, '', '18.00'),
(349, 'IRBIS SF02, 1.77\" (128x160), 2xSimCard, Bluetooth, microUSB, MicroSD, Black/Blue', 11, 1, 1, 0, 'SF02x', '10.00', 'a:2:{i:0;s:114:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=bigimage&id=4BD7BB99-9AAA-4C39-B5B0-D199FC79E758\";i:1;s:111:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=image&id=4BD7BB99-9AAA-4C39-B5B0-D199FC79E758\";}', 'bot', 0, 'a:0:{}', '10', 10, 0, 0, 'SF02x', 'title', 'desc', 'keyw', 0, 0, '7.00', '7.00', 1, '', '10.00'),
(350, 'IRBIS SF07, 1.77\" (128x160), cam 0,08MPx, 2xSimCard, Bluetooth, microUSB, MicroSD, Deep Blue', 11, 1, 1, 0, 'SF07d', '18.00', 'a:2:{i:0;s:111:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=image&id=E51DB0BD-1ADD-4BD4-8B35-88267ECEF222\";i:1;s:114:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=bigimage&id=4B326F5A-7EBF-4D35-BADD-D4C5D619DBA3\";}', 'bot', 0, 'a:0:{}', '10', 10, 0, 0, 'SF07d', 'title', 'desc', 'keyw', 0, 0, '12.00', '13.00', 1, '', '18.00'),
(351, 'IRBIS SF02, 1.77\" (128x160), 2xSimCard, Bluetooth, microUSB, MicroSD, Black/Red', 11, 1, 1, 0, 'SF02r', '10.00', 'a:2:{i:0;s:114:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=bigimage&id=B034DF4F-B0ED-497F-9578-C66A81224693\";i:1;s:111:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=image&id=B034DF4F-B0ED-497F-9578-C66A81224693\";}', 'bot', 0, 'a:0:{}', '10', 10, 0, 0, 'SF02r', 'title', 'desc', 'keyw', 0, 0, '7.00', '7.00', 1, '', '10.00'),
(352, 'IRBIS SP517, 5.0\" (1280x720),  MT6737 4x1,25Ghz (QuadCore), 1024MB, 8GB, cam 5.0MPx+8.0MPx, Wi-Fi, LTE+3G (2xSimCard), Bluetooth, GPS, Android 7.0, microUSB, MicroSD, jack 3.5, Silver', 11, 1, 1, 0, 'SP517s', '97.00', 'a:2:{i:0;s:114:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=bigimage&id=DF049B64-C627-4E59-8FCA-645999EF5FCC\";i:1;s:111:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=image&id=DF049B64-C627-4E59-8FCA-645999EF5FCC\";}', 'bot', 0, 'a:0:{}', '10', 10, 0, 0, 'SP517s', 'title', 'desc', 'keyw', 0, 0, '65.00', '71.00', 1, '', '97.00'),
(353, 'IRBIS SF07, 1.77\" (128x160), cam 0,08MPx, 2xSimCard, Bluetooth, microUSB, MicroSD, Red', 11, 1, 1, 0, 'SF07r', '18.00', 'a:2:{i:0;s:114:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=bigimage&id=3C90860E-0559-4BC5-81A0-D3D6FDB98FE3\";i:1;s:111:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=image&id=3C90860E-0559-4BC5-81A0-D3D6FDB98FE3\";}', 'bot', 0, 'a:0:{}', '10', 10, 0, 0, 'SF07r', 'title', 'desc', 'keyw', 0, 0, '12.00', '13.00', 1, '', '18.00'),
(354, 'IRBIS SF07, 1.77\" (128x160), cam 0,08MPx, 2xSimCard, Bluetooth, microUSB, MicroSD, Blue', 11, 1, 1, 0, 'SF07x', '18.00', 'a:2:{i:0;s:114:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=bigimage&id=621518C9-CE0C-4DB7-92AC-9858D4A80831\";i:1;s:111:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=image&id=621518C9-CE0C-4DB7-92AC-9858D4A80831\";}', 'bot', 0, 'a:0:{}', '10', 10, 0, 0, 'SF07x', 'title', 'desc', 'keyw', 0, 0, '12.00', '13.00', 1, '', '18.00'),
(355, 'IRBIS SF15, 1.77\" (128x160), cam 0,08MPx, 2xSimCard, Bluetooth, microUSB, MicroSD, Red', 11, 1, 1, 0, 'SF15r', '18.00', 'a:2:{i:0;s:114:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=bigimage&id=8D493174-8200-4FCB-BA7D-8A2F7CA1E856\";i:1;s:111:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=image&id=8D493174-8200-4FCB-BA7D-8A2F7CA1E856\";}', 'bot', 0, 'a:0:{}', '10', 10, 0, 0, 'SF15r', 'title', 'desc', 'keyw', 0, 0, '12.00', '13.00', 1, '', '18.00'),
(356, 'IRBIS SF15, 1.77\" (128x160), cam 0,08MPx, 2xSimCard, Bluetooth, microUSB, MicroSD, Blue', 11, 1, 1, 0, 'SF15x', '18.00', 'a:2:{i:0;s:114:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=bigimage&id=3357B0B5-3C27-4076-BD76-D0C4A802B29A\";i:1;s:111:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=image&id=3357B0B5-3C27-4076-BD76-D0C4A802B29A\";}', 'bot', 0, 'a:0:{}', '10', 10, 0, 0, 'SF15x', 'title', 'desc', 'keyw', 0, 0, '12.00', '13.00', 1, '', '18.00'),
(357, 'IRBIS SF08, 2.4\" (240x320), 2xSimCard, Bluetooth, microUSB, MicroSD, Black', 11, 1, 1, 0, 'SF08', '28.00', 'a:2:{i:0;s:114:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=bigimage&id=92B418CE-80CC-417E-A543-8604382D96AE\";i:1;s:111:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=image&id=92B418CE-80CC-417E-A543-8604382D96AE\";}', 'bot', 0, 'a:0:{}', '10', 10, 0, 0, 'SF08', 'title', 'desc', 'keyw', 0, 0, '19.00', '20.00', 1, '', '28.00'),
(358, 'IRBIS SF15, 1.77\" (128x160), cam 0,08MPx, 2xSimCard, Bluetooth, microUSB, MicroSD, Pink', 11, 1, 1, 0, 'SF15p', '18.00', 'a:2:{i:0;s:114:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=bigimage&id=418F0D1A-8924-4831-81E5-D9EFC767D3E1\";i:1;s:111:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=image&id=418F0D1A-8924-4831-81E5-D9EFC767D3E1\";}', 'bot', 0, 'a:0:{}', '10', 10, 0, 0, 'SF15p', 'title', 'desc', 'keyw', 0, 0, '12.00', '13.00', 1, '', '18.00'),
(359, 'IRBIS SP517, 5.0\" (1280x720),  MT6737 4x1,25Ghz (QuadCore), 1024MB, 8GB, cam 5.0MPx+8.0MPx, Wi-Fi, LTE+3G (2xSimCard), Bluetooth, GPS, Android 7.0, microUSB, MicroSD, jack 3.5, Red', 11, 1, 1, 0, 'SP517r', '97.00', 'a:2:{i:0;s:114:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=bigimage&id=185BC3ED-DFC1-4D63-8289-C60D440D94E4\";i:1;s:111:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=image&id=185BC3ED-DFC1-4D63-8289-C60D440D94E4\";}', 'bot', 0, 'a:0:{}', '10', 10, 0, 0, 'SP517r', 'title', 'desc', 'keyw', 0, 0, '65.00', '71.00', 1, '', '97.00'),
(360, 'IRBIS SF12, 2.4\" (240x320), 2xSimCard, Bluetooth, microUSB, MicroSD, Black/red', 11, 1, 1, 0, 'SF12r', '16.00', 'a:2:{i:0;s:114:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=bigimage&id=5EA0B23A-CCEA-458A-8F08-481C26CA7EAE\";i:1;s:111:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=image&id=5EA0B23A-CCEA-458A-8F08-481C26CA7EAE\";}', 'bot', 0, 'a:0:{}', '10', 10, 0, 0, 'SF12r', 'title', 'desc', 'keyw', 0, 0, '11.00', '12.00', 1, '', '16.00'),
(361, 'IRBIS SP517, 5.0\" (1280x720),  MT6737 4x1,25Ghz (QuadCore), 1024MB, 8GB, cam 5.0MPx+8.0MPx, Wi-Fi, LTE+3G (2xSimCard), Bluetooth, GPS, Android 7.0, microUSB, MicroSD, jack 3.5, Black', 11, 1, 1, 0, 'SP517b', '97.00', 'a:2:{i:0;s:114:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=bigimage&id=12F2041A-A764-4F5C-8651-AD9DE7F16D9F\";i:1;s:111:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=image&id=12F2041A-A764-4F5C-8651-AD9DE7F16D9F\";}', 'bot', 0, 'a:0:{}', '10', 10, 0, 0, 'SP517b', 'title', 'desc', 'keyw', 0, 0, '65.00', '71.00', 1, '', '97.00'),
(362, 'Внешний аккумулятор Xiaomi Mi Power Bank  2C 20000mAh white', 11, 1, 1, 0, 'PowerBank2C 20000 white', '2815.00', 'a:2:{i:0;s:114:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=bigimage&id=D8A40C2F-DBC1-406F-8C21-F48F064A61D6\";i:1;s:111:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=image&id=D8A40C2F-DBC1-406F-8C21-F48F064A61D6\";}', 'bot', 0, 'a:0:{}', '10', 10, 0, 0, 'PowerBank2C20000white', 'title', 'desc', 'keyw', 0, 0, '1877.00', '2064.00', 1, '', '2815.00'),
(363, 'Фитнес-браслет Xiaomi Mi band 3 BLACK', 12, -10800, 1543407058, 0, 'MiBand 3 black', '3500.00', 'a:2:{i:0;s:114:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=bigimage&id=CC4AB766-48F0-42E3-8251-B666E498E0A7\";i:1;s:111:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=image&id=CC4AB766-48F0-42E3-8251-B666E498E0A7\";}', 'bot', 0, 'a:0:{}', 'a:0:{}', 0, 0, 0, 'MiBand3black', 'title', 'desc', 'keyw', 0, 0, '2382.00', '2700.00', 1, '', '3573.00'),
(364, 'Внешний аккумулятор Xiaomi Mi Power Bank 2S 10000mAh silver', 11, 1, 1, 0, 'PowerBank2S 10000 silver', '2211.00', 'a:2:{i:0;s:114:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=bigimage&id=2DD19D3C-AA77-455D-9C1C-D7E49BEDF02B\";i:1;s:111:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=image&id=2DD19D3C-AA77-455D-9C1C-D7E49BEDF02B\";}', 'bot', 0, 'a:0:{}', '10', 10, 0, 0, 'PowerBank2S10000silver', 'title', 'desc', 'keyw', 0, 0, '1474.00', '1621.00', 1, '', '2211.00'),
(365, 'Xiaomi MI MIX 2 smartphone 5.99\"(2160  1080) 6Gb 64Gb 2Sim 4G Wi-Fi BT 12Mpix Snapdragon 835 And7.1 black', 11, 1, 1, 0, 'MI MIX 2 64Gb black', '45444.00', 'a:2:{i:0;s:114:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=bigimage&id=3FC290A1-CE60-479B-AD67-DC009CFA1DDA\";i:1;s:111:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=image&id=3FC290A1-CE60-479B-AD67-DC009CFA1DDA\";}', 'bot', 0, 'a:0:{}', '10', 10, 0, 0, 'MIMIX264Gbblack', 'title', 'desc', 'keyw', 0, 0, '30296.00', '33325.00', 1, '', '45444.00'),
(366, 'Xiaomi Mi Mix 2S  smartphone 5,99\"(2160 х 1080) 6Gb 128 GB 2Sim 4G Wi-Fi BT 2x12Mpix Snapdragon 845 Android 8.0 white', 11, 1, 1, 0, 'Mi Mix 2S 128Gb White', '60612.00', 'a:2:{i:0;s:114:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=bigimage&id=CCDBBDB6-4C90-4A14-9ACE-513433C04B0F\";i:1;s:111:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=image&id=CCDBBDB6-4C90-4A14-9ACE-513433C04B0F\";}', 'bot', 0, 'a:0:{}', '10', 10, 0, 0, 'MiMix2S128GbWhite', 'title', 'desc', 'keyw', 0, 0, '40408.00', '44448.00', 1, '', '60612.00'),
(367, 'Xiaomi Redmi 5 Smartphone 5,7\"(1440x720) 2Gb 32Gb 2Sim 4G Wi-Fi 12Mpix Snapdragon 450 Andr blue', 11, 1, 1, 0, 'Redmi 5 32Gb blue', '15778.00', 'a:2:{i:0;s:114:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=bigimage&id=47070660-A9AB-4A4C-A94C-1217F3B1FA69\";i:1;s:111:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=image&id=47070660-A9AB-4A4C-A94C-1217F3B1FA69\";}', 'bot', 0, 'a:0:{}', '10', 10, 0, 0, 'Redmi532Gbblue', 'title', 'desc', 'keyw', 0, 0, '10519.00', '11570.00', 1, '', '15778.00'),
(368, 'Xiaomi Redmi 6 blue smartphone 5.45\'(1440x720) 4GB 64GB 2 Sim 4G  Wi-Fi 12Mpix+5Mpix/5Mpix Helio P22', 11, 1, 1, 0, 'Redmi 6 64GB blue', '20047.00', 'a:2:{i:0;s:114:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=bigimage&id=3DE4DAE8-4653-48F5-90D8-9173992C2D00\";i:1;s:111:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=image&id=3DE4DAE8-4653-48F5-90D8-9173992C2D00\";}', 'bot', 0, 'a:0:{}', '10', 10, 0, 0, 'Redmi664GBblue', 'title', 'desc', 'keyw', 0, 0, '13365.00', '14701.00', 1, '', '20047.00'),
(369, 'Xiaomi Redmi 6 gold smartphone 5.45\'(1440x720) 3GB 32GB 2 Sim 4G  Wi-Fi 12Mpix+5Mpix/5Mpix Helio P22', 11, 1, 1, 0, 'Redmi 6 32Gb gold', '17539.00', 'a:2:{i:0;s:114:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=bigimage&id=28997785-35CB-4B45-A45A-655C665ABD6B\";i:1;s:111:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=image&id=28997785-35CB-4B45-A45A-655C665ABD6B\";}', 'bot', 0, 'a:0:{}', '10', 10, 0, 0, 'Redmi632Gbgold', 'title', 'desc', 'keyw', 0, 0, '11693.00', '12862.00', 1, '', '17539.00'),
(370, 'Xiaomi Redmi Note 6 Pro black smartphone 6,26\"(2280x1080) 4GB 64GB 2 Sim 12Mpix+5Mpix/20Mpix+5Mpix 4G Wi-Fi Android Snapdragon 636', 11, 1, 1, 0, 'Redmi Note 6 Pro 64GB black', '26206.00', 'a:2:{i:0;s:114:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=bigimage&id=5991E97A-1BD6-4201-9176-2D43597DB1D4\";i:1;s:111:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=image&id=5991E97A-1BD6-4201-9176-2D43597DB1D4\";}', 'bot', 0, 'a:0:{}', '10', 10, 0, 0, 'RedmiNote6Pro64GBblack', 'title', 'desc', 'keyw', 0, 0, '17471.00', '19218.00', 1, '', '26206.00'),
(371, 'Xiaomi Redmi S2 Grey smartphone 5,99\"(1440x720) 3GB 32GB 2 Sim 12Mpix+5Mpix/16Mpix 4G Wi-Fi Snapdragon 625 And 8.1', 11, 1, 1, 0, 'Redmi S2 32Gb grey', '18406.00', 'a:2:{i:0;s:114:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=bigimage&id=DFD68B56-4C81-486B-8167-38ECB2A009E6\";i:1;s:111:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=image&id=DFD68B56-4C81-486B-8167-38ECB2A009E6\";}', 'bot', 0, 'a:0:{}', '10', 10, 0, 0, 'RedmiS232Gbgrey', 'title', 'desc', 'keyw', 0, 0, '12271.00', '13498.00', 1, '', '18406.00'),
(372, 'Xiaomi Mi A2 blue smartphone 5.99\'(2160x1080) 4GB 64GB 2 Sim 12MPx+20MPx/20Mpix 4G Wi-Fi Android One Snapdragon 627', 11, 1, 1, 0, 'Mi A2 64GB blue', '30076.00', 'a:2:{i:0;s:114:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=bigimage&id=81A78D25-2E46-4567-8431-22D8459865A5\";i:1;s:111:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=image&id=81A78D25-2E46-4567-8431-22D8459865A5\";}', 'bot', 0, 'a:0:{}', '10', 10, 0, 0, 'MiA264GBblue', 'title', 'desc', 'keyw', 0, 0, '20051.00', '22056.00', 1, '', '30076.00'),
(373, 'Xiaomi Redmi S2 Gold smartphone 5,99\"(1440x720) 4GB 64GB 2 Sim 12Mpix+5Mpix/16Mpix 4G Wi-Fi Snapdragon 625 And 8.1', 11, 1, 1, 0, 'Redmi S2 64Gb grey', '20094.00', 'a:2:{i:0;s:114:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=bigimage&id=DFD68B56-4C81-486B-8167-38ECB2A009E6\";i:1;s:111:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=image&id=DFD68B56-4C81-486B-8167-38ECB2A009E6\";}', 'bot', 0, 'a:0:{}', '10', 10, 0, 0, 'RedmiS264Gbgrey', 'title', 'desc', 'keyw', 0, 0, '13396.00', '14735.00', 1, '', '20094.00'),
(374, 'Xiaomi Redmi 6A gold smartphone5.45', 11, -10800, 1543398099, 0, 'Redmi 6A 16Gb gold', '10786.00', 'a:2:{i:0;s:114:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=bigimage&id=976BCD01-ED91-463B-AA68-1AF11235E6D8\";i:1;s:111:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=image&id=976BCD01-ED91-463B-AA68-1AF11235E6D8\";}', 'bot', 0, 'a:0:{}', 'a:0:{}', 0, 0, 0, 'Redmi6A16Gbgold', 'title', 'Xiaomi Redmi 6A 16Gb Gold', 'Xiaomi Redmi 6A 16Gb Gold', 0, 0, '7191.00', '7910.00', 1, '', '10786.00'),
(375, 'Xiaomi Redmi 6 gold smartphone 5.45\'(1440x720) 4GB 64GB 2 Sim 4G  Wi-Fi 12Mpix+5Mpix/5Mpix Helio P22', 11, 1, 1, 0, 'Redmi 6 64Gb gold', '20047.00', 'a:2:{i:0;s:114:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=bigimage&id=28997785-35CB-4B45-A45A-655C665ABD6B\";i:1;s:111:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=image&id=28997785-35CB-4B45-A45A-655C665ABD6B\";}', 'bot', 0, 'a:0:{}', '10', 10, 0, 0, 'Redmi664Gbgold', 'title', 'desc', 'keyw', 0, 0, '13365.00', '14701.00', 1, '', '20047.00'),
(376, 'Xiaomi Redmi Note 6 Pro rose gold smartphone 6,26\"(2280x1080) 4GB 64GB 2 Sim 12Mpix+5Mpix/20Mpix+5Mpix 4G Wi-Fi Android Snapdragon 636', 11, 1, 1, 0, 'Redmi Note 6 Pro 64GB rose gold', '26206.00', 'a:2:{i:0;s:114:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=bigimage&id=662696D1-E627-4488-B1AE-828B4B9C869C\";i:1;s:111:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=image&id=662696D1-E627-4488-B1AE-828B4B9C869C\";}', 'bot', 0, 'a:0:{}', '10', 10, 0, 0, 'RedmiNote6Pro64GBrosegold', 'title', 'desc', 'keyw', 0, 0, '17471.00', '19218.00', 1, '', '26206.00'),
(377, 'Xiaomi Redmi Note 5 red smartphone 5,99\"(2160x1080) 4GB 64GB 2 Sim 12Mpix+5Mpix/13Mpix 4G Wi-Fi Snapdragon 636', 11, 1, 1, 0, 'Redmi Note 5 red 64G', '25401.00', 'a:2:{i:0;s:114:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=bigimage&id=4DEF9E89-DF07-48F5-8107-BB1743F23670\";i:1;s:111:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=image&id=4DEF9E89-DF07-48F5-8107-BB1743F23670\";}', 'bot', 0, 'a:0:{}', '10', 10, 0, 0, 'RedmiNote5red64G', 'title', 'desc', 'keyw', 0, 0, '16934.00', '18627.00', 1, '', '25401.00'),
(378, 'Xiaomi Redmi S2 Gold smartphone 5,99\"(1440x720) 3GB 32GB 2 Sim 12Mpix+5Mpix/16Mpix 4G Wi-Fi Snapdragon 625 And 8.1', 11, 1, 1, 0, 'Redmi S2 32Gb gold', '18406.00', 'a:2:{i:0;s:114:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=bigimage&id=FCBC9E4B-9D4B-4392-B4B2-005A7389F38E\";i:1;s:111:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=image&id=FCBC9E4B-9D4B-4392-B4B2-005A7389F38E\";}', 'bot', 0, 'a:0:{}', '10', 10, 0, 0, 'RedmiS232Gbgold', 'title', 'desc', 'keyw', 0, 0, '12271.00', '13498.00', 1, '', '18406.00'),
(379, 'Xiaomi Redmi Note 6 Pro black smartphone 6,26\"(2280x1080) 3GB 32GB 2 Sim 12Mpix+5Mpix/20Mpix+5Mpix 4G Wi-Fi Android Snapdragon 636', 11, 1, 1, 0, 'Redmi Note 6 Pro 32GB black', '23583.00', 'a:2:{i:0;s:114:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=bigimage&id=5991E97A-1BD6-4201-9176-2D43597DB1D4\";i:1;s:111:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=image&id=5991E97A-1BD6-4201-9176-2D43597DB1D4\";}', 'bot', 0, 'a:0:{}', '10', 10, 0, 0, 'RedmiNote6Pro32GBblack', 'title', 'desc', 'keyw', 0, 0, '15722.00', '17294.00', 1, '', '23583.00'),
(380, 'Xiaomi Mi A2 gold smartphone 5.99\'(2160x1080) 4GB 64GB 2 Sim 12MPx+20MPx/20Mpix 4G Wi-Fi Android One Snapdragon 626', 11, 1, 1, 0, 'Mi A2 64Gb gold', '30076.00', 'a:2:{i:0;s:114:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=bigimage&id=810692C0-B473-46A6-B0AF-8FBD08E33851\";i:1;s:111:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=image&id=810692C0-B473-46A6-B0AF-8FBD08E33851\";}', 'bot', 0, 'a:0:{}', '10', 10, 0, 0, 'MiA264Gbgold', 'title', 'desc', 'keyw', 0, 0, '20051.00', '22056.00', 1, '', '30076.00'),
(381, 'Xiaomi Redmi Note 6 Pro blue smartphone 6,26\"(2280x1080) 3GB 32GB 2 Sim 12Mpix+5Mpix/20Mpix+5Mpix 4G Wi-Fi Android Snapdragon 636', 11, 1, 1, 0, 'Redmi Note 6 Pro 32GB blue', '23583.00', 'a:2:{i:0;s:114:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=bigimage&id=860FBEA5-D675-4C7D-AF11-9F333A2E253F\";i:1;s:111:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=image&id=860FBEA5-D675-4C7D-AF11-9F333A2E253F\";}', 'bot', 0, 'a:0:{}', '10', 10, 0, 0, 'RedmiNote6Pro32GBblue', 'title', 'desc', 'keyw', 0, 0, '15722.00', '17294.00', 1, '', '23583.00'),
(382, 'Xiaomi Redmi 6A black smartphone5.45\'(1440x720) 2GB 32GB 2 Sim 4G Wi-Fi 13Mpix/5Mpix  Helio A22', 11, 1, 1, 0, 'Redmi 6A 32Gb black', '13006.00', 'a:2:{i:0;s:114:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=bigimage&id=7F1AA58D-14FC-4F3A-BA4E-AFF469C293A1\";i:1;s:111:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=image&id=7F1AA58D-14FC-4F3A-BA4E-AFF469C293A1\";}', 'bot', 0, 'a:0:{}', '10', 10, 0, 0, 'Redmi6A32Gbblack', 'title', 'desc', 'keyw', 0, 0, '8671.00', '9538.00', 1, '', '13006.00');
INSERT INTO `product` (`id`, `name`, `category_id`, `created_at`, `updated_at`, `status_id`, `sku`, `price`, `images`, `description`, `original`, `certificate`, `owner_id`, `warehouse_id`, `availability`, `sale_id`, `alias`, `meta_title`, `meta_description`, `meta_keywords`, `commission`, `type`, `purchase_price`, `agent_price`, `active`, `region_id`, `old_price`) VALUES
(383, 'Xiaomi Redmi Note 6 Pro rose gold smartphone 6,26\"(2280x1080) 3GB 32GB 2 Sim 12Mpix+5Mpix/20Mpix+5Mpix 4G Wi-Fi Android Snapdragon 636', 11, 1, 1, 0, 'Redmi Note 6 Pro 32GB rose gold', '23583.00', 'a:2:{i:0;s:114:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=bigimage&id=662696D1-E627-4488-B1AE-828B4B9C869C\";i:1;s:111:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=image&id=662696D1-E627-4488-B1AE-828B4B9C869C\";}', 'bot', 0, 'a:0:{}', '10', 10, 0, 0, 'RedmiNote6Pro32GBrosegold', 'title', 'desc', 'keyw', 0, 0, '15722.00', '17294.00', 1, '', '23583.00'),
(384, 'Xiaomi Mi A2 Lite blue smartphone 5.84\"(1080x2280) 3GB 32GB 2 Sim 12Mpix+5Mpix/5Mpix 4G Wi-Fi Android One Snapdragon 625', 11, 1, 1, 0, 'Mi A2 Lite 32Gb blue', '20047.00', 'a:2:{i:0;s:114:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=bigimage&id=6EC7E42C-8553-4A3C-9F2D-7BBEEF856540\";i:1;s:111:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=image&id=6EC7E42C-8553-4A3C-9F2D-7BBEEF856540\";}', 'bot', 0, 'a:0:{}', '10', 10, 0, 0, 'MiA2Lite32Gbblue', 'title', 'desc', 'keyw', 0, 0, '13365.00', '14701.00', 1, '', '20047.00'),
(385, 'Xiaomi Mi A2 Lite black smartphone 5.84\"(1080x2280) 3GB 32GB 2 Sim 12Mpix+5Mpix/5Mpix 4G Wi-Fi Android One Snapdragon 625', 11, 1, 1, 0, 'Mi A2 Lite 32Gb black', '20047.00', 'a:2:{i:0;s:114:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=bigimage&id=45984984-821D-4D70-B98E-B7C2B295797C\";i:1;s:111:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=image&id=45984984-821D-4D70-B98E-B7C2B295797C\";}', 'bot', 0, 'a:0:{}', '10', 10, 0, 0, 'MiA2Lite32Gbblack', 'title', 'desc', 'keyw', 0, 0, '13365.00', '14701.00', 1, '', '20047.00'),
(386, 'Xiaomi Redmi 6A black smartphone5.45\'(1440x720) 2GB 16GB 2 Sim 4G Wi-Fi 13Mpix/5Mpix  Helio A22', 11, 1, 1, 0, 'Redmi6A16Gbblack', '10786.00', 'a:2:{i:0;s:114:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=bigimage&id=7F1AA58D-14FC-4F3A-BA4E-AFF469C293A1\";i:1;s:111:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=image&id=7F1AA58D-14FC-4F3A-BA4E-AFF469C293A1\";}', 'bot', 0, 'a:0:{}', '10', 10, 0, 0, 'Redmi6A16Gbblack', 'title', 'desc', 'keyw', 0, 0, '7191.00', '7910.00', 1, '', '10786.00'),
(387, 'Xiaomi Mi 8 smartphone 6,21\"(2248 х 1080) 6Gb 64 GB 2Sim 4G Wi-Fi BT 12Mpix+12Mpix/20Mpix Snapdragon 845 Android 8.1 blue', 11, 1, 1, 0, 'Mi864GBblue', '52093.00', 'a:2:{i:0;s:114:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=bigimage&id=29C24FBB-6275-401B-831E-403A8AE48656\";i:1;s:111:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=image&id=29C24FBB-6275-401B-831E-403A8AE48656\";}', 'bot', 0, 'a:0:{}', '10', 10, 0, 0, 'Mi864GBblue', 'title', 'desc', 'keyw', 0, 0, '34729.00', '38201.00', 1, '', '52093.00'),
(388, 'Xiaomi Redmi Note 5 gold smartphone 5,99\"(2160x1080) 3GB 32GB 2 Sim 12Mpix+5Mpix/13Mpix 4G Wi-Fi Snapdragon 636', 11, 1, 1, 0, 'RedmiNote5gold32G', '22707.00', 'a:2:{i:0;s:114:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=bigimage&id=6B5F5182-EDC7-4A39-9356-5488B0A05E9D\";i:1;s:111:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=image&id=6B5F5182-EDC7-4A39-9356-5488B0A05E9D\";}', 'bot', 0, 'a:0:{}', '10', 10, 0, 0, 'RedmiNote5gold32G', 'title', 'desc', 'keyw', 0, 0, '15138.00', '16651.00', 1, '', '22707.00'),
(389, 'Xiaomi Redmi 5 Smartphone 5,7\"(1440x720) 2Gb 16Gb 2Sim 4G Wi-Fi 12Mpix Snapdragon 450 Andr blue', 11, 1, 1, 0, 'Redmi516Gbblue', '13906.00', 'a:2:{i:0;s:114:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=bigimage&id=70E6252A-712E-4A1F-96FC-E9AF1E5C0DC8\";i:1;s:111:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=image&id=70E6252A-712E-4A1F-96FC-E9AF1E5C0DC8\";}', 'bot', 0, 'a:0:{}', '10', 10, 0, 0, 'Redmi516Gbblue', 'title', 'desc', 'keyw', 0, 0, '9271.00', '10198.00', 1, '', '13906.00'),
(390, 'Xiaomi Redmi 6 blue smartphone 5.45\'(1440x720) 3GB 32GB 2 Sim 4G  Wi-Fi 12Mpix+5Mpix/5Mpix Helio P22', 11, 1, 1, 0, 'Redmi632Gbblue', '17539.00', 'a:2:{i:0;s:114:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=bigimage&id=FB6FCA5E-6DB7-4322-89B9-6A6812F2DD90\";i:1;s:111:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=image&id=FB6FCA5E-6DB7-4322-89B9-6A6812F2DD90\";}', 'bot', 0, 'a:0:{}', '10', 10, 0, 0, 'Redmi632Gbblue', 'title', 'desc', 'keyw', 0, 0, '11693.00', '12862.00', 1, '', '17539.00'),
(391, 'Xiaomi Redmi Note 6 Pro blue smartphone 6,26\"(2280x1080) 4GB 64GB 2 Sim 12Mpix+5Mpix/20Mpix+5Mpix 4G Wi-Fi Android Snapdragon 636', 11, 1, 1, 0, 'RedmiNote6Pro64GBblue', '26206.00', 'a:2:{i:0;s:114:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=bigimage&id=860FBEA5-D675-4C7D-AF11-9F333A2E253F\";i:1;s:111:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=image&id=860FBEA5-D675-4C7D-AF11-9F333A2E253F\";}', 'bot', 0, 'a:0:{}', '10', 10, 0, 0, 'RedmiNote6Pro64GBblue', 'title', 'desc', 'keyw', 0, 0, '17471.00', '19218.00', 1, '', '26206.00'),
(392, 'Xiaomi MI MAX 3 smartphone 6.9\"(2160  1080) 4Gb 64Gb 2Sim 4G Wi-Fi BT 12Mpix+5Mpix/8Mpix Snapdragon 636 And8.1 black', 11, 1, 1, 0, 'MiMax364GBblack', '32022.00', 'a:2:{i:0;s:114:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=bigimage&id=4B6E9EEC-0604-4FC3-B5A4-92D51A843F57\";i:1;s:111:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=image&id=4B6E9EEC-0604-4FC3-B5A4-92D51A843F57\";}', 'bot', 0, 'a:0:{}', '10', 10, 0, 0, 'MiMax364GBblack', 'title', 'desc', 'keyw', 0, 0, '21348.00', '23482.00', 1, '', '32022.00'),
(393, 'Xiaomi Mi A2 black 5.99\'(2160x1080) 4GB 64GB 2 Sim 12MPx+20MPx/20Mpix 4G Wi-Fi Android One Snapdragon 625', 11, 1, 1543406894, 0, 'MiA264Gbblack', '30076.00', 'a:2:{i:0;s:114:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=bigimage&id=C9E36548-4A9C-4CAC-9351-F8F57D38C36D\";i:1;s:111:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=image&id=C9E36548-4A9C-4CAC-9351-F8F57D38C36D\";}', 'bot', 0, 'a:0:{}', '10', 0, 0, 0, 'MiA264Gbblack', 'title', 'desc', 'keyw', 0, 0, '20051.00', '22056.00', 1, '', '30076.00'),
(394, 'Xiaomi A1 smartphone 5.5\"(1920x1080) 4Gb 64Gb 2Sim 4G Wi-Fi BT 2x12Mpix Snapdragon 625 And7.1 rose gold', 11, 1, 1, 0, 'MiA164Gbrosegold', '20509.00', 'a:2:{i:0;s:114:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=bigimage&id=6EA3725D-E74F-4E96-8B91-82895B1D22E1\";i:1;s:111:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=image&id=6EA3725D-E74F-4E96-8B91-82895B1D22E1\";}', 'bot', 0, 'a:0:{}', '10', 10, 0, 0, 'MiA164Gbrosegold', 'title', 'desc', 'keyw', 0, 0, '13673.00', '15040.00', 1, '', '20509.00'),
(395, 'Xiaomi Redmi 6A blue smartphone5.45\'(1440x720) 2GB 16GB 2 Sim 4G Wi-Fi 13Mpix/5Mpix  Helio A22', 11, 1, 1, 0, 'Redmi6A16GBblue', '10786.00', 'a:2:{i:0;s:114:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=bigimage&id=06597554-1756-4DA2-B30A-277DE040917F\";i:1;s:111:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=image&id=06597554-1756-4DA2-B30A-277DE040917F\";}', 'bot', 0, 'a:0:{}', '10', 10, 0, 0, 'Redmi6A16GBblue', 'title', 'desc', 'keyw', 0, 0, '7191.00', '7910.00', 1, '', '10786.00'),
(396, 'Xiaomi Mi 8 smartphone 6,21\"(2248 х 1080) 6Gb 64 GB 2Sim 4G Wi-Fi BT 12Mpix+12Mpix/20Mpix Snapdragon 845 Android 8.1 black', 11, 1, 1, 0, 'Mi864GBblack', '52093.00', 'a:2:{i:0;s:114:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=bigimage&id=29C24FBB-6275-401B-831E-403A8AE48656\";i:1;s:111:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=image&id=29C24FBB-6275-401B-831E-403A8AE48656\";}', 'bot', 0, 'a:0:{}', '10', 10, 0, 0, 'Mi864GBblack', 'title', 'desc', 'keyw', 0, 0, '34729.00', '38201.00', 1, '', '52093.00'),
(397, 'Xiaomi Redmi 6 black smartphone 5.45\'(1440x720) 4GB 64GB 2 Sim 4G  Wi-Fi 12Mpix+5Mpix/5Mpix Helio P22', 11, 1, 1, 0, 'Redmi664Gbblack', '20047.00', 'a:2:{i:0;s:114:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=bigimage&id=E35C3B33-019E-40F7-B113-CBC1DB8782BB\";i:1;s:111:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=image&id=E35C3B33-019E-40F7-B113-CBC1DB8782BB\";}', 'bot', 0, 'a:0:{}', '10', 10, 0, 0, 'Redmi664Gbblack', 'title', 'desc', 'keyw', 0, 0, '13365.00', '14701.00', 1, '', '20047.00'),
(398, 'Xiaomi Mi A2 Lite gold smartphone 5.84\"(1080x2280) 3GB 32GB 2 Sim 12Mpix+5Mpix/5Mpix 4G Wi-Fi Android One Snapdragon 625', 11, 1, 1, 0, 'MiA2Lite32Gbgold', '20047.00', 'a:2:{i:0;s:114:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=bigimage&id=759009EE-B4BA-4A07-A989-65CB6F638D1E\";i:1;s:111:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=image&id=759009EE-B4BA-4A07-A989-65CB6F638D1E\";}', 'bot', 0, 'a:0:{}', '10', 10, 0, 0, 'MiA2Lite32Gbgold', 'title', 'desc', 'keyw', 0, 0, '13365.00', '14701.00', 1, '', '20047.00'),
(399, 'Xiaomi Redmi Note 5 black smartphone 5,99\"(2160x1080) 3GB 32GB 2 Sim 12Mpix+5Mpix/13Mpix 4G Wi-Fi Snapdragon 636 (same Redmi Note 5 black 32G)', 11, 1, 1, 0, 'RedmiNote532Gblack', '22707.00', 'a:2:{i:0;s:114:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=bigimage&id=405DD087-76B8-4214-8CB2-522DA9EEDCED\";i:1;s:111:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=image&id=405DD087-76B8-4214-8CB2-522DA9EEDCED\";}', 'bot', 0, 'a:0:{}', '10', 10, 0, 0, 'RedmiNote532Gblack', 'title', 'desc', 'keyw', 0, 0, '15138.00', '16651.00', 1, '', '22707.00'),
(400, 'Xiaomi Redmi 6 black smartphone 5.45\'(1440x720) 3GB 32GB 2 Sim 4G  Wi-Fi 12Mpix+5Mpix/5Mpix Helio P22', 11, 1, 1, 0, 'Redmi632Gbblack', '17539.00', 'a:2:{i:0;s:114:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=bigimage&id=E35C3B33-019E-40F7-B113-CBC1DB8782BB\";i:1;s:111:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=image&id=E35C3B33-019E-40F7-B113-CBC1DB8782BB\";}', 'bot', 0, 'a:0:{}', '10', 10, 0, 0, 'Redmi632Gbblack', 'title', 'desc', 'keyw', 0, 0, '11693.00', '12862.00', 1, '', '17539.00'),
(401, 'Xiaomi Mi 8 smartphone 6,21&quot;(2248 х 1080) 6Gb 128 GB 2Sim 4G Wi-Fi BT 12Mpix+12Mpix/20Mpix Snapdragon 845 Android 8.1 black', 11, -10800, 1543932843, 0, 'Mi8128GBblack', '56559.00', 'a:2:{i:0;s:114:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=bigimage&id=29C24FBB-6275-401B-831E-403A8AE48656\";i:1;s:111:\"https://api.treolan.ru/Webservice2008/ProductImage.ashx?ImageSize=image&id=29C24FBB-6275-401B-831E-403A8AE48656\";}', 'bot', 0, 'a:0:{}', 'a:1:{i:0;s:3:\"112\";}', 0, 0, 0, 'Mi8128GBblack', 'title', 'desc', 'keyw', 0, 0, '37706.00', '41476.00', 1, '', '56559.00'),
(402, 'Тестовый смартфон', 11, 1543525200, 1543917357, 0, '172t361g111', '23000.00', 'a:1:{i:0;s:44:\"/images/c60a2a40d8d67c2b1ba8dd402ff48e19.png\";}', '', 0, 'a:0:{}', 'a:1:{i:0;s:2:\"50\";}', 0, 0, 0, 'smartphonegooglepixel2qq', '', '', '', 0, 1, '19800.00', '21000.00', 1, 'a:2:{i:0;s:1:\"1\";i:1;s:1:\"2\";}', '0.00'),
(403, 'ВелесГлобал', 13, 1543870800, 1543932723, 0, '2536445', '1500.00', 'a:1:{i:0;s:44:\"/images/03496b3f251894fe7136c83015539962.jpg\";}', '', 0, 'a:0:{}', 'a:1:{i:0;s:3:\"111\";}', 0, 5, 0, 'televizor-bmv', '', '', '', 0, 1, '1000.00', '1200.00', 1, 'a:1:{i:0;s:1:\"1\";}', '0.00'),
(404, 'фантик', 16, 1543870800, 1543932933, 0, '45678', '1600.00', 'a:1:{i:0;s:44:\"/images/ce6864cd1519d5567c40716380020160.jpg\";}', '', 0, 'a:0:{}', 'a:1:{i:0;s:2:\"50\";}', 0, 0, 0, 'fantik', '', '', '', 0, 1, '1000.00', '1200.00', 1, 'a:1:{i:0;s:1:\"1\";}', '0.00');

-- --------------------------------------------------------

--
-- Структура таблицы `product_promotion`
--

CREATE TABLE `product_promotion` (
  `id` int(255) NOT NULL,
  `promotion_id` int(255) NOT NULL,
  `product_id` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `product_status`
--

CREATE TABLE `product_status` (
  `id` int(11) NOT NULL,
  `type` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `product_status`
--

INSERT INTO `product_status` (`id`, `type`) VALUES
(1, 'Ожидает проверки'),
(2, 'Требует изменения'),
(3, 'Отклонен'),
(4, 'Одобрен');

-- --------------------------------------------------------

--
-- Структура таблицы `promotion`
--

CREATE TABLE `promotion` (
  `id` int(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `percent` int(3) NOT NULL,
  `owner_id` int(255) NOT NULL,
  `start` int(255) NOT NULL,
  `finish` int(255) NOT NULL,
  `is_active` int(1) NOT NULL,
  `alias` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `promotion`
--

INSERT INTO `promotion` (`id`, `name`, `description`, `percent`, `owner_id`, `start`, `finish`, `is_active`, `alias`) VALUES
(2, 'test', 'ttttttttt tttttttttt ttttttttttt ttttttttttt ttttttttt ttttttt tttttttttt ttttttt', 3, 22, 1532638800, 1532984400, 1, 'asdasd'),
(3, 'test2', 'fgedfg wregrwerg werg wre123', 5, 22, 1532725200, 1538168400, 1, 'test');

-- --------------------------------------------------------

--
-- Структура таблицы `properties`
--

CREATE TABLE `properties` (
  `id` int(255) NOT NULL,
  `name` varchar(1024) NOT NULL,
  `code` varchar(1024) NOT NULL,
  `active` int(1) NOT NULL,
  `sort` int(255) NOT NULL,
  `multi` int(1) NOT NULL,
  `required` int(1) NOT NULL,
  `filter` int(1) NOT NULL,
  `property_description` text NOT NULL,
  `main` int(1) NOT NULL,
  `type` varchar(1024) NOT NULL,
  `template` varchar(1024) NOT NULL,
  `template_filter` varchar(1024) NOT NULL,
  `property_group_id` int(255) NOT NULL,
  `description_field` int(1) NOT NULL,
  `category_id` int(255) NOT NULL,
  `redactor` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `properties`
--

INSERT INTO `properties` (`id`, `name`, `code`, `active`, `sort`, `multi`, `required`, `filter`, `property_description`, `main`, `type`, `template`, `template_filter`, `property_group_id`, `description_field`, `category_id`, `redactor`) VALUES
(1, 'Цвет', 'asdfasasfd', 1, 0, 1, 1, 1, 'dfsggsdfgdf', 1, 'L', 'S', 'CO', 1, 1, 2, 0),
(2, 'test', 'sadfsadgsagsd', 1, 0, 0, 1, 1, 'asdgsadg', 1, 'L', 'S', 'PU', 1, 1, 19, 0),
(3, 'string', 'string', 1, 0, 0, 1, 0, 'string', 0, 'S', 'S', 'BU', 1, 0, 19, 0),
(4, 'Основной цвет', 'color', 1, 1, 1, 1, 1, 'werfgvjksdf jkaerfbh lkjerfwerf', 1, 'L', 'S', 'CO', 1, 1, 19, 0),
(5, 'Размер', 'size', 1, 1, 1, 1, 1, 'test size', 0, 'L', 'S', 'CH', 1, 1, 19, 0),
(6, 'Разрешение экрана', 'screen_resolution', 1, 1, 0, 1, 1, 'разрешение', 0, 'S', 'L', 'BU', 1, 1, 19, 0),
(7, 'Назначение', 'for', 1, 1, 0, 1, 1, 'Назначение', 0, 'S', 'L', 'BU', 2, 1, 19, 0),
(8, 'ssssssssssss', 'ssssssssss', 1, 0, 0, 0, 0, '', 0, 'L', 'S', 'CH', 1, 0, 19, 0),
(9, 'prop', 'prop', 1, 0, 0, 0, 1, '', 1, 'L', 'L', 'PU', 1, 0, 19, 0),
(10, 'list', 'list', 1, 0, 1, 0, 1, 'test', 1, 'L', 'L', 'BU', 2, 0, 19, 0),
(11, 'file', 'file', 1, 1, 0, 0, 0, 'test', 0, 'F', 'L', 'CH', 1, 0, 19, 0),
(12, 'text', 'text', 1, 1, 1, 0, 0, 'multi text', 0, 'T', 'L', 'CH', 2, 1, 19, 0),
(13, 'date', 'date', 1, 12, 1, 0, 0, 'date', 0, 'D', 'L', 'CH', 1, 0, 19, 0),
(16, 'multi string', 'ms', 1, 0, 1, 1, 0, 'test', 0, 'S', 'L', 'CH', 1, 0, 19, 0),
(17, 'Тип процессора', 'mobile-processor-type', 1, 0, 0, 0, 0, '', 0, 'S', 'L', 'CH', 1, 0, 11, 0),
(18, 'Индекс процессора', 'mobile-processor-index', 1, 0, 0, 0, 0, '', 0, 'S', 'L', 'CH', 1, 0, 11, 0),
(19, 'Операционная система', 'mobile-oc', 1, 0, 0, 0, 0, '', 1, 'S', 'S', 'CH', 1, 0, 11, 0),
(20, 'Диагональ экрана, дюйм', 'mobile-display-size', 1, 0, 0, 0, 0, '', 1, 'S', 'L', 'CH', 1, 0, 11, 0),
(21, 'Тип экрана', 'mobile-display-type', 1, 0, 0, 0, 0, '', 0, 'S', 'L', 'CH', 1, 0, 11, 0),
(22, 'Номинальное разрешение экрана', 'mobile-display-resolytion', 1, 0, 0, 0, 0, '', 0, 'S', 'L', 'CH', 1, 0, 11, 0),
(23, 'Объем оперативной памяти, Гб', 'mobile-ram-size', 1, 0, 0, 0, 1, '', 0, 'S', 'L', 'BU', 1, 0, 11, 0),
(24, 'Wi-Fi', 'mobile-wifi', 1, 0, 0, 0, 0, '', 0, 'S', 'L', 'CH', 1, 0, 11, 0),
(25, 'Bluetooth', 'mobile-bluetooth', 1, 0, 0, 0, 0, '', 0, 'S', 'L', 'CH', 1, 0, 11, 0),
(26, 'Тип SIM-карты', 'mobile-sim-type', 1, 0, 0, 0, 0, '', 0, 'S', 'L', 'CH', 1, 0, 11, 0),
(27, 'Тип оперативной памяти', 'mobile-ram-type', 1, 0, 0, 0, 0, '', 0, 'S', 'L', 'CH', 1, 0, 11, 0),
(28, 'Поддержка карт памяти', 'mobile-sd', 1, 0, 0, 0, 0, '', 0, 'S', 'L', 'CH', 1, 0, 11, 0),
(29, 'Встроенная память', 'mobile-memory', 1, 0, 0, 0, 0, '', 0, 'S', 'L', 'CH', 1, 0, 11, 0),
(30, 'Наличие 3G/4G', 'mobile-lte', 1, 0, 0, 0, 0, '', 0, 'S', 'L', 'CH', 1, 0, 11, 0),
(31, 'Время работы', 'mobile-battery-time', 1, 0, 0, 0, 0, '', 0, 'S', 'L', 'CH', 1, 0, 11, 0),
(32, 'Навигация', 'mobile-navigation', 1, 0, 0, 0, 0, '', 0, 'S', 'L', 'CH', 1, 0, 11, 0),
(33, 'Графический процессор', 'mobile-nproc', 1, 0, 0, 0, 0, '', 0, 'S', 'L', 'CH', 1, 0, 11, 0),
(34, 'Звук', 'mobile-sound', 1, 0, 0, 0, 0, '', 0, 'S', 'L', 'CH', 1, 0, 11, 0),
(35, 'Камера', 'mobile-camera', 1, 0, 0, 0, 0, '', 0, 'S', 'L', 'CH', 1, 0, 11, 0),
(36, 'Количество слотов SIM-карт', 'mobile-sim-count', 1, 0, 0, 0, 0, '', 0, 'S', 'L', 'CH', 1, 0, 11, 0),
(37, 'Порты и разъёмы', 'mobile-ports', 1, 0, 0, 0, 0, '', 0, 'S', 'L', 'CH', 1, 0, 11, 0),
(38, 'Функция телефона', 'mobile-call', 1, 0, 0, 0, 0, '', 0, 'S', 'L', 'CH', 1, 0, 11, 0),
(39, 'Дополнительная информация', 'mobile-additional', 1, 0, 0, 0, 0, '', 0, 'S', 'L', 'CH', 1, 0, 11, 0),
(40, 'Размеры, вес', 'mobile-sizes', 1, 0, 0, 0, 0, '', 0, 'S', 'L', 'CH', 1, 0, 11, 0),
(41, 'Материал', 'mobile-material', 1, 0, 0, 0, 0, '', 0, 'S', 'L', 'CH', 1, 0, 11, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `property_enum`
--

CREATE TABLE `property_enum` (
  `id` int(255) NOT NULL,
  `property_id` int(255) NOT NULL,
  `value` varchar(1024) NOT NULL,
  `description` text NOT NULL,
  `file` varchar(1024) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `property_enum`
--

INSERT INTO `property_enum` (`id`, `property_id`, `value`, `description`, `file`) VALUES
(5, 2, 'gsdasgd', '#463adf', '/images/5ba9dabb7991b.jpg'),
(6, 2, 'sadgsgad', '#d31717', '/images/5b9b5f4e980bd.jpg'),
(7, 1, 'green', '#21a730', ''),
(8, 1, 'red', '#e81212', ''),
(9, 1, 'gray', '#3d3d3d', ''),
(10, 4, 'black', '#e3f000', ''),
(11, 4, 'green', '#2ec794', ''),
(12, 5, 'l', 'l', ''),
(13, 5, 'xl', 'xl', ''),
(14, 5, 'm', 'm', ''),
(15, 8, 'Yes', '', ''),
(16, 9, '#fb1313', '#fb1313', '/images/5b9a30bf91527.jpg'),
(17, 9, '#003893', '#00e405', '/images/5b9a30bf92ca2.jpg'),
(18, 9, '#fbf200', '#fbf200', '/images/5b9a30bf9447c.jpg'),
(19, 10, '1', '', ''),
(20, 10, '2', '', ''),
(21, 10, '3', '', ''),
(22, 10, '4', '', ''),
(23, 10, '5', '', ''),
(24, 10, '6', '', '');

-- --------------------------------------------------------

--
-- Структура таблицы `property_group`
--

CREATE TABLE `property_group` (
  `id` int(255) NOT NULL,
  `name` varchar(1024) NOT NULL,
  `active` int(1) NOT NULL,
  `sort` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `property_group`
--

INSERT INTO `property_group` (`id`, `name`, `active`, `sort`) VALUES
(1, 'Основные', 1, 3),
(2, 'Общая информация', 1, 2);

-- --------------------------------------------------------

--
-- Структура таблицы `property_value`
--

CREATE TABLE `property_value` (
  `id` int(255) NOT NULL,
  `property_id` int(255) NOT NULL,
  `product_id` int(255) NOT NULL,
  `value` varchar(1024) NOT NULL,
  `description` text NOT NULL,
  `type` varchar(1024) NOT NULL,
  `value_enum` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `property_value`
--

INSERT INTO `property_value` (`id`, `property_id`, `product_id`, `value`, `description`, `type`, `value_enum`) VALUES
(103, 4, 4, '', '', '', 10),
(104, 4, 4, '', '', '', 11),
(105, 3, 4, 'test', 'test', '', 0),
(117, 1, 3, '', '', '', 7),
(118, 1, 3, '', '', '', 9),
(119, 2, 3, '', '', '', 5),
(120, 5, 3, '', '', '', 13),
(121, 5, 3, '', '', '', 14),
(122, 6, 3, '1920x1080', '', '', 0),
(123, 7, 3, 'Потребительский', '', '', 0),
(124, 1, 12, '', '', '', 7),
(125, 1, 12, '', '', '', 8),
(126, 2, 12, '', '', '', 5),
(127, 5, 12, '', '', '', 13),
(128, 6, 12, '1920x1080', '', '', 0),
(129, 7, 12, 'Потребительский', '', '', 0),
(130, 1, 20, '', '', '', 7),
(131, 1, 20, '', '', '', 9),
(132, 2, 20, '', '', '', 5),
(133, 2, 20, '', '', '', 6),
(134, 5, 20, '', '', '', 12),
(135, 5, 20, '', '', '', 14),
(136, 6, 20, '1232', 'sdasddsa', '', 0),
(137, 7, 20, 'teeeeeeet', 'adsasdasdas', '', 0),
(243, 1, 44, '', '', '', 7),
(244, 1, 44, '', '', '', 8),
(245, 1, 44, '', '', '', 9),
(246, 2, 44, '', '', '', 5),
(247, 2, 44, '', '', '', 6),
(248, 5, 44, '', '', '', 12),
(249, 5, 44, '', '', '', 13),
(250, 5, 44, '', '', '', 14),
(251, 6, 44, '12312x1233', 'sadsa', '', 0),
(826, 1, 18, '', '', '', 8),
(827, 2, 18, '', '', '', 6),
(828, 5, 18, '', '', '', 13),
(829, 8, 18, '', '', '', 15),
(830, 6, 18, '1', 'asdfsadgdfsh', '', 0),
(831, 7, 18, '2', 'пылесос', '', 0),
(832, 12, 18, '3', '', '', 0),
(833, 13, 18, '4', '', '', 0),
(839, 100, 64, 'asdfsafsdafsdf', 'fdgdhfghgfh', 'jklugjg', 0),
(840, 100, 0, '', '', '', 0),
(868, 1, 74, '', '', 'enum', 7),
(869, 1, 74, '', '', 'enum', 9),
(870, 2, 74, '', '', 'enum', 5),
(871, 2, 74, '', '', 'enum', 6),
(872, 3, 74, 'asdsdaasd', '', 'value', 0),
(873, 4, 74, '', '', 'enum', 10),
(874, 5, 74, '', '', 'enum', 12),
(875, 6, 74, 'dsasdasad', '', 'value', 0),
(876, 7, 74, 'sdaasdsad', 'asdfasdf', 'value', 0),
(877, 8, 74, '', '', 'enum', 15),
(878, 12, 74, 'asdfasdf', '', 'multi', 0),
(879, 13, 74, '24.09.2018', '', 'multi', 0),
(1157, 6, 13, 'adsfas', 'asdfsaf', 'value', 0),
(1158, 7, 13, 'asdfsadf', 'asdfsaf', 'value', 0),
(1159, 12, 13, 'sadfdsa', 'fsadfdsa', 'multi', 0),
(1169, 3, 29, 'test', '', 'value', 0),
(1174, 6, 29, 'test', 'test', 'value', 0),
(1175, 7, 29, 'test2test', 'test2test', 'value', 0),
(1178, 12, 29, 'text', 'test text', 'multi', 0),
(1179, 13, 29, '28.09.2018', '', 'multi', 0),
(1180, 16, 29, 'multi string *', '', 'multi', 0),
(1181, 16, 29, 'multi string *2', '', 'multi', 0),
(1182, 16, 29, 'multi string *3', '', 'multi', 0),
(1196, 13, 29, '18.09.2018', '', 'multi', 0),
(1229, 12, 29, 'text', 'test text', 'multi', 0),
(1347, 3, 43, 'sdfasaafsdf', '', 'value', 0),
(1352, 6, 43, 'sdagsadgasdg', 'asdgasdg', 'value', 0),
(1353, 7, 43, 'sadgsadg', 'sdagasdg', 'value', 0),
(1360, 12, 43, 'sagdsgdsda', 'asdgasdg', 'multi', 0),
(1361, 13, 43, '28.09.2018', '', 'multi', 0),
(1362, 16, 43, 'asdgasdgsadg', '', 'multi', 0),
(1391, 1, 13, '', '', 'enum', 7),
(1392, 2, 13, '', '', 'enum', 5),
(1393, 2, 13, '', '', 'enum', 6),
(1394, 3, 13, 'fagsgg', '', 'value', 0),
(1395, 4, 13, '', '', 'enum', 10),
(1396, 5, 13, '', '', 'enum', 13),
(1397, 5, 13, '', '', 'enum', 14),
(1398, 8, 13, '', '', 'enum', 15),
(1399, 9, 13, '', '', 'enum', 17),
(1400, 10, 13, '', '', 'enum', 20),
(1401, 10, 13, '', '', 'enum', 21),
(1402, 10, 13, '', '', 'enum', 22),
(1403, 10, 13, '', '', 'enum', 23),
(1404, 16, 13, 'fsadfsa', '', 'multi', 0),
(1405, 1, 88, '', '', 'enum', 8),
(1406, 2, 88, '', '', 'enum', 5),
(1407, 3, 88, 'dsasdfasd', '', 'value', 0),
(1408, 4, 88, '', '', 'enum', 10),
(1409, 5, 88, '', '', 'enum', 12),
(1410, 6, 88, 'sdfasdfasd', '', 'value', 0),
(1411, 7, 88, 'fasdfsad', '', 'value', 0),
(1412, 8, 88, '', '', 'enum', 15),
(1413, 9, 88, '', '', 'enum', 16),
(1414, 10, 88, '', '', 'enum', 21),
(1415, 10, 88, '', '', 'enum', 22),
(1416, 10, 88, '', '', 'enum', 23),
(1417, 11, 88, '/images/product/b3689a92eafbc04660c84b9e95d81e68.jpg', '', 'value', 0),
(1418, 12, 88, 'sadfsadfasdf', '', 'multi', 0),
(1419, 16, 88, 'asdfasdfasdfsadf', '', 'multi', 0),
(1420, 1, 89, '', '', 'enum', 8),
(1421, 2, 89, '', '', 'enum', 5),
(1422, 3, 89, 'dsasdfasd', '', 'value', 0),
(1423, 4, 89, '', '', 'enum', 10),
(1424, 5, 89, '', '', 'enum', 12),
(1425, 6, 89, 'sdfasdfasd', '', 'value', 0),
(1426, 7, 89, 'fasdfsad', '', 'value', 0),
(1427, 8, 89, '', '', 'enum', 15),
(1428, 9, 89, '', '', 'enum', 16),
(1429, 10, 89, '', '', 'enum', 21),
(1430, 10, 89, '', '', 'enum', 22),
(1431, 10, 89, '', '', 'enum', 23),
(1432, 11, 89, '/images/product/33f1e0edc29ca0e9eb2d6b03ed97db4e.jpg', '', 'value', 0),
(1433, 12, 89, 'sadfsadfasdf', '', 'multi', 0),
(1434, 16, 89, 'asdfasdfasdfsadf', '', 'multi', 0),
(1435, 1, 90, '', '', 'enum', 7),
(1436, 1, 90, '', '', 'enum', 8),
(1437, 2, 90, '', '', 'enum', 5),
(1438, 2, 90, '', '', 'enum', 6),
(1439, 3, 90, 'sadffsdfa', '', 'value', 0),
(1440, 4, 90, '', '', 'enum', 10),
(1441, 5, 90, '', '', 'enum', 12),
(1442, 6, 90, 'safasfasd', 'safdsdfa', 'value', 0),
(1443, 7, 90, 'sdfasdfa', 'sdfasdfa', 'value', 0),
(1444, 9, 90, '', '', 'enum', 17),
(1445, 10, 90, '', '', 'enum', 20),
(1446, 10, 90, '', '', 'enum', 21),
(1447, 10, 90, '', '', 'enum', 22),
(1448, 12, 90, 'sdaffsadfsdaf', '', 'multi', 0),
(1449, 16, 90, 'asdffdsfasdfa', '', 'multi', 0),
(1488, 1, 29, '', '', 'enum', 7),
(1489, 1, 29, '', '', 'enum', 8),
(1490, 1, 29, '', '', 'enum', 9),
(1491, 2, 29, '', '', 'enum', 5),
(1492, 2, 29, '', '', 'enum', 6),
(1493, 4, 29, '', '', 'enum', 10),
(1494, 4, 29, '', '', 'enum', 11),
(1495, 5, 29, '', '', 'enum', 12),
(1496, 5, 29, '', '', 'enum', 13),
(1497, 5, 29, '', '', 'enum', 14),
(1498, 8, 29, '', '', 'enum', 15),
(1499, 9, 29, '', '', 'enum', 18),
(1500, 10, 29, '', '', 'enum', 19),
(1501, 10, 29, '', '', 'enum', 20),
(1502, 10, 29, '', '', 'enum', 21),
(1503, 10, 29, '', '', 'enum', 22),
(1504, 10, 29, '', '', 'enum', 23),
(1505, 10, 29, '', '', 'enum', 24),
(1698, 100, 64, 'asdfsafsdafsdf', 'fdgdhfghgfh', 'jklugjg', 0),
(1699, 100, 0, '', '', '', 0),
(2305, 17, 321, 'A12 Bionic', '', '', 0),
(2306, 18, 321, 'н/д', '', '', 0),
(2307, 19, 321, 'iOS 12', '', '', 0),
(2308, 20, 321, '6.5000', '', '', 0),
(2309, 21, 321, 'OLED', '', '', 0),
(2310, 22, 321, '2688x1242', '', '', 0),
(2311, 23, 321, '4.0000', '', '', 0),
(2312, 24, 321, 'Да', '', '', 0),
(2313, 25, 321, 'Да', '', '', 0),
(2314, 26, 321, 'nano-SIM', '', '', 0),
(2315, 27, 321, 'н/д', '', '', 0),
(2316, 28, 321, 'Нет', '', '', 0),
(2317, 29, 321, '256 Гб', '', '', 0),
(2318, 30, 321, '4G LTE', '', '', 0),
(2319, 31, 321, 'до 65 ч', '', '', 0),
(2320, 32, 321, 'GPS, ГЛОНАСС, Galileo, QZSS', '', '', 0),
(2321, 33, 321, 'A12 Bionic', '', '', 0),
(2322, 34, 321, 'Встроенные динамики и микрофон ', '', '', 0),
(2323, 35, 321, 'двойная 12 Мп, запись видео в 4K, фронтальная камера 7 Мп', '', '', 0),
(2324, 36, 321, '1', '', '', 0),
(2325, 37, 321, 'Lightning', '', '', 0),
(2326, 38, 321, 'Да', '', '', 0),
(2327, 39, 321, 'Аутентификация - Face ID, датчики: трёхосевой гироскоп, акселерометр, барометр, датчик внешней освещённости, уровень защиты от влаги IP68', '', '', 0),
(2328, 40, 321, '157,5x77,4x7,7 мм, 208 г', '', '', 0),
(2329, 41, 321, 'Алюминий', '', '', 0),
(2330, 17, 322, 'A12 Bionic', '', '', 0),
(2331, 18, 322, 'н/д', '', '', 0),
(2332, 19, 322, 'iOS 12', '', '', 0),
(2333, 20, 322, '6.1000', '', '', 0),
(2334, 21, 322, 'IPS', '', '', 0),
(2335, 22, 322, '1792x828', '', '', 0),
(2336, 23, 322, '3.0000', '', '', 0),
(2337, 24, 322, 'Да', '', '', 0),
(2338, 25, 322, 'Да', '', '', 0),
(2339, 26, 322, 'nano-SIM', '', '', 0),
(2340, 27, 322, 'н/д', '', '', 0),
(2341, 28, 322, 'нет', '', '', 0),
(2342, 29, 322, '256 Гб', '', '', 0),
(2343, 30, 322, '4G LTE', '', '', 0),
(2344, 31, 322, 'До 65 ч', '', '', 0),
(2345, 32, 322, 'GPS, ГЛОНАСС, Galileo, QZSS', '', '', 0),
(2346, 33, 322, 'A12 Bionic', '', '', 0),
(2347, 34, 322, 'Встроенные динамики и микрофон ', '', '', 0),
(2348, 35, 322, '12 Мп, запись видео в 4K, фронтальная камера 7 Мп', '', '', 0),
(2349, 36, 322, '1', '', '', 0),
(2350, 37, 322, 'Lightning ', '', '', 0),
(2351, 38, 322, 'Да', '', '', 0),
(2352, 39, 322, 'Аутентификация - Face ID, датчики: гироскоп, акселерометр, барометр, датчик внешней освещённости', '', '', 0),
(2353, 40, 322, '150,9x75.7x8.3 мм, 194 г', '', '', 0),
(2354, 41, 322, 'Алюминий', '', '', 0),
(2355, 17, 323, 'A12 Bionic', '', '', 0),
(2356, 18, 323, 'н/д', '', '', 0),
(2357, 19, 323, 'iOS 12', '', '', 0),
(2358, 20, 323, '6.1000', '', '', 0),
(2359, 21, 323, 'IPS', '', '', 0),
(2360, 22, 323, '1792x828', '', '', 0),
(2361, 23, 323, '3.0000', '', '', 0),
(2362, 24, 323, 'Да', '', '', 0),
(2363, 25, 323, 'Да', '', '', 0),
(2364, 26, 323, 'nano-SIM', '', '', 0),
(2365, 27, 323, 'н/д', '', '', 0),
(2366, 28, 323, 'Нет', '', '', 0),
(2367, 29, 323, '128 Гб', '', '', 0),
(2368, 30, 323, '4G LTE', '', '', 0),
(2369, 31, 323, 'до 65 ч', '', '', 0),
(2370, 32, 323, 'GPS, ГЛОНАСС, Galileo, QZSS', '', '', 0),
(2371, 33, 323, 'A12 Bionic', '', '', 0),
(2372, 34, 323, 'Встроенные динамики и микрофон ', '', '', 0),
(2373, 35, 323, '12 Мп, запись видео в 4K, фронтальная камера 7 Мп', '', '', 0),
(2374, 36, 323, '1', '', '', 0),
(2375, 37, 323, 'Lightning ', '', '', 0),
(2376, 38, 323, 'Да', '', '', 0),
(2377, 39, 323, 'Аутентификация - Face ID, датчики: гироскоп, акселерометр, барометр, датчик внешней освещённости', '', '', 0),
(2378, 40, 323, '150,9x75.7x8.3 мм, 194 г', '', '', 0),
(2379, 41, 323, 'алюминий', '', '', 0),
(2380, 17, 324, 'A12 Bionic', '', '', 0),
(2381, 18, 324, 'н/д', '', '', 0),
(2382, 19, 324, 'iOS 12', '', '', 0),
(2383, 20, 324, '5.8000', '', '', 0),
(2384, 21, 324, 'OLED', '', '', 0),
(2385, 22, 324, '2436x1125', '', '', 0),
(2386, 23, 324, '4.0000', '', '', 0),
(2387, 24, 324, 'Да', '', '', 0),
(2388, 25, 324, 'Да', '', '', 0),
(2389, 26, 324, 'nano-SIM', '', '', 0),
(2390, 27, 324, 'н/д', '', '', 0),
(2391, 28, 324, 'Нет', '', '', 0),
(2392, 29, 324, '256 Гб', '', '', 0),
(2393, 30, 324, '4G LTE', '', '', 0),
(2394, 31, 324, 'до 60 ч', '', '', 0),
(2395, 32, 324, 'GPS, ГЛОНАСС, Galileo, QZSS', '', '', 0),
(2396, 33, 324, 'A12 Bionic', '', '', 0),
(2397, 34, 324, 'Встроенные динамики и микрофон ', '', '', 0),
(2398, 35, 324, 'двойная 12 Мп, запись видео в 4K, фронтальная камера 7 Мп', '', '', 0),
(2399, 36, 324, '1', '', '', 0),
(2400, 37, 324, 'Lightning', '', '', 0),
(2401, 38, 324, 'Да', '', '', 0),
(2402, 39, 324, 'Аутентификация - Face ID, датчики: трёхосевой гироскоп, акселерометр, барометр, датчик внешней освещённости, уровень защиты от влаги IP68', '', '', 0),
(2403, 40, 324, '143,6x70.9x7.7 мм, 177 г', '', '', 0),
(2404, 41, 324, 'Алюминий', '', '', 0),
(2405, 17, 325, 'A12 Bionic', '', '', 0),
(2406, 18, 325, 'н/д', '', '', 0),
(2407, 19, 325, 'iOS 12', '', '', 0),
(2408, 20, 325, '6.1000', '', '', 0),
(2409, 21, 325, 'IPS', '', '', 0),
(2410, 22, 325, '1792x828', '', '', 0),
(2411, 23, 325, '3.0000', '', '', 0),
(2412, 24, 325, 'Да', '', '', 0),
(2413, 25, 325, 'Да', '', '', 0),
(2414, 26, 325, 'nano-SIM', '', '', 0),
(2415, 27, 325, 'н/д', '', '', 0),
(2416, 28, 325, 'Нет', '', '', 0),
(2417, 29, 325, '64 Гб', '', '', 0),
(2418, 30, 325, '4G LTE', '', '', 0),
(2419, 31, 325, 'до 65 ч', '', '', 0),
(2420, 32, 325, 'GPS, ГЛОНАСС, Galileo, QZSS', '', '', 0),
(2421, 33, 325, 'A12 Bionic', '', '', 0),
(2422, 34, 325, 'Встроенные динамики и микрофон ', '', '', 0),
(2423, 35, 325, '12 Мп, запись видео в 4K, фронтальная камера 7 Мп', '', '', 0),
(2424, 36, 325, '1', '', '', 0),
(2425, 37, 325, 'Lightning ', '', '', 0),
(2426, 38, 325, 'Да', '', '', 0),
(2427, 39, 325, 'Аутентификация - Face ID, датчики: гироскоп, акселерометр, барометр, датчик внешней освещённости', '', '', 0),
(2428, 40, 325, '150,9x75.7x8.3 мм, 194 г', '', '', 0),
(2429, 41, 325, 'алюминий', '', '', 0),
(2430, 17, 326, 'A12 Bionic', '', '', 0),
(2431, 18, 326, 'н/д', '', '', 0),
(2432, 19, 326, 'iOS 12', '', '', 0),
(2433, 20, 326, '6.5000', '', '', 0),
(2434, 21, 326, 'OLED', '', '', 0),
(2435, 22, 326, '2688x1242', '', '', 0),
(2436, 23, 326, '4.0000', '', '', 0),
(2437, 24, 326, 'Да', '', '', 0),
(2438, 25, 326, 'Да', '', '', 0),
(2439, 26, 326, 'nano-SIM', '', '', 0),
(2440, 27, 326, 'н/д', '', '', 0),
(2441, 28, 326, 'Нет', '', '', 0),
(2442, 29, 326, '64 Гб', '', '', 0),
(2443, 30, 326, '4G LTE', '', '', 0),
(2444, 31, 326, 'до 65 ч', '', '', 0),
(2445, 32, 326, 'GPS, ГЛОНАСС, Galileo, QZSS', '', '', 0),
(2446, 33, 326, 'A12 Bionic', '', '', 0),
(2447, 34, 326, 'Встроенные динамики и микрофон ', '', '', 0),
(2448, 35, 326, 'двойная 12 Мп, запись видео в 4K, фронтальная камера 7 Мп', '', '', 0),
(2449, 36, 326, '1', '', '', 0),
(2450, 37, 326, 'Lightning', '', '', 0),
(2451, 38, 326, 'Да', '', '', 0),
(2452, 39, 326, 'Аутентификация - Face ID, датчики: трёхосевой гироскоп, акселерометр, барометр, датчик внешней освещённости, уровень защиты от влаги IP68', '', '', 0),
(2453, 40, 326, '157,5x77,4x7,7 мм, 208 г', '', '', 0),
(2454, 41, 326, 'Алюминий', '', '', 0),
(2455, 17, 327, 'A12 Bionic', '', '', 0),
(2456, 18, 327, 'н/д', '', '', 0),
(2457, 19, 327, 'iOS 12', '', '', 0),
(2458, 20, 327, '6.1000', '', '', 0),
(2459, 21, 327, 'IPS', '', '', 0),
(2460, 22, 327, '1792x828', '', '', 0),
(2461, 23, 327, '3.0000', '', '', 0),
(2462, 24, 327, 'Да', '', '', 0),
(2463, 25, 327, 'Да', '', '', 0),
(2464, 26, 327, 'nano-SIM', '', '', 0),
(2465, 27, 327, 'н/д', '', '', 0),
(2466, 28, 327, 'нет', '', '', 0),
(2467, 29, 327, '128 Гб', '', '', 0),
(2468, 30, 327, '4G LTE', '', '', 0),
(2469, 31, 327, 'До 65 ч', '', '', 0),
(2470, 32, 327, 'GPS, ГЛОНАСС, Galileo, QZSS', '', '', 0),
(2471, 33, 327, 'A12 Bionic', '', '', 0),
(2472, 34, 327, 'встроенные динамики и микрофон ', '', '', 0),
(2473, 35, 327, '12 Мп, запись видео в 4K, фронтальная камера 7 Мп', '', '', 0),
(2474, 36, 327, '1', '', '', 0),
(2475, 37, 327, 'Lightning ', '', '', 0),
(2476, 38, 327, 'Да', '', '', 0),
(2477, 39, 327, 'Аутентификация - Face ID, датчики: гироскоп, акселерометр, барометр, датчик внешней освещённости', '', '', 0),
(2478, 40, 327, '150,9x75.7x8.3 мм, 194г', '', '', 0),
(2479, 41, 327, 'алюминий', '', '', 0),
(2480, 17, 328, 'A12 Bionic', '', '', 0),
(2481, 18, 328, 'н/д', '', '', 0),
(2482, 19, 328, 'iOS 12', '', '', 0),
(2483, 20, 328, '5.8000', '', '', 0),
(2484, 21, 328, 'OLED', '', '', 0),
(2485, 22, 328, '2436x1125', '', '', 0),
(2486, 23, 328, '4.0000', '', '', 0),
(2487, 24, 328, 'Да', '', '', 0),
(2488, 25, 328, 'Да', '', '', 0),
(2489, 26, 328, 'nano-SIM', '', '', 0),
(2490, 27, 328, 'н/д', '', '', 0),
(2491, 28, 328, 'Нет', '', '', 0),
(2492, 29, 328, '64 Гб', '', '', 0),
(2493, 30, 328, '4G LTE', '', '', 0),
(2494, 31, 328, 'до 60 ч', '', '', 0),
(2495, 32, 328, 'GPS, ГЛОНАСС, Galileo, QZSS', '', '', 0),
(2496, 33, 328, 'A12 Bionic', '', '', 0),
(2497, 34, 328, 'Встроенные динамики и микрофон ', '', '', 0),
(2498, 35, 328, 'двойная 12 Мп, запись видео в 4K, фронтальная камера 7 Мп', '', '', 0),
(2499, 36, 328, '1', '', '', 0),
(2500, 37, 328, 'Lightning', '', '', 0),
(2501, 38, 328, 'Да', '', '', 0),
(2502, 39, 328, 'Аутентификация - Face ID, датчики: трёхосевой гироскоп, акселерометр, барометр, датчик внешней освещённости, уровень защиты от влаги IP68', '', '', 0),
(2503, 40, 328, '143,6x70.9x7.7 мм, 177 г', '', '', 0),
(2504, 41, 328, 'Алюминий', '', '', 0),
(2505, 17, 329, 'A12 Bionic', '', '', 0),
(2506, 18, 329, 'н/д', '', '', 0),
(2507, 19, 329, 'iOS 12', '', '', 0),
(2508, 20, 329, '5.8000', '', '', 0),
(2509, 21, 329, 'OLED', '', '', 0),
(2510, 22, 329, '2436x1125', '', '', 0),
(2511, 23, 329, '4.0000', '', '', 0),
(2512, 24, 329, 'Да', '', '', 0),
(2513, 25, 329, 'Да', '', '', 0),
(2514, 26, 329, 'nano-SIM', '', '', 0),
(2515, 27, 329, 'н/д', '', '', 0),
(2516, 28, 329, 'Нет', '', '', 0),
(2517, 29, 329, '512 Гб', '', '', 0),
(2518, 30, 329, '4G LTE', '', '', 0),
(2519, 31, 329, 'до 60 ч', '', '', 0),
(2520, 32, 329, 'GPS, ГЛОНАСС, Galileo, QZSS', '', '', 0),
(2521, 33, 329, 'A12 Bionic', '', '', 0),
(2522, 34, 329, 'Встроенные динамики и микрофон ', '', '', 0),
(2523, 35, 329, 'двойная 12 Мп, запись видео в 4K, фронтальная камера 7 Мп', '', '', 0),
(2524, 36, 329, '1', '', '', 0),
(2525, 37, 329, 'Lightning', '', '', 0),
(2526, 38, 329, 'Да', '', '', 0),
(2527, 39, 329, 'Аутентификация - Face ID, датчики: трёхосевой гироскоп, акселерометр, барометр, датчик внешней освещённости, уровень защиты от влаги IP68', '', '', 0),
(2528, 40, 329, '143,6x70.9x7.7 мм, 177 г', '', '', 0),
(2529, 41, 329, 'Алюминий', '', '', 0),
(2530, 17, 330, 'A12 Bionic', '', '', 0),
(2531, 18, 330, 'н/д', '', '', 0),
(2532, 19, 330, 'iOS 12', '', '', 0),
(2533, 20, 330, '5.8000', '', '', 0),
(2534, 21, 330, 'OLED', '', '', 0),
(2535, 22, 330, '2436x1125', '', '', 0),
(2536, 23, 330, '4.0000', '', '', 0),
(2537, 24, 330, 'Да', '', '', 0),
(2538, 25, 330, 'Да', '', '', 0),
(2539, 26, 330, 'nano-SIM', '', '', 0),
(2540, 27, 330, 'н/д', '', '', 0),
(2541, 28, 330, 'Нет', '', '', 0),
(2542, 29, 330, '64 Гб', '', '', 0),
(2543, 30, 330, '4G LTE', '', '', 0),
(2544, 31, 330, 'до 60 ч', '', '', 0),
(2545, 32, 330, 'GPS, ГЛОНАСС, Galileo, QZSS', '', '', 0),
(2546, 33, 330, 'A12 Bionic', '', '', 0),
(2547, 34, 330, 'Встроенные динамики и микрофон ', '', '', 0),
(2548, 35, 330, 'двойная 12 Мп, запись видео в 4K, фронтальная камера 7 Мп', '', '', 0),
(2549, 36, 330, '1', '', '', 0),
(2550, 37, 330, 'Lightning', '', '', 0),
(2551, 38, 330, 'Да', '', '', 0),
(2552, 39, 330, 'Аутентификация - Face ID, датчики: трёхосевой гироскоп, акселерометр, барометр, датчик внешней освещённости, уровень защиты от влаги IP68', '', '', 0),
(2553, 40, 330, '143,6x70.9x7.7 мм, 177 г', '', '', 0),
(2554, 41, 330, 'Алюминий', '', '', 0),
(2555, 17, 331, 'A12 Bionic', '', '', 0),
(2556, 18, 331, 'н/д', '', '', 0),
(2557, 19, 331, 'iOS 12', '', '', 0),
(2558, 20, 331, '6.5000', '', '', 0),
(2559, 21, 331, 'OLED', '', '', 0),
(2560, 22, 331, '2688x1242', '', '', 0),
(2561, 23, 331, '4.0000', '', '', 0),
(2562, 24, 331, 'Да', '', '', 0),
(2563, 25, 331, 'Да', '', '', 0),
(2564, 26, 331, 'nano-SIM', '', '', 0),
(2565, 27, 331, 'н/д', '', '', 0),
(2566, 28, 331, 'Нет', '', '', 0),
(2567, 29, 331, '256 Гб', '', '', 0),
(2568, 30, 331, '4G LTE', '', '', 0),
(2569, 31, 331, 'до 65 ч', '', '', 0),
(2570, 32, 331, 'GPS, ГЛОНАСС, Galileo, QZSS', '', '', 0),
(2571, 33, 331, 'A12 Bionic', '', '', 0),
(2572, 34, 331, 'Встроенные динамики и микрофон ', '', '', 0),
(2573, 35, 331, 'двойная 12 Мп, запись видео в 4K, фронтальная камера 7 Мп', '', '', 0),
(2574, 36, 331, '1', '', '', 0),
(2575, 37, 331, 'Lightning', '', '', 0),
(2576, 38, 331, 'Да', '', '', 0),
(2577, 39, 331, 'Аутентификация - Face ID, датчики: трёхосевой гироскоп, акселерометр, барометр, датчик внешней освещённости, уровень защиты от влаги IP68', '', '', 0),
(2578, 40, 331, '157,5x77,4x7,7 мм, 208 г', '', '', 0),
(2579, 41, 331, 'Алюминий', '', '', 0),
(2580, 17, 332, 'A12 Bionic', '', '', 0),
(2581, 18, 332, 'н/д', '', '', 0),
(2582, 19, 332, 'iOS 12', '', '', 0),
(2583, 20, 332, '6.1000', '', '', 0),
(2584, 21, 332, 'IPS', '', '', 0),
(2585, 22, 332, '1792x828', '', '', 0),
(2586, 23, 332, '3.0000', '', '', 0),
(2587, 24, 332, 'Да', '', '', 0),
(2588, 25, 332, 'Да', '', '', 0),
(2589, 26, 332, 'nano-SIM', '', '', 0),
(2590, 27, 332, 'н/д', '', '', 0),
(2591, 28, 332, 'Нет', '', '', 0),
(2592, 29, 332, '256 Гб', '', '', 0),
(2593, 30, 332, '4G LTE', '', '', 0),
(2594, 31, 332, 'до 65 ч', '', '', 0),
(2595, 32, 332, 'GPS, ГЛОНАСС, Galileo, QZSS', '', '', 0),
(2596, 33, 332, 'A12 Bionic', '', '', 0),
(2597, 34, 332, 'Встроенные динамики и микрофон ', '', '', 0),
(2598, 35, 332, '12 Мп, запись видео в 4K, фронтальная камера 7 Мп', '', '', 0),
(2599, 36, 332, '1', '', '', 0),
(2600, 37, 332, 'Lightning ', '', '', 0),
(2601, 38, 332, 'Да', '', '', 0),
(2602, 39, 332, 'Аутентификация - Face ID, датчики: гироскоп, акселерометр, барометр, датчик внешней освещённости', '', '', 0),
(2603, 40, 332, '150,9x75.7x8.3 мм, 194 г', '', '', 0),
(2604, 41, 332, 'алюминий', '', '', 0),
(2605, 17, 333, 'A12 Bionic', '', '', 0),
(2606, 18, 333, 'н/д', '', '', 0),
(2607, 19, 333, 'iOS 12', '', '', 0),
(2608, 20, 333, '6.5000', '', '', 0),
(2609, 21, 333, 'OLED', '', '', 0),
(2610, 22, 333, '2688x1242', '', '', 0),
(2611, 23, 333, '4.0000', '', '', 0),
(2612, 24, 333, 'Да', '', '', 0),
(2613, 25, 333, 'Да', '', '', 0),
(2614, 26, 333, 'nano-SIM', '', '', 0),
(2615, 27, 333, 'н/д', '', '', 0),
(2616, 28, 333, 'Нет', '', '', 0),
(2617, 29, 333, '64 Гб', '', '', 0),
(2618, 30, 333, '4G LTE', '', '', 0),
(2619, 31, 333, 'до 65 ч', '', '', 0),
(2620, 32, 333, 'GPS, ГЛОНАСС, Galileo, QZSS', '', '', 0),
(2621, 33, 333, 'A12 Bionic', '', '', 0),
(2622, 34, 333, 'Встроенные динамики и микрофон ', '', '', 0),
(2623, 35, 333, 'двойная 12 Мп, запись видео в 4K, фронтальная камера 7 Мп', '', '', 0),
(2624, 36, 333, '1', '', '', 0),
(2625, 37, 333, 'Lightning', '', '', 0),
(2626, 38, 333, 'Да', '', '', 0),
(2627, 39, 333, 'Аутентификация - Face ID, датчики: трёхосевой гироскоп, акселерометр, барометр, датчик внешней освещённости, уровень защиты от влаги IP68', '', '', 0),
(2628, 40, 333, '157,5x77,4x7,7 мм, 208 г', '', '', 0),
(2629, 41, 333, 'Алюминий', '', '', 0),
(2630, 17, 334, 'A12 Bionic', '', '', 0),
(2631, 18, 334, 'н/д', '', '', 0),
(2632, 19, 334, 'iOS 12', '', '', 0),
(2633, 20, 334, '6.5000', '', '', 0),
(2634, 21, 334, 'OLED', '', '', 0),
(2635, 22, 334, '2688x1242', '', '', 0),
(2636, 23, 334, '4.0000', '', '', 0),
(2637, 24, 334, 'Да', '', '', 0),
(2638, 25, 334, 'Да', '', '', 0),
(2639, 26, 334, 'nano-SIM', '', '', 0),
(2640, 27, 334, 'н/д', '', '', 0),
(2641, 28, 334, 'Нет', '', '', 0),
(2642, 29, 334, '256 Гб', '', '', 0),
(2643, 30, 334, '4G LTE', '', '', 0),
(2644, 31, 334, 'до 65 ч', '', '', 0),
(2645, 32, 334, 'GPS, ГЛОНАСС, Galileo, QZSS', '', '', 0),
(2646, 33, 334, 'A12 Bionic', '', '', 0),
(2647, 34, 334, 'Встроенные динамики и микрофон ', '', '', 0),
(2648, 35, 334, 'двойная 12 Мп, запись видео в 4K, фронтальная камера 7 Мп', '', '', 0),
(2649, 36, 334, '1', '', '', 0),
(2650, 37, 334, 'Lightning', '', '', 0),
(2651, 38, 334, 'Да', '', '', 0),
(2652, 39, 334, 'Аутентификация - Face ID, датчики: трёхосевой гироскоп, акселерометр, барометр, датчик внешней освещённости, уровень защиты от влаги IP68', '', '', 0),
(2653, 40, 334, '157,5x77,4x7,7 мм, 208 г', '', '', 0),
(2654, 41, 334, 'Алюминий', '', '', 0),
(2655, 17, 335, 'A12 Bionic', '', '', 0),
(2656, 18, 335, 'н/д', '', '', 0),
(2657, 19, 335, 'iOS 12', '', '', 0),
(2658, 20, 335, '6.1000', '', '', 0),
(2659, 21, 335, 'IPS', '', '', 0),
(2660, 22, 335, '1792x828', '', '', 0),
(2661, 23, 335, '3.0000', '', '', 0),
(2662, 24, 335, 'Да', '', '', 0),
(2663, 25, 335, 'Да', '', '', 0),
(2664, 26, 335, 'nano-SIM', '', '', 0),
(2665, 27, 335, 'н/д', '', '', 0),
(2666, 28, 335, 'Нет', '', '', 0),
(2667, 29, 335, '256 Гб', '', '', 0),
(2668, 30, 335, '4G LTE', '', '', 0),
(2669, 31, 335, 'до 65 ч', '', '', 0),
(2670, 32, 335, 'GPS, ГЛОНАСС, Galileo, QZSS', '', '', 0),
(2671, 33, 335, 'A12 Bionic', '', '', 0),
(2672, 34, 335, 'Встроенные динамики и микрофон ', '', '', 0),
(2673, 35, 335, '12 Мп, запись видео в 4K, фронтальная камера 7 Мп', '', '', 0),
(2674, 36, 335, '1', '', '', 0),
(2675, 37, 335, 'Lightning ', '', '', 0),
(2676, 38, 335, 'Да', '', '', 0),
(2677, 39, 335, 'Аутентификация - Face ID, датчики: гироскоп, акселерометр, барометр, датчик внешней освещённости', '', '', 0),
(2678, 40, 335, '150,9x75.7x8.3 мм, 194 г', '', '', 0),
(2679, 41, 335, 'алюминий', '', '', 0),
(2680, 17, 336, 'A12 Bionic', '', 'value', 0),
(2681, 18, 336, 'н/д', '', 'value', 0),
(2682, 19, 336, 'iOS 12', '', 'value', 0),
(2683, 20, 336, '6.1000', '', 'value', 0),
(2684, 21, 336, 'IPS', '', 'value', 0),
(2685, 22, 336, '1792x828', '', 'value', 0),
(2686, 23, 336, '3.0000', '', 'value', 0),
(2687, 24, 336, 'Да', '', 'value', 0),
(2688, 25, 336, 'Да', '', 'value', 0),
(2689, 26, 336, 'nano-SIM', '', 'value', 0),
(2690, 27, 336, 'н/д', '', 'value', 0),
(2691, 28, 336, 'Нет', '', 'value', 0),
(2692, 29, 336, '128 Гб', '', 'value', 0),
(2693, 30, 336, '4G LTE', '', 'value', 0),
(2694, 31, 336, 'до 65 ч', '', 'value', 0),
(2695, 32, 336, 'GPS, ГЛОНАСС, Galileo, QZSS', '', 'value', 0),
(2696, 33, 336, 'A12 Bionic', '', 'value', 0),
(2697, 34, 336, 'Встроенные динамики и микрофон', '', 'value', 0),
(2698, 35, 336, '12 Мп, запись видео в 4K, фронтальная камера 7 Мп', '', 'value', 0),
(2699, 36, 336, '1', '', 'value', 0),
(2700, 37, 336, 'Lightning', '', 'value', 0),
(2701, 38, 336, 'Да', '', 'value', 0),
(2702, 39, 336, 'Аутентификация - Face ID, датчики: гироскоп, акселерометр, барометр, датчик внешней освещённости', '', 'value', 0),
(2703, 40, 336, '150,9x75.7x8.3 мм, 194 г', '', 'value', 0),
(2704, 41, 336, 'алюминий', '', 'value', 0),
(2705, 19, 337, 'iOS 11', '', 'value', 0),
(2706, 20, 337, '5.8000', '', 'value', 0),
(2707, 21, 337, 'Полноэкранный дисплей OLED Multi-Touch', '', 'value', 0),
(2708, 22, 337, '2436x1125', '', 'value', 0),
(2709, 24, 337, 'Да', '', 'value', 0),
(2710, 25, 337, 'Да', '', 'value', 0),
(2711, 26, 337, 'Nano SIM', '', 'value', 0),
(2712, 29, 337, '256', '', 'value', 0),
(2713, 30, 337, 'Да', '', 'value', 0),
(2714, 31, 337, '21 ч', '', 'value', 0),
(2715, 32, 337, 'GPS/ГЛОНАСС', '', 'value', 0),
(2716, 34, 337, 'Аудиоформаты: AAC-LC, HE-AAC, HE-AAC v2, защищённый AAC, MP3, линейный PCM, Apple Lossless, FLAC, Dolby Digital (AC-3), Dolby Digital Plus (E-AC-3), Audible (форматы 2,3,4, Enhanced Audio,AAX и AAX+)', '', 'value', 0),
(2717, 35, 337, 'Фронтальная: 7 Мп, Задняя: 12 Мп', '', 'value', 0),
(2718, 36, 337, '1', '', 'value', 0),
(2719, 40, 337, '70.9x143.6x7.7 мм, 0.174 кг', '', 'value', 0),
(2720, 17, 338, 'A12 Bionic', '', 'value', 0),
(2721, 18, 338, 'н/д', '', 'value', 0),
(2722, 19, 338, 'iOS 12', '', 'value', 0),
(2723, 20, 338, '6.1000', '', 'value', 0),
(2724, 21, 338, 'IPS', '', 'value', 0),
(2725, 22, 338, '1792x828', '', 'value', 0),
(2726, 23, 338, '3.0000', '', 'value', 0),
(2727, 24, 338, 'Да', '', 'value', 0),
(2728, 25, 338, 'Да', '', 'value', 0),
(2729, 26, 338, 'nano-SIM', '', 'value', 0),
(2730, 27, 338, 'н/д', '', 'value', 0),
(2731, 28, 338, 'Нет', '', 'value', 0),
(2732, 29, 338, '256 Гб', '', 'value', 0),
(2733, 30, 338, '4G LTE', '', 'value', 0),
(2734, 31, 338, 'до 65 ч', '', 'value', 0),
(2735, 32, 338, 'GPS, ГЛОНАСС, Galileo, QZSS', '', 'value', 0),
(2736, 33, 338, 'A12 Bionic', '', 'value', 0),
(2737, 34, 338, 'Встроенные динамики и микрофон', '', 'value', 0),
(2738, 35, 338, '12 Мп, запись видео в 4K, фронтальная камера 7 Мп', '', 'value', 0),
(2739, 36, 338, '1', '', 'value', 0),
(2740, 37, 338, 'Lightning', '', 'value', 0),
(2741, 38, 338, 'Да', '', 'value', 0),
(2742, 39, 338, 'Аутентификация - Face ID, датчики: гироскоп, акселерометр, барометр, датчик внешней освещённости', '', 'value', 0),
(2743, 40, 338, '150,9x75.7x8.3 мм, 194 г', '', 'value', 0),
(2744, 41, 338, 'алюминий', '', 'value', 0),
(2745, 17, 339, 'A12 Bionic', '', 'value', 0),
(2746, 18, 339, 'н/д', '', 'value', 0),
(2747, 19, 339, 'iOS 12', '', 'value', 0),
(2748, 20, 339, '6.1000', '', 'value', 0),
(2749, 21, 339, 'IPS', '', 'value', 0),
(2750, 22, 339, '1792x828', '', 'value', 0),
(2751, 23, 339, '3.0000', '', 'value', 0),
(2752, 24, 339, 'Да', '', 'value', 0),
(2753, 25, 339, 'Да', '', 'value', 0),
(2754, 26, 339, 'nano-SIM', '', 'value', 0),
(2755, 27, 339, 'н/д', '', 'value', 0),
(2756, 28, 339, 'Нет', '', 'value', 0),
(2757, 29, 339, '64 Гб', '', 'value', 0),
(2758, 30, 339, '4G LTE', '', 'value', 0),
(2759, 31, 339, 'до 65 ч', '', 'value', 0),
(2760, 32, 339, 'GPS, ГЛОНАСС, Galileo, QZSS', '', 'value', 0),
(2761, 33, 339, 'A12 Bionic', '', 'value', 0),
(2762, 34, 339, 'Встроенные динамики и микрофон', '', 'value', 0),
(2763, 35, 339, '12 Мп, запись видео в 4K, фронтальная камера 7 Мп', '', 'value', 0),
(2764, 36, 339, '1', '', 'value', 0),
(2765, 37, 339, 'Lightning', '', 'value', 0),
(2766, 38, 339, 'Да', '', 'value', 0),
(2767, 39, 339, 'Аутентификация - Face ID, датчики: гироскоп, акселерометр, барометр, датчик внешней освещённости', '', 'value', 0),
(2768, 40, 339, '150,9x75.7x8.3 мм, 194 г', '', 'value', 0),
(2769, 41, 339, 'алюминий', '', 'value', 0),
(2770, 17, 340, 'A12 Bionic', '', 'value', 0),
(2771, 18, 340, 'н/д', '', 'value', 0),
(2772, 19, 340, 'iOS 12', '', 'value', 0),
(2773, 20, 340, '6.5000', '', 'value', 0),
(2774, 21, 340, 'OLED', '', 'value', 0),
(2775, 22, 340, '2688x1242', '', 'value', 0),
(2776, 23, 340, '4.0000', '', 'value', 0),
(2777, 24, 340, 'Да', '', 'value', 0),
(2778, 25, 340, 'Да', '', 'value', 0),
(2779, 26, 340, 'nano-SIM', '', 'value', 0),
(2780, 27, 340, 'н/д', '', 'value', 0),
(2781, 28, 340, 'Нет', '', 'value', 0),
(2782, 29, 340, '512 Гб', '', 'value', 0),
(2783, 30, 340, '4G LTE', '', 'value', 0),
(2784, 31, 340, 'до 65 ч', '', 'value', 0),
(2785, 32, 340, 'GPS, ГЛОНАСС, Galileo, QZSS', '', 'value', 0),
(2786, 33, 340, 'A12 Bionic', '', 'value', 0),
(2787, 34, 340, 'Встроенные динамики и микрофон', '', 'value', 0),
(2788, 35, 340, 'двойная 12 Мп, запись видео в 4K, фронтальная камера 7 Мп', '', 'value', 0),
(2789, 36, 340, '1', '', 'value', 0),
(2790, 37, 340, 'Lightning', '', 'value', 0),
(2791, 38, 340, 'Да', '', 'value', 0),
(2792, 39, 340, 'Аутентификация - Face ID, датчики: трёхосевой гироскоп, акселерометр, барометр, датчик внешней освещённости, уровень защиты от влаги IP68', '', 'value', 0),
(2793, 40, 340, '157,5x77,4x7,7 мм, 208 г', '', 'value', 0),
(2794, 41, 340, 'Алюминий', '', 'value', 0),
(2795, 17, 341, 'A12 Bionic', '', '', 0),
(2796, 18, 341, 'н/д', '', '', 0),
(2797, 19, 341, 'iOS 12', '', '', 0),
(2798, 20, 341, '6.1000', '', '', 0),
(2799, 21, 341, 'IPS', '', '', 0),
(2800, 22, 341, '1792x828', '', '', 0),
(2801, 23, 341, '3.0000', '', '', 0),
(2802, 24, 341, 'Да', '', '', 0),
(2803, 25, 341, 'Да', '', '', 0),
(2804, 26, 341, 'nano-SIM', '', '', 0),
(2805, 27, 341, 'н/д', '', '', 0),
(2806, 28, 341, 'Нет', '', '', 0),
(2807, 29, 341, '128 Гб', '', '', 0),
(2808, 30, 341, '4G LTE', '', '', 0),
(2809, 31, 341, 'до 65 ч', '', '', 0),
(2810, 32, 341, 'GPS, ГЛОНАСС, Galileo, QZSS', '', '', 0),
(2811, 33, 341, 'A12 Bionic', '', '', 0),
(2812, 34, 341, 'Встроенные динамики и микрофон ', '', '', 0),
(2813, 35, 341, '12 Мп, запись видео в 4K, фронтальная камера 7 Мп', '', '', 0),
(2814, 36, 341, '1', '', '', 0),
(2815, 37, 341, 'Lightning ', '', '', 0),
(2816, 38, 341, 'Да', '', '', 0),
(2817, 39, 341, 'Аутентификация - Face ID, датчики: гироскоп, акселерометр, барометр, датчик внешней освещённости', '', '', 0),
(2818, 40, 341, '150,9x75.7x8.3 мм, 194 г', '', '', 0),
(2819, 41, 341, 'Алюминий', '', '', 0),
(2820, 17, 342, 'A12 Bionic', '', '', 0),
(2821, 18, 342, 'н/д', '', '', 0),
(2822, 19, 342, 'iOS 12', '', '', 0),
(2823, 20, 342, '5.8000', '', '', 0),
(2824, 21, 342, 'OLED', '', '', 0),
(2825, 22, 342, '2436x1125', '', '', 0),
(2826, 23, 342, '4.0000', '', '', 0),
(2827, 24, 342, 'Да', '', '', 0),
(2828, 25, 342, 'Да', '', '', 0),
(2829, 26, 342, 'nano-SIM', '', '', 0),
(2830, 27, 342, 'н/д', '', '', 0),
(2831, 28, 342, 'Нет', '', '', 0),
(2832, 29, 342, '64 Гб', '', '', 0),
(2833, 30, 342, '4G LTE', '', '', 0),
(2834, 31, 342, 'до 60 ч', '', '', 0),
(2835, 32, 342, 'GPS, ГЛОНАСС, Galileo, QZSS', '', '', 0),
(2836, 33, 342, 'A12 Bionic', '', '', 0),
(2837, 34, 342, 'Встроенные динамики и микрофон ', '', '', 0),
(2838, 35, 342, 'двойная 12 Мп, запись видео в 4K, фронтальная камера 7 Мп', '', '', 0),
(2839, 36, 342, '1', '', '', 0),
(2840, 37, 342, 'Lightning', '', '', 0),
(2841, 38, 342, 'Да', '', '', 0),
(2842, 39, 342, 'Аутентификация - Face ID, датчики: трёхосевой гироскоп, акселерометр, барометр, датчик внешней освещённости, уровень защиты от влаги IP68', '', '', 0),
(2843, 40, 342, '143,6x70.9x7.7 мм, 177 г', '', '', 0),
(2844, 41, 342, 'Алюминий', '', '', 0),
(2845, 17, 343, 'A12 Bionic', '', '', 0),
(2846, 18, 343, 'н/д', '', '', 0),
(2847, 19, 343, 'iOS 12', '', '', 0),
(2848, 20, 343, '5.8000', '', '', 0),
(2849, 21, 343, 'OLED', '', '', 0),
(2850, 22, 343, '2436x1125', '', '', 0),
(2851, 23, 343, '4.0000', '', '', 0),
(2852, 24, 343, 'Да', '', '', 0),
(2853, 25, 343, 'Да', '', '', 0),
(2854, 26, 343, 'nano-SIM', '', '', 0),
(2855, 27, 343, 'н/д', '', '', 0),
(2856, 28, 343, 'Нет', '', '', 0),
(2857, 29, 343, '512 Гб', '', '', 0),
(2858, 30, 343, '4G LTE', '', '', 0),
(2859, 31, 343, 'до 60 ч', '', '', 0),
(2860, 32, 343, 'GPS, ГЛОНАСС, Galileo, QZSS', '', '', 0),
(2861, 33, 343, 'A12 Bionic', '', '', 0),
(2862, 34, 343, 'Встроенные динамики и микрофон ', '', '', 0),
(2863, 35, 343, 'двойная 12 Мп, запись видео в 4K, фронтальная камера 7 Мп', '', '', 0),
(2864, 36, 343, '1', '', '', 0),
(2865, 37, 343, 'Lightning', '', '', 0),
(2866, 38, 343, 'Да', '', '', 0),
(2867, 39, 343, 'Аутентификация - Face ID, датчики: трёхосевой гироскоп, акселерометр, барометр, датчик внешней освещённости, уровень защиты от влаги IP68', '', '', 0),
(2868, 40, 343, '143,6x70.9x7.7 мм, 177 г', '', '', 0),
(2869, 41, 343, 'Алюминий', '', '', 0),
(2870, 17, 344, 'A12 Bionic', '', 'value', 0),
(2871, 18, 344, 'н/д', '', 'value', 0),
(2872, 19, 344, 'iOS 12', '', 'value', 0),
(2873, 20, 344, '6.5000', '', 'value', 0),
(2874, 21, 344, 'OLED', '', 'value', 0),
(2875, 22, 344, '2688x1242', '', 'value', 0),
(2876, 23, 344, '4.0000', '', 'value', 0),
(2877, 24, 344, 'Да', '', 'value', 0),
(2878, 25, 344, 'Да', '', 'value', 0),
(2879, 26, 344, 'nano-SIM', '', 'value', 0),
(2880, 27, 344, 'н/д', '', 'value', 0),
(2881, 28, 344, 'Нет', '', 'value', 0),
(2882, 29, 344, '512 Гб', '', 'value', 0),
(2883, 30, 344, '4G LTE', '', 'value', 0),
(2884, 31, 344, 'до 65 ч', '', 'value', 0),
(2885, 32, 344, 'GPS, ГЛОНАСС, Galileo, QZSS', '', 'value', 0),
(2886, 33, 344, 'A12 Bionic', '', 'value', 0),
(2887, 34, 344, 'Встроенные динамики и микрофон', '', 'value', 0),
(2888, 35, 344, 'двойная 12 Мп, запись видео в 4K, фронтальная камера 7 Мп', '', 'value', 0),
(2889, 36, 344, '1', '', 'value', 0),
(2890, 37, 344, 'Lightning', '', 'value', 0),
(2891, 38, 344, 'Да', '', 'value', 0),
(2892, 39, 344, 'Аутентификация - Face ID, датчики: трёхосевой гироскоп, акселерометр, барометр, датчик внешней освещённости, уровень защиты от влаги IP68', '', 'value', 0),
(2893, 40, 344, '157,5x77,4x7,7 мм, 208 г', '', 'value', 0),
(2894, 41, 344, 'Алюминий', '', 'value', 0),
(2895, 17, 345, 'A12 Bionic', '', '', 0),
(2896, 18, 345, 'н/д', '', '', 0),
(2897, 19, 345, 'iOS 12', '', '', 0),
(2898, 20, 345, '6.1000', '', '', 0),
(2899, 21, 345, 'IPS', '', '', 0),
(2900, 22, 345, '1792x828', '', '', 0),
(2901, 23, 345, '3.0000', '', '', 0),
(2902, 24, 345, 'Да', '', '', 0),
(2903, 25, 345, 'Да', '', '', 0),
(2904, 26, 345, 'nano-SIM', '', '', 0),
(2905, 27, 345, 'н/д', '', '', 0),
(2906, 28, 345, 'Нет', '', '', 0),
(2907, 29, 345, '256 Гб', '', '', 0),
(2908, 30, 345, '4G LTE', '', '', 0),
(2909, 31, 345, 'до 65 ч', '', '', 0),
(2910, 32, 345, 'GPS, ГЛОНАСС, Galileo, QZSS', '', '', 0),
(2911, 33, 345, 'A12 Bionic', '', '', 0),
(2912, 34, 345, 'Встроенные динамики и микрофон ', '', '', 0),
(2913, 35, 345, '12 Мп, запись видео в 4K, фронтальная камера 7 Мп', '', '', 0),
(2914, 36, 345, '1', '', '', 0),
(2915, 37, 345, 'Lightning ', '', '', 0),
(2916, 38, 345, 'Да', '', '', 0),
(2917, 39, 345, 'Аутентификация - Face ID, датчики: гироскоп, акселерометр, барометр, датчик внешней освещённости', '', '', 0),
(2918, 40, 345, '150,9x75.7x8.3 мм, 194 г', '', '', 0),
(2919, 41, 345, 'алюминий', '', '', 0),
(3181, 20, 346, '1.8000', '', '', 0),
(3182, 21, 346, 'TFT', '', '', 0),
(3183, 22, 346, '128x160', '', '', 0),
(3184, 23, 346, '32.0000', '', '', 0),
(3185, 25, 346, 'Да', '', '', 0),
(3186, 28, 346, 'MicroSD HC', '', '', 0),
(3187, 29, 346, '32', '', '', 0),
(3188, 34, 346, 'Встроенный динамик и микрофон', '', '', 0),
(3189, 36, 346, '2', '', '', 0),
(3190, 37, 346, '1 разъем для наушников, microUSB', '', '', 0),
(3191, 38, 346, 'Да', '', '', 0),
(3192, 39, 346, 'Тип аккумулятора: Li-Ion', '', '', 0),
(3193, 40, 346, '109х46х14,3 мм, 49 г', '', '', 0),
(3194, 41, 346, 'Пластик', '', '', 0),
(3195, 17, 347, 'MTK6737', '', '', 0),
(3196, 19, 347, 'Android 7.0', '', '', 0),
(3197, 20, 347, '5.5000', '', '', 0),
(3198, 21, 347, 'IPS', '', '', 0),
(3199, 22, 347, '1440x720', '', '', 0),
(3200, 23, 347, '2.0000', '', '', 0),
(3201, 24, 347, 'Да', '', '', 0),
(3202, 25, 347, 'Да', '', '', 0),
(3203, 28, 347, 'Да', '', '', 0),
(3204, 29, 347, '16 Гб', '', '', 0),
(3205, 30, 347, 'Да', '', '', 0),
(3206, 32, 347, 'GPS', '', '', 0),
(3207, 33, 347, 'Mali-T720', '', '', 0),
(3208, 34, 347, 'Встроенный динамик', '', '', 0),
(3209, 35, 347, '13 Мп', '', '', 0),
(3210, 36, 347, '2', '', '', 0),
(3211, 37, 347, 'microUSB, наушники', '', '', 0),
(3212, 38, 347, 'Да', '', '', 0),
(3213, 40, 347, '70,3x147,4x8,9 мм, 163 гр.', '', '', 0),
(3214, 41, 347, 'Пластик/металл', '', '', 0),
(3215, 20, 348, '1.7700', '', '', 0),
(3216, 21, 348, 'TFT', '', '', 0),
(3217, 22, 348, '128x160', '', '', 0),
(3218, 24, 348, 'Нет', '', '', 0),
(3219, 25, 348, 'Да', '', '', 0),
(3220, 28, 348, 'micro SD', '', '', 0),
(3221, 29, 348, '32', '', '', 0),
(3222, 30, 348, 'Нет', '', '', 0),
(3223, 34, 348, 'Встроенный динамик и микрофон', '', '', 0),
(3224, 35, 348, '0.08', '', '', 0),
(3225, 36, 348, '2', '', '', 0),
(3226, 37, 348, '1 разъем для наушников, microUSB', '', '', 0),
(3227, 38, 348, 'Да', '', '', 0),
(3228, 39, 348, 'Зарядка от USB', '', '', 0),
(3229, 40, 348, '95x45,2x18 мм, 69 г', '', '', 0),
(3230, 41, 348, 'Пластик', '', '', 0),
(3231, 20, 349, '1.8000', '', '', 0),
(3232, 21, 349, 'TFT', '', '', 0),
(3233, 22, 349, '128x160', '', '', 0),
(3234, 23, 349, '32.0000', '', '', 0),
(3235, 25, 349, 'Да', '', '', 0),
(3236, 28, 349, 'MicroSD HC', '', '', 0),
(3237, 29, 349, '32', '', '', 0),
(3238, 34, 349, 'Встроенный динамик и микрофон', '', '', 0),
(3239, 36, 349, '2', '', '', 0),
(3240, 37, 349, '1 разъем для наушников, microUSB', '', '', 0),
(3241, 38, 349, 'Да', '', '', 0),
(3242, 39, 349, 'Тип аккумулятора: Li-Ion', '', '', 0),
(3243, 40, 349, '109х46х14,3 мм, 49 г', '', '', 0),
(3244, 41, 349, 'Пластик', '', '', 0),
(3245, 20, 350, '1.7700', '', '', 0),
(3246, 21, 350, 'TFT', '', '', 0),
(3247, 22, 350, '128x160', '', '', 0),
(3248, 23, 350, '32.0000', '', '', 0),
(3249, 25, 350, 'Да', '', '', 0),
(3250, 28, 350, 'MicroSD HC', '', '', 0),
(3251, 29, 350, '32', '', '', 0),
(3252, 34, 350, 'Встроенный динамик и микрофон', '', '', 0),
(3253, 35, 350, '0,08', '', '', 0),
(3254, 36, 350, '2', '', '', 0),
(3255, 37, 350, '1 разъем для наушников, microUSB', '', '', 0),
(3256, 39, 350, 'Тип аккумулятора: Li-Ion', '', '', 0),
(3257, 40, 350, '95х45,2х18 мм, 69 г', '', '', 0),
(3258, 41, 350, 'Пластик', '', '', 0),
(3259, 20, 351, '1.8000', '', '', 0),
(3260, 21, 351, 'TFT', '', '', 0),
(3261, 22, 351, '128x160', '', '', 0),
(3262, 23, 351, '32.0000', '', '', 0),
(3263, 25, 351, 'Да', '', '', 0),
(3264, 28, 351, 'MicroSD HC', '', '', 0),
(3265, 29, 351, '32', '', '', 0),
(3266, 34, 351, 'Встроенный динамик и микрофон', '', '', 0),
(3267, 36, 351, '2', '', '', 0),
(3268, 37, 351, '1 разъем для наушников, microUSB', '', '', 0),
(3269, 38, 351, 'Да', '', '', 0),
(3270, 39, 351, 'Тип аккумулятора: Li-Ion', '', '', 0),
(3271, 40, 351, '109х46х14,3 мм, 49 г', '', '', 0),
(3272, 41, 351, 'Пластик', '', '', 0),
(3273, 17, 352, 'MT6737', '', '', 0),
(3274, 19, 352, 'Android 7.0', '', '', 0),
(3275, 20, 352, '5.0000', '', '', 0),
(3276, 21, 352, 'IPS', '', '', 0),
(3277, 22, 352, '1280x720', '', '', 0),
(3278, 23, 352, '1.0000', '', '', 0),
(3279, 24, 352, 'Да', '', '', 0),
(3280, 25, 352, 'Да', '', '', 0),
(3281, 28, 352, 'Да', '', '', 0),
(3282, 29, 352, '8', '', '', 0),
(3283, 30, 352, 'Да', '', '', 0),
(3284, 32, 352, 'GPS', '', '', 0),
(3285, 33, 352, 'Mali-T720', '', '', 0),
(3286, 34, 352, 'Встроенный динамик', '', '', 0),
(3287, 35, 352, '8 Мп', '', '', 0),
(3288, 36, 352, '2', '', '', 0),
(3289, 37, 352, 'microUSB, наушники', '', '', 0),
(3290, 38, 352, 'Да', '', '', 0),
(3291, 40, 352, '72,2x146x9,2 мм, 155 гр.', '', '', 0),
(3292, 41, 352, 'Пластик', '', '', 0),
(3293, 20, 353, '1.7700', '', '', 0),
(3294, 21, 353, 'TFT', '', '', 0),
(3295, 22, 353, '128x160', '', '', 0),
(3296, 23, 353, '32.0000', '', '', 0),
(3297, 25, 353, 'Да', '', '', 0),
(3298, 28, 353, 'MicroSD HC', '', '', 0),
(3299, 29, 353, '32', '', '', 0),
(3300, 34, 353, 'Встроенный динамик и микрофон', '', '', 0),
(3301, 35, 353, '0,08', '', '', 0),
(3302, 36, 353, '2', '', '', 0),
(3303, 37, 353, '1 разъем для наушников, microUSB', '', '', 0),
(3304, 39, 353, 'Тип аккумулятора: Li-Ion', '', '', 0),
(3305, 40, 353, '95х45,2х18 мм, 69 г', '', '', 0),
(3306, 41, 353, 'Пластик', '', '', 0),
(3307, 20, 354, '1.7700', '', '', 0),
(3308, 21, 354, 'TFT', '', '', 0),
(3309, 22, 354, '128x160', '', '', 0),
(3310, 23, 354, '32.0000', '', '', 0),
(3311, 25, 354, 'Да', '', '', 0),
(3312, 28, 354, 'MicroSD HC', '', '', 0),
(3313, 29, 354, '32', '', '', 0),
(3314, 34, 354, 'Встроенный динамик и микрофон', '', '', 0),
(3315, 35, 354, '0,08', '', '', 0),
(3316, 36, 354, '2', '', '', 0),
(3317, 37, 354, '1 разъем для наушников, microUSB', '', '', 0),
(3318, 38, 354, 'Да', '', '', 0),
(3319, 39, 354, 'Тип аккумулятора: Li-Ion', '', '', 0),
(3320, 40, 354, '95х45,2х18 мм, 69 г', '', '', 0),
(3321, 41, 354, 'Пластик', '', '', 0),
(3322, 20, 355, '1.7700', '', '', 0),
(3323, 21, 355, 'TFT', '', '', 0),
(3324, 22, 355, '128x160', '', '', 0),
(3325, 24, 355, 'Нет', '', '', 0),
(3326, 25, 355, 'Да', '', '', 0),
(3327, 28, 355, 'micro SD', '', '', 0),
(3328, 29, 355, '32', '', '', 0),
(3329, 30, 355, 'Нет', '', '', 0),
(3330, 34, 355, 'Встроенный динамик и микрофон', '', '', 0),
(3331, 35, 355, '0.08', '', '', 0),
(3332, 36, 355, '2', '', '', 0),
(3333, 37, 355, '1 разъем для наушников, microUSB', '', '', 0),
(3334, 38, 355, 'Да', '', '', 0),
(3335, 39, 355, 'Зарядка от USB', '', '', 0),
(3336, 40, 355, '95x45,2x18 мм, 69 г', '', '', 0),
(3337, 41, 355, 'Пластик', '', '', 0),
(3338, 20, 356, '1.7700', '', '', 0),
(3339, 21, 356, 'TFT', '', '', 0),
(3340, 22, 356, '128x160', '', '', 0),
(3341, 24, 356, 'Нет', '', '', 0),
(3342, 25, 356, 'Да', '', '', 0),
(3343, 28, 356, 'micro SD', '', '', 0),
(3344, 29, 356, '32', '', '', 0),
(3345, 30, 356, 'Нет', '', '', 0),
(3346, 34, 356, 'Встроенный динамик и микрофон', '', '', 0),
(3347, 35, 356, '0.08', '', '', 0),
(3348, 36, 356, '2', '', '', 0),
(3349, 37, 356, '1 разъем для наушников, microUSB', '', '', 0),
(3350, 38, 356, 'Да', '', '', 0),
(3351, 39, 356, 'Зарядка от USB', '', '', 0),
(3352, 40, 356, '95x45,2x18 мм, 69 г', '', '', 0),
(3353, 41, 356, 'Пластик', '', '', 0),
(3354, 17, 357, '6531DA', '', '', 0),
(3355, 20, 357, '2.4000', '', '', 0),
(3356, 21, 357, 'TFT', '', '', 0),
(3357, 22, 357, '240x320', '', '', 0),
(3358, 25, 357, 'Да', '', '', 0),
(3359, 28, 357, 'micro SD', '', '', 0),
(3360, 29, 357, '32', '', '', 0),
(3361, 34, 357, '1 динамик, встроенный микрофон', '', '', 0),
(3362, 35, 357, '0.08 Мпикс', '', '', 0),
(3363, 36, 357, '2', '', '', 0),
(3364, 37, 357, 'microUSB, mini jack', '', '', 0),
(3365, 38, 357, 'Да', '', '', 0),
(3366, 39, 357, 'Тип аккумулятора: Li-Ion', '', '', 0),
(3367, 40, 357, '101,6x51.7x16 мм, 79 гр', '', '', 0),
(3368, 41, 357, 'Пластик', '', '', 0),
(3369, 20, 358, '1.7700', '', '', 0),
(3370, 21, 358, 'TFT', '', '', 0),
(3371, 22, 358, '128x160', '', '', 0),
(3372, 24, 358, 'Нет', '', '', 0),
(3373, 25, 358, 'Да', '', '', 0),
(3374, 28, 358, 'micro SD', '', '', 0),
(3375, 29, 358, '32', '', '', 0),
(3376, 30, 358, 'Нет', '', '', 0),
(3377, 34, 358, 'Встроенный динамик и микрофон', '', '', 0),
(3378, 35, 358, '0.08', '', '', 0),
(3379, 36, 358, '2', '', '', 0),
(3380, 37, 358, '1 разъем для наушников, microUSB', '', '', 0),
(3381, 38, 358, 'Да', '', '', 0),
(3382, 39, 358, 'Зарядка от USB', '', '', 0),
(3383, 40, 358, '95x45,2x18 мм, 69 г', '', '', 0),
(3384, 41, 358, 'Пластик', '', '', 0),
(3385, 17, 359, 'MT6737', '', '', 0),
(3386, 19, 359, 'Android 7.0', '', '', 0),
(3387, 20, 359, '5.0000', '', '', 0),
(3388, 21, 359, 'IPS', '', '', 0),
(3389, 22, 359, '1280x720', '', '', 0),
(3390, 23, 359, '1.0000', '', '', 0),
(3391, 24, 359, 'Да', '', '', 0),
(3392, 25, 359, 'Да', '', '', 0),
(3393, 28, 359, 'Да', '', '', 0),
(3394, 29, 359, '8', '', '', 0),
(3395, 30, 359, 'Да', '', '', 0),
(3396, 32, 359, 'GPS', '', '', 0),
(3397, 33, 359, 'Mali-T720', '', '', 0),
(3398, 34, 359, 'Встроенный динамик', '', '', 0),
(3399, 35, 359, '8 Мп', '', '', 0),
(3400, 36, 359, '2', '', '', 0),
(3401, 37, 359, 'microUSB, наушники', '', '', 0),
(3402, 38, 359, 'Да', '', '', 0),
(3403, 40, 359, '72,2x146x9,2 мм, 155 гр.', '', '', 0),
(3404, 41, 359, 'Пластик', '', '', 0),
(3405, 20, 360, '2.4000', '', '', 0),
(3406, 21, 360, 'TFT', '', '', 0),
(3407, 22, 360, '240x320', '', '', 0),
(3408, 24, 360, 'Нет', '', '', 0),
(3409, 25, 360, 'Да', '', '', 0),
(3410, 28, 360, 'microSD', '', '', 0),
(3411, 29, 360, '32 Мб', '', '', 0),
(3412, 30, 360, 'Нет', '', '', 0),
(3413, 32, 360, 'Нет', '', '', 0),
(3414, 34, 360, 'Встроенный динамик и микрофон', '', '', 0),
(3415, 35, 360, 'Основная: 0.08 Мпикс', '', '', 0),
(3416, 36, 360, '2', '', '', 0),
(3417, 37, 360, 'Разъем для наушников, microUSB', '', '', 0),
(3418, 38, 360, 'Да', '', '', 0),
(3419, 39, 360, 'Зарядка от USB', '', '', 0),
(3420, 40, 360, '119x52x14.5 мм, 0.089 кг', '', '', 0),
(3421, 41, 360, 'Пластик', '', '', 0),
(3422, 17, 361, 'MT6737', '', '', 0),
(3423, 19, 361, 'Android 7.0', '', '', 0),
(3424, 20, 361, '5.0000', '', '', 0),
(3425, 21, 361, 'IPS', '', '', 0),
(3426, 22, 361, '1280x720', '', '', 0),
(3427, 23, 361, '1.0000', '', '', 0),
(3428, 24, 361, 'Да', '', '', 0),
(3429, 25, 361, 'Да', '', '', 0),
(3430, 28, 361, 'Да', '', '', 0),
(3431, 29, 361, '8', '', '', 0),
(3432, 30, 361, 'Да', '', '', 0),
(3433, 32, 361, 'GPS', '', '', 0),
(3434, 33, 361, 'Mali-T720', '', '', 0),
(3435, 34, 361, 'Встроенный динамик', '', '', 0),
(3436, 35, 361, '8 Мп', '', '', 0),
(3437, 36, 361, '2', '', '', 0),
(3438, 37, 361, 'microUSB, наушники', '', '', 0),
(3439, 38, 361, 'Да', '', '', 0),
(3440, 40, 361, '72,2x146x9,2 мм, 155 гр.', '', '', 0),
(3441, 41, 361, 'Пластик', '', '', 0),
(3442, 39, 362, 'Емкость: 20000 мАч, поддержка быстрой зарядки: Qualcomm Quick Charge 3.0, рабочая температура: от 0С до +45С', '', '', 0),
(3443, 40, 362, '69,6x149,5x23,9 мм, 328 гр ', '', '', 0),
(3444, 39, 363, 'IP: 5 ATM, рабочая температура: от -10С до +50С', '', 'value', 0),
(3445, 40, 363, '17,9x46,9x12 мм, 20 гр', '', 'value', 0),
(3446, 39, 364, 'Емкость:10000 мАч', '', '', 0),
(3447, 40, 364, '71x147x14 мм, 240 гр', '', '', 0),
(3448, 19, 365, 'Android', '', '', 0),
(3449, 20, 365, '5.9900', '', '', 0),
(3450, 21, 365, 'IPS', '', '', 0),
(3451, 22, 365, '2160x1080', '', '', 0),
(3452, 23, 365, '6.0000', '', '', 0),
(3453, 24, 365, 'Да', '', '', 0),
(3454, 25, 365, 'Да', '', '', 0),
(3455, 26, 365, 'nano-SIM', '', '', 0),
(3456, 27, 365, 'LPDDR4', '', '', 0),
(3457, 28, 365, 'Нет', '', '', 0),
(3458, 29, 365, '64 Гб', '', '', 0),
(3459, 30, 365, 'Да', '', '', 0),
(3460, 32, 365, 'GPS/ГЛОНАСС/BeiDou', '', '', 0),
(3461, 33, 365, 'Adreno 540', '', '', 0),
(3462, 35, 365, '12 Мп, автофокус, оптическая стабилизация, режим макросъемки, макс.разрешение видео: 3840x2160', '', '', 0),
(3463, 36, 365, '2', '', '', 0),
(3464, 37, 365, 'USB Type C', '', '', 0),
(3465, 39, 365, 'Автоповорот экрана, устойчивое к царапинам стекло, функция быстрой зарядки', '', '', 0),
(3466, 40, 365, '75,5x151,8x7,7 мм, 185 гр.', '', '', 0),
(3467, 41, 365, 'Керамика', '', '', 0),
(3468, 17, 366, 'Qualcomm Snapdragon 845', '', '', 0),
(3469, 19, 366, 'Android 8.0', '', '', 0),
(3470, 20, 366, '5.9900', '', '', 0),
(3471, 22, 366, '2160x1080', '', '', 0),
(3472, 23, 366, '6.0000', '', '', 0),
(3473, 24, 366, 'Да', '', '', 0),
(3474, 25, 366, 'Да', '', '', 0),
(3475, 26, 366, 'nano-SIM', '', '', 0),
(3476, 28, 366, 'Нет', '', '', 0),
(3477, 29, 366, '128 Гб', '', '', 0),
(3478, 30, 366, 'Да', '', '', 0),
(3479, 32, 366, 'GPS/ГЛОНАСС/BeiDou', '', '', 0),
(3480, 33, 366, 'Adreno 630', '', '', 0),
(3481, 35, 366, '12 Мп+12 Мп, 5 Мп фронтальная камера', '', '', 0),
(3482, 36, 366, '2', '', '', 0),
(3483, 37, 366, 'USB Type-C', '', '', 0),
(3484, 38, 366, 'Да', '', '', 0),
(3485, 40, 366, '74,9x150,86x8,1 мм, 189 гр', '', '', 0),
(3486, 17, 367, 'Qualcomm Snapdragon 450', '', '', 0),
(3487, 19, 367, 'Android', '', '', 0),
(3488, 20, 367, '5.7000', '', '', 0),
(3489, 22, 367, '1440x720', '', '', 0),
(3490, 23, 367, '3.0000', '', '', 0),
(3491, 24, 367, 'Да', '', '', 0),
(3492, 25, 367, 'Да', '', '', 0),
(3493, 28, 367, 'Да', '', '', 0),
(3494, 29, 367, '32 Гб', '', '', 0),
(3495, 30, 367, 'Да', '', '', 0),
(3496, 32, 367, 'GPS/ГЛОНАСС/BeiDou', '', '', 0),
(3497, 33, 367, 'Adreno 506', '', '', 0),
(3498, 34, 367, 'Встроенный динамик', '', '', 0),
(3499, 35, 367, '12 Мп, автофокус, макс.разрешение видео: 1920x1080', '', '', 0),
(3500, 36, 367, '2', '', '', 0),
(3501, 37, 367, 'microUSB, наушники', '', '', 0),
(3502, 38, 367, 'Да', '', '', 0),
(3503, 40, 367, '72,8x151,8x7,7 мм, 157 гр.', '', '', 0),
(3504, 41, 367, 'Металл', '', '', 0),
(3505, 17, 368, 'Mediatek Helio P22', '', '', 0),
(3506, 19, 368, 'Android', '', '', 0),
(3507, 20, 368, '5.4500', '', '', 0),
(3508, 22, 368, '1440x720', '', '', 0),
(3509, 23, 368, '4.0000', '', '', 0),
(3510, 24, 368, 'Да', '', '', 0),
(3511, 25, 368, 'Да', '', '', 0),
(3512, 26, 368, 'nano-SIM', '', '', 0),
(3513, 28, 368, 'Да', '', '', 0),
(3514, 29, 368, '64 Гб', '', '', 0),
(3515, 30, 368, 'Да', '', '', 0),
(3516, 32, 368, 'GPS/ГЛОНАСС/BeiDou', '', '', 0),
(3517, 33, 368, 'PowerVR GE8320', '', '', 0),
(3518, 35, 368, '12 Мп и 5 Мп', '', '', 0),
(3519, 36, 368, '2', '', '', 0),
(3520, 37, 368, 'micro-USB', '', '', 0),
(3521, 38, 368, 'Да', '', '', 0),
(3522, 39, 368, 'Сканер отпечатков пальцев на задней панели, Слот для SIM-карт 2 + 1 (две SIM-карты и карта памяти)', '', '', 0),
(3523, 40, 368, '71,5x147,5x8,3 мм, 146 гр', '', '', 0),
(3524, 17, 369, 'MediaTek Helio P22', '', '', 0),
(3525, 19, 369, 'Android', '', '', 0),
(3526, 20, 369, '5.4500', '', '', 0),
(3527, 22, 369, '1440x720', '', '', 0),
(3528, 23, 369, '3.0000', '', '', 0),
(3529, 24, 369, 'Да', '', '', 0),
(3530, 25, 369, 'Да', '', '', 0),
(3531, 26, 369, 'Nano-Sim', '', '', 0),
(3532, 28, 369, 'Да', '', '', 0),
(3533, 29, 369, '32 Гб', '', '', 0),
(3534, 30, 369, 'Да', '', '', 0),
(3535, 32, 369, 'GPS/ГЛОНАСС/BeiDou', '', '', 0),
(3536, 34, 369, 'Встроенный динамик', '', '', 0),
(3537, 35, 369, '12 Мп и 5 Мп, автофокус, вспышка', '', '', 0),
(3538, 36, 369, '2', '', '', 0),
(3539, 37, 369, 'USB, наушники', '', '', 0),
(3540, 38, 369, 'Да', '', '', 0),
(3541, 40, 369, '71,5x147,5x8,3 мм, 146 гр', '', '', 0),
(3542, 17, 370, 'Qualcomm Snapdragon 636', '', '', 0),
(3543, 19, 370, 'Android', '', '', 0),
(3544, 20, 370, '6.2600', '', '', 0),
(3545, 22, 370, '2280x1080', '', '', 0),
(3546, 23, 370, '4.0000', '', '', 0),
(3547, 24, 370, 'Да', '', '', 0),
(3548, 25, 370, 'Да', '', '', 0),
(3549, 26, 370, 'nano-SIM', '', '', 0),
(3550, 27, 370, 'LPDDR4X', '', '', 0),
(3551, 28, 370, 'Да', '', '', 0),
(3552, 29, 370, '64 Гб', '', '', 0),
(3553, 30, 370, 'Да', '', '', 0),
(3554, 32, 370, 'GPS/ГЛОНАСС/BeiDou', '', '', 0),
(3555, 33, 370, 'Adreno 509', '', '', 0),
(3556, 35, 370, '12 Мп и 5 Мп, 20 Мп и 2 Мп фронтальная камера', '', '', 0),
(3557, 36, 370, '2', '', '', 0),
(3558, 37, 370, 'micro-USB', '', '', 0),
(3559, 38, 370, 'Да', '', '', 0),
(3560, 40, 370, '76,4x157,9x8,26 мм, 182 гр', '', '', 0),
(3561, 17, 371, 'Qualcomm Snapdragon 625', '', '', 0),
(3562, 19, 371, 'Android', '', '', 0),
(3563, 20, 371, '5.9900', '', '', 0),
(3564, 22, 371, '1440x720', '', '', 0),
(3565, 23, 371, '3.0000', '', '', 0),
(3566, 24, 371, 'Да', '', '', 0),
(3567, 25, 371, 'Да', '', '', 0),
(3568, 26, 371, 'Nano-Sim', '', '', 0),
(3569, 28, 371, 'Да', '', '', 0),
(3570, 29, 371, '32 Гб', '', '', 0),
(3571, 30, 371, 'Да', '', '', 0),
(3572, 32, 371, 'GPS/ГЛОНАСС/BeiDou', '', '', 0),
(3573, 33, 371, 'Adreno 506', '', '', 0),
(3574, 34, 371, 'Встроенный динамик', '', '', 0),
(3575, 35, 371, '12 Мп и 5 Мп, автофокус, вспышка', '', '', 0),
(3576, 36, 371, '2', '', '', 0),
(3577, 37, 371, 'USB, наушники', '', '', 0),
(3578, 38, 371, 'Да', '', '', 0),
(3579, 40, 371, '77,26x160,73x8,1 мм', '', '', 0);
INSERT INTO `property_value` (`id`, `property_id`, `product_id`, `value`, `description`, `type`, `value_enum`) VALUES
(3580, 41, 371, 'Металл', '', '', 0),
(3581, 17, 372, 'Qualcomm Snapdragon 660', '', '', 0),
(3582, 19, 372, 'Android One', '', '', 0),
(3583, 20, 372, '5.9900', '', '', 0),
(3584, 22, 372, '2160x1080', '', '', 0),
(3585, 23, 372, '4.0000', '', '', 0),
(3586, 24, 372, 'Да', '', '', 0),
(3587, 25, 372, 'Да', '', '', 0),
(3588, 28, 372, 'Нет', '', '', 0),
(3589, 29, 372, '64 Гб', '', '', 0),
(3590, 30, 372, 'Да', '', '', 0),
(3591, 32, 372, 'GPS/ГЛОНАСС/Beidou', '', '', 0),
(3592, 33, 372, 'Adreno 512', '', '', 0),
(3593, 35, 372, '12 Мп+20 Мп, вспышка, съемка с ИИ', '', '', 0),
(3594, 36, 372, '2', '', '', 0),
(3595, 38, 372, 'Да', '', '', 0),
(3596, 39, 372, 'Поддержка Qualcomm Quick Charge 3.0 ', '', '', 0),
(3597, 40, 372, '75,4x158,7x7,3 мм, 168 гр', '', '', 0),
(3598, 17, 373, 'Qualcomm Snapdragon 625', '', '', 0),
(3599, 19, 373, 'Android', '', '', 0),
(3600, 20, 373, '5.9900', '', '', 0),
(3601, 22, 373, '1440x720', '', '', 0),
(3602, 23, 373, '4.0000', '', '', 0),
(3603, 24, 373, 'Да', '', '', 0),
(3604, 25, 373, 'Да', '', '', 0),
(3605, 26, 373, 'Nano-Sim', '', '', 0),
(3606, 28, 373, 'Да', '', '', 0),
(3607, 29, 373, '64 Гб', '', '', 0),
(3608, 30, 373, 'Да', '', '', 0),
(3609, 32, 373, 'GPS/ГЛОНАСС/BeiDou', '', '', 0),
(3610, 33, 373, 'Adreno 506', '', '', 0),
(3611, 34, 373, 'Встроенный динамик', '', '', 0),
(3612, 35, 373, '12 Мп и 5 Мп, автофокус, вспышка', '', '', 0),
(3613, 36, 373, '2', '', '', 0),
(3614, 37, 373, 'USB, наушники', '', '', 0),
(3615, 38, 373, 'Да', '', '', 0),
(3616, 40, 373, '77,26x160,73x8,1 мм', '', '', 0),
(3617, 41, 373, 'Металл', '', '', 0),
(3618, 17, 374, 'Mediatek Helio A22', '', 'value', 0),
(3619, 19, 374, 'Android', '', 'value', 0),
(3620, 20, 374, '5.4500', '', 'value', 0),
(3621, 22, 374, '1440x720', '', 'value', 0),
(3622, 23, 374, '2.0000', '', 'value', 0),
(3623, 24, 374, 'Да', '', 'value', 0),
(3624, 25, 374, 'Да', '', 'value', 0),
(3625, 26, 374, 'Nano-Sim', '', 'value', 0),
(3626, 28, 374, 'Да', '', 'value', 0),
(3627, 29, 374, '16 Гб', '', 'value', 0),
(3628, 30, 374, 'Да', '', 'value', 0),
(3629, 32, 374, 'GPS/ГЛОНАСС/BeiDou', '', 'value', 0),
(3630, 34, 374, 'Встроенный динамик', '', 'value', 0),
(3631, 35, 374, '13 Мп', '', 'value', 0),
(3632, 36, 374, '2', '', 'value', 0),
(3633, 37, 374, 'USB, наушники', '', 'value', 0),
(3634, 38, 374, 'Да', '', 'value', 0),
(3635, 40, 374, '71,5x147,5x8,3 мм, 145 гр', '', 'value', 0),
(3636, 17, 375, 'MediaTek Helio P22', '', '', 0),
(3637, 19, 375, 'Android', '', '', 0),
(3638, 20, 375, '5.4500', '', '', 0),
(3639, 22, 375, '1440x720', '', '', 0),
(3640, 23, 375, '4.0000', '', '', 0),
(3641, 24, 375, 'Да', '', '', 0),
(3642, 25, 375, 'Да', '', '', 0),
(3643, 26, 375, 'Nano-Sim', '', '', 0),
(3644, 28, 375, 'Да', '', '', 0),
(3645, 29, 375, '64 Гб', '', '', 0),
(3646, 30, 375, 'Да', '', '', 0),
(3647, 32, 375, 'GPS/ГЛОНАСС/BeiDou', '', '', 0),
(3648, 34, 375, 'Встроенный динамик', '', '', 0),
(3649, 35, 375, '12 Мп и 5 Мп, автофокус, вспышка', '', '', 0),
(3650, 36, 375, '2', '', '', 0),
(3651, 37, 375, 'USB, наушники', '', '', 0),
(3652, 38, 375, 'Да', '', '', 0),
(3653, 40, 375, '71,5x147,5x8,3 мм, 146 гр', '', '', 0),
(3654, 17, 376, 'Qualcomm Snapdragon 636', '', '', 0),
(3655, 19, 376, 'Android', '', '', 0),
(3656, 20, 376, '6.2600', '', '', 0),
(3657, 22, 376, '2280x1080', '', '', 0),
(3658, 23, 376, '4.0000', '', '', 0),
(3659, 24, 376, 'Да', '', '', 0),
(3660, 25, 376, 'Да', '', '', 0),
(3661, 26, 376, 'nano-SIM', '', '', 0),
(3662, 27, 376, 'LPDDR4X', '', '', 0),
(3663, 28, 376, 'Да', '', '', 0),
(3664, 29, 376, '64 Гб', '', '', 0),
(3665, 30, 376, 'Да', '', '', 0),
(3666, 32, 376, 'GPS/ГЛОНАСС/BeiDou', '', '', 0),
(3667, 33, 376, 'Adreno 509', '', '', 0),
(3668, 35, 376, '12 Мп и 5 Мп, 20 Мп и 2 Мп фронтальная камера', '', '', 0),
(3669, 36, 376, '2', '', '', 0),
(3670, 37, 376, 'micro-USB', '', '', 0),
(3671, 38, 376, 'Да', '', '', 0),
(3672, 40, 376, '76,4x157,9x8,26 мм, 182 гр', '', '', 0),
(3673, 17, 377, 'Qualcomm Snapdragon 636', '', '', 0),
(3674, 19, 377, 'Android', '', '', 0),
(3675, 20, 377, '5.9900', '', '', 0),
(3676, 22, 377, '2160x1080', '', '', 0),
(3677, 23, 377, '4.0000', '', '', 0),
(3678, 24, 377, 'Да', '', '', 0),
(3679, 25, 377, 'Да', '', '', 0),
(3680, 28, 377, 'Да', '', '', 0),
(3681, 29, 377, '64 Гб', '', '', 0),
(3682, 30, 377, 'Да', '', '', 0),
(3683, 32, 377, 'GPS/ГЛОНАСС/BeiDou', '', '', 0),
(3684, 33, 377, 'Adreno 509', '', '', 0),
(3685, 34, 377, 'Встроенный динамик', '', '', 0),
(3686, 35, 377, '12 Мп и 5 Мп, автофокус, вспышка', '', '', 0),
(3687, 36, 377, '2', '', '', 0),
(3688, 37, 377, 'USB, наушники', '', '', 0),
(3689, 38, 377, 'Да', '', '', 0),
(3690, 40, 377, '75,45x158,5x8,05 мм, 181 гр', '', '', 0),
(3691, 41, 377, 'Алюминий и пластик', '', '', 0),
(3692, 17, 378, 'Qualcomm Snapdragon 625', '', '', 0),
(3693, 19, 378, 'Android', '', '', 0),
(3694, 20, 378, '5.9900', '', '', 0),
(3695, 22, 378, '1440x720', '', '', 0),
(3696, 23, 378, '3.0000', '', '', 0),
(3697, 24, 378, 'Да', '', '', 0),
(3698, 25, 378, 'Да', '', '', 0),
(3699, 26, 378, 'Nano-Sim', '', '', 0),
(3700, 28, 378, 'Да', '', '', 0),
(3701, 29, 378, '32 Гб', '', '', 0),
(3702, 30, 378, 'Да', '', '', 0),
(3703, 32, 378, 'GPS/ГЛОНАСС/BeiDou', '', '', 0),
(3704, 33, 378, 'Adreno 506', '', '', 0),
(3705, 34, 378, 'Встроенный динамик', '', '', 0),
(3706, 35, 378, '12 Мп и 5 Мп, автофокус, вспышка', '', '', 0),
(3707, 36, 378, '2', '', '', 0),
(3708, 37, 378, 'USB, наушники', '', '', 0),
(3709, 38, 378, 'Да', '', '', 0),
(3710, 40, 378, '77,26x160,73x8,1 мм', '', '', 0),
(3711, 41, 378, 'Металл', '', '', 0),
(3712, 17, 379, 'Qualcomm Snapdragon 636', '', '', 0),
(3713, 19, 379, 'Android', '', '', 0),
(3714, 20, 379, '6.2600', '', '', 0),
(3715, 22, 379, '2280x1080', '', '', 0),
(3716, 23, 379, '3.0000', '', '', 0),
(3717, 24, 379, 'Да', '', '', 0),
(3718, 25, 379, 'Да', '', '', 0),
(3719, 26, 379, 'nano-SIM', '', '', 0),
(3720, 27, 379, 'LPDDR4X', '', '', 0),
(3721, 28, 379, 'Да', '', '', 0),
(3722, 29, 379, '32 Гб', '', '', 0),
(3723, 30, 379, 'Да', '', '', 0),
(3724, 32, 379, 'GPS/ГЛОНАСС/BeiDou', '', '', 0),
(3725, 33, 379, 'Adreno 509', '', '', 0),
(3726, 35, 379, '12 Мп и 5 Мп, 20 Мп и 2 Мп фронтальная камера', '', '', 0),
(3727, 36, 379, '2', '', '', 0),
(3728, 37, 379, 'micro-USB', '', '', 0),
(3729, 38, 379, 'Да', '', '', 0),
(3730, 40, 379, '76,4x157,9x8,26 мм, 182 гр', '', '', 0),
(3731, 17, 380, 'Qualcomm Snapdragon 660', '', '', 0),
(3732, 19, 380, 'Android', '', '', 0),
(3733, 20, 380, '5.9900', '', '', 0),
(3734, 22, 380, '2160x1080', '', '', 0),
(3735, 23, 380, '4.0000', '', '', 0),
(3736, 24, 380, 'Да', '', '', 0),
(3737, 25, 380, 'Да', '', '', 0),
(3738, 26, 380, 'Nano-Sim', '', '', 0),
(3739, 28, 380, 'Нет', '', '', 0),
(3740, 29, 380, '64 Гб', '', '', 0),
(3741, 30, 380, 'Да', '', '', 0),
(3742, 32, 380, 'GPS/ГЛОНАСС/BeiDou', '', '', 0),
(3743, 33, 380, 'Adreno 512', '', '', 0),
(3744, 34, 380, 'Встроенный динамик', '', '', 0),
(3745, 35, 380, '20 Мп+12 Мп, автофокус, портретный режим с ИИ, вспышка', '', '', 0),
(3746, 36, 380, '2', '', '', 0),
(3747, 37, 380, 'USB Type-C', '', '', 0),
(3748, 38, 380, 'Да', '', '', 0),
(3749, 40, 380, '75,4x158,7x7,3 мм, 168 гр', '', '', 0),
(3750, 41, 380, 'Алюминий', '', '', 0),
(3751, 17, 381, 'Qualcomm Snapdragon 636', '', '', 0),
(3752, 19, 381, 'Android', '', '', 0),
(3753, 20, 381, '6.2600', '', '', 0),
(3754, 22, 381, '2280x1080', '', '', 0),
(3755, 23, 381, '3.0000', '', '', 0),
(3756, 24, 381, 'Да', '', '', 0),
(3757, 25, 381, 'Да', '', '', 0),
(3758, 26, 381, 'nano-SIM', '', '', 0),
(3759, 27, 381, 'LPDDR4X', '', '', 0),
(3760, 28, 381, 'Да', '', '', 0),
(3761, 29, 381, '32 Гб', '', '', 0),
(3762, 30, 381, 'Да', '', '', 0),
(3763, 32, 381, 'GPS/ГЛОНАСС/BeiDou', '', '', 0),
(3764, 33, 381, 'Adreno 509', '', '', 0),
(3765, 35, 381, '12 Мп и 5 Мп, 20 Мп и 2 Мп фронтальная камера', '', '', 0),
(3766, 36, 381, '2', '', '', 0),
(3767, 37, 381, 'micro-USB', '', '', 0),
(3768, 38, 381, 'Да', '', '', 0),
(3769, 40, 381, '76,4x157,9x8,26 мм, 182 гр', '', '', 0),
(3770, 17, 382, 'Mediatek Helio A22', '', '', 0),
(3771, 19, 382, 'Android', '', '', 0),
(3772, 20, 382, '5.4500', '', '', 0),
(3773, 22, 382, '1440x720', '', '', 0),
(3774, 23, 382, '2.0000', '', '', 0),
(3775, 24, 382, 'Да', '', '', 0),
(3776, 25, 382, 'Да', '', '', 0),
(3777, 26, 382, 'Nano-Sim', '', '', 0),
(3778, 28, 382, 'Да', '', '', 0),
(3779, 29, 382, '32 Гб', '', '', 0),
(3780, 30, 382, 'Да', '', '', 0),
(3781, 32, 382, 'GPS/ГЛОНАСС/BeiDou', '', '', 0),
(3782, 34, 382, 'Встроенный динамик', '', '', 0),
(3783, 35, 382, '13 Мп', '', '', 0),
(3784, 36, 382, '2', '', '', 0),
(3785, 37, 382, 'USB, наушники', '', '', 0),
(3786, 38, 382, 'Да', '', '', 0),
(3787, 40, 382, '71,5x147,5x8,3 мм, 145 гр', '', '', 0),
(3788, 17, 383, 'Qualcomm Snapdragon 636', '', '', 0),
(3789, 19, 383, 'Android', '', '', 0),
(3790, 20, 383, '6.2600', '', '', 0),
(3791, 22, 383, '2280x1080', '', '', 0),
(3792, 23, 383, '3.0000', '', '', 0),
(3793, 24, 383, 'Да', '', '', 0),
(3794, 25, 383, 'Да', '', '', 0),
(3795, 26, 383, 'nano-SIM', '', '', 0),
(3796, 27, 383, 'LPDDR4X', '', '', 0),
(3797, 28, 383, 'Да', '', '', 0),
(3798, 29, 383, '32 Гб', '', '', 0),
(3799, 30, 383, 'Да', '', '', 0),
(3800, 32, 383, 'GPS/ГЛОНАСС/BeiDou', '', '', 0),
(3801, 33, 383, 'Adreno 509', '', '', 0),
(3802, 35, 383, '12 Мп и 5 Мп, 20 Мп и 2 Мп фронтальная камера', '', '', 0),
(3803, 36, 383, '2', '', '', 0),
(3804, 37, 383, 'micro-USB', '', '', 0),
(3805, 38, 383, 'Да', '', '', 0),
(3806, 40, 383, '76,4x157,9x8,26 мм, 182 гр', '', '', 0),
(3807, 17, 384, 'Qualcomm Snapdragon 625', '', '', 0),
(3808, 19, 384, 'Android', '', '', 0),
(3809, 20, 384, '5.8400', '', '', 0),
(3810, 22, 384, '2280x1080', '', '', 0),
(3811, 23, 384, '3.0000', '', '', 0),
(3812, 24, 384, 'Да', '', '', 0),
(3813, 25, 384, 'Да', '', '', 0),
(3814, 26, 384, 'Nano-Sim', '', '', 0),
(3815, 28, 384, 'Да', '', '', 0),
(3816, 29, 384, '32 Гб', '', '', 0),
(3817, 30, 384, 'Да', '', '', 0),
(3818, 32, 384, 'GPS/ГЛОНАСС/BeiDou', '', '', 0),
(3819, 33, 384, 'Adreno 506', '', '', 0),
(3820, 34, 384, 'Встроенный динамик', '', '', 0),
(3821, 35, 384, '12 Мп и 5 Мп, автофокус, вспышка', '', '', 0),
(3822, 36, 384, '2', '', '', 0),
(3823, 37, 384, 'USB, наушники', '', '', 0),
(3824, 38, 384, 'Да', '', '', 0),
(3825, 40, 384, '71,68x149,33x8,75 мм, 178 гр', '', '', 0),
(3826, 41, 384, 'Пластик', '', '', 0),
(3827, 17, 385, 'Qualcomm Snapdragon 625', '', '', 0),
(3828, 19, 385, 'Android', '', '', 0),
(3829, 20, 385, '5.8400', '', '', 0),
(3830, 22, 385, '2280x1080', '', '', 0),
(3831, 23, 385, '3.0000', '', '', 0),
(3832, 24, 385, 'Да', '', '', 0),
(3833, 25, 385, 'Да', '', '', 0),
(3834, 26, 385, 'Nano-Sim', '', '', 0),
(3835, 28, 385, 'Да', '', '', 0),
(3836, 29, 385, '32 Гб', '', '', 0),
(3837, 30, 385, 'Да', '', '', 0),
(3838, 32, 385, 'GPS/ГЛОНАСС/BeiDou', '', '', 0),
(3839, 33, 385, 'Adreno 506', '', '', 0),
(3840, 34, 385, 'Встроенный динамик', '', '', 0),
(3841, 35, 385, '12 Мп и 5 Мп, автофокус, вспышка', '', '', 0),
(3842, 36, 385, '2', '', '', 0),
(3843, 37, 385, 'USB, наушники', '', '', 0),
(3844, 38, 385, 'Да', '', '', 0),
(3845, 40, 385, '71,68x149,33x8,75 мм, 178 гр', '', '', 0),
(3846, 41, 385, 'Пластик', '', '', 0),
(3847, 17, 386, 'Mediatek Helio A22', '', '', 0),
(3848, 19, 386, 'Android', '', '', 0),
(3849, 20, 386, '5.4500', '', '', 0),
(3850, 22, 386, '1440x720', '', '', 0),
(3851, 23, 386, '2.0000', '', '', 0),
(3852, 24, 386, 'Да', '', '', 0),
(3853, 25, 386, 'Да', '', '', 0),
(3854, 26, 386, 'Nano-Sim', '', '', 0),
(3855, 28, 386, 'Да', '', '', 0),
(3856, 29, 386, '16 Гб', '', '', 0),
(3857, 30, 386, 'Да', '', '', 0),
(3858, 32, 386, 'GPS/ГЛОНАСС/BeiDou', '', '', 0),
(3859, 34, 386, 'Встроенный динамик', '', '', 0),
(3860, 35, 386, '13 Мп', '', '', 0),
(3861, 36, 386, '2', '', '', 0),
(3862, 37, 386, 'USB, наушники', '', '', 0),
(3863, 38, 386, 'Да', '', '', 0),
(3864, 40, 386, '71,5x147,5x8,3 мм, 145 гр', '', '', 0),
(3865, 17, 387, 'Qualcomm Snapdragon 845', '', '', 0),
(3866, 19, 387, 'Android 8.1', '', '', 0),
(3867, 20, 387, '6.2100', '', '', 0),
(3868, 21, 387, 'Amoled', '', '', 0),
(3869, 22, 387, '2248x1080', '', '', 0),
(3870, 23, 387, '6.0000', '', '', 0),
(3871, 24, 387, 'Да', '', '', 0),
(3872, 25, 387, 'Да', '', '', 0),
(3873, 26, 387, 'nano-SIM', '', '', 0),
(3874, 27, 387, 'LPDDR4x', '', '', 0),
(3875, 28, 387, 'Нет', '', '', 0),
(3876, 29, 387, '64 Гб', '', '', 0),
(3877, 30, 387, 'Да', '', '', 0),
(3878, 32, 387, 'GPS/ГЛОНАСС/Beidou/Галилео/QZSS', '', '', 0),
(3879, 33, 387, 'Adreno 630', '', '', 0),
(3880, 35, 387, '12 Мп, двойная камера, 4-осевая оптическая стабилизация, вспышка', '', '', 0),
(3881, 36, 387, '2', '', '', 0),
(3882, 37, 387, 'USB Type-C', '', '', 0),
(3883, 38, 387, 'Да', '', '', 0),
(3884, 39, 387, 'Цветовая гамма DCI-P3, поддержка режима активного дисплея, поддержка Регулировка цветовой температуры/Стандартный режим, поддержка Дневной режим/Режим чтения, Сканер отпечатков пальцев на задней панели, Поддержка Quick Charge 4+ зарядки', '', '', 0),
(3885, 40, 387, '74,8x154,9x7,6 мм, 175 гр', '', '', 0),
(3886, 41, 387, 'Алюминий, стекло', '', '', 0),
(3887, 17, 388, 'Qualcomm Snapdragon 636', '', '', 0),
(3888, 19, 388, 'Android', '', '', 0),
(3889, 20, 388, '5.9900', '', '', 0),
(3890, 22, 388, '2160x1080', '', '', 0),
(3891, 23, 388, '3.0000', '', '', 0),
(3892, 24, 388, 'Да', '', '', 0),
(3893, 25, 388, 'Да', '', '', 0),
(3894, 28, 388, 'Да', '', '', 0),
(3895, 29, 388, '32 Гб', '', '', 0),
(3896, 30, 388, 'Да', '', '', 0),
(3897, 32, 388, 'GPS/ГЛОНАСС/BeiDou', '', '', 0),
(3898, 33, 388, 'Adreno 509', '', '', 0),
(3899, 34, 388, 'Встроенный динамик', '', '', 0),
(3900, 35, 388, '12 Мп и 5 Мп, автофокус, вспышка', '', '', 0),
(3901, 36, 388, '2', '', '', 0),
(3902, 37, 388, 'USB, наушники', '', '', 0),
(3903, 38, 388, 'Да', '', '', 0),
(3904, 40, 388, '75,45x158,5x8,05 мм, 181 гр', '', '', 0),
(3905, 41, 388, 'Алюминий и пластик', '', '', 0),
(3906, 19, 389, 'Android', '', '', 0),
(3907, 20, 389, '5.7000', '', '', 0),
(3908, 22, 389, '1440x720', '', '', 0),
(3909, 23, 389, '2.0000', '', '', 0),
(3910, 24, 389, 'Да', '', '', 0),
(3911, 25, 389, 'Да', '', '', 0),
(3912, 26, 389, 'micro-Sim/nano-Sim', '', '', 0),
(3913, 28, 389, 'Да', '', '', 0),
(3914, 29, 389, '16 Гб', '', '', 0),
(3915, 30, 389, 'Да', '', '', 0),
(3916, 32, 389, 'GPS/ГЛОНАСС/BEIDOU', '', '', 0),
(3917, 33, 389, 'Adreno 506', '', '', 0),
(3918, 35, 389, '12 Мп, автофокус', '', '', 0),
(3919, 36, 389, '2', '', '', 0),
(3920, 37, 389, 'USB', '', '', 0),
(3921, 38, 389, 'Да', '', '', 0),
(3922, 39, 389, 'Сканер отпечатка пальца', '', '', 0),
(3923, 40, 389, '72,8x151,8x7,7 мм, 157 гр', '', '', 0),
(3924, 41, 389, 'Металл', '', '', 0),
(3925, 17, 390, 'MediaTek Helio P22', '', '', 0),
(3926, 19, 390, 'Android', '', '', 0),
(3927, 20, 390, '5.4500', '', '', 0),
(3928, 22, 390, '1440x720', '', '', 0),
(3929, 23, 390, '3.0000', '', '', 0),
(3930, 24, 390, 'Да', '', '', 0),
(3931, 25, 390, 'Да', '', '', 0),
(3932, 26, 390, 'Nano-Sim', '', '', 0),
(3933, 28, 390, 'Да', '', '', 0),
(3934, 29, 390, '32 Гб', '', '', 0),
(3935, 30, 390, 'Да', '', '', 0),
(3936, 32, 390, 'GPS/ГЛОНАСС/BeiDou', '', '', 0),
(3937, 34, 390, 'Встроенный динамик', '', '', 0),
(3938, 35, 390, '12 Мп и 5 Мп, автофокус, вспышка', '', '', 0),
(3939, 36, 390, '2', '', '', 0),
(3940, 37, 390, 'USB, наушники', '', '', 0),
(3941, 38, 390, 'Да', '', '', 0),
(3942, 40, 390, '71,5x147,5x8,3 мм, 146 гр', '', '', 0),
(3943, 17, 391, 'Qualcomm Snapdragon 636', '', '', 0),
(3944, 19, 391, 'Android', '', '', 0),
(3945, 20, 391, '6.2600', '', '', 0),
(3946, 22, 391, '2280x1080', '', '', 0),
(3947, 23, 391, '4.0000', '', '', 0),
(3948, 24, 391, 'Да', '', '', 0),
(3949, 25, 391, 'Да', '', '', 0),
(3950, 26, 391, 'nano-SIM', '', '', 0),
(3951, 27, 391, 'LPDDR4X', '', '', 0),
(3952, 28, 391, 'Да', '', '', 0),
(3953, 29, 391, '64 Гб', '', '', 0),
(3954, 30, 391, 'Да', '', '', 0),
(3955, 32, 391, 'GPS/ГЛОНАСС/BeiDou', '', '', 0),
(3956, 33, 391, 'Adreno 509', '', '', 0),
(3957, 35, 391, '12 Мп и 5 Мп, 20 Мп и 2 Мп фронтальная камера', '', '', 0),
(3958, 36, 391, '2', '', '', 0),
(3959, 37, 391, 'micro-USB', '', '', 0),
(3960, 38, 391, 'Да', '', '', 0),
(3961, 40, 391, '76,4x157,9x8,26 мм, 182 гр', '', '', 0),
(3962, 17, 392, 'Qualcomm Snapdragon 636', '', '', 0),
(3963, 19, 392, 'Android 8.1', '', '', 0),
(3964, 20, 392, '6.9000', '', '', 0),
(3965, 22, 392, '2160x1080', '', '', 0),
(3966, 23, 392, '4.0000', '', '', 0),
(3967, 24, 392, 'Да', '', '', 0),
(3968, 25, 392, 'Да', '', '', 0),
(3969, 26, 392, 'nano-SIM', '', '', 0),
(3970, 27, 392, 'LPDDR4x', '', '', 0),
(3971, 28, 392, 'Да', '', '', 0),
(3972, 29, 392, '64 Гб', '', '', 0),
(3973, 30, 392, 'Да', '', '', 0),
(3974, 32, 392, 'GPS/ГЛОНАСС/BeiDou', '', '', 0),
(3975, 33, 392, 'Adreno 509', '', '', 0),
(3976, 35, 392, '12 Мп и 5 Мп, 8 Мп фронтальная камера, автофокус, вспышка', '', '', 0),
(3977, 36, 392, '2', '', '', 0),
(3978, 37, 392, 'USB Type-C', '', '', 0),
(3979, 38, 392, 'Да', '', '', 0),
(3980, 40, 392, '87,4x176,15x7,99 мм, 221 гр', '', '', 0),
(3981, 41, 392, 'Металл', '', '', 0),
(3982, 17, 393, 'Qualcomm Snapdragon 660', '', '', 0),
(3983, 19, 393, 'Android', '', '', 0),
(3984, 20, 393, '5.9900', '', '', 0),
(3985, 22, 393, '2160x1080', '', '', 0),
(3986, 23, 393, '4.0000', '', '', 0),
(3987, 24, 393, 'Да', '', '', 0),
(3988, 25, 393, 'Да', '', '', 0),
(3989, 26, 393, 'Nano-Sim', '', '', 0),
(3990, 28, 393, 'Нет', '', '', 0),
(3991, 29, 393, '64 Гб', '', '', 0),
(3992, 30, 393, 'Да', '', '', 0),
(3993, 32, 393, 'GPS/ГЛОНАСС/BeiDou', '', '', 0),
(3994, 33, 393, 'Adreno 512', '', '', 0),
(3995, 34, 393, 'Встроенный динамик', '', '', 0),
(3996, 35, 393, '20 Мп+12 Мп, автофокус, портретный режим с ИИ, вспышка', '', '', 0),
(3997, 36, 393, '2', '', '', 0),
(3998, 37, 393, 'USB Type-C', '', '', 0),
(3999, 38, 393, 'Да', '', '', 0),
(4000, 40, 393, '75,4x158,7x7,3 мм, 168 гр', '', '', 0),
(4001, 41, 393, 'Алюминий', '', '', 0),
(4002, 19, 394, 'Android 7.1', '', '', 0),
(4003, 20, 394, '5.5000', '', '', 0),
(4004, 22, 394, '1920x1080', '', '', 0),
(4005, 23, 394, '4.0000', '', '', 0),
(4006, 24, 394, 'Да', '', '', 0),
(4007, 25, 394, 'Да', '', '', 0),
(4008, 26, 394, 'nano-Sim', '', '', 0),
(4009, 28, 394, 'Да', '', '', 0),
(4010, 29, 394, '64 Гб', '', '', 0),
(4011, 30, 394, 'Да', '', '', 0),
(4012, 32, 394, 'GPS/ГЛОНАСС/BEIDOU', '', '', 0),
(4013, 33, 394, 'Adreno 506', '', '', 0),
(4014, 35, 394, '12 Мп, двойная камера, автофокус, широкоугольный объектив, двойная вспышка', '', '', 0),
(4015, 36, 394, '2', '', '', 0),
(4016, 37, 394, 'USB', '', '', 0),
(4017, 38, 394, 'Да', '', '', 0),
(4018, 39, 394, 'Сканер отпечатка пальца', '', '', 0),
(4019, 40, 394, '75,8x155,4x7,3 мм, 165 гр.', '', '', 0),
(4020, 41, 394, 'Металл', '', '', 0),
(4021, 17, 395, 'MediaTek Helio A22', '', '', 0),
(4022, 19, 395, 'Android', '', '', 0),
(4023, 20, 395, '5.4500', '', '', 0),
(4024, 22, 395, '1440x720', '', '', 0),
(4025, 23, 395, '2.0000', '', '', 0),
(4026, 24, 395, 'Да', '', '', 0),
(4027, 25, 395, 'Да', '', '', 0),
(4028, 26, 395, 'nano-SIM', '', '', 0),
(4029, 28, 395, 'Да', '', '', 0),
(4030, 29, 395, '16 Гб', '', '', 0),
(4031, 30, 395, 'Да', '', '', 0),
(4032, 32, 395, 'GPS/ГЛОНАСС/BeiDou', '', '', 0),
(4033, 33, 395, 'IMG PowerVR', '', '', 0),
(4034, 35, 395, '13 Мп, автофокус, светодиодная вспышка', '', '', 0),
(4035, 36, 395, '2', '', '', 0),
(4036, 37, 395, 'micro-USB', '', '', 0),
(4037, 38, 395, 'Да', '', '', 0),
(4038, 39, 395, 'Слот для SIM-карт 2+1 (Две SIM-карты и карта памяти)', '', '', 0),
(4039, 40, 395, '71,5x147,5x8,3 мм, 145 гр', '', '', 0),
(4040, 17, 396, 'Qualcomm Snapdragon 845', '', '', 0),
(4041, 19, 396, 'Android 8.1', '', '', 0),
(4042, 20, 396, '6.2100', '', '', 0),
(4043, 21, 396, 'Amoled', '', '', 0),
(4044, 22, 396, '2248x1080', '', '', 0),
(4045, 23, 396, '6.0000', '', '', 0),
(4046, 24, 396, 'Да', '', '', 0),
(4047, 25, 396, 'Да', '', '', 0),
(4048, 26, 396, 'nano-SIM', '', '', 0),
(4049, 27, 396, 'LPDDR4x', '', '', 0),
(4050, 28, 396, 'Нет', '', '', 0),
(4051, 29, 396, '64 Гб', '', '', 0),
(4052, 30, 396, 'Да', '', '', 0),
(4053, 32, 396, 'GPS/ГЛОНАСС/Beidou/Галилео/QZSS', '', '', 0),
(4054, 33, 396, 'Adreno 630', '', '', 0),
(4055, 35, 396, '12 Мп, двойная камера, 4-осевая оптическая стабилизация, вспышка', '', '', 0),
(4056, 36, 396, '2', '', '', 0),
(4057, 37, 396, 'USB Type-C', '', '', 0),
(4058, 38, 396, 'Да', '', '', 0),
(4059, 39, 396, 'Цветовая гамма DCI-P3, поддержка режима активного дисплея, поддержка Регулировка цветовой температуры/Стандартный режим, поддержка Дневной режим/Режим чтения, Сканер отпечатков пальцев на задней панели, Поддержка Quick Charge 4+ зарядки', '', '', 0),
(4060, 40, 396, '74,8x154,9x7,6 мм, 175 гр', '', '', 0),
(4061, 41, 396, 'Алюминий, стекло', '', '', 0),
(4062, 17, 397, 'MediaTek Helio P22', '', '', 0),
(4063, 19, 397, 'Android', '', '', 0),
(4064, 20, 397, '5.4500', '', '', 0),
(4065, 22, 397, '1440x720', '', '', 0),
(4066, 23, 397, '4.0000', '', '', 0),
(4067, 24, 397, 'Да', '', '', 0),
(4068, 25, 397, 'Да', '', '', 0),
(4069, 26, 397, 'Nano-Sim', '', '', 0),
(4070, 28, 397, 'Да', '', '', 0),
(4071, 29, 397, '64 Гб', '', '', 0),
(4072, 30, 397, 'Да', '', '', 0),
(4073, 32, 397, 'GPS/ГЛОНАСС/BeiDou', '', '', 0),
(4074, 34, 397, 'Встроенный динамик', '', '', 0),
(4075, 35, 397, '12 Мп и 5 Мп, автофокус, вспышка', '', '', 0),
(4076, 36, 397, '2', '', '', 0),
(4077, 37, 397, 'USB, наушники', '', '', 0),
(4078, 38, 397, 'Да', '', '', 0),
(4079, 40, 397, '71,5x147,5x8,3 мм, 146 гр', '', '', 0),
(4080, 17, 398, 'Qualcomm Snapdragon 625', '', '', 0),
(4081, 19, 398, 'Android', '', '', 0),
(4082, 20, 398, '5.8400', '', '', 0),
(4083, 22, 398, '2280x1080', '', '', 0),
(4084, 23, 398, '3.0000', '', '', 0),
(4085, 24, 398, 'Да', '', '', 0),
(4086, 25, 398, 'Да', '', '', 0),
(4087, 26, 398, 'Nano-Sim', '', '', 0),
(4088, 28, 398, 'Да', '', '', 0),
(4089, 29, 398, '32 Гб', '', '', 0),
(4090, 30, 398, 'Да', '', '', 0),
(4091, 32, 398, 'GPS/ГЛОНАСС/BeiDou', '', '', 0),
(4092, 33, 398, 'Adreno 506', '', '', 0),
(4093, 34, 398, 'Встроенный динамик', '', '', 0),
(4094, 35, 398, '12 Мп и 5 Мп, автофокус, вспышка', '', '', 0),
(4095, 36, 398, '2', '', '', 0),
(4096, 37, 398, 'USB, наушники', '', '', 0),
(4097, 38, 398, 'Да', '', '', 0),
(4098, 40, 398, '71,68x149,33x8,75 мм, 178 гр', '', '', 0),
(4099, 41, 398, 'Пластик', '', '', 0),
(4100, 17, 399, 'Qualcomm Snapdragon 636', '', '', 0),
(4101, 19, 399, 'Android', '', '', 0),
(4102, 20, 399, '5.9900', '', '', 0),
(4103, 22, 399, '2160x1080', '', '', 0),
(4104, 23, 399, '3.0000', '', '', 0),
(4105, 24, 399, 'Да', '', '', 0),
(4106, 25, 399, 'Да', '', '', 0),
(4107, 26, 399, 'nano-SIM', '', '', 0),
(4108, 28, 399, 'Да', '', '', 0),
(4109, 29, 399, '32 Гб', '', '', 0),
(4110, 30, 399, 'Да', '', '', 0),
(4111, 32, 399, 'GPS/ГЛОНАСС/BeiDou', '', '', 0),
(4112, 33, 399, 'Adreno 509', '', '', 0),
(4113, 35, 399, '12 Мп и 5 Мп, 13 Мп фронтальная камера, автофокус, светодиодная вспышка', '', '', 0),
(4114, 36, 399, '2', '', '', 0),
(4115, 37, 399, 'micro-USB', '', '', 0),
(4116, 38, 399, 'Да', '', '', 0),
(4117, 40, 399, '75,45x158,5x8,05 мм, 181 гр', '', '', 0),
(4118, 41, 399, 'Алюминий, пластик', '', '', 0),
(4119, 17, 400, 'MediaTek Helio P22', '', '', 0),
(4120, 19, 400, 'Android', '', '', 0),
(4121, 20, 400, '5.4500', '', '', 0),
(4122, 22, 400, '1440x720', '', '', 0),
(4123, 23, 400, '3.0000', '', '', 0),
(4124, 24, 400, 'Да', '', '', 0),
(4125, 25, 400, 'Да', '', '', 0),
(4126, 26, 400, 'Nano-Sim', '', '', 0),
(4127, 28, 400, 'Да', '', '', 0),
(4128, 29, 400, '32 Гб', '', '', 0),
(4129, 30, 400, 'Да', '', '', 0),
(4130, 32, 400, 'GPS/ГЛОНАСС/BeiDou', '', '', 0),
(4131, 34, 400, 'Встроенный динамик', '', '', 0),
(4132, 35, 400, '12 Мп и 5 Мп, автофокус, вспышка', '', '', 0),
(4133, 36, 400, '2', '', '', 0),
(4134, 37, 400, 'USB, наушники', '', '', 0),
(4135, 38, 400, 'Да', '', '', 0),
(4136, 40, 400, '71,5x147,5x8,3 мм, 146 гр', '', '', 0),
(4137, 17, 401, 'Qualcomm Snapdragon 845', '', 'value', 0),
(4138, 19, 401, 'Android 8.1', '', 'value', 0),
(4139, 20, 401, '6.2100', '', 'value', 0),
(4140, 21, 401, 'Amoled', '', 'value', 0),
(4141, 22, 401, '2248x1080', '', 'value', 0),
(4142, 23, 401, '6.0000', '', 'value', 0),
(4143, 24, 401, 'Да', '', 'value', 0),
(4144, 25, 401, 'Да', '', 'value', 0),
(4145, 26, 401, 'nano-SIM', '', 'value', 0),
(4146, 27, 401, 'LPDDR4x', '', 'value', 0),
(4147, 28, 401, 'Нет', '', 'value', 0),
(4148, 29, 401, '128 Гб', '', 'value', 0),
(4149, 30, 401, 'Да', '', 'value', 0),
(4150, 32, 401, 'GPS/ГЛОНАСС/Beidou/Галилео/QZSS', '', 'value', 0),
(4151, 33, 401, 'Adreno 630', '', 'value', 0),
(4152, 35, 401, '12 Мп, двойная камера, 4-осевая оптическая стабилизация, вспышка', '', 'value', 0),
(4153, 36, 401, '2', '', 'value', 0),
(4154, 37, 401, 'USB Type-C', '', 'value', 0),
(4155, 38, 401, 'Да', '', 'value', 0),
(4156, 39, 401, 'Цветовая гамма DCI-P3, поддержка режима активного дисплея, поддержка Регулировка цветовой температуры/Стандартный режим, поддержка Дневной режим/Режим чтения, Сканер отпечатков пальцев на задней панели, Поддержка Quick Charge 4+ зарядки', '', 'value', 0),
(4157, 40, 401, '74,8x154,9x7,6 мм, 175 гр', '', 'value', 0),
(4158, 41, 401, 'Алюминий, стекло', '', 'value', 0),
(4159, 17, 321, 'A12 Bionic', '', '', 0),
(4160, 18, 321, 'н/д', '', '', 0),
(4161, 19, 321, 'iOS 12', '', '', 0),
(4162, 20, 321, '6.5000', '', '', 0),
(4163, 21, 321, 'OLED', '', '', 0),
(4164, 22, 321, '2688x1242', '', '', 0),
(4165, 23, 321, '4.0000', '', '', 0),
(4166, 24, 321, 'Да', '', '', 0),
(4167, 25, 321, 'Да', '', '', 0),
(4168, 26, 321, 'nano-SIM', '', '', 0),
(4169, 27, 321, 'н/д', '', '', 0),
(4170, 28, 321, 'Нет', '', '', 0),
(4171, 29, 321, '256 Гб', '', '', 0),
(4172, 30, 321, '4G LTE', '', '', 0),
(4173, 31, 321, 'до 65 ч', '', '', 0),
(4174, 32, 321, 'GPS, ГЛОНАСС, Galileo, QZSS', '', '', 0),
(4175, 33, 321, 'A12 Bionic', '', '', 0),
(4176, 34, 321, 'Встроенные динамики и микрофон ', '', '', 0),
(4177, 35, 321, 'двойная 12 Мп, запись видео в 4K, фронтальная камера 7 Мп', '', '', 0),
(4178, 36, 321, '1', '', '', 0),
(4179, 37, 321, 'Lightning', '', '', 0),
(4180, 38, 321, 'Да', '', '', 0),
(4181, 39, 321, 'Аутентификация - Face ID, датчики: трёхосевой гироскоп, акселерометр, барометр, датчик внешней освещённости, уровень защиты от влаги IP68', '', '', 0),
(4182, 40, 321, '157,5x77,4x7,7 мм, 208 г', '', '', 0),
(4183, 41, 321, 'Алюминий', '', '', 0),
(4184, 17, 322, 'A12 Bionic', '', '', 0),
(4185, 18, 322, 'н/д', '', '', 0),
(4186, 19, 322, 'iOS 12', '', '', 0),
(4187, 20, 322, '6.1000', '', '', 0),
(4188, 21, 322, 'IPS', '', '', 0),
(4189, 22, 322, '1792x828', '', '', 0),
(4190, 23, 322, '3.0000', '', '', 0),
(4415, 17, 13, 'Intel core i10', '', 'value', 0),
(4416, 18, 13, 'Индекс процессора', '', 'value', 0),
(4417, 19, 13, 'Операционная система', '', 'value', 0),
(4418, 20, 13, 'Диагональ экрана, дюйм', '', 'value', 0),
(4419, 17, 402, 'Intel', '', 'value', 0),
(4420, 18, 402, 'celeron', '', 'value', 0),
(4421, 19, 402, 'windows', '', 'value', 0),
(4422, 20, 402, '17\'', '', 'value', 0),
(4423, 21, 402, 'amoled', '', 'value', 0),
(4424, 22, 402, '1920x1080', '', 'value', 0),
(4425, 23, 402, '3', '', 'value', 0),
(4426, 24, 402, 'да', '', 'value', 0),
(4427, 25, 402, 'да', '', 'value', 0),
(4428, 26, 402, 'micro-sim', '', 'value', 0),
(4443, 1, 43, '', '', 'enum', 7),
(4444, 1, 43, '', '', 'enum', 8),
(4445, 1, 43, '', '', 'enum', 9),
(4446, 2, 43, '', '', 'enum', 6),
(4447, 4, 43, '', '', 'enum', 10),
(4448, 4, 43, '', '', 'enum', 11),
(4449, 5, 43, '', '', 'enum', 12),
(4450, 5, 43, '', '', 'enum', 14),
(4451, 8, 43, '', '', 'enum', 15),
(4452, 9, 43, '', '', 'enum', 17),
(4453, 10, 43, '', '', 'enum', 21),
(4454, 10, 43, '', '', 'enum', 22),
(4455, 10, 43, '', '', 'enum', 23),
(4456, 10, 43, '', '', 'enum', 24);

-- --------------------------------------------------------

--
-- Структура таблицы `question`
--

CREATE TABLE `question` (
  `id` int(255) NOT NULL,
  `name` text NOT NULL,
  `parent_id` int(255) DEFAULT NULL,
  `text` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `question`
--

INSERT INTO `question` (`id`, `name`, `parent_id`, `text`) VALUES
(7, 'Оплата товаров', NULL, ''),
(8, 'Доставка по России', 7, ''),
(9, 'Доставка по Москве', 7, 'пулькой доставим на техно дронах в ночное время!'),
(10, 'Какая стоимость доставки в регионы?', 8, 'вап тлдывалждпоывждоа ываопизы оп ыьваджлполд отажджлыжва тлдывадлтлдытва лдвытарпв сдмтивилптдлиывтадплт длжрттываптл ывджлаывдалтпыджлвта плтывдлатдылвапт длываапыкжвптожщыв'),
(11, 'Какая стоимость доставки в регионы?', 9, 'вап тлдывалждпоывждоа ываопизы оп ыьваджлполд отажджлыжва тлдывадлтлдытва лдвытарпв сдмтивилптдлиывтадплт длжрттываптл ывджлаывдалтпыджлвта плтывдлатдылвапт длываапыкжвптожщыв');

-- --------------------------------------------------------

--
-- Структура таблицы `question_category`
--

CREATE TABLE `question_category` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `question_category`
--

INSERT INTO `question_category` (`id`, `name`) VALUES
(1, 'Category 1'),
(2, 'Category 2'),
(3, 'Category 3'),
(4, 'Category 4');

-- --------------------------------------------------------

--
-- Структура таблицы `question_type`
--

CREATE TABLE `question_type` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `question_type`
--

INSERT INTO `question_type` (`id`, `name`) VALUES
(1, 'Type 1'),
(2, 'Type 2'),
(3, 'Type 3'),
(4, 'Type 4');

-- --------------------------------------------------------

--
-- Структура таблицы `rate`
--

CREATE TABLE `rate` (
  `id` int(255) NOT NULL,
  `active` int(1) NOT NULL DEFAULT '1',
  `name` varchar(1024) NOT NULL,
  `description` text NOT NULL,
  `price` decimal(65,2) NOT NULL,
  `count_categories` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `rate`
--

INSERT INTO `rate` (`id`, `active`, `name`, `description`, `price`, `count_categories`) VALUES
(1, 1, 'Дешевый', 'Дешевый тариф', '100.00', 1000),
(2, 1, 'Стандартный', 'Стандартный тариф', '1000.00', 2),
(3, 1, 'Премиум', 'Премиум тариф', '2000.00', 3);

-- --------------------------------------------------------

--
-- Структура таблицы `region`
--

CREATE TABLE `region` (
  `id` int(255) NOT NULL,
  `name` varchar(1024) NOT NULL,
  `active` int(255) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `region`
--

INSERT INTO `region` (`id`, `name`, `active`) VALUES
(1, 'test', 1),
(2, 'test2', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `report_Transaction`
--

CREATE TABLE `report_Transaction` (
  `id` int(255) NOT NULL,
  `created_at` int(60) NOT NULL,
  `updated_at` int(60) NOT NULL,
  `type` int(255) NOT NULL,
  `user_from` int(255) NOT NULL,
  `user_to` int(255) NOT NULL,
  `sum` decimal(65,2) NOT NULL,
  `text` text NOT NULL,
  `complete` int(1) NOT NULL DEFAULT '0',
  `fail` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `report_Transaction`
--

INSERT INTO `report_Transaction` (`id`, `created_at`, `updated_at`, `type`, `user_from`, `user_to`, `sum`, `text`, `complete`, `fail`) VALUES
(3, 1540561893, 1542887646, 1, 23, 34, '10.00', 'Перевод средств', 1, 0),
(4, 1537390800, 1542890480, 1, 23, 49, '123.00', 'Перевод средств', 1, 0),
(6, 1540561893, 1543053168, 1, 23, 34, '10.00', 'Перевод средств', 1, 0),
(7, 1540561893, 1540561893, 1, 23, 34, '10.00', 'Перевод средств', 0, 0),
(8, 1540561893, 1540561893, 1, 23, 34, '10.00', 'Перевод средств', 0, 0),
(9, 1540562184, 1540562184, 1, 23, 34, '10.00', 'Перевод средств', 0, 0),
(10, 1540799589, 1543320352, 1, 34, 23, '20.00', 'Перевод средств', 1, 0),
(11, 1540799876, 1543320189, 1, 34, 23, '30.00', 'Перевод средств', 0, 1),
(13, 1540886472, 1543320356, 1, 23, 33, '111.00', 'Перевод средств', 0, 1),
(14, 1540901129, 1543320132, 1, 23, 33, '222.00', 'Перевод средств', 0, 1),
(15, 1540901443, 1543320360, 1, 23, 33, '100.00', 'Перевод средств', 0, 1),
(16, 1541140253, 1543320169, 1, 33, 23, '111.00', 'Перевод средств', 0, 1),
(17, 1541416065, 1541416065, 1, 23, 34, '111.00', 'Перевод средств', 0, 0),
(18, 1541760380, 1541760380, 1, 23, 33, '1000.00', 'Перевод средств', 0, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `request_output`
--

CREATE TABLE `request_output` (
  `id` int(255) NOT NULL,
  `created_at` int(60) NOT NULL,
  `updated_at` int(60) NOT NULL,
  `sum` decimal(65,2) NOT NULL,
  `user_id` int(255) NOT NULL,
  `status_id` int(11) NOT NULL,
  `complete` int(1) NOT NULL DEFAULT '0',
  `card` varchar(255) NOT NULL,
  `juristic_type_id` int(11) NOT NULL,
  `bank_name` varchar(255) NOT NULL,
  `success_date` varchar(255) NOT NULL,
  `fail` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `request_output`
--

INSERT INTO `request_output` (`id`, `created_at`, `updated_at`, `sum`, `user_id`, `status_id`, `complete`, `card`, `juristic_type_id`, `bank_name`, `success_date`, `fail`) VALUES
(8, 1537184038, 1543316481, '11.00', 23, 1, 1, '', 0, '', '1543316481', 0),
(9, 1537184396, 1543316483, '33.00', 23, 1, 1, '', 0, '', '1543316483', 0),
(10, 1540902713, 1543316485, '222.00', 23, 1, 1, '', 0, '', '1543316485', 0),
(11, 1540967567, 1543316488, '222.00', 23, 1, 1, '', 0, '', '1543316488', 0),
(12, 1540967652, 1543316490, '222.00', 23, 1, 1, '', 0, '', '1543316490', 0),
(13, 1540967700, 1543316492, '123.00', 23, 1, 1, '', 0, '', '1543316492', 0),
(14, 1540967737, 1543577974, '111.00', 23, 1, 1, '', 0, '', '1543577974', 0),
(15, 1540967762, 1543320369, '111.00', 23, 1, 1, '', 0, '', '1543320369', 0),
(16, 1540967853, 1543316471, '123.00', 23, 1, 1, '', 0, '', '1543316471', 0),
(17, 1543235975, 1543235975, '111.00', 33, 1, 0, '', 0, '', '0', 0),
(18, 1543236228, 1543316466, '222.00', 33, 1, 1, '', 0, '', '1543316466', 0),
(19, 1543236629, 1543322950, '111.00', 33, 1, 1, 'undefined', 0, '', '1543322950', 0),
(20, 1543237639, 1543316290, '122.00', 33, 1, 1, '10', 0, '', '', 0),
(21, 1543237655, 1543316231, '122.00', 33, 1, 1, 'undefined', 0, '', '0', 0),
(22, 1543237873, 1543316477, '123.00', 33, 1, 1, '234234', 0, '', '1543316477', 0),
(23, 1543237892, 1543323864, '123.00', 33, 1, 1, '11', 0, '', '1543323864', 0),
(24, 1543238037, 1543323913, '123.00', 33, 1, 0, 'undefined', 0, '', '0', 1),
(25, 1543239024, 1543239024, '123.00', 33, 1, 0, '2222222222222222', 0, '', '0', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `restriction`
--

CREATE TABLE `restriction` (
  `id` int(255) NOT NULL,
  `type` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `restriction`
--

INSERT INTO `restriction` (`id`, `type`) VALUES
(1, 'Не ограничен'),
(2, 'Ограничен'),
(3, 'Забанен');

-- --------------------------------------------------------

--
-- Структура таблицы `review`
--

CREATE TABLE `review` (
  `id` int(255) NOT NULL,
  `created_at` int(60) NOT NULL,
  `updated_at` int(60) NOT NULL,
  `updater_id` int(255) NOT NULL,
  `product_id` int(255) NOT NULL,
  `buyer_id` int(255) NOT NULL,
  `order_id` int(255) NOT NULL,
  `rating` int(255) NOT NULL,
  `comment` text NOT NULL,
  `accomplishments` text NOT NULL,
  `limitations` text NOT NULL,
  `recommendations` text NOT NULL,
  `like` int(255) NOT NULL,
  `dislike` int(255) NOT NULL,
  `active` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `review`
--

INSERT INTO `review` (`id`, `created_at`, `updated_at`, `updater_id`, `product_id`, `buyer_id`, `order_id`, `rating`, `comment`, `accomplishments`, `limitations`, `recommendations`, `like`, `dislike`, `active`) VALUES
(1, 1543073167, 1543073167, 33, 29, 33, 241, 3, 'test', 'test', 'test', 'test', 1, 2, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `review_like`
--

CREATE TABLE `review_like` (
  `id` int(255) NOT NULL,
  `review_id` int(255) NOT NULL,
  `user_id` int(255) NOT NULL,
  `type` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `seller_type`
--

CREATE TABLE `seller_type` (
  `id` int(255) NOT NULL,
  `seller_type` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `seller_type`
--

INSERT INTO `seller_type` (`id`, `seller_type`) VALUES
(1, 'Физ. лицо'),
(2, 'Юр. лицо');

-- --------------------------------------------------------

--
-- Структура таблицы `shop`
--

CREATE TABLE `shop` (
  `id` int(255) NOT NULL,
  `created_at` int(60) NOT NULL,
  `updated_at` int(60) NOT NULL,
  `seller_id` int(255) NOT NULL DEFAULT '0',
  `name` varchar(1024) NOT NULL,
  `url` varchar(1024) NOT NULL,
  `status` int(255) NOT NULL DEFAULT '5',
  `rate` int(255) NOT NULL,
  `paid` int(1) NOT NULL,
  `date_active_start` int(255) NOT NULL,
  `date_active_end` int(255) NOT NULL,
  `config` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `shop`
--

INSERT INTO `shop` (`id`, `created_at`, `updated_at`, `seller_id`, `name`, `url`, `status`, `rate`, `paid`, `date_active_start`, `date_active_end`, `config`) VALUES
(23, 1534453200, 1543567627, 23, 'test1', 'http://test.ru', 3, 3, 1, 1541278800, 1572814800, 'a:3:{s:6:\"header\";i:1;s:6:\"blocks\";a:1:{i:0;a:3:{s:4:\"type\";s:6:\"slider\";s:9:\"variation\";i:1;s:7:\"options\";a:0:{}}}s:6:\"footer\";i:1;}'),
(24, 1534453200, 1541181760, 23, 'test2133311', 'http://test2.ru', 3, 3, 1, 1541278800, 1572814800, 'a:3:{s:6:\"header\";i:1;s:6:\"blocks\";a:1:{i:0;a:3:{s:4:\"type\";s:6:\"slider\";s:9:\"variation\";i:1;s:7:\"options\";a:0:{}}}s:6:\"footer\";i:1;}'),
(25, 1534747221, 1535628087, 33, 'test3', 'https://test3.ru', 1, 3, 1, 1541278800, 0, 'a:3:{s:6:\"header\";i:1;s:6:\"blocks\";a:1:{i:0;a:3:{s:4:\"type\";s:6:\"slider\";s:9:\"variation\";i:1;s:7:\"options\";a:0:{}}}s:6:\"footer\";i:1;}'),
(27, 1537276459, 1540316376, 23, 'test', '', 5, 2, 0, 1541278800, 0, 'a:3:{s:6:\"header\";i:1;s:6:\"blocks\";a:1:{i:0;a:3:{s:4:\"type\";s:6:\"slider\";s:9:\"variation\";i:1;s:7:\"options\";a:0:{}}}s:6:\"footer\";i:1;}'),
(28, 1537360626, 1540316376, 23, 'test shop', '', 5, 0, 0, 0, 0, 'a:3:{s:6:\"header\";i:1;s:6:\"blocks\";a:1:{i:0;a:3:{s:4:\"type\";s:6:\"slider\";s:9:\"variation\";i:1;s:7:\"options\";a:0:{}}}s:6:\"footer\";i:1;}'),
(29, 1537390800, 1537520622, 23, 'qwerty', '', 5, 2, 0, 0, 0, 'a:3:{s:6:\"header\";i:1;s:6:\"blocks\";a:1:{i:0;a:3:{s:4:\"type\";s:6:\"slider\";s:9:\"variation\";i:1;s:7:\"options\";a:0:{}}}s:6:\"footer\";i:1;}'),
(30, 1543491671, 1543497985, 97, 'fsdasadfasdf', 'http://asdasasd.as', 1, 2, 1, 1543491671, 1543491671, 'a:3:{s:6:\"header\";i:1;s:6:\"blocks\";a:1:{i:0;a:3:{s:4:\"type\";s:6:\"slider\";s:9:\"variation\";i:1;s:7:\"options\";a:0:{}}}s:6:\"footer\";i:1;}');

-- --------------------------------------------------------

--
-- Структура таблицы `shopcase`
--

CREATE TABLE `shopcase` (
  `id` int(255) NOT NULL,
  `product_id` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `shopcase`
--

INSERT INTO `shopcase` (`id`, `product_id`) VALUES
(1, '[\"13\",\"21\",\"30\",\"36\",\"19\",\"37\",\"38\",\"39\",\"41\",\"43\",\"91\",\"321\",\"354\"]');

-- --------------------------------------------------------

--
-- Структура таблицы `shop_categories`
--

CREATE TABLE `shop_categories` (
  `id` int(11) NOT NULL,
  `shop_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `shop_categories`
--

INSERT INTO `shop_categories` (`id`, `shop_id`, `category_id`) VALUES
(72, 25, 1),
(73, 25, 3),
(74, 25, 4),
(83, 27, 1),
(84, 27, 2),
(85, 27, 3),
(96, 29, 3),
(97, 29, 6),
(98, 29, 8),
(111, 24, 1),
(112, 24, 2),
(113, 24, 4),
(117, 30, 1),
(118, 30, 2),
(119, 23, 1),
(120, 23, 2),
(121, 23, 3);

-- --------------------------------------------------------

--
-- Структура таблицы `shop_status`
--

CREATE TABLE `shop_status` (
  `id` int(255) NOT NULL,
  `name` varchar(1024) NOT NULL,
  `active` int(1) NOT NULL DEFAULT '1',
  `code` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `shop_status`
--

INSERT INTO `shop_status` (`id`, `name`, `active`, `code`) VALUES
(1, 'На модерации', 1, 'mod'),
(2, 'Требуются правки', 1, 'edits'),
(3, 'Активен', 1, 'active'),
(4, 'Неактивен', 1, 'disable'),
(5, 'Удален', 1, 'delete');

-- --------------------------------------------------------

--
-- Структура таблицы `slider`
--

CREATE TABLE `slider` (
  `id` int(255) NOT NULL,
  `active` int(255) NOT NULL DEFAULT '1',
  `sort` int(255) NOT NULL,
  `image` text NOT NULL,
  `link` varchar(1024) NOT NULL,
  `text_cursive` varchar(1024) NOT NULL,
  `text` varchar(1024) NOT NULL,
  `text_bold` varchar(1024) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `slider`
--

INSERT INTO `slider` (`id`, `active`, `sort`, `image`, `link`, `text_cursive`, `text`, `text_bold`) VALUES
(1, 1, 2, '/images/0ea61ec4d197008ac3ce63249f6c29ab.jpg', 'test', 'время отдыхать', 'Шезглонги', 'VELLER'),
(2, 1, 1, '/images/gipu/offer_1.png', 'test2', 'покоритель горных козлов', 'Горные велоспеды', 'CRONUS'),
(3, 1, 0, '/images/637623931a22c2eb510a4f9dada004f3.jpg', 'vremya-buhat', 'Время бухать', 'а вас не достало все?', 'Надо что-то менять'),
(4, 1, 0, '/images/e42076ba8f557a17646921b0fdedbcff.png', 'rasprodazha', 'А у нас Новогодние СКИДКИ', 'Заходите и покупайте товары к Новому Году', 'Скидки 2019');

-- --------------------------------------------------------

--
-- Структура таблицы `store_order`
--

CREATE TABLE `store_order` (
  `id` int(255) NOT NULL,
  `parent_id` int(255) DEFAULT NULL,
  `created_at` bigint(255) NOT NULL,
  `updated_at` bigint(255) NOT NULL,
  `buyer_id` int(11) NOT NULL,
  `seller_id` int(255) NOT NULL,
  `supplier_id` int(255) NOT NULL,
  `pay_type_id` int(255) NOT NULL,
  `status_id` int(5) NOT NULL,
  `pay_date` bigint(255) NOT NULL,
  `status_updated_at` bigint(20) NOT NULL,
  `delivery_status_id` int(5) NOT NULL,
  `courier_id` int(255) NOT NULL,
  `count_products` int(255) NOT NULL,
  `total` decimal(65,2) NOT NULL,
  `delivery_price` int(255) NOT NULL,
  `discount` int(255) NOT NULL,
  `shop_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `comment` varchar(255) NOT NULL,
  `internal_account` int(1) NOT NULL DEFAULT '0',
  `confirm_internal` int(1) NOT NULL DEFAULT '0',
  `delivery_date` int(255) NOT NULL,
  `delivery_type_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `store_order`
--

INSERT INTO `store_order` (`id`, `parent_id`, `created_at`, `updated_at`, `buyer_id`, `seller_id`, `supplier_id`, `pay_type_id`, `status_id`, `pay_date`, `status_updated_at`, `delivery_status_id`, `courier_id`, `count_products`, `total`, `delivery_price`, `discount`, `shop_id`, `name`, `phone`, `email`, `address`, `comment`, `internal_account`, `confirm_internal`, `delivery_date`, `delivery_type_id`) VALUES
(240, NULL, 1542834000, 1542834000, 96, 47, 34, 2, 1, 1542920400, 1542834000, 1, 99, 2, '24000.00', 300, 0, 0, 'test', '+375 (33) 333-33-33', 'test@test.tes', 'test', 'Комментарий', 1, 1, 1543352400, 1),
(241, NULL, 1542920400, 1542920400, 21, 47, 0, 1, 1, 1542920400, 1542920400, 1, 23, 1, '22000.00', 300, 0, 0, 'Лично', '+7 (999) 531-95-39', 'renata8229@mail.ru', 'rgergergergerg', 'ergergcwwww44', 0, 0, 1541624400, 1),
(242, NULL, 1543576044, 1543576044, 0, 0, 0, 1, 2, 0, 1543576044, 1, 110, 1, '157170.00', 300, 0, 0, 'Vasili Matishev', '+79117879322', 'vmatishev@gmail.com', 'Краснзвездная, 13', 'gjgdrrfkfgjfgj', 0, 0, 1543611600, 0),
(243, NULL, 1543576798, 1543576798, 0, 47, 0, 1, 2, 0, 1543576798, 1, 110, 1, '20000.00', 300, 0, 0, 'Vasili Matishev', '+79117879322', 'vmatishev@gmail.com', 'Краснзвездная, 13', '', 0, 0, 1542315600, 0),
(244, NULL, 1543577756, 1543577756, 96, 0, 0, 2, 2, 0, 1543577756, 1, 110, 1, '10.00', 300, 0, 0, 'Злой', '79219546182', 'zloy@mail.ru', 'куда угодно ', '', 0, 0, 1543611600, 0),
(245, NULL, 1543844980, 1543844980, 0, 47, 0, 1, 2, 0, 1543844980, 1, 110, 1, '20000.00', 300, 0, 0, 'Vasili Matishev', '+79117879322', 'vmatishev@gmail.com', 'Краснзвездная, 13', '', 0, 0, 1543957200, 0),
(246, NULL, 1543931591, 1543931591, 0, 0, 0, 1, 1, 0, 1543931591, 1, 0, 3, '188160.00', 300, 0, 0, 'Vasili Matishev', '+79117879322', 'vmatishev@gmail.com', 'Краснзвездная, 13', '', 0, 0, 0, 0),
(247, 246, 1543931591, 1543931591, 0, 0, 0, 1, 1, 0, 1543931591, 1, 0, 1, '157170.00', 0, 0, 0, 'Vasili Matishev', '+79117879322', 'vmatishev@gmail.com', 'Краснзвездная, 13', '', 0, 0, 1543957200, 0),
(248, 246, 1543931591, 1543931591, 0, 0, 47, 1, 1, 0, 1543931591, 1, 0, 2, '30990.00', 0, 0, 0, 'Vasili Matishev', '+79117879322', 'vmatishev@gmail.com', 'Краснзвездная, 13', '', 0, 0, 1543957200, 0),
(249, NULL, 1543932782, 1543932782, 96, 0, 0, 1, 1, 0, 1543932782, 1, 0, 2, '24500.00', 300, 0, 0, 'Vasili Matishev', '+79117879322', 'vmatishev@gmail.com', 'Краснзвездная, 13', '', 0, 0, 0, 0),
(250, 249, 1543932782, 1543932782, 96, 0, 111, 1, 1, 0, 1543932782, 1, 0, 1, '1500.00', 0, 0, 0, 'Vasili Matishev', '+79117879322', 'vmatishev@gmail.com', 'Краснзвездная, 13', '', 0, 0, 0, 0),
(251, 249, 1543932782, 1543932782, 96, 0, 50, 1, 1, 0, 1543932782, 1, 0, 1, '23000.00', 0, 0, 0, 'Vasili Matishev', '+79117879322', 'vmatishev@gmail.com', 'Краснзвездная, 13', '', 0, 0, 0, 0),
(252, NULL, 1543932879, 1543932879, 96, 0, 0, 1, 1, 0, 1543932879, 1, 0, 2, '24600.00', 300, 0, 0, 'Vasili Matishev', '+79117879322', 'vmatishev@gmail.com', 'Краснзвездная, 13', '', 0, 0, 0, 0),
(253, 252, 1543932879, 1543932879, 96, 0, 50, 1, 1, 0, 1543932879, 1, 0, 1, '23000.00', 0, 0, 0, 'Vasili Matishev', '+79117879322', 'vmatishev@gmail.com', 'Краснзвездная, 13', '', 0, 0, 0, 0),
(254, 252, 1543932879, 1543932879, 96, 0, 22, 1, 1, 0, 1543932879, 1, 0, 1, '1600.00', 0, 0, 0, 'Vasili Matishev', '+79117879322', 'vmatishev@gmail.com', 'Краснзвездная, 13', '', 0, 0, 0, 0),
(255, NULL, 1543932961, 1543932961, 96, 50, 0, 1, 1, 0, 1543932961, 1, 0, 2, '24600.00', 300, 0, 0, 'Vasili Matishev', '+79117879322', 'vmatishev@gmail.com', 'Краснзвездная, 13', '', 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `support_question`
--

CREATE TABLE `support_question` (
  `id` int(255) NOT NULL,
  `email` varchar(1024) NOT NULL,
  `question_category` int(11) NOT NULL,
  `question_type` int(11) NOT NULL,
  `question` text NOT NULL,
  `status` int(2) NOT NULL DEFAULT '0',
  `answer` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `support_question`
--

INSERT INTO `support_question` (`id`, `email`, `question_category`, `question_type`, `question`, `status`, `answer`) VALUES
(36, 'sadfasd@asd.as', 2, 1, 'варвпровпововповрыкпрыкрыкрфыкрыцкрыцерыцерыцр', 1, 'testadsdsaadsdas'),
(37, 'test@test.tes', 4, 4, 'test', 0, '');

-- --------------------------------------------------------

--
-- Структура таблицы `template`
--

CREATE TABLE `template` (
  `id` int(255) NOT NULL,
  `name` varchar(1024) NOT NULL,
  `image` text NOT NULL,
  `code` text NOT NULL,
  `block_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `template`
--

INSERT INTO `template` (`id`, `name`, `image`, `code`, `block_id`) VALUES
(1, 'test', '/images/ad63a14a8cf92a555af2c910a37de5c9.jpg', '&lt;?php echo &quot;Hello world&quot;; ?&gt;', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `user`
--

CREATE TABLE `user` (
  `id` int(255) NOT NULL,
  `registration_date` int(65) NOT NULL,
  `type` int(1) NOT NULL,
  `region` int(120) NOT NULL,
  `city` int(120) NOT NULL,
  `email` varchar(120) NOT NULL,
  `phone` varchar(25) NOT NULL,
  `name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `restrict_id` int(1) NOT NULL,
  `last_activity` int(65) NOT NULL,
  `notify_by_email` int(1) NOT NULL,
  `notify_by_phone` int(1) NOT NULL,
  `passport_data` text NOT NULL,
  `balance` decimal(65,2) NOT NULL,
  `referral_id` int(255) DEFAULT '0',
  `referral_level` int(255) NOT NULL,
  `status_id` int(255) NOT NULL,
  `seller_type` int(2) NOT NULL,
  `juristic_documents` text NOT NULL,
  `juristic_type_id` int(2) NOT NULL,
  `juristic_data_id` int(255) NOT NULL,
  `secret_question` text NOT NULL,
  `secret_answer` varchar(255) NOT NULL,
  `user_group_id` int(11) NOT NULL,
  `avatar` text NOT NULL,
  `active` int(1) NOT NULL DEFAULT '1',
  `position` varchar(255) NOT NULL,
  `access` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `user`
--

INSERT INTO `user` (`id`, `registration_date`, `type`, `region`, `city`, `email`, `phone`, `name`, `last_name`, `password`, `restrict_id`, `last_activity`, `notify_by_email`, `notify_by_phone`, `passport_data`, `balance`, `referral_id`, `referral_level`, `status_id`, `seller_type`, `juristic_documents`, `juristic_type_id`, `juristic_data_id`, `secret_question`, `secret_answer`, `user_group_id`, `avatar`, `active`, `position`, `access`) VALUES
(1, 0, 4, 0, 0, 'support@gipu.ru', '', 'Тех поддержка', '', '', 0, 0, 0, 0, '', '0.00', 0, 0, 0, 0, '', 0, 0, '', '', 0, '', 1, '', ''),
(21, 1532034000, 3, 1, 1, 'dessites@bk.ru', '12341234321212123', 'test', 'testovich', '', 1, 1532034000, 0, 0, '', '0.00', 0, 3, 1, 0, '', 1, 4, '', '', 4, '', 1, '', 'a:1:{i:0;s:1:\"4\";}'),
(22, 1531520986, 5, 1, 1, 'test1@example.org', '2222222321112123123', 'test2345', 'test234', '', 1, 1543235934, 0, 0, '', '0.00', 0, 0, 5, 0, '', 0, 35, '', '', 0, '', 1, '', 'a:2:{i:0;s:2:\"15\";i:1;s:2:\"16\";}'),
(23, 1531515600, 4, 1, 1, 'test@example.org', '+ 7 777 777-77-77', 'example test', 'test12', '41a5fd1f309869282433974acf72788d', 1, 1543950219, 0, 0, '', '10250.00', 33, 3, 5, 2, '', 1, 36, '', '', 0, '/images/6fd3ea3ec0684abb00cc852f6af841ff.jpg', 1, '', ''),
(33, 1531947600, 4, 1, 1, 'asd@example.org', '+ 7 777 777-77-73', 'Имя', 'test444', '41a5fd1f309869282433974acf72788d', 1, 1543917866, 0, 0, '', '1150.00', 34, 4, 5, 0, '', 1, 37, '', '', 0, '/images/7318944339364a99e822af1f661f75d0.jpg', 1, '', ''),
(34, 1541080633, 4, 2, 2, 'qweqwe@example.org', '43211234', 'seller', 'testqwe', '41a5fd1f309869282433974acf72788d', 1, 1541416065, 0, 0, '', '200.00', 45, 5, 1, 0, '', 0, 4, '', '', 0, '', 1, '', ''),
(45, 1533623543, 4, 1, 0, 'aaaaaa@aaa.aa', '22222222222222', 'aaaaaaaa', 'aaaaaa', '41a5fd1f309869282433974acf72788d', 1, 1533623543, 0, 0, '', '0.00', 46, 5, 1, 0, '', 0, 4, '', '', 0, '', 1, '', ''),
(46, 1533629019, 4, 1, 0, 'asdas@adas.ru', 'вфывфы', 'вфывфывфы', 'фвыфывфыв', 'f3425f5741f1417e6eb61a9ba0d48dcf', 1, 1533629019, 0, 0, '', '0.00', 0, 0, 1, 0, '', 0, 4, '', '', 0, '', 1, '', ''),
(47, 1533677294, 4, 2, 0, 'c-cappuccino@mail.ru', '+79999999999', 'Name', 'Second', '41a5fd1f309869282433974acf72788d', 1, 1533677294, 0, 0, '', '0.00', 0, 0, 5, 0, '', 0, 11, '', '', 0, '', 1, '', ''),
(48, 1533762000, 4, 1, 1, 'aaaaaaaaaaaaa@aaa.aa', '1111111111', 'aaaaaaaaaaaa', 'aaaaaaaaaaaa', '4a2511b001c66250c6dc8dc9f50ed5c5', 1, 1533762000, 0, 0, 'aaaaaaaaaaaa', '0.00', 0, 0, 2, 0, 'aaaaaaaaaaaa', 1, 7, 'aaaaaaaaaaaa', 'aaaaaaaaaaaa', 0, '', 1, '', ''),
(49, 1533814685, 4, 1, 1, 'adassaddsa@asdasd.as', '11111', 'dasdssda', 'dssdasadsad', '81004725b35229a80e1a998e9ea59442', 1, 1533814685, 0, 0, 'aaaaa', '123.00', 0, 0, 5, 11, 'sadsaasasd', 1, 7, 'sadsaddas', 'dsasadsaddsa', 2, '', 1, '', ''),
(50, 1536526800, 5, 2, 0, 'vmatishev@gmail.com', '+79643711313', 'Vasili', 'Matishev', 'edae0d807957150a61f351421d12e0ec', 1, 1542982291, 0, 0, 'aaaaa', '0.00', 0, 0, 5, 0, '', 0, 12, '', '', 2, '', 1, '', ''),
(87, 1537536026, 3, 0, 0, 'qwe@qwe.qwe', '123123123123', 'qweqweqweqweqwe', 'qweqweqweqweqwe', '93caf8fe618b9cb3b04111b43ca13b8e', 1, 1537536026, 0, 0, '', '0.00', 0, 0, 0, 0, '', 0, 30, '', '', 0, 'a:0:{}', 1, '', ''),
(94, 1537822800, 4, 0, 0, 'sssssssssssssssss@saasd.as', 'sssssssssssssssss', 'sssssssssssssssss', 'sssssssssssssssss', 'e8312b338c66580c7efde0a3a668fa8b', 1, 1537822800, 0, 0, 'sssssssssssssssss', '1231.00', 21, 2, 0, 2, 'sssssssssssssssss', 2, 36, 'sssssssssssssssss', 'sssssssssssssssss', 2, 'a:0:{}', 1, '', ''),
(95, 1536527800, 4, 1, 1, 'admin@admin.admin', '6545', 'admin', 'admin', '41a5fd1f309869282433974acf72788d', 1, 1537892800, 0, 0, '', '0.00', 0, 0, 1, 0, '', 0, 0, '', '', 1, '', 1, '', ''),
(96, 1531846006, 1, 1, 0, 'qweqwe@qwe.qwe', '123123', 'asdas', 'asd', 'db5bc23b632f1e12e4ee92f076fe8877', 1, 1543940772, 0, 0, '', '0.00', 0, 0, 1, 0, '', 0, 4, '', '', 1, '', 1, '', ''),
(97, 1543490658, 4, 1, 0, 'test@test.ru', '1231231231111', '', '', '81004725b35229a80e1a998e9ea59442', 1, 1543497985, 0, 0, '', '0.00', 23, 0, 5, 0, '', 0, 0, '', '', 0, '', 1, '', ''),
(98, 1543578661, 0, 0, 0, 'cour@test.te', '321213231231231213', '', '', '07bb4dd7bc3b1f4013bf3a73d569e50f', 1, 0, 0, 0, '', '0.00', 0, 0, 0, 0, '', 0, 0, '', '', 0, '', 1, '', ''),
(99, 1543578865, 0, 0, 0, 'ssssadfasd@asd.as', '12231213231231213', '', '', '00d0add73af6f8a6b86a86a1efd6c399', 1, 0, 0, 0, '', '0.00', 0, 0, 0, 0, '', 0, 0, '', '', 0, '', 1, '', ''),
(101, 1543581062, 0, 0, 0, 'dsfsddfs@asd.as', '4324334234234243243243243', 'asdasdsdasda', '', '4a2511b001c66250c6dc8dc9f50ed5c5', 1, 0, 0, 0, '', '0.00', 0, 0, 0, 0, '', 0, 0, '', '', 0, '', 1, '', ''),
(103, 1543581085, 0, 0, 0, 'dsasdafsadfas@asdas.da', '23412142323244231231', 'sdfasdfafdssfda', '', '4a2511b001c66250c6dc8dc9f50ed5c5', 1, 0, 0, 0, '', '0.00', 0, 0, 0, 0, '', 0, 0, '', '', 0, '', 1, '', ''),
(104, 1543581140, 0, 0, 0, 'sdafafasdffas@asdasd.sa', '213123123132132', '213321213231', '', '4a2511b001c66250c6dc8dc9f50ed5c5', 1, 0, 0, 0, '', '0.00', 0, 0, 0, 0, '', 0, 0, '', '', 0, '', 1, '', ''),
(106, 1543581226, 0, 0, 0, 'dsfsadfasd@asdasd.as', '321421234342114234231342', 'sdsdaasdasfd', '', '4a2511b001c66250c6dc8dc9f50ed5c5', 1, 0, 0, 0, '', '0.00', 0, 0, 0, 0, '', 0, 0, '', '', 0, '', 1, '', ''),
(108, 1543581292, 0, 0, 0, 'tsetsdfasd@sadas.sa', '32234342234423432342', 'testse', '', '4a2511b001c66250c6dc8dc9f50ed5c5', 1, 0, 0, 0, '', '0.00', 0, 0, 0, 0, '', 0, 0, '', '', 0, '', 1, '', ''),
(110, 1543581421, 0, 0, 0, 'ssssss@sss.sss', '22222222222223333333', 'sssssssss311111', '', '', 1, 0, 0, 0, '', '0.00', 0, 0, 0, 0, '', 0, 0, '', '', 0, '', 1, '', ''),
(111, 1543932696, 5, 0, 0, 'gdheheh@bbn.yt', '+91187896541', 'брондон', '', '41a5fd1f309869282433974acf72788d', 1, 0, 0, 0, '', '0.00', 0, 0, 0, 0, '', 0, 0, '', '', 0, '', 1, '', 'a:1:{i:0;s:2:\"36\";}'),
(112, 1543932805, 5, 0, 0, 'hddbkw@list.ru', '+879621584565', 'треолан', '', '41a5fd1f309869282433974acf72788d', 1, 0, 0, 0, '', '0.00', 0, 0, 0, 0, '', 0, 0, '', '', 0, '', 1, '', 'a:1:{i:0;s:1:\"5\";}');

-- --------------------------------------------------------

--
-- Структура таблицы `user_group`
--

CREATE TABLE `user_group` (
  `id` int(255) NOT NULL,
  `name` varchar(1024) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `user_group`
--

INSERT INTO `user_group` (`id`, `name`) VALUES
(2, 'mod'),
(4, '2312'),
(5, '23123'),
(7, 'ssdasa');

-- --------------------------------------------------------

--
-- Структура таблицы `user_group_rights`
--

CREATE TABLE `user_group_rights` (
  `id` int(255) NOT NULL,
  `admin_page_id` int(255) NOT NULL,
  `user_group_id` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `user_group_rights`
--

INSERT INTO `user_group_rights` (`id`, `admin_page_id`, `user_group_id`) VALUES
(4, 3, 4),
(5, 3, 5),
(6, 1, 5),
(9, 1, 7),
(10, 3, 7),
(13, 1, 4),
(14, 4, 4),
(19, 6, 2),
(71, 3, 1),
(72, 1, 1),
(73, 4, 1),
(74, 5, 1),
(75, 6, 1),
(76, 6, 2),
(77, 7, 1),
(78, 8, 1),
(79, 11, 1),
(80, 12, 1),
(81, 13, 1),
(82, 15, 1),
(83, 16, 1),
(84, 17, 1),
(85, 18, 1),
(86, 19, 1),
(87, 20, 1),
(88, 21, 1),
(89, 22, 1),
(90, 23, 1),
(91, 24, 1),
(92, 25, 1),
(93, 26, 1),
(94, 27, 1),
(95, 28, 1),
(96, 30, 1),
(97, 31, 1),
(98, 32, 1),
(99, 33, 1),
(100, 34, 1),
(101, 35, 1),
(102, 36, 1),
(103, 37, 1),
(104, 38, 1),
(105, 39, 1),
(106, 40, 1),
(107, 41, 1),
(108, 42, 1),
(109, 45, 1),
(110, 46, 1),
(111, 47, 1),
(112, 48, 1),
(113, 49, 1),
(114, 50, 1),
(115, 43, 1),
(116, 44, 1),
(117, 51, 1),
(118, 52, 1),
(119, 53, 1),
(120, 54, 1),
(121, 7, 1),
(122, 55, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `user_status`
--

CREATE TABLE `user_status` (
  `id` int(255) NOT NULL,
  `type` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `user_status`
--

INSERT INTO `user_status` (`id`, `type`) VALUES
(1, 'Не верифицирован'),
(2, 'Ожидает проверки'),
(3, 'Требует правки'),
(4, 'Отказано'),
(5, 'Верифицирован');

-- --------------------------------------------------------

--
-- Структура таблицы `user_type`
--

CREATE TABLE `user_type` (
  `id` int(255) NOT NULL,
  `type_code` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `user_type`
--

INSERT INTO `user_type` (`id`, `type_code`, `type`) VALUES
(1, 'admin', 'Администратор'),
(2, 'moderator', 'Модератор'),
(3, 'buyer', 'Покупатель'),
(4, 'seller', 'Продавец'),
(5, 'supplier', 'Поставщик');

-- --------------------------------------------------------

--
-- Структура таблицы `warehouse`
--

CREATE TABLE `warehouse` (
  `id` int(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `status_id` int(3) NOT NULL,
  `owner_id` int(255) NOT NULL,
  `address` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `warehouse`
--

INSERT INTO `warehouse` (`id`, `name`, `status_id`, `owner_id`, `address`) VALUES
(1, 'test', 1, 47, 'werertgwergw'),
(2, 'test', 1, 47, 'werertgwergw'),
(3, 'fsdgsdgs', 2, 33, 'sdbfasfd asefdqwfasdfd asefqw ewerr');

-- --------------------------------------------------------

--
-- Структура таблицы `warehouse_product`
--

CREATE TABLE `warehouse_product` (
  `id` int(11) NOT NULL,
  `warehouse_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `price` decimal(65,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `warehouse_product`
--

INSERT INTO `warehouse_product` (`id`, `warehouse_id`, `product_id`, `price`) VALUES
(77, 1, 74, '0.00'),
(78, 2, 74, '0.00'),
(79, 3, 74, '0.00'),
(208, 1, 86, '1232.00'),
(209, 2, 86, '1232.00'),
(210, 3, 86, '1232.00'),
(211, 2, 87, '123231123.00'),
(212, 3, 87, '123231123.00'),
(213, 2, 88, '213123123.00'),
(214, 3, 88, '213123123.00'),
(215, 1, 89, '213123.00'),
(216, 2, 89, '213123.00'),
(217, 3, 89, '213123.00'),
(221, 2, 90, '1500.00'),
(222, 3, 90, '1500.00'),
(226, 1, 91, '300.00');

-- --------------------------------------------------------

--
-- Структура таблицы `warehouse_status`
--

CREATE TABLE `warehouse_status` (
  `id` int(255) NOT NULL,
  `type` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `warehouse_status`
--

INSERT INTO `warehouse_status` (`id`, `type`) VALUES
(1, 'Доступен'),
(2, 'Ограничен'),
(3, 'Удален');

-- --------------------------------------------------------

--
-- Структура таблицы `wishlist`
--

CREATE TABLE `wishlist` (
  `id` int(255) NOT NULL,
  `user_id` int(255) NOT NULL,
  `product_id` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `wishlist`
--

INSERT INTO `wishlist` (`id`, `user_id`, `product_id`) VALUES
(62, 50, 21),
(63, 50, 13),
(64, 50, 27),
(102, 22, 29),
(103, 22, 33),
(109, 23, 18),
(110, 23, 29),
(111, 23, 43),
(112, 23, 35),
(113, 23, 27),
(114, 23, 36),
(115, 23, 34),
(116, 96, 90);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `admin_pages`
--
ALTER TABLE `admin_pages`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `admin_page_category`
--
ALTER TABLE `admin_page_category`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `block`
--
ALTER TABLE `block`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `brands`
--
ALTER TABLE `brands`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `alias_unique` (`alias`);

--
-- Индексы таблицы `color_scheme`
--
ALTER TABLE `color_scheme`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `courier`
--
ALTER TABLE `courier`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Индексы таблицы `delivery_address`
--
ALTER TABLE `delivery_address`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Индексы таблицы `delivery_status`
--
ALTER TABLE `delivery_status`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `delivery_type`
--
ALTER TABLE `delivery_type`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `dialog`
--
ALTER TABLE `dialog`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sender_id` (`sender_id`),
  ADD KEY `receiver_id` (`receiver_id`);

--
-- Индексы таблицы `discount`
--
ALTER TABLE `discount`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `title_unique` (`title`);

--
-- Индексы таблицы `info_slider`
--
ALTER TABLE `info_slider`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `juristic_data`
--
ALTER TABLE `juristic_data`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `juristic_type`
--
ALTER TABLE `juristic_type`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `message`
--
ALTER TABLE `message`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `dialog_id` (`dialog_id`);

--
-- Индексы таблицы `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `alias_unique` (`alias`);

--
-- Индексы таблицы `news_category`
--
ALTER TABLE `news_category`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `alias` (`alias`);

--
-- Индексы таблицы `order_address`
--
ALTER TABLE `order_address`
  ADD PRIMARY KEY (`id`),
  ADD KEY `order_id` (`order_id`);

--
-- Индексы таблицы `order_product`
--
ALTER TABLE `order_product`
  ADD PRIMARY KEY (`id`),
  ADD KEY `order_id` (`order_id`),
  ADD KEY `product_id` (`product_id`),
  ADD KEY `buyer_id` (`buyer_id`),
  ADD KEY `warehouse_id` (`shop_id`);

--
-- Индексы таблицы `order_status`
--
ALTER TABLE `order_status`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `pay_card`
--
ALTER TABLE `pay_card`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Индексы таблицы `pay_status`
--
ALTER TABLE `pay_status`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `pay_type`
--
ALTER TABLE `pay_type`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `price`
--
ALTER TABLE `price`
  ADD PRIMARY KEY (`id`),
  ADD KEY `shop_id_price` (`shop_id`);

--
-- Индексы таблицы `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `alias_unique` (`alias`);

--
-- Индексы таблицы `product_promotion`
--
ALTER TABLE `product_promotion`
  ADD PRIMARY KEY (`id`),
  ADD KEY `promotion_id` (`promotion_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Индексы таблицы `product_status`
--
ALTER TABLE `product_status`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `promotion`
--
ALTER TABLE `promotion`
  ADD PRIMARY KEY (`id`),
  ADD KEY `owner_id` (`owner_id`);

--
-- Индексы таблицы `properties`
--
ALTER TABLE `properties`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `property_enum`
--
ALTER TABLE `property_enum`
  ADD PRIMARY KEY (`id`),
  ADD KEY `property_id_enum` (`property_id`);

--
-- Индексы таблицы `property_group`
--
ALTER TABLE `property_group`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `property_value`
--
ALTER TABLE `property_value`
  ADD PRIMARY KEY (`id`),
  ADD KEY `property_id_value` (`property_id`),
  ADD KEY `product_id` (`product_id`),
  ADD KEY `value_enum` (`value_enum`);

--
-- Индексы таблицы `question`
--
ALTER TABLE `question`
  ADD PRIMARY KEY (`id`),
  ADD KEY `parent_id` (`parent_id`);

--
-- Индексы таблицы `question_category`
--
ALTER TABLE `question_category`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `question_type`
--
ALTER TABLE `question_type`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `rate`
--
ALTER TABLE `rate`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `region`
--
ALTER TABLE `region`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `report_Transaction`
--
ALTER TABLE `report_Transaction`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_from` (`user_from`),
  ADD KEY `user_to` (`user_to`);

--
-- Индексы таблицы `request_output`
--
ALTER TABLE `request_output`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Индексы таблицы `restriction`
--
ALTER TABLE `restriction`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `review`
--
ALTER TABLE `review`
  ADD PRIMARY KEY (`id`),
  ADD KEY `buyer_id` (`buyer_id`),
  ADD KEY `order_id` (`order_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Индексы таблицы `review_like`
--
ALTER TABLE `review_like`
  ADD PRIMARY KEY (`id`),
  ADD KEY `review_id` (`review_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Индексы таблицы `seller_type`
--
ALTER TABLE `seller_type`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `shop`
--
ALTER TABLE `shop`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `shopcase`
--
ALTER TABLE `shopcase`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `shop_categories`
--
ALTER TABLE `shop_categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `category_id` (`category_id`),
  ADD KEY `shop_id` (`shop_id`);

--
-- Индексы таблицы `shop_status`
--
ALTER TABLE `shop_status`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `slider`
--
ALTER TABLE `slider`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `store_order`
--
ALTER TABLE `store_order`
  ADD PRIMARY KEY (`id`),
  ADD KEY `buyer_id` (`buyer_id`),
  ADD KEY `parent_id` (`parent_id`);

--
-- Индексы таблицы `support_question`
--
ALTER TABLE `support_question`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `template`
--
ALTER TABLE `template`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email_unique` (`email`),
  ADD UNIQUE KEY `phone_unique` (`phone`),
  ADD KEY `juristic_data_id` (`juristic_data_id`);

--
-- Индексы таблицы `user_group`
--
ALTER TABLE `user_group`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `user_group_rights`
--
ALTER TABLE `user_group_rights`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_group_rights_admin_page_id` (`admin_page_id`),
  ADD KEY `user_group_rights_user_group_id` (`user_group_id`);

--
-- Индексы таблицы `user_status`
--
ALTER TABLE `user_status`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `user_type`
--
ALTER TABLE `user_type`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `warehouse`
--
ALTER TABLE `warehouse`
  ADD PRIMARY KEY (`id`),
  ADD KEY `owner_id` (`owner_id`);

--
-- Индексы таблицы `warehouse_product`
--
ALTER TABLE `warehouse_product`
  ADD PRIMARY KEY (`id`),
  ADD KEY `warehouse_id` (`warehouse_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Индексы таблицы `warehouse_status`
--
ALTER TABLE `warehouse_status`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `wishlist`
--
ALTER TABLE `wishlist`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `product_id` (`product_id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `admin_pages`
--
ALTER TABLE `admin_pages`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;
--
-- AUTO_INCREMENT для таблицы `admin_page_category`
--
ALTER TABLE `admin_page_category`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT для таблицы `block`
--
ALTER TABLE `block`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблицы `brands`
--
ALTER TABLE `brands`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT для таблицы `category`
--
ALTER TABLE `category`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=83;
--
-- AUTO_INCREMENT для таблицы `color_scheme`
--
ALTER TABLE `color_scheme`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблицы `contact`
--
ALTER TABLE `contact`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблицы `courier`
--
ALTER TABLE `courier`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `delivery_address`
--
ALTER TABLE `delivery_address`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT для таблицы `delivery_status`
--
ALTER TABLE `delivery_status`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблицы `delivery_type`
--
ALTER TABLE `delivery_type`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблицы `dialog`
--
ALTER TABLE `dialog`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT для таблицы `discount`
--
ALTER TABLE `discount`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT для таблицы `info_slider`
--
ALTER TABLE `info_slider`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT для таблицы `juristic_data`
--
ALTER TABLE `juristic_data`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;
--
-- AUTO_INCREMENT для таблицы `juristic_type`
--
ALTER TABLE `juristic_type`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `message`
--
ALTER TABLE `message`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=156;
--
-- AUTO_INCREMENT для таблицы `news`
--
ALTER TABLE `news`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT для таблицы `news_category`
--
ALTER TABLE `news_category`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT для таблицы `order_address`
--
ALTER TABLE `order_address`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `order_product`
--
ALTER TABLE `order_product`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=223;
--
-- AUTO_INCREMENT для таблицы `order_status`
--
ALTER TABLE `order_status`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT для таблицы `pay_card`
--
ALTER TABLE `pay_card`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT для таблицы `pay_status`
--
ALTER TABLE `pay_status`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT для таблицы `pay_type`
--
ALTER TABLE `pay_type`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблицы `price`
--
ALTER TABLE `price`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT для таблицы `product`
--
ALTER TABLE `product`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=405;
--
-- AUTO_INCREMENT для таблицы `product_promotion`
--
ALTER TABLE `product_promotion`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `product_status`
--
ALTER TABLE `product_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT для таблицы `promotion`
--
ALTER TABLE `promotion`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблицы `properties`
--
ALTER TABLE `properties`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;
--
-- AUTO_INCREMENT для таблицы `property_enum`
--
ALTER TABLE `property_enum`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT для таблицы `property_group`
--
ALTER TABLE `property_group`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `property_value`
--
ALTER TABLE `property_value`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4457;
--
-- AUTO_INCREMENT для таблицы `question`
--
ALTER TABLE `question`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT для таблицы `question_category`
--
ALTER TABLE `question_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT для таблицы `question_type`
--
ALTER TABLE `question_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT для таблицы `rate`
--
ALTER TABLE `rate`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблицы `region`
--
ALTER TABLE `region`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `report_Transaction`
--
ALTER TABLE `report_Transaction`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT для таблицы `request_output`
--
ALTER TABLE `request_output`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT для таблицы `restriction`
--
ALTER TABLE `restriction`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблицы `review`
--
ALTER TABLE `review`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблицы `review_like`
--
ALTER TABLE `review_like`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `seller_type`
--
ALTER TABLE `seller_type`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `shop`
--
ALTER TABLE `shop`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT для таблицы `shopcase`
--
ALTER TABLE `shopcase`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблицы `shop_categories`
--
ALTER TABLE `shop_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=122;
--
-- AUTO_INCREMENT для таблицы `shop_status`
--
ALTER TABLE `shop_status`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT для таблицы `slider`
--
ALTER TABLE `slider`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT для таблицы `store_order`
--
ALTER TABLE `store_order`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=256;
--
-- AUTO_INCREMENT для таблицы `support_question`
--
ALTER TABLE `support_question`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;
--
-- AUTO_INCREMENT для таблицы `template`
--
ALTER TABLE `template`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблицы `user`
--
ALTER TABLE `user`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=113;
--
-- AUTO_INCREMENT для таблицы `user_group`
--
ALTER TABLE `user_group`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT для таблицы `user_group_rights`
--
ALTER TABLE `user_group_rights`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=123;
--
-- AUTO_INCREMENT для таблицы `user_status`
--
ALTER TABLE `user_status`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT для таблицы `user_type`
--
ALTER TABLE `user_type`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT для таблицы `warehouse`
--
ALTER TABLE `warehouse`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблицы `warehouse_product`
--
ALTER TABLE `warehouse_product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=233;
--
-- AUTO_INCREMENT для таблицы `warehouse_status`
--
ALTER TABLE `warehouse_status`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблицы `wishlist`
--
ALTER TABLE `wishlist`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=117;
--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `delivery_address`
--
ALTER TABLE `delivery_address`
  ADD CONSTRAINT `delivery_address_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `dialog`
--
ALTER TABLE `dialog`
  ADD CONSTRAINT `dialog_ibfk_1` FOREIGN KEY (`sender_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `dialog_ibfk_2` FOREIGN KEY (`receiver_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `message`
--
ALTER TABLE `message`
  ADD CONSTRAINT `message_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `message_ibfk_2` FOREIGN KEY (`dialog_id`) REFERENCES `dialog` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `order_address`
--
ALTER TABLE `order_address`
  ADD CONSTRAINT `order_address_ibfk_1` FOREIGN KEY (`order_id`) REFERENCES `store_order` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `order_product`
--
ALTER TABLE `order_product`
  ADD CONSTRAINT `order_product_ibfk_1` FOREIGN KEY (`order_id`) REFERENCES `store_order` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `order_product_ibfk_2` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `pay_card`
--
ALTER TABLE `pay_card`
  ADD CONSTRAINT `pay_card_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `product_promotion`
--
ALTER TABLE `product_promotion`
  ADD CONSTRAINT `product_promotion_ibfk_1` FOREIGN KEY (`promotion_id`) REFERENCES `promotion` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `product_promotion_ibfk_2` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `promotion`
--
ALTER TABLE `promotion`
  ADD CONSTRAINT `promotion_ibfk_1` FOREIGN KEY (`owner_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `question`
--
ALTER TABLE `question`
  ADD CONSTRAINT `question_ibfk_1` FOREIGN KEY (`parent_id`) REFERENCES `question` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `report_Transaction`
--
ALTER TABLE `report_Transaction`
  ADD CONSTRAINT `report_Transaction_ibfk_1` FOREIGN KEY (`user_from`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `report_Transaction_ibfk_2` FOREIGN KEY (`user_to`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `request_output`
--
ALTER TABLE `request_output`
  ADD CONSTRAINT `request_output_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `review`
--
ALTER TABLE `review`
  ADD CONSTRAINT `review_ibfk_1` FOREIGN KEY (`buyer_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `review_ibfk_2` FOREIGN KEY (`order_id`) REFERENCES `store_order` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `review_ibfk_3` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `review_like`
--
ALTER TABLE `review_like`
  ADD CONSTRAINT `review_like_ibfk_1` FOREIGN KEY (`review_id`) REFERENCES `review` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `review_like_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `shop_categories`
--
ALTER TABLE `shop_categories`
  ADD CONSTRAINT `shop_categories_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `shop_categories_ibfk_2` FOREIGN KEY (`shop_id`) REFERENCES `shop` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `store_order`
--
ALTER TABLE `store_order`
  ADD CONSTRAINT `store_order_ibfk_2` FOREIGN KEY (`parent_id`) REFERENCES `store_order` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `warehouse`
--
ALTER TABLE `warehouse`
  ADD CONSTRAINT `warehouse_ibfk_1` FOREIGN KEY (`owner_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `warehouse_product`
--
ALTER TABLE `warehouse_product`
  ADD CONSTRAINT `warehouse_product_ibfk_1` FOREIGN KEY (`warehouse_id`) REFERENCES `warehouse` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `warehouse_product_ibfk_2` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `wishlist`
--
ALTER TABLE `wishlist`
  ADD CONSTRAINT `wishlist_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `wishlist_ibfk_2` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
