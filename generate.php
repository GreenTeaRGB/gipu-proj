<?php
  $t = true;
  $m = true;
  $c = true;
  if(count($_SERVER['argv']) > 1){
    if(!in_array('m', $_SERVER['argv'])){
      $m = false;
    }
    if(!in_array('t', $_SERVER['argv'])){
      $t = false;
    }
    if(!in_array('c', $_SERVER['argv'])){
      $c = false;
    }
  }
  if(!$t && !$m && !$c) die('Нечего создавать');
if(!function_exists("readline")) {
	function readline($prompt = null){
		if($prompt){
			echo $prompt;
		}
		$fp = fopen("php://stdin","r");
		$line = rtrim(fgets($fp, 1024));
		return $line;
	}
}
if(file_exists(dirname(__FILE__).'/Application/config/db_params.php')){
	$db = include dirname(__FILE__).'/Application/config/db_params.php';
}else{
	echo "Не удалось найти файл подключения базы данных\n";
	die;
}

$dbh = new PDO('mysql:host='.$db['host'].';dbname='.$db['dbname'], $db['user'], $db['password']);
$dbh->exec("set names utf8");
if($c) {
  echo "Введите имя Сontroller: ";
  $controller = readline();
  if( $controller == '' ) {
    echo "Название контроллера не должно быть пустым\n";
    die();
  }
  if( file_exists( dirname( __FILE__ ).'/Application/Controllers/Admin'.$controller.'Controller.php' )
      || file_exists( dirname( __FILE__ ).'/core/controllers/Admin'.$controller.'ControllerCore.php' ) ) {
    echo "Такой контроллер уже существует\n";
    die();
  }
}

if($m){
  if($t){
    echo "Введите название пункта меню: ";
    $page_name = readline();
    if($page_name == '') {
      echo "Название меню не должно быть пустым\n";
      die();
    }
  }
  echo "Введите имя Entity: ";
  $entity = readline();
  if($entity == '') {
    echo "Название модели не должно быть пустым\n";
    die();
  }

  if(file_exists(dirname(__FILE__).'/Application/Models/'.$entity.'.php')
      || file_exists(dirname(__FILE__).'/core/models/'.$entity.'Core.php')){
    echo "Такая модель уже существует\n";
    die();
  }
}

if($t){
  echo "Введите название таблицы: ";
  $table_name = readline();
}
echo "==============================================\n";
echo "Создание полей таблицы \n";
echo "ПРИМЕЧАНИЕ!!!: \n";
echo "Поле активности должно инметь код ' active ' \n";
echo "Поле сортировки должно инметь код ' sort ' \n";
echo "Поле id будет создано автоматически\n";
echo "Доступные типы полей ( boolean, text, string, decimal, content, select, mselect, int, datetime, file, modals, color )\n";
echo "Поле с типом 'mselect' всегда будет множественным\n";
echo "При использовании типа select или mselect првязанная модель должна содержать статический метод find и возвращать массив\n";
echo "==============================================\n";
$count = 1;
$name = [];
if($m || $t) {
  while( true ) {
    echo "Введите код поля: ";
    $name[$count] = readline();
    if( $name[$count] == '' ) {
      unset( $name[$count] );
      break;
    }
    echo "Введите тип поля [string]: ";
    $type[$count] = readline();
    if( $type[$count] == '' )
      $type[$count] = 'string';
    $continue = false;
    switch( $type[$count] ) {
      case 'boolean' :
        break;
      case 'text' :
        break;
      case 'string' :
        break;
      case 'decimal' :
        break;
      case 'int' :
        break;
      case 'datetime' :
        break;
      case 'file' :
        break;
      case 'content' :
        break;
      case 'select' :
        break;
      case 'mselect' :
        break;
      case 'modals' :
        break;
      case 'checkbox' :
        break;
      case 'color' :
        break;
      default:
        echo "Типа $type[$count] не существует попробуй еще раз\n";
        unset( $name[$count] );
        unset( $type[$count] );
        $continue = true;
    }
    if( $continue )
      continue;
    if( $type[$count] != 'mselect' && $type[$count] != 'select' ) {
      echo "Введите значение по умолчанию: ";
      $defaultValue[$count] = readline();
    }
    if( $type[$count] != 'sort' && $type[$count] != 'active' ) {
      echo "Отображение поля [true]: ";
      //		$visible[$count] = ;
      if( readline() == '' )
        $visible[$count] = true;
      else $visible[$count] = false;
    }

    echo "Редактирование поля [true]: ";
    //	$editable[$count] = ;
    if( readline() == '' )
      $editable[$count] = true;
    else $editable[$count] = false;

    if( $type[$count] != 'mselect' && $type[$count] != 'select' && $type[$count] != 'modals' ) {
      echo "Моножественное поле [false]: ";
      //		$multi[$count] = ;
      if( readline() == '' )
        $multi[$count] = false;
    } else {
      $multi[$count] = true;
    }

    if( $type[$count] == 'select' || $type[$count] == 'mselect' || $type[$count] == 'modals' ) {
      echo "Модель от куда будут браться данные например ( User ): ";
      $model[$count] = readline();
      if( $model[$count] == '' )
        die( "Модель не может быть пустой попробуй еще раз\n" );

      echo "ID значения в привязанной модели [id]: ";
      $field_id[$count] = readline();
      if( $field_id[$count] == '' )
        $field_id[$count] = 'id';

      echo "Name значения в привязанной модели [name]: ";
      $field_name[$count] = readline();
      if( $field_name[$count] == '' )
        $field_name[$count] = 'name';

    }
    echo "Обязательное поле [false]: ";
    //	$required[$count] = ;
    if( readline() == '' )
      $required[$count] = false;
    else $required[$count] = true;

    echo "Label поля [".$name[$count]."]: ";
    $label[$count] = readline();
    if( $label[$count] == '' )
      $label[$count] = $name[$count];

    $count++;
  }

  if( count( $name ) == 0 ) {
    echo "Ниодного поля не было введено попробуй еще раз\n";
    die();
  }

  $table_fields   = '';
  $entity_types   = '';
  $entity_fields  = '';
  $sort           = false;
  $active         = false;
  $req            = [];
  $string         = [];
  $number         = [];
  $file           = [];
  $double         = [];
  $email          = [];
  $image          = [];
  $datetime       = [];
  $formFields     = '';
  $attributeLabel = '';
  foreach( $name as $key => $value ) {

    if( $value == 'email' )
      $email[] = $value;


    $vi = $visible[$key]
        ? 'true'
        : 'false';
    $ed = $editable[$key]
        ? 'true'
        : 'false';
    $la = trim( $label[$key] );
    $re = $required[$key]
        ? 'true'
        : 'false';
    if( isset( $multi[$key] ) )
      $mu = $multi[$key]
          ? 'true'
          : 'false';
    else $mu = 'false';
    if( $required[$key] )
      $req[] = $value;
    if( $type[$key] == 'sort' || $type[$key] == 'active' ) {
      $vi = 'false';

    }
    if( $value == 'sort' ) {
      $sort = true;
    }
    if( $value == 'active' ) {
      $active = true;
    }
    $attributeLabel .= "'$value'=>'$la',\n";
    switch( $type[$key] ) {
      case 'checkbox' :
      case 'boolean' :
        $type_field    = ' int(1) NOT NULL';
        $entity_types  .= "			'".$value."' => 'int',\n";
        $number[]      = $value;
        $formFields    .= "'".$value."' => ['type'=>'checkbox', 'class'=>['form-control']],\n";
        $entity_fields .= '			
			\''.$value.'\' => array(
				\'type\'=>\'checkbox\',
				\'visible\'=>'.$vi.',
				\'edited\'=>'.$ed.',
				\'label\'=>\''.$la.'\',
				\'required\'=>'.$re.',
				\'multiple\'=>'.$mu.',
			),'."\n";

        break;
      case 'text' :
        $type_field    = ' text NOT NULL';
        $entity_types  .= "			'".$value."' => 'str',\n";
        $formFields    .= "'".$value."' => ['type'=>'textarea', 'class'=>['form-control']],\n";
        $entity_fields .= '			
			\''.$value.'\' => array(
				\'type\'=>\'text\',
				\'visible\'=>'.$vi.',
				\'edited\'=>'.$ed.',
				\'label\'=>\''.$la.'\',
				\'required\'=>'.$re.',
				\'multiple\'=>'.$mu.',
			),'."\n";
        break;
      case 'content' :
        $type_field    = ' text NOT NULL';
        $entity_types  .= "			'".$value."' => 'str',\n";
        $formFields    .= "'".$value."' => ['type'=>'textarea', 'class'=>['form-control']],\n";
        $entity_fields .= '			
			\''.$value.'\' => array(
				\'type\'=>\'content\',
				\'visible\'=>'.$vi.',
				\'edited\'=>'.$ed.',
				\'label\'=>\''.$la.'\',
				\'required\'=>'.$re.',
				\'multiple\'=>'.$mu.',
			),'."\n";
        break;
      case 'mselect' :
        $type_field    = ' varchar(1024) NOT NULL';
        $entity_types  .= "			'".$value."' => 'str',\n";
        $formFields    .= "'".$value."' => ['type'=>'mselect', 'values'=>\\Application\\Models\\{$model[$key]}::find(), 'id'=>'{$field_id[$key]}', 'name'=>'{$field_name[$key]}' ,'class'=>['form-control']],\n";
        $entity_fields .= '			
			\''.$value.'\' => array(
				\'type\'=>\'mselect\',
				\'values\' => '.$model[$key].'::find(),
				\'id_key\'=>\''.$field_id[$key].'\',
				\'name_key\'=>\''.$field_name[$key].'\',
				\'visible\'=>'.$vi.',
				\'edited\'=>'.$ed.',
				\'label\'=>\''.$la.'\',
				\'required\'=>'.$re.',
				\'multiple\'=>'.$mu.',
			),'."\n";

        break;
      case 'file' :
        $type_field   = ' text NOT NULL';
        $entity_types .= "			'".$value."' => 'str',\n";
        if( $value == 'img' || $value == 'image' || $value == 'images' || $value == 'photo' || $value == 'photos' )
          $image[] = $value;
        else
          $file[] = $value;
        $formFields    .= "'".$value."' => ['type'=>'file', 'class'=>['form-control']],\n";
        $entity_fields .= '			
			\''.$value.'\' => array(
				\'type\'=>\'file\',
				\'visible\'=>'.$vi.',
				\'edited\'=>'.$ed.',
				\'label\'=>\''.$la.'\',
				\'required\'=>'.$re.',
				\'file-type\'=>\'image/*\',
				\'multiple\'=>'.$mu.',
			),'."\n";

        break;
      case 'color' :
        $string[]      = $value;
        $type_field    = " varchar(1024) NOT NULL";
        $formFields    .= "'".$value."' => ['type'=>'color', 'class'=>['form-control']],\n";
        $entity_types  .= "			'".$value."' => 'str',\n";
        $entity_fields .= '			
			\''.$value.'\' => array(
				\'type\'=>\'color\',
				\'visible\'=>'.$vi.',
				\'edited\'=>'.$ed.',
				\'label\'=>\''.$la.'\',
				\'required\'=>'.$re.',
				\'multiple\'=>'.$mu.',
			),'."\n";
        break;
      case 'string' :
        $string[]      = $value;
        $type_field    = " varchar(1024) NOT NULL";
        $formFields    .= "'".$value."' => ['type'=>'text', 'class'=>['form-control']],\n";
        $entity_types  .= "			'".$value."' => 'str',\n";
        $entity_fields .= '			
			\''.$value.'\' => array(
				\'type\'=>\'string\',
				\'visible\'=>'.$vi.',
				\'edited\'=>'.$ed.',
				\'label\'=>\''.$la.'\',
				\'required\'=>'.$re.',
				\'multiple\'=>'.$mu.',
			),'."\n";

        break;
      case 'decimal' :
        $type_field    = " DECIMAL(65,2) NOT NULL";
        $formFields    .= "'".$value."' => ['type'=>'text', 'class'=>['form-control']],\n";
        $entity_types  .= "			'".$value."' => 'str',\n";
        $double[]      = $value;
        $entity_fields .= '			
			\''.$value.'\' => array(
				\'type\'=>\'string\',
				\'visible\'=>'.$vi.',
				\'edited\'=>'.$ed.',
				\'label\'=>\''.$la.'\',
				\'required\'=>'.$re.',
				\'multiple\'=>'.$mu.',
			),'."\n";

        break;
      case 'select' :
        $type_field    = " int(255) NOT NULL";
        $entity_types  .= "			'".$value."' => 'int',\n";
        $formFields    .= "'".$value."' => ['type'=>'select', 'values'=>\\Application\\Models\\{$model[$key]}::find(), 'id'=>'{$field_id[$key]}', 'name'=>'{$field_name[$key]}' ,'class'=>['form-control']],\n";
        $number[]      = $value;
        $entity_fields .= '			
			\''.$value.'\' => array(
				\'type\'=>\'select\',
				\'values\' => '.$model[$key].'::find(),
				\'id_key\'=>\''.$field_id[$key].'\',
				\'name_key\'=>\''.$field_name[$key].'\',
				\'visible\'=>'.$vi.',
				\'edited\'=>'.$ed.',
				\'label\'=>\''.$la.'\',
				\'required\'=>'.$re.',
				\'multiple\'=>'.$mu.',
			),'."\n";

        break;
      case 'modals' :
        $type_field    = " int(255) NOT NULL";
        $entity_types  .= "			'".$value."' => 'int',\n";
        $formFields    .= "'".$value."' => ['type'=>'select', 'values'=>\\Application\\Models\\{$model[$key]}::find(), 'id'=>'{$field_id[$key]}', 'name'=>'{$field_name[$key]}' ,'class'=>['form-control']],\n";
        $number[]      = $value;
        $entity_fields .= '			
			\''.$value.'\' => array(
				\'type\'=>\'modals\',
				\'class\' => \\Application\\Models\\'.$model[$key].',
				\'visible\'=>'.$vi.',
				\'edited\'=>'.$ed.',
				\'label\'=>\''.$la.'\',
				\'required\'=>'.$re.',
				\'multiple\'=>'.$mu.',
			),'."\n";

        break;
      case 'int' :
        $type_field    = " int(255) NOT NULL";
        $formFields    .= "'".$value."' => ['type'=>'text', 'class'=>['form-control']],\n";
        $entity_types  .= "			'".$value."' => 'int',\n";
        $number[]      = $value;
        $entity_fields .= '			
			\''.$value.'\' => array(
				\'type\'=>\'string\',
				\'visible\'=>'.$vi.',
				\'edited\'=>'.$ed.',
				\'label\'=>\''.$la.'\',
				\'required\'=>'.$re.',
				\'multiple\'=>'.$mu.',
			),'."\n";

        break;
      case 'datetime' :
        $datetime[]    = $value;
        $formFields    .= "'".$value."' => ['type'=>'date', 'class'=>['form-control']],\n";
        $entity_fields .= '			
			\''.$value.'\' => array(
				\'type\'=>\'date\',
				\'visible\'=>'.$vi.',
				\'edited\'=>'.$ed.',
				\'label\'=>\''.$la.'\',
				\'required\'=>'.$re.',
				\'multiple\'=>'.$mu.',
			),'."\n";

        $type_field   = " INT(60) NOT NULL ";
        $entity_types .= "			'".$value."' => 'str',\n";
        break;
    }
    $table_fields .= "`".$value."`".$type_field;
    if( isset( $defaultValue[$key] ) && $defaultValue[$key] != '' )
      $table_fields .= " DEFAULT '".$defaultValue[$key]."'";
    $table_fields .= ',';
  }
  $rules   = '';
  $setters = '';
  $getters = '';
  if( count( $req ) > 0 ) {
    $rules .= "[[\"".implode( '","', $req )."\"], 'required'],\n";
  }
  if( count( $email ) > 0 ) {
    $rules .= "[[\"".implode( '","', $email )."\"], 'email'],\n";
  }
  if( count( $string ) > 0 ) {
    $rules .= "[[\"".implode( '","', $string )."\"], 'string'],\n";
  }
  if( count( $number ) > 0 ) {
    $rules .= "[[\"".implode( '","', $number )."\"], 'number'],\n";
  }
  if( count( $file ) > 0 ) {
    $rules .= "[[\"".implode( '","', $file )."\"], 'file', 'extensions' => [/* Нужно заполнить */]],\n";
  }
  if( count( $image ) > 0 ) {
    $rules .= "[[\"".implode( '","', $image )."\"], 'image', 'extensions' => ['png', 'jpg', 'jpeg']],\n";
  }
  if( count( $double ) > 0 ) {
    $rules .= "[[\"".implode( '","', $double )."\"], 'double'],\n";
  }
  $beforeInsert = '$unixTime = $this->getUnixTime();'."\n";

  if( count( $datetime ) > 0 ) {

    foreach( $datetime as $field_time ) {
      $funcName = trim(
          strtolower(
              str_replace(
                  ' ', '', str_replace(
                  [
                      '-',
                      '_',
                      '.'
                  ], ' ', preg_replace( '/(?<![A-Z])[A-Z]/', ' \0', $field_time )
                  )
              )
          )
      );
      $beforeInsert .= '$data['.$field_time.'] = $unixTime;';
      $setters  .= '
		protected function set'.ucfirst( $funcName ).'($element){
			return strtotime( $element );
		}
		';
      $getters  .= '
		protected function get'.ucfirst( $funcName ).'( $element )
		{
			return date( \'d.m.Y\', $element );
		}
		';
    }
    $rules .= "[[\"".implode( '","', $datetime )."\"], 'date'],\n";

  }

  if( $table_fields == '' )
    die( "Поля таблицы не могут быть пустыми попробуй еще раз\n" );
}
echo "Подтвердить создание сущеностей y/n [y]:";
$confirm = readline();
if($confirm != '' && ($confirm != 'Y' || $confirm != 'y')) die("Отмена\n");
$options = '';
if($sort){
	$options = ',[\'sortable\'=>\'sort\']';
}
if($active){
	$options = ',[\'activate\'=>\'active\']';
}
if($sort && $active){
	$options = ',[\'activate\'=>\'active\', \'sortable\'=>\'sort\']';
}

$phpController = '<?php
namespace Application\Controllers;
use \Application\Classes\AdminBuilder;
	use \Application\Models\\'.$entity.';
class Admin'.$controller.'Controller extends \Application\Classes\AdminBase
{

	public function actionIndex($page = 1)
	{
		$builder = new AdminBuilder(new '.$entity.'(), [] '.$options.');
		$builder->index($page);
		return true;
	}

	public function actionCreate()
	{
		$builder = new AdminBuilder(new '.$entity.'());
		$builder->create();
		return true;
	}

	public function actionEdit($id)
	{
		$builder = new AdminBuilder(new '.$entity.'());
		$builder->edit($id);
		return true;
	}
	
	public function actionActivate(){
		$builder = new AdminBuilder(new '.$entity.'());
		$builder->active();
		 return true;
	}
	
	public function actionSortable(){
		$builder = new AdminBuilder(new '.$entity.'());
		$builder->sort();
		 return true;
	}
	public function actionDelete(){
		$builder = new AdminBuilder(new '.$entity.'());
		$builder->delete();
		 return true;
	}
		public function actionModal(){
    $builder = new AdminBuilder(new '.$entity.'());
    $builder->modal();
    return true;
  }
	
}
?>';

$phpEntity = '<?php
namespace Application\Models;
class '.$entity.' extends \Application\Classes\Model implements \Application\Classes\Forms\Interfaces\FormBuilderInterface
{
	public function __construct()
	{
		parent::__construct(\''.($t?$table_name:'').'\', $this->getTypes(), $this);
	}

	public function getTypes()
	{
		return [
			\'id\'          => \'int\',
			'.$entity_types.'
		];
	}

	public function getFields()
	{
		return array(
			\'id\'          => array(
				\'type\'    => \'string\',
				\'visible\' => true,
				\'edited\'  => false,
				\'label\'   => \'ID\'
			),
		'.$entity_fields.'
		);
	}
	
	public function rules(): array
	{
		return [
			'.$rules.'
		];
	}
	
	public function attributeLabels(): array
	{
		return [
			'.$attributeLabel.'
		];
	}
	
	public function formFields(array $data = [])
	{
		$fields = [
				'.$formFields.'
			];
			if( count( $data ) > 0 ) {
				$fieldsFilter = [];
				foreach( $data as $datum ) {
					if( isset( $fields[ $datum ] ) )
						$fieldsFilter[ $datum ] = $fields[ $datum ];
				}
				return $fieldsFilter;
			}
			return $fields;
	}
    protected function beforeInsert( array $data ):array
    {
      '.$beforeInsert.'
      return $data;
    }
'.$setters.'
'.$getters.'

}
';
if($c){
  if(!file_exists(dirname(__FILE__).'/Application/config/BuilderRouters.php'))
    file_put_contents(dirname(__FILE__).'/Application/config/BuilderRouters.php', '<?php return []; ?>');

  $routers = include dirname(__FILE__).'/Application/config/BuilderRouters.php';
  $oldRouters = '';
  foreach ($routers as $url => $router) {
    $oldRouters .= '   "'.$url. '" => "'.$router.'"'.",\n";
  }

  $addRoutes = '
		\'admin/'.strtolower($controller).'/create\' => \'admin'.$controller.'/create\',
		\'admin/'.strtolower($controller).'/modal\' => \'admin'.$controller.'/modal\',
		\'admin/'.strtolower($controller).'/delete\' => \'admin'.$controller.'/delete\',
		\'admin/'.strtolower($controller).'/activate\' => \'admin'.$controller.'/activate\',
		\'admin/'.strtolower($controller).'/sortable\' => \'admin'.$controller.'/sortable\',
		\'admin/'.strtolower($controller).'/edit/([0-9]+)\' => \'admin'.$controller.'/edit/$1\',
		\'admin/'.strtolower($controller).'/page-([0-9]+)\' => \'admin'.$controller.'/index/$1\',
		\'admin/'.strtolower($controller).'\' => \'admin'.$controller.'/index\',
';

  $phpRouters = '
<?php
	return [
	'.$oldRouters.'
	'.$addRoutes.'
	];
';
  file_put_contents(dirname(__FILE__).'/Application/config/BuilderRouters.php', $phpRouters);
  file_put_contents(dirname(__FILE__).'/Application/Controllers/Admin'.$controller.'Controller.php', $phpController);
  chmod(dirname(__FILE__).'/Application/Controllers/Admin'.$controller.'Controller.php', 0777);
}


if($m){

  file_put_contents(dirname(__FILE__).'/Application/Models/'.$entity.'.php', $phpEntity);
  chmod(dirname(__FILE__).'/Application/Models/'.$entity.'.php', 0777);
}

if($t){
  $dbh->query('CREATE TABLE IF NOT EXISTS `'.$table_name.'` (
`id` int(255) NOT NULL AUTO_INCREMENT,
'.$table_fields.'
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;');
  $dbh->query("INSERT INTO  `admin_pages` (
`id` ,
`name` ,
`icon` ,
`uri` ,
`sort_order` ,
`site_access` ,
`visible`
)
VALUES (
NULL ,  '".$page_name."',  '<i class=\"fa fa-database\" aria-hidden=\"true\"></i>',  '".strtolower($controller)."',  '4000',  '4',  '1'
);
");
  $sql ="SELECT * FROM `admin_pages` WHERE `uri`='".strtolower($controller)."'";

  foreach ($dbh->query($sql) as $row) {
    $dbh->query("INSERT INTO  `user_group_rights` (
`admin_page_id` ,
`user_group_id`
)
VALUES ('".$row['id']."', 1);
");
  }
}


echo "Успешно завершено!\n";